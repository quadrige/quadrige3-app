FROM gitlab-registry.ifremer.fr/ifremer-commons/docker/images/custom/web-static-nginx:1.27-alpine-slim AS runtime

# Copy files
COPY --chown=nginx:gcontainer www /usr/share/nginx/html
