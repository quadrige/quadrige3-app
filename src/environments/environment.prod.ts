import { Environment, StorageDrivers } from '@sumaris-net/ngx-components';

// @ts-ignore
import pkg from '../../package.json';

export const environment = Object.freeze(<Environment>{
  name: pkg.name as string,
  version: pkg.version as string,
  production: true,
  externalEnvironmentUrl: 'assets/environments/environment.json',
  baseUrl: '/',
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',
  mock: false,

  // Must be change manually. Can be overridden using Pod properties 'quadrige3.app.min.version'
  peerMinVersion: '4.0.0',

  // FIXME: GraphQL subscription never unsubscribe...
  listenRemoteChanges: false,

  // FIXME: enable cache
  persistCache: false,

  // Leave null,
  defaultPeer: null,

  // first deployment config
  defaultPeers: [
    {
      host: 'localhost',
      port: 8080,
    },
    {
      host: 'localhost',
      port: 8081,
    },
  ],

  /* initial config but not valid ,
  // Production and public peers
  defaultPeers: [
    {
      host: 'quadrige3-admin.ifremer.fr',
      port: 443
    }
  ],
*/

  defaultAppName: 'Quadrige',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=fr.ifremer.quadrige3.app',

  account: {
    showAccountButton: false,
    enableListenChanges: true,
    listenIntervalInSeconds: 10,
  },

  // Storage
  storage: {
    driverOrder: [StorageDrivers.SQLLite, StorageDrivers.IndexedDB, StorageDrivers.WebSQL, StorageDrivers.LocalStorage],
  },

  // About
  sourceUrl: pkg.repository && (pkg.repository.url as string),
  reportIssueUrl: pkg.bugs && (pkg.bugs.url as string),
});
