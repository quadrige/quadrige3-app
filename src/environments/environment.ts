// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { Environment, StorageDrivers } from '@sumaris-net/ngx-components';

// @ts-ignore
import pkg from '../../package.json';

export const environment = Object.freeze(<Environment>{
  name: pkg.name as string,
  version: pkg.version as string,
  production: false,
  externalEnvironmentUrl: 'assets/environments/environment-dev.json',
  baseUrl: '/',
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',
  connectionTimeout: 1000,
  mock: false,

  // Must be change manually. Can be override using Pod properties 'quadrige3.app.min.version'
  peerMinVersion: '4.2.1',

  // FIXME: GraphQL subscription never unsubscribe...
  listenRemoteChanges: false,

  // FIXME: enable cache
  persistCache: false,

  // TODO: make this works
  //offline: true,

  defaultPeers: [
    {
      host: 'ludovic-ubuntu-20',
      port: 8080,
    },
    {
      host: 'ludovic-windows-10',
      port: 8080,
    },
    {
      host: 'localhost',
      port: 8080,
    },
    {
      host: 'localhost',
      port: 8081,
    },
    {
      host: 'visi-common-docker3.ifremer.fr',
      port: 8080,
    },
    {
      host: 'quadrige-core.isival.ifremer.fr',
      port: 443,
    },
  ],

  defaultAppName: 'Quadrige',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=fr.ifremer.quadrige3.app',

  // Default login user
  defaultAuthValues: {
    // Basic auth (using Person.username)
    username: 'sbocande',
    password: 'stephane',
  },

  account: {
    showAccountButton: false,
    enableListenChanges: true,
    listenIntervalInSeconds: 10,
  },

  // Storage
  storage: {
    driverOrder: [StorageDrivers.SQLLite, StorageDrivers.IndexedDB, StorageDrivers.WebSQL, StorageDrivers.LocalStorage],
  },

  // About
  sourceUrl: pkg.repository && (pkg.repository.url as string),
  reportIssueUrl: pkg.bugs && (pkg.bugs.url as string),
});
