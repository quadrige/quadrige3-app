// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// Environment to use only with unit tests

import { Environment, StorageDrivers } from '@sumaris-net/ngx-components';

// @ts-ignore
import pkg from '../../package.json';

export const environment: Environment = Object.freeze({
  name: pkg.name as string,
  version: pkg.version as string,
  production: false,
  baseUrl: '/',
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',
  mock: false,

  // FIXME: GraphQL subscription never unsubscribe...
  listenRemoteChanges: false,

  // FIXME: enable cache
  persistCache: false,

  peerMinVersion: '1.4.6',

  // TODO: make this works
  //offline: true,

  defaultPeer: {
    host: 'localhost',
    port: 8080,
  },
  defaultPeers: [
    {
      host: 'localhost',
      port: 8080,
    },
    {
      host: 'localhost',
      port: 8081,
    },
  ],

  defaultAppName: 'Quadrige',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=fr.ifremer.quadrige3.app',

  // Storage
  storage: {
    driverOrder: [StorageDrivers.SQLLite, StorageDrivers.IndexedDB, StorageDrivers.WebSQL, StorageDrivers.LocalStorage],
  },
});
