import { ApplicationRef, Component, createComponent, ElementRef, inject, Injector, Input, OnInit, signal, Type, viewChild } from '@angular/core';
import { IAddResult, ISelectCriteria, ISelectModalOptions, SelectMode } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { selectionServiceToken } from '@app/selection/selection.model';
import { IAddTable } from '@app/shared/table/table.model';
import { entitiesTableWithoutSplit } from '@app/referential/model/referential.constants';
import { QuadrigeSharedModule } from '@app/shared/quadrige.shared.module';

@Component({
  selector: 'app-entity-add-modal',
  templateUrl: './entity.add.modal.html',
  standalone: true,
  imports: [QuadrigeSharedModule],
})
export class EntityAddModal extends ModalComponent<IAddResult> implements ISelectModalOptions, OnInit {
  @Input() component: Type<any>;
  @Input() entityName: string;
  @Input() titlePrefixI18n: string;
  @Input() titleAddPrefixI18n: string;
  @Input() addTitle: string;
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() showMode: boolean;
  @Input() mode: SelectMode = 'include';
  @Input() cssClass = 'modal-full-content';
  @Input() fromSelectTable = false;

  protected host = viewChild('host', { read: ElementRef });
  protected addTable: IAddTable<any>;
  protected selectionEmpty = signal(true);
  protected readonly appRef = inject(ApplicationRef);
  protected readonly selectionService = inject(selectionServiceToken, { optional: true });

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    if (!this.component && !this.selectionService) {
      throw new Error('[entity-select-modal] A component must be provided if no SelectionService injected !');
    }

    // Hack table height for table without as-split
    if (entitiesTableWithoutSplit.includes(this.entityName)) {
      this.cssClass = 'modal-content';
    }

    const component = this.component || this.selectionService.getTableComponent(this.entityName, true);
    const componentRef = createComponent(component, { environmentInjector: this.appRef.injector, hostElement: this.host().nativeElement });
    this.appRef.attachView(componentRef.hostView);
    this.addTable = componentRef.instance;

    this.addTable.addTable = true;
    this.addTable.title = this.addTitle;
    this.addTable.titleI18n = this.titleI18n;
    this.addTable.titlePrefixI18n = this.titlePrefixI18n;
    this.addTable.titleAddPrefixI18n = this.titleAddPrefixI18n;
    this.addTable.addCriteria = this.addCriteria;
    this.addTable.addFirstCriteria = this.addFirstCriteria;
    if (!this.addTable.entityName) this.addTable.entityName = this.entityName;
  }

  protected async afterInit() {
    this.registerSubscription(
      this.addTable.permanentSelection.changed.subscribe(() => this.selectionEmpty.set(this.addTable.permanentSelection.isEmpty()))
    );
    await this.addTable.ready();
    // Important: Don't call resetFilter on a IAddTable itself
    await this.addTable.resetFilter(undefined, { emitEvent: true, firstTime: !this.fromSelectTable });
  }

  protected dataToValidate(): IAddResult {
    return { added: this.addTable.selectedEntities, addedIds: this.addTable.selectedEntityIds, mode: this.mode };
  }

  protected onError(e) {
    super.onError(e);
    this.addTable.markAsError(e?.message || e);
  }
}
