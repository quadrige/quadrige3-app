import { NgModule } from '@angular/core';
import { EntitySelectModal } from '@app/selection/entity.select.modal';
import { EntityAddModal } from '@app/selection/entity.add.modal';

@NgModule({
  imports: [EntitySelectModal, EntityAddModal],
  exports: [EntitySelectModal, EntityAddModal],
})
export class SelectionModule {}
