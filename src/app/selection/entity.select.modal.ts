import { ApplicationRef, Component, createComponent, ElementRef, inject, Injector, Input, OnInit, Type, viewChild } from '@angular/core';
import { IAddResult, ISelectCriteria, ISelectModalOptions, ISelectResult, SelectMode } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { isNotEmptyArray, toBoolean } from '@sumaris-net/ngx-components';
import { selectionServiceToken } from '@app/selection/selection.model';
import { ISelectTable } from '@app/shared/table/table.model';
import { entitiesTableNeedingMoreWidth, entitiesTableWithoutSplit } from '@app/referential/model/referential.constants';
import { accountConfigOptions } from '@app/core/config/account.config';
import { QuadrigeSharedModule } from '@app/shared/quadrige.shared.module';

@Component({
  selector: 'app-entity-select-modal',
  templateUrl: './entity.select.modal.html',
  standalone: true,
  imports: [QuadrigeSharedModule],
})
export class EntitySelectModal extends ModalComponent<ISelectResult> implements ISelectModalOptions, OnInit {
  @Input() component: Type<any>;
  @Input() entityName: string;
  @Input() titlePrefixI18n?: string;
  @Input() titleAddPrefixI18n?: string;
  @Input() addTitle?: string;
  @Input() selectCriteria?: ISelectCriteria;
  @Input() addCriteria?: ISelectCriteria;
  @Input() addFirstCriteria?: ISelectCriteria;
  @Input() showMode?: boolean;
  @Input() mode?: SelectMode;
  @Input() cssClass = 'modal-full-content';

  protected host = viewChild('host', { read: ElementRef });
  protected selectTable: ISelectTable<any>;
  protected readonly appRef = inject(ApplicationRef);
  protected readonly selectionService = inject(selectionServiceToken, { optional: true });

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    if (!this.component && !this.selectionService) {
      throw new Error('[entity-select-modal] A component must be provided if no SelectionService injected !');
    }

    // Hack table height for table without as-split
    if (entitiesTableWithoutSplit.includes(this.entityName)) {
      this.cssClass = 'modal-content';
    }

    const component = this.component || this.selectionService.getTableComponent(this.entityName, false);
    const componentRef = createComponent(component, { environmentInjector: this.appRef.injector, hostElement: this.host().nativeElement });
    this.appRef.attachView(componentRef.hostView);
    this.selectTable = componentRef.instance;

    this.canEdit = toBoolean(this.canEdit, true);
    this.selectTable.selectTable = true;
    this.selectTable.canEdit = this.canEdit;
    if (!this.canEdit) this.selectTable.checkBoxSelection = false;
    else this.selectTable.checkBoxSelection = this.settings.getPropertyAsBoolean(accountConfigOptions.tableCheckBoxSelection);
    this.selectTable.titleI18n = this.titleI18n;
    this.selectTable.titlePrefixI18n = this.titlePrefixI18n;
    this.selectTable.selectCriteria = this.selectCriteria;
    if (!this.selectTable.entityName) this.selectTable.entityName = this.entityName;

    if (this.selectTable.openAddEntities) {
      this.registerSubscription(
        this.selectTable.openAddEntities.subscribe(async () => {
          // Open corresponding add modal
          const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
            EntityAddModal,
            {
              fromSelectTable: true,
              entityName: this.entityName,
              addCriteria: { ...this.addCriteria, excludedIds: this.selectTable.selectCriteria.includedIds || [] },
              addFirstCriteria: this.addFirstCriteria,
              titleI18n: this.titleI18n,
              titlePrefixI18n: this.titlePrefixI18n,
              titleAddPrefixI18n: this.titleAddPrefixI18n,
              showMode: false,
            },
            entitiesTableNeedingMoreWidth.includes(this.entityName) ? 'modal-large' : 'modal-medium'
          );

          // Add new values
          if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
            this.selectTable.selectCriteria.includedIds = [...this.selectTable.selectCriteria.includedIds, ...data.addedIds];
            // todo call selectionChanged
            await this.selectTable.resetFilter(undefined, { firstTime: false });
          }
        })
      );
    }
  }

  protected async afterInit() {
    await this.selectTable.ready();
    await this.selectTable.resetFilter(undefined, { firstTime: false });
  }

  protected dataToValidate(): ISelectResult {
    return { selectedIds: this.selectTable.selectCriteria.includedIds, mode: this.mode };
  }

  protected onError(e) {
    super.onError(e);
    this.selectTable.markAsError(e?.message || e);
  }
}
