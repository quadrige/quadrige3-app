import { InjectionToken, Type } from '@angular/core';

export const selectionServiceToken = new InjectionToken<ISelectionService>('selectionService');

export interface ISelectionService {
  getTableComponent(entityName: string, add: boolean): Type<any>;
}
