import { Injectable, Type } from '@angular/core';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { ProgramSelectTable } from '@app/referential/program-strategy/program/program.select.table';
import { ProgramAddTable } from '@app/referential/program-strategy/program/program.add.table';
import { MonitoringLocationAddTable } from '@app/referential/monitoring-location/monitoring-location.add.table';
import { MonitoringLocationSelectTable } from '@app/referential/monitoring-location/monitoring-location.select.table';
import { ISelectionService } from '@app/selection/selection.model';
import { MetaProgram } from '@app/referential/meta-program/meta-program.model';
import { MetaProgramAddTable } from '@app/referential/meta-program/meta-program.add.table';
import { MetaProgramSelectTable } from '@app/referential/meta-program/meta-program.select.table';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { ParameterAddTable } from '@app/referential/parameter/parameter.add.table';
import { ParameterSelectTable } from '@app/referential/parameter/parameter.select.table';
import { Matrix } from '@app/referential/matrix/matrix.model';
import { MatrixAddTable } from '@app/referential/matrix/matrix.add.table';
import { MatrixSelectTable } from '@app/referential/matrix/matrix.select.table';
import { Fraction } from '@app/referential/fraction/fraction.model';
import { FractionAddTable } from '@app/referential/fraction/fraction.add.table';
import { FractionSelectTable } from '@app/referential/fraction/fraction.select.table';
import { Method } from '@app/referential/method/method.model';
import { MethodAddTable } from '@app/referential/method/method.add.table';
import { MethodSelectTable } from '@app/referential/method/method.select.table';
import { Unit } from '@app/referential/unit/unit.model';
import { UnitAddTable } from '@app/referential/unit/unit.add.table';
import { UnitSelectTable } from '@app/referential/unit/unit.select.table';
import { ParameterGroup } from '@app/referential/parameter-group/parameter-group.model';
import { ParameterGroupSelectTable } from '@app/referential/parameter-group/parameter-group.select.table';
import { ParameterGroupAddTable } from '@app/referential/parameter-group/parameter-group.add.table';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { PmfmuAddTable } from '@app/referential/pmfmu/pmfmu.add.table';
import { PmfmuSelectTable } from '@app/referential/pmfmu/pmfmu.select.table';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import { OrderItemSelectTable } from '@app/referential/order-item/order-item.select.table';
import { OrderItemAddTable } from '@app/referential/order-item/order-item.add.table';
import { TaxonGroup } from '@app/referential/taxon-group/taxon-group.model';
import { TaxonGroupAddTable } from '@app/referential/taxon-group/taxon-group.add.table';
import { TaxonGroupSelectTable } from '@app/referential/taxon-group/taxon-group.select.table';
import { TaxonName } from '@app/referential/taxon-name/taxon-name.model';
import { TaxonNameSelectTable } from '@app/referential/taxon-name/taxon-name.select.table';
import { TaxonNameAddTable } from '@app/referential/taxon-name/taxon-name.add.table';
import { Campaign } from '@app/referential/campaign/campaign.model';
import { CampaignAddTable } from '@app/referential/campaign/campaign.add.table';
import { CampaignSelectTable } from '@app/referential/campaign/campaign.select.table';
import { Event } from '@app/referential/event/event.model';
import { EventAddTable } from '@app/referential/event/event.add.table';
import { EventSelectTable } from '@app/referential/event/event.select.table';
import { BatchRef } from '@app/referential/batch/batch-ref.model';
import { BatchRefAddTable } from '@app/referential/batch/batch-ref.add.table';
import { BatchRefSelectTable } from '@app/referential/batch/batch-ref.select.table';
import { User } from '@app/referential/user/user.model';
import { UserAddTable } from '@app/referential/user/user.add.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { Department } from '@app/referential/department/department.model';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { DepartmentAddTable } from '@app/referential/department/department.add.table';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { GenericAddTable } from '@app/referential/generic/generic.add.table';
import { StrategySelectTable } from '@app/referential/program-strategy/strategy/strategy.select.table';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { StrategyAddTable } from '@app/referential/program-strategy/strategy/strategy.add.table';
import { TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { TranscribingItemRowAddTable } from '@app/referential/transcribing-item/transcribing-item-row.add.table';
import { TranscribingItemRowSelectTable } from '@app/referential/transcribing-item/transcribing-item-row.select.table';

@Injectable()
export class SelectionService implements ISelectionService {
  public getTableComponent(entityName: string, add: boolean): Type<any> {
    if (!entityName) {
      throw new Error('[selection-service] No entityName provided !');
    }
    switch (entityName) {
      case TranscribingItemRow.entityName:
        return add ? TranscribingItemRowAddTable : TranscribingItemRowSelectTable;
      case User.entityName:
        return add ? UserAddTable : UserSelectTable;
      case Department.entityName:
        return add ? DepartmentAddTable : DepartmentSelectTable;
      case MetaProgram.entityName:
        return add ? MetaProgramAddTable : MetaProgramSelectTable;
      case Program.entityName:
        return add ? ProgramAddTable : ProgramSelectTable;
      case Strategy.entityName:
        return add ? StrategyAddTable : StrategySelectTable;
      case MonitoringLocation.entityName:
        return add ? MonitoringLocationAddTable : MonitoringLocationSelectTable;
      case OrderItem.entityName:
        return add ? OrderItemAddTable : OrderItemSelectTable;
      case ParameterGroup.entityName:
        return add ? ParameterGroupAddTable : ParameterGroupSelectTable;
      case Parameter.entityName:
        return add ? ParameterAddTable : ParameterSelectTable;
      case Matrix.entityName:
        return add ? MatrixAddTable : MatrixSelectTable;
      case Fraction.entityName:
        return add ? FractionAddTable : FractionSelectTable;
      case Method.entityName:
        return add ? MethodAddTable : MethodSelectTable;
      case Unit.entityName:
        return add ? UnitAddTable : UnitSelectTable;
      case Pmfmu.entityName:
        return add ? PmfmuAddTable : PmfmuSelectTable;
      case TaxonGroup.entityName:
        return add ? TaxonGroupAddTable : TaxonGroupSelectTable;
      case TaxonName.entityName:
        return add ? TaxonNameAddTable : TaxonNameSelectTable;
      case Campaign.entityName:
        return add ? CampaignAddTable : CampaignSelectTable;
      case Event.entityName:
        return add ? EventAddTable : EventSelectTable;
      case BatchRef.entityName:
        return add ? BatchRefAddTable : BatchRefSelectTable;
      default:
        return add ? GenericAddTable : GenericSelectTable;
    }
  }
}
