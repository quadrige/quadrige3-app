import { inject, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { AUTH_GUARD } from '@app/core/service/auth-guard.service';
import { AccountPage, HomePage } from '@sumaris-net/ngx-components';
import { managementRoutes, referentialRoutes } from '@app/referential/referential.routing.module';
import { ReferentialModule } from '@app/referential/referential.module';

const routes: Routes = [
  // Core path
  {
    path: '',
    component: HomePage,
  },

  {
    path: 'home/:action',
    component: HomePage,
  },
  {
    path: 'account',
    pathMatch: 'full',
    component: AccountPage,
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
  },

  // Admin
  {
    path: 'admin',
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
    loadChildren: () => import('./admin/admin-routing.module').then((m) => m.AdminRoutingModule),
  },

  // Management
  {
    path: 'management',
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
    children: managementRoutes,
  },

  // Referential
  {
    path: 'referential',
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
    children: referentialRoutes,
  },

  // Extraction
  {
    path: 'extraction',
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
    loadChildren: () => import('./extraction/extraction.routing.module').then((m) => m.ExtractionRoutingModule),
  },

  // Social
  {
    path: 'social',
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
    loadChildren: () => import('./social/social.routing.module').then((m) => m.SocialRoutingModule),
  },

  // Test module (disable in menu, by default - can be enabled by the Pod configuration page)
  {
    path: 'testing',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'shared',
      },
      // Shared module
      {
        path: 'shared',
        loadChildren: () => import('./testing/quadrige-shared-testing.module').then((m) => m.QuadrigeSharedTestingModule),
      },
      // Table
      {
        path: 'table',
        loadChildren: () => import('./referential/table/testing/table.testing.module').then((m) => m.TableTestingModule),
      },
      // Color
      {
        path: 'color',
        loadChildren: () => import('./testing/color/color.testing.module').then((m) => m.ColorTestingModule),
      },
      // Tooltips
      {
        path: 'tooltip',
        loadChildren: () => import('./testing/tooltip/tooltip.testing.module').then((m) => m.TooltipTestingModule),
      },
      // Filters
      {
        path: 'filter',
        loadChildren: () => import('@app/referential/component/testing/filter.testing.module').then((m) => m.FilterTestingModule),
      },
    ],
  },

  // Other route redirection (should at the end of the array)
  {
    path: '**',
    redirectTo: '/',
  },
];

@NgModule({
  imports: [
    ReferentialModule,
    RouterModule.forRoot(routes, {
      enableTracing: false, //!environment.production,
      useHash: false,
      onSameUrlNavigation: 'reload',
      // preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
