import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { ExtractFilterValidator } from '@app/extraction/extract-filter.validator';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { ExtractFilter } from '@app/extraction/extract-filter.model';
import { ENVIRONMENT, isNotNilOrBlank, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS, toBoolean } from '@sumaris-net/ngx-components';
import { ExtractFilterFilter, ExtractFilterFilterCriteria } from '@app/extraction/filter/extract-filter.filter.model';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { BaseTableDatasource } from '@app/shared/table/base.datasource';

@Component({
  selector: 'app-extract-filter-add-table',
  templateUrl: './extract-filter.add.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractFilterAddTable
  extends ReferentialTable<ExtractFilter, number, ExtractFilterFilter, ExtractFilterFilterCriteria, ExtractFilterValidator>
  implements OnInit, AfterViewInit
{
  @Input() service: ExtractFilterService;
  @Input() addFirstCriteria: ISelectCriteria;

  constructor(protected injector: Injector) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description']).concat(RESERVED_END_COLUMNS),
      ExtractFilter,
      null, // Entities service not injected here
      null
    );

    this.i18nColumnPrefix = 'EXTRACTION.EXTRACT_FILTER.';
    this.logPrefix = '[extract-filter-add-table]';
    this.setShowColumn('statusId', false);
  }

  ngOnInit() {
    // Create datasource
    if (!this.service) throw new Error('Missing service');
    this.validatorService = new ExtractFilterValidator(this.injector, this.service);
    this.setDatasource(
      new BaseTableDatasource<ExtractFilter, number, ExtractFilterFilter>(ExtractFilter, this.service, this.validatorService, {
        prependNewElements: false,
        saveOnlyDirtyRows: true,
        restoreOriginalDataOnCancel: true,
        suppressErrors: toBoolean(this.injector.get(ENVIRONMENT)?.production, false),
        onRowCreated: (row) => this.onDefaultRowCreated(row),
        onError: (error) => this.handleError(error),
      })
    );
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };

    // default properties
    this.addTable = true;
    // Override selection model
    this.multipleSelection = false;

    super.ngOnInit();

    // Auto select template on add if we previously select a template item on main table
    if (isNotNilOrBlank(this.addFirstCriteria?.searchText)) {
      const searchText = this.addFirstCriteria?.searchText;
      this.waitIdle().then(() => {
        this.selectRowByData(ExtractFilter.fromObject({ name: searchText })).then(() => this.markForCheck());
      });
    }

    this.autoSaveFilter = true;
  }

  protected getFilterEntityName(): string {
    return `${this.entityName}Template`;
  }

  protected getFilterSettingId(): string {
    return `${this.entityName}Template`;
  }

  async resetFilter(filter?: ExtractFilterFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, <ISetFilterOptions<ExtractFilterFilterCriteria>>{
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: { template: true }, // On add table, items are necessarily templates
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'template', 'description');
  }
}
