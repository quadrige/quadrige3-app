import { ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';
import { ExtractFilterAddTable } from '@app/extraction/add/extract-filter.add.table';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';
import { IExtractFilterFilterCriteria } from '@app/extraction/filter/extract-filter.filter.model';

export interface IExtractFilterAppOptions extends IModalOptions {
  service: ExtractFilterService;
  addFirstCriteria: IExtractFilterFilterCriteria;
  canValidateWithoutTemplate: boolean;
}

export interface IExtractFilterAddResult {
  extractFilterId: number;
}

@Component({
  selector: 'app-extract-filter-add-modal',
  templateUrl: './extract-filter.add.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractFilterAddModal extends ModalComponent<IExtractFilterAddResult> implements IExtractFilterAppOptions {
  @ViewChild('addTable') addTable: ExtractFilterAddTable;

  @Input() service: ExtractFilterService;
  @Input() addFirstCriteria: IExtractFilterFilterCriteria;
  @Input() canValidateWithoutTemplate: boolean;

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled(): boolean {
    return super.disabled || (!this.addTable?.selectedEntityIds.length && !this.canValidateWithoutTemplate);
  }

  protected async afterInit() {
    await this.addTable.ready();
    await this.addTable.resetFilter(undefined, { emitEvent: true, firstTime: true });
  }

  protected dataToValidate(): IExtractFilterAddResult {
    return {
      extractFilterId: this.addTable.selectedEntityIds[0],
    };
  }
}
