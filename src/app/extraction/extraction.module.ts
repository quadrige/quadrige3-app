import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { ResultHomePage } from '@app/extraction/menu/result-home.page';
import { OrderItemModule } from '@app/referential/order-item/order-item.module';
import { ExtractFilterAddTable } from '@app/extraction/add/extract-filter.add.table';
import { ExtractFilterAddModal } from '@app/extraction/add/extract-filter.add.modal';
import { ExtractSurveyPeriodForm } from '@app/extraction/period/extract-survey-period.form';
import { ExtractCriteriaForm, MatExpansionPanelFilterTypeDirective } from '@app/extraction/criteria/extract-criteria.form';
import { ExtractCriteriaSurveyComponent } from '@app/extraction/criteria/survey/extract-criteria-survey.component';
import { ExtractCriteriaSamplingOperationComponent } from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.component';
import { ExtractCriteriaSampleComponent } from '@app/extraction/criteria/sample/extract-criteria-sample.component';
import { ExtractCriteriaMeasurementComponent } from '@app/extraction/criteria/measurement/extract-criteria-measurement.component';
import { ExtractCriteriaPhotoComponent } from '@app/extraction/criteria/photo/extract-criteria-photo.component';
import { ExtractCriteriaQualityFlagComponent } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.component';
import { ExtractCriteriaAcquisitionLevelComponent } from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.component';
import { ExtractCriteriaMeasurementTypeComponent } from '@app/extraction/criteria/measurement-type/extract-criteria-measurement-type.component';
import { ExtractCriteriaPhotoResolutionComponent } from '@app/extraction/criteria/photo-resolution/extract-criteria-photo-resolution.component';
import { ExtractCriteriaAddModal } from '@app/extraction/criteria/action/add/extract-criteria.add.modal';
import { ExtractCriteriaAddTable } from '@app/extraction/criteria/action/add/extract-criteria.add.table';
import { ExtractCriteriaRemoveComponent } from '@app/extraction/criteria/action/remove/extract-criteria.remove.component';
import { ExtractOutputForm } from '@app/extraction/output/extract-output.form';
import { ExtractFieldTable } from '@app/extraction/output/fields/extract-field.table';
import { ExtractFieldAddModal } from '@app/extraction/output/fields/add/extract-field.add.modal';
import { ExtractFieldAddTable } from '@app/extraction/output/fields/add/extract-field.add.table';
import { ExtractMapForm } from '@app/extraction/map/extract-map.form';
import { ExtractTemplateModal } from '@app/extraction/template/extract-template.modal';
import { ExtractFilterTable } from '@app/extraction/extract-filter.table';
import { UserModule } from '@app/referential/user/user.module';
import { DepartmentModule } from '@app/referential/department/department.module';
import { ExtractCriteriaCampaignComponent } from '@app/extraction/criteria/campaign/extract-criteria-campaign.component';
import { ExtractCriteriaOccasionComponent } from '@app/extraction/criteria/occasion/extract-criteria-occasion.component';
import { ExtractCriteriaEventComponent } from '@app/extraction/criteria/event/extract-criteria-event.component';
import { ExtractFilterFilterForm } from '@app/extraction/filter/extract-filter.filter.form';
import { ExtractCriteriaAddFilterForm } from '@app/extraction/criteria/action/add/filter/extract-criteria.add.filter.form';
import { ExtractFieldAddFilterForm } from '@app/extraction/output/fields/add/filter/extract-field.add.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { DataHomePage } from '@app/extraction/menu/data-home.page';
import { OtherHomePage } from '@app/extraction/menu/other-home.page';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { SocialComponent } from '@app/social/social.component';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';
import { ReferentialFilterHintComponent } from '@app/referential/component/referential-filter-hint.component';

const components = [
  ExtractFilterTable,
  ExtractFilterAddTable,
  ExtractFilterAddModal,
  ExtractFilterFilterForm,
  ExtractTemplateModal,
  ExtractSurveyPeriodForm,
  ExtractCriteriaForm,
  ExtractCriteriaSurveyComponent,
  ExtractCriteriaSamplingOperationComponent,
  ExtractCriteriaSampleComponent,
  ExtractCriteriaMeasurementComponent,
  ExtractCriteriaPhotoComponent,
  ExtractCriteriaCampaignComponent,
  ExtractCriteriaOccasionComponent,
  ExtractCriteriaEventComponent,
  ExtractCriteriaQualityFlagComponent,
  ExtractCriteriaAcquisitionLevelComponent,
  ExtractCriteriaMeasurementTypeComponent,
  ExtractCriteriaPhotoResolutionComponent,
  ExtractCriteriaAddModal,
  ExtractCriteriaAddTable,
  ExtractCriteriaAddFilterForm,
  ExtractCriteriaRemoveComponent,
  MatExpansionPanelFilterTypeDirective,
  ExtractOutputForm,
  ExtractFieldTable,
  ExtractFieldAddModal,
  ExtractFieldAddTable,
  ExtractFieldAddFilterForm,
  ExtractMapForm,
];

@NgModule({
  imports: [
    ReferentialModule,
    UserModule,
    DepartmentModule,
    OrderItemModule,
    GenericSelectTable,
    DepartmentSelectTable,
    UserSelectTable,
    SocialComponent,
    ReferentialFilterFormField,
    ReferentialFilterHintComponent,
  ],
  declarations: [ResultHomePage, DataHomePage, OtherHomePage, ...components],
  exports: [ReferentialModule, ...components],
})
export class ExtractionModule {}
