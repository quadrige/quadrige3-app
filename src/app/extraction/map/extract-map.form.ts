import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { arraySize, FileEvent, FileResponse, FilesUtils, isNotEmptyArray, isNotNil, StatusIds } from '@sumaris-net/ngx-components';
import { catchError, filter, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AlertController, PopoverController } from '@ionic/angular';
import { IExtractMap } from '@app/extraction/map/extract-map.model';
import { MapComponent } from '@app/shared/component/map/map.component';
import { FileUtils } from '@app/shared/files';
import { HttpEventType } from '@angular/common/http';
import { FeatureCollectionWithFilename, parseZip } from 'shpjs';
import { Feature, FeatureCollection } from 'geojson';
import { Geometries } from '@app/shared/geometries';
import { OrderItem, OrderItemUtils } from '@app/referential/order-item/order-item.model';
import { Alerts } from '@app/shared/alerts';
import { OrderItemService } from '@app/referential/order-item/order-item.service';
import { entityId } from '@app/shared/entity.utils';
import { BaseForm } from '@app/shared/form/base.form';
import { IAddResult, ISelectModalOptions } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { ExtractFilterGeometrySourceType } from '@app/extraction/extract-filter.model';
import { IOrderItemFilterCriteria } from '@app/referential/order-item/filter/order-item.filter.model';

@Component({
  selector: 'app-extract-map-form',
  templateUrl: './extract-map.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractMapForm extends BaseForm<any> implements AfterViewInit {
  @Input() i18nPrefix: string;

  @ViewChild('map') mapComponent: MapComponent;

  geometrySources: ExtractFilterGeometrySourceType[] = ['ORDER_ITEM', 'BUILT_IN', 'SHAPEFILE'];

  protected geometrySource: ExtractFilterGeometrySourceType;
  protected orderItems: OrderItem[] = [];
  protected features: FeatureCollection;

  constructor(
    protected injector: Injector,
    protected orderItemService: OrderItemService,
    protected popoverController: PopoverController,
    protected alertController: AlertController
  ) {
    super(injector);
    this.setForm(this.formBuilder.group({}));
    this.logPrefix = '[extract-map]';
  }

  get featureExists(): boolean {
    switch (this.geometrySource) {
      case 'ORDER_ITEM':
        return isNotEmptyArray(this.orderItems);
      case 'SHAPEFILE':
        return isNotEmptyArray(this.features?.features);
      case 'BUILT_IN':
        return isNotEmptyArray(this.features?.features) || this.mapComponent.hasDrawnFeatures();
    }
  }

  async clear() {
    await this.setData(undefined);
    this.markAsDirty();
  }

  refreshComponents() {
    this.mapComponent.containerResize();
  }

  async setData(data: IExtractMap) {
    // Set data
    this.geometrySource = data?.geometrySource;
    this.features = data?.features;
    this.orderItems = [];

    if (isNotEmptyArray(data?.orderItemIds)) {
      // Load OrderItems and their geometries
      await this.loadOrderItems(data.orderItemIds);
    } else if (Geometries.isFeatureCollectionNotEmpty(data?.features)) {
      // Load feature collection directly
      await this.loadMap(data.features);
    } else {
      // Nothing to load
      await this.loadMap(undefined);
    }
    this.markAsPristine();
  }

  getData(): IExtractMap {
    const withOrderItems = this.geometrySource === 'ORDER_ITEM' && isNotEmptyArray(this.orderItems);
    const withShapefile = this.geometrySource === 'SHAPEFILE' && isNotEmptyArray(this.features?.features);
    const withBuiltIn = this.geometrySource === 'BUILT_IN' && this.mapComponent.hasDrawnFeatures();
    if (!withOrderItems && !withShapefile && !withBuiltIn) {
      return {
        geometrySource: undefined,
        orderItemIds: undefined,
        features: undefined,
      };
    }
    return {
      geometrySource: this.geometrySource,
      orderItemIds: withOrderItems ? this.orderItems.map(entityId) : undefined,
      features: withShapefile ? this.features : withBuiltIn ? this.mapComponent.getDrawnLayers() : undefined,
    };
  }

  async add(event: MouseEvent, geometrySource: ExtractFilterGeometrySourceType) {
    event?.preventDefault();
    this.geometrySource = geometrySource;

    switch (geometrySource) {
      case 'BUILT_IN': {
        this.mapComponent.enableDraw();
        break;
      }
      case 'ORDER_ITEM': {
        this.mapComponent.disableDraw();

        // Open modal
        const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
          EntityAddModal,
          {
            entityName: OrderItem.entityName,
            titleI18n: 'EXTRACTION.EXTRACT_FILTER.MAP.ADD_ORDER_ITEMS',
            addCriteria: <IOrderItemFilterCriteria>{
              excludedIds: this.orderItems.map(entityId),
              statusId: StatusIds.ENABLE,
              orderItemTypeFilter: {
                statusId: StatusIds.ENABLE,
              },
            },
          },
          'modal-large'
        );

        if (role === 'validate' && isNotEmptyArray(data?.added)) {
          this.orderItems.push(...data.added.map(OrderItem.fromObject));
          // Load geometries in map
          await this.loadOrderItemGeometries();
          this.markAsDirty();
        }

        break;
      }
      case 'SHAPEFILE': {
        this.mapComponent.disableDraw();

        const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
          title: '',
          fileExtension: '.zip',
          uniqueFile: true,
          uploadFn: (file: File) => this.uploadShapefile(file),
        });
        if (this.debug) console.debug(`${this.logPrefix} shapefile upload result:`, data);

        if (isNotEmptyArray(data)) {
          // Parse geometries: take only area features
          const featureCollections: FeatureCollection[] = data[0].response.body;
          const validFeatures: Feature[] = [];
          featureCollections.forEach((featureCollection) => {
            featureCollection.features.forEach((feature) => {
              if (Geometries.isAreaGeometry(feature.geometry)) {
                // Add properties
                // feature.properties = {
                //   ...feature.properties,
                //   name: this.translate.instant('EXTRACTION.EXTRACT_FILTER.MAP.FROM_SHAPEFILE', { name: data[0].name }),
                // };
                validFeatures.push(feature);
              }
            });
          });

          if (isNotEmptyArray(validFeatures)) {
            // Send features to map
            this.features = Geometries.toCollection(validFeatures);
            await this.mapComponent.loadGeoData(validFeatures);
            this.markAsDirty();
          } else {
            await Alerts.showError('EXTRACTION.EXTRACT_FILTER.MAP.ERROR.INVALID_SHAPEFILE_CONTENT', this.alertController, this.translate);
          }
        }
        break;
      }
    }
  }

  // Make setError public
  setError(value: string, opts?: { emitEvent?: boolean }) {
    super.setError(value, opts);
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.disable(opts);
  }

  private uploadShapefile(file: File): Observable<FileEvent<FeatureCollectionWithFilename[]>> {
    console.info(`${this.logPrefix} Importing Shapefile ${file.name}`);

    return FileUtils.readAsArrayBuffer(file).pipe(
      mergeMap(async (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          const loaded = Math.round(event.loaded * 0.8);
          return { ...event, loaded };
        } else if (event instanceof FileResponse) {
          const parsed = await parseZip(event.body);
          if (Array.isArray(parsed)) {
            return new FileResponse<FeatureCollectionWithFilename[]>({ body: parsed });
          }
          return new FileResponse<FeatureCollectionWithFilename[]>({ body: [parsed] });
        } else {
          // Unknown event: skip
          return null;
        }
      }),
      filter(isNotNil),
      catchError((err) => {
        console.error(`${this.logPrefix} Error while parsing shapefile`, err);
        throw new Error(this.translate.instant('EXTRACTION.EXTRACT_FILTER.MAP.ERROR.INVALID_SHAPEFILE'));
      })
    );
  }

  private async loadOrderItems(ids: number[]) {
    this.markAsLoading();
    // Load missing OrderItems
    const loadedOrderItemIds = this.orderItems.map(entityId);
    const idsToLoad = ids.filter((id) => !loadedOrderItemIds.includes(id));
    if (isNotEmptyArray(idsToLoad)) {
      const orderItems = await this.orderItemService.loadAllByCriteria({ includedIds: idsToLoad });
      this.orderItems.push(...orderItems);
    }
    // Then load geometries
    await this.loadOrderItemGeometries();
  }

  private async loadOrderItemGeometries() {
    this.markAsLoading();
    // Load missing Geometries
    const geometryIdsToLoad = this.orderItems.filter((value) => !value.geometryLoaded).map(entityId);
    if (isNotEmptyArray(geometryIdsToLoad)) {
      const geometries = await this.orderItemService.loadGeometryFeatures(geometryIdsToLoad);
      OrderItemUtils.updateOrderItemGeometries(this.orderItems, geometries);
      if (arraySize(Object.keys(geometries)) < arraySize(geometryIdsToLoad)) {
        this.removeOrderItemsWithoutGeometry();
        await Alerts.showError('EXTRACTION.EXTRACT_FILTER.MAP.ERROR.INVALID_ORDER_ITEM', this.alertController, this.translate);
      }
    }
    // Load Map
    await this.loadMap(this.orderItems.map((value) => value.geometry));
  }

  private async loadMap(data: Feature[] | FeatureCollection | undefined) {
    this.markAsLoading();
    await this.mapComponent.loadGeoData(data, this.geometrySource === 'BUILT_IN');
    this.markAsLoaded();
  }

  private removeOrderItemsWithoutGeometry() {
    this.orderItems = this.orderItems.filter((value) => !value.geometryLoaded);
  }
}
