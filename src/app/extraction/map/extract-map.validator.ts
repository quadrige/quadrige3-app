import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { FormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractMap, IExtractMap } from '@app/extraction/map/extract-map.model';

/**
 * fixme: not used
 */
@Injectable({ providedIn: 'root' })
export class ExtractMapValidator extends ReferentialValidatorService<ExtractMap> {
  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractMap, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return <Record<keyof IExtractMap, any>>{
      orderItemIds: [data?.orderItemIds || null],
      features: [data?.features || null],
    };
  }
}
