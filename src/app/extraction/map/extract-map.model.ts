import { Entity, EntityAsObjectOptions } from '@sumaris-net/ngx-components';
import { FeatureCollection } from 'geojson';
import { Geometries } from '@app/shared/geometries';
import { ExtractFilterGeometrySourceType } from '@app/extraction/extract-filter.model';

export interface IExtractMapAddMenuItem {
  geometrySource: ExtractFilterGeometrySourceType;
  enabled: boolean;
}

export interface IExtractMap {
  geometrySource: ExtractFilterGeometrySourceType;
  orderItemIds: number[];
  features: FeatureCollection;
}

export class ExtractMap extends Entity<ExtractMap> implements IExtractMap {
  geometrySource: ExtractFilterGeometrySourceType;
  orderItemIds: number[] = [];
  features: FeatureCollection = undefined;

  constructor() {
    super('ExtractMap');
  }

  static fromObject(source: any): ExtractMap {
    const target = new ExtractMap();
    target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.geometrySource = source.geometrySource;
    this.orderItemIds = source.orderItemIds || [];
    this.features = source.features || Geometries.toFeatureCollection(source.geometry);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    return super.asObject(opts) as any;
  }
}
