import { Entity, EntityClass, IEntity, ReferentialAsObjectOptions, toNumber } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';

export type ExtractFieldType =
  | 'MONITORING_LOCATION'
  | 'SURVEY'
  | 'FIELD_OBSERVATION'
  | 'SAMPLING_OPERATION'
  | 'SAMPLE'
  | 'MEASUREMENT'
  | 'PHOTO'
  | 'CAMPAIGN'
  | 'OCCASION'
  | 'EVENT';

export const orderedExtractFieldTypes: ExtractFieldType[] = [
  'MONITORING_LOCATION',
  'SURVEY',
  'SAMPLING_OPERATION',
  'SAMPLE',
  'MEASUREMENT',
  'PHOTO',
  'FIELD_OBSERVATION',
  'CAMPAIGN',
  'OCCASION',
  'EVENT',
];

export type ExtractFieldStaticType = 'SANDRE' | 'ID' | 'ALL';
export const extractFieldStaticTypes: ExtractFieldStaticType[] = ['SANDRE', 'ID', 'ALL'];

export type ExtractSortDirection = 'ASC' | 'DESC' | 'NONE';

export interface IExtractField {
  id?: number;
  name: string;
  type: ExtractFieldType;
  rankOrder?: number;
  sortDirection: ExtractSortDirection;
  updateDate: Moment;
}

@EntityClass({ typename: 'ExtractFieldVO' })
export class ExtractField extends Entity<ExtractField> implements IExtractField {
  static fromObject: (source: any, opts?: any) => ExtractField;

  name: string;
  type: ExtractFieldType;
  rankOrder: number;
  sortDirection: ExtractSortDirection;

  constructor(name?: string, type?: ExtractFieldType, sortDirection?: ExtractSortDirection, rankOrder?: number) {
    super(ExtractField.TYPENAME);
    this.name = name;
    this.type = type;
    this.sortDirection = sortDirection;
    this.rankOrder = rankOrder;
  }

  equals(other: ExtractField): boolean {
    return super.equals(other) || this.name === other.name;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.name = source.name;
    this.type = source.type;
    this.rankOrder = toNumber(source.rankOrder);
    this.sortDirection = source.sortDirection || 'NONE';
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.rankOrder = toNumber(this.rankOrder);
    if (opts?.minify) {
      target.sortDirection = this.sortDirection !== 'NONE' ? this.sortDirection : null;
    }
    // todo delete some property ?
    return target;
  }
}

export class ExtractFieldUtils {
  static isOrderItemType(field: IExtractField): boolean {
    return field?.type === 'MONITORING_LOCATION' && field?.name.startsWith('MONITORING_LOCATION_ORDER_ITEM');
  }

  static isUserPersonalDataType(field: IExtractField): boolean {
    return (
      (field?.type === 'SURVEY' && ['SURVEY_OBSERVER_ID', 'SURVEY_OBSERVER_NAME'].includes(field?.name)) ||
      (field?.type === 'OCCASION' && ['OCCASION_PARTICIPANT_ID', 'OCCASION_PARTICIPANT_NAME'].includes(field?.name))
    );
  }
}

export interface IExtractFieldDefinition extends IEntity<IExtractFieldDefinition, string> {
  name: string;
  type: ExtractFieldType;
  rankOrder: number;
}

@EntityClass({ typename: 'ExtractFieldDefinitionVO' })
export class ExtractFieldDefinition extends Entity<ExtractFieldDefinition, string> implements IExtractFieldDefinition {
  static fromObject: (source: any, opts?: any) => ExtractFieldDefinition;

  name: string;
  type: ExtractFieldType;
  rankOrder: number;

  constructor() {
    super(ExtractFieldDefinition.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.name = source.name;
    this.type = source.type;
    this.rankOrder = source.rankOrder;
  }
}
