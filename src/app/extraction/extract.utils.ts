import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { CriteriaDefinitionRecord, ICriteriaDefinition } from '@app/extraction/extract.type';
import { Optional } from 'typescript-optional';
import { IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { FilterCriteriaValue, FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import {
  arraySize,
  fromDateISOString,
  isEmptyArray,
  isNilOrBlank,
  isNilOrNaN,
  isNotEmptyArray,
  isNotNilOrBlank,
  isNotNilOrNaN,
} from '@sumaris-net/ngx-components';
import { FilterUtils } from '@app/referential/filter/model/filter.utils';
import { INumberFilterValue, NumberFilterValue } from '@app/referential/component/number-filter-form-field.component';
import { Utils } from '@app/shared/utils';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { ITimeFilterValue, TimeFilterValue } from '@app/referential/component/time-filter-form-field.component';
import { Times } from '@app/shared/times';
import { DateFilterValue, IDateFilterValue } from '@app/referential/component/date-filter-form-field.component';
import { Dates } from '@app/shared/dates';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

// todo maybe move to FilterUtils
export class ExtractUtils {
  static getReferentialFilterCriteria<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I
  ): IReferentialFilterCriteria<any> {
    const referentialFilterCriteria: IReferentialFilterCriteria<any> = {};
    Optional.ofNullable(ExtractUtils.getValues(criterias, criteriaDefinitionRecord, property))
      .filter((values) => isNotEmptyArray(values.values))
      .ifPresent((values) => {
        if (values.inverse) referentialFilterCriteria.excludedIds = values.values;
        else referentialFilterCriteria.includedIds = values.values;
        referentialFilterCriteria.systemId = values.systemId;
      });
    Optional.ofNullable(ExtractUtils.getValueOptional(criterias, criteriaDefinitionRecord, property, true))
      .filter((value) => isNotNilOrBlank(value.value))
      .ifPresent((value) => {
        referentialFilterCriteria.searchText = value.value;
        referentialFilterCriteria.systemId = value.systemId;
      });
    return referentialFilterCriteria;
  }

  static setReferentialFilterCriteria<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    referentialFilterCriteria: IReferentialFilterCriteria<any>
  ) {
    if (!BaseFilterUtils.isCriteriaEmpty(referentialFilterCriteria, true)) {
      // Set values
      const inverse = isNotEmptyArray(referentialFilterCriteria.excludedIds);
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, {
        values: inverse ? referentialFilterCriteria.excludedIds : referentialFilterCriteria.includedIds,
        inverse,
        systemId: referentialFilterCriteria.systemId,
      });
      ExtractUtils.setValueOptional(criterias, criteriaDefinitionRecord, property, true, {
        value: referentialFilterCriteria.searchText,
        systemId: referentialFilterCriteria.systemId,
      });
    } else {
      // Remove values
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, null);
      ExtractUtils.setValueOptional(criterias, criteriaDefinitionRecord, property, true, null);
    }
  }

  static getNumberFilter<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I
  ): INumberFilterValue {
    const numberFilterValue: INumberFilterValue = NumberFilterValue.default();
    Optional.ofNullable(ExtractUtils.getValues(criterias, criteriaDefinitionRecord, property))
      .filter((values) => isNotEmptyArray(values.values))
      .ifPresent((values) => {
        numberFilterValue.operator = values.operator;
        numberFilterValue.value = values.values?.[0];
        numberFilterValue.value2 = values.values?.[1];
      });
    return numberFilterValue;
  }

  static setNumberFilter<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    numberFilterValue: INumberFilterValue
  ) {
    if (isNotNilOrNaN(numberFilterValue?.value)) {
      // Set values
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, {
        values: isNilOrNaN(numberFilterValue.value2) ? [numberFilterValue.value] : [numberFilterValue.value, numberFilterValue.value2],
        operator: numberFilterValue.operator,
      });
    } else {
      // Remove values
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, null);
    }
  }

  static getTimeFilter<I>(criterias: FilterCriteria[], criteriaDefinitionRecord: CriteriaDefinitionRecord<I>, property: keyof I): ITimeFilterValue {
    const timeFilterValue: ITimeFilterValue = TimeFilterValue.default();
    Optional.ofNullable(ExtractUtils.getValues(criterias, criteriaDefinitionRecord, property))
      .filter((values) => isNotEmptyArray(values.values))
      .ifPresent((values) => {
        timeFilterValue.operator = values.operator;
        timeFilterValue.value = Times.secondsToString(values.values?.[0]);
        timeFilterValue.value2 = Times.secondsToString(values.values?.[1]);
      });
    return timeFilterValue;
  }

  static setTimeFilter<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    timeFilterValue: ITimeFilterValue
  ) {
    if (isNotNilOrBlank(timeFilterValue?.value)) {
      // Set values
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, {
        values: isNilOrBlank(timeFilterValue.value2)
          ? [Times.stringToSeconds(timeFilterValue.value)]
          : [Times.stringToSeconds(timeFilterValue.value), Times.stringToSeconds(timeFilterValue.value2)],
        operator: timeFilterValue.operator,
      });
    } else {
      // Remove values
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, null);
    }
  }

  static getDateFilter<I>(criterias: FilterCriteria[], criteriaDefinitionRecord: CriteriaDefinitionRecord<I>, property: keyof I): IDateFilterValue {
    const dateFilterValue: IDateFilterValue = DateFilterValue.default();
    Optional.ofNullable(ExtractUtils.getValues(criterias, criteriaDefinitionRecord, property))
      .filter((values) => isNotEmptyArray(values.values))
      .ifPresent((values) => {
        dateFilterValue.operator = values.operator;
        dateFilterValue.value = fromDateISOString(values.values?.[0]);
        dateFilterValue.value2 = fromDateISOString(values.values?.[1]);
      });
    return dateFilterValue;
  }

  static setDateFilter<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    dateFilterValue: IDateFilterValue
  ) {
    if (isNotNilOrBlank(dateFilterValue?.value)) {
      // Set values
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, {
        values: isNilOrBlank(dateFilterValue.value2)
          ? [Dates.toLocalDateString(dateFilterValue.value)]
          : [Dates.toLocalDateString(dateFilterValue.value), Dates.toLocalDateString(dateFilterValue.value2)],
        operator: dateFilterValue.operator,
      });
    } else {
      // Remove values
      ExtractUtils.setValues(criterias, criteriaDefinitionRecord, property, null);
    }
  }

  static getTextFilter<I>(criterias: FilterCriteria[], criteriaDefinitionRecord: CriteriaDefinitionRecord<I>, property: keyof I): ITextFilterValue {
    const textFilterValue: ITextFilterValue = TextFilterValue.default(criteriaDefinitionRecord[property].valueOperatorType);
    Optional.ofNullable(ExtractUtils.getValue(criterias, criteriaDefinitionRecord, property))
      .filter((value) => isNotNilOrBlank(value.value))
      .ifPresent((value) => {
        textFilterValue.operator = value.operator;
        textFilterValue.value = value.value;
      });
    return textFilterValue;
  }

  static setTextFilter<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    textFilterValue: ITextFilterValue
  ) {
    if (isNotNilOrBlank(textFilterValue?.value)) {
      // Set values
      ExtractUtils.setValue(criterias, criteriaDefinitionRecord, property, {
        value: textFilterValue.value,
        operator: textFilterValue.operator,
      });
    } else {
      // Remove values
      ExtractUtils.setValue(criterias, criteriaDefinitionRecord, property, null);
    }
  }

  static getValue<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    defaultValue?: any
  ): FilterValue {
    return ExtractUtils.getValueOptional(criterias, criteriaDefinitionRecord, property, false, defaultValue);
  }

  static setValue<I>(criterias: FilterCriteria[], criteriaDefinitionRecord: CriteriaDefinitionRecord<I>, property: keyof I, newValue: FilterValue) {
    ExtractUtils.setValueOptional(criterias, criteriaDefinitionRecord, property, false, newValue);
  }

  static getValues<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    defaultValues: any[] = []
  ): FilterValues {
    const criteriaDefinition: ICriteriaDefinition = criteriaDefinitionRecord[property];
    if (!criteriaDefinition) {
      console.error(`ExtractUtils.getValues: no criteriaDefinition found for ${property.toString()}`, criterias);
      ExtractUtils.throw();
    }
    if (!criteriaDefinition.valuesCriteriaType) {
      console.error(`ExtractUtils.getValues: no valuesCriteriaType found for criteriaOperator`, criteriaDefinition);
      ExtractUtils.throw();
    }
    return Optional.of(criteriaDefinition.valuesCriteriaType)
      .map((criteriaType) => criterias.find((value) => value.filterCriteriaType === criteriaType))
      .map((criteria) => FilterUtils.getValues(criteria))
      .orElse({ values: defaultValues });
  }

  static setValues<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    newValues: FilterValues
  ) {
    const criteriaDefinition: ICriteriaDefinition = criteriaDefinitionRecord[property];
    if (!criteriaDefinition) {
      console.error(`ExtractUtils.setValues: no criteriaDefinition found for ${property.toString()}`, criterias);
      ExtractUtils.throw();
    }
    if (!criteriaDefinition.valuesCriteriaType) {
      console.error(`ExtractUtils.setValues: no valuesCriteriaType found for criteriaOperator`, criteriaDefinition);
      ExtractUtils.throw();
    }
    if (isNotEmptyArray(newValues?.values)) {
      // Create or update criteria
      let criteria = criterias.find((value) => value.filterCriteriaType === criteriaDefinition.valuesCriteriaType);
      if (!criteria) {
        criteria = new FilterCriteria();
        criteria.filterCriteriaType = criteriaDefinition.valuesCriteriaType;
        criterias.push(criteria);
      }
      // Always update the operator type
      criteria.filterOperatorType = newValues.operator || criteriaDefinition.valuesOperatorType || 'TEXT_EQUAL';
      criteria.inverse = newValues.inverse;
      criteria.systemId = newValues.systemId;

      // Merge values
      if (isEmptyArray(criteria.values)) criteria.values = [];
      newValues.values.forEach((value, index) => {
        let criteriaValue = criteria.values[index];
        if (!criteriaValue) {
          criteriaValue = new FilterCriteriaValue();
          criteria.values.push(criteriaValue);
        }
        criteriaValue.value = value;
      });
      // Remove remaining criteria values
      Utils.trimArray(criteria.values, newValues.values.length);
    } else {
      // Remove existing criteria
      const criteriaIndex = criterias.findIndex((value) => value.filterCriteriaType === criteriaDefinition.valuesCriteriaType);
      if (criteriaIndex !== -1) {
        criterias.splice(criteriaIndex, 1);
      }
    }
  }

  private static getValueOptional<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    skipMissingCriteriaType: boolean,
    defaultValue?: any
  ): FilterValue {
    const criteriaDefinition: ICriteriaDefinition = criteriaDefinitionRecord[property];
    if (!criteriaDefinition) {
      console.error(`ExtractUtils.getValue: no criteriaDefinition found for ${property.toString()}`, criterias);
      ExtractUtils.throw();
    }
    if (!criteriaDefinition.valueCriteriaType) {
      if (skipMissingCriteriaType) {
        return undefined;
      } else {
        console.error(`ExtractUtils.getValue: no valueCriteriaType found for criteriaOperator`, criteriaDefinition);
        ExtractUtils.throw();
      }
    }
    return Optional.of(criteriaDefinition.valueCriteriaType)
      .map((criteriaType) => criterias.find((value) => value.filterCriteriaType === criteriaType))
      .map((criteria) => FilterUtils.getSingleValue(criteria))
      .orElse({ value: defaultValue });
  }

  private static setValueOptional<I>(
    criterias: FilterCriteria[],
    criteriaDefinitionRecord: CriteriaDefinitionRecord<I>,
    property: keyof I,
    skipMissingCriteriaType: boolean,
    newValue: FilterValue
  ) {
    const criteriaDefinition: ICriteriaDefinition = criteriaDefinitionRecord[property];
    if (!criteriaDefinition) {
      console.error(`ExtractUtils.setValue: no criteriaDefinition found for ${property.toString()}`, criterias);
      ExtractUtils.throw();
    }
    if (!criteriaDefinition.valueCriteriaType) {
      if (!skipMissingCriteriaType) {
        console.error(`ExtractUtils.setValue: no valueCriteriaType found for criteriaOperator`, criteriaDefinition);
        ExtractUtils.throw();
      }
      return;
    }
    if (isNotNilOrBlank(newValue?.value)) {
      // Create or update criteria
      let criteria = criterias.find((value) => value.filterCriteriaType === criteriaDefinition.valueCriteriaType);
      if (!criteria) {
        criteria = new FilterCriteria();
        criteria.filterCriteriaType = criteriaDefinition.valueCriteriaType;
        criterias.push(criteria);
      }
      // Always update the operator type
      criteria.filterOperatorType = newValue.operator || criteriaDefinition.valueOperatorType || 'TEXT_EQUAL';
      if (arraySize(criteria.values) !== 1) {
        criteria.values = [new FilterCriteriaValue()];
      }
      criteria.values[0].value = newValue.value;
      criteria.systemId = newValue.systemId;
    } else {
      // Remove existing criteria
      const criteriaIndex = criterias.findIndex((value) => value.filterCriteriaType === criteriaDefinition.valueCriteriaType);
      if (criteriaIndex !== -1) {
        criterias.splice(criteriaIndex, 1);
      }
    }
  }

  private static throw() {
    throw 'ExtractUtils error';
  }
}
