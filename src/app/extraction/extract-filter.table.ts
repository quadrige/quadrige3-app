import { AfterViewInit, ChangeDetectionStrategy, Component, HostListener, Injector, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import {
  arraySize,
  ErrorCodes,
  FileEvent,
  FileResponse,
  FilesUtils,
  isEmptyArray,
  isNil,
  isNotEmptyArray,
  isNotNil,
  isNotNilOrBlank,
  PlatformService,
  PromiseEvent,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  toBoolean,
  toNumber,
  waitWhilePending,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { EntityMenuComponent, IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { ExtractFilter, ExtractFilterUtils, IExtractFilter, IMergeCriteriaOptions, IVisibleFilter } from '@app/extraction/extract-filter.model';
import { ExtractFilterValidator } from '@app/extraction/extract-filter.validator';
import { ExtractCriteriaForm } from '@app/extraction/criteria/extract-criteria.form';
import { ExtractCriteriaMain, IExtractCriteria, IExtractCriteriaMain, mainCriteriaDefinition } from '@app/extraction/criteria/extract-criteria.model';
import { ExtractCriteriaSurvey } from '@app/extraction/criteria/survey/extract-criteria-survey.model';
import { ExtractCriteriaSamplingOperation } from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.model';
import { ExtractCriteriaSample } from '@app/extraction/criteria/sample/extract-criteria-sample.model';
import { ExtractCriteriaMeasurement } from '@app/extraction/criteria/measurement/extract-criteria-measurement.model';
import { ExtractCriteriaPhoto } from '@app/extraction/criteria/photo/extract-criteria-photo.model';
import { FilterUtils } from '@app/referential/filter/model/filter.utils';
import { EntityUtils } from '@app/shared/entity.utils';
import { Utils } from '@app/shared/utils';
import { ExtractOutputForm } from '@app/extraction/output/extract-output.form';
import { ExtractOutput, IExtractOutput } from '@app/extraction/output/extract-output.model';
import { ExtractMapForm } from '@app/extraction/map/extract-map.form';
import { ExtractMap } from '@app/extraction/map/extract-map.model';
import { Alerts } from '@app/shared/alerts';
import { TextFrameComponent } from '@app/shared/component/text/text-frame.component';
import { ExtractTemplateModal, IExtractTemplateOptions, IExtractTemplateResult } from '@app/extraction/template/extract-template.modal';
import { catchError, distinctUntilChanged, filter, mergeMap } from 'rxjs/operators';
import { ExtractFilterAddModal, IExtractFilterAddResult, IExtractFilterAppOptions } from '@app/extraction/add/extract-filter.add.modal';
import { ExtractFilterFilter, ExtractFilterFilterCriteria } from '@app/extraction/filter/extract-filter.filter.model';
import { BaseTable } from '@app/shared/table/base.table';
import { FilterType } from '@app/referential/filter/model/filter-type';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { Filter } from '@app/referential/filter/model/filter.model';
import { Observable, of } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { FileUtils } from '@app/shared/files';
import { HttpEventType } from '@angular/common/http';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';
import { ExtractCriteriaCampaign, IExtractCriteriaCampaign } from '@app/extraction/criteria/campaign/extract-criteria-campaign.model';
import { ExtractCriteriaOccasion } from '@app/extraction/criteria/occasion/extract-criteria-occasion.model';
import { ExtractCriteriaEvent, IExtractCriteriaEvent } from '@app/extraction/criteria/event/extract-criteria-event.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { ExtractionType } from '@app/extraction/extract.type';
import { ExtractSurveyPeriod } from '@app/extraction/period/extract-survey-period.model';
import { lengthDescription } from '@app/referential/service/referential-validator.service';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { IAddResult, ISelectModalOptions } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { IRightSelectTable } from '@app/shared/table/table.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

import { ReferentialUtils } from '@app/referential/model/referential.utils';

type ExtractFilterView = 'criterias' | 'output' | 'map' | 'description' | 'userTable' | 'departmentTable';

@Component({
  selector: 'app-extract-filter-table',
  templateUrl: './extract-filter.table.html',
  styleUrls: ['./extract-filter.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractFilterTable
  extends BaseTable<ExtractFilter, number, ExtractFilterFilter, ExtractFilterFilterCriteria, ExtractFilterValidator>
  implements OnInit, AfterViewInit
{
  @ViewChild('extractFilterMenu') extractFilterMenu: EntityMenuComponent<ExtractFilter, ExtractFilterView>;
  @ViewChild('extractCriteriaForm') extractCriteriaForm: ExtractCriteriaForm;
  @ViewChild('extractOutputForm') extractOutputForm: ExtractOutputForm;
  @ViewChild('extractMapForm') extractMapForm: ExtractMapForm;
  @ViewChild('descriptionComponent') descriptionComponent: TextFrameComponent;
  @ViewChild('userTable') userTable: UserSelectTable;
  @ViewChild('departmentTable') departmentTable: DepartmentSelectTable;
  @ViewChildren(EntityMenuComponent) menuComponents: QueryList<EntityMenuComponent<ExtractFilter, ExtractFilterView>>;

  dataLoading = false;
  executing = false;
  checking = false;

  readonly mapMenuItem: IEntityMenuItem<ExtractFilter, ExtractFilterView> = {
    title: 'EXTRACTION.EXTRACT_FILTER.MENU.MAP',
    view: 'map',
  };
  readonly criteriaMenuItem: IEntityMenuItem<ExtractFilter, ExtractFilterView> = {
    title: 'EXTRACTION.EXTRACT_FILTER.MENU.CRITERIAS',
    view: 'criterias',
  };
  readonly outputMenuItem: IEntityMenuItem<ExtractFilter, ExtractFilterView> = {
    title: 'EXTRACTION.EXTRACT_FILTER.MENU.OUTPUT',
    view: 'output',
  };
  readonly descriptionMenuItem: IEntityMenuItem<ExtractFilter, ExtractFilterView> = {
    title: 'EXTRACTION.EXTRACT_FILTER.MENU.DESCRIPTION',
    view: 'description',
  };
  readonly responsibleMenuItems: IEntityMenuItem<ExtractFilter, ExtractFilterView> = {
    title: 'EXTRACTION.EXTRACT_FILTER.MENU.RESPONSIBLE',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.USER',
        entityName: 'User',
        attribute: 'responsibleUserIds',
        view: 'userTable',
        viewTitle: 'EXTRACTION.EXTRACT_FILTER.RESPONSIBLE.USER',
      },
      {
        title: 'REFERENTIAL.ENTITY.DEPARTMENT',
        entityName: 'Department',
        attribute: 'responsibleDepartmentIds',
        view: 'departmentTable',
        viewTitle: 'EXTRACTION.EXTRACT_FILTER.RESPONSIBLE.DEPARTMENT',
      },
    ],
  };
  readonly defaultMenus = [this.criteriaMenuItem, this.mapMenuItem, this.outputMenuItem];

  selectedMenuItem: IEntityMenuItem<ExtractFilter, ExtractFilterView> = this.criteriaMenuItem;
  rightAreaEnabled = false;
  preventRestoreVisibleFilters = false;
  mapButtonAccent = false;
  allFilterTypes: FilterType[];

  protected readonly lengthDescription = lengthDescription;

  constructor(
    protected injector: Injector,
    protected _entityService: ExtractFilterService,
    protected validatorService: ExtractFilterValidator,
    protected alertController: AlertController,
    protected platform: PlatformService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'templateTemp']).concat(RESERVED_END_COLUMNS),
      ExtractFilter,
      _entityService,
      validatorService
    );

    this.titleI18n = 'EXTRACTION.EXTRACT_FILTER.TITLE.' + _entityService.type;
    this.i18nColumnPrefix = 'EXTRACTION.EXTRACT_FILTER.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[extract-filter-table]';

    // Will start service with pre-loading
    _entityService.ready().then(() => {
      // Get all filter types available fot this extraction type
      this.allFilterTypes = this._entityService.filterTypes;
    });
  }

  get extractFilterMenuItems(): IEntityMenuItem<ExtractFilter, ExtractFilterView>[] {
    return this.singleSelectedRow?.currentData.template
      ? [...this.defaultMenus, this.descriptionMenuItem, this.responsibleMenuItems]
      : this.defaultMenus;
  }

  get executeDisabled(): boolean {
    return this.disabled || this.loading || this.dirty || !this.singleSelectedRow?.currentData?.id;
  }

  get extractionType(): ExtractionType {
    return this._entityService.type;
  }

  @HostListener('keydown', ['$event'])
  keyPressed(event: KeyboardEvent) {
    if (event.ctrlKey && event.key === 'i') {
      event.preventDefault();
      event.stopPropagation();
      this.addRow(event);
    } else if (event.ctrlKey && event.key === 'd') {
      event.preventDefault();
      event.stopPropagation();
      this.duplicateRow(event);
    }
  }

  ngOnInit() {
    super.ngOnInit();

    this.autoSaveFilter = true;

    // Listen template cell changes
    this.registerSubscription(
      this.registerCellValueChanges('templateTemp', 'templateTemp', true)
        .pipe(
          distinctUntilChanged(),
          filter(() => !!this.singleEditingRow && this.singleEditingRow.currentData.template !== this.singleEditingRow.currentData.templateTemp)
        )
        .subscribe((value) => {
          this.setTemplate(this.singleEditingRow, value);
        })
    );
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.restoreRightPanel(true, 75);

    // First right area load
    this.loadRightArea();
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    const wasEnabled = this.rightAreaEnabled;
    try {
      this.rightAreaEnabled = false;
      this.preventRestoreVisibleFilters = true;
      this.markAsSaving();

      // Try first to confirm current row
      if (!(await this.confirmEditCreate())) {
        // Throw an error with current error if any
        throw { code: ErrorCodes.TABLE_INVALID_ROW_ERROR, message: this.error || 'ERROR.TABLE_INVALID_ROW_ERROR' };
      }

      // Remove kept visible filters
      this.getDirtyRows().forEach((row) => row.validator.patchValue({ visibleFilters: null }));

      // Save
      return await super.save(opts);
    } finally {
      this.preventRestoreVisibleFilters = false;
      this.rightAreaEnabled = wasEnabled;
      this.markAsSaved();
    }
  }

  async onAfterSelectionChange(row?: AsyncTableElement<ExtractFilter>) {
    // Reset selected view if selected row is not template
    await this.resetRightArea(row, false);
    this.refreshButtons();
    await super.onAfterSelectionChange(row);
  }

  async addRow(event?: Event, insertAt?: number, opts?: { focusColumn?: string; editing?: boolean }): Promise<boolean> {
    if (!(await this.confirmEditCreate())) {
      if (this.debug) console.debug(`${this.logPrefix} Cannot confirm current row. Add cancelled`);
      return false;
    }

    // Determine user permission: Only admins or program managers can create empty extract filters
    const canValidateWithoutTemplate = this.permissionService.hasSomeManagementPermission;

    // Open add modal
    const { data, role } = await this.modalService.openModal<IExtractFilterAppOptions, IExtractFilterAddResult>(
      ExtractFilterAddModal,
      {
        service: this._entityService,
        titleI18n: canValidateWithoutTemplate
          ? 'EXTRACTION.EXTRACT_FILTER.ADD_MODAL.TITLE_WITHOUT_TEMPLATE'
          : 'EXTRACTION.EXTRACT_FILTER.ADD_MODAL.TITLE_WITH_TEMPLATE',
        canValidateWithoutTemplate,
        addFirstCriteria: {
          template: true,
          searchText: this.singleSelectedRow?.currentData?.template ? this.singleSelectedRow.currentData.name : undefined,
        },
      },
      'modal-medium'
    );

    if (role === 'cancel') {
      return false;
    } else if (role === 'validate' && !!data) {
      if (!!data.extractFilterId) {
        // Load full extractFilter template
        const template = await this._entityService.load(data.extractFilterId, { fetchPolicy: 'network-only' });
        const templateJson = template.asObject({ minify: false });
        const newRow = await this.addRowToTable(insertAt || toNumber(this.singleSelectedRow?.id + 1));
        newRow.validator.patchValue(this.cleanData(templateJson));
        await this.resetRightArea(newRow, false);
        await this.loadRightArea(newRow);
        return true;
      } else if (canValidateWithoutTemplate) {
        // Add empty extractFilter
        return super.addRow(event, insertAt, opts);
      }
    }
  }

  protected async patchDuplicateEntity(entity: ExtractFilter, opts?: any): Promise<any> {
    const entityObject = ExtractFilter.fromObject(entity);
    return this.cleanData(entityObject.asObject({ minify: false, keepTypename: true, ...opts }));
  }

  cleanData(data: IExtractFilter): IExtractFilter {
    const extractFilter: IExtractFilter = {
      ...data,
      id: undefined,
      updateDate: undefined,
      template: false,
      templateTemp: false,
      description: undefined,
      responsibleUserIds: [],
      responsibleDepartmentIds: [],
      userId: this.accountService.account.id,
      filtersLoaded: true,
      fieldsLoaded: true,
      featuresLoaded: true,
    };
    if (isNotEmptyArray(extractFilter.periods)) {
      extractFilter.periods.forEach((period) => {
        period.id = undefined;
        period.updateDate = undefined;
      });
    }
    if (isNotEmptyArray(extractFilter.filters)) {
      // eslint-disable-next-line @typescript-eslint/no-shadow
      extractFilter.filters.forEach((filter) => {
        filter.id = undefined;
        filter.updateDate = undefined;
        filter.name = undefined;
        filter.userId = extractFilter.userId;
        if (isNotEmptyArray(filter.blocks)) {
          filter.blocks.forEach((block) => {
            block.id = undefined;
            block.updateDate = undefined;
            if (isNotEmptyArray(block.criterias)) {
              block.criterias.forEach((criteria) => {
                criteria.id = undefined;
                criteria.updateDate = undefined;
                if (isNotEmptyArray(criteria.values)) {
                  criteria.values.forEach((value) => {
                    value.id = undefined;
                    value.updateDate = undefined;
                  });
                }
              });
            }
          });
        }
      });
    }
    if (isNotEmptyArray(extractFilter.fields)) {
      extractFilter.fields.forEach((field) => {
        field.id = undefined;
        field.updateDate = undefined;
      });
    }
    return extractFilter;
  }

  updatePermission() {
    // Manage user rights :
    // By default, all user have edition write to allow further finer checks
    this.canEdit = true;
    // Refresh controls
    this.refreshButtons();
    this.markForCheck();
  }

  async extractFilterMenuItemWillChange(event: PromiseEvent<boolean, IEntityMenuItem<ExtractFilter, ExtractFilterView>>, load?: boolean) {
    const canSet = await this.setExtractFilterMenuItem(event.detail, load);
    event.detail.success(canSet);
  }

  async setExtractFilterMenuItem(menuItem: IEntityMenuItem<ExtractFilter, ExtractFilterView>, load?: boolean): Promise<boolean> {
    if (!menuItem) {
      throw new Error(`Cannot determinate the type of right area`);
    }
    const canChange = await this.saveDetail();
    if (!canChange) {
      return false;
    }
    this.selectedMenuItem = menuItem;
    if (load !== false) {
      await this.loadRightArea();
    }
    return true;
  }

  descriptionChanged(event: string) {
    if (isNil(event)) return; // Caused when descriptionComponent is added to DOM
    const row = this.singleEditingRow;
    if (!row) {
      console.warn(`${this.logPrefix} no row to update`);
      return;
    }
    if (this.debug) {
      console.debug(`${this.logPrefix} current description:`, row.currentData.description);
    }
    row.validator.patchValue({ description: event });
    this.markRowAsDirty(row);
    if (this.debug) {
      console.debug(`${this.logPrefix} new description:`, row.currentData.description);
    }
  }

  responsibleSelectionChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow(this.selectedMenuItem.attribute, values);
    }
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<ExtractFilter>): Promise<boolean> {
    // following lines comes from super.editRow(event, row), don't remove
    if (!this._enabled) return false;
    if (this.singleEditingRow === row) return true; // Already the edited row
    if (event?.defaultPrevented) return false;
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // User without right to edit, don't turn on row edition
    if (!this.hasRightOnExtractFilter(row.currentData)) {
      // return true to accept row selection change but don't call super method to disable edition
      return true;
    }

    return super.editRow(event, row);
  }

  readOnlyTemplateFn(): ReadOnlyIfFn {
    return (row) => !this.hasRightOnExtractFilter(row.currentData, true);
  }

  mapFormDirty(dirty: boolean) {
    this.mapButtonAccent = this.extractMapForm?.featureExists;
    this.markForCheck();
    this.subFormDirty(dirty);
  }

  subFormDirty(dirty: boolean) {
    if (dirty && this.canEdit && this.singleSelectedRow) {
      this.markRowAsDirty(this.singleSelectedRow);
    }
  }

  async confirmEditCreate(
    event?: Event,
    row?: AsyncTableElement<ExtractFilter>,
    opts?: { saveDetail?: boolean; interactive?: boolean }
  ): Promise<boolean> {
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row || !row.editing) return true; // no row to confirm

    if (opts?.saveDetail !== false) {
      if (!(await this.saveDetail(row))) return false;
    }

    // Let's check at least one period, one non-empty filter criteria or one geometry source
    if (opts?.interactive !== false) {
      if (!['CAMPAIGN', 'OCCASION', 'EVENT'].includes(this.extractionType) && ExtractFilterUtils.isEmpty(row.currentData)) {
        row.validator.setErrors({ invalid: true });
        await Alerts.showError('EXTRACTION.EXTRACT_FILTER.ERROR.INVALID', this.alertCtrl, this.translate);
        return false;
      }
    }

    // Continue with default row validation
    return super.confirmEditCreate(event, row);
  }

  async importExtractFilter(event: UIEvent) {
    if (this.hasEditingRow()) {
      if (!(await this.confirmEditCreate())) {
        return;
      }
    }

    const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
      title: '',
      fileExtension: '.json, .xml',
      uniqueFile: false,
      maxParallelUpload: 3,
      uploadFn: (file: File) => this.uploadExtractFilter(file),
    });
    if (this.debug) console.debug(`${this.logPrefix} upload result:`, data);

    if (isNotEmptyArray(data)) {
      const invalidFiles: string[] = [];
      const invalidTypeFiles: string[] = [];
      const validExtractFilters = [];

      data.forEach((uploadedFile) => {
        const response = uploadedFile?.response;
        const imported = response?.body;

        if (!imported) {
          // Import invalid
          invalidFiles.push(uploadedFile.name);
          return;
        } else {
          const extractFilter: IExtractFilter = this.cleanData(ExtractFilter.fromObject(imported));

          // Check type against service type
          if (!extractFilter.type || extractFilter.type !== this.extractionType) {
            invalidTypeFiles.push(uploadedFile.name);
            return;
          }

          // Add to valid extractFilter array
          validExtractFilters.push(extractFilter);
        }
      });

      if (isNotEmptyArray(invalidFiles)) {
        // Import invalid todo change i18n
        await Alerts.showDetailedMessage('EXTRACTION.EXTRACT_FILTER.ERROR.INVALID_IMPORT', this.alertController, this.translate, {
          detailedMessage: invalidFiles.join('<br/>'),
        });
      }

      if (isNotEmptyArray(invalidTypeFiles)) {
        await Alerts.showDetailedMessage(
          'EXTRACTION.EXTRACT_FILTER.ERROR.INVALID_EXTRACTION_TYPE',
          this.alertController,
          this.translate,
          { detailedMessage: invalidTypeFiles.join('<br/>') },
          {
            type: this.translate.instant(`EXTRACTION.EXTRACT_FILTER.TITLE.${this.extractionType}`),
          }
        );
      }

      if (isNotEmptyArray(validExtractFilters)) {
        let newRow: AsyncTableElement<ExtractFilter>;
        for (let extractFilter of validExtractFilters) {
          // Validate all filters values (eliminate nonexistent referential ids)
          const cleanedExtractFilter = await this._entityService.cleanExtractFilter(
            ExtractFilter.fromObject(extractFilter).asObject({ keepEntityName: false })
          );
          extractFilter = ExtractFilter.fromObject(cleanedExtractFilter);
          // Validate name
          extractFilter.name = await ReferentialUtils.proposeUniqueName(this._entityService, undefined, extractFilter.name);
          // Add new row
          newRow = await this.addRowToTable();
          if (newRow) {
            newRow.validator.patchValue({ ...extractFilter, filtersLoaded: true, fieldsLoaded: true, featuresLoaded: true });
            this.markAsLoading();
            this.confirmEditCreate(undefined, newRow, { saveDetail: false, interactive: false }).then(() => this.markAsLoaded());
            await Promise.all([waitWhilePending(newRow.validator), this.waitIdle()]);
          } else {
            console.warn(`${this.logPrefix} Cannot add new row for imported extract filter`);
          }
        }
      }
    }
  }

  async exportExtractFilter(event: UIEvent) {
    event?.stopPropagation();
    if (!this.singleSelectedRow) return;
    if (this.hasEditingRow()) {
      if (!(await this.confirmEditCreate())) {
        return;
      }
    }

    if (this.debug) {
      console.debug(`${this.logPrefix} export selected extractFilter`);
    }

    const extractFilter = ExtractFilter.fromObject(this.singleSelectedRow.currentData);
    // Remove unnecessary properties
    extractFilter.fields?.forEach((field) => {
      delete field.id;
      delete field.updateDate;
    });
    extractFilter.filters?.forEach((filter) => {
      delete filter.id;
      delete filter.updateDate;
      delete filter.name;
      delete filter.userId;
      filter.blocks?.forEach((block) => {
        {
          delete block.id;
          delete block.updateDate;
          block.criterias?.forEach((criteria) => {
            delete criteria.id;
            delete criteria.updateDate;
            criteria.values?.forEach((value) => {
              delete value.id;
              delete value.updateDate;
            });
          });
        }
      });
    });

    let json: string;
    if (this.extractionType === 'RESULT') {
      // Merge criterias again with all values (don't remove default values)
      const updatedExtractFilter = this.mergeAllCriterias(extractFilter, { defaultIfEmpty: true });
      extractFilter.filters = updatedExtractFilter.filters;

      // Remove unmanaged property
      delete extractFilter.entityName;

      // Convert to API JSON format
      json = await this._entityService.convertExtractFilterToAPI(extractFilter.asObject());
    }
    // Download directly as JSON (complete format)
    else {
      // Add type
      extractFilter.type = this.extractionType;
      // Remove template information (Mantis #62351)
      extractFilter.template = undefined;
      extractFilter.description = undefined;
      extractFilter.responsibleDepartmentIds = undefined;
      extractFilter.responsibleUserIds = undefined;
      json = JSON.stringify(extractFilter.asObject());
    }
    // Create downloadable JSON file
    const url = window.URL.createObjectURL(new Blob([json], { type: 'text/json' }));
    this.platform.download({ uri: url, filename: `${extractFilter.name}.json` });
  }

  async check(event: UIEvent) {
    event?.preventDefault();
    const row = this.singleSelectedRow;
    const extractFilterId = row?.currentData?.id;
    if (!row || !extractFilterId) {
      console.warn(`${this.logPrefix} Invalid extract filter selected`);
      return;
    }
    if (row.dirty) {
      throw new Error(`${this.logPrefix} Current row is dirty. Cannot check extraction.`);
    }

    // Force row full validation
    const wasEditing = row.editing;
    if (!(await this.forceRowValidation(row, true))) return;

    try {
      this.markAsLoading();
      this.checking = true;
      const checked = await this._entityService.check(extractFilterId);
      await Alerts.showMessage(`EXTRACTION.CHECK.${checked ? 'SHOULD_RETURN_DATA' : 'WILL_RETURN_NO_DATA'}`, this.alertCtrl, this.translate);
    } finally {
      this.markAsLoaded();
      this.checking = false;
      if (wasEditing) {
        this.editRow(undefined, row);
      }
    }
  }

  async execute(event: UIEvent) {
    event?.preventDefault();
    const row = this.singleSelectedRow;
    const extractFilterId = row?.currentData?.id;
    if (!row || !extractFilterId) {
      console.warn(`${this.logPrefix} Invalid extract filter selected`);
      return;
    }
    if (row.dirty) {
      throw new Error(`${this.logPrefix} Current row is dirty. Cannot execute extraction.`);
    }

    // Force row full validation
    const wasEditing = row.editing;
    if (!(await this.forceRowValidation(row, true))) return;

    try {
      this.markAsLoading();
      this.executing = true;
      await this._entityService.execute(extractFilterId);
      await Alerts.showMessage('EXTRACTION.COMMON.EXECUTE_MESSAGE', this.alertCtrl, this.translate);
    } finally {
      this.markAsLoaded();
      this.executing = false;
      if (wasEditing) {
        this.editRow(undefined, row);
      }
    }
  }

  protected rightSplitResized() {
    super.rightSplitResized();
    this.extractCriteriaForm?.refreshComponents();
    this.extractOutputForm?.refreshComponents();
    this.extractMapForm?.refreshComponents();
    this.descriptionComponent?.containerResize();
  }

  // protected methods

  protected async setTemplate(row: AsyncTableElement<ExtractFilter>, template: boolean) {
    row = row || this.singleEditingRow;

    if (template) {
      if (row.currentData.template) {
        // Already a template
        return;
      }
      const { role, data } = await this.modalService.openModal<IExtractTemplateOptions, IExtractTemplateResult>(
        ExtractTemplateModal,
        {
          titleI18n: 'EXTRACTION.EXTRACT_FILTER.TEMPLATE_MODAL.TITLE',
          addCriteria: this.defaultReferentialCriteria,
        },
        'modal-medium'
      );
      if (role === 'validate' && data && this.canEdit) {
        row.validator.patchValue({ ...data, template: true });
        this.markRowAsDirty(row);
      } else {
        // revert
        row.validator.patchValue({ templateTemp: false });
      }
    } else {
      if (!row.currentData.template) {
        // Already a non-template
        return;
      }
      const confirmed = await Alerts.askYesNoConfirmation('EXTRACTION.EXTRACT_FILTER.CONFIRM.NON_TEMPLATE', this.alertCtrl, this.translate);
      if (confirmed) {
        row.validator.patchValue({ template: false, description: null, responsibleUserIds: null, responsibleDepartmentIds: null });
        setTimeout(() => this.resetRightArea(row, true), 50);
      } else {
        // revert
        row.validator.patchValue({ templateTemp: true });
      }
    }
  }

  protected async loadRightArea(row?: AsyncTableElement<ExtractFilter>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;

    if (!row) {
      this.rightAreaEnabled = false;
      return;
    }

    // Mark as loading asap
    this.cd.detectChanges();
    this.extractCriteriaForm?.markAsLoading({ emitEvent: false });
    this.extractOutputForm?.markAsLoading({ emitEvent: false });
    this.extractMapForm?.markAsLoading({ emitEvent: false });
    this.descriptionComponent?.markAsLoading({ emitEvent: false });
    this.userTable?.markAsLoading({ emitEvent: false });
    this.departmentTable?.markAsLoading({ emitEvent: false });

    // Enable right panel
    this.rightAreaEnabled = this.hasRightOnExtractFilter(row?.currentData);

    // Load data
    await this.loadExtractFilterData(row);

    switch (this.selectedMenuItem.view) {
      case 'criterias': {
        this.registerSubForm(this.extractCriteriaForm);
        await this.loadCriteriaForm(row);
        break;
      }
      case 'output': {
        this.registerSubForm(this.extractOutputForm);
        await this.loadOutputForm(row);
        break;
      }
      case 'map': {
        this.registerSubForm(this.extractMapForm);
        await this.loadMapForm(row);
        break;
      }
      case 'description': {
        this.registerSubForm(this.descriptionComponent);
        await this.loadDescriptionForm(row);
        break;
      }
      case 'userTable': {
        this.registerSubForm(this.userTable);
        await this.loadUserTable(row);
        break;
      }
      case 'departmentTable': {
        this.registerSubForm(this.departmentTable);
        await this.loadDepartmentTable(row);
        break;
      }
      default: {
        console.error(`${this.logPrefix} No view for menu item: ${this.selectedMenuItem.title}`);
      }
    }

    // Update VIEW
    this.mapButtonAccent = !!row.currentData?.geometrySource;

    this.extractCriteriaForm?.markAsLoaded();
    this.extractOutputForm?.markAsLoaded();
    this.extractMapForm?.markAsLoaded();
    this.descriptionComponent?.markAsLoaded();
    this.userTable?.markAsLoaded();
    this.departmentTable?.markAsLoaded();
  }

  protected async resetRightArea(row: AsyncTableElement<ExtractFilter>, load: boolean) {
    if (this.defaultMenus.includes(this.selectedMenuItem)) return;
    if (!row || !row.currentData.template) {
      // Reset to criteria view
      await this.setExtractFilterMenuItem(this.criteriaMenuItem, load);
    }
  }

  protected async defaultNewRowValue() {
    return <IExtractFilter>{
      ...(await super.defaultNewRowValue()),
      userId: this.accountService.person.id,
      fileTypes: ['CSV'],
    };
  }

  protected equals(d1: ExtractFilter, d2: ExtractFilter): boolean {
    return super.equals(d1, d2) || EntityUtils.equals(d1, d2, 'name');
  }

  protected async saveDetail(row?: AsyncTableElement<ExtractFilter>): Promise<boolean> {
    row = row || this.singleEditingRow || this.singleSelectedRow;

    if (row) {
      if (!(await this.saveCriteriaForm(row))) return false;
      if (!(await this.saveOutputForm(row))) return false;
      if (!this.saveMapForm(row)) return false;
    }

    return true;
  }

  protected hasRightOnExtractFilter(extractFilter: ExtractFilter, templateChange?: boolean): boolean {
    if (!extractFilter) {
      return false;
    }

    // Admins have all rights
    if (this.accountService.isAdmin()) return true;

    // Admins and user owning the entity can edit
    return (
      (!extractFilter.template &&
        (templateChange
          ? // Only program managers can create templates
            this.permissionService.hasSomeManagementPermission
          : // Only user owning the entity can edit
            extractFilter.userId === this.accountService.person.id)) ||
      // Only template responsible can edit/remove templates
      (extractFilter.template &&
        (extractFilter.responsibleUserIds?.includes(this.accountService.person.id) ||
          extractFilter.responsibleDepartmentIds?.includes(this.accountService.person.department?.id)))
    );
  }

  /**
   * Refresh buttons state according to user profile or program rights
   *
   * @protected
   */
  protected refreshButtons() {
    // Only users with specific rights can modify
    const hasRightOnSelection = toBoolean(
      this.selection.selected.every((row) => this.hasRightOnExtractFilter(row?.currentData)),
      false
    );
    const hasSelectedTemplate = toBoolean(
      this.selection.selected.some((row) => row.currentData?.template),
      false
    );
    this.tableButtons.canDuplicate = hasRightOnSelection && !hasSelectedTemplate;
    this.tableButtons.canDelete = hasRightOnSelection;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'templateTemp');
  }

  private async loadCriteriaForm(row: AsyncTableElement<ExtractFilter>) {
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.extractCriteriaForm?.clear();
      return;
    }

    // Parse filters to get all criterias
    const data = row.currentData;
    const extractCriteria = this.parseExtractCriteria(data);

    // Initial visible filters
    if (isNil(data.id)) {
      if (['CAMPAIGN', 'OCCASION'].includes(this.extractionType)) {
        let criteriaCampaign: IExtractCriteriaCampaign = extractCriteria.campaignCriterias[0];
        if (!criteriaCampaign) {
          criteriaCampaign = new ExtractCriteriaCampaign();
          extractCriteria.campaignCriterias.push(criteriaCampaign);
        }
        criteriaCampaign.visibleFilters = ['programFilter', 'startDate', 'endDate'];
      } else if (this.extractionType === 'EVENT') {
        let criteriaEvent: IExtractCriteriaEvent = extractCriteria.eventCriterias[0];
        if (!criteriaEvent) {
          criteriaEvent = new ExtractCriteriaEvent();
          extractCriteria.eventCriterias.push(criteriaEvent);
        }
        criteriaEvent.visibleFilters = ['typeFilter', 'startDate', 'endDate'];
      } else {
        extractCriteria.visibleFilters = ['programFilter', 'monitoringLocationFilter'];
        // Add a period if any
        if (isEmptyArray(extractCriteria.periods)) {
          extractCriteria.periods = [new ExtractSurveyPeriod()];
        }
      }
    }

    // this.extractCriteriaCache = extractCriteria;
    this.extractCriteriaForm?.setValue(ExtractCriteriaMain.fromObject(extractCriteria));
    // Make this to ensure this form and sub forms are enabled or disabled (Mantis #62332)
    setTimeout(() => this.extractCriteriaForm?.enable());
  }

  private parseExtractCriteria(extractFilter: ExtractFilter): IExtractCriteriaMain {
    // Main criterias
    const extractCriteria: IExtractCriteriaMain = {
      id: 0,
      ...ExtractCriteriaMain.parse(extractFilter.filters?.find((value) => value.filterType === 'EXTRACT_DATA_MAIN')?.blocks?.[0]?.criterias || []),
      periods: extractFilter.periods?.slice() || [],
      visibleFilters: extractFilter.visibleFilters?.mainFilters?.[0],
    };

    // Survey criterias
    if (this.allFilterTypes.includes('EXTRACT_DATA_SURVEY'))
      extractCriteria.surveyCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_DATA_SURVEY',
        ExtractCriteriaSurvey.parse,
        ExtractCriteriaSurvey.fromObject,
        'surveyFilters'
      );

    // Sampling operation criterias
    if (this.allFilterTypes.includes('EXTRACT_DATA_SAMPLING_OPERATION'))
      extractCriteria.samplingOperationCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_DATA_SAMPLING_OPERATION',
        ExtractCriteriaSamplingOperation.parse,
        ExtractCriteriaSamplingOperation.fromObject,
        'samplingOperationFilters'
      );

    // Sample criterias
    if (this.allFilterTypes.includes('EXTRACT_DATA_SAMPLE'))
      extractCriteria.sampleCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_DATA_SAMPLE',
        ExtractCriteriaSample.parse,
        ExtractCriteriaSample.fromObject,
        'sampleFilters'
      );

    // Measurement criterias
    if (this.allFilterTypes.includes('EXTRACT_DATA_MEASUREMENT'))
      extractCriteria.measurementCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_DATA_MEASUREMENT',
        ExtractCriteriaMeasurement.parse,
        ExtractCriteriaMeasurement.fromObject,
        'measurementFilters'
      );

    // Photo criterias
    if (this.allFilterTypes.includes('EXTRACT_DATA_PHOTO'))
      extractCriteria.photoCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_DATA_PHOTO',
        ExtractCriteriaPhoto.parse,
        ExtractCriteriaPhoto.fromObject,
        'photoFilters'
      );

    // Campaign criterias
    if (this.allFilterTypes.includes('EXTRACT_CAMPAIGN'))
      extractCriteria.campaignCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_CAMPAIGN',
        ExtractCriteriaCampaign.parse,
        ExtractCriteriaCampaign.fromObject,
        'campaignFilters'
      );

    // Occasion criterias
    if (this.allFilterTypes.includes('EXTRACT_OCCASION'))
      extractCriteria.occasionCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_OCCASION',
        ExtractCriteriaOccasion.parse,
        ExtractCriteriaOccasion.fromObject,
        'occasionFilters'
      );

    // Event criterias
    if (this.allFilterTypes.includes('EXTRACT_EVENT'))
      extractCriteria.eventCriterias = this.parseCriterias(
        extractFilter,
        'EXTRACT_EVENT',
        ExtractCriteriaEvent.parse,
        ExtractCriteriaEvent.fromObject,
        'eventFilters'
      );

    return extractCriteria;
  }

  private parseCriterias(
    data: ExtractFilter,
    filterType: FilterType,
    parseFn: (criterias: FilterCriteria[], id: number) => any,
    fromObjectFn: (source: any) => any,
    visibleFiltersProperty: keyof IVisibleFilter
  ): any[] {
    const extractCriterias =
      data.filters
        ?.find((value) => value.filterType === filterType)
        ?.blocks.sort(EntityUtils.sortComparator('rankOrder'))
        .map((block, index) => ({
          ...parseFn(block.criterias, index),
          visibleFilters: data.visibleFilters?.[visibleFiltersProperty]?.[index],
        })) || [];
    // Add empty blocks to restore previous visible filters
    const previousVisibleFilters: string[][] = data.visibleFilters?.[visibleFiltersProperty] || [];
    if (previousVisibleFilters.length > extractCriterias.length) {
      for (let index = extractCriterias.length; index < previousVisibleFilters.length; index++) {
        if (!!previousVisibleFilters[index]) {
          extractCriterias.push({ ...fromObjectFn({ visibleFilters: previousVisibleFilters[index] }) });
        }
      }
    }
    return extractCriterias;
  }

  private async saveCriteriaForm(row: AsyncTableElement<ExtractFilter>): Promise<boolean> {
    if (this.extractCriteriaForm) {
      if (!this.preventRestoreVisibleFilters) {
        // Keep visible filters in row
        const visibleFilters = this.extractCriteriaForm.getAllVisibleFilters();
        row.validator.patchValue({ visibleFilters }, { onlySelf: true, emitEvent: false });
      }

      if (this.extractCriteriaForm.dirty || row.id === -1) {
        this.extractCriteriaForm.removeEmptyPeriod();

        // Check validity
        this.extractCriteriaForm.form.updateValueAndValidity();
        if (this.extractCriteriaForm.invalid) {
          const error = this.extractCriteriaForm.getValidationError();
          this.markAsError(error);
          return false;
        }

        // Merge all criterias and update current row
        const extractFilter = this.mergeAllCriterias(row.currentData);
        row.validator.patchValue(extractFilter);

        // Mark as pristine to avoid future save (ex: after a table save)
        this.extractCriteriaForm.markAsPristine();
      }
    }
    return true;
  }

  /**
   * Merge all criterias in provided filters
   *
   * @param extractFilter in which criterias will be merged
   * @param opts merge options
   * @return resulting IExtractFilter
   */
  private mergeAllCriterias(extractFilter: ExtractFilter, opts?: IMergeCriteriaOptions): IExtractFilter {
    // Default options: don't use default value if a criteria is empty
    opts = { defaultIfEmpty: false, ...opts };

    let extractCriteria: IExtractCriteriaMain;
    if (this.extractCriteriaForm) {
      // Must use fromObject->asObject to compute boolean properties
      extractCriteria = ExtractCriteriaMain.fromObject(this.extractCriteriaForm.value).asObject();
    } else {
      // Recalculate from data
      extractCriteria = this.parseExtractCriteria(extractFilter);
    }

    const filters = extractFilter.filters || [];

    // main filters
    if (this.allFilterTypes.includes('EXTRACT_DATA_MAIN'))
      ExtractCriteriaMain.merge(
        FilterUtils.getOrCreateBlock(FilterUtils.getOrCreateFilter(filters, 'EXTRACT_DATA_MAIN').blocks, 0).criterias,
        extractCriteria
      );

    // survey
    if (this.allFilterTypes.includes('EXTRACT_DATA_SURVEY'))
      this.mergeCriterias(filters, 'EXTRACT_DATA_SURVEY', extractCriteria.surveyCriterias, opts, ExtractCriteriaSurvey.merge);

    // sampling operation
    if (this.allFilterTypes.includes('EXTRACT_DATA_SAMPLING_OPERATION'))
      this.mergeCriterias(
        filters,
        'EXTRACT_DATA_SAMPLING_OPERATION',
        extractCriteria.samplingOperationCriterias,
        opts,
        ExtractCriteriaSamplingOperation.merge
      );

    // sample
    if (this.allFilterTypes.includes('EXTRACT_DATA_SAMPLE'))
      this.mergeCriterias(filters, 'EXTRACT_DATA_SAMPLE', extractCriteria.sampleCriterias, opts, ExtractCriteriaSample.merge);

    // measurement
    if (this.allFilterTypes.includes('EXTRACT_DATA_MEASUREMENT'))
      this.mergeCriterias(filters, 'EXTRACT_DATA_MEASUREMENT', extractCriteria.measurementCriterias, opts, ExtractCriteriaMeasurement.merge);

    // photo
    if (this.allFilterTypes.includes('EXTRACT_DATA_PHOTO'))
      this.mergeCriterias(filters, 'EXTRACT_DATA_PHOTO', extractCriteria.photoCriterias, opts, ExtractCriteriaPhoto.merge);

    // campaign
    if (this.allFilterTypes.includes('EXTRACT_CAMPAIGN'))
      this.mergeCriterias(filters, 'EXTRACT_CAMPAIGN', extractCriteria.campaignCriterias, opts, ExtractCriteriaCampaign.merge);

    // occasion
    if (this.allFilterTypes.includes('EXTRACT_OCCASION'))
      this.mergeCriterias(filters, 'EXTRACT_OCCASION', extractCriteria.occasionCriterias, opts, ExtractCriteriaOccasion.merge);

    // event
    if (this.allFilterTypes.includes('EXTRACT_EVENT'))
      this.mergeCriterias(filters, 'EXTRACT_EVENT', extractCriteria.eventCriterias, opts, ExtractCriteriaEvent.merge);

    return <IExtractFilter>{
      periods: extractCriteria.periods,
      filters,
      filtersLoaded: true,
    };
  }

  private mergeCriterias(
    filters: Filter[],
    filterType: FilterType,
    extractCriterias: IExtractCriteria[],
    opts: IMergeCriteriaOptions,
    mergeFn: (filterCriterias: FilterCriteria[], extractCriteria: IExtractCriteria, opts: IMergeCriteriaOptions) => void
  ) {
    const targetFilter = FilterUtils.getOrCreateFilter(filters, filterType);
    extractCriterias?.forEach((criteria, index) => mergeFn(FilterUtils.getOrCreateBlock(targetFilter.blocks, index).criterias, criteria, opts));
    // Remove empty blocks (Mantis #60665)
    targetFilter.blocks = targetFilter.blocks.filter((block) => isNotEmptyArray(block.criterias));
    Utils.trimArray(targetFilter.blocks, arraySize(extractCriterias));
    FilterUtils.setBlockRankOrders(targetFilter.blocks);
  }

  private async loadOutputForm(row: AsyncTableElement<ExtractFilter>) {
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.extractOutputForm?.clear();
      return;
    }

    const data = row.currentData;

    // Create output model from ExtractFilter and some filters (from main criterias)
    const mainCriterias = data.filters?.find((value) => value.filterType === 'EXTRACT_DATA_MAIN')?.blocks?.[0]?.criterias || [];

    const hiddenFields = [];
    // Find meta program filter
    if (
      BaseFilterUtils.isCriteriaEmpty(
        ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaMain>(mainCriterias, mainCriteriaDefinition, 'metaProgramFilter'),
        true
      )
    ) {
      // Hide meta program fields if no meta program filter filled
      hiddenFields.push(
        ...[
          'SURVEY_META_PROGRAMS_ID',
          'SURVEY_META_PROGRAMS_NAME',
          'SAMPLING_OPERATION_META_PROGRAMS_ID',
          'SAMPLING_OPERATION_META_PROGRAMS_NAME',
          'SAMPLE_META_PROGRAMS_ID',
          'SAMPLE_META_PROGRAMS_NAME',
          'MEASUREMENT_META_PROGRAMS_ID',
          'MEASUREMENT_META_PROGRAMS_NAME',
        ]
      );
    }

    const extractOutput = ExtractOutput.fromObject({
      ...data,
      ...ExtractOutput.parse(mainCriterias),
      hiddenFields,
    });

    await this.extractOutputForm?.setValue(extractOutput.asObject());
    setTimeout(() => this.extractOutputForm?.enable());
  }

  private async saveOutputForm(row: AsyncTableElement<ExtractFilter>): Promise<boolean> {
    if (this.extractOutputForm && (this.extractOutputForm.dirty || row.id === -1)) {
      // Check validity
      this.extractOutputForm.updateValueAndValidity();
      if (this.extractOutputForm.invalid) {
        const error = this.extractOutputForm.getValidationError();
        this.markAsError(error);
        return false;
      }

      // Save fields directly on row
      if (await this.extractOutputForm.fieldTable.save()) {
        const fields = this.extractOutputForm.fieldTable.value;
        this.patchRow('fields', fields, row);
        if (isEmptyArray(fields)) {
          this.markAsError(this.translate.instant('EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_EXTRACT_FIELD'));
          return false;
        }
      } else {
        return false;
      }

      const filters = row.currentData.filters || [];
      // Must use fromObject->asObject to compute boolean properties
      const extractOutput = ExtractOutput.fromObject(this.extractOutputForm.value).asObject() as IExtractOutput;

      // Remove order item type filter if no type of this kind
      if (!this.extractOutputForm.orderItemTypeFilterEnabled) {
        extractOutput.orderItemTypeFilter = undefined;
      }

      // main filters
      ExtractOutput.merge(
        FilterUtils.getOrCreateBlock(FilterUtils.getOrCreateFilter(filters, 'EXTRACT_DATA_MAIN').blocks, 0).criterias,
        extractOutput
      );

      row.validator.patchValue(<IExtractFilter>{
        fileTypes: extractOutput.fileTypes,
        filters,
        filtersLoaded: true,
        fieldsLoaded: true,
      });

      // Mark as pristine to avoid future save (ex: after a table save)
      this.extractOutputForm.markAsPristine();
    }
    return true;
  }

  private async loadMapForm(row: AsyncTableElement<ExtractFilter>) {
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.extractMapForm?.setData(undefined);
      return;
    }

    const data = row.currentData;

    // Create map model from ExtractFilter
    const extractOutput = ExtractMap.fromObject({ ...data });

    await this.extractMapForm?.setData(extractOutput.asObject());
    setTimeout(() => this.extractMapForm?.enable());
  }

  private saveMapForm(row: AsyncTableElement<ExtractFilter>): boolean {
    if (this.extractMapForm && (this.extractMapForm.dirty || row.id === -1)) {
      // Get Map data
      const extractMap = this.extractMapForm.getData();
      row.validator.patchValue(extractMap);

      // Mark as pristine to avoid future save (ex: after a table save)
      this.extractMapForm.markAsPristine();
    }
    return true;
  }

  private async loadDescriptionForm(row: AsyncTableElement<ExtractFilter>) {
    this.descriptionComponent.value = row?.currentData.description;
    this.descriptionComponent.containerResize();
  }

  protected async loadUserTable(row?: AsyncTableElement<ExtractFilter>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.userTable, row);
      await this.userTable.setSelectedIds(undefined);
      return;
    }

    // Load selected users
    this.applyRightOptions(this.userTable, row);
    await this.userTable.setSelectedIds((row.currentData?.responsibleUserIds || []).slice());
  }

  protected async addUser(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'User',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.selectedMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.userTable.selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.userTable);
      await this.userTable.setSelectedIds(includedIds);
      this.responsibleSelectionChanged(includedIds);
    }
  }

  protected async loadDepartmentTable(row?: AsyncTableElement<ExtractFilter>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.departmentTable, row);
      await this.departmentTable.setSelectedIds(undefined);
      return;
    }

    // Load selected departments
    this.applyRightOptions(this.departmentTable, row);
    await this.departmentTable.setSelectedIds((row.currentData?.responsibleDepartmentIds || []).slice());
  }

  protected async addDepartment(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'Department',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.selectedMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.departmentTable.selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.departmentTable);
      await this.departmentTable.setSelectedIds(includedIds);
      this.responsibleSelectionChanged(includedIds);
    }
  }

  protected applyRightOptions(table: IRightSelectTable<any>, row?: AsyncTableElement<ExtractFilter>) {
    row = row || this.singleEditingRow;
    table.rightOptions = {
      entityName: ExtractFilter.entityName,
      extractFilterId: row?.currentData.id,
    };
  }

  private async loadExtractFilterData(row: AsyncTableElement<ExtractFilter>) {
    if (!row) return;
    if (this.dataLoading) {
      console.warn(`${this.logPrefix} Already loading`);
      return;
    }
    try {
      this.dataLoading = true;
      // Call service if detail data is not loaded yet
      if (!row.currentData.filtersLoaded) {
        const filters = await this._entityService.loadFilters(row.currentData.id);
        row.validator.patchValue({ filters, filtersLoaded: true });
      }
      if (!row.currentData.fieldsLoaded) {
        const fields = await this._entityService.loadFields(row.currentData.id);
        row.validator.patchValue({ fields, fieldsLoaded: isNotEmptyArray(fields) });
      }
      if (!row.currentData.featuresLoaded) {
        const features = await this._entityService.loadFeatures(row.currentData.id);
        row.validator.patchValue({ features, featuresLoaded: true });
      }
    } catch (e) {
      await this.handleError(e);
    } finally {
      this.dataLoading = false;
    }
  }

  private uploadExtractFilter(file: File): Observable<FileEvent<IExtractFilter>> {
    console.info(`${this.logPrefix} Importing ${file.name}`);
    const extension = file.name.split('.').pop().toLowerCase();
    // 'jso' extension added to support 8.3 file format (Mantis #63048)
    if (!['json', 'jso', 'xml'].includes(extension)) {
      return of(new FileResponse<IExtractFilter>({ status: 400 }));
    }
    return FileUtils.readAsText(file).pipe(
      mergeMap(async (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          const loaded = Math.round(event.loaded * 0.8);
          return { ...event, loaded };
        } else if (event instanceof FileResponse) {
          if (['json', 'jso'].includes(extension)) {
            const content = event.body;
            // Detect API format or full format
            if (content.includes('"entityName":"ExtractFilter"') || content.includes('"type":')) {
              // Parse JSON directly
              return new FileResponse<IExtractFilter>({ body: JSON.parse(content) });
            } else {
              // Try to convert a API Json
              const extractFilter = await this._entityService.convertAPIJsonToExtractFilter(content);
              let body = undefined;
              if (isNotNil(extractFilter)) {
                body = { ...extractFilter, type: 'RESULT' };
              }
              // Force result type
              return new FileResponse<IExtractFilter>({ body });
            }
          } else if (extension === 'xml') {
            // Parse XML in Q² format
            const converted = await this._entityService.convertXMLToExtractFilter(event.body);
            if (this.debug) {
              console.debug(`${this.logPrefix} Converted object`, converted);
            }
            return new FileResponse<IExtractFilter>({ body: converted });
          } else {
            console.error(`${this.logPrefix} Should not happen`);
            return null;
          }
        } else {
          // Unknown event: skip
          return null;
        }
      }),
      filter(isNotNil),
      catchError((err) => {
        console.error(`${this.logPrefix} Error while parsing ${file}`, err);
        throw new Error(this.translate.instant('EXTRACTION.EXTRACT_FILTER.ERROR.INVALID_IMPORT'));
      })
    );
  }

  private async forceRowValidation(row: AsyncTableElement<ExtractFilter>, showAlert?: boolean) {
    const wasEnabled = row.validator.enabled;
    if (!wasEnabled) {
      row.validator.enable();
      await waitWhilePending(row.validator);
    }
    row.validator.patchValue({ fieldsLoaded: true });
    const confirmed = await this.confirmEditCreate(undefined, row, { saveDetail: false });
    const error = this.getRowError(row);
    if (!wasEnabled) {
      row.validator.disable();
    }
    if (!confirmed) {
      setTimeout(async () => {
        await this.selectRowById(row.id);
        if (isNotNilOrBlank(error)) {
          if (showAlert) {
            Alerts.showError(error, this.alertCtrl, this.translate);
          } else {
            this.setError(error);
          }
        }
      });
      return false;
    }
    return true;
  }
}
