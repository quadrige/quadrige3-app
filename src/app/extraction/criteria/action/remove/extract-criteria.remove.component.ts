import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-extract-criteria-remove-component',
  templateUrl: './extract-criteria.remove.component.html',
})
export class ExtractCriteriaRemoveComponent {
  @Input() titleI18n = 'EXTRACTION.EXTRACT_FILTER.BTN_REMOVE_FILTER';
  @Input() visible = true;
  @Output() remove = new EventEmitter<UIEvent>();

  constructor() {}
}
