import { ExtractCriteriaAddModel } from '@app/extraction/criteria/action/add/extract-criteria.add.model';
import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { ExtractCriteriaAddModelFilter } from '@app/extraction/criteria/action/add/filter/extract-criteria.add.filter.model';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaAddService extends EntitiesMemoryService<ExtractCriteriaAddModel, ExtractCriteriaAddModelFilter, string> {
  constructor() {
    super(ExtractCriteriaAddModel, ExtractCriteriaAddModelFilter);
  }
}
