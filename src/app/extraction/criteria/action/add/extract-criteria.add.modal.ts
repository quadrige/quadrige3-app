import { Component, inject, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { ExtractCriteriaAddTable } from '@app/extraction/criteria/action/add/extract-criteria.add.table';
import { MatTabNav } from '@angular/material/tabs';
import { ExtractCriteriaAddModel } from '@app/extraction/criteria/action/add/extract-criteria.add.model';
import { filter } from 'rxjs/operators';
import { uniqueValue } from '@app/shared/utils';
import { EntityUtils } from '@app/shared/entity.utils';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';
import { Alerts } from '@app/shared/alerts';
import { AlertController } from '@ionic/angular';
import { arraySize, isNotEmptyArray } from '@sumaris-net/ngx-components';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

export interface IExtractCriteriaOptions extends IModalOptions {
  i18nLabel: string; // todo titleI18n
  criteriaDefinition: CriteriaDefinitionRecord<any>;
  availableFiltersByGroup: Record<number, string[]>;
  possibleFilters: string[];
  blocksEnabled: boolean;
}

@Component({
  selector: 'app-extract-criteria-add-modal',
  templateUrl: './extract-criteria.add.modal.html',
  styleUrls: ['./extract-criteria.add.modal.scss'],
})
export class ExtractCriteriaAddModal extends ModalComponent<string[][]> implements IExtractCriteriaOptions, OnInit {
  @Input() i18nLabel: string;
  @Input() criteriaDefinition: CriteriaDefinitionRecord<any>;
  @Input() availableFiltersByGroup: string[][];
  @Input() possibleFilters: string[];
  @Input() blocksEnabled: boolean;

  @ViewChild('groupTabs') groupTabs: MatTabNav;
  @ViewChild('extractCriteriaAddTable') table: ExtractCriteriaAddTable;

  nbTabs: number;
  tabs: number[];
  activeTab: number;
  selectedFiltersByGroup: string[][];

  protected readonly alertController = inject(AlertController);

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled(): boolean {
    return super.disabled || !arraySize(this.selectedFiltersByGroup) || this.selectedFiltersByGroup.every((value) => !value.length);
  }

  ngOnInit() {
    this.nbTabs = arraySize(this.availableFiltersByGroup);
    this.initTabs();
  }

  tabChange(event: MouseEvent, tab: number) {
    this.activeTab = tab;
    this.updateTable();
  }

  selectedCriteriaCount(tab: number): number | undefined {
    const count = arraySize(this.selectedFiltersByGroup[tab]);
    return count === 0 ? undefined : count;
  }

  /**
   * Add a tab at the end
   */
  addTab() {
    this.nbTabs++;
    this.initTabs();
    this.activeTab = this.nbTabs - 1;
    this.updateTable();
  }

  /**
   * Remove active tab
   */
  async removeTab() {
    if (this.selectedCriteriaCount(this.activeTab)) {
      if (!(await Alerts.askDetailedConfirmation('EXTRACTION.EXTRACT_FILTER.BLOCK.CONFIRM.DELETE', this.alertController, this.translate))) {
        return;
      }
    }
    this.selectedFiltersByGroup.splice(this.activeTab, 1);
    this.nbTabs--;
    this.initTabs();
    this.activeTab = Math.max(0, this.activeTab - 1);
    this.updateTable();
  }

  /**
   * Duplicate active tab
   */
  duplicateTab() {
    this.selectedFiltersByGroup.splice(this.activeTab, 0, this.selectedFiltersByGroup[this.activeTab]);
    this.nbTabs++;
    this.initTabs();
    this.activeTab++;
    this.updateTable();
  }

  protected afterInit() {
    this.registerSubscription(
      this.table.dataSource.datasourceSubject.subscribe(() => {
        // restore the 'adding' selection (for example after a page or sort change)
        this.refreshSelection();
      })
    );

    // Listen selection change
    this.registerSubscription(
      this.table.selection.changed.pipe(filter(() => this.table.loaded && !this.loading)).subscribe((value) => {
        // add to selection
        if (isNotEmptyArray(value.added)) {
          const selectedIds = this.selectedFiltersByGroup[this.activeTab].slice();
          selectedIds.push(...EntityUtils.ids(value.added));
          this.selectedFiltersByGroup[this.activeTab] = selectedIds.filter(uniqueValue);
        }
        // remove from selection
        if (isNotEmptyArray(value.removed)) {
          const removedIds = EntityUtils.ids(value.removed);
          this.selectedFiltersByGroup[this.activeTab] = this.selectedFiltersByGroup[this.activeTab].filter((id) => !removedIds.includes(id));
        }
      })
    );

    // Must use a small timeout to allow tabs refreshing correctly
    setTimeout(() => {
      this.activeTab = 0;
      this.groupTabs?.updatePagination();
      this.groupTabs?._alignInkBarToSelectedTab();
      this.updateTable();
    }, 100);
  }

  protected dataToValidate(): string[][] {
    return this.selectedFiltersByGroup.filter(
      // Keep non-empty filters
      (value, index) =>
        arraySize(value) > 0 ||
        // And previously non-empty blocs
        (!!this.availableFiltersByGroup[index] && arraySize(this.availableFiltersByGroup[index]) !== arraySize(this.possibleFilters))
    );
  }

  protected initTabs() {
    this.tabs = [...Array(this.nbTabs).keys()];
    if (!this.selectedFiltersByGroup) {
      this.selectedFiltersByGroup = [...Array(this.nbTabs).fill([])];
    } else {
      for (let i = arraySize(this.selectedFiltersByGroup); i < this.nbTabs; i++) {
        this.selectedFiltersByGroup.push([]);
      }
    }
  }

  protected updateTable() {
    this.markAsLoading();
    const filters = this.availableFiltersByGroup[this.activeTab]?.filter((value) => this.possibleFilters?.includes(value)) || this.possibleFilters;
    this.table
      .setValue(
        filters.map((id) =>
          ExtractCriteriaAddModel.fromObject({
            id,
            name: this.translate.instant(this.criteriaDefinition[id].i18nLabel),
            rankOrder: filters.indexOf(id),
          })
        )
      )
      .then(() => {
        this.refreshSelection();
        this.markAsLoaded();
      });
  }

  protected refreshSelection() {
    this.table.getRows().forEach((row) => {
      if (this.selectedFiltersByGroup[this.activeTab].includes(row.currentData.id)) this.table.selection.select(row);
    });
    this.table.markForCheck();
  }
}
