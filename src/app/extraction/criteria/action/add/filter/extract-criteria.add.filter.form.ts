import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import {
  ExtractCriteriaAddModelFilter,
  ExtractCriteriaAddModelFilterCriteria,
} from '@app/extraction/criteria/action/add/filter/extract-criteria.add.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-extract-criteria-add-filter-form',
  templateUrl: './extract-criteria.add.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaAddFilterForm extends BaseCriteriaFormComponent<ExtractCriteriaAddModelFilter, ExtractCriteriaAddModelFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.name;
  }
}
