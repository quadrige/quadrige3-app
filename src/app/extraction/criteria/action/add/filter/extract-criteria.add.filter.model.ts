import { BaseEntityFilter, BaseEntityFilterCriteria } from '@app/shared/model/filter.model';
import { ExtractCriteriaAddModel } from '@app/extraction/criteria/action/add/extract-criteria.add.model';

export class ExtractCriteriaAddModelFilterCriteria extends BaseEntityFilterCriteria<ExtractCriteriaAddModel, string> {
  static fromObject(source: any): ExtractCriteriaAddModelFilterCriteria {
    const target = new ExtractCriteriaAddModelFilterCriteria();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.searchAttributes = ['name']; // default search attribute
  }
}

export class ExtractCriteriaAddModelFilter extends BaseEntityFilter<
  ExtractCriteriaAddModelFilter,
  ExtractCriteriaAddModelFilterCriteria,
  ExtractCriteriaAddModel,
  string
> {
  static fromObject(source: any): ExtractCriteriaAddModelFilter {
    const target = new ExtractCriteriaAddModelFilter();
    if (source) target.fromObject(source);
    return target;
  }

  criteriaFromObject(source: any, opts: any): ExtractCriteriaAddModelFilterCriteria {
    return ExtractCriteriaAddModelFilterCriteria.fromObject(source);
  }
}
