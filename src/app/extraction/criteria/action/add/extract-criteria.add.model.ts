import { Entity } from '@sumaris-net/ngx-components';

export class ExtractCriteriaAddModel extends Entity<ExtractCriteriaAddModel, string> {
  name: string;
  rankOrder: number;

  constructor() {
    super('ExtractCriteriaAddModel');
  }

  static fromObject(source: any): ExtractCriteriaAddModel {
    const target = new ExtractCriteriaAddModel();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.name = source.name;
    this.rankOrder = source.rankOrder;
  }
}
