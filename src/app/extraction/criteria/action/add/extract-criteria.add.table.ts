import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { ExtractCriteriaAddModel } from '@app/extraction/criteria/action/add/extract-criteria.add.model';
import { ExtractCriteriaAddService } from '@app/extraction/criteria/action/add/extract-criteria.add.service';
import {
  ExtractCriteriaAddModelFilter,
  ExtractCriteriaAddModelFilterCriteria,
} from '@app/extraction/criteria/action/add/filter/extract-criteria.add.filter.model';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { SortDirection } from '@angular/material/sort';

@Component({
  selector: 'app-extract-criteria-add-table',
  templateUrl: './extract-criteria.add.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaAddTable
  extends BaseMemoryTable<ExtractCriteriaAddModel, string, ExtractCriteriaAddModelFilter, ExtractCriteriaAddModelFilterCriteria>
  implements OnInit, AfterViewInit
{
  @Input() blocksEnabled: boolean;
  @Input() deleteDisabled = true;
  @Output() addEvent = new EventEmitter<void>();
  @Output() deleteEvent = new EventEmitter<void>();
  @Output() duplicateEvent = new EventEmitter<void>();

  constructor(
    protected injector: Injector,
    protected _entityService: ExtractCriteriaAddService
  ) {
    super(injector, RESERVED_START_COLUMNS.concat(['name']).concat(RESERVED_END_COLUMNS), ExtractCriteriaAddModel, _entityService, undefined);
    this.logPrefix = '[extract-criteria-add-table]';
  }

  get sortActive(): string {
    return 'rankOrder';
  }

  get sortDirection(): SortDirection {
    return 'asc';
  }

  ngOnInit() {
    // default properties
    this.subTable = true;
    this.showPaginator = false;
    this.forceNoInlineEdition = true;
    this.checkBoxSelection = true;
    this.toggleOnlySelection = true;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;

    // Listen sort change
    this.registerSubscription(
      this.onSort.subscribe(() => this.markAsLoading()) // prevent selection change on sort
    );
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.ready().then(() => {
      this.resetFilter();
    });
  }

  // Make it public
  public markForCheck() {
    super.markForCheck();
  }
}
