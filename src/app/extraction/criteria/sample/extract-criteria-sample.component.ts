import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import {
  ExtractCriteriaSample,
  IExtractCriteriaSample,
  sampleCriteriaDefinition,
  sampleFilters,
} from '@app/extraction/criteria/sample/extract-criteria-sample.model';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';

@Component({
  selector: 'app-extract-criteria-sample-component',
  templateUrl: './extract-criteria-sample.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaSampleComponent extends ExtractCriteriaComponent<ExtractCriteriaSample, IExtractCriteriaSample> {
  criteriaDefinition = sampleCriteriaDefinition;
  possibleFilters = sampleFilters;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaSample);
    this.logPrefix = '[extract-criteria-sample]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaSample): (keyof IExtractCriteriaSample)[] {
    return ExtractCriteriaSample.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaSample): any {
    return ExtractCriteriaSample.defaultValue(property);
  }

  addFilter(event: UIEvent, filter: any) {
    if (['havingMeasurement', 'havingMeasurementFile'].includes(filter)) {
      // Default value: true
      this.form.get(filter).setValue(true);
      this.markAsDirty();
    }
    super.addFilter(event, filter);
  }
}
