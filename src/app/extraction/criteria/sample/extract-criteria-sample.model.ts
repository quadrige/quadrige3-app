import { isEmptyArray, isNil, isNotNil } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria, IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import {
  dataQualityFlagI18nLabelPrefix,
  ExtractCriteriaQualityFlag,
  IExtractCriteriaQualityFlag,
  qualityFlagI18nLabels,
} from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.model';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { Utils } from '@app/shared/utils';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSample) =>
  ExtractUtils.getValue<IExtractCriteriaSample>(criterias, sampleCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSample, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaSample>(criterias, sampleCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSample) =>
  ExtractUtils.getValues<IExtractCriteriaSample>(criterias, sampleCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSample, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaSample>(criterias, sampleCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSample) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaSample>(criterias, sampleCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaSample,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaSample>(criterias, sampleCriteriaDefinition, property, referentialFilter);
const getTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSample) =>
  ExtractUtils.getTextFilter(criterias, sampleCriteriaDefinition, property);
const setTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSample, value: ITextFilterValue) =>
  ExtractUtils.setTextFilter(criterias, sampleCriteriaDefinition, property, value);

export const sampleFilters: (keyof IExtractCriteriaSample)[] = [
  'matrixFilter',
  'label',
  'comments',
  'recorderDepartmentFilter',
  'taxonNameFilter',
  'taxonGroupFilter',
  'controlled',
  'validated',
  'qualifications',
  'havingMeasurement',
  'havingMeasurementFile',
];

export interface IExtractCriteriaSample extends IExtractCriteria {
  label: ITextFilterValue;
  comments: ITextFilterValue;
  matrixFilter: IReferentialFilterCriteria<number>;
  recorderDepartmentFilter: IReferentialFilterCriteria<number>;
  taxonNameFilter: IReferentialFilterCriteria<number>;
  childrenTaxonName: boolean;
  taxonGroupFilter: IReferentialFilterCriteria<number>;
  childrenTaxonGroup: boolean;
  controlled: boolean;
  validated: boolean;
  qualifications: IExtractCriteriaQualityFlag;
  havingMeasurement: boolean;
  havingMeasurementFile: boolean;
}

export const sampleCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaSample> = {
  label: {
    i18nLabel: 'DATA.SAMPLE.LABEL',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_LABEL',
  },
  comments: {
    i18nLabel: 'DATA.SAMPLE.COMMENTS',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_COMMENT',
    valueOperatorType: 'TEXT_CONTAINS',
  },
  matrixFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.MATRIX',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLE_MATRIX_ID',
    valuesOperatorType: 'MATRIX_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_MATRIX_NAME',
  },
  recorderDepartmentFilter: {
    i18nLabel: 'DATA.SAMPLE.RECORDER_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_LABEL',
  },
  taxonNameFilter: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.SUPPORT_TAXON_NAME.SAMPLE',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID',
    valuesOperatorType: 'TAXON_NAME_IN',
  },
  childrenTaxonName: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CHILDREN_TAXON',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_TAXON_NAME_CHILDREN',
    valueOperatorType: 'BOOLEAN',
  },
  taxonGroupFilter: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.SUPPORT_TAXON_GROUP.SAMPLE',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID',
    valuesOperatorType: 'TAXON_GROUP_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME',
  },
  childrenTaxonGroup: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CHILDREN_TAXON_GROUP',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_TAXON_GROUP_CHILDREN',
    valueOperatorType: 'BOOLEAN',
  },
  controlled: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CONTROLLED',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_CONTROL',
    valueOperatorType: 'BOOLEAN',
  },
  validated: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.VALIDATED',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_VALIDATION',
    valueOperatorType: 'BOOLEAN',
  },
  qualifications: {
    i18nLabel: dataQualityFlagI18nLabelPrefix + 'TITLE',
    i18nLabelPrefix: dataQualityFlagI18nLabelPrefix,
    valuesI18Labels: qualityFlagI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLE_QUALIFICATION',
  },
  havingMeasurement: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.HAVING_MEASUREMENT.SAMPLE',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT',
    valueOperatorType: 'BOOLEAN',
  },
  havingMeasurementFile: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.HAVING_MEASUREMENT_FILE.SAMPLE',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT_FILE',
    valueOperatorType: 'BOOLEAN',
  },
};

export class ExtractCriteriaSample implements IExtractCriteriaSample {
  id = null;
  label = TextFilterValue.default();
  comments = TextFilterValue.default('TEXT_CONTAINS');
  matrixFilter: IntReferentialFilterCriteria;
  recorderDepartmentFilter: IntReferentialFilterCriteria;
  taxonNameFilter: IntReferentialFilterCriteria;
  childrenTaxonName = false;
  taxonGroupFilter: IntReferentialFilterCriteria;
  childrenTaxonGroup = false;
  controlled = null;
  validated = null;
  qualifications = ExtractCriteriaQualityFlag.default();
  havingMeasurement = null;
  havingMeasurementFile = null;
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaSample): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaSample.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaSample, c2: ExtractCriteriaSample): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        TextFilterValue.equals(c1.label, c2.label) &&
        TextFilterValue.equals(c1.comments, c2.comments) &&
        Utils.equals(c1.matrixFilter, c2.matrixFilter) &&
        Utils.equals(c1.recorderDepartmentFilter, c2.recorderDepartmentFilter) &&
        Utils.equals(c1.taxonNameFilter, c2.taxonNameFilter) &&
        c1.childrenTaxonName === c2.childrenTaxonName &&
        Utils.equals(c1.taxonGroupFilter, c2.taxonGroupFilter) &&
        c1.childrenTaxonGroup === c2.childrenTaxonGroup &&
        c1.controlled === c2.controlled &&
        c1.validated === c2.validated &&
        ExtractCriteriaQualityFlag.equals(c1.qualifications, c2.qualifications) &&
        c1.havingMeasurement === c2.havingMeasurement &&
        c1.havingMeasurementFile === c2.havingMeasurementFile)
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaSample {
    return {
      id,
      label: getTextFilter(criterias, 'label'),
      comments: getTextFilter(criterias, 'comments'),
      matrixFilter: getReferentialFilterCriteria(criterias, 'matrixFilter'),
      recorderDepartmentFilter: getReferentialFilterCriteria(criterias, 'recorderDepartmentFilter'),
      taxonNameFilter: getReferentialFilterCriteria(criterias, 'taxonNameFilter'),
      childrenTaxonName: Utils.fromBooleanString(getValue(criterias, 'childrenTaxonName').value, false),
      taxonGroupFilter: getReferentialFilterCriteria(criterias, 'taxonGroupFilter'),
      childrenTaxonGroup: Utils.fromBooleanString(getValue(criterias, 'childrenTaxonGroup').value, false),
      controlled: Utils.fromOptionalBooleanString(getValue(criterias, 'controlled').value),
      validated: Utils.fromOptionalBooleanString(getValue(criterias, 'validated').value),
      qualifications: ExtractCriteriaQualityFlag.fromValues(getValues(criterias, 'qualifications').values),
      havingMeasurement: Utils.fromOptionalBooleanString(getValue(criterias, 'havingMeasurement').value),
      havingMeasurementFile: Utils.fromOptionalBooleanString(getValue(criterias, 'havingMeasurementFile').value),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaSample, opts: IMergeCriteriaOptions) {
    setTextFilter(criterias, 'label', value?.label);
    setTextFilter(criterias, 'comments', value?.comments);
    setReferentialFilterCriteria(criterias, 'matrixFilter', value?.matrixFilter);
    setReferentialFilterCriteria(criterias, 'recorderDepartmentFilter', value?.recorderDepartmentFilter);
    setReferentialFilterCriteria(criterias, 'taxonNameFilter', value?.taxonNameFilter);
    setValue(criterias, 'childrenTaxonName', {
      value: BaseFilterUtils.isCriteriaEmpty(value?.taxonNameFilter, true) ? undefined : Utils.toBooleanString(value?.childrenTaxonName),
    });
    setReferentialFilterCriteria(criterias, 'taxonGroupFilter', value?.taxonGroupFilter);
    setValue(criterias, 'childrenTaxonGroup', {
      value: BaseFilterUtils.isCriteriaEmpty(value?.taxonGroupFilter, true) ? undefined : Utils.toBooleanString(value?.childrenTaxonGroup),
    });
    setValue(criterias, 'controlled', {
      value: isNotNil(value?.controlled) ? Utils.toBooleanString(value?.controlled) : null,
    });
    setValue(criterias, 'validated', {
      value: isNotNil(value?.validated) ? Utils.toBooleanString(value?.validated) : null,
    });
    setValues(criterias, 'qualifications', { values: ExtractCriteriaQualityFlag.toValues(value?.qualifications, opts.defaultIfEmpty) });
    setValue(criterias, 'havingMeasurement', {
      value: isNotNil(value?.havingMeasurement) ? Utils.toBooleanString(value?.havingMeasurement) : null,
    });
    setValue(criterias, 'havingMeasurementFile', {
      value: isNotNil(value?.havingMeasurementFile) ? Utils.toBooleanString(value?.havingMeasurementFile) : null,
    });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaSample): (keyof IExtractCriteriaSample)[] {
    const properties: (keyof IExtractCriteriaSample)[] = [];
    if (criteria) {
      if (!TextFilterValue.isEmpty(criteria.label)) properties.push('label');
      if (!TextFilterValue.isEmpty(criteria.comments)) properties.push('comments');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.matrixFilter, true)) properties.push('matrixFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.recorderDepartmentFilter, true)) properties.push('recorderDepartmentFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.taxonNameFilter, true)) properties.push('taxonNameFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.taxonGroupFilter, true)) properties.push('taxonGroupFilter');
      if (isNotNil(criteria.controlled)) properties.push('controlled');
      if (isNotNil(criteria.validated)) properties.push('validated');
      if (!ExtractCriteriaQualityFlag.isEmptyOrDefault(criteria.qualifications)) properties.push('qualifications');
      if (isNotNil(criteria.havingMeasurement)) properties.push('havingMeasurement');
      if (isNotNil(criteria.havingMeasurementFile)) properties.push('havingMeasurementFile');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaSample): any {
    switch (property) {
      case 'label':
        return TextFilterValue.default();
      case 'comments':
        return TextFilterValue.default('TEXT_CONTAINS');
      case 'qualifications':
        return ExtractCriteriaQualityFlag.default();
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaSample {
    const target = new ExtractCriteriaSample();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.label = source.label || TextFilterValue.default();
    this.comments = source.comments || TextFilterValue.default('TEXT_CONTAINS');
    this.matrixFilter = IntReferentialFilterCriteria.fromObject(source.matrixFilter || {});
    this.recorderDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.recorderDepartmentFilter || {});
    this.taxonNameFilter = IntReferentialFilterCriteria.fromObject(source.taxonNameFilter || {});
    this.childrenTaxonName = source.childrenTaxonName;
    this.taxonGroupFilter = IntReferentialFilterCriteria.fromObject(source.taxonGroupFilter || {});
    this.childrenTaxonGroup = source.childrenTaxonGroup;
    this.controlled = source.controlled;
    this.validated = source.validated;
    this.qualifications = ExtractCriteriaQualityFlag.fromObject(source.qualifications);
    this.havingMeasurement = source.havingMeasurement;
    this.havingMeasurementFile = source.havingMeasurementFile;
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target = { ...this };
    target.matrixFilter = this.matrixFilter?.asObject();
    target.recorderDepartmentFilter = this.recorderDepartmentFilter?.asObject();
    target.taxonNameFilter = this.taxonNameFilter?.asObject();
    target.taxonGroupFilter = this.taxonGroupFilter?.asObject();
    target.qualifications = this.qualifications?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
