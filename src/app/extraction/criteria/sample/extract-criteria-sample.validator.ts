import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaSample, IExtractCriteriaSample } from '@app/extraction/criteria/sample/extract-criteria-sample.model';
import { toBoolean } from '@sumaris-net/ngx-components';
import { ExtractCriteriaQualityFlagValidatorService } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.validator';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { TextFilterValidator } from '@app/referential/component/text-filter-form-field.component';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaSampleValidatorService extends BaseValidatorService<ExtractCriteriaSample> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected qualityFlagValidator: ExtractCriteriaQualityFlagValidatorService,
    protected textFilterValidator: TextFilterValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaSample, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaSample, any>>{
      id: [data?.id || null],
      label: this.textFilterValidator.getFormGroup(data?.label),
      comments: this.textFilterValidator.getFormGroup(data?.comments),
      matrixFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.matrixFilter),
      recorderDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.recorderDepartmentFilter),
      taxonNameFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.taxonNameFilter),
      childrenTaxonName: [toBoolean(data?.childrenTaxonName, false)],
      taxonGroupFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.taxonGroupFilter),
      childrenTaxonGroup: [toBoolean(data?.childrenTaxonGroup, false)],
      controlled: [toBoolean(data?.controlled)],
      validated: [toBoolean(data?.validated)],
      qualifications: this.qualityFlagValidator.getFormGroup(data?.qualifications),
      havingMeasurement: [toBoolean(data?.havingMeasurement)],
      havingMeasurementFile: [toBoolean(data?.havingMeasurementFile)],
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
