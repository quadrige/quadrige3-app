import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import {
  ExtractCriteriaMeasurement,
  IExtractCriteriaMeasurement,
  measurementCriteriaDefinition,
  measurementFilters,
} from '@app/extraction/criteria/measurement/extract-criteria-measurement.model';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';
import { IProgramFilterCriteria } from '@app/referential/program-strategy/program/filter/program.filter.model';

@Component({
  selector: 'app-extract-criteria-measurement-component',
  templateUrl: './extract-criteria-measurement.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaMeasurementComponent extends ExtractCriteriaComponent<ExtractCriteriaMeasurement, IExtractCriteriaMeasurement> {
  @Input() programAddCriteria: Partial<IProgramFilterCriteria>;

  criteriaDefinition = measurementCriteriaDefinition;
  possibleFilters = measurementFilters;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaMeasurement);
    this.logPrefix = '[extract-criteria-measurement]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaMeasurement): (keyof IExtractCriteriaMeasurement)[] {
    return ExtractCriteriaMeasurement.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaMeasurement): any {
    return ExtractCriteriaMeasurement.defaultValue(property);
  }
}
