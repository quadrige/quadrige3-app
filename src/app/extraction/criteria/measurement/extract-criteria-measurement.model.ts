import { isEmptyArray, isNil, isNotNil } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import {
  dataQualityFlagI18nLabelPrefix,
  ExtractCriteriaQualityFlag,
  IExtractCriteriaQualityFlag,
  qualityFlagI18nLabels,
} from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.model';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { Utils } from '@app/shared/utils';
import {
  ExtractCriteriaMeasurementType,
  IExtractCriteriaMeasurementType,
  measurementTypeI18nLabels,
  measurementTypeI18nPrefix,
} from '@app/extraction/criteria/measurement-type/extract-criteria-measurement-type.model';
import {
  acquisitionLevelI18nLabels,
  acquisitionLevelI18nPrefix,
  ExtractCriteriaAcquisitionLevel,
  IExtractCriteriaAcquisitionLevel,
} from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.model';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMeasurement) =>
  ExtractUtils.getValue<IExtractCriteriaMeasurement>(criterias, measurementCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMeasurement, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaMeasurement>(criterias, measurementCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMeasurement) =>
  ExtractUtils.getValues<IExtractCriteriaMeasurement>(criterias, measurementCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMeasurement, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaMeasurement>(criterias, measurementCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMeasurement) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaMeasurement>(criterias, measurementCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaMeasurement,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaMeasurement>(criterias, measurementCriteriaDefinition, property, referentialFilter);

export const measurementFilters: (keyof IExtractCriteriaMeasurement)[] = [
  'measurementType',
  'parameterGroupFilter',
  'parameterFilter',
  'matrixFilter',
  'fractionFilter',
  'methodFilter',
  'unitFilter',
  'pmfmuFilter',
  'taxonNameFilter',
  'taxonGroupFilter',
  'recorderDepartmentFilter',
  'analysisDepartmentFilter',
  'analysisInstrumentFilter',
  'acquisitionLevel',
  'controlled',
  'validated',
  'qualifications',
];

export interface IExtractCriteriaMeasurement extends IExtractCriteria {
  measurementType?: IExtractCriteriaMeasurementType;
  acquisitionLevel?: IExtractCriteriaAcquisitionLevel;
  parameterGroupFilter: IReferentialFilterCriteria<number>;
  childrenParameterGroup: boolean;
  parameterFilter: IReferentialFilterCriteria<string>;
  matrixFilter: IReferentialFilterCriteria<number>;
  fractionFilter: IReferentialFilterCriteria<number>;
  methodFilter: IReferentialFilterCriteria<number>;
  unitFilter: IReferentialFilterCriteria<number>;
  pmfmuFilter: IReferentialFilterCriteria<number>;
  taxonNameFilter: IReferentialFilterCriteria<number>;
  childrenTaxonName: boolean;
  taxonGroupFilter: IReferentialFilterCriteria<number>;
  childrenTaxonGroup: boolean;
  recorderDepartmentFilter: IReferentialFilterCriteria<number>;
  analysisDepartmentFilter: IReferentialFilterCriteria<number>;
  analysisInstrumentFilter: IReferentialFilterCriteria<number>;
  controlled: boolean;
  validated: boolean;
  qualifications: IExtractCriteriaQualityFlag;
}

export const measurementCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaMeasurement> = {
  measurementType: {
    i18nLabel: measurementTypeI18nPrefix + 'TITLE',
    i18nLabelPrefix: measurementTypeI18nPrefix,
    valuesI18Labels: measurementTypeI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_TYPE',
  },
  acquisitionLevel: {
    i18nLabel: acquisitionLevelI18nPrefix + 'TITLE',
    i18nLabelPrefix: acquisitionLevelI18nPrefix,
    valuesI18Labels: acquisitionLevelI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL',
  },
  parameterGroupFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.PARAMETER_GROUP',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID',
    valuesOperatorType: 'PARAMETER_GROUP_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_NAME',
  },
  childrenParameterGroup: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CHILDREN_PARAMETER_GROUP',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_CHILDREN',
    valueOperatorType: 'BOOLEAN',
  },
  parameterFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.PARAMETER',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID',
    valuesOperatorType: 'PARAMETER_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_NAME',
  },
  matrixFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.MATRIX',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_MATRIX_ID',
    valuesOperatorType: 'MATRIX_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_MATRIX_NAME',
  },
  fractionFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.FRACTION',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_FRACTION_ID',
    valuesOperatorType: 'FRACTION_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_FRACTION_NAME',
  },
  methodFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.METHOD',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_METHOD_ID',
    valuesOperatorType: 'METHOD_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_METHOD_NAME',
  },
  unitFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.UNIT',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_UNIT_ID',
    valuesOperatorType: 'UNIT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_UNIT_NAME',
  },
  pmfmuFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.PMFMU',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_PMFMU_ID',
    valuesOperatorType: 'PMFMU_IN',
  },
  taxonNameFilter: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.SUPPORT_TAXON_NAME.RESULT',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID',
    valuesOperatorType: 'TAXON_NAME_IN',
  },
  childrenTaxonName: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CHILDREN_TAXON',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN',
    valueOperatorType: 'BOOLEAN',
  },
  taxonGroupFilter: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.SUPPORT_TAXON_GROUP.RESULT',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID',
    valuesOperatorType: 'TAXON_GROUP_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME',
  },
  childrenTaxonGroup: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CHILDREN_TAXON_GROUP',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN',
    valueOperatorType: 'BOOLEAN',
  },
  recorderDepartmentFilter: {
    i18nLabel: 'DATA.MEASUREMENT.RECORDER_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_LABEL',
  },
  analysisDepartmentFilter: {
    i18nLabel: 'DATA.MEASUREMENT.ANALYSIS_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_LABEL',
  },
  analysisInstrumentFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.ANALYSIS_INSTRUMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_ID',
    valuesOperatorType: 'ANALYSIS_INSTRUMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_NAME',
  },
  controlled: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CONTROLLED',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_CONTROL',
    valueOperatorType: 'BOOLEAN',
  },
  validated: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.VALIDATED',
    valueCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_VALIDATION',
    valueOperatorType: 'BOOLEAN',
  },
  qualifications: {
    i18nLabel: dataQualityFlagI18nLabelPrefix + 'TITLE',
    i18nLabelPrefix: dataQualityFlagI18nLabelPrefix,
    valuesI18Labels: qualityFlagI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_MEASUREMENT_QUALIFICATION',
  },
};

export class ExtractCriteriaMeasurement implements IExtractCriteriaMeasurement {
  id = null;
  measurementType = ExtractCriteriaMeasurementType.default();
  acquisitionLevel = ExtractCriteriaAcquisitionLevel.default();
  parameterGroupFilter: IntReferentialFilterCriteria;
  childrenParameterGroup = false;
  parameterFilter: StrReferentialFilterCriteria;
  matrixFilter: IntReferentialFilterCriteria;
  fractionFilter: IntReferentialFilterCriteria;
  methodFilter: IntReferentialFilterCriteria;
  unitFilter: IntReferentialFilterCriteria;
  pmfmuFilter: IntReferentialFilterCriteria;
  taxonNameFilter: IntReferentialFilterCriteria;
  childrenTaxonName = false;
  taxonGroupFilter: IntReferentialFilterCriteria;
  childrenTaxonGroup = false;
  recorderDepartmentFilter: IntReferentialFilterCriteria;
  analysisDepartmentFilter: IntReferentialFilterCriteria;
  analysisInstrumentFilter: IntReferentialFilterCriteria;
  controlled = null;
  validated = null;
  qualifications = ExtractCriteriaQualityFlag.default();
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaMeasurement): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaMeasurement.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaMeasurement, c2: ExtractCriteriaMeasurement): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        ExtractCriteriaMeasurementType.equals(c1.measurementType, c2.measurementType) &&
        ExtractCriteriaAcquisitionLevel.equals(c1.acquisitionLevel, c2.acquisitionLevel) &&
        Utils.equals(c1.parameterGroupFilter, c2.parameterGroupFilter) &&
        c1.childrenParameterGroup === c2.childrenParameterGroup &&
        Utils.equals(c1.parameterFilter, c2.parameterFilter) &&
        Utils.equals(c1.matrixFilter, c2.matrixFilter) &&
        Utils.equals(c1.fractionFilter, c2.fractionFilter) &&
        Utils.equals(c1.methodFilter, c2.methodFilter) &&
        Utils.equals(c1.unitFilter, c2.unitFilter) &&
        Utils.equals(c1.pmfmuFilter, c2.pmfmuFilter) &&
        Utils.equals(c1.taxonNameFilter, c2.taxonNameFilter) &&
        c1.childrenTaxonName === c2.childrenTaxonName &&
        Utils.equals(c1.taxonGroupFilter, c2.taxonGroupFilter) &&
        c1.childrenTaxonGroup === c2.childrenTaxonGroup &&
        Utils.equals(c1.recorderDepartmentFilter, c2.recorderDepartmentFilter) &&
        Utils.equals(c1.analysisDepartmentFilter, c2.analysisDepartmentFilter) &&
        Utils.equals(c1.analysisInstrumentFilter, c2.analysisInstrumentFilter) &&
        c1.controlled === c2.controlled &&
        c1.validated === c2.validated &&
        ExtractCriteriaQualityFlag.equals(c1.qualifications, c2.qualifications))
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaMeasurement {
    return {
      id,
      measurementType: ExtractCriteriaMeasurementType.fromValues(getValues(criterias, 'measurementType').values),
      acquisitionLevel: ExtractCriteriaAcquisitionLevel.fromValues(getValues(criterias, 'acquisitionLevel').values),
      parameterGroupFilter: getReferentialFilterCriteria(criterias, 'parameterGroupFilter'),
      childrenParameterGroup: Utils.fromBooleanString(getValue(criterias, 'childrenParameterGroup').value, false),
      parameterFilter: getReferentialFilterCriteria(criterias, 'parameterFilter'),
      matrixFilter: getReferentialFilterCriteria(criterias, 'matrixFilter'),
      fractionFilter: getReferentialFilterCriteria(criterias, 'fractionFilter'),
      methodFilter: getReferentialFilterCriteria(criterias, 'methodFilter'),
      unitFilter: getReferentialFilterCriteria(criterias, 'unitFilter'),
      pmfmuFilter: getReferentialFilterCriteria(criterias, 'pmfmuFilter'),
      taxonNameFilter: getReferentialFilterCriteria(criterias, 'taxonNameFilter'),
      childrenTaxonName: Utils.fromBooleanString(getValue(criterias, 'childrenTaxonName').value, false),
      taxonGroupFilter: getReferentialFilterCriteria(criterias, 'taxonGroupFilter'),
      childrenTaxonGroup: Utils.fromBooleanString(getValue(criterias, 'childrenTaxonGroup').value, false),
      recorderDepartmentFilter: getReferentialFilterCriteria(criterias, 'recorderDepartmentFilter'),
      analysisDepartmentFilter: getReferentialFilterCriteria(criterias, 'analysisDepartmentFilter'),
      analysisInstrumentFilter: getReferentialFilterCriteria(criterias, 'analysisInstrumentFilter'),
      controlled: Utils.fromOptionalBooleanString(getValue(criterias, 'controlled').value),
      validated: Utils.fromOptionalBooleanString(getValue(criterias, 'validated').value),
      qualifications: ExtractCriteriaQualityFlag.fromValues(getValues(criterias, 'qualifications').values),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaMeasurement, opts: IMergeCriteriaOptions) {
    setValues(criterias, 'measurementType', { values: ExtractCriteriaMeasurementType.toValues(value?.measurementType, opts.defaultIfEmpty) });
    setValues(criterias, 'acquisitionLevel', { values: ExtractCriteriaAcquisitionLevel.toValues(value?.acquisitionLevel, opts.defaultIfEmpty) });
    setReferentialFilterCriteria(criterias, 'parameterGroupFilter', value?.parameterGroupFilter);
    setValue(criterias, 'childrenParameterGroup', {
      value: BaseFilterUtils.isCriteriaEmpty(value?.parameterGroupFilter, true) ? undefined : Utils.toBooleanString(value?.childrenParameterGroup),
    });
    setReferentialFilterCriteria(criterias, 'parameterFilter', value?.parameterFilter);
    setReferentialFilterCriteria(criterias, 'matrixFilter', value?.matrixFilter);
    setReferentialFilterCriteria(criterias, 'fractionFilter', value?.fractionFilter);
    setReferentialFilterCriteria(criterias, 'methodFilter', value?.methodFilter);
    setReferentialFilterCriteria(criterias, 'unitFilter', value?.unitFilter);
    setReferentialFilterCriteria(criterias, 'pmfmuFilter', value?.pmfmuFilter);
    setReferentialFilterCriteria(criterias, 'taxonNameFilter', value?.taxonNameFilter);
    setValue(criterias, 'childrenTaxonName', {
      value: BaseFilterUtils.isCriteriaEmpty(value?.taxonNameFilter, true) ? undefined : Utils.toBooleanString(value?.childrenTaxonName),
    });
    setReferentialFilterCriteria(criterias, 'taxonGroupFilter', value?.taxonGroupFilter);
    setValue(criterias, 'childrenTaxonGroup', {
      value: BaseFilterUtils.isCriteriaEmpty(value?.taxonGroupFilter, true) ? undefined : Utils.toBooleanString(value?.childrenTaxonGroup),
    });
    setReferentialFilterCriteria(criterias, 'recorderDepartmentFilter', value?.recorderDepartmentFilter);
    setReferentialFilterCriteria(criterias, 'analysisDepartmentFilter', value?.analysisDepartmentFilter);
    setReferentialFilterCriteria(criterias, 'analysisInstrumentFilter', value?.analysisInstrumentFilter);
    setValue(criterias, 'controlled', {
      value: isNotNil(value?.controlled) ? Utils.toBooleanString(value?.controlled) : null,
    });
    setValue(criterias, 'validated', {
      value: isNotNil(value?.validated) ? Utils.toBooleanString(value?.validated) : null,
    });
    setValues(criterias, 'qualifications', { values: ExtractCriteriaQualityFlag.toValues(value?.qualifications, opts.defaultIfEmpty) });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaMeasurement): (keyof IExtractCriteriaMeasurement)[] {
    const properties: (keyof IExtractCriteriaMeasurement)[] = [];
    if (criteria) {
      if (!ExtractCriteriaMeasurementType.isEmptyOrDefault(criteria.measurementType)) properties.push('measurementType');
      if (!ExtractCriteriaAcquisitionLevel.isEmptyOrDefault(criteria.acquisitionLevel)) properties.push('acquisitionLevel');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.parameterGroupFilter, true)) properties.push('parameterGroupFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.parameterFilter, true)) properties.push('parameterFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.matrixFilter, true)) properties.push('matrixFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.fractionFilter, true)) properties.push('fractionFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.methodFilter, true)) properties.push('methodFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.unitFilter, true)) properties.push('unitFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.pmfmuFilter, true)) properties.push('pmfmuFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.taxonNameFilter, true)) properties.push('taxonNameFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.taxonGroupFilter, true)) properties.push('taxonGroupFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.recorderDepartmentFilter, true)) properties.push('recorderDepartmentFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.analysisDepartmentFilter, true)) properties.push('analysisDepartmentFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.analysisInstrumentFilter, true)) properties.push('analysisInstrumentFilter');
      if (isNotNil(criteria.controlled)) properties.push('controlled');
      if (isNotNil(criteria.validated)) properties.push('validated');
      if (!ExtractCriteriaQualityFlag.isEmptyOrDefault(criteria.qualifications)) properties.push('qualifications');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaMeasurement): any {
    switch (property) {
      case 'measurementType':
        return ExtractCriteriaMeasurementType.default();
      case 'acquisitionLevel':
        return ExtractCriteriaAcquisitionLevel.default();
      case 'qualifications':
        return ExtractCriteriaQualityFlag.default();
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaMeasurement {
    const target = new ExtractCriteriaMeasurement();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.measurementType = ExtractCriteriaMeasurementType.fromObject(source.measurementType);
    this.acquisitionLevel = ExtractCriteriaAcquisitionLevel.fromObject(source.acquisitionLevel);
    this.parameterGroupFilter = IntReferentialFilterCriteria.fromObject(source.parameterGroupFilter || {});
    this.childrenParameterGroup = source.childrenParameterGroup;
    this.parameterFilter = StrReferentialFilterCriteria.fromObject(source.parameterFilter || {});
    this.matrixFilter = IntReferentialFilterCriteria.fromObject(source.matrixFilter || {});
    this.fractionFilter = IntReferentialFilterCriteria.fromObject(source.fractionFilter || {});
    this.methodFilter = IntReferentialFilterCriteria.fromObject(source.methodFilter || {});
    this.unitFilter = IntReferentialFilterCriteria.fromObject(source.unitFilter || {});
    this.pmfmuFilter = IntReferentialFilterCriteria.fromObject(source.pmfmuFilter || {});
    this.taxonNameFilter = IntReferentialFilterCriteria.fromObject(source.taxonNameFilter || {});
    this.childrenTaxonName = source.childrenTaxonName;
    this.taxonGroupFilter = IntReferentialFilterCriteria.fromObject(source.taxonGroupFilter || {});
    this.childrenTaxonGroup = source.childrenTaxonGroup;
    this.recorderDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.recorderDepartmentFilter || {});
    this.analysisDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.analysisDepartmentFilter || {});
    this.analysisInstrumentFilter = IntReferentialFilterCriteria.fromObject(source.analysisInstrumentFilter || {});
    this.controlled = source.controlled;
    this.validated = source.validated;
    this.qualifications = ExtractCriteriaQualityFlag.fromObject(source.qualifications);
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target = { ...this };
    target.measurementType = this.measurementType?.asObject() as any;
    target.acquisitionLevel = this.acquisitionLevel?.asObject() as any;
    target.parameterGroupFilter = this.parameterGroupFilter?.asObject();
    target.parameterFilter = this.parameterFilter?.asObject();
    target.matrixFilter = this.matrixFilter?.asObject();
    target.fractionFilter = this.fractionFilter?.asObject();
    target.methodFilter = this.methodFilter?.asObject();
    target.unitFilter = this.unitFilter?.asObject();
    target.pmfmuFilter = this.pmfmuFilter?.asObject();
    target.taxonNameFilter = this.taxonNameFilter?.asObject();
    target.taxonGroupFilter = this.taxonGroupFilter?.asObject();
    target.recorderDepartmentFilter = this.recorderDepartmentFilter?.asObject();
    target.analysisDepartmentFilter = this.analysisDepartmentFilter?.asObject();
    target.analysisInstrumentFilter = this.analysisInstrumentFilter?.asObject();
    target.qualifications = this.qualifications?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
