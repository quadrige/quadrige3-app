import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaMeasurement, IExtractCriteriaMeasurement } from '@app/extraction/criteria/measurement/extract-criteria-measurement.model';
import { toBoolean } from '@sumaris-net/ngx-components';
import { ExtractCriteriaQualityFlagValidatorService } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.validator';
import { ExtractCriteriaMeasurementTypeValidatorService } from '@app/extraction/criteria/measurement-type/extract-criteria-measurement-type.validator';
import { ExtractCriteriaAcquisitionLevelValidatorService } from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.validator';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaMeasurementValidatorService extends BaseValidatorService<ExtractCriteriaMeasurement> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected measurementTypeValidator: ExtractCriteriaMeasurementTypeValidatorService,
    protected acquisitionLevelValidator: ExtractCriteriaAcquisitionLevelValidatorService,
    protected qualityFlagValidator: ExtractCriteriaQualityFlagValidatorService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaMeasurement, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaMeasurement, any>>{
      id: [data?.id || null],
      measurementType: this.measurementTypeValidator.getFormGroup(data?.measurementType),
      acquisitionLevel: this.acquisitionLevelValidator.getFormGroup(data?.acquisitionLevel),
      parameterGroupFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.parameterGroupFilter),
      childrenParameterGroup: [toBoolean(data?.childrenParameterGroup, false)],
      parameterFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.parameterFilter),
      matrixFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.matrixFilter),
      fractionFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.fractionFilter),
      methodFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.methodFilter),
      unitFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.unitFilter),
      pmfmuFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.pmfmuFilter),
      taxonNameFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.taxonNameFilter),
      childrenTaxonName: [toBoolean(data?.childrenTaxonName, false)],
      taxonGroupFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.taxonGroupFilter),
      childrenTaxonGroup: [toBoolean(data?.childrenTaxonGroup, false)],
      recorderDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.recorderDepartmentFilter),
      analysisDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.analysisDepartmentFilter),
      analysisInstrumentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.analysisInstrumentFilter),
      controlled: [toBoolean(data?.controlled)],
      validated: [toBoolean(data?.validated)],
      qualifications: this.qualityFlagValidator.getFormGroup(data?.qualifications),
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
