import { isEmptyArray, isNil } from '@sumaris-net/ngx-components';
import { ExtractCriteriaSurvey, IExtractCriteriaSurvey } from '@app/extraction/criteria/survey/extract-criteria-survey.model';
import {
  ExtractCriteriaSamplingOperation,
  IExtractCriteriaSamplingOperation,
} from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.model';
import { ExtractCriteriaSample, IExtractCriteriaSample } from '@app/extraction/criteria/sample/extract-criteria-sample.model';
import { ExtractCriteriaMeasurement, IExtractCriteriaMeasurement } from '@app/extraction/criteria/measurement/extract-criteria-measurement.model';
import { ExtractCriteriaPhoto, IExtractCriteriaPhoto } from '@app/extraction/criteria/photo/extract-criteria-photo.model';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { ExtractSurveyPeriod } from '@app/extraction/period/extract-survey-period.model';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { ExtractCriteriaCampaign, IExtractCriteriaCampaign } from '@app/extraction/criteria/campaign/extract-criteria-campaign.model';
import { ExtractCriteriaOccasion, IExtractCriteriaOccasion } from '@app/extraction/criteria/occasion/extract-criteria-occasion.model';
import { ExtractCriteriaEvent, IExtractCriteriaEvent } from '@app/extraction/criteria/event/extract-criteria-event.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { EntityUtils } from '@app/shared/entity.utils';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMain, defaultValue?) =>
  ExtractUtils.getValue<IExtractCriteriaMain>(criterias, mainCriteriaDefinition, property, defaultValue);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMain, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaMain>(criterias, mainCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMain) =>
  ExtractUtils.getValues<IExtractCriteriaMain>(criterias, mainCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMain, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaMain>(criterias, mainCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaMain) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaMain>(criterias, mainCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaMain,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaMain>(criterias, mainCriteriaDefinition, property, referentialFilter);

export const mainFilters: (keyof IExtractCriteriaMain)[] = [
  'metaProgramFilter',
  'programFilter',
  'monitoringLocationFilter',
  'harbourFilter',
  'campaignFilter',
  'eventFilter',
  'batchFilter',
];

export interface IExtractCriteria {
  id: number;
  visibleFilters?: string[];
}

export interface IExtractCriteriaMain extends IExtractCriteria {
  periods: ExtractSurveyPeriod[];
  // Main filters
  metaProgramFilter?: IReferentialFilterCriteria<string>;
  programFilter?: IReferentialFilterCriteria<string>;
  monitoringLocationFilter?: IReferentialFilterCriteria<number>;
  harbourFilter?: IReferentialFilterCriteria<string>;
  campaignFilter?: IReferentialFilterCriteria<number>;
  eventFilter?: IReferentialFilterCriteria<number>;
  batchFilter?: IReferentialFilterCriteria<number>;
  // Data criterias
  surveyCriterias?: IExtractCriteriaSurvey[];
  samplingOperationCriterias?: IExtractCriteriaSamplingOperation[];
  sampleCriterias?: IExtractCriteriaSample[];
  measurementCriterias?: IExtractCriteriaMeasurement[];
  photoCriterias?: IExtractCriteriaPhoto[];
  campaignCriterias?: IExtractCriteriaCampaign[];
  occasionCriterias?: IExtractCriteriaOccasion[];
  eventCriterias?: IExtractCriteriaEvent[];
}

export const mainCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaMain> = {
  metaProgramFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.META_PROGRAM',
    valuesCriteriaType: 'EXTRACT_RESULT_META_PROGRAM_ID',
    valueCriteriaType: 'EXTRACT_RESULT_META_PROGRAM_NAME',
  },
  programFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.PROGRAM', // Should be updated depending on extraction type
    valuesCriteriaType: 'EXTRACT_RESULT_PROGRAM_ID',
    valueCriteriaType: 'EXTRACT_RESULT_PROGRAM_NAME',
  },
  monitoringLocationFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.MONITORING_LOCATION',
    valuesCriteriaType: 'EXTRACT_RESULT_MONITORING_LOCATION_ID',
    valueCriteriaType: 'EXTRACT_RESULT_MONITORING_LOCATION_NAME',
  },
  harbourFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.HARBOUR',
    valuesCriteriaType: 'EXTRACT_RESULT_HARBOUR_ID',
    valueCriteriaType: 'EXTRACT_RESULT_HARBOUR_NAME',
  },
  campaignFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.CAMPAIGN',
    valuesCriteriaType: 'EXTRACT_RESULT_CAMPAIGN_ID',
    valueCriteriaType: 'EXTRACT_RESULT_CAMPAIGN_NAME',
  },
  eventFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.EVENT',
    valuesCriteriaType: 'EXTRACT_RESULT_EVENT_ID',
    valueCriteriaType: 'EXTRACT_RESULT_EVENT_NAME',
  },
  batchFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.BATCH',
    valuesCriteriaType: 'EXTRACT_RESULT_BATCH_ID',
    valueCriteriaType: 'EXTRACT_RESULT_BATCH_NAME',
  },
};

export class ExtractCriteriaMain implements IExtractCriteriaMain {
  id = null;
  periods: ExtractSurveyPeriod[] = [];
  metaProgramFilter: StrReferentialFilterCriteria;
  programFilter: StrReferentialFilterCriteria;
  monitoringLocationFilter: IntReferentialFilterCriteria;
  harbourFilter: StrReferentialFilterCriteria;
  campaignFilter: IntReferentialFilterCriteria;
  eventFilter: IntReferentialFilterCriteria;
  batchFilter: IntReferentialFilterCriteria;
  surveyCriterias: ExtractCriteriaSurvey[] = [];
  samplingOperationCriterias: ExtractCriteriaSamplingOperation[] = [];
  sampleCriterias: ExtractCriteriaSample[] = [];
  measurementCriterias: ExtractCriteriaMeasurement[] = [];
  photoCriterias: ExtractCriteriaPhoto[] = [];
  campaignCriterias: ExtractCriteriaCampaign[] = [];
  occasionCriterias: ExtractCriteriaOccasion[] = [];
  eventCriterias: ExtractCriteriaEvent[] = [];
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaMain): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaMain.nonEmptyProperties(c));
  }

  static parse(criterias: FilterCriteria[]): Partial<IExtractCriteriaMain> {
    return {
      metaProgramFilter: getReferentialFilterCriteria(criterias, 'metaProgramFilter'),
      programFilter: getReferentialFilterCriteria(criterias, 'programFilter'),
      monitoringLocationFilter: getReferentialFilterCriteria(criterias, 'monitoringLocationFilter'),
      harbourFilter: getReferentialFilterCriteria(criterias, 'harbourFilter'),
      campaignFilter: getReferentialFilterCriteria(criterias, 'campaignFilter'),
      eventFilter: getReferentialFilterCriteria(criterias, 'eventFilter'),
      batchFilter: getReferentialFilterCriteria(criterias, 'batchFilter'),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaMain) {
    setReferentialFilterCriteria(criterias, 'metaProgramFilter', StrReferentialFilterCriteria.fromObject(value?.metaProgramFilter));
    setReferentialFilterCriteria(criterias, 'programFilter', StrReferentialFilterCriteria.fromObject(value?.programFilter));
    setReferentialFilterCriteria(criterias, 'monitoringLocationFilter', IntReferentialFilterCriteria.fromObject(value?.monitoringLocationFilter));
    setReferentialFilterCriteria(criterias, 'harbourFilter', StrReferentialFilterCriteria.fromObject(value?.harbourFilter));
    setReferentialFilterCriteria(criterias, 'campaignFilter', IntReferentialFilterCriteria.fromObject(value?.campaignFilter));
    setReferentialFilterCriteria(criterias, 'eventFilter', IntReferentialFilterCriteria.fromObject(value?.eventFilter));
    setReferentialFilterCriteria(criterias, 'batchFilter', IntReferentialFilterCriteria.fromObject(value?.batchFilter));
  }

  static nonEmptyProperties(criteria: IExtractCriteriaMain): (keyof IExtractCriteriaMain)[] {
    const properties: (keyof IExtractCriteriaMain)[] = [];
    if (criteria) {
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.metaProgramFilter, true)) properties.push('metaProgramFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.programFilter, true)) properties.push('programFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.monitoringLocationFilter, true)) properties.push('monitoringLocationFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.harbourFilter, true)) properties.push('harbourFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.campaignFilter, true)) properties.push('campaignFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.eventFilter, true)) properties.push('eventFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.batchFilter, true)) properties.push('batchFilter');
    }
    return properties;
  }

  static fromObject(source: any): ExtractCriteriaMain {
    const target = new ExtractCriteriaMain();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.periods = source.periods?.map(ExtractSurveyPeriod.fromObject) || [];
    this.metaProgramFilter = StrReferentialFilterCriteria.fromObject(source.metaProgramFilter || {});
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.harbourFilter = StrReferentialFilterCriteria.fromObject(source.harbourFilter || {});
    this.campaignFilter = IntReferentialFilterCriteria.fromObject(source.campaignFilter || {});
    this.eventFilter = IntReferentialFilterCriteria.fromObject(source.eventFilter || {});
    this.batchFilter = IntReferentialFilterCriteria.fromObject(source.batchFilter || {});
    this.surveyCriterias = (source.surveyCriterias || []).map(ExtractCriteriaSurvey.fromObject);
    this.samplingOperationCriterias = (source.samplingOperationCriterias || []).map(ExtractCriteriaSamplingOperation.fromObject);
    this.sampleCriterias = (source.sampleCriterias || []).map(ExtractCriteriaSample.fromObject);
    this.measurementCriterias = (source.measurementCriterias || []).map(ExtractCriteriaMeasurement.fromObject);
    this.photoCriterias = (source.photoCriterias || []).map(ExtractCriteriaPhoto.fromObject);
    this.campaignCriterias = (source.campaignCriterias || []).map(ExtractCriteriaCampaign.fromObject);
    this.occasionCriterias = (source.occasionCriterias || []).map(ExtractCriteriaOccasion.fromObject);
    this.eventCriterias = (source.eventCriterias || []).map(ExtractCriteriaEvent.fromObject);
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target: IExtractCriteriaMain = { ...this };
    target.periods = this.periods?.map((value) => EntityUtils.asMinifiedObject(value));
    target.metaProgramFilter = this.metaProgramFilter?.asObject();
    target.programFilter = this.programFilter?.asObject();
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject();
    target.harbourFilter = this.harbourFilter?.asObject();
    target.campaignFilter = this.campaignFilter?.asObject();
    target.eventFilter = this.eventFilter?.asObject();
    target.batchFilter = this.batchFilter?.asObject();
    target.surveyCriterias = this.surveyCriterias?.map((value) => value.asObject());
    target.samplingOperationCriterias = this.samplingOperationCriterias?.map((value) => value.asObject());
    target.sampleCriterias = this.sampleCriterias?.map((value) => value.asObject());
    target.measurementCriterias = this.measurementCriterias?.map((value) => value.asObject());
    target.photoCriterias = this.photoCriterias?.map((value) => value.asObject());
    target.campaignCriterias = this.campaignCriterias?.map((value) => value.asObject());
    target.occasionCriterias = this.occasionCriterias?.map((value) => value.asObject());
    target.eventCriterias = this.eventCriterias?.map((value) => value.asObject());
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
