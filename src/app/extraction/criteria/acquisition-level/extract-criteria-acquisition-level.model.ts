import { isEmptyArray, isNil, toBoolean } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { AcquisitionLevelType } from '@app/referential/model/referential.constants';
import { Utils } from '@app/shared/utils';

export interface IExtractCriteriaAcquisitionLevel {
  survey: boolean;
  samplingOperation: boolean;
  sample: boolean;
}

export type IExtractCriteriaAcquisitionLevelKey = keyof IExtractCriteriaAcquisitionLevel;

export const acquisitionLevelI18nPrefix = 'EXTRACTION.EXTRACT_FILTER.DEFINITION.ACQUISITION_LEVEL.';
export const acquisitionLevelI18nLabels: Record<IExtractCriteriaAcquisitionLevelKey, string> = {
  survey: 'SURVEY',
  samplingOperation: 'SAMPLING_OPERATION',
  sample: 'SAMPLE',
};

export class ExtractCriteriaAcquisitionLevel implements IExtractCriteriaAcquisitionLevel {
  survey = true;
  samplingOperation = true;
  sample = true;

  static default(): ExtractCriteriaAcquisitionLevel {
    return new ExtractCriteriaAcquisitionLevel();
  }

  static isEmptyOrDefault(s: IExtractCriteriaAcquisitionLevel): boolean {
    return isNil(s) || ExtractCriteriaAcquisitionLevel.equals(ExtractCriteriaAcquisitionLevel.default(), s);
  }

  static equals(s1: IExtractCriteriaAcquisitionLevel, s2: IExtractCriteriaAcquisitionLevel): boolean {
    return EntityUtils.deepEquals(s1, s2, <IExtractCriteriaAcquisitionLevelKey[]>['survey', 'samplingOperation', 'sample']);
  }

  static fromObject(source: any): ExtractCriteriaAcquisitionLevel {
    const target = new ExtractCriteriaAcquisitionLevel();
    if (source) Object.assign(target, source);
    return target;
  }

  static fromValues(values: AcquisitionLevelType[]): IExtractCriteriaAcquisitionLevel {
    if (isEmptyArray(values)) return ExtractCriteriaAcquisitionLevel.default();
    if (values.length === 1 && values[0].includes(';')) return ExtractCriteriaAcquisitionLevel.fromOldValue(values[0]);
    return {
      survey: toBoolean(values?.includes('PASS')),
      samplingOperation: toBoolean(values?.includes('PREL')),
      sample: toBoolean(values?.includes('ECHANT')),
    };
  }

  static toValues(value: IExtractCriteriaAcquisitionLevel, defaultIfEmpty: boolean): AcquisitionLevelType[] {
    if (!defaultIfEmpty && ExtractCriteriaAcquisitionLevel.isEmptyOrDefault(value)) return [];
    if (defaultIfEmpty && isNil(value)) value = ExtractCriteriaAcquisitionLevel.default();
    const values: AcquisitionLevelType[] = [];
    if (value.survey) values.push('PASS');
    if (value.samplingOperation) values.push('PREL');
    if (value.sample) values.push('ECHANT');
    return values;
  }

  private static fromOldValue(value: string): IExtractCriteriaAcquisitionLevel {
    const b = value?.split(';');
    return {
      survey: Utils.fromBooleanString(b?.[0]),
      samplingOperation: Utils.fromBooleanString(b?.[1]),
      sample: Utils.fromBooleanString(b?.[2]),
    };
  }

  asObject(): any {
    return { ...this };
  }
}
