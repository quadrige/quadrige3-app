import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { throttleTime } from 'rxjs/operators';
import {
  acquisitionLevelI18nLabels,
  ExtractCriteriaAcquisitionLevel,
} from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.model';
import { BaseForm } from '@app/shared/form/base.form';

@Component({
  selector: 'app-extract-criteria-acquisition-level-component',
  templateUrl: './extract-criteria-acquisition-level.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaAcquisitionLevelComponent extends BaseForm<ExtractCriteriaAcquisitionLevel> implements OnInit {
  @Input() i18nLabelPrefix: string;

  i18nLabels = acquisitionLevelI18nLabels;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.form.valueChanges.pipe(throttleTime(50)).subscribe((value) => {
        // if (this.debug) console.debug('[extract-criteria-acquisition-level] value', value);
        if (this.loaded && this.enabled) {
          this.markAsDirty();
        }
      })
    );
  }
}
