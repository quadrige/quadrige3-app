import { AbstractControlOptions, UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { toBoolean } from '@sumaris-net/ngx-components';
import {
  ExtractCriteriaAcquisitionLevel,
  IExtractCriteriaAcquisitionLevelKey,
} from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.model';
import { BaseGroupValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaAcquisitionLevelValidatorService extends BaseValidatorService<ExtractCriteriaAcquisitionLevel> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaAcquisitionLevel, opts?: any): { [p: string]: any } {
    return <Record<IExtractCriteriaAcquisitionLevelKey, any>>{
      survey: [toBoolean(data?.survey, true)],
      samplingOperation: [toBoolean(data?.samplingOperation, true)],
      sample: [toBoolean(data?.sample, true)],
    };
  }

  getFormGroupOptions(data?: ExtractCriteriaAcquisitionLevel, opts?: any): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredAnyOf(
          <IExtractCriteriaAcquisitionLevelKey[]>['survey', 'samplingOperation', 'sample'],
          true,
          'EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_ACQUISITION_LEVEL'
        ),
      ],
    };
  }
}
