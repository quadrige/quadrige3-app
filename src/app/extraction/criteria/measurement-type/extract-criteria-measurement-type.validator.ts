import { AbstractControlOptions, UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { toBoolean } from '@sumaris-net/ngx-components';
import { ExtractCriteriaMeasurementType, IExtractCriteriaMeasurementTypeKey } from './extract-criteria-measurement-type.model';
import { BaseGroupValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaMeasurementTypeValidatorService extends BaseValidatorService<ExtractCriteriaMeasurementType> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaMeasurementType, opts?: any): { [p: string]: any } {
    return <Record<IExtractCriteriaMeasurementTypeKey, any>>{
      measurement: [toBoolean(data?.measurement, true)],
      // taxonMeasurement: [toBoolean(data?.taxonMeasurement, true)],
      measurementFile: [toBoolean(data?.measurementFile, true)],
    };
  }

  getFormGroupOptions(data?: ExtractCriteriaMeasurementType, opts?: any): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredAnyOf(
          <IExtractCriteriaMeasurementTypeKey[]>['measurement', /*'taxonMeasurement',*/ 'measurementFile'],
          true,
          'EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_MEASUREMENT_TYPE'
        ),
      ],
    };
  }
}
