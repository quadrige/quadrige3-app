import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { throttleTime } from 'rxjs/operators';
import {
  ExtractCriteriaMeasurementType,
  measurementTypeI18nLabels,
} from '@app/extraction/criteria/measurement-type/extract-criteria-measurement-type.model';
import { BaseForm } from '@app/shared/form/base.form';

@Component({
  selector: 'app-extract-criteria-measurement-type-component',
  templateUrl: './extract-criteria-measurement-type.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaMeasurementTypeComponent extends BaseForm<ExtractCriteriaMeasurementType> implements OnInit {
  @Input() i18nLabelPrefix: string;

  i18nLabels = measurementTypeI18nLabels;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.form.valueChanges.pipe(throttleTime(50)).subscribe((value) => {
        // if (this.debug) console.debug('[extract-criteria-measurement-type] value', value);
        if (this.loaded && this.enabled) {
          this.markAsDirty();
        }
      })
    );
  }
}
