import { arraySize, isEmptyArray, isNil, toBoolean } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { Utils } from '@app/shared/utils';

export type MeasurementType = 'MEASUREMENT' | /*'TAXON_MEASUREMENT' |*/ 'MEASUREMENT_FILE';

export interface IExtractCriteriaMeasurementType {
  measurement: boolean;
  // taxonMeasurement: boolean;
  measurementFile: boolean;
}

export type IExtractCriteriaMeasurementTypeKey = keyof IExtractCriteriaMeasurementType;

export const measurementTypeI18nPrefix = 'EXTRACTION.EXTRACT_FILTER.DEFINITION.MEASUREMENT_TYPE.';
export const measurementTypeI18nLabels: Record<IExtractCriteriaMeasurementTypeKey, string> = {
  measurement: 'MEASUREMENT',
  // taxonMeasurement: 'TAXON_MEASUREMENT',
  measurementFile: 'MEASUREMENT_FILE',
};

export class ExtractCriteriaMeasurementType implements IExtractCriteriaMeasurementType {
  measurement = true;
  // taxonMeasurement = true;
  measurementFile = true;

  static default(): ExtractCriteriaMeasurementType {
    return new ExtractCriteriaMeasurementType();
  }

  static isEmptyOrDefault(s: IExtractCriteriaMeasurementType): boolean {
    return isNil(s) || ExtractCriteriaMeasurementType.equals(ExtractCriteriaMeasurementType.default(), s);
  }

  static equals(s1: IExtractCriteriaMeasurementType, s2: IExtractCriteriaMeasurementType): boolean {
    return EntityUtils.deepEquals(s1, s2, <IExtractCriteriaMeasurementTypeKey[]>['measurement', /*'taxonMeasurement',*/ 'measurementFile']);
  }

  static fromObject(source: any): ExtractCriteriaMeasurementType {
    const target = new ExtractCriteriaMeasurementType();
    if (source) Object.assign(target, source);
    return target;
  }

  static fromValues(values: MeasurementType[]): IExtractCriteriaMeasurementType {
    if (isEmptyArray(values)) return ExtractCriteriaMeasurementType.default();
    if (arraySize(values) === 1 && values[0].includes(';')) return ExtractCriteriaMeasurementType.fromOldValue(values[0]);
    return {
      measurement: toBoolean(values?.includes('MEASUREMENT')),
      // taxonMeasurement: toBoolean(values?.includes('TAXON_MEASUREMENT')),
      measurementFile: toBoolean(values?.includes('MEASUREMENT_FILE')),
    };
  }

  static toValues(value: IExtractCriteriaMeasurementType, defaultIfEmpty: boolean): MeasurementType[] {
    if (!defaultIfEmpty && ExtractCriteriaMeasurementType.isEmptyOrDefault(value)) return [];
    if (defaultIfEmpty && isNil(value)) value = ExtractCriteriaMeasurementType.default();
    const values: MeasurementType[] = [];
    if (value.measurement) values.push('MEASUREMENT');
    // if (value.taxonMeasurement) values.push('TAXON_MEASUREMENT');
    if (value.measurementFile) values.push('MEASUREMENT_FILE');
    return values;
  }

  private static fromOldValue(value: string): IExtractCriteriaMeasurementType {
    const b = value?.split(';');
    return {
      measurement: Utils.fromBooleanString(b?.[0]) || Utils.fromBooleanString(b?.[1]),
      // taxonMeasurement: Utils.fromBooleanString(b?.[1]),
      measurementFile: Utils.fromBooleanString(b?.[2]),
    };
  }

  asObject(): any {
    return { ...this };
  }
}
