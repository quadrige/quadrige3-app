import { Directive, EventEmitter, Injector, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';
import { uniqueValue, Utils } from '@app/shared/utils';
import { debounceTime } from 'rxjs/operators';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { Optional } from 'typescript-optional';
import { BaseForm } from '@app/shared/form/base.form';
import { ExtractCriteriaQualityFlagComponent } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.component';
import { ExtractCriteriaMeasurementTypeComponent } from '@app/extraction/criteria/measurement-type/extract-criteria-measurement-type.component';
import { ExtractCriteriaAcquisitionLevelComponent } from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.component';
import { ExtractCriteriaPhotoResolutionComponent } from '@app/extraction/criteria/photo-resolution/extract-criteria-photo-resolution.component';
import { transcribingFunctionsForExtraction } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Directive()
export abstract class ExtractCriteriaComponent<E, I> extends BaseForm<E> implements OnInit {
  @Output() filterRemoved = new EventEmitter<keyof I>();
  @Output() filterAdded = new EventEmitter<keyof I>();

  @ViewChildren(ReferentialFilterFormField) referentialFilterFormFields: QueryList<ReferentialFilterFormField>;
  @ViewChildren(ExtractCriteriaQualityFlagComponent) qualityFlagComponents: QueryList<ExtractCriteriaQualityFlagComponent>;
  @ViewChildren(ExtractCriteriaMeasurementTypeComponent) measurementTypeComponents: QueryList<ExtractCriteriaMeasurementTypeComponent>;
  @ViewChildren(ExtractCriteriaAcquisitionLevelComponent) acquisitionLevelComponents: QueryList<ExtractCriteriaAcquisitionLevelComponent>;
  @ViewChildren(ExtractCriteriaPhotoResolutionComponent) photoResolutionComponents: QueryList<ExtractCriteriaPhotoResolutionComponent>;

  visibleFilters: (keyof I)[] = [];
  availableFilters: (keyof I)[] = [];

  transcribingFunctionsForExtraction = transcribingFunctionsForExtraction;

  protected constructor(
    protected injector: Injector,
    protected dataType: new () => E
  ) {
    super(injector);
  }

  abstract get criteriaDefinition(): CriteriaDefinitionRecord<I>;

  abstract get possibleFilters(): (keyof I)[];

  get data(): any {
    return this.form.getRawValue();
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(this.form.valueChanges.pipe(debounceTime(50)).subscribe((value) => this.formChange(value)));
  }

  formChange(value: I) {
    // if (this.debug) console.debug(`${this.logPrefix} value`, value);
    if (this.loaded && this.enabled && this.dirty) {
      this.markAsDirty();
    }
  }

  addFilter(event: UIEvent, filter: any) {
    this.visibleFilters.push(filter);
    this.storeVisibleFilters();
    this.availableFilters.splice(this.availableFilters.indexOf(filter), 1);
    this.filterAdded.emit(filter);
    this.markForCheck();
  }

  removeFilter(event: UIEvent, filter: keyof I) {
    const valuedFilters = this.computeVisibleFiltersFromValue(this.data);
    this.clearFilter(event, filter, { emitEvent: false });
    // Restore filter position in available list (Mantis #60781)
    this.availableFilters.splice(this.possibleFilters.indexOf(filter), 0, filter);
    this.visibleFilters.splice(this.visibleFilters.indexOf(filter), 1);
    this.storeVisibleFilters();
    this.filterRemoved.emit(filter);
    if (!Utils.arrayEquals(valuedFilters, this.computeVisibleFiltersFromValue(this.data))) {
      this.markAsDirty();
    }
    this.updateValueAndValidity();
  }

  clearFilter(event: UIEvent, filter: any, opts?: { emitEvent?: boolean }) {
    Optional.ofNullable(this.defaultValue(filter)).ifPresentOrElse(
      (defaultValue) => this.form.get(filter).patchValue(defaultValue.asObject(), { emitEvent: false }),
      () => Utils.clearControlValue(event, this.form, filter, { emitEvent: false })
    );
    if (opts?.emitEvent !== false) {
      this.markAsDirty();
    }
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    this.qualityFlagComponents?.forEach((item) => item.enable(opts));
    this.measurementTypeComponents?.forEach((item) => item.enable(opts));
    this.acquisitionLevelComponents?.forEach((item) => item.enable(opts));
    this.photoResolutionComponents?.forEach((item) => item.enable(opts));
    this.refreshComponents();
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.disable(opts);
    this.qualityFlagComponents?.forEach((item) => item.disable(opts));
    this.measurementTypeComponents?.forEach((item) => item.disable(opts));
    this.acquisitionLevelComponents?.forEach((item) => item.disable(opts));
    this.photoResolutionComponents?.forEach((item) => item.disable(opts));
  }

  clear() {
    this.reset(new this.dataType());
    this.disable();
  }

  // Make setError public
  setError(value: string, opts?: { emitEvent?: boolean }) {
    super.setError(value, opts);
  }

  refresh() {
    this.refreshComponents();
    // Refresh list of visible/remaining filters
    this.visibleFilters = [...this.computeVisibleFiltersFromValue(this.data), ...(this.data.visibleFilters || [])].filter(uniqueValue);
    this.availableFilters = [...this.possibleFilters.filter((value) => !this.visibleFilters.includes(value))];
    this.markForCheck();
  }

  refreshComponents() {
    this.detectChanges();
    this.referentialFilterFormFields?.forEach((item) => item.resetView());
  }

  protected storeVisibleFilters() {
    this.form.patchValue({ visibleFilters: this.visibleFilters }, { onlySelf: true, emitEvent: false });
  }

  abstract computeVisibleFiltersFromValue(value: I): (keyof I)[];

  // Must be overridden to customize default value
  abstract defaultValue(property: keyof I): any;
}
