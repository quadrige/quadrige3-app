import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaOccasion, IExtractCriteriaOccasion } from '@app/extraction/criteria/occasion/extract-criteria-occasion.model';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { TextFilterValidator } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValidator } from '@app/referential/component/date-filter-form-field.component';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaOccasionValidatorService extends BaseValidatorService<ExtractCriteriaOccasion> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected textFilterValidator: TextFilterValidator,
    protected dateFilterValidator: DateFilterValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaOccasion, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaOccasion, any>>{
      id: [data?.id || null],
      name: this.textFilterValidator.getFormGroup(data?.name),
      date: this.dateFilterValidator.getFormGroup(data?.date),
      shipFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.shipFilter),
      recorderDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.recorderDepartmentFilter),
      userFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.userFilter),
      geometryType: [data?.geometryType || null],
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
