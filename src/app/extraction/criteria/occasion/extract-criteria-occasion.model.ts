import { capitalizeFirstLetter, isEmptyArray, isNil } from '@sumaris-net/ngx-components';
import { GeometryType } from '@app/referential/monitoring-location/geometry-type';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValue, IDateFilterValue } from '@app/referential/component/date-filter-form-field.component';
import { IntReferentialFilterCriteria, IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Utils } from '@app/shared/utils';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion) =>
  ExtractUtils.getValue<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property, newValue);
const getTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion) =>
  ExtractUtils.getTextFilter<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property);
const setTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion, newValue: ITextFilterValue) =>
  ExtractUtils.setTextFilter<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property, newValue);
const getDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion) =>
  ExtractUtils.getDateFilter<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property);
const setDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion, newValue: IDateFilterValue) =>
  ExtractUtils.setDateFilter<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion) =>
  ExtractUtils.getValues<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaOccasion) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaOccasion,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaOccasion>(criterias, occasionCriteriaDefinition, property, referentialFilter);

export const occasionFilters: (keyof IExtractCriteriaOccasion)[] = [
  'name',
  'date',
  'shipFilter',
  'recorderDepartmentFilter',
  'userFilter',
  // 'geometryType', // TODO uncomment when geometry type is wanted
];

export interface IExtractCriteriaOccasion extends IExtractCriteria {
  name: ITextFilterValue;
  date: IDateFilterValue;
  shipFilter: IReferentialFilterCriteria<number>;
  recorderDepartmentFilter: IReferentialFilterCriteria<number>;
  userFilter: IReferentialFilterCriteria<number>;
  geometryType: GeometryType;
}

export const occasionCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaOccasion> = {
  name: {
    i18nLabel: 'DATA.OCCASION.NAME',
    valueCriteriaType: 'EXTRACT_OCCASION_NAME',
  },
  date: {
    i18nLabel: 'DATA.OCCASION.DATE',
    valuesCriteriaType: 'EXTRACT_OCCASION_DATE',
  },
  shipFilter: {
    i18nLabel: 'DATA.OCCASION.SHIP',
    valuesCriteriaType: 'EXTRACT_OCCASION_SHIP_ID',
    valuesOperatorType: 'SHIP_IN',
    valueCriteriaType: 'EXTRACT_OCCASION_SHIP_NAME',
  },
  recorderDepartmentFilter: {
    i18nLabel: 'DATA.OCCASION.RECORDER_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_OCCASION_RECORDER_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_WITH_DEFAULT_IN',
    valueCriteriaType: 'EXTRACT_OCCASION_RECORDER_DEPARTMENT_NAME',
  },
  userFilter: {
    i18nLabel: 'DATA.OCCASION.USER',
    valuesCriteriaType: 'EXTRACT_OCCASION_USER_ID',
    valuesOperatorType: 'USER_IN',
    valueCriteriaType: 'EXTRACT_OCCASION_USER_NAME',
  },
  geometryType: {
    i18nLabel: 'DATA.OCCASION.GEOMETRY_TYPE',
    valuesI18Labels: {
      point: 'REFERENTIAL.GEOMETRY_TYPE.POINT',
      line: 'REFERENTIAL.GEOMETRY_TYPE.LINE',
      area: 'REFERENTIAL.GEOMETRY_TYPE.AREA',
    },
    valueCriteriaType: 'EXTRACT_OCCASION_GEOMETRY_TYPE',
    valueOperatorType: 'GEOMETRY_TYPE_EQUAL',
  },
};

export class ExtractCriteriaOccasion implements IExtractCriteriaOccasion {
  id = null;
  name = TextFilterValue.default();
  date = DateFilterValue.default();
  shipFilter: IntReferentialFilterCriteria;
  recorderDepartmentFilter: IntReferentialFilterCriteria;
  userFilter: IntReferentialFilterCriteria;
  geometryType: GeometryType;
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaOccasion): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaOccasion.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaOccasion, c2: ExtractCriteriaOccasion): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        TextFilterValue.equals(c1.name, c2.name) &&
        DateFilterValue.equals(c1.date, c2.date) &&
        Utils.equals(c1.shipFilter, c2.shipFilter) &&
        Utils.equals(c1.recorderDepartmentFilter, c2.recorderDepartmentFilter) &&
        Utils.equals(c1.userFilter, c2.userFilter) &&
        c1.geometryType === c2.geometryType)
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaOccasion {
    return {
      id,
      name: getTextFilter(criterias, 'name'),
      date: getDateFilter(criterias, 'date'),
      shipFilter: getReferentialFilterCriteria(criterias, 'shipFilter'),
      recorderDepartmentFilter: getReferentialFilterCriteria(criterias, 'recorderDepartmentFilter'),
      userFilter: getReferentialFilterCriteria(criterias, 'userFilter'),
      geometryType: getValue(criterias, 'geometryType').value?.toUpperCase(),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaOccasion, opts: IMergeCriteriaOptions) {
    setTextFilter(criterias, 'name', value?.name);
    setDateFilter(criterias, 'date', value?.date);
    setReferentialFilterCriteria(criterias, 'shipFilter', IntReferentialFilterCriteria.fromObject(value?.shipFilter));
    setReferentialFilterCriteria(criterias, 'recorderDepartmentFilter', IntReferentialFilterCriteria.fromObject(value?.recorderDepartmentFilter));
    setReferentialFilterCriteria(criterias, 'userFilter', IntReferentialFilterCriteria.fromObject(value?.userFilter));
    setValue(criterias, 'geometryType', { value: capitalizeFirstLetter(value?.geometryType?.toLowerCase()) });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaOccasion): (keyof IExtractCriteriaOccasion)[] {
    const properties: (keyof IExtractCriteriaOccasion)[] = [];
    if (criteria) {
      if (!TextFilterValue.isEmpty(criteria.name)) properties.push('name');
      if (!DateFilterValue.isEmpty(criteria.date)) properties.push('date');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.shipFilter, true)) properties.push('shipFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.recorderDepartmentFilter, true)) properties.push('recorderDepartmentFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.userFilter, true)) properties.push('userFilter');
      if (!!criteria.geometryType) properties.push('geometryType');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaOccasion): any {
    switch (property) {
      case 'date':
        return DateFilterValue.default();
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaOccasion {
    const target = new ExtractCriteriaOccasion();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.name = source.name || TextFilterValue.default();
    this.date = DateFilterValue.fromObject(source.date);
    this.shipFilter = IntReferentialFilterCriteria.fromObject(source.shipFilter || {});
    this.recorderDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.recorderDepartmentFilter || {});
    this.userFilter = IntReferentialFilterCriteria.fromObject(source.userFilter || {});
    this.geometryType = source.geometryType;
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target: IExtractCriteriaOccasion = { ...this };
    target.date = this.date?.asObject();
    target.shipFilter = this.shipFilter?.asObject();
    target.recorderDepartmentFilter = this.recorderDepartmentFilter?.asObject();
    target.userFilter = this.userFilter?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
