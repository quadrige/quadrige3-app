import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import {
  ExtractCriteriaOccasion,
  IExtractCriteriaOccasion,
  occasionCriteriaDefinition,
  occasionFilters,
} from '@app/extraction/criteria/occasion/extract-criteria-occasion.model';
import { geometryTypes } from '@app/referential/monitoring-location/geometry-type';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';

@Component({
  selector: 'app-extract-criteria-occasion-component',
  templateUrl: './extract-criteria-occasion.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaOccasionComponent extends ExtractCriteriaComponent<ExtractCriteriaOccasion, IExtractCriteriaOccasion> {
  criteriaDefinition = occasionCriteriaDefinition;
  possibleFilters = occasionFilters;
  geometryTypes = geometryTypes;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaOccasion);
    this.logPrefix = '[extract-criteria-occasion]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaOccasion): (keyof IExtractCriteriaOccasion)[] {
    return ExtractCriteriaOccasion.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaOccasion): any {
    return ExtractCriteriaOccasion.defaultValue(property);
  }
}
