import { ReferentialValidatorOptions } from '@app/referential/service/referential-validator.service';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaMain, IExtractCriteriaMain } from '@app/extraction/criteria/extract-criteria.model';
import { ExtractCriteriaSurveyValidatorService } from '@app/extraction/criteria/survey/extract-criteria-survey.validator';
import { ExtractCriteriaSamplingOperationValidatorService } from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.validator';
import { ExtractCriteriaSampleValidatorService } from '@app/extraction/criteria/sample/extract-criteria-sample.validator';
import { ExtractCriteriaMeasurementValidatorService } from '@app/extraction/criteria/measurement/extract-criteria-measurement.validator';
import { ExtractCriteriaSurvey } from '@app/extraction/criteria/survey/extract-criteria-survey.model';
import { ExtractCriteriaSamplingOperation } from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.model';
import { ExtractCriteriaSample } from '@app/extraction/criteria/sample/extract-criteria-sample.model';
import { ExtractCriteriaMeasurement } from '@app/extraction/criteria/measurement/extract-criteria-measurement.model';
import { ExtractCriteriaPhotoValidatorService } from '@app/extraction/criteria/photo/extract-criteria-photo.validator';
import { ExtractCriteriaPhoto } from '@app/extraction/criteria/photo/extract-criteria-photo.model';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { ExtractSurveyPeriod } from '@app/extraction/period/extract-survey-period.model';
import { ExtractSurveyPeriodValidatorService } from '@app/extraction/period/extract-survey-period.validator';
import { ExtractCriteriaCampaignValidatorService } from '@app/extraction/criteria/campaign/extract-criteria-campaign.validator';
import { ExtractCriteriaCampaign } from '@app/extraction/criteria/campaign/extract-criteria-campaign.model';
import { FilterType } from '@app/referential/filter/model/filter-type';
import { ExtractCriteriaOccasionValidatorService } from '@app/extraction/criteria/occasion/extract-criteria-occasion.validator';
import { ExtractCriteriaOccasion } from '@app/extraction/criteria/occasion/extract-criteria-occasion.model';
import { ExtractCriteriaEventValidatorService } from '@app/extraction/criteria/event/extract-criteria-event.validator';
import { ExtractCriteriaEvent } from '@app/extraction/criteria/event/extract-criteria-event.model';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaValidatorService extends BaseValidatorService<ExtractCriteriaMain> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected extractSurveyPeriodValidatorService: ExtractSurveyPeriodValidatorService,
    protected extractCriteriaSurveyValidator: ExtractCriteriaSurveyValidatorService,
    protected extractCriteriaSamplingOperationValidator: ExtractCriteriaSamplingOperationValidatorService,
    protected extractCriteriaSampleValidator: ExtractCriteriaSampleValidatorService,
    protected extractCriteriaMeasurementValidator: ExtractCriteriaMeasurementValidatorService,
    protected extractCriteriaPhotoValidator: ExtractCriteriaPhotoValidatorService,
    protected extractCriteriaCampaignValidator: ExtractCriteriaCampaignValidatorService,
    protected extractCriteriaOccasionValidator: ExtractCriteriaOccasionValidatorService,
    protected extractCriteriaEventValidator: ExtractCriteriaEventValidatorService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaMain, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaMain, any>>{
      periods: this.formBuilder.array((data?.periods || []).map((value) => this.getExtractSurveyPeriodControl(value))),
      metaProgramFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.metaProgramFilter),
      programFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.programFilter),
      monitoringLocationFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.monitoringLocationFilter),
      harbourFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.harbourFilter),
      campaignFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.campaignFilter),
      eventFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.eventFilter),
      batchFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.batchFilter),
      surveyCriterias: this.formBuilder.array((data?.surveyCriterias || [null]).map((value) => this.getExtractCriteriaSurveyControl(value))),
      samplingOperationCriterias: this.formBuilder.array(
        (data?.samplingOperationCriterias || [null]).map((value) => this.getExtractCriteriaSamplingOperationControl(value))
      ),
      sampleCriterias: this.formBuilder.array((data?.sampleCriterias || [null]).map((value) => this.getExtractCriteriaSampleControl(value))),
      measurementCriterias: this.formBuilder.array(
        (data?.measurementCriterias || [null]).map((value) => this.getExtractCriteriaMeasurementControl(value))
      ),
      photoCriterias: this.formBuilder.array((data?.photoCriterias || [null]).map((value) => this.getExtractCriteriaPhotoControl(value))),
      campaignCriterias: this.formBuilder.array((data?.campaignCriterias || [null]).map((value) => this.getExtractCriteriaCampaignControl(value))),
      occasionCriterias: this.formBuilder.array((data?.occasionCriterias || [null]).map((value) => this.getExtractCriteriaOccasionControl(value))),
      eventCriterias: this.formBuilder.array((data?.eventCriterias || [null]).map((value) => this.getExtractCriteriaEventControl(value))),
      visibleFilters: [data?.visibleFilters || null],
    };
  }

  updateFormGroup(form: UntypedFormGroup, filterTypes: FilterType[]) {
    if (!filterTypes.includes('EXTRACT_DATA_PERIOD')) {
      // Reset periods value to avoid inner date validators
      form.controls.periods = this.formBuilder.control(null);
    }
  }

  getExtractSurveyPeriodControl(data: ExtractSurveyPeriod) {
    return this.extractSurveyPeriodValidatorService.getFormGroup(data);
  }

  getExtractCriteriaSurveyControl(criteria: ExtractCriteriaSurvey) {
    return this.extractCriteriaSurveyValidator.getFormGroup(criteria);
  }

  getExtractCriteriaSamplingOperationControl(criteria: ExtractCriteriaSamplingOperation) {
    return this.extractCriteriaSamplingOperationValidator.getFormGroup(criteria);
  }

  getExtractCriteriaSampleControl(criteria: ExtractCriteriaSample) {
    return this.extractCriteriaSampleValidator.getFormGroup(criteria);
  }

  getExtractCriteriaMeasurementControl(criteria: ExtractCriteriaMeasurement) {
    return this.extractCriteriaMeasurementValidator.getFormGroup(criteria);
  }

  getExtractCriteriaPhotoControl(criteria: ExtractCriteriaPhoto) {
    return this.extractCriteriaPhotoValidator.getFormGroup(criteria);
  }

  getExtractCriteriaCampaignControl(criteria: ExtractCriteriaCampaign) {
    return this.extractCriteriaCampaignValidator.getFormGroup(criteria);
  }

  getExtractCriteriaOccasionControl(criteria: ExtractCriteriaOccasion) {
    return this.extractCriteriaOccasionValidator.getFormGroup(criteria);
  }

  getExtractCriteriaEventControl(criteria: ExtractCriteriaEvent) {
    return this.extractCriteriaEventValidator.getFormGroup(criteria);
  }
}
