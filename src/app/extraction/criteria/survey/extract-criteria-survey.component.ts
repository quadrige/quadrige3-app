import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import {
  ExtractCriteriaSurvey,
  IExtractCriteriaSurvey,
  surveyCriteriaDefinition,
  surveyFilters,
} from '@app/extraction/criteria/survey/extract-criteria-survey.model';
import { geometryTypes } from '@app/referential/monitoring-location/geometry-type';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';

@Component({
  selector: 'app-extract-criteria-survey-component',
  templateUrl: './extract-criteria-survey.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaSurveyComponent extends ExtractCriteriaComponent<ExtractCriteriaSurvey, IExtractCriteriaSurvey> {
  criteriaDefinition = surveyCriteriaDefinition;
  possibleFilters = surveyFilters;
  geometryTypes = geometryTypes;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaSurvey);
    this.logPrefix = '[extract-criteria-survey]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaSurvey): (keyof IExtractCriteriaSurvey)[] {
    return ExtractCriteriaSurvey.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaSurvey): any {
    return ExtractCriteriaSurvey.defaultValue(property);
  }

  addFilter(event: UIEvent, filter: any) {
    if (['havingMeasurement', 'havingMeasurementFile'].includes(filter)) {
      // Default value: true
      this.form.get(filter).setValue(true);
      this.markAsDirty();
    }
    super.addFilter(event, filter);
  }
}
