import { capitalizeFirstLetter, isEmptyArray, isNil, isNotNil } from '@sumaris-net/ngx-components';
import { GeometryType } from '@app/referential/monitoring-location/geometry-type';
import { IntReferentialFilterCriteria, IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import {
  dataQualityFlagI18nLabelPrefix,
  ExtractCriteriaQualityFlag,
  IExtractCriteriaQualityFlag,
  qualityFlagI18nLabels,
} from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.model';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValue, IDateFilterValue } from '@app/referential/component/date-filter-form-field.component';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { Utils } from '@app/shared/utils';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey) =>
  ExtractUtils.getValue<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property, newValue);
const getTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey) =>
  ExtractUtils.getTextFilter<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property);
const setTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey, newValue: ITextFilterValue) =>
  ExtractUtils.setTextFilter<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property, newValue);
const getDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey) =>
  ExtractUtils.getDateFilter<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property);
const setDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey, newValue: IDateFilterValue) =>
  ExtractUtils.setDateFilter<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey) =>
  ExtractUtils.getValues<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSurvey) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaSurvey,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaSurvey>(criterias, surveyCriteriaDefinition, property, referentialFilter);

export const surveyFilters: (keyof IExtractCriteriaSurvey)[] = [
  'label',
  'comments',
  'geometryType',
  'recorderDepartmentFilter',
  'controlled',
  'validated',
  'qualifications',
  'updateDate',
  'havingMeasurement',
  'havingMeasurementFile',
];

export interface IExtractCriteriaSurvey extends IExtractCriteria {
  label: ITextFilterValue;
  comments: ITextFilterValue;
  geometryType: GeometryType;
  recorderDepartmentFilter: IReferentialFilterCriteria<number>;
  controlled: boolean;
  validated: boolean;
  qualifications: IExtractCriteriaQualityFlag;
  updateDate: IDateFilterValue;
  havingMeasurement: boolean;
  havingMeasurementFile: boolean;
}

export const surveyCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaSurvey> = {
  label: {
    i18nLabel: 'DATA.SURVEY.LABEL',
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_LABEL',
  },
  comments: {
    i18nLabel: 'DATA.SURVEY.COMMENTS',
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_COMMENT',
    valueOperatorType: 'TEXT_CONTAINS',
  },
  geometryType: {
    i18nLabel: 'REFERENTIAL.MONITORING_LOCATION.GEOMETRY_TYPE',
    valuesI18Labels: {
      point: 'REFERENTIAL.GEOMETRY_TYPE.POINT',
      line: 'REFERENTIAL.GEOMETRY_TYPE.LINE',
      area: 'REFERENTIAL.GEOMETRY_TYPE.AREA',
    },
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE',
    valueOperatorType: 'GEOMETRY_TYPE_EQUAL',
  },
  recorderDepartmentFilter: {
    i18nLabel: 'DATA.SURVEY.RECORDER_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_LABEL',
  },
  controlled: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CONTROLLED',
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_CONTROL',
    valueOperatorType: 'BOOLEAN',
  },
  validated: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.VALIDATED',
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_VALIDATION',
    valueOperatorType: 'BOOLEAN',
  },
  qualifications: {
    i18nLabel: dataQualityFlagI18nLabelPrefix + 'TITLE',
    i18nLabelPrefix: dataQualityFlagI18nLabelPrefix,
    valuesI18Labels: qualityFlagI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_SURVEY_QUALIFICATION',
  },
  updateDate: {
    i18nLabel: 'DATA.SURVEY.UPDATE_DATE',
    valuesCriteriaType: 'EXTRACT_RESULT_SURVEY_UPDATE_DATE',
  },
  havingMeasurement: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.HAVING_MEASUREMENT.SURVEY',
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT',
    valueOperatorType: 'BOOLEAN',
  },
  havingMeasurementFile: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.HAVING_MEASUREMENT_FILE.SURVEY',
    valueCriteriaType: 'EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE',
    valueOperatorType: 'BOOLEAN',
  },
};

export class ExtractCriteriaSurvey implements IExtractCriteriaSurvey {
  id = null;
  label = TextFilterValue.default();
  comments = TextFilterValue.default('TEXT_CONTAINS');
  geometryType: GeometryType;
  recorderDepartmentFilter: IntReferentialFilterCriteria;
  controlled = null;
  validated = null;
  qualifications = ExtractCriteriaQualityFlag.default();
  updateDate = DateFilterValue.default();
  havingMeasurement = null;
  havingMeasurementFile = null;
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaSurvey): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaSurvey.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaSurvey, c2: ExtractCriteriaSurvey): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        TextFilterValue.equals(c1.label, c2.label) &&
        TextFilterValue.equals(c1.comments, c2.comments) &&
        c1.geometryType === c2.geometryType &&
        Utils.equals(c1.recorderDepartmentFilter, c2.recorderDepartmentFilter) &&
        c1.controlled === c2.controlled &&
        c1.validated === c2.validated &&
        ExtractCriteriaQualityFlag.equals(c1.qualifications, c2.qualifications) &&
        DateFilterValue.equals(c1.updateDate, c2.updateDate) &&
        c1.havingMeasurement === c2.havingMeasurement &&
        c1.havingMeasurementFile === c2.havingMeasurementFile)
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaSurvey {
    return {
      id,
      label: getTextFilter(criterias, 'label'),
      comments: getTextFilter(criterias, 'comments'),
      geometryType: getValue(criterias, 'geometryType').value?.toUpperCase(),
      recorderDepartmentFilter: getReferentialFilterCriteria(criterias, 'recorderDepartmentFilter'),
      controlled: Utils.fromOptionalBooleanString(getValue(criterias, 'controlled').value),
      validated: Utils.fromOptionalBooleanString(getValue(criterias, 'validated').value),
      qualifications: ExtractCriteriaQualityFlag.fromValues(getValues(criterias, 'qualifications').values),
      updateDate: getDateFilter(criterias, 'updateDate'),
      havingMeasurement: Utils.fromOptionalBooleanString(getValue(criterias, 'havingMeasurement').value),
      havingMeasurementFile: Utils.fromOptionalBooleanString(getValue(criterias, 'havingMeasurementFile').value),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaSurvey, opts: IMergeCriteriaOptions) {
    setTextFilter(criterias, 'label', value?.label);
    setTextFilter(criterias, 'comments', value?.comments);
    setValue(criterias, 'geometryType', { value: capitalizeFirstLetter(value?.geometryType?.toLowerCase()) });
    setReferentialFilterCriteria(criterias, 'recorderDepartmentFilter', IntReferentialFilterCriteria.fromObject(value?.recorderDepartmentFilter));
    setValue(criterias, 'controlled', {
      value: isNotNil(value?.controlled) ? Utils.toBooleanString(value?.controlled) : null,
    });
    setValue(criterias, 'validated', {
      value: isNotNil(value?.validated) ? Utils.toBooleanString(value?.validated) : null,
    });
    setValues(criterias, 'qualifications', { values: ExtractCriteriaQualityFlag.toValues(value?.qualifications, opts.defaultIfEmpty) });
    setDateFilter(criterias, 'updateDate', value?.updateDate);
    setValue(criterias, 'havingMeasurement', {
      value: isNotNil(value?.havingMeasurement) ? Utils.toBooleanString(value?.havingMeasurement) : null,
    });
    setValue(criterias, 'havingMeasurementFile', {
      value: isNotNil(value?.havingMeasurementFile) ? Utils.toBooleanString(value?.havingMeasurementFile) : null,
    });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaSurvey): (keyof IExtractCriteriaSurvey)[] {
    const properties: (keyof IExtractCriteriaSurvey)[] = [];
    if (criteria) {
      if (!TextFilterValue.isEmpty(criteria.label)) properties.push('label');
      if (!TextFilterValue.isEmpty(criteria.comments)) properties.push('comments');
      if (!!criteria.geometryType) properties.push('geometryType');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.recorderDepartmentFilter, true)) properties.push('recorderDepartmentFilter');
      if (isNotNil(criteria.controlled)) properties.push('controlled');
      if (isNotNil(criteria.validated)) properties.push('validated');
      if (!ExtractCriteriaQualityFlag.isEmptyOrDefault(criteria.qualifications)) properties.push('qualifications');
      if (!DateFilterValue.isEmpty(criteria.updateDate)) properties.push('updateDate');
      if (isNotNil(criteria.havingMeasurement)) properties.push('havingMeasurement');
      if (isNotNil(criteria.havingMeasurementFile)) properties.push('havingMeasurementFile');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaSurvey): any {
    switch (property) {
      case 'label':
        return TextFilterValue.default();
      case 'comments':
        return TextFilterValue.default('TEXT_CONTAINS');
      case 'qualifications':
        return ExtractCriteriaQualityFlag.default();
      case 'updateDate':
        return DateFilterValue.default();
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaSurvey {
    const target = new ExtractCriteriaSurvey();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.label = source.label || TextFilterValue.default();
    this.comments = source.comments || TextFilterValue.default('TEXT_CONTAINS');
    this.geometryType = source.geometryType;
    this.recorderDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.recorderDepartmentFilter || {});
    this.controlled = source.controlled;
    this.validated = source.validated;
    this.qualifications = ExtractCriteriaQualityFlag.fromObject(source.qualifications);
    this.updateDate = source.updateDate || DateFilterValue.default();
    this.havingMeasurement = source.havingMeasurement;
    this.havingMeasurementFile = source.havingMeasurementFile;
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target: IExtractCriteriaSurvey = { ...this };
    target.recorderDepartmentFilter = this.recorderDepartmentFilter?.asObject();
    target.qualifications = this.qualifications?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
