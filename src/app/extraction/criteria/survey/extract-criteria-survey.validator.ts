import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaSurvey, IExtractCriteriaSurvey } from '@app/extraction/criteria/survey/extract-criteria-survey.model';
import { ExtractCriteriaQualityFlagValidatorService } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.validator';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { TextFilterValidator } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValidator } from '@app/referential/component/date-filter-form-field.component';
import { toBoolean } from '@sumaris-net/ngx-components';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaSurveyValidatorService extends BaseValidatorService<ExtractCriteriaSurvey> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected qualityFlagValidator: ExtractCriteriaQualityFlagValidatorService,
    protected textFilterValidator: TextFilterValidator,
    protected dateFilterValidator: DateFilterValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaSurvey, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaSurvey, any>>{
      id: [data?.id || null],
      label: this.textFilterValidator.getFormGroup(data?.label),
      comments: this.textFilterValidator.getFormGroup(data?.comments),
      geometryType: [data?.geometryType || null],
      recorderDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.recorderDepartmentFilter),
      controlled: [toBoolean(data?.controlled)],
      validated: [toBoolean(data?.validated)],
      qualifications: this.qualityFlagValidator.getFormGroup(data?.qualifications),
      updateDate: this.dateFilterValidator.getFormGroup(data?.updateDate),
      havingMeasurement: [toBoolean(data?.havingMeasurement)],
      havingMeasurementFile: [toBoolean(data?.havingMeasurementFile)],
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
