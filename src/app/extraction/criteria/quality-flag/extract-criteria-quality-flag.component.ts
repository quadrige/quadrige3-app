import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { throttleTime } from 'rxjs/operators';
import { ExtractCriteriaQualityFlag, qualityFlagI18nLabels } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.model';
import { BaseForm } from '@app/shared/form/base.form';

@Component({
  selector: 'app-extract-criteria-quality-flag-component',
  templateUrl: './extract-criteria-quality-flag.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaQualityFlagComponent extends BaseForm<ExtractCriteriaQualityFlag> implements OnInit {
  @Input() i18nLabelPrefix: string;

  i18nLabels = qualityFlagI18nLabels;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.form.valueChanges.pipe(throttleTime(50)).subscribe((value) => {
        // if (this.debug) console.debug('[extract-criteria-quality-flag] value', value);
        if (this.loaded && this.enabled) {
          this.markAsDirty();
        }
      })
    );
  }
}
