import { AbstractControlOptions, UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { toBoolean } from '@sumaris-net/ngx-components';
import {
  ExtractCriteriaQualityFlag,
  IExtractCriteriaQualityFlagKey,
} from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.model';
import { BaseGroupValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaQualityFlagValidatorService extends BaseValidatorService<ExtractCriteriaQualityFlag> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaQualityFlag, opts?: any): { [p: string]: any } {
    return <Record<IExtractCriteriaQualityFlagKey, any>>{
      good: [toBoolean(data?.good, true)],
      doubtful: [toBoolean(data?.doubtful, true)],
      bad: [toBoolean(data?.bad, true)],
      notQualified: [toBoolean(data?.notQualified, true)],
    };
  }

  getFormGroupOptions(data?: ExtractCriteriaQualityFlag, opts?: any): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredAnyOf(
          <IExtractCriteriaQualityFlagKey[]>['good', 'doubtful', 'bad', 'notQualified'],
          true,
          'EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_QUALITY_FLAG'
        ),
      ],
    };
  }
}
