import { arraySize, isEmptyArray, isNil, toBoolean } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { Utils } from '@app/shared/utils';

export type QualityFlagType = 'GOOD' | 'DOUBTFUL' | 'BAD' | 'NOT_QUALIFIED';

export interface IExtractCriteriaQualityFlag {
  good: boolean;
  doubtful: boolean;
  bad: boolean;
  notQualified: boolean;
}

export type IExtractCriteriaQualityFlagKey = keyof IExtractCriteriaQualityFlag;

const qualityFlagI18nLabelPrefix = 'EXTRACTION.EXTRACT_FILTER.DEFINITION.QUALITY_FLAG.';
export const dataQualityFlagI18nLabelPrefix = qualityFlagI18nLabelPrefix + 'DATA.';
export const photoQualityFlagI18nLabelPrefix = qualityFlagI18nLabelPrefix + 'PHOTO.';
export const qualityFlagI18nLabels: Record<IExtractCriteriaQualityFlagKey, string> = {
  good: 'GOOD',
  doubtful: 'DOUBTFUL',
  bad: 'BAD',
  notQualified: 'NOT_QUALIFIED',
};

export class ExtractCriteriaQualityFlag implements IExtractCriteriaQualityFlag {
  good = true;
  doubtful = true;
  bad = true;
  notQualified = true;

  static default(): ExtractCriteriaQualityFlag {
    return new ExtractCriteriaQualityFlag();
  }

  static isEmptyOrDefault(s: IExtractCriteriaQualityFlag): boolean {
    return isNil(s) || ExtractCriteriaQualityFlag.equals(ExtractCriteriaQualityFlag.default(), s);
  }

  static equals(s1: IExtractCriteriaQualityFlag, s2: IExtractCriteriaQualityFlag): boolean {
    return EntityUtils.deepEquals(s1, s2, <IExtractCriteriaQualityFlagKey[]>['good', 'doubtful', 'bad', 'notQualified']);
  }

  static fromObject(source: any): ExtractCriteriaQualityFlag {
    const target = new ExtractCriteriaQualityFlag();
    if (source) Object.assign(target, source);
    return target;
  }

  static fromValues(values: QualityFlagType[]): IExtractCriteriaQualityFlag {
    if (isEmptyArray(values)) return ExtractCriteriaQualityFlag.default();
    if (arraySize(values) === 1 && values[0].includes(';')) return ExtractCriteriaQualityFlag.fromOldValue(values[0]);
    return {
      good: toBoolean(values?.includes('GOOD')),
      doubtful: toBoolean(values?.includes('DOUBTFUL')),
      bad: toBoolean(values?.includes('BAD')),
      notQualified: toBoolean(values?.includes('NOT_QUALIFIED')),
    };
  }

  static toValues(value: IExtractCriteriaQualityFlag, defaultIfEmpty: boolean): QualityFlagType[] {
    if (!defaultIfEmpty && ExtractCriteriaQualityFlag.isEmptyOrDefault(value)) return [];
    if (defaultIfEmpty && isNil(value)) value = ExtractCriteriaQualityFlag.default();
    const values: QualityFlagType[] = [];
    if (value.good) values.push('GOOD');
    if (value.doubtful) values.push('DOUBTFUL');
    if (value.bad) values.push('BAD');
    if (value.notQualified) values.push('NOT_QUALIFIED');
    return values;
  }

  private static fromOldValue(value: string): IExtractCriteriaQualityFlag {
    const b = value?.split(';');
    return {
      good: Utils.fromBooleanString(b?.[3]),
      doubtful: Utils.fromBooleanString(b?.[4]),
      bad: Utils.fromBooleanString(b?.[5]),
      notQualified: true,
    };
  }

  asObject(): any {
    return { ...this };
  }
}
