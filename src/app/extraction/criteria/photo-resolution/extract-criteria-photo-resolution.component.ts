import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { throttleTime } from 'rxjs/operators';
import {
  ExtractCriteriaPhotoResolution,
  photoResolutionI18nLabels,
} from '@app/extraction/criteria/photo-resolution/extract-criteria-photo-resolution.model';
import { BaseForm } from '@app/shared/form/base.form';

@Component({
  selector: 'app-extract-criteria-photo-resolution-component',
  templateUrl: './extract-criteria-photo-resolution.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaPhotoResolutionComponent extends BaseForm<ExtractCriteriaPhotoResolution> implements OnInit {
  @Input() i18nLabelPrefix: string;

  i18nLabels = photoResolutionI18nLabels;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.form.valueChanges.pipe(throttleTime(50)).subscribe((value) => {
        // if (this.debug) console.debug('[extract-criteria-photo-resolution] value', value);
        if (this.loaded && this.enabled) {
          this.markAsDirty();
        }
      })
    );
  }
}
