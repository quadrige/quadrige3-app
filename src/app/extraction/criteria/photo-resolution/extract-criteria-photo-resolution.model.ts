import { arraySize, isEmptyArray, isNil, toBoolean } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { Utils } from '@app/shared/utils';

export type PhotoResolutionType = 'SMALL' | 'MEDIUM' | 'LARGE';

export interface IExtractCriteriaPhotoResolution {
  small: boolean;
  medium: boolean;
  large: boolean;
}

export type IExtractCriteriaPhotoResolutionKey = keyof IExtractCriteriaPhotoResolution;

export const photoResolutionI18nPrefix = 'EXTRACTION.EXTRACT_FILTER.DEFINITION.PHOTO_RESOLUTION.';
export const photoResolutionI18nLabels: Record<IExtractCriteriaPhotoResolutionKey, string> = {
  small: 'SMALL',
  medium: 'MEDIUM',
  large: 'LARGE',
};

export class ExtractCriteriaPhotoResolution implements IExtractCriteriaPhotoResolution {
  small = true;
  medium = true;
  large = true;

  static default(): ExtractCriteriaPhotoResolution {
    return new ExtractCriteriaPhotoResolution();
  }

  static isEmptyOrDefault(s: IExtractCriteriaPhotoResolution): boolean {
    return isNil(s) || ExtractCriteriaPhotoResolution.equals(ExtractCriteriaPhotoResolution.default(), s);
  }

  static equals(s1: IExtractCriteriaPhotoResolution, s2: IExtractCriteriaPhotoResolution): boolean {
    return EntityUtils.deepEquals(s1, s2, <IExtractCriteriaPhotoResolutionKey[]>['small', 'medium', 'large']);
  }

  static fromObject(source: any): ExtractCriteriaPhotoResolution {
    const target = new ExtractCriteriaPhotoResolution();
    if (source) Object.assign(target, source);
    return target;
  }

  static fromValues(values: PhotoResolutionType[]): IExtractCriteriaPhotoResolution {
    if (isEmptyArray(values)) return ExtractCriteriaPhotoResolution.default();
    if (arraySize(values) === 1 && values[0].includes(';')) return ExtractCriteriaPhotoResolution.fromOldValue(values[0]);
    return {
      small: toBoolean(values?.includes('SMALL')),
      medium: toBoolean(values?.includes('MEDIUM')),
      large: toBoolean(values?.includes('LARGE')),
    };
  }

  static toValues(value: IExtractCriteriaPhotoResolution, defaultIfEmpty: boolean): PhotoResolutionType[] {
    if (!defaultIfEmpty && ExtractCriteriaPhotoResolution.isEmptyOrDefault(value)) return [];
    if (defaultIfEmpty && isNil(value)) value = ExtractCriteriaPhotoResolution.default();
    const values: PhotoResolutionType[] = [];
    if (value.small) values.push('SMALL');
    if (value.medium) values.push('MEDIUM');
    if (value.large) values.push('LARGE');
    return values;
  }

  private static fromOldValue(value: string): IExtractCriteriaPhotoResolution {
    const b = value?.split(';');
    return {
      small: Utils.fromBooleanString(b?.[0]),
      medium: Utils.fromBooleanString(b?.[1]),
      large: Utils.fromBooleanString(b?.[2]),
    };
  }

  asObject(): any {
    return { ...this };
  }
}
