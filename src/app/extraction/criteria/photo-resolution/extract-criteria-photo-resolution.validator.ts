import { AbstractControlOptions, UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { toBoolean } from '@sumaris-net/ngx-components';
import {
  ExtractCriteriaPhotoResolution,
  IExtractCriteriaPhotoResolutionKey,
} from '@app/extraction/criteria/photo-resolution/extract-criteria-photo-resolution.model';
import { BaseGroupValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaPhotoResolutionValidatorService extends BaseValidatorService<ExtractCriteriaPhotoResolution> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaPhotoResolution, opts?: any): { [p: string]: any } {
    return <Record<IExtractCriteriaPhotoResolutionKey, any>>{
      small: [toBoolean(data?.small, true)],
      medium: [toBoolean(data?.medium, true)],
      large: [toBoolean(data?.large, true)],
    };
  }

  getFormGroupOptions(data?: ExtractCriteriaPhotoResolution, opts?: any): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredAnyOf(
          <IExtractCriteriaPhotoResolutionKey[]>['small', 'medium', 'large'],
          true,
          'EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_PHOTO_RESOLUTION'
        ),
      ],
    };
  }
}
