import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Directive,
  ElementRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { arraySize, FormArrayHelper, isEmptyArray, isNotEmptyArray } from '@sumaris-net/ngx-components';
import { ExtractCriteriaMain, IExtractCriteriaMain, mainCriteriaDefinition, mainFilters } from '@app/extraction/criteria/extract-criteria.model';
import { ExtractCriteriaValidatorService } from '@app/extraction/criteria/extract-criteria.validator';
import { ExtractCriteriaSurvey } from '@app/extraction/criteria/survey/extract-criteria-survey.model';
import { ExtractCriteriaSamplingOperation } from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.model';
import { ExtractCriteriaSample, sampleCriteriaDefinition } from '@app/extraction/criteria/sample/extract-criteria-sample.model';
import { ExtractCriteriaMeasurement } from '@app/extraction/criteria/measurement/extract-criteria-measurement.model';
import { ExtractCriteriaPhoto } from '@app/extraction/criteria/photo/extract-criteria-photo.model';
import { ExtractCriteriaSurveyComponent } from '@app/extraction/criteria/survey/extract-criteria-survey.component';
import { ExtractCriteriaSamplingOperationComponent } from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.component';
import { ExtractCriteriaSampleComponent } from '@app/extraction/criteria/sample/extract-criteria-sample.component';
import { ExtractCriteriaMeasurementComponent } from '@app/extraction/criteria/measurement/extract-criteria-measurement.component';
import { ExtractCriteriaPhotoComponent } from '@app/extraction/criteria/photo/extract-criteria-photo.component';
import { MatExpansionPanel } from '@angular/material/expansion';
import { FilterType } from '@app/referential/filter/model/filter-type';
import { PartialRecord } from '@app/shared/model/interface';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';
import { ExtractCriteriaAddModal, IExtractCriteriaOptions } from '@app/extraction/criteria/action/add/extract-criteria.add.modal';
import { IVisibleFilter } from '@app/extraction/extract-filter.model';
import { ExtractSurveyPeriodForm } from '@app/extraction/period/extract-survey-period.form';
import { ExtractSurveyPeriod } from '@app/extraction/period/extract-survey-period.model';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';
import { ExtractCriteriaCampaignComponent } from '@app/extraction/criteria/campaign/extract-criteria-campaign.component';
import { ExtractCriteriaCampaign } from '@app/extraction/criteria/campaign/extract-criteria-campaign.model';
import { ExtractCriteriaOccasionComponent } from '@app/extraction/criteria/occasion/extract-criteria-occasion.component';
import { ExtractCriteriaOccasion } from '@app/extraction/criteria/occasion/extract-criteria-occasion.model';
import { ExtractCriteriaEventComponent } from '@app/extraction/criteria/event/extract-criteria-event.component';
import { ExtractCriteriaEvent } from '@app/extraction/criteria/event/extract-criteria-event.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { IProgramFilterCriteria } from '@app/referential/program-strategy/program/filter/program.filter.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

interface IFilterDefinition {
  optional: boolean;
  expandable: boolean;
  i18nLabel: string;
  hasVisibleFilters?: boolean;
  arrayName?: string;
  arrayHelper?: FormArrayHelper<any>;
}

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'mat-expansion-panel[filterType]',
})
export class MatExpansionPanelFilterTypeDirective {
  @Input() filterType: FilterType;

  constructor(
    public matExpansionPanel: MatExpansionPanel,
    public element: ElementRef
  ) {}
}

@Component({
  selector: 'app-extract-criteria-form',
  templateUrl: './extract-criteria.form.html',
  styleUrls: ['./extract-criteria.form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaForm
  extends ExtractCriteriaComponent<ExtractCriteriaMain, IExtractCriteriaMain>
  implements OnInit, AfterViewInit, OnDestroy
{
  @Input() i18nPrefix: string;
  @Input() allFilterTypes: FilterType[];
  @ViewChild('extractSurveyPeriodForm') extractSurveyPeriodForm: ExtractSurveyPeriodForm;
  @ViewChildren(MatExpansionPanelFilterTypeDirective) expansionPanelsByFilterType: QueryList<MatExpansionPanelFilterTypeDirective>;
  @ViewChildren(ExtractCriteriaSurveyComponent) criteriaSurveyComponents: QueryList<ExtractCriteriaSurveyComponent>;
  @ViewChildren(ExtractCriteriaSamplingOperationComponent) criteriaSamplingOperationComponents: QueryList<ExtractCriteriaSamplingOperationComponent>;
  @ViewChildren(ExtractCriteriaSampleComponent) criteriaSampleComponents: QueryList<ExtractCriteriaSampleComponent>;
  @ViewChildren(ExtractCriteriaMeasurementComponent) criteriaMeasurementComponents: QueryList<ExtractCriteriaMeasurementComponent>;
  @ViewChildren(ExtractCriteriaPhotoComponent) criteriaPhotoComponents: QueryList<ExtractCriteriaPhotoComponent>;
  @ViewChildren(ExtractCriteriaCampaignComponent) criteriaCampaignComponents: QueryList<ExtractCriteriaCampaignComponent>;
  @ViewChildren(ExtractCriteriaOccasionComponent) criteriaOccasionComponents: QueryList<ExtractCriteriaOccasionComponent>;
  @ViewChildren(ExtractCriteriaEventComponent) criteriaEventComponents: QueryList<ExtractCriteriaEventComponent>;

  criteriaDefinition = mainCriteriaDefinition;
  possibleFilters = mainFilters;

  surveyCriteriasHelper: FormArrayHelper<ExtractCriteriaSurvey>;
  samplingOperationCriteriasHelper: FormArrayHelper<ExtractCriteriaSamplingOperation>;
  sampleCriteriasHelper: FormArrayHelper<ExtractCriteriaSample>;
  measurementCriteriasHelper: FormArrayHelper<ExtractCriteriaMeasurement>;
  photoCriteriasHelper: FormArrayHelper<ExtractCriteriaPhoto>;
  campaignCriteriasHelper: FormArrayHelper<ExtractCriteriaCampaign>;
  occasionCriteriasHelper: FormArrayHelper<ExtractCriteriaOccasion>;
  eventCriteriasHelper: FormArrayHelper<ExtractCriteriaEvent>;

  optionalFilterTypes: FilterType[];
  expandableFilterTypes: FilterType[];
  filterTypeDefinition: PartialRecord<FilterType, IFilterDefinition> = {
    /* eslint-disable @typescript-eslint/naming-convention */
    EXTRACT_DATA_PERIOD: { optional: false, expandable: false, i18nLabel: 'PERIOD' },
    EXTRACT_DATA_MAIN: { optional: false, expandable: true, i18nLabel: 'MAIN' },
    EXTRACT_DATA_SURVEY: { optional: true, expandable: true, i18nLabel: 'SURVEY', arrayName: 'surveyCriterias' },
    EXTRACT_DATA_SAMPLING_OPERATION: { optional: true, expandable: true, i18nLabel: 'SAMPLING_OPERATION', arrayName: 'samplingOperationCriterias' },
    EXTRACT_DATA_SAMPLE: { optional: true, expandable: true, i18nLabel: 'SAMPLE', arrayName: 'sampleCriterias' },
    EXTRACT_DATA_MEASUREMENT: { optional: true, expandable: true, i18nLabel: 'MEASUREMENT', arrayName: 'measurementCriterias' },
    EXTRACT_DATA_PHOTO: { optional: true, expandable: true, i18nLabel: 'PHOTO', arrayName: 'photoCriterias' },
    EXTRACT_CAMPAIGN: { optional: true, expandable: true, i18nLabel: 'CAMPAIGN', arrayName: 'campaignCriterias' },
    EXTRACT_OCCASION: { optional: true, expandable: true, i18nLabel: 'OCCASION', arrayName: 'occasionCriterias' },
    EXTRACT_EVENT: { optional: true, expandable: true, i18nLabel: 'EVENT', arrayName: 'eventCriterias' },
    /* eslint-enable @typescript-eslint/naming-convention */
  };

  programAddCriteria: Partial<IProgramFilterCriteria>;

  taxonNameFilterExistsInSample = false;
  taxonNameFilterExistsInMeasurement = false;
  taxonGroupFilterExistsInSample = false;
  taxonGroupFilterExistsInMeasurement = false;

  constructor(
    protected injector: Injector,
    protected validatorService: ExtractCriteriaValidatorService,
    protected extractFilterService: ExtractFilterService
  ) {
    super(injector, ExtractCriteriaMain);
    this.setForm(validatorService.getFormGroup());

    this.optionalFilterTypes = Object.keys(this.filterTypeDefinition)
      .filter((type) => this.filterTypeDefinition[type].optional)
      .map((type) => type as FilterType);
    this.expandableFilterTypes = Object.keys(this.filterTypeDefinition)
      .filter((type) => this.filterTypeDefinition[type].expandable)
      .map((type) => type as FilterType);
    this.logPrefix = '[extract-criteria-form]';
    this.i18nFieldPrefix = 'EXTRACTION.EXTRACT_FILTER.';
    // Update program criteria label
    this.criteriaDefinition.programFilter.i18nLabel = `EXTRACTION.EXTRACT_FILTER.DEFINITION.MAIN.PROGRAM.${this.extractFilterService.type}`;
    // Update met-program criteria label
    this.criteriaDefinition.metaProgramFilter.i18nLabel = `EXTRACTION.EXTRACT_FILTER.DEFINITION.MAIN.META_PROGRAM.${this.extractFilterService.type}`;
    // Update taxonNameFilter and taxonGroupFilter on sample if extraction is RESULT
    sampleCriteriaDefinition.taxonGroupFilter.i18nLabel = `EXTRACTION.EXTRACT_FILTER.DEFINITION.SUPPORT_TAXON_GROUP.${
      this.extractFilterService.type === 'RESULT' ? 'RESULT' : 'SAMPLE'
    }`;
    sampleCriteriaDefinition.taxonNameFilter.i18nLabel = `EXTRACTION.EXTRACT_FILTER.DEFINITION.SUPPORT_TAXON_NAME.${
      this.extractFilterService.type === 'RESULT' ? 'RESULT' : 'SAMPLE'
    }`;
  }

  get value() {
    return { ...super.value, periods: this.extractSurveyPeriodForm?.value };
  }

  set value(value: any) {
    this.setValue(value);
  }

  ngOnInit() {
    super.ngOnInit();

    // Update validator
    this.validatorService.updateFormGroup(this.form, this.allFilterTypes);

    this.initHelpers();
    this.registerSubscription(this.dirtyEvent.subscribe(() => this.updateFilterCount()));
  }

  formChange(value: IExtractCriteriaMain) {
    super.formChange(value);

    // Must propagate programFilter to other forms
    this.programAddCriteria = value.programFilter;
  }

  clear() {
    // this.accordion.closeAll(); // todo !
    this.value = undefined;
    this.surveyCriteriasHelper.resize(1);
    this.samplingOperationCriteriasHelper.resize(1);
    this.sampleCriteriasHelper.resize(1);
    this.measurementCriteriasHelper.resize(1);
    this.photoCriteriasHelper.resize(1);
    this.campaignCriteriasHelper.resize(1);
    this.occasionCriteriasHelper.resize(1);
    this.eventCriteriasHelper.resize(1);
    this.criteriaSurveyComponents?.forEach((item) => item.clear());
    this.criteriaSamplingOperationComponents?.forEach((item) => item.clear());
    this.criteriaSampleComponents?.forEach((item) => item.clear());
    this.criteriaMeasurementComponents?.forEach((item) => item.clear());
    this.criteriaPhotoComponents?.forEach((item) => item.clear());
    this.criteriaCampaignComponents?.forEach((item) => item.clear());
    this.criteriaOccasionComponents?.forEach((item) => item.clear());
    this.criteriaEventComponents?.forEach((item) => item.clear());
  }

  setValue(data: ExtractCriteriaMain, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> | void {
    this.markAsLoading();
    this.markAsUntouched();

    this.surveyCriteriasHelper.resize(0);
    this.surveyCriteriasHelper.resize(Math.max(1, arraySize(data?.surveyCriterias)));
    this.samplingOperationCriteriasHelper.resize(0);
    this.samplingOperationCriteriasHelper.resize(Math.max(1, arraySize(data?.samplingOperationCriterias)));
    this.sampleCriteriasHelper.resize(0);
    this.sampleCriteriasHelper.resize(Math.max(1, arraySize(data?.sampleCriterias)));
    this.measurementCriteriasHelper.resize(0);
    this.measurementCriteriasHelper.resize(Math.max(1, arraySize(data?.measurementCriterias)));
    this.photoCriteriasHelper.resize(0);
    this.photoCriteriasHelper.resize(Math.max(1, arraySize(data?.photoCriterias)));
    this.campaignCriteriasHelper.resize(0);
    this.campaignCriteriasHelper.resize(Math.max(1, arraySize(data?.campaignCriterias)));
    this.occasionCriteriasHelper.resize(0);
    this.occasionCriteriasHelper.resize(Math.max(1, arraySize(data?.occasionCriterias)));
    this.eventCriteriasHelper.resize(0);
    this.eventCriteriasHelper.resize(Math.max(1, arraySize(data?.eventCriterias)));

    // Detect presence of non-empty taxonName and taxonGroup filters
    this.taxonNameFilterExistsInSample = data?.sampleCriterias?.some((value) => !BaseFilterUtils.isCriteriaEmpty(value.taxonNameFilter, true));
    this.taxonNameFilterExistsInMeasurement = data?.measurementCriterias?.some(
      (value) => !BaseFilterUtils.isCriteriaEmpty(value.taxonNameFilter, true)
    );
    this.taxonGroupFilterExistsInSample = data?.sampleCriterias?.some((value) => !BaseFilterUtils.isCriteriaEmpty(value.taxonGroupFilter, true));
    this.taxonGroupFilterExistsInMeasurement = data?.measurementCriterias?.some(
      (value) => !BaseFilterUtils.isCriteriaEmpty(value.taxonGroupFilter, true)
    );

    // Compute data to set without periods
    data = data || ExtractCriteriaMain.fromObject({});
    const periods = data.periods;
    delete data.periods;
    this.disable();
    super.setValue(data, opts);

    // periods
    this.extractSurveyPeriodForm?.setValue(periods);

    this.enable();
    this.markAsPristine();
    this.markAsLoaded();

    this.refresh();
    this.updateFilterCount();
    // open expansion panel if filled filters
    for (const filterType of this.expandableFilterTypes) {
      if (this.filterTypeDefinition[filterType].hasVisibleFilters) {
        this.getExpansionPanel(filterType)?.open();
      }
    }
  }

  removeEmptyPeriod() {
    if (this.allFilterTypes.includes('EXTRACT_DATA_PERIOD')) {
      const periods = this.extractSurveyPeriodForm.value;
      if (isNotEmptyArray(periods)) {
        this.extractSurveyPeriodForm.value = periods.filter((period) => !!period.startDate || !!period.endDate);
      }
    }
  }

  async openAddModal(event: MouseEvent, filterType: FilterType) {
    event.preventDefault();

    let criteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaMain>;
    const availableFiltersByGroup: string[][] = [];
    let possibleFilters: string[];
    if (filterType === 'EXTRACT_DATA_PERIOD') {
      this.extractSurveyPeriodForm?.add(new ExtractSurveyPeriod());
      return;
    } else if (filterType === 'EXTRACT_DATA_MAIN') {
      criteriaDefinition = this.criteriaDefinition;
      availableFiltersByGroup.push([...this.availableFilters]);
      if (this.extractFilterService.type === 'SURVEY') {
        // Batch is not supported in survey extraction type (Mantis #62994)
        possibleFilters = this.possibleFilters.filter((value) => value !== 'batchFilter');
      } else {
        possibleFilters = this.possibleFilters;
      }
    } else {
      const helper = this.filterTypeDefinition[filterType].arrayHelper;
      if (helper.size() === 0) this.addFilterBlock(filterType);
      for (let i = 0; i < helper.size(); i++) {
        const component = this.getCriteriaComponent(filterType, i);
        if (!criteriaDefinition) criteriaDefinition = component.criteriaDefinition;
        if (!possibleFilters) possibleFilters = component.possibleFilters;
        // Remove taxon filters if already present (Mantis #61504)
        if (
          (filterType === 'EXTRACT_DATA_SAMPLE' && this.taxonNameFilterExistsInMeasurement) ||
          (filterType === 'EXTRACT_DATA_MEASUREMENT' && this.taxonNameFilterExistsInSample)
        ) {
          possibleFilters = possibleFilters.filter((value) => value !== 'taxonNameFilter');
        }
        if (
          (filterType === 'EXTRACT_DATA_SAMPLE' && this.taxonGroupFilterExistsInMeasurement) ||
          (filterType === 'EXTRACT_DATA_MEASUREMENT' && this.taxonGroupFilterExistsInSample)
        ) {
          possibleFilters = possibleFilters.filter((value) => value !== 'taxonGroupFilter');
        }
        // Remove havingMeasurementFilter if extraction type don't allow it
        if (!['SURVEY', 'SAMPLING_OPERATION', 'SAMPLE'].includes(this.extractFilterService.type)) {
          possibleFilters = possibleFilters.filter((value) => !['havingMeasurement', 'havingMeasurementFile'].includes(value));
        }
        availableFiltersByGroup.push([...component.availableFilters]);
      }
    }

    // Open add modal
    const { role, data } = await this.modalService.openModal<IExtractCriteriaOptions, string[][]>(ExtractCriteriaAddModal, {
      i18nLabel: this.filterTypeDefinition[filterType].i18nLabel,
      criteriaDefinition,
      availableFiltersByGroup,
      possibleFilters,
      blocksEnabled: filterType !== 'EXTRACT_DATA_MAIN',
    });

    if (role === 'validate' && data) {
      if (filterType === 'EXTRACT_DATA_MAIN') {
        // Add filters to this component
        const filters = data[0];
        for (const f of filters) {
          this.addFilter(event, f);
        }
      } else {
        // Add blocks and filters
        while (this.filterTypeDefinition[filterType].arrayHelper.size() < arraySize(data)) {
          this.addFilterBlock(filterType);
        }
        for (let i = 0; i < arraySize(data); i++) {
          const filters = data[i];
          const component = this.getCriteriaComponent(filterType, i);
          for (const f of filters) {
            component.addFilter(event, f);
          }
        }
      }
      this.updateFilterCount();
      this.enable();

      // Open panel
      this.getExpansionPanel(filterType)?.open();

      // try to focus panel
      const panelRef = this.getExpansionPanelRef(filterType);
      if (panelRef) {
        setTimeout(() => panelRef.nativeElement.scrollIntoView({ block: 'end', behavior: 'smooth' }), 250);
      }
    }
  }

  onFilterAdded(filterAdded, filterType: FilterType) {
    if (filterAdded === 'taxonNameFilter') {
      if (filterType === 'EXTRACT_DATA_SAMPLE') this.taxonNameFilterExistsInSample = true;
      if (filterType === 'EXTRACT_DATA_MEASUREMENT') this.taxonNameFilterExistsInMeasurement = true;
    }
    if (filterAdded === 'taxonGroupFilter') {
      if (filterType === 'EXTRACT_DATA_SAMPLE') this.taxonGroupFilterExistsInSample = true;
      if (filterType === 'EXTRACT_DATA_MEASUREMENT') this.taxonGroupFilterExistsInMeasurement = true;
    }
  }

  onFilterRemoved(filterRemoved, filterType: FilterType, index: number) {
    if (filterRemoved === 'taxonNameFilter') {
      this.taxonNameFilterExistsInSample = this.checkFilterExists(filterRemoved, 'EXTRACT_DATA_SAMPLE');
      this.taxonNameFilterExistsInMeasurement = this.checkFilterExists(filterRemoved, 'EXTRACT_DATA_MEASUREMENT');
    }
    if (filterRemoved === 'taxonGroupFilter') {
      this.taxonGroupFilterExistsInSample = this.checkFilterExists(filterRemoved, 'EXTRACT_DATA_SAMPLE');
      this.taxonGroupFilterExistsInMeasurement = this.checkFilterExists(filterRemoved, 'EXTRACT_DATA_MEASUREMENT');
    }

    this.checkRemoveBlock(filterType, index);
  }

  checkFilterExists(f, filterType: FilterType): boolean {
    if (filterType === 'EXTRACT_DATA_MAIN') {
      return this.visibleFilters.includes(f);
    } else {
      const helper = this.filterTypeDefinition[filterType].arrayHelper;
      for (let i = 0; i < helper.size(); i++) {
        const component = this.getCriteriaComponent(filterType, i);
        if (component.visibleFilters.includes(f)) return true;
      }
    }
    return false;
  }

  checkRemoveBlock(filterType: FilterType, index: number) {
    if (isEmptyArray(this.getCriteriaComponent(filterType, index).visibleFilters)) {
      this.filterTypeDefinition[filterType].arrayHelper.removeAt(index);
    }
    this.updateFilterCount();
  }

  getValidationError(): string {
    return this.getFormError(this.form, { separator: '<br>' });
  }

  translateFormPath(path: string): any {
    // remove indexes in path
    path = path.replace(/\.\d+\./g, '.');

    return super.translateFormPath(path);
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    this.extractSurveyPeriodForm?.enable();
    this.criteriaSurveyComponents?.forEach((item) => item.enable(opts));
    this.criteriaSamplingOperationComponents?.forEach((item) => item.enable(opts));
    this.criteriaSampleComponents?.forEach((item) => item.enable(opts));
    this.criteriaMeasurementComponents?.forEach((item) => item.enable(opts));
    this.criteriaPhotoComponents?.forEach((item) => item.enable(opts));
    this.criteriaCampaignComponents?.forEach((item) => item.enable(opts));
    this.criteriaOccasionComponents?.forEach((item) => item.enable(opts));
    this.criteriaEventComponents?.forEach((item) => item.enable(opts));
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.disable(opts);
    this.extractSurveyPeriodForm?.disable();
    this.criteriaSurveyComponents?.forEach((item) => item.disable(opts));
    this.criteriaSamplingOperationComponents?.forEach((item) => item.disable(opts));
    this.criteriaSampleComponents?.forEach((item) => item.disable(opts));
    this.criteriaMeasurementComponents?.forEach((item) => item.disable(opts));
    this.criteriaPhotoComponents?.forEach((item) => item.disable(opts));
    this.criteriaCampaignComponents?.forEach((item) => item.disable(opts));
    this.criteriaOccasionComponents?.forEach((item) => item.disable(opts));
    this.criteriaEventComponents?.forEach((item) => item.disable(opts));
  }

  markAsLoading(opts?: { emitEvent?: boolean }) {
    super.markAsLoading(opts);
    this.criteriaSurveyComponents?.forEach((item) => item.markAsLoading(opts));
    this.criteriaSamplingOperationComponents?.forEach((item) => item.markAsLoading(opts));
    this.criteriaSampleComponents?.forEach((item) => item.markAsLoading(opts));
    this.criteriaMeasurementComponents?.forEach((item) => item.markAsLoading(opts));
    this.criteriaPhotoComponents?.forEach((item) => item.markAsLoading(opts));
    this.criteriaCampaignComponents?.forEach((item) => item.markAsLoading(opts));
    this.criteriaOccasionComponents?.forEach((item) => item.markAsLoading(opts));
    this.criteriaEventComponents?.forEach((item) => item.markAsLoading(opts));
  }

  markAsLoaded(opts?: { emitEvent?: boolean }) {
    super.markAsLoaded(opts);
    this.criteriaSurveyComponents?.forEach((item) => item.markAsLoaded(opts));
    this.criteriaSamplingOperationComponents?.forEach((item) => item.markAsLoaded(opts));
    this.criteriaSampleComponents?.forEach((item) => item.markAsLoaded(opts));
    this.criteriaMeasurementComponents?.forEach((item) => item.markAsLoaded(opts));
    this.criteriaPhotoComponents?.forEach((item) => item.markAsLoaded(opts));
    this.criteriaCampaignComponents?.forEach((item) => item.markAsLoaded(opts));
    this.criteriaOccasionComponents?.forEach((item) => item.markAsLoaded(opts));
    this.criteriaEventComponents?.forEach((item) => item.markAsLoaded(opts));
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaMain): (keyof IExtractCriteriaMain)[] {
    return ExtractCriteriaMain.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaMain): any {
    return undefined;
  }

  refresh() {
    super.refresh();

    // Refresh data criterias
    this.criteriaSurveyComponents?.forEach((item) => item.refresh());
    this.criteriaSamplingOperationComponents?.forEach((item) => item.refresh());
    this.criteriaSampleComponents?.forEach((item) => item.refresh());
    this.criteriaMeasurementComponents?.forEach((item) => item.refresh());
    this.criteriaPhotoComponents?.forEach((item) => item.refresh());
    this.criteriaCampaignComponents?.forEach((item) => item.refresh());
    this.criteriaOccasionComponents?.forEach((item) => item.refresh());
    this.criteriaEventComponents?.forEach((item) => item.refresh());
  }

  refreshComponents() {
    super.refreshComponents();

    // Refresh children components
    this.criteriaSurveyComponents?.forEach((item) => item.refreshComponents());
    this.criteriaSamplingOperationComponents?.forEach((item) => item.refreshComponents());
    this.criteriaSampleComponents?.forEach((item) => item.refreshComponents());
    this.criteriaMeasurementComponents?.forEach((item) => item.refreshComponents());
    this.criteriaPhotoComponents?.forEach((item) => item.refreshComponents());
    this.criteriaCampaignComponents?.forEach((item) => item.refreshComponents());
    this.criteriaOccasionComponents?.forEach((item) => item.refreshComponents());
    this.criteriaEventComponents?.forEach((item) => item.refreshComponents());
  }

  getAllVisibleFilters(): IVisibleFilter {
    const visibleFilter: IVisibleFilter = {};
    visibleFilter.mainFilters = [this.data.visibleFilters];
    visibleFilter.surveyFilters = new Array(this.criteriaSurveyComponents.length);
    this.criteriaSurveyComponents?.forEach((item, index) => (visibleFilter.surveyFilters[index] = item.value.visibleFilters));
    visibleFilter.samplingOperationFilters = new Array(this.criteriaSamplingOperationComponents.length);
    this.criteriaSamplingOperationComponents?.forEach((item, index) => (visibleFilter.samplingOperationFilters[index] = item.value.visibleFilters));
    visibleFilter.sampleFilters = new Array(this.criteriaSampleComponents.length);
    this.criteriaSampleComponents?.forEach((item, index) => (visibleFilter.sampleFilters[index] = item.value.visibleFilters));
    visibleFilter.measurementFilters = new Array(this.criteriaMeasurementComponents.length);
    this.criteriaMeasurementComponents?.forEach((item, index) => (visibleFilter.measurementFilters[index] = item.value.visibleFilters));
    visibleFilter.photoFilters = new Array(this.criteriaPhotoComponents.length);
    this.criteriaPhotoComponents?.forEach((item, index) => (visibleFilter.photoFilters[index] = item.value.visibleFilters));
    visibleFilter.campaignFilters = new Array(this.criteriaCampaignComponents.length);
    this.criteriaCampaignComponents?.forEach((item, index) => (visibleFilter.campaignFilters[index] = item.value.visibleFilters));
    visibleFilter.occasionFilters = new Array(this.criteriaOccasionComponents.length);
    this.criteriaOccasionComponents?.forEach((item, index) => (visibleFilter.occasionFilters[index] = item.value.visibleFilters));
    visibleFilter.eventFilters = new Array(this.criteriaEventComponents.length);
    this.criteriaEventComponents?.forEach((item, index) => (visibleFilter.eventFilters[index] = item.value.visibleFilters));
    return visibleFilter;
  }

  // protected methods

  private initHelpers() {
    this.surveyCriteriasHelper = new FormArrayHelper<ExtractCriteriaSurvey>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'surveyCriterias'),
      (value) => this.validatorService.getExtractCriteriaSurveyControl(value),
      ExtractCriteriaSurvey.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.surveyCriteriasHelper.size() === 0) this.surveyCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_DATA_SURVEY.arrayHelper = this.surveyCriteriasHelper;

    this.samplingOperationCriteriasHelper = new FormArrayHelper<ExtractCriteriaSamplingOperation>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'samplingOperationCriterias'),
      (value) => this.validatorService.getExtractCriteriaSamplingOperationControl(value),
      ExtractCriteriaSamplingOperation.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.samplingOperationCriteriasHelper.size() === 0) this.samplingOperationCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_DATA_SAMPLING_OPERATION.arrayHelper = this.samplingOperationCriteriasHelper;

    this.sampleCriteriasHelper = new FormArrayHelper<ExtractCriteriaSample>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'sampleCriterias'),
      (value) => this.validatorService.getExtractCriteriaSampleControl(value),
      ExtractCriteriaSample.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.sampleCriteriasHelper.size() === 0) this.sampleCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_DATA_SAMPLE.arrayHelper = this.sampleCriteriasHelper;

    this.measurementCriteriasHelper = new FormArrayHelper<ExtractCriteriaMeasurement>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'measurementCriterias'),
      (value) => this.validatorService.getExtractCriteriaMeasurementControl(value),
      ExtractCriteriaMeasurement.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.measurementCriteriasHelper.size() === 0) this.measurementCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_DATA_MEASUREMENT.arrayHelper = this.measurementCriteriasHelper;

    this.photoCriteriasHelper = new FormArrayHelper<ExtractCriteriaPhoto>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'photoCriterias'),
      (value) => this.validatorService.getExtractCriteriaPhotoControl(value),
      ExtractCriteriaPhoto.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.photoCriteriasHelper.size() === 0) this.photoCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_DATA_PHOTO.arrayHelper = this.photoCriteriasHelper;

    this.campaignCriteriasHelper = new FormArrayHelper<ExtractCriteriaCampaign>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'campaignCriterias'),
      (value) => this.validatorService.getExtractCriteriaCampaignControl(value),
      ExtractCriteriaCampaign.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.campaignCriteriasHelper.size() === 0) this.campaignCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_CAMPAIGN.arrayHelper = this.campaignCriteriasHelper;

    this.occasionCriteriasHelper = new FormArrayHelper<ExtractCriteriaOccasion>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'occasionCriterias'),
      (value) => this.validatorService.getExtractCriteriaOccasionControl(value),
      ExtractCriteriaOccasion.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.occasionCriteriasHelper.size() === 0) this.occasionCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_OCCASION.arrayHelper = this.occasionCriteriasHelper;

    this.eventCriteriasHelper = new FormArrayHelper<ExtractCriteriaEvent>(
      FormArrayHelper.getOrCreateArray(this.formBuilder, this.form, 'eventCriterias'),
      (value) => this.validatorService.getExtractCriteriaEventControl(value),
      ExtractCriteriaEvent.equals,
      () => false,
      { allowEmptyArray: true }
    );
    if (this.eventCriteriasHelper.size() === 0) this.eventCriteriasHelper.resize(1);
    this.filterTypeDefinition.EXTRACT_EVENT.arrayHelper = this.eventCriteriasHelper;

    this.markForCheck();
  }

  private updateFilterCount() {
    for (const filterType of this.allFilterTypes) {
      if (filterType === 'EXTRACT_DATA_PERIOD') continue;
      const definition: IFilterDefinition = this.filterTypeDefinition[filterType];
      let hasVisibleFilters = false;
      if (filterType === 'EXTRACT_DATA_MAIN') {
        hasVisibleFilters = isNotEmptyArray(this.visibleFilters) || isNotEmptyArray(this.computeVisibleFiltersFromValue(this.data));
      } else {
        for (let i = 0; i < definition.arrayHelper.size(); i++) {
          const component: ExtractCriteriaComponent<any, any> = this.getCriteriaComponent(filterType, i);
          hasVisibleFilters =
            hasVisibleFilters ||
            isNotEmptyArray(component.visibleFilters) ||
            isNotEmptyArray(component.computeVisibleFiltersFromValue(definition.arrayHelper.at(i).value));
        }
      }
      definition.hasVisibleFilters = hasVisibleFilters;
    }
  }

  private getCriteriaComponent(
    type: FilterType,
    index: number
  ):
    | ExtractCriteriaSurveyComponent
    | ExtractCriteriaSamplingOperationComponent
    | ExtractCriteriaSampleComponent
    | ExtractCriteriaMeasurementComponent
    | ExtractCriteriaPhotoComponent
    | ExtractCriteriaCampaignComponent
    | ExtractCriteriaOccasionComponent
    | ExtractCriteriaEventComponent {
    switch (type) {
      case 'EXTRACT_DATA_SURVEY':
        return this.criteriaSurveyComponents?.find((item) => item.tabindex === index);
      case 'EXTRACT_DATA_SAMPLING_OPERATION':
        return this.criteriaSamplingOperationComponents?.find((item) => item.tabindex === index);
      case 'EXTRACT_DATA_SAMPLE':
        return this.criteriaSampleComponents?.find((item) => item.tabindex === index);
      case 'EXTRACT_DATA_MEASUREMENT':
        return this.criteriaMeasurementComponents?.find((item) => item.tabindex === index);
      case 'EXTRACT_DATA_PHOTO':
        return this.criteriaPhotoComponents?.find((item) => item.tabindex === index);
      case 'EXTRACT_CAMPAIGN':
        return this.criteriaCampaignComponents?.find((item) => item.tabindex === index);
      case 'EXTRACT_OCCASION':
        return this.criteriaOccasionComponents?.find((item) => item.tabindex === index);
      case 'EXTRACT_EVENT':
        return this.criteriaEventComponents?.find((item) => item.tabindex === index);
      default:
        throw new Error(`${this.logPrefix} filter type ${type} not managed`);
    }
  }

  private getExpansionPanel(filterType: FilterType): MatExpansionPanel {
    return this.expansionPanelsByFilterType.find((item) => item.filterType === filterType)?.matExpansionPanel;
  }

  private getExpansionPanelRef(filterType: FilterType): ElementRef {
    return this.expansionPanelsByFilterType.find((item) => item.filterType === filterType)?.element;
  }

  private addFilterBlock(type: FilterType) {
    switch (type) {
      case 'EXTRACT_DATA_SURVEY':
        this.addSurveyFilterBlock();
        break;
      case 'EXTRACT_DATA_SAMPLING_OPERATION':
        this.addSamplingOperationFilterBlock();
        break;
      case 'EXTRACT_DATA_SAMPLE':
        this.addSampleFilterBlock();
        break;
      case 'EXTRACT_DATA_MEASUREMENT':
        this.addMeasurementFilterBlock();
        break;
      case 'EXTRACT_DATA_PHOTO':
        this.addPhotoFilterBlock();
        break;
      case 'EXTRACT_CAMPAIGN':
        this.addCampaignFilterBlock();
        break;
      case 'EXTRACT_OCCASION':
        this.addOccasionFilterBlock();
        break;
      case 'EXTRACT_EVENT':
        this.addEventFilterBlock();
        break;
      default:
        throw new Error(`${this.logPrefix} filter type ${type} not managed`);
    }
  }

  private addSurveyFilterBlock() {
    this.surveyCriteriasHelper.add();
    this.detectChanges();
    this.criteriaSurveyComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }

  private addSamplingOperationFilterBlock() {
    this.samplingOperationCriteriasHelper.add();
    this.detectChanges();
    this.criteriaSamplingOperationComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }

  private addSampleFilterBlock() {
    this.sampleCriteriasHelper.add();
    this.detectChanges();
    this.criteriaSampleComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }

  private addMeasurementFilterBlock() {
    this.measurementCriteriasHelper.add();
    this.detectChanges();
    this.criteriaMeasurementComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }

  private addPhotoFilterBlock() {
    this.photoCriteriasHelper.add();
    this.detectChanges();
    this.criteriaPhotoComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }

  private addCampaignFilterBlock() {
    this.campaignCriteriasHelper.add();
    this.detectChanges();
    this.criteriaCampaignComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }

  private addOccasionFilterBlock() {
    this.occasionCriteriasHelper.add();
    this.detectChanges();
    this.criteriaOccasionComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }

  private addEventFilterBlock() {
    this.eventCriteriasHelper.add();
    this.detectChanges();
    this.criteriaEventComponents?.forEach((item) => {
      item.refresh();
      item.enable();
    });
  }
}
