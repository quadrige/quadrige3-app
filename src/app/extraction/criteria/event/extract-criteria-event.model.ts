import { capitalizeFirstLetter, isEmptyArray, isNil } from '@sumaris-net/ngx-components';
import { GeometryType } from '@app/referential/monitoring-location/geometry-type';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValue, IDateFilterValue } from '@app/referential/component/date-filter-form-field.component';
import { IntReferentialFilterCriteria, IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Utils } from '@app/shared/utils';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent) =>
  ExtractUtils.getValue<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property, newValue);
const getTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent) =>
  ExtractUtils.getTextFilter<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property);
const setTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent, newValue: ITextFilterValue) =>
  ExtractUtils.setTextFilter<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property, newValue);
const getDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent) =>
  ExtractUtils.getDateFilter<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property);
const setDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent, newValue: IDateFilterValue) =>
  ExtractUtils.setDateFilter<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent) =>
  ExtractUtils.getValues<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaEvent) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaEvent,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaEvent>(criterias, eventCriteriaDefinition, property, referentialFilter);

export const eventFilters: (keyof IExtractCriteriaEvent)[] = [
  'typeFilter',
  'description',
  'startDate',
  'endDate',
  // 'geometryType', // TODO uncomment when geometry type is wanted
  'recorderDepartmentFilter',
];

export interface IExtractCriteriaEvent extends IExtractCriteria {
  typeFilter: IReferentialFilterCriteria<number>;
  description: ITextFilterValue;
  startDate: IDateFilterValue;
  endDate: IDateFilterValue;
  geometryType: GeometryType;
  recorderDepartmentFilter: IReferentialFilterCriteria<number>;
}

export const eventCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaEvent> = {
  typeFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.EVENT_TYPE',
    valuesCriteriaType: 'EXTRACT_EVENT_TYPE_ID',
    valuesOperatorType: 'EVENT_TYPE_EQUAL',
    valueCriteriaType: 'EXTRACT_EVENT_TYPE_NAME',
  },
  description: {
    i18nLabel: 'DATA.EVENT.DESCRIPTION',
    valueCriteriaType: 'EXTRACT_EVENT_DESCRIPTION',
  },
  startDate: {
    i18nLabel: 'DATA.EVENT.START_DATE',
    valuesCriteriaType: 'EXTRACT_EVENT_START_DATE',
  },
  endDate: {
    i18nLabel: 'DATA.EVENT.END_DATE',
    valuesCriteriaType: 'EXTRACT_EVENT_END_DATE',
  },
  geometryType: {
    i18nLabel: 'DATA.EVENT.GEOMETRY_TYPE',
    valuesI18Labels: {
      point: 'REFERENTIAL.GEOMETRY_TYPE.POINT',
      line: 'REFERENTIAL.GEOMETRY_TYPE.LINE',
      area: 'REFERENTIAL.GEOMETRY_TYPE.AREA',
    },
    valueCriteriaType: 'EXTRACT_EVENT_GEOMETRY_TYPE',
    valueOperatorType: 'GEOMETRY_TYPE_EQUAL',
  },
  recorderDepartmentFilter: {
    i18nLabel: 'DATA.EVENT.RECORDER_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_EVENT_RECORDER_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_WITH_DEFAULT_IN',
    valueCriteriaType: 'EXTRACT_EVENT_RECORDER_DEPARTMENT_NAME',
  },
};

export class ExtractCriteriaEvent implements IExtractCriteriaEvent {
  id = null;
  typeFilter: IntReferentialFilterCriteria;
  description = TextFilterValue.default();
  startDate = DateFilterValue.default();
  endDate = DateFilterValue.default();
  geometryType: GeometryType;
  recorderDepartmentFilter: IntReferentialFilterCriteria;
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaEvent): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaEvent.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaEvent, c2: ExtractCriteriaEvent): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        Utils.equals(c1.typeFilter, c2.typeFilter) &&
        TextFilterValue.equals(c1.description, c2.description) &&
        Utils.equals(c1.recorderDepartmentFilter, c2.recorderDepartmentFilter) &&
        DateFilterValue.equals(c1.startDate, c2.startDate) &&
        DateFilterValue.equals(c1.endDate, c2.endDate) &&
        c1.geometryType === c2.geometryType)
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaEvent {
    return {
      id,
      typeFilter: getReferentialFilterCriteria(criterias, 'typeFilter'),
      description: getTextFilter(criterias, 'description'),
      recorderDepartmentFilter: getReferentialFilterCriteria(criterias, 'recorderDepartmentFilter'),
      startDate: getDateFilter(criterias, 'startDate'),
      endDate: getDateFilter(criterias, 'endDate'),
      geometryType: getValue(criterias, 'geometryType').value?.toUpperCase(),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaEvent, opts: IMergeCriteriaOptions) {
    setReferentialFilterCriteria(criterias, 'typeFilter', IntReferentialFilterCriteria.fromObject(value?.typeFilter));
    setTextFilter(criterias, 'description', value?.description);
    setReferentialFilterCriteria(criterias, 'recorderDepartmentFilter', IntReferentialFilterCriteria.fromObject(value?.recorderDepartmentFilter));
    setDateFilter(criterias, 'startDate', value?.startDate);
    setDateFilter(criterias, 'endDate', value?.endDate);
    setValue(criterias, 'geometryType', { value: capitalizeFirstLetter(value?.geometryType?.toLowerCase()) });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaEvent): (keyof IExtractCriteriaEvent)[] {
    const properties: (keyof IExtractCriteriaEvent)[] = [];
    if (criteria) {
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.typeFilter, true)) properties.push('typeFilter');
      if (!TextFilterValue.isEmpty(criteria.description)) properties.push('description');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.recorderDepartmentFilter, true)) properties.push('recorderDepartmentFilter');
      if (!DateFilterValue.isEmpty(criteria.startDate)) properties.push('startDate');
      if (!DateFilterValue.isEmpty(criteria.endDate)) properties.push('endDate');
      if (!!criteria.geometryType) properties.push('geometryType');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaEvent): any {
    switch (property) {
      case 'startDate':
        return DateFilterValue.default();
      case 'endDate':
        return DateFilterValue.default();
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaEvent {
    const target = new ExtractCriteriaEvent();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.typeFilter = IntReferentialFilterCriteria.fromObject(source.typeFilter || {});
    this.description = source.description || TextFilterValue.default();
    this.recorderDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.recorderDepartmentFilter || {});
    this.startDate = DateFilterValue.fromObject(source.startDate);
    this.endDate = DateFilterValue.fromObject(source.endDate);
    this.geometryType = source.geometryType;
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target: IExtractCriteriaEvent = { ...this };
    target.typeFilter = this.typeFilter?.asObject();
    target.recorderDepartmentFilter = this.recorderDepartmentFilter?.asObject();
    target.startDate = this.startDate?.asObject();
    target.endDate = this.endDate?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
