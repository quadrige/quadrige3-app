import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaEvent, IExtractCriteriaEvent } from '@app/extraction/criteria/event/extract-criteria-event.model';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { TextFilterValidator } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValidator } from '@app/referential/component/date-filter-form-field.component';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaEventValidatorService extends BaseValidatorService<ExtractCriteriaEvent> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected textFilterValidator: TextFilterValidator,
    protected dateFilterValidator: DateFilterValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaEvent, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaEvent, any>>{
      id: [data?.id || null],
      typeFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.typeFilter),
      description: this.textFilterValidator.getFormGroup(data?.description),
      startDate: this.dateFilterValidator.getFormGroup(data?.startDate),
      endDate: this.dateFilterValidator.getFormGroup(data?.endDate),
      geometryType: [data?.geometryType || null],
      recorderDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.recorderDepartmentFilter),
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
