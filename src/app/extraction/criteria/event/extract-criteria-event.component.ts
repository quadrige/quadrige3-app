import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import {
  eventCriteriaDefinition,
  eventFilters,
  ExtractCriteriaEvent,
  IExtractCriteriaEvent,
} from '@app/extraction/criteria/event/extract-criteria-event.model';
import { geometryTypes } from '@app/referential/monitoring-location/geometry-type';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';

@Component({
  selector: 'app-extract-criteria-event-component',
  templateUrl: './extract-criteria-event.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaEventComponent extends ExtractCriteriaComponent<ExtractCriteriaEvent, IExtractCriteriaEvent> {
  criteriaDefinition = eventCriteriaDefinition;
  possibleFilters = eventFilters;
  geometryTypes = geometryTypes;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaEvent);
    this.logPrefix = '[extract-criteria-event]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaEvent): (keyof IExtractCriteriaEvent)[] {
    return ExtractCriteriaEvent.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaEvent): any {
    return ExtractCriteriaEvent.defaultValue(property);
  }
}
