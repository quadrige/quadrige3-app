import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaPhoto, IExtractCriteriaPhoto } from '@app/extraction/criteria/photo/extract-criteria-photo.model';
import { ExtractCriteriaPhotoResolutionValidatorService } from '@app/extraction/criteria/photo-resolution/extract-criteria-photo-resolution.validator';
import { ExtractCriteriaAcquisitionLevelValidatorService } from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.validator';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { toBoolean } from '@sumaris-net/ngx-components';
import { TextFilterValidator } from '@app/referential/component/text-filter-form-field.component';
import { ExtractCriteriaQualityFlagValidatorService } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.validator';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaPhotoValidatorService extends BaseValidatorService<ExtractCriteriaPhoto> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected resolutionValidator: ExtractCriteriaPhotoResolutionValidatorService,
    protected acquisitionLevelValidator: ExtractCriteriaAcquisitionLevelValidatorService,
    protected qualityFlagValidator: ExtractCriteriaQualityFlagValidatorService,
    protected textFilterValidator: TextFilterValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaPhoto, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaPhoto, any>>{
      id: [data?.id || null],
      included: [toBoolean(data?.included, false)],
      acquisitionLevel: this.acquisitionLevelValidator.getFormGroup(data?.acquisitionLevel),
      name: this.textFilterValidator.getFormGroup(data?.name),
      photoTypeFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.photoTypeFilter),
      resolution: this.resolutionValidator.getFormGroup(data?.resolution),
      validated: [toBoolean(data?.validated)],
      qualifications: this.qualityFlagValidator.getFormGroup(data?.qualifications),
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
