import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import {
  ExtractCriteriaPhoto,
  IExtractCriteriaPhoto,
  photoCriteriaDefinition,
  photoFilters,
} from '@app/extraction/criteria/photo/extract-criteria-photo.model';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';

@Component({
  selector: 'app-extract-criteria-photo-component',
  templateUrl: './extract-criteria-photo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaPhotoComponent extends ExtractCriteriaComponent<ExtractCriteriaPhoto, IExtractCriteriaPhoto> {
  criteriaDefinition = photoCriteriaDefinition;
  possibleFilters = photoFilters;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaPhoto);
    this.logPrefix = '[extract-criteria-photo]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaPhoto): (keyof IExtractCriteriaPhoto)[] {
    return ExtractCriteriaPhoto.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaPhoto): any {
    return ExtractCriteriaPhoto.defaultValue(property);
  }
}
