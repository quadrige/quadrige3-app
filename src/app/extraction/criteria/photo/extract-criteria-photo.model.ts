import { isEmptyArray, isNil, isNotNil } from '@sumaris-net/ngx-components';
import { IReferentialFilterCriteria, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import {
  ExtractCriteriaPhotoResolution,
  IExtractCriteriaPhotoResolution,
  photoResolutionI18nLabels,
  photoResolutionI18nPrefix,
} from '../photo-resolution/extract-criteria-photo-resolution.model';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import {
  acquisitionLevelI18nLabels,
  acquisitionLevelI18nPrefix,
  ExtractCriteriaAcquisitionLevel,
  IExtractCriteriaAcquisitionLevel,
} from '@app/extraction/criteria/acquisition-level/extract-criteria-acquisition-level.model';
import { Utils } from '@app/shared/utils';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';
import {
  ExtractCriteriaQualityFlag,
  IExtractCriteriaQualityFlag,
  photoQualityFlagI18nLabelPrefix,
  qualityFlagI18nLabels,
} from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaPhoto) =>
  ExtractUtils.getValue(criterias, photoCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaPhoto, newValue: FilterValue) =>
  ExtractUtils.setValue(criterias, photoCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaPhoto) =>
  ExtractUtils.getValues(criterias, photoCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaPhoto, newValues: FilterValues) =>
  ExtractUtils.setValues(criterias, photoCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaPhoto) =>
  ExtractUtils.getReferentialFilterCriteria(criterias, photoCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaPhoto,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria(criterias, photoCriteriaDefinition, property, referentialFilter);
const getTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaPhoto) =>
  ExtractUtils.getTextFilter(criterias, photoCriteriaDefinition, property);
const setTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaPhoto, newValue: ITextFilterValue) =>
  ExtractUtils.setTextFilter(criterias, photoCriteriaDefinition, property, newValue);

export const photoFilters: (keyof IExtractCriteriaPhoto)[] = [
  'included',
  'name',
  'photoTypeFilter',
  'resolution',
  'acquisitionLevel',
  'validated',
  'qualifications',
];

export interface IExtractCriteriaPhoto extends IExtractCriteria {
  included: boolean;
  name: ITextFilterValue;
  photoTypeFilter: IReferentialFilterCriteria<string>;
  resolution: IExtractCriteriaPhotoResolution;
  acquisitionLevel?: IExtractCriteriaAcquisitionLevel;
  validated: boolean;
  qualifications: IExtractCriteriaQualityFlag;
}

export const photoCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaPhoto> = {
  included: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.MAIN.WITH_PHOTOS',
    valueCriteriaType: 'EXTRACT_RESULT_PHOTO_INCLUDED',
    valueOperatorType: 'BOOLEAN',
  },
  acquisitionLevel: {
    i18nLabel: acquisitionLevelI18nPrefix + 'TITLE',
    i18nLabelPrefix: acquisitionLevelI18nPrefix,
    valuesI18Labels: acquisitionLevelI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL',
  },
  name: {
    i18nLabel: 'DATA.PHOTO.NAME',
    valueCriteriaType: 'EXTRACT_RESULT_PHOTO_NAME',
  },
  photoTypeFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.PHOTO_TYPE',
    valuesCriteriaType: 'EXTRACT_RESULT_PHOTO_TYPE_ID',
    valuesOperatorType: 'PHOTO_TYPE_EQUAL',
    valueCriteriaType: 'EXTRACT_RESULT_PHOTO_TYPE_NAME',
  },
  resolution: {
    i18nLabel: photoResolutionI18nPrefix + 'TITLE',
    i18nLabelPrefix: photoResolutionI18nPrefix,
    valuesI18Labels: photoResolutionI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_PHOTO_RESOLUTION_TYPE',
  },
  validated: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.VALIDATED',
    valueCriteriaType: 'EXTRACT_RESULT_PHOTO_VALIDATION',
    valueOperatorType: 'BOOLEAN',
  },
  qualifications: {
    i18nLabel: photoQualityFlagI18nLabelPrefix + 'TITLE',
    i18nLabelPrefix: photoQualityFlagI18nLabelPrefix,
    valuesI18Labels: qualityFlagI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_PHOTO_QUALIFICATION',
  },
};

export class ExtractCriteriaPhoto implements IExtractCriteriaPhoto {
  id = null;
  included = false;
  acquisitionLevel = ExtractCriteriaAcquisitionLevel.default();
  name = TextFilterValue.default();
  photoTypeFilter: StrReferentialFilterCriteria;
  resolution = ExtractCriteriaPhotoResolution.default();
  validated = null;
  qualifications = ExtractCriteriaQualityFlag.default();
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaPhoto): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaPhoto.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaPhoto, c2: ExtractCriteriaPhoto): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        c1.included === c2.included &&
        ExtractCriteriaAcquisitionLevel.equals(c1.acquisitionLevel, c2.acquisitionLevel) &&
        TextFilterValue.equals(c1.name, c2.name) &&
        Utils.equals(c1.photoTypeFilter, c2.photoTypeFilter) &&
        ExtractCriteriaPhotoResolution.equals(c1.resolution, c2.resolution) &&
        c1.validated === c2.validated &&
        ExtractCriteriaQualityFlag.equals(c1.qualifications, c2.qualifications))
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaPhoto {
    return {
      id,
      included: Utils.fromBooleanString(getValue(criterias, 'included').value, false),
      acquisitionLevel: ExtractCriteriaAcquisitionLevel.fromValues(getValues(criterias, 'acquisitionLevel').values),
      name: getTextFilter(criterias, 'name'),
      photoTypeFilter: getReferentialFilterCriteria(criterias, 'photoTypeFilter'),
      resolution: ExtractCriteriaPhotoResolution.fromValues(getValues(criterias, 'resolution').values),
      validated: Utils.fromOptionalBooleanString(getValue(criterias, 'validated').value),
      qualifications: ExtractCriteriaQualityFlag.fromValues(getValues(criterias, 'qualifications').values),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaPhoto, opts: IMergeCriteriaOptions) {
    setValue(criterias, 'included', { value: Utils.toBooleanString(value?.included) });
    setValues(criterias, 'acquisitionLevel', { values: ExtractCriteriaAcquisitionLevel.toValues(value?.acquisitionLevel, opts.defaultIfEmpty) });
    setTextFilter(criterias, 'name', value?.name);
    setReferentialFilterCriteria(criterias, 'photoTypeFilter', value?.photoTypeFilter);
    setValues(criterias, 'resolution', { values: ExtractCriteriaPhotoResolution.toValues(value?.resolution, opts.defaultIfEmpty) });
    setValue(criterias, 'validated', {
      value: isNotNil(value?.validated) ? Utils.toBooleanString(value?.validated) : null,
    });
    setValues(criterias, 'qualifications', { values: ExtractCriteriaQualityFlag.toValues(value?.qualifications, opts.defaultIfEmpty) });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaPhoto): (keyof IExtractCriteriaPhoto)[] {
    const properties: (keyof IExtractCriteriaPhoto)[] = [];
    if (criteria) {
      if (criteria.included) properties.push('included');
      if (!ExtractCriteriaAcquisitionLevel.isEmptyOrDefault(criteria.acquisitionLevel)) properties.push('acquisitionLevel');
      if (!TextFilterValue.isEmpty(criteria.name)) properties.push('name');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.photoTypeFilter, true)) properties.push('photoTypeFilter');
      if (!ExtractCriteriaPhotoResolution.isEmptyOrDefault(criteria.resolution)) properties.push('resolution');
      if (isNotNil(criteria.validated)) properties.push('validated');
      if (!ExtractCriteriaQualityFlag.isEmptyOrDefault(criteria.qualifications)) properties.push('qualifications');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaPhoto): any {
    switch (property) {
      case 'name':
        return TextFilterValue.default();
      case 'acquisitionLevel':
        return ExtractCriteriaAcquisitionLevel.default();
      case 'resolution':
        return ExtractCriteriaPhotoResolution.default();
      case 'qualifications':
        return ExtractCriteriaQualityFlag.default();
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaPhoto {
    const target = new ExtractCriteriaPhoto();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.included = source.included;
    this.acquisitionLevel = ExtractCriteriaAcquisitionLevel.fromObject(source.acquisitionLevel);
    this.name = source.name || TextFilterValue.default();
    this.photoTypeFilter = StrReferentialFilterCriteria.fromObject(source.photoTypeFilter || {});
    this.resolution = ExtractCriteriaPhotoResolution.fromObject(source.resolution);
    this.validated = source.validated;
    this.qualifications = ExtractCriteriaQualityFlag.fromObject(source.qualifications);
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target = { ...this };
    target.acquisitionLevel = this.acquisitionLevel?.asObject();
    target.photoTypeFilter = this.photoTypeFilter?.asObject();
    target.resolution = this.resolution?.asObject();
    target.qualifications = this.qualifications?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
