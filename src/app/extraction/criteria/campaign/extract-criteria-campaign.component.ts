import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import {
  campaignCriteriaDefinition,
  campaignFilters,
  ExtractCriteriaCampaign,
  IExtractCriteriaCampaign,
} from '@app/extraction/criteria/campaign/extract-criteria-campaign.model';
import { geometryTypes } from '@app/referential/monitoring-location/geometry-type';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';

@Component({
  selector: 'app-extract-criteria-campaign-component',
  templateUrl: './extract-criteria-campaign.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaCampaignComponent extends ExtractCriteriaComponent<ExtractCriteriaCampaign, IExtractCriteriaCampaign> {
  criteriaDefinition = campaignCriteriaDefinition;
  possibleFilters = campaignFilters;
  geometryTypes = geometryTypes;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaCampaign);
    this.logPrefix = '[extract-criteria-campaign]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaCampaign): (keyof IExtractCriteriaCampaign)[] {
    return ExtractCriteriaCampaign.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaCampaign): any {
    return ExtractCriteriaCampaign.defaultValue(property);
  }
}
