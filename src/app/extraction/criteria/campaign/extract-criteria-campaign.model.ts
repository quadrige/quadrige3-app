import { capitalizeFirstLetter, isEmptyArray, isNil } from '@sumaris-net/ngx-components';
import { GeometryType } from '@app/referential/monitoring-location/geometry-type';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValue, IDateFilterValue } from '@app/referential/component/date-filter-form-field.component';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { Utils } from '@app/shared/utils';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign) =>
  ExtractUtils.getValue<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property, newValue);
const getTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign) =>
  ExtractUtils.getTextFilter<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property);
const setTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign, newValue: ITextFilterValue) =>
  ExtractUtils.setTextFilter<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property, newValue);
const getDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign) =>
  ExtractUtils.getDateFilter<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property);
const setDateFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign, newValue: IDateFilterValue) =>
  ExtractUtils.setDateFilter<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign) =>
  ExtractUtils.getValues<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaCampaign) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaCampaign,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaCampaign>(criterias, campaignCriteriaDefinition, property, referentialFilter);

export const campaignFilters: (keyof IExtractCriteriaCampaign)[] = [
  'programFilter',
  'userFilter',
  'campaignFilter',
  'sismer',
  'startDate',
  'endDate',
  // 'geometryType', // TODO uncomment when geometry type is wanted
  'shipFilter',
  'recorderDepartmentFilter',
];

export interface IExtractCriteriaCampaign extends IExtractCriteria {
  programFilter: IReferentialFilterCriteria<string>;
  userFilter: IReferentialFilterCriteria<number>;
  campaignFilter: IReferentialFilterCriteria<number>;
  sismer: ITextFilterValue;
  startDate: IDateFilterValue;
  endDate: IDateFilterValue;
  geometryType: GeometryType;
  shipFilter: IReferentialFilterCriteria<number>;
  recorderDepartmentFilter: IReferentialFilterCriteria<number>;
}

export const campaignCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaCampaign> = {
  programFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.PROGRAM',
    valuesCriteriaType: 'EXTRACT_CAMPAIGN_PROGRAM_ID',
    valuesOperatorType: 'PROGRAM_IN',
    valueCriteriaType: 'EXTRACT_CAMPAIGN_PROGRAM_NAME',
  },
  userFilter: {
    i18nLabel: 'DATA.CAMPAIGN.USER',
    valuesCriteriaType: 'EXTRACT_CAMPAIGN_USER_ID',
    valuesOperatorType: 'USER_IN',
    valueCriteriaType: 'EXTRACT_CAMPAIGN_USER_NAME',
  },
  campaignFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.CAMPAIGN',
    valuesCriteriaType: 'EXTRACT_CAMPAIGN_ID',
    valuesOperatorType: 'CAMPAIGN_IN',
    valueCriteriaType: 'EXTRACT_CAMPAIGN_NAME',
  },
  sismer: {
    i18nLabel: 'DATA.CAMPAIGN.SISMER',
    valueCriteriaType: 'EXTRACT_CAMPAIGN_SISMER_NAME',
    valueOperatorType: 'TEXT_CONTAINS',
  },
  startDate: {
    i18nLabel: 'DATA.CAMPAIGN.START_DATE',
    valuesCriteriaType: 'EXTRACT_CAMPAIGN_START_DATE',
  },
  endDate: {
    i18nLabel: 'DATA.CAMPAIGN.END_DATE',
    valuesCriteriaType: 'EXTRACT_CAMPAIGN_END_DATE',
  },
  geometryType: {
    i18nLabel: 'DATA.CAMPAIGN.GEOMETRY_TYPE',
    valuesI18Labels: {
      point: 'REFERENTIAL.GEOMETRY_TYPE.POINT',
      line: 'REFERENTIAL.GEOMETRY_TYPE.LINE',
      area: 'REFERENTIAL.GEOMETRY_TYPE.AREA',
    },
    valueCriteriaType: 'EXTRACT_CAMPAIGN_GEOMETRY_TYPE',
    valueOperatorType: 'GEOMETRY_TYPE_EQUAL',
  },
  shipFilter: {
    i18nLabel: 'DATA.CAMPAIGN.SHIP',
    valuesCriteriaType: 'EXTRACT_CAMPAIGN_SHIP_ID',
    valuesOperatorType: 'SHIP_IN',
    valueCriteriaType: 'EXTRACT_CAMPAIGN_SHIP_NAME',
  },
  recorderDepartmentFilter: {
    i18nLabel: 'DATA.CAMPAIGN.RECORDER_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_WITH_DEFAULT_IN',
    valueCriteriaType: 'EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_NAME',
  },
};

export class ExtractCriteriaCampaign implements IExtractCriteriaCampaign {
  id = null;
  programFilter: StrReferentialFilterCriteria;
  userFilter: IntReferentialFilterCriteria;
  campaignFilter: IntReferentialFilterCriteria;
  sismer = TextFilterValue.default('TEXT_CONTAINS');
  startDate = DateFilterValue.default();
  endDate = DateFilterValue.default();
  geometryType: GeometryType;
  shipFilter: IntReferentialFilterCriteria;
  recorderDepartmentFilter: IntReferentialFilterCriteria;
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaCampaign): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaCampaign.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaCampaign, c2: ExtractCriteriaCampaign): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        Utils.equals(c1.programFilter, c2.programFilter) &&
        Utils.equals(c1.userFilter, c2.userFilter) &&
        Utils.equals(c1.campaignFilter, c2.campaignFilter) &&
        TextFilterValue.equals(c1.sismer, c2.sismer) &&
        Utils.equals(c1.shipFilter, c2.shipFilter) &&
        Utils.equals(c1.recorderDepartmentFilter, c2.recorderDepartmentFilter) &&
        DateFilterValue.equals(c1.startDate, c2.startDate) &&
        DateFilterValue.equals(c1.endDate, c2.endDate) &&
        c1.geometryType === c2.geometryType)
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaCampaign {
    return {
      id,
      programFilter: getReferentialFilterCriteria(criterias, 'programFilter'),
      userFilter: getReferentialFilterCriteria(criterias, 'userFilter'),
      campaignFilter: getReferentialFilterCriteria(criterias, 'campaignFilter'),
      sismer: getTextFilter(criterias, 'sismer'),
      shipFilter: getReferentialFilterCriteria(criterias, 'shipFilter'),
      recorderDepartmentFilter: getReferentialFilterCriteria(criterias, 'recorderDepartmentFilter'),
      startDate: getDateFilter(criterias, 'startDate'),
      endDate: getDateFilter(criterias, 'endDate'),
      geometryType: getValue(criterias, 'geometryType').value?.toUpperCase(),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaCampaign, opts: IMergeCriteriaOptions) {
    setReferentialFilterCriteria(criterias, 'programFilter', StrReferentialFilterCriteria.fromObject(value?.programFilter));
    setReferentialFilterCriteria(criterias, 'userFilter', IntReferentialFilterCriteria.fromObject(value?.userFilter));
    setReferentialFilterCriteria(criterias, 'campaignFilter', IntReferentialFilterCriteria.fromObject(value?.campaignFilter));
    setTextFilter(criterias, 'sismer', value?.sismer);
    setReferentialFilterCriteria(criterias, 'shipFilter', IntReferentialFilterCriteria.fromObject(value?.shipFilter));
    setReferentialFilterCriteria(criterias, 'recorderDepartmentFilter', IntReferentialFilterCriteria.fromObject(value?.recorderDepartmentFilter));
    setDateFilter(criterias, 'startDate', value?.startDate);
    setDateFilter(criterias, 'endDate', value?.endDate);
    setValue(criterias, 'geometryType', { value: capitalizeFirstLetter(value?.geometryType?.toLowerCase()) });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaCampaign): (keyof IExtractCriteriaCampaign)[] {
    const properties: (keyof IExtractCriteriaCampaign)[] = [];
    if (criteria) {
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.programFilter, true)) properties.push('programFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.userFilter, true)) properties.push('userFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.campaignFilter, true)) properties.push('campaignFilter');
      if (!TextFilterValue.isEmpty(criteria.sismer)) properties.push('sismer');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.shipFilter, true)) properties.push('shipFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.recorderDepartmentFilter, true)) properties.push('recorderDepartmentFilter');
      if (!DateFilterValue.isEmpty(criteria.startDate)) properties.push('startDate');
      if (!DateFilterValue.isEmpty(criteria.endDate)) properties.push('endDate');
      if (!!criteria.geometryType) properties.push('geometryType');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaCampaign): any {
    switch (property) {
      case 'startDate':
        return DateFilterValue.default();
      case 'endDate':
        return DateFilterValue.default();
      case 'sismer':
        return TextFilterValue.default('TEXT_CONTAINS');
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaCampaign {
    const target = new ExtractCriteriaCampaign();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.userFilter = IntReferentialFilterCriteria.fromObject(source.userFilter || {});
    this.campaignFilter = IntReferentialFilterCriteria.fromObject(source.campaignFilter || {});
    this.sismer = source.sismer || TextFilterValue.default('TEXT_CONTAINS');
    this.shipFilter = IntReferentialFilterCriteria.fromObject(source.shipFilter || {});
    this.recorderDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.recorderDepartmentFilter || {});
    this.startDate = DateFilterValue.fromObject(source.startDate);
    this.endDate = DateFilterValue.fromObject(source.endDate);
    this.geometryType = source.geometryType;
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target: IExtractCriteriaCampaign = { ...this };
    target.programFilter = this.programFilter?.asObject();
    target.userFilter = this.userFilter?.asObject();
    target.campaignFilter = this.campaignFilter?.asObject();
    target.shipFilter = this.shipFilter?.asObject();
    target.recorderDepartmentFilter = this.recorderDepartmentFilter?.asObject();
    target.startDate = this.startDate?.asObject();
    target.endDate = this.endDate?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
