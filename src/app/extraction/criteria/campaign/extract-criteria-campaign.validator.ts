import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ExtractCriteriaCampaign, IExtractCriteriaCampaign } from '@app/extraction/criteria/campaign/extract-criteria-campaign.model';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { TextFilterValidator } from '@app/referential/component/text-filter-form-field.component';
import { DateFilterValidator } from '@app/referential/component/date-filter-form-field.component';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaCampaignValidatorService extends BaseValidatorService<ExtractCriteriaCampaign> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected textFilterValidator: TextFilterValidator,
    protected dateFilterValidator: DateFilterValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaCampaign, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaCampaign, any>>{
      id: [data?.id || null],
      programFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.programFilter),
      userFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.userFilter),
      campaignFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.campaignFilter),
      sismer: this.textFilterValidator.getFormGroup(data?.sismer),
      startDate: this.dateFilterValidator.getFormGroup(data?.startDate),
      endDate: this.dateFilterValidator.getFormGroup(data?.endDate),
      geometryType: [data?.geometryType || null],
      shipFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.shipFilter),
      recorderDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.recorderDepartmentFilter),
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
