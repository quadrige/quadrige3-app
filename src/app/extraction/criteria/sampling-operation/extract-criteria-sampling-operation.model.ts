import { capitalizeFirstLetter, isEmptyArray, isNil, isNotNil, isNotNilOrNaN } from '@sumaris-net/ngx-components';
import { GeometryType } from '@app/referential/monitoring-location/geometry-type';
import { IntReferentialFilterCriteria, IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { ExtractUtils } from '@app/extraction/extract.utils';
import {
  dataQualityFlagI18nLabelPrefix,
  ExtractCriteriaQualityFlag,
  IExtractCriteriaQualityFlag,
  qualityFlagI18nLabels,
} from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.model';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { INumberFilterValue, NumberFilterValue } from '@app/referential/component/number-filter-form-field.component';
import { IExtractCriteria } from '@app/extraction/criteria/extract-criteria.model';
import { ITextFilterValue, TextFilterValue } from '@app/referential/component/text-filter-form-field.component';
import { ITimeFilterValue, TimeFilterValue } from '@app/referential/component/time-filter-form-field.component';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { Utils } from '@app/shared/utils';
import { IMergeCriteriaOptions } from '@app/extraction/extract-filter.model';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation) =>
  ExtractUtils.getValue<IExtractCriteriaSamplingOperation>(criterias, samplingOperationCriteriaDefinition, property);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractCriteriaSamplingOperation>(criterias, samplingOperationCriteriaDefinition, property, newValue);
const getValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation) =>
  ExtractUtils.getValues<IExtractCriteriaSamplingOperation>(criterias, samplingOperationCriteriaDefinition, property);
const setValues = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation, newValues: FilterValues) =>
  ExtractUtils.setValues<IExtractCriteriaSamplingOperation>(criterias, samplingOperationCriteriaDefinition, property, newValues);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractCriteriaSamplingOperation>(criterias, samplingOperationCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractCriteriaSamplingOperation,
  referentialFilter: IReferentialFilterCriteria<any>
) =>
  ExtractUtils.setReferentialFilterCriteria<IExtractCriteriaSamplingOperation>(
    criterias,
    samplingOperationCriteriaDefinition,
    property,
    referentialFilter
  );
const getNumberFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation) =>
  ExtractUtils.getNumberFilter(criterias, samplingOperationCriteriaDefinition, property);
const setNumberFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation, value: INumberFilterValue) =>
  ExtractUtils.setNumberFilter(criterias, samplingOperationCriteriaDefinition, property, value);
const getTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation) =>
  ExtractUtils.getTextFilter(criterias, samplingOperationCriteriaDefinition, property);
const setTextFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation, value: ITextFilterValue) =>
  ExtractUtils.setTextFilter(criterias, samplingOperationCriteriaDefinition, property, value);
const getTimeFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation) =>
  ExtractUtils.getTimeFilter(criterias, samplingOperationCriteriaDefinition, property);
const setTimeFilter = (criterias: FilterCriteria[], property: keyof IExtractCriteriaSamplingOperation, value: ITimeFilterValue) =>
  ExtractUtils.setTimeFilter(criterias, samplingOperationCriteriaDefinition, property, value);

export const samplingOperationFilters: (keyof IExtractCriteriaSamplingOperation)[] = [
  'label',
  'comments',
  'time',
  'utFormat',
  'geometryType',
  'samplingEquipmentFilter',
  'recorderDepartmentFilter',
  'samplingDepartmentFilter',
  'depthLevelFilter',
  'depth',
  'controlled',
  'validated',
  'qualifications',
  'havingMeasurement',
  'havingMeasurementFile',
];

export interface IExtractCriteriaSamplingOperation extends IExtractCriteria {
  label: ITextFilterValue;
  comments: ITextFilterValue;
  time: ITimeFilterValue;
  utFormat: number;
  geometryType: GeometryType;
  samplingEquipmentFilter: IReferentialFilterCriteria<number>;
  recorderDepartmentFilter: IReferentialFilterCriteria<number>;
  samplingDepartmentFilter: IReferentialFilterCriteria<number>;
  depthLevelFilter: IReferentialFilterCriteria<number>;
  depth: INumberFilterValue;
  controlled: boolean;
  validated: boolean;
  qualifications: IExtractCriteriaQualityFlag;
  havingMeasurement: boolean;
  havingMeasurementFile: boolean;
}

export const samplingOperationCriteriaDefinition: CriteriaDefinitionRecord<IExtractCriteriaSamplingOperation> = {
  label: {
    i18nLabel: 'DATA.SAMPLING_OPERATION.LABEL',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_LABEL',
  },
  comments: {
    i18nLabel: 'DATA.SAMPLING_OPERATION.COMMENTS',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_COMMENT',
    valueOperatorType: 'TEXT_CONTAINS',
  },
  time: {
    i18nLabel: 'DATA.SAMPLING_OPERATION.TIME',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_TIME',
  },
  utFormat: {
    i18nLabel: 'DATA.SAMPLING_OPERATION.UT_FORMAT',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_UT_FORMAT',
    valueOperatorType: 'UT_FORMAT_EQUAL',
  },
  geometryType: {
    i18nLabel: 'REFERENTIAL.MONITORING_LOCATION.GEOMETRY_TYPE',
    valuesI18Labels: {
      point: 'REFERENTIAL.GEOMETRY_TYPE.POINT',
      line: 'REFERENTIAL.GEOMETRY_TYPE.LINE',
      area: 'REFERENTIAL.GEOMETRY_TYPE.AREA',
    },
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE',
    valueOperatorType: 'GEOMETRY_TYPE_EQUAL',
  },
  samplingEquipmentFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.SAMPLING_EQUIPMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID',
    valuesOperatorType: 'SAMPLING_EQUIPMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_NAME',
  },
  recorderDepartmentFilter: {
    i18nLabel: 'DATA.SAMPLING_OPERATION.RECORDER_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL',
  },
  samplingDepartmentFilter: {
    i18nLabel: 'DATA.SAMPLING_OPERATION.SAMPLING_DEPARTMENT',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID',
    valuesOperatorType: 'DEPARTMENT_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL',
  },
  depthLevelFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.DEPTH_LEVEL',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_ID',
    valuesOperatorType: 'DEPTH_LEVEL_IN',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_LABEL',
  },
  depth: {
    i18nLabel: 'DATA.SAMPLING_OPERATION.DEPTH',
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH',
  },
  controlled: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.CONTROLLED',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_CONTROL',
    valueOperatorType: 'BOOLEAN',
  },
  validated: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.VALIDATED',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION',
    valueOperatorType: 'BOOLEAN',
  },
  qualifications: {
    i18nLabel: dataQualityFlagI18nLabelPrefix + 'TITLE',
    i18nLabelPrefix: dataQualityFlagI18nLabelPrefix,
    valuesI18Labels: qualityFlagI18nLabels,
    valuesCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION',
  },
  havingMeasurement: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.HAVING_MEASUREMENT.SAMPLING_OPERATION',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT',
    valueOperatorType: 'BOOLEAN',
  },
  havingMeasurementFile: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.HAVING_MEASUREMENT_FILE.SAMPLING_OPERATION',
    valueCriteriaType: 'EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT_FILE',
    valueOperatorType: 'BOOLEAN',
  },
};

export class ExtractCriteriaSamplingOperation implements IExtractCriteriaSamplingOperation {
  id = null;
  label = TextFilterValue.default();
  comments = TextFilterValue.default('TEXT_CONTAINS');
  time = TimeFilterValue.default();
  utFormat: number;
  geometryType: GeometryType;
  samplingEquipmentFilter: IntReferentialFilterCriteria;
  recorderDepartmentFilter: IntReferentialFilterCriteria;
  samplingDepartmentFilter: IntReferentialFilterCriteria;
  depthLevelFilter: IntReferentialFilterCriteria;
  depth = NumberFilterValue.default();
  controlled = null;
  validated = null;
  qualifications = ExtractCriteriaQualityFlag.default();
  havingMeasurement = null;
  havingMeasurementFile = null;
  visibleFilters: string[];

  static isEmpty(c: ExtractCriteriaSamplingOperation): boolean {
    return isNil(c) || isEmptyArray(ExtractCriteriaSamplingOperation.nonEmptyProperties(c));
  }

  static equals(c1: ExtractCriteriaSamplingOperation, c2: ExtractCriteriaSamplingOperation): boolean {
    return (
      (isNil(c1) && isNil(c2)) ||
      c1 === c2 ||
      (c1 &&
        c2 &&
        TextFilterValue.equals(c1.label, c2.label) &&
        TextFilterValue.equals(c1.comments, c2.comments) &&
        TimeFilterValue.equals(c1.time, c2.time) &&
        c1.utFormat === c2.utFormat &&
        c1.geometryType === c2.geometryType &&
        Utils.equals(c1.samplingEquipmentFilter, c2.samplingEquipmentFilter) &&
        Utils.equals(c1.recorderDepartmentFilter, c2.recorderDepartmentFilter) &&
        Utils.equals(c1.samplingDepartmentFilter, c2.samplingDepartmentFilter) &&
        Utils.equals(c1.depthLevelFilter, c2.depthLevelFilter) &&
        NumberFilterValue.equals(c1.depth, c2.depth) &&
        c1.controlled === c2.controlled &&
        c1.validated === c2.validated &&
        ExtractCriteriaQualityFlag.equals(c1.qualifications, c2.qualifications) &&
        c1.havingMeasurement === c2.havingMeasurement &&
        c1.havingMeasurementFile === c2.havingMeasurementFile)
    );
  }

  static parse(criterias: FilterCriteria[], id: number): IExtractCriteriaSamplingOperation {
    return {
      id,
      label: getTextFilter(criterias, 'label'),
      comments: getTextFilter(criterias, 'comments'),
      time: getTimeFilter(criterias, 'time'),
      utFormat: getValue(criterias, 'utFormat').value,
      geometryType: getValue(criterias, 'geometryType').value?.toUpperCase(),
      samplingEquipmentFilter: getReferentialFilterCriteria(criterias, 'samplingEquipmentFilter'),
      recorderDepartmentFilter: getReferentialFilterCriteria(criterias, 'recorderDepartmentFilter'),
      samplingDepartmentFilter: getReferentialFilterCriteria(criterias, 'samplingDepartmentFilter'),
      depthLevelFilter: getReferentialFilterCriteria(criterias, 'depthLevelFilter'),
      depth: getNumberFilter(criterias, 'depth'),
      controlled: Utils.fromOptionalBooleanString(getValue(criterias, 'controlled').value),
      validated: Utils.fromOptionalBooleanString(getValue(criterias, 'validated').value),
      qualifications: ExtractCriteriaQualityFlag.fromValues(getValues(criterias, 'qualifications').values),
      havingMeasurement: Utils.fromOptionalBooleanString(getValue(criterias, 'havingMeasurement').value),
      havingMeasurementFile: Utils.fromOptionalBooleanString(getValue(criterias, 'havingMeasurementFile').value),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractCriteriaSamplingOperation, opts: IMergeCriteriaOptions) {
    setTextFilter(criterias, 'label', value?.label);
    setTextFilter(criterias, 'comments', value?.comments);
    setTimeFilter(criterias, 'time', value?.time);
    setValue(criterias, 'utFormat', { value: value?.utFormat });
    setValue(criterias, 'geometryType', { value: capitalizeFirstLetter(value?.geometryType?.toLowerCase()) });
    setReferentialFilterCriteria(criterias, 'samplingEquipmentFilter', value?.samplingEquipmentFilter);
    setReferentialFilterCriteria(criterias, 'recorderDepartmentFilter', value?.recorderDepartmentFilter);
    setReferentialFilterCriteria(criterias, 'samplingDepartmentFilter', value?.samplingDepartmentFilter);
    setReferentialFilterCriteria(criterias, 'depthLevelFilter', value?.depthLevelFilter);
    setNumberFilter(criterias, 'depth', value?.depth);
    setValue(criterias, 'controlled', {
      value: isNotNil(value?.controlled) ? Utils.toBooleanString(value?.controlled) : null,
    });
    setValue(criterias, 'validated', {
      value: isNotNil(value?.validated) ? Utils.toBooleanString(value?.validated) : null,
    });
    setValues(criterias, 'qualifications', { values: ExtractCriteriaQualityFlag.toValues(value?.qualifications, opts.defaultIfEmpty) });
    setValue(criterias, 'havingMeasurement', {
      value: isNotNil(value?.havingMeasurement) ? Utils.toBooleanString(value?.havingMeasurement) : null,
    });
    setValue(criterias, 'havingMeasurementFile', {
      value: isNotNil(value?.havingMeasurementFile) ? Utils.toBooleanString(value?.havingMeasurementFile) : null,
    });
  }

  static nonEmptyProperties(criteria: IExtractCriteriaSamplingOperation): (keyof IExtractCriteriaSamplingOperation)[] {
    const properties: (keyof IExtractCriteriaSamplingOperation)[] = [];
    if (criteria) {
      if (!TextFilterValue.isEmpty(criteria.label)) properties.push('label');
      if (!TextFilterValue.isEmpty(criteria.comments)) properties.push('comments');
      if (!TimeFilterValue.isEmpty(criteria.time)) properties.push('time');
      if (isNotNilOrNaN(criteria.utFormat)) properties.push('utFormat');
      if (!!criteria.geometryType) properties.push('geometryType');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.samplingEquipmentFilter, true)) properties.push('samplingEquipmentFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.recorderDepartmentFilter, true)) properties.push('recorderDepartmentFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.samplingDepartmentFilter, true)) properties.push('samplingDepartmentFilter');
      if (!BaseFilterUtils.isCriteriaEmpty(criteria.depthLevelFilter, true)) properties.push('depthLevelFilter');
      if (!NumberFilterValue.isEmpty(criteria.depth)) properties.push('depth');
      if (isNotNil(criteria.controlled)) properties.push('controlled');
      if (isNotNil(criteria.validated)) properties.push('validated');
      if (!ExtractCriteriaQualityFlag.isEmptyOrDefault(criteria.qualifications)) properties.push('qualifications');
      if (isNotNil(criteria.havingMeasurement)) properties.push('havingMeasurement');
      if (isNotNil(criteria.havingMeasurementFile)) properties.push('havingMeasurementFile');
    }
    return properties;
  }

  static defaultValue(property: keyof IExtractCriteriaSamplingOperation): any {
    switch (property) {
      case 'label':
        return TextFilterValue.default();
      case 'comments':
        return TextFilterValue.default('TEXT_CONTAINS');
      case 'time':
        return TimeFilterValue.default();
      case 'depth':
        return NumberFilterValue.default();
      case 'qualifications':
        return ExtractCriteriaQualityFlag.default();
    }
    return undefined;
  }

  static fromObject(source: any): ExtractCriteriaSamplingOperation {
    const target = new ExtractCriteriaSamplingOperation();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    this.label = source.label || TextFilterValue.default();
    this.comments = source.comments || TextFilterValue.default('TEXT_CONTAINS');
    this.time = source.time || TimeFilterValue.default();
    this.utFormat = source.utFormat;
    this.geometryType = source.geometryType;
    this.samplingEquipmentFilter = IntReferentialFilterCriteria.fromObject(source.samplingEquipmentFilter || {});
    this.recorderDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.recorderDepartmentFilter || {});
    this.samplingDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.samplingDepartmentFilter || {});
    this.depthLevelFilter = IntReferentialFilterCriteria.fromObject(source.depthLevelFilter || {});
    this.depth = source.depth || NumberFilterValue.default();
    this.controlled = source.controlled;
    this.validated = source.validated;
    this.qualifications = ExtractCriteriaQualityFlag.fromObject(source.qualifications);
    this.havingMeasurement = source.havingMeasurement;
    this.havingMeasurementFile = source.havingMeasurementFile;
    this.visibleFilters = source.visibleFilters;
  }

  asObject(): any {
    const target = { ...this };
    target.samplingEquipmentFilter = this.samplingEquipmentFilter?.asObject();
    target.recorderDepartmentFilter = this.recorderDepartmentFilter?.asObject();
    target.samplingDepartmentFilter = this.samplingDepartmentFilter?.asObject();
    target.depthLevelFilter = this.depthLevelFilter?.asObject();
    target.qualifications = this.qualifications?.asObject();
    target.visibleFilters = this.visibleFilters;
    return target;
  }
}
