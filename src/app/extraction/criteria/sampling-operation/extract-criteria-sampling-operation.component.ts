import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import {
  ExtractCriteriaSamplingOperation,
  IExtractCriteriaSamplingOperation,
  samplingOperationCriteriaDefinition,
  samplingOperationFilters,
} from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.model';
import { geometryTypes } from '@app/referential/monitoring-location/geometry-type';
import { ExtractCriteriaComponent } from '@app/extraction/criteria/extract-criteria.component';

@Component({
  selector: 'app-extract-criteria-sampling-operation-component',
  templateUrl: './extract-criteria-sampling-operation.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractCriteriaSamplingOperationComponent extends ExtractCriteriaComponent<
  ExtractCriteriaSamplingOperation,
  IExtractCriteriaSamplingOperation
> {
  criteriaDefinition = samplingOperationCriteriaDefinition;
  possibleFilters = samplingOperationFilters;
  geometryTypes = geometryTypes;

  constructor(protected injector: Injector) {
    super(injector, ExtractCriteriaSamplingOperation);
    this.logPrefix = '[extract-criteria-sampling-operation]';
  }

  computeVisibleFiltersFromValue(value: IExtractCriteriaSamplingOperation): (keyof IExtractCriteriaSamplingOperation)[] {
    return ExtractCriteriaSamplingOperation.nonEmptyProperties(value);
  }

  defaultValue(property: keyof IExtractCriteriaSamplingOperation): any {
    return ExtractCriteriaSamplingOperation.defaultValue(property);
  }

  addFilter(event: UIEvent, filter: any) {
    if (['havingMeasurement', 'havingMeasurementFile'].includes(filter)) {
      // Default value: true
      this.form.get(filter).setValue(true);
      this.markAsDirty();
    }
    super.addFilter(event, filter);
  }
}
