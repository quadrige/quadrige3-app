import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import {
  ExtractCriteriaSamplingOperation,
  IExtractCriteriaSamplingOperation,
} from '@app/extraction/criteria/sampling-operation/extract-criteria-sampling-operation.model';
import { ExtractCriteriaQualityFlagValidatorService } from '@app/extraction/criteria/quality-flag/extract-criteria-quality-flag.validator';
import { NumberFilterValidator } from '@app/referential/component/number-filter-form-field.component';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { TextFilterValidator } from '@app/referential/component/text-filter-form-field.component';
import { TimeFilterValidator } from '@app/referential/component/time-filter-form-field.component';
import { toBoolean } from '@sumaris-net/ngx-components';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

@Injectable({ providedIn: 'root' })
export class ExtractCriteriaSamplingOperationValidatorService extends BaseValidatorService<ExtractCriteriaSamplingOperation> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator,
    protected qualityFlagValidator: ExtractCriteriaQualityFlagValidatorService,
    protected numberFilterValidator: NumberFilterValidator,
    protected textFilterValidator: TextFilterValidator,
    protected timeFilterValidator: TimeFilterValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractCriteriaSamplingOperation, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractCriteriaSamplingOperation, any>>{
      id: [data?.id || null],
      label: this.textFilterValidator.getFormGroup(data?.label),
      comments: this.textFilterValidator.getFormGroup(data?.comments),
      time: this.timeFilterValidator.getFormGroup(data?.time),
      utFormat: [data?.utFormat || null],
      geometryType: [data?.geometryType || null],
      samplingEquipmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.samplingEquipmentFilter),
      recorderDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.recorderDepartmentFilter),
      samplingDepartmentFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.samplingDepartmentFilter),
      depthLevelFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.depthLevelFilter),
      depth: this.numberFilterValidator.getFormGroup(data?.depth),
      controlled: [toBoolean(data?.controlled)],
      validated: [toBoolean(data?.validated)],
      qualifications: this.qualityFlagValidator.getFormGroup(data?.qualifications),
      havingMeasurement: [toBoolean(data?.havingMeasurement)],
      havingMeasurementFile: [toBoolean(data?.havingMeasurementFile)],
      visibleFilters: [data?.visibleFilters || null],
    };
  }
}
