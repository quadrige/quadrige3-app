import {
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
} from '@app/referential/service/referential-validator.service';
import { ExtractFilter, IExtractFilter } from '@app/extraction/extract-filter.model';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedFormGroupValidators, toBoolean } from '@sumaris-net/ngx-components';
import { Injectable, Injector } from '@angular/core';
import { PartialRecord } from '@app/shared/model/interface';
import { BaseGroupValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';

@Injectable()
export class ExtractFilterValidator extends BaseValidatorService<ExtractFilter> {
  constructor(
    protected injector: Injector,
    protected service: ExtractFilterService
  ) {
    super(injector.get(UntypedFormBuilder));
  }

  getFormGroupConfig(data?: ExtractFilter, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return <PartialRecord<keyof IExtractFilter, any>>{
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      userId: [data?.userId || null, Validators.required],
      fileTypes: [data?.fileTypes || null],
      periods: [data?.periods || null],
      fields: [data?.fields || null],
      fieldsLoaded: [toBoolean(data?.fieldsLoaded, false)],
      filters: [data?.filters || null],
      filtersLoaded: [toBoolean(data?.filtersLoaded, false)],
      geometrySource: [data?.geometrySource || null],
      orderItemIds: [data?.orderItemIds || null],
      features: [data?.features || null],
      featuresLoaded: [toBoolean(data?.featuresLoaded, false)],
      template: [toBoolean(data?.template, false)],
      templateTemp: [toBoolean(data?.templateTemp, false)],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      responsibleDepartmentIds: [data?.responsibleDepartmentIds || null],
      responsibleUserIds: [data?.responsibleUserIds || null],
      visibleFilters: [data?.visibleFilters || null],
      creationDate: [data?.creationDate || null],
      entityName: [data?.entityName || null, Validators.required],
      updateDate: [data?.updateDate || null],
    };
  }

  getFormGroupOptions(data?: ExtractFilter, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredIfArrayEmpty('fieldsLoaded', false, 'fields', 'EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_EXTRACT_FIELD'),
        BaseGroupValidators.requiredIfOneArrayNotEmpty(
          'template',
          true,
          ['responsibleUserIds', 'responsibleDepartmentIds'],
          'EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_MANAGER'
        ),
        SharedFormGroupValidators.requiredIfTrue('description', 'template'),
      ],
    };
  }
}
