import gql from 'graphql-tag';
import { filterFragment } from '@app/referential/filter/filter.service';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { inject, Injector } from '@angular/core';
import { ExtractFilter, IExtractFilter } from '@app/extraction/extract-filter.model';
import { AccountService, isNil, isNilOrBlank } from '@sumaris-net/ngx-components';
import { ExtractField, ExtractFieldDefinition } from '@app/extraction/extract-field.model';
import { Filter } from '@app/referential/filter/model/filter.model';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { EntityUtils } from '@app/shared/entity.utils';
import { ExtractionType } from '@app/extraction/extract.type';
import { FeatureCollection } from 'geojson';
import { Geometries } from '@app/shared/geometries';
import { ExtractFilterFilter, ExtractFilterFilterCriteria } from '@app/extraction/filter/extract-filter.filter.model';
import { Job } from '@app/social/job/job.model';
import { jobFragments } from '@app/social/job/job.service';
import { FilterType } from '@app/referential/filter/model/filter-type';

const extractFieldFragment = gql`
  fragment ExtractFieldFragment on ExtractFieldVO {
    id
    type
    name
    rankOrder
    sortDirection
    updateDate
    __typename
  }
`;

const extractSurveyPeriodFragment = gql`
  fragment ExtractSurveyPeriod on ExtractSurveyPeriodVO {
    id
    startDate
    endDate
    updateDate
    __typename
  }
`;

const extractFilterFragments = {
  lightExtractFilterFragment: gql`
    fragment LightExtractFilterFragment on ExtractFilterVO {
      id
      name
      periods {
        ...ExtractSurveyPeriod
      }
      type
      fileTypes
      geometrySource
      orderItemIds
      userId
      template
      description
      responsibleDepartmentIds
      responsibleUserIds
      updateDate
      __typename
    }
    ${extractSurveyPeriodFragment}
  `,
  extractFilterFragment: gql`
    fragment ExtractFilterFragment on ExtractFilterVO {
      id
      name
      type
      periods {
        ...ExtractSurveyPeriod
      }
      fields {
        ...ExtractFieldFragment
      }
      fileTypes
      filters {
        ...FilterFragment
      }
      geometrySource
      geometry
      orderItemIds
      userId
      template
      description
      responsibleDepartmentIds
      responsibleUserIds
      updateDate
      __typename
    }
    ${extractSurveyPeriodFragment}
    ${extractFieldFragment}
    ${filterFragment}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  load: gql`
    query ExtractFilter($id: Int!) {
      data: extractFilter(id: $id) {
        ...ExtractFilterFragment
      }
    }
    ${extractFilterFragments.extractFilterFragment}
  `,
  loadAll: gql`
    query ExtractFilters($page: PageInput, $filter: ExtractFilterFilterVOInput) {
      data: extractFilters(page: $page, filter: $filter) {
        ...LightExtractFilterFragment
      }
    }
    ${extractFilterFragments.lightExtractFilterFragment}
  `,
  loadAllWithTotal: gql`
    query ExtractFiltersWithCount($page: PageInput, $filter: ExtractFilterFilterVOInput) {
      data: extractFilters(page: $page, filter: $filter) {
        ...LightExtractFilterFragment
      }
      total: extractFiltersCount(filter: $filter)
    }
    ${extractFilterFragments.lightExtractFilterFragment}
  `,
  countAll: gql`
    query ExtractFilterCount($filter: ExtractFilterFilterVOInput) {
      total: extractFiltersCount(filter: $filter)
    }
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveExtractFilters($data: [ExtractFilterVOInput]) {
      data: saveExtractFilters(extractFilters: $data) {
        ...ExtractFilterFragment
      }
    }
    ${extractFilterFragments.extractFilterFragment}
  `,
  deleteAll: gql`
    mutation DeleteExtractFilters($ids: [Int]) {
      deleteExtractFilters(ids: $ids)
    }
  `,
};

const otherQueries = {
  extractFilterTypes: gql`
    query ExtractFilterTypes($type: ExtractionTypeEnum) {
      data: extractFilterTypes(type: $type)
    }
  `,
  extractFieldDefinitions: gql`
    query ExtractFieldDefinition($type: ExtractionTypeEnum) {
      data: extractFieldDefinitions(type: $type) {
        id
        name
        type
        rankOrder
        __typename
      }
    }
  `,
  extractFields: gql`
    query ExtractFilterFields($extractFilterId: Int!) {
      data: extractFilterFields(extractFilterId: $extractFilterId) {
        ...ExtractFieldFragment
      }
    }
    ${extractFieldFragment}
  `,
  filters: gql`
    query ExtractFilterFilters($extractFilterId: Int!) {
      data: extractFilterFilters(extractFilterId: $extractFilterId) {
        ...FilterFragment
      }
    }
    ${filterFragment}
  `,
  geometry: gql`
    query ExtractFilterGeometry($extractFilterId: Int!) {
      data: extractFilterGeometry(extractFilterId: $extractFilterId)
    }
  `,
  check: gql`
    query CheckExtractFilter($extractFilterId: Int!) {
      data: checkExtractFilter(extractFilterId: $extractFilterId)
    }
  `,
  executeAsync: gql`
    query ExecuteExtractFilter($extractFilterId: Int!) {
      data: executeExtractFilterAsync(extractFilterId: $extractFilterId) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
  convertXMLToExtractFilter: gql`
    query ConvertXMLToExtractFilter($xmlContent: String) {
      data: convertXMLToExtractFilter(xmlContent: $xmlContent) {
        ...ExtractFilterFragment
      }
    }
    ${extractFilterFragments.extractFilterFragment}
  `,
  convertExtractFilterToAPI: gql`
    query ConvertExtractFilterToAPI($extractFilter: ExtractFilterVOInput) {
      data: convertExtractFilterToAPI(extractFilter: $extractFilter)
    }
  `,
  convertAPIToExtractFilter: gql`
    query ConvertAPIToExtractFilter($jsonContent: String) {
      data: convertAPIToExtractFilter(jsonContent: $jsonContent) {
        ...ExtractFilterFragment
      }
    }
    ${extractFilterFragments.extractFilterFragment}
  `,
  cleanExtractFilter: gql`
    query CleanExtractFilter($extractFilter: ExtractFilterVOInput) {
      data: cleanExtractFilter(extractFilter: $extractFilter) {
        ...ExtractFilterFragment
      }
    }
    ${extractFilterFragments.extractFilterFragment}
  `,
};

export class ExtractFilterService extends BaseReferentialService<ExtractFilter, ExtractFilterFilter, ExtractFilterFilterCriteria> {
  private readonly accountService = inject(AccountService);
  filterTypes: FilterType[];
  fieldDefinitions: ExtractFieldDefinition[];

  constructor(
    protected injector: Injector,
    public readonly type: ExtractionType
  ) {
    super(injector, ExtractFilter, ExtractFilterFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = `[${type.toLowerCase()}-extract-filter-service]`;
    console.debug(`${this._logPrefix} Starting...`);
  }

  asFilter(source: Partial<ExtractFilterFilter>): ExtractFilterFilter {
    const filter = super.asFilter(source);
    filter.patch({
      // apply default filter
      type: this.type,
      // apply default userId
      userId: this.accountService.account.id,
    });
    return filter;
  }

  copyIdAndUpdateDate(source: ExtractFilter, target: ExtractFilter) {
    if (!source) return;
    super.copyIdAndUpdateDate(source, target);

    // Update periods
    if (source.periods && target.periods) {
      target.periods.forEach((period) => {
        const savedPeriod = source.periods.find((value) => period.equals(value));
        EntityUtils.copyIdAndUpdateDate(savedPeriod, period);
      });
    }

    // Update filters
    if (source.filters && target.filters) {
      target.filters.forEach((filter) => {
        const savedFilter = source.filters.find((value) => filter.equals(value));
        EntityUtils.copyIdAndUpdateDate(savedFilter, filter);

        // Update blocks
        if (savedFilter.blocks && filter.blocks) {
          filter.blocks.forEach((block) => {
            const savedBlock = savedFilter.blocks.find((value) => block.equals(value));
            EntityUtils.copyIdAndUpdateDate(savedBlock, block);

            // Update criterias
            if (savedBlock.criterias && block.criterias) {
              block.criterias.forEach((criteria) => {
                const savedCriteria = savedBlock.criterias.find((value) => criteria.equals(value));
                EntityUtils.copyIdAndUpdateDate(savedCriteria, criteria);

                // Update vales
                if (savedCriteria.values && criteria.values) {
                  criteria.values.forEach((value) => {
                    const savedValue = savedCriteria.values.find((value1) => value.equals(value1));
                    EntityUtils.copyIdAndUpdateDate(savedValue, value);
                  });
                }
              });
            }
          });
        }
      });
    }

    // Update fields
    if (source.fields && target.fields) {
      target.fields.forEach((field) => {
        const savedField = source.fields.find((value) => field.equals(value));
        EntityUtils.copyIdAndUpdateDate(savedField, field);
      });
    }
  }

  async loadFields(extractFilterId: number): Promise<ExtractField[]> {
    if (isNil(extractFilterId)) {
      return [];
    }

    const variables: any = { extractFilterId };
    if (this._debug) console.debug(`${this._logPrefix} Loading extract fields...`, variables);
    const now = Date.now();
    const query = otherQueries.extractFields;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(ExtractField.fromObject); //.sort(EntityUtils.sortComparator('rankOrder')); already sorted by pod service
    if (this._debug) console.debug(`${this._logPrefix} extract fields loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadFilters(extractFilterId: number): Promise<Filter[]> {
    if (isNil(extractFilterId)) {
      return [];
    }

    const variables: any = { extractFilterId };
    if (this._debug) console.debug(`${this._logPrefix} Loading filters...`, variables);
    const now = Date.now();
    const query = otherQueries.filters;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(Filter.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} filters loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadFeatures(extractFilterId: number): Promise<FeatureCollection> {
    if (isNil(extractFilterId)) {
      return undefined;
    }

    const variables: any = { extractFilterId };
    if (this._debug) console.debug(`${this._logPrefix} Loading features...`, variables);
    const now = Date.now();
    const query = otherQueries.geometry;
    const res = await this.graphql.query<{ data: any; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} features loaded in ${Date.now() - now}ms`, data);
    return Geometries.toFeatureCollection(data);
  }

  async check(extractFilterId: number): Promise<boolean> {
    if (!extractFilterId) {
      console.warn(`${this._logPrefix} missing extractFilterId argument, extraction aborted`);
      return undefined;
    }
    if (!otherQueries.check) {
      console.warn(`${this._logPrefix} check graphql query undefined`);
      return undefined;
    }

    const variables = { extractFilterId };
    if (this._debug) console.debug(`${this._logPrefix} Check extraction`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: boolean }>({
      query: otherQueries.check,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Extraction job checked in ${Date.now() - now}ms`, data);
    return data;
  }

  async execute(extractFilterId: number): Promise<Job> {
    if (!extractFilterId) {
      console.warn(`${this._logPrefix} missing extractFilterId argument, extraction aborted`);
      return undefined;
    }
    if (!otherQueries.executeAsync) {
      console.warn(`${this._logPrefix} executeAsync graphql query undefined`);
      return undefined;
    }

    const variables = { extractFilterId };
    if (this._debug) console.debug(`${this._logPrefix} Execute extraction`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: Job }>({
      query: otherQueries.executeAsync,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Extraction job started in ${Date.now() - now}ms`, data);
    return data;
  }

  async convertXMLToExtractFilter(xmlContent: string): Promise<IExtractFilter> {
    if (isNilOrBlank(xmlContent)) {
      console.warn(`${this._logPrefix} missing xmlContent argument`);
      return undefined;
    }
    if (!otherQueries.convertXMLToExtractFilter) {
      console.warn(`${this._logPrefix} convertExtractFilter graphql query undefined`);
      return undefined;
    }

    const variables = { xmlContent };
    if (this._debug) console.debug(`${this._logPrefix} Convert Q2 Xml to ExtractFilter`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: IExtractFilter }>({
      query: otherQueries.convertXMLToExtractFilter,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Q2 Xml converted in ${Date.now() - now}ms`, data);
    return data;
  }

  async convertAPIJsonToExtractFilter(jsonContent: string): Promise<IExtractFilter> {
    if (isNilOrBlank(jsonContent)) {
      console.warn(`${this._logPrefix} missing jsonContent argument`);
      return undefined;
    }
    if (!otherQueries.convertAPIToExtractFilter) {
      console.warn(`${this._logPrefix} convertExtractFilter graphql query undefined`);
      return undefined;
    }

    const variables = { jsonContent };
    if (this._debug) console.debug(`${this._logPrefix} Convert API Json to ExtractFilter`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: IExtractFilter }>({
      query: otherQueries.convertAPIToExtractFilter,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} API Json converted in ${Date.now() - now}ms`, data);
    return data;
  }

  async convertExtractFilterToAPI(extractFilter: IExtractFilter): Promise<string> {
    if (isNil(extractFilter)) {
      console.warn(`${this._logPrefix} missing extractFilter argument`);
      return undefined;
    }
    if (!otherQueries.convertExtractFilterToAPI) {
      console.warn(`${this._logPrefix} convertExtractFilterToAPI graphql query undefined`);
      return undefined;
    }

    const variables = { extractFilter };
    if (this._debug) console.debug(`${this._logPrefix} Convert ExtractFilter to JSON API`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: string }>({
      query: otherQueries.convertExtractFilterToAPI,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} ExtractFilter converted in ${Date.now() - now}ms`, data);
    return data;
  }

  async cleanExtractFilter(extractFilter: IExtractFilter): Promise<IExtractFilter> {
    if (isNil(extractFilter)) {
      console.warn(`${this._logPrefix} missing extractFilter argument`);
      return undefined;
    }
    if (!otherQueries.cleanExtractFilter) {
      console.warn(`${this._logPrefix} cleanExtractFilter graphql query undefined`);
      return undefined;
    }

    const variables = { extractFilter };
    if (this._debug) console.debug(`${this._logPrefix} Clean ExtractFilter`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: IExtractFilter }>({
      query: otherQueries.cleanExtractFilter,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} ExtractFilter cleaned in ${Date.now() - now}ms`, data);
    return data;
  }

  protected fillDefaultProperties(source: ExtractFilter) {
    super.fillDefaultProperties(source);
    source.type = this.type;
  }

  protected async ngOnStart() {
    await super.ngOnStart();
    // Load filter types and field definitions
    this.filterTypes = await this.loadFilterTypes();
    this.fieldDefinitions = await this.loadFieldDefinitions();
  }

  protected async loadFilterTypes(): Promise<FilterType[]> {
    if (this._debug) console.debug(`${this._logPrefix} Loading extract filter types...`);
    const now = Date.now();
    const res = await this.graphql.query<{ data: FilterType[]; total?: number }>({
      query: otherQueries.extractFilterTypes,
      variables: { type: this.type },
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'cache-first',
    });
    const data = res?.data || [];
    if (this._debug) console.debug(`${this._logPrefix} extract filter types loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  protected async loadFieldDefinitions(): Promise<ExtractFieldDefinition[]> {
    if (this._debug) console.debug(`${this._logPrefix} Loading extract field definitions...`);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query: otherQueries.extractFieldDefinitions,
      variables: { type: this.type },
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'cache-first',
    });
    const data = (res?.data || []).map(ExtractFieldDefinition.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} extract field definitions loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
