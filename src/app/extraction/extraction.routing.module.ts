import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ExtractionModule } from '@app/extraction/extraction.module';
import { ResultHomePage } from '@app/extraction/menu/result-home.page';
import { DataHomePage } from '@app/extraction/menu/data-home.page';
import { OtherHomePage } from '@app/extraction/menu/other-home.page';

const routes: Routes = [
  {
    path: 'result',
    component: ResultHomePage,
    children: [
      {
        path: 'Result',
        loadChildren: () => import('./module/result-extract.routing.module').then((m) => m.ResultExtractRoutingModule),
      },
    ],
  },
  {
    path: 'data',
    component: DataHomePage,
    children: [
      {
        path: 'Survey',
        loadChildren: () => import('./module/survey-extract.routing.module').then((m) => m.SurveyExtractRoutingModule),
      },
      {
        path: 'SamplingOperation',
        loadChildren: () => import('./module/sampling-operation-extract.routing.module').then((m) => m.SamplingOperationExtractRoutingModule),
      },
      {
        path: 'Sample',
        loadChildren: () => import('./module/sample-extract.routing.module').then((m) => m.SampleExtractRoutingModule),
      },
      {
        path: 'InSituWithoutResult',
        loadChildren: () => import('./module/in-situ-without-result-extract.routing.module').then((m) => m.InSituWithoutResultExtractRoutingModule),
      },
    ],
  },
  {
    path: 'other',
    component: OtherHomePage,
    children: [
      {
        path: 'Campaign',
        loadChildren: () => import('./module/campaign-extract.routing.module').then((m) => m.CampaignExtractRoutingModule),
      },
      {
        path: 'Occasion',
        loadChildren: () => import('./module/occasion-extract.routing.module').then((m) => m.OccasionExtractRoutingModule),
      },
      {
        path: 'Event',
        loadChildren: () => import('./module/event-extract.routing.module').then((m) => m.EventExtractRoutingModule),
      },
    ],
  },
];

@NgModule({
  imports: [ExtractionModule, RouterModule.forChild(routes)],
})
export class ExtractionRoutingModule {}
