import { Referential } from '@app/referential/model/referential.model';
import { EntityClass, IReferentialRef, isEmptyArray, ReferentialAsObjectOptions, toBoolean } from '@sumaris-net/ngx-components';
import { Filter } from '@app/referential/filter/model/filter.model';
import { ExtractSurveyPeriod } from '@app/extraction/period/extract-survey-period.model';
import { ExtractField, IExtractField } from '@app/extraction/extract-field.model';
import { ExtractionType } from '@app/extraction/extract.type';
import { FeatureCollection, Geometry } from 'geojson';
import { Geometries } from '@app/shared/geometries';
import { ExtractCriteriaMain } from '@app/extraction/criteria/extract-criteria.model';
import { ISimpleType } from '@app/shared/model/interface';
import { EntityUtils } from '@app/shared/entity.utils';

export type MainExtractFileType = 'CSV' | 'JSON' | 'SANDRE_QELI';

export type ShapefileExtractFileType =
  | 'SHAPEFILE_MONITORING_LOCATION'
  | 'SHAPEFILE_SURVEY'
  | 'SHAPEFILE_SAMPLING_OPERATION'
  | 'SHAPEFILE_CAMPAIGN'
  | 'SHAPEFILE_OCCASION'
  | 'SHAPEFILE_EVENT';

export type ExtractFileType = MainExtractFileType | ShapefileExtractFileType;

export const mainFileTypesWithShapefile: MainExtractFileType[] = ['CSV'];

export const mainFileTypeList: Readonly<ISimpleType<ExtractFileType>[]> = Object.freeze([
  {
    id: 'CSV',
    i18nLabel: 'EXTRACTION.FILE_TYPE.CSV',
  },
  // JSON and SANDRE_QELI are not supported yet (Mantis #64637)
  // {
  //   id: 'JSON',
  //   i18nLabel: 'EXTRACTION.FILE_TYPE.JSON',
  // },
  // {
  //   id: 'SANDRE_QELI',
  //   i18nLabel: 'EXTRACTION.FILE_TYPE.SANDRE_QELI',
  // },
]);

export const shapefileFileTypeList: Readonly<ISimpleType<ExtractFileType>[]> = Object.freeze([
  {
    id: 'SHAPEFILE_MONITORING_LOCATION',
    i18nLabel: 'EXTRACTION.FILE_TYPE.SHAPEFILE_MONITORING_LOCATION',
  },
  {
    id: 'SHAPEFILE_SURVEY',
    i18nLabel: 'EXTRACTION.FILE_TYPE.SHAPEFILE_SURVEY',
  },
  {
    id: 'SHAPEFILE_SAMPLING_OPERATION',
    i18nLabel: 'EXTRACTION.FILE_TYPE.SHAPEFILE_SAMPLING_OPERATION',
  },
  {
    id: 'SHAPEFILE_CAMPAIGN',
    i18nLabel: 'EXTRACTION.FILE_TYPE.SHAPEFILE_CAMPAIGN',
  },
  {
    id: 'SHAPEFILE_OCCASION',
    i18nLabel: 'EXTRACTION.FILE_TYPE.SHAPEFILE_OCCASION',
  },
  {
    id: 'SHAPEFILE_EVENT',
    i18nLabel: 'EXTRACTION.FILE_TYPE.SHAPEFILE_EVENT',
  },
]);

export type ExtractFilterGeometrySourceType = 'ORDER_ITEM' | 'BUILT_IN' | 'SHAPEFILE';

export interface IVisibleFilter {
  mainFilters?: string[][];
  surveyFilters?: string[][];
  samplingOperationFilters?: string[][];
  sampleFilters?: string[][];
  measurementFilters?: string[][];
  photoFilters?: string[][];
  campaignFilters?: string[][];
  occasionFilters?: string[][];
  eventFilters?: string[][];
}

export interface IExtractFilter extends IReferentialRef {
  userId: number;
  type: ExtractionType;
  fileTypes: ExtractFileType[];
  periods: ExtractSurveyPeriod[];
  fields: IExtractField[];
  fieldsLoaded: boolean;
  filters: Filter[];
  filtersLoaded: boolean;
  geometrySource: ExtractFilterGeometrySourceType;
  orderItemIds: number[];
  features: FeatureCollection;
  geometry?: Geometry;
  featuresLoaded: boolean;
  template: boolean;
  templateTemp: boolean;
  responsibleUserIds: number[];
  responsibleDepartmentIds: number[];
  visibleFilters?: IVisibleFilter;
}

export interface IMergeCriteriaOptions {
  defaultIfEmpty?: boolean;
}

@EntityClass({ typename: 'ExtractFilterVO' })
export class ExtractFilter extends Referential<ExtractFilter> implements IExtractFilter {
  static entityName = 'ExtractFilter';
  static fromObject: (source: any, opts?: any) => ExtractFilter;

  userId: number = null;
  type: ExtractionType = null;
  fileTypes: ExtractFileType[] = [];
  periods: ExtractSurveyPeriod[] = [];
  fields: ExtractField[] = [];
  fieldsLoaded = false;
  filters: Filter[] = [];
  filtersLoaded = false;
  geometrySource: ExtractFilterGeometrySourceType = null;
  orderItemIds = [];
  features: FeatureCollection = undefined;
  featuresLoaded = false;
  template = false;
  templateTemp = false;
  responsibleUserIds: number[] = null;
  responsibleDepartmentIds: number[] = null;
  visibleFilters: IVisibleFilter = null;

  constructor() {
    super(ExtractFilter.TYPENAME);
    this.entityName = ExtractFilter.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = ExtractFilter.entityName;
    this.userId = source.userId;
    this.type = source.type;
    this.fileTypes = source.fileTypes || [];
    this.periods = (source.periods?.map(ExtractSurveyPeriod.fromObject) || [])
      .slice()
      .sort((a: ExtractSurveyPeriod, b: ExtractSurveyPeriod) => a.startDate.diff(b.startDate));
    this.fields = source.fields?.map(ExtractField.fromObject) || [];
    this.fieldsLoaded = source.fieldsLoaded;
    this.filters = source.filters?.map(Filter.fromObject) || [];
    this.filtersLoaded = source.filtersLoaded;
    this.geometrySource = source.geometrySource;
    this.orderItemIds = source.orderItemIds || [];
    this.features = source.features || Geometries.toFeatureCollection(source.geometry);
    this.featuresLoaded = source.featuresLoaded;
    this.template = toBoolean(source.template, false);
    this.templateTemp = this.template;
    this.responsibleUserIds = source.responsibleUserIds || [];
    this.responsibleDepartmentIds = source.responsibleDepartmentIds || [];
    this.visibleFilters = source.visibleFilters;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target: IExtractFilter = super.asObject(opts);
    target.periods = this.periods?.map((value) => EntityUtils.asMinifiedObject(value, opts));
    target.fields = this.fields?.map((value) => EntityUtils.asMinifiedObject(value, opts));
    // Rename filters (Mantis #61846)
    this.filters?.forEach((value) => (value.name = this.name));
    target.filters = this.filters?.map((value) => value.asObject(opts)); // Don't minify
    if (opts?.minify !== false) {
      target.geometry = Geometries.toGeometryCollection(this.features);
      delete target.features;
      delete target.fieldsLoaded;
      delete target.filtersLoaded;
      delete target.featuresLoaded;
      delete target.visibleFilters;
      delete target.templateTemp;
    }
    return target;
  }
}

export class ExtractFilterUtils {
  static isEmpty(data: IExtractFilter): boolean {
    return isEmptyArray(data.periods) && ExtractCriteriaMain.isEmpty(ExtractFilterUtils.getMainCriterias(data)) && !data.geometrySource;
  }

  static getMainCriterias(data: IExtractFilter): ExtractCriteriaMain {
    return ExtractCriteriaMain.fromObject(
      ExtractCriteriaMain.parse(data.filters?.find((value) => value.filterType === 'EXTRACT_DATA_MAIN')?.blocks?.[0]?.criterias || [])
    );
  }
}
