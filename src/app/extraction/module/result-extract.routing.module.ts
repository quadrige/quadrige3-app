import { RouterModule, Routes } from '@angular/router';
import { inject, NgModule } from '@angular/core';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { ResultExtractModule } from '@app/extraction/module/result-extract.module';
import { ExtractFilterTable } from '@app/extraction/extract-filter.table';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ExtractFilterTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [ResultExtractModule, RouterModule.forChild(routes)],
})
export class ResultExtractRoutingModule {}
