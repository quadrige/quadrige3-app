import { Injector, NgModule } from '@angular/core';
import { ExtractionModule } from '@app/extraction/extraction.module';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';
import { ExtractFilterValidator } from '@app/extraction/extract-filter.validator';

@NgModule({
  imports: [ExtractionModule],
  providers: [
    {
      provide: ExtractFilterService,
      useFactory: (injector: Injector) => new ExtractFilterService(injector, 'CAMPAIGN'),
      deps: [Injector],
    },
    {
      provide: ExtractFilterValidator,
      useFactory: (injector: Injector, service: ExtractFilterService) => new ExtractFilterValidator(injector, service),
      deps: [Injector, ExtractFilterService],
    },
  ],
})
export class CampaignExtractModule {}
