import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions, ISelectCriteria } from '@app/shared/model/options.model';
import { MatStep, MatStepper } from '@angular/material/stepper';
import { AccountService, isEmptyArray, isNilOrBlank } from '@sumaris-net/ngx-components';
import { TextFrameComponent } from '@app/shared/component/text/text-frame.component';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { lengthDescription } from '@app/referential/service/referential-validator.service';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

export interface IExtractTemplateOptions extends IModalOptions {
  addCriteria: ISelectCriteria;
}

export interface IExtractTemplateResult {
  description: string;
  responsibleUserIds: number[];
  responsibleDepartmentIds: number[];
}

@Component({
  selector: 'app-extract-template-modal',
  templateUrl: './extract-template.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractTemplateModal extends ModalComponent<IExtractTemplateResult> implements IExtractTemplateOptions, AfterViewInit {
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  @ViewChild('descriptionStep') descriptionStep: MatStep;
  @ViewChild('userStep') userStep: MatStep;
  @ViewChild('departmentStep') departmentStep: MatStep;
  @ViewChild('descriptionComponent') descriptionComponent: TextFrameComponent;
  @ViewChild('userTable') userTable: GenericSelectTable;
  @ViewChild('departmentTable') departmentTable: GenericSelectTable;

  @Input() addCriteria: ISelectCriteria;

  protected readonly lengthDescription = lengthDescription;

  constructor(
    protected injector: Injector,
    protected account: AccountService
  ) {
    super(injector);
  }

  get canGoNext() {
    return this.stepper.selected === this.descriptionStep ? this.descriptionStep.completed : true;
  }

  get canValidate() {
    return this.stepper.selected === this.departmentStep;
  }

  async next(event: any) {
    event?.stopPropagation();
    this.stepper.next();
  }

  async stepChanged(event: StepperSelectionEvent) {
    this.setError(undefined);
    if (event.selectedStep === this.userStep) {
      if (isEmptyArray(this.userTable.selectedIds)) {
        // Add authenticated user as responsible (Mantis #60800)
        await this.userTable.setSelectedIds([this.account.account.id]);
      }
    }
  }

  protected async afterInit() {
    await this.userTable.ready();
    await this.userTable.resetFilter();
    await this.departmentTable.ready();
    await this.departmentTable.resetFilter();
    // Force focus on description field
    setTimeout(() => this.descriptionComponent.textForm.focusInput(), 250);
  }

  protected dataToValidate(): IExtractTemplateResult {
    if (isNilOrBlank(this.descriptionComponent.value)) {
      throw new Error(this.translate.instant('EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_DESCRIPTION'));
    }
    if (isEmptyArray(this.userTable.selectedIds) && isEmptyArray(this.departmentTable.selectedIds)) {
      throw new Error(this.translate.instant('EXTRACTION.EXTRACT_FILTER.ERROR.MISSING_MANAGER'));
    }
    return {
      description: this.descriptionComponent.value,
      responsibleUserIds: this.userTable.selectedIds.map((value) => +value),
      responsibleDepartmentIds: this.departmentTable.selectedIds.map((value) => +value),
    };
  }
}
