import { FilterCriteriaType } from '@app/referential/filter/model/filter-criteria-type';
import { PartialRecord } from '@app/shared/model/interface';
import { FilterOperatorType } from '@app/referential/filter/model/filter-operator-type';

export type ExtractionType = 'SURVEY' | 'SAMPLING_OPERATION' | 'SAMPLE' | 'IN_SITU_WITHOUT_RESULT' | 'RESULT' | 'CAMPAIGN' | 'OCCASION' | 'EVENT';

export interface ICriteriaDefinition {
  i18nLabel: string;
  i18nLabelPrefix?: string;
  valuesI18Labels?: Record<any, string>;
  valuesCriteriaType?: FilterCriteriaType;
  valuesOperatorType?: FilterOperatorType;
  valueCriteriaType?: FilterCriteriaType;
  valueOperatorType?: FilterOperatorType;
}

export type CriteriaDefinitionRecord<I> = PartialRecord<keyof I, ICriteriaDefinition>;
