import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ExtractFilterFilter, ExtractFilterFilterCriteria } from '@app/extraction/filter/extract-filter.filter.model';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { ExtractionType } from '@app/extraction/extract.type';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-extract-filter-filter-form',
  templateUrl: './extract-filter.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractFilterFilterForm extends ReferentialCriteriaFormComponent<ExtractFilterFilter, ExtractFilterFilterCriteria> implements OnInit {
  @Input() filterForAdd = false;
  @Input() extractionType: ExtractionType;

  showProgramFilter: boolean;
  showParameterFilter: boolean;
  showMatrixFilter: boolean;

  constructor() {
    super();
  }

  get filterCriteriaCount(): number {
    let filterCriteriaCount = super.filterCriteriaCount;
    // The removed criterion is 'template' : on add table it is always true, and it must never be valued
    if (this.filterForAdd) filterCriteriaCount--;
    return filterCriteriaCount;
  }

  ngOnInit() {
    this.showProgramFilter = this.extractionType !== 'EVENT';
    this.showParameterFilter = this.extractionType === 'RESULT';
    this.showMatrixFilter = ['RESULT', 'IN_SITU_WITHOUT_RESULT', 'SAMPLE'].includes(this.extractionType);
  }

  protected getSearchAttributes(): string[] {
    return attributes.name;
  }

  protected criteriaToQueryParams(criteria: ExtractFilterFilterCriteria): PartialRecord<keyof ExtractFilterFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.template)) params.template = criteria.template;
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'parameterFilter');
    this.subCriteriaToQueryParams(params, criteria, 'matrixFilter');
    return params;
  }
}
