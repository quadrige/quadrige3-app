import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { ExtractionType } from '@app/extraction/extract.type';
import { ExtractFilter } from '@app/extraction/extract-filter.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export interface IExtractFilterFilterCriteria extends IReferentialFilterCriteria<number> {
  userId?: number;
  type?: ExtractionType;
  template?: boolean;
  programFilter?: StrReferentialFilterCriteria;
  parameterFilter?: StrReferentialFilterCriteria;
  matrixFilter?: IntReferentialFilterCriteria;
}

@EntityClass({ typename: 'ExtractFilterFilterCriteriaVO' })
export class ExtractFilterFilterCriteria extends ReferentialFilterCriteria<ExtractFilter, number> implements IExtractFilterFilterCriteria {
  static fromObject: (source: any, opts?: any) => ExtractFilterFilterCriteria;

  userId: number = null;
  type: ExtractionType = null;
  template: boolean = null;
  programFilter: StrReferentialFilterCriteria = null;
  parameterFilter: StrReferentialFilterCriteria = null;
  matrixFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(ExtractFilterFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.userId = source.userId;
    this.type = source.type;
    this.template = source.template;
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.parameterFilter = StrReferentialFilterCriteria.fromObject(source.parameterFilter || {});
    this.matrixFilter = IntReferentialFilterCriteria.fromObject(source.matrixFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    target.parameterFilter = this.parameterFilter?.asObject(opts);
    target.matrixFilter = this.matrixFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    // ignore these properties
    if (['userId', 'type'].includes(key)) return false;
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'parameterFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterFilter, true);
    if (key === 'matrixFilter') return !BaseFilterUtils.isCriteriaEmpty(this.matrixFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'ExtractFilterFilterVO' })
export class ExtractFilterFilter extends ReferentialFilter<ExtractFilterFilter, ExtractFilterFilterCriteria, ExtractFilter> {
  static fromObject: (source: any, opts?: any) => ExtractFilterFilter;

  constructor() {
    super(ExtractFilterFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): ExtractFilterFilterCriteria {
    return ExtractFilterFilterCriteria.fromObject(source, opts);
  }
}
