import { Component } from '@angular/core';
import { ExtractionMenuModel } from '@app/extraction/menu/extraction-menu.model';
import { MenuPage } from '@app/shared/component/menu/menu.page';

@Component({
  selector: 'app-extraction-other-home-page',
  templateUrl: 'extraction-home.page.html',
})
export class OtherHomePage extends MenuPage {
  constructor() {
    super(ExtractionMenuModel.otherMenuItems());
  }
}
