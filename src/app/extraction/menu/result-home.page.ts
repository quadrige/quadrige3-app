import { Component } from '@angular/core';
import { ExtractionMenuModel } from '@app/extraction/menu/extraction-menu.model';
import { MenuPage } from '@app/shared/component/menu/menu.page';

@Component({
  selector: 'app-extraction-result-home-page',
  templateUrl: 'extraction-home.page.html',
})
export class ResultHomePage extends MenuPage {
  constructor() {
    super(ExtractionMenuModel.resultMenuItems());
  }
}
