import { IMenuItem } from '@sumaris-net/ngx-components';

export class ExtractionMenuModel {
  static resultMenuItems(): IMenuItem[] {
    return [
      {
        title: 'MENU.EXTRACTION.RESULT',
        path: '/extraction/result/Result',
      },
    ];
  }

  static dataMenuItems(): IMenuItem[] {
    return [
      {
        title: 'MENU.EXTRACTION.SURVEY',
        path: '/extraction/data/Survey',
      },
      {
        title: 'MENU.EXTRACTION.SAMPLING_OPERATION',
        path: '/extraction/data/SamplingOperation',
      },
      {
        title: 'MENU.EXTRACTION.SAMPLE',
        path: '/extraction/data/Sample',
      },
      {
        title: 'MENU.EXTRACTION.IN_SITU_WITHOUT_RESULT',
        path: '/extraction/data/InSituWithoutResult',
        cssClass: 'larger',
      },
    ];
  }

  static otherMenuItems(): IMenuItem[] {
    return [
      {
        title: 'MENU.EXTRACTION.CAMPAIGN',
        path: '/extraction/other/Campaign',
      },
      {
        title: 'MENU.EXTRACTION.OCCASION',
        path: '/extraction/other/Occasion',
      },
      {
        title: 'MENU.EXTRACTION.EVENT',
        path: '/extraction/other/Event',
      },
    ];
  }
}
