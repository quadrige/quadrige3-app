import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { toBoolean } from '@sumaris-net/ngx-components';
import { Injectable } from '@angular/core';
import { ExtractOutput, IExtractOutput } from '@app/extraction/output/extract-output.model';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';

export interface ExtractOutputValidatorOptions extends ReferentialValidatorOptions {
  orderItemTypeFilterRequired?: boolean;
}

@Injectable({ providedIn: 'root' })
export class ExtractOutputValidator extends ReferentialValidatorService<ExtractOutput, number, ExtractOutputValidatorOptions> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialFilterFormFieldValidator: ReferentialFilterFormFieldValidator
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractOutput, opts?: ExtractOutputValidatorOptions): { [p: string]: any } {
    return <Record<keyof IExtractOutput, any>>{
      mainFileType: [data?.mainFileType || null, Validators.required],
      shapefileFileTypes: [data?.shapefileFileTypes || null],
      withDataUnderMoratorium: [toBoolean(data?.withDataUnderMoratorium, false)],
      withUserPersonalData: [toBoolean(data?.withUserPersonalData, false)],
      fields: [data?.fields || []],
      hiddenFields: [data?.hiddenFields || []],
      orderItemTypeFilter: this.referentialFilterFormFieldValidator.getFormGroup(data?.orderItemTypeFilter, {
        required: opts?.orderItemTypeFilterRequired,
      }),
    };
  }

  updateFormGroup(validator: UntypedFormGroup, data: ExtractOutput, opts: ExtractOutputValidatorOptions) {
    const orderItemTypeFilterControl = validator.get('orderItemTypeFilter') as UntypedFormGroup;
    if (!orderItemTypeFilterControl) throw new Error('orderItemTypeFilter control should exists');
    this.referentialFilterFormFieldValidator.updateFormGroup(orderItemTypeFilterControl, { required: opts?.orderItemTypeFilterRequired });
  }
}
