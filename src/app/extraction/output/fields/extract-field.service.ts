import { Injectable } from '@angular/core';
import { ExtractField } from '@app/extraction/extract-field.model';
import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { EntityUtils } from '@app/shared/entity.utils';

@Injectable()
export class ExtractFieldService extends UnfilteredEntitiesMemoryService<ExtractField> {
  constructor() {
    super(ExtractField, { equals: (d1, d2) => EntityUtils.deepEquals(d1, d2, ['id', 'name']) });
  }
}
