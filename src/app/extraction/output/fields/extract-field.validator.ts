import { UntypedFormBuilder } from '@angular/forms';
import { Injectable } from '@angular/core';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { ExtractField, IExtractField } from '@app/extraction/extract-field.model';
import { toNumber } from '@sumaris-net/ngx-components';

@Injectable({ providedIn: 'root' })
export class ExtractFieldValidatorService extends BaseValidatorService<ExtractField> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractField, opts?: any): { [p: string]: any } {
    return <Record<keyof IExtractField, any>>{
      id: [data?.id || null],
      name: [data?.name || null],
      type: [data?.type || null],
      rankOrder: [toNumber(data?.rankOrder, null)],
      sortDirection: [data?.sortDirection || null],
    };
  }
}
