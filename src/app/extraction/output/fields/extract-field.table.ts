import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import {
  ExtractField,
  ExtractFieldDefinition,
  ExtractFieldStaticType,
  extractFieldStaticTypes,
  ExtractFieldType,
  ExtractFieldUtils,
  IExtractField,
  IExtractFieldDefinition,
  orderedExtractFieldTypes,
} from '@app/extraction/extract-field.model';
import { ExtractFieldValidatorService } from '@app/extraction/output/fields/extract-field.validator';
import { ExtractFieldService } from '@app/extraction/output/fields/extract-field.service';
import { ExtractFieldAddModal, IExtractFieldOptions } from '@app/extraction/output/fields/add/extract-field.add.modal';
import { asyncEvery, uniqueValue, Utils } from '@app/shared/utils';
import { Optional } from 'typescript-optional';
import { ISelectCriteria } from '@app/shared/model/options.model';
import { ReferentialFilterHintComponent } from '@app/referential/component/referential-filter-hint.component';
import { BehaviorSubject } from 'rxjs';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-extract-field-table',
  templateUrl: './extract-field.table.html',
  styleUrls: ['./extract-field.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ExtractFieldService,
      useExisting: false,
    },
  ],
})
export class ExtractFieldTable extends BaseMemoryTable<ExtractField, number, any, any, ExtractFieldValidatorService> implements OnInit {
  @Input() hiddenFields: string[];
  @Output() fieldAdded = new EventEmitter<void>();

  @ViewChild('orderItemTypeHintComponent') orderItemTypeHintComponent: ReferentialFilterHintComponent;
  @ViewChild('orderItemTypeHintRef') orderItemTypeHintRef: ElementRef;
  @ViewChild('orderItemTypeHintDiv') orderItemTypeHintDiv: ElementRef;

  fieldDefinitions: ExtractFieldDefinition[];
  fieldDefinitionRecord: Record<string, ExtractFieldDefinition>;
  fieldTypes: ExtractFieldType[];
  fieldStaticTypes = extractFieldStaticTypes;

  isOrderItemType = ExtractFieldUtils.isOrderItemType;
  orderItemTypeHint = new BehaviorSubject<string>(undefined);
  private _orderItemTypeFilter: ISelectCriteria;

  constructor(
    protected injector: Injector,
    protected _entityService: ExtractFieldService,
    protected validator: ExtractFieldValidatorService,
    protected extractFilterService: ExtractFilterService
  ) {
    super(
      injector,
      RESERVED_START_COLUMNS.concat(['name', 'orderItemType', 'sortDirection']).concat(RESERVED_END_COLUMNS),
      ExtractField,
      _entityService,
      validator
    );
    this.i18nColumnPrefix = 'EXTRACTION.EXTRACT_FILTER.FIELDS.';
    this.logPrefix = '[extract-field-table]';
    this.defaultSortBy = 'rankOrder';

    this.extractFilterService.ready().then(() => {
      // Get field definitions
      const fieldDefinitions = this.extractFilterService.fieldDefinitions;
      // Sort by type and rank order
      fieldDefinitions.sort(
        (a, b) => orderedExtractFieldTypes.indexOf(a.type) - orderedExtractFieldTypes.indexOf(b.type) || a.rankOrder - b.rankOrder
      );
      this.fieldTypes = fieldDefinitions.map((value) => value.type).filter(uniqueValue);
      this.fieldDefinitions = fieldDefinitions;
      this.fieldDefinitionRecord = Utils.toRecord(this.fieldDefinitions, 'id');
      this.markForCheck();
    });
  }

  @Input() set orderItemTypeFilter(value: ISelectCriteria) {
    this._orderItemTypeFilter = value;
    this.renderOrderItemTypeHint();
  }

  @HostListener('keydown', ['$event'])
  async keyPressed(event: KeyboardEvent) {
    // console.debug('KEYDOWN', event);
    if (['ArrowUp', 'ArrowDown', 'PageUp', 'PageDown', 'Enter', 'Space'].includes(event.key)) {
      // fixme: Space is not prevented ...
      event.preventDefault();
      event.stopPropagation();
      if (this.selection.selected.length > 0) {
        if (event.key === 'ArrowUp' && this.canGoUp()) {
          await this.goUp();
        } else if (event.key === 'ArrowDown' && this.canGoDown()) {
          await this.goDown();
        } else if (event.key === 'PageUp' && this.canGoUp()) {
          await this.goTop();
        } else if (event.key === 'PageDown' && this.canGoDown()) {
          await this.goBottom();
        }
      }
    }
  }

  // Fixme: Find why keyPressed method never triggers
  // @HostListener('document:keydown.pageDown', ['$event'])
  // pageDownPressed(event: KeyboardEvent) {
  //   console.warn('pageDown down');
  // }

  ngOnInit() {
    // default properties
    this.subTable = true;
    this.selectRowOnEdit = false;

    super.ngOnInit();
  }

  displayFieldNameFn = (field: IExtractField): string =>
    Optional.ofNullable(this.fieldDefinitionRecord?.[field.name])
      .map((value) => value.name)
      .orElse(`${field.name} (not managed yet)`);

  async openAddFieldModal(event: UIEvent, fieldType: ExtractFieldType) {
    event?.preventDefault();

    await this.save();
    const existingFields: ExtractField[] = this.value || [];
    const existingFieldDefinitionIds = existingFields.map((value) => value.name);
    const excludedFieldDefinitionIds = [...existingFieldDefinitionIds, ...this.hiddenFields];

    const { role, data } = await this.modalService.openModal<IExtractFieldOptions, IExtractFieldDefinition[]>(ExtractFieldAddModal, {
      titleI18n: 'EXTRACTION.EXTRACT_FILTER.FIELDS.ADD_MODAL_TITLE',
      addCriteria: { excludedIds: excludedFieldDefinitionIds, type: fieldType },
      fieldDefinitions: this.fieldDefinitions.slice(),
    });

    if (role === 'validate' && data) {
      // Add new fields at the end
      let nextRankOrder = existingFields.reduce((rankOrder, field) => Math.max(rankOrder, field.rankOrder || 0), 0);
      for (const fieldDefinition of data) {
        existingFields.push(new ExtractField(fieldDefinition.id, fieldDefinition.type, 'NONE', ++nextRankOrder));
      }
      await this.setValue(existingFields);
      this.markAsDirty();
      this.fieldAdded.emit();
    }
  }

  async addStaticFields(event: UIEvent, type: ExtractFieldStaticType) {
    event?.preventDefault();

    await this.save();
    const existingFields: ExtractField[] = this.value || [];
    const existingFieldDefinitionIds = existingFields.map((value) => value.name);
    const allFieldDefinitions = this.fieldDefinitions.slice();
    const staticFieldDefinitions: ExtractFieldDefinition[] = [];
    switch (type) {
      case 'SANDRE':
        staticFieldDefinitions.push(...allFieldDefinitions.filter((value) => value.id.endsWith('_SANDRE')));
        break;
      case 'ID':
        staticFieldDefinitions.push(...allFieldDefinitions.filter((value) => value.id.endsWith('_ID')));
        break;
      case 'ALL':
        staticFieldDefinitions.push(...allFieldDefinitions);
    }

    // Add new fields at the end
    let nextRankOrder = existingFields.reduce((rankOrder, field) => Math.max(rankOrder, field.rankOrder || 0), 0);
    for (const fieldDefinition of staticFieldDefinitions) {
      if (existingFieldDefinitionIds.includes(fieldDefinition.id)) continue;
      existingFields.push(new ExtractField(fieldDefinition.id, fieldDefinition.type, 'NONE', ++nextRankOrder));
    }
    await this.setValue(existingFields);
    this.markAsDirty();
    this.fieldAdded.emit();
  }

  canGoUp(): boolean {
    return this.filterIsEmpty && (this.minSelectedRowId() > 0 || false);
  }

  canGoDown(): boolean {
    return this.filterIsEmpty && (this.maxSelectedRowId() < this.visibleRowCount - 1 || false);
  }

  async goUp(opts?: { direction?: number; focus?: boolean }) {
    if (await asyncEvery(this.selection.selected, (row: AsyncTableElement<ExtractField>) => this.confirmEditCreate(undefined, row))) {
      this.selection.selected
        .slice()
        // ensure order
        .sort((row1, row2) => row1.id - row2.id)
        .forEach((row) => this.moveRow(row.id, opts?.direction || -1));
      this.updateRankOrders();
      this.markAsDirty();
      if (opts?.focus !== false) {
        this.focusRow(this.dataSource.getRow(this.minSelectedRowId()));
      }
    }
  }

  async goDown(opts?: { direction?: number; focus?: boolean }) {
    if (await asyncEvery(this.selection.selected, (row: AsyncTableElement<ExtractField>) => this.confirmEditCreate(undefined, row))) {
      this.selection.selected
        .slice()
        // ensure reverse order
        .sort((row1, row2) => row2.id - row1.id)
        .forEach((row) => this.moveRow(row.id, opts?.direction || 1));
      this.updateRankOrders();
      this.markAsDirty();
      if (opts?.focus !== false) {
        this.focusRow(this.dataSource.getRow(this.maxSelectedRowId()));
      }
    }
  }

  async goTop() {
    await this.goUp({
      direction: -this.minSelectedRowId(),
      focus: false,
    });
    this.focusRow(this.dataSource.getRow(this.minSelectedRowId()));
  }

  async goBottom() {
    await this.goDown({
      direction: this.visibleRowCount - 1 - this.maxSelectedRowId(),
      focus: false,
    });
    this.focusRow(this.dataSource.getRow(this.maxSelectedRowId()));
  }

  refreshComponents() {
    this.renderOrderItemTypeHint();
  }

  applyOrderItemTypeHint() {
    this.cd.detectChanges();
    this.orderItemTypeHint.next(this.orderItemTypeHintDiv.nativeElement.innerHTML);
    this.markForCheck();
  }

  protected renderOrderItemTypeHint() {
    // Use a timeout to wait offsetWidth returns the correct value
    setTimeout(() => {
      if (!this.orderItemTypeHintRef?.nativeElement) return;
      this.orderItemTypeHintComponent?.render(this._orderItemTypeFilter, {
        font: window.getComputedStyle(this.orderItemTypeHintRef.nativeElement).getPropertyValue('font'),
        maxWidth: this.orderItemTypeHintRef.nativeElement.parentElement?.offsetWidth,
      });
    }, 50);
  }

  protected updateRankOrders() {
    this.getRows().forEach((row, index) => row.validator.patchValue({ rankOrder: index }));
  }
}
