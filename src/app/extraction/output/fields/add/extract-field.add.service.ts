import { Injectable } from '@angular/core';
import { ExtractFieldDefinition } from '@app/extraction/extract-field.model';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { ExtractFieldDefinitionFilter } from '@app/extraction/output/fields/add/filter/extract-field.add.filter.model';

@Injectable()
export class ExtractFieldAddService extends EntitiesMemoryService<ExtractFieldDefinition, ExtractFieldDefinitionFilter, string> {
  constructor() {
    super(ExtractFieldDefinition, ExtractFieldDefinitionFilter);
  }
}
