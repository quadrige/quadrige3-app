import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { ExtractFieldAddService } from '@app/extraction/output/fields/add/extract-field.add.service';
import { ExtractFieldDefinition } from '@app/extraction/extract-field.model';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import {
  ExtractFieldDefinitionFilter,
  ExtractFieldDefinitionFilterCriteria,
  IExtractFieldDefinitionFilterCriteria,
} from '@app/extraction/output/fields/add/filter/extract-field.add.filter.model';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';

@Component({
  selector: 'app-extract-field-add-table',
  templateUrl: './extract-field.add.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ExtractFieldAddService,
      useExisting: false,
    },
  ],
})
export class ExtractFieldAddTable
  extends BaseMemoryTable<ExtractFieldDefinition, string, ExtractFieldDefinitionFilter, ExtractFieldDefinitionFilterCriteria>
  implements OnInit, AfterViewInit, OnDestroy
{
  @Input() addCriteria: IExtractFieldDefinitionFilterCriteria;

  constructor(
    protected injector: Injector,
    protected _entityService: ExtractFieldAddService
  ) {
    super(injector, RESERVED_START_COLUMNS.concat(['name']).concat(RESERVED_END_COLUMNS), ExtractFieldDefinition, _entityService, undefined);
    this.logPrefix = '[extract-field-add-table]';
    this.defaultSortBy = 'rankOrder';
  }

  ngOnInit() {
    // default properties
    this.addTable = true;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.ready().then(() => {
      this.resetFilter();
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  async resetFilter(filter?: ExtractFieldDefinitionFilter, opts?: ISetFilterOptions) {
    filter = ExtractFieldDefinitionFilter.fromObject(filter || {});
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: this.addCriteria,
    });
  }
}
