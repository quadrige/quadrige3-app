import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import {
  ExtractFieldDefinitionFilter,
  ExtractFieldDefinitionFilterCriteria,
} from '@app/extraction/output/fields/add/filter/extract-field.add.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-extract-field-add-filter-form',
  templateUrl: './extract-field.add.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractFieldAddFilterForm extends BaseCriteriaFormComponent<ExtractFieldDefinitionFilter, ExtractFieldDefinitionFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.name;
  }
}
