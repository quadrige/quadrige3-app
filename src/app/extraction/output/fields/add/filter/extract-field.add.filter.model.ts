import { BaseEntityFilter, BaseEntityFilterCriteria, IBaseEntityFilterCriteria } from '@app/shared/model/filter.model';
import { FilterFn } from '@sumaris-net/ngx-components';
import { ExtractFieldDefinition, ExtractFieldType } from '@app/extraction/extract-field.model';

export interface IExtractFieldDefinitionFilterCriteria extends IBaseEntityFilterCriteria<string> {
  type: ExtractFieldType;
}

export class ExtractFieldDefinitionFilterCriteria
  extends BaseEntityFilterCriteria<ExtractFieldDefinition, string>
  implements IExtractFieldDefinitionFilterCriteria
{
  type: ExtractFieldType = null;

  static fromObject(source: any): ExtractFieldDefinitionFilterCriteria {
    const target = new ExtractFieldDefinitionFilterCriteria();
    target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.searchAttributes = ['name']; // default search attribute
    this.type = source.type;
  }

  protected buildFilter(): FilterFn<ExtractFieldDefinition>[] {
    const filterFns = super.buildFilter() || [];

    if (this.type) {
      filterFns.push((data) => this.type === data?.type);
    }

    return filterFns;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'type') return false;
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

export class ExtractFieldDefinitionFilter extends BaseEntityFilter<
  ExtractFieldDefinitionFilter,
  ExtractFieldDefinitionFilterCriteria,
  ExtractFieldDefinition,
  string
> {
  static fromObject(source: any): ExtractFieldDefinitionFilter {
    const target = new ExtractFieldDefinitionFilter();
    target.fromObject(source);
    return target;
  }

  criteriaFromObject(source: any): ExtractFieldDefinitionFilterCriteria {
    return ExtractFieldDefinitionFilterCriteria.fromObject(source);
  }
}
