import { Component, Injector, Input, ViewChild } from '@angular/core';
import { ExtractFieldAddTable } from '@app/extraction/output/fields/add/extract-field.add.table';
import { IExtractFieldDefinition } from '@app/extraction/extract-field.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';
import { IExtractFieldDefinitionFilterCriteria } from '@app/extraction/output/fields/add/filter/extract-field.add.filter.model';

export interface IExtractFieldOptions extends IModalOptions {
  addCriteria: IExtractFieldDefinitionFilterCriteria;
  fieldDefinitions: IExtractFieldDefinition[];
}

@Component({
  selector: 'app-extract-field-add-modal',
  templateUrl: './extract-field.add.modal.html',
  styleUrls: ['./extract-field.add.modal.scss'],
})
export class ExtractFieldAddModal extends ModalComponent<IExtractFieldDefinition[]> implements IExtractFieldOptions {
  @Input() addCriteria: IExtractFieldDefinitionFilterCriteria;
  @Input() fieldDefinitions: IExtractFieldDefinition[];

  @ViewChild('extractFieldAddTable') table: ExtractFieldAddTable;

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled(): boolean {
    return super.disabled || (this.table ? this.table.selection.isEmpty() : true);
  }

  protected async afterInit() {
    await this.table.setValue(this.fieldDefinitions);
    this.table.markAsReady();
  }

  protected dataToValidate(): IExtractFieldDefinition[] {
    return this.table.selectedEntities;
  }
}
