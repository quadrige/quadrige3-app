import { Entity, EntityAsObjectOptions } from '@sumaris-net/ngx-components';
import {
  ExtractFileType,
  MainExtractFileType,
  mainFileTypeList,
  ShapefileExtractFileType,
  shapefileFileTypeList,
} from '@app/extraction/extract-filter.model';
import { ExtractField, IExtractField } from '@app/extraction/extract-field.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { Utils } from '@app/shared/utils';
import { ExtractUtils } from '@app/extraction/extract.utils';
import { FilterValue } from '@app/referential/filter/model/filter-criteria-value.model';
import { IReferentialFilterCriteria, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { CriteriaDefinitionRecord } from '@app/extraction/extract.type';

const getValue = (criterias: FilterCriteria[], property: keyof IExtractOutput, defaultValue?) =>
  ExtractUtils.getValue<IExtractOutput>(criterias, outputCriteriaDefinition, property, defaultValue);
const setValue = (criterias: FilterCriteria[], property: keyof IExtractOutput, newValue: FilterValue) =>
  ExtractUtils.setValue<IExtractOutput>(criterias, outputCriteriaDefinition, property, newValue);
const getReferentialFilterCriteria = (criterias: FilterCriteria[], property: keyof IExtractOutput) =>
  ExtractUtils.getReferentialFilterCriteria<IExtractOutput>(criterias, outputCriteriaDefinition, property);
const setReferentialFilterCriteria = (
  criterias: FilterCriteria[],
  property: keyof IExtractOutput,
  referentialFilter: IReferentialFilterCriteria<any>
) => ExtractUtils.setReferentialFilterCriteria<IExtractOutput>(criterias, outputCriteriaDefinition, property, referentialFilter);

export interface IExtractOutput {
  fileTypes?: ExtractFileType[];
  mainFileType: MainExtractFileType;
  shapefileFileTypes: ShapefileExtractFileType[];
  withDataUnderMoratorium: boolean;
  withUserPersonalData: boolean;
  orderItemTypeFilter: IReferentialFilterCriteria<string>;
  fields: IExtractField[];
  hiddenFields: string[];
}

export const outputCriteriaDefinition: CriteriaDefinitionRecord<IExtractOutput> = {
  withDataUnderMoratorium: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.MAIN.WITH_DATA_UNDER_MORATORIUM',
    valueCriteriaType: 'EXTRACT_WITH_DATA_UNDER_MORATORIUM',
    valueOperatorType: 'BOOLEAN',
  },
  withUserPersonalData: {
    i18nLabel: 'EXTRACTION.EXTRACT_FILTER.DEFINITION.MAIN.WITH_USER_PERSONAL_DATA',
    valueCriteriaType: 'EXTRACT_WITH_USER_PERSONAL_DATA',
    valueOperatorType: 'BOOLEAN',
  },
  orderItemTypeFilter: {
    i18nLabel: 'REFERENTIAL.ENTITY.ORDER_ITEM_TYPE',
    valuesCriteriaType: 'EXTRACT_RESULT_ORDER_ITEM_TYPE_ID',
    valueCriteriaType: 'EXTRACT_RESULT_ORDER_ITEM_TYPE_NAME',
  },
};

export class ExtractOutput extends Entity<ExtractOutput> implements IExtractOutput {
  mainFileType: MainExtractFileType;
  shapefileFileTypes: ShapefileExtractFileType[];
  withDataUnderMoratorium = false;
  withUserPersonalData = false;
  orderItemTypeFilter: StrReferentialFilterCriteria;
  fields: ExtractField[];
  hiddenFields: string[];

  constructor() {
    super('ExtractOutput');
  }

  static parse(criterias: FilterCriteria[]): Partial<IExtractOutput> {
    return {
      withDataUnderMoratorium: Utils.fromBooleanString(getValue(criterias, 'withDataUnderMoratorium').value, false),
      withUserPersonalData: Utils.fromBooleanString(getValue(criterias, 'withUserPersonalData').value, false),
      orderItemTypeFilter: getReferentialFilterCriteria(criterias, 'orderItemTypeFilter'),
    };
  }

  static merge(criterias: FilterCriteria[], value: IExtractOutput) {
    setValue(criterias, 'withDataUnderMoratorium', { value: Utils.toBooleanString(value?.withDataUnderMoratorium) });
    setValue(criterias, 'withUserPersonalData', { value: Utils.toBooleanString(value?.withUserPersonalData) });
    setReferentialFilterCriteria(criterias, 'orderItemTypeFilter', StrReferentialFilterCriteria.fromObject(value?.orderItemTypeFilter));
  }

  static fromObject(source: any): ExtractOutput {
    const target = new ExtractOutput();
    target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    const fileTypes: ExtractFileType[] = source.fileTypes || [];
    this.mainFileType = source.mainFileType || fileTypes.filter((value) => mainFileTypeList.map((type) => type.id).includes(value))[0]; // Should be only 1 file type
    this.shapefileFileTypes =
      source.shapefileFileTypes || fileTypes.filter((value) => shapefileFileTypeList.map((type) => type.id).includes(value)) || [];
    this.withDataUnderMoratorium = source.withDataUnderMoratorium;
    this.withUserPersonalData = source.withUserPersonalData;
    this.orderItemTypeFilter = StrReferentialFilterCriteria.fromObject(source.orderItemTypeFilter || {});
    this.fields = (source.fields || []).map(ExtractField.fromObject);
    this.hiddenFields = source.hiddenFields || [];
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target: IExtractOutput = super.asObject(opts) as any;
    target.orderItemTypeFilter = this.orderItemTypeFilter?.asObject(opts);
    target.fileTypes = [this.mainFileType, ...this.shapefileFileTypes];
    target.fields = this.fields?.map((value) => value.asObject(opts));
    return target;
  }
}
