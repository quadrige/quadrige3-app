import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { LocalSettingsService } from '@sumaris-net/ngx-components';
import { distinctUntilChanged, filter, throttleTime } from 'rxjs/operators';
import { ExtractOutputValidator } from '@app/extraction/output/extract-output.validator';
import { ExtractField, ExtractFieldUtils, IExtractField } from '@app/extraction/extract-field.model';
import { BaseForm } from '@app/shared/form/base.form';
import { ExtractFieldTable } from '@app/extraction/output/fields/extract-field.table';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';
import { IExtractOutput, outputCriteriaDefinition } from '@app/extraction/output/extract-output.model';
import { ExtractFileType, mainFileTypeList, mainFileTypesWithShapefile, shapefileFileTypeList } from '@app/extraction/extract-filter.model';
import { ExtractFilterService } from '@app/extraction/extract-filter.service';
import { ISimpleType } from '@app/shared/model/interface';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-extract-output-form',
  templateUrl: './extract-output.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractOutputForm extends BaseForm<IExtractOutput> implements OnInit, AfterViewInit, OnDestroy {
  @Input() i18nFieldPrefix = '';
  @ViewChild('fieldTable') fieldTable: ExtractFieldTable;
  @ViewChildren(ReferentialFilterFormField) referentialFilterFormFields: QueryList<ReferentialFilterFormField>;

  criteriaDefinition = outputCriteriaDefinition;
  availableMainFileTypeList: ISimpleType<ExtractFileType>[];
  availableShapefileFileTypeList: ISimpleType<ExtractFileType>[];
  orderItemTypeFilterEnabled = false;
  withUserPersonalDataEnabled = false;
  withDataUnderMoratoriumEnabled = false;

  private _preventDirty = false;

  constructor(
    protected injector: Injector,
    protected settings: LocalSettingsService,
    protected validatorService: ExtractOutputValidator,
    protected extractFilterService: ExtractFilterService
  ) {
    super(injector, validatorService.getFormGroup());
    this.logPrefix = '[extract-output]';

    // Set availableMainFileTypeList with extractionType
    this.availableMainFileTypeList =
      extractFilterService.type === 'RESULT' ? [...mainFileTypeList] : mainFileTypeList.filter((value) => value.id !== 'SANDRE_QELI');

    // Set availableShapefileFileTypeList with extractionType
    let shapefileFileTypes: ExtractFileType[];
    switch (extractFilterService.type) {
      case 'SURVEY':
        shapefileFileTypes = ['SHAPEFILE_MONITORING_LOCATION', 'SHAPEFILE_SURVEY'];
        break;
      case 'SAMPLING_OPERATION':
      case 'SAMPLE':
      case 'IN_SITU_WITHOUT_RESULT':
      case 'RESULT':
        shapefileFileTypes = ['SHAPEFILE_MONITORING_LOCATION', 'SHAPEFILE_SURVEY', 'SHAPEFILE_SAMPLING_OPERATION'];
        break;
      case 'CAMPAIGN':
        shapefileFileTypes = ['SHAPEFILE_CAMPAIGN'];
        break;
      case 'OCCASION':
        shapefileFileTypes = ['SHAPEFILE_CAMPAIGN', 'SHAPEFILE_OCCASION'];
        break;
      case 'EVENT':
        shapefileFileTypes = ['SHAPEFILE_EVENT'];
        break;
    }
    this.availableShapefileFileTypeList = shapefileFileTypeList.filter((type) => shapefileFileTypes.includes(type.id));

    // Set default output options
    this.withDataUnderMoratoriumEnabled = ['SURVEY', 'SAMPLING_OPERATION', 'SAMPLE', 'IN_SITU_WITHOUT_RESULT', 'RESULT'].includes(
      extractFilterService.type
    );
  }

  get dirty(): boolean {
    return super.dirty || this.fieldTable?.dirty || false;
  }

  get invalid(): boolean {
    return super.invalid || this.fieldTable?.invalid || false;
  }

  get valid(): boolean {
    return super.valid && (this.fieldTable?.valid || false);
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.form.valueChanges
        .pipe(
          // Prevent event if processing
          filter(() => !this._preventDirty),
          throttleTime(50)
        )
        .subscribe(() => {
          // if (this.debug) console.debug(`${this.logPrefix} value`, value);
          if (this.loaded && this.enabled && this.dirty) {
            this.markAsDirty();
            this.refreshComponents();
          }
        })
    );
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.registerSubscription(
      this.form.controls.mainFileType.valueChanges
        .pipe(
          filter(() => this.loaded),
          distinctUntilChanged()
        )
        .subscribe((fileType) => {
          this.form.markAsTouched();
          if (mainFileTypesWithShapefile.includes(fileType)) {
            this.form.controls.shapefileFileTypes.enable();
          } else {
            this.form.controls.shapefileFileTypes.disable();
            this.form.controls.shapefileFileTypes.patchValue([]);
          }
        })
    );
  }

  clear() {
    this.value = {};
    this.disable();
  }

  refreshComponents() {
    setTimeout(() => {
      this.referentialFilterFormFields?.forEach((item) => item.onBlur());
      this.fieldTable.refreshComponents();
      this.markForCheck();
    }, 100);
  }

  getValidationError(): string {
    return this.getFormError(this.form, { separator: '<br>' });
  }

  async setValue(data: IExtractOutput, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> {
    this.markAsLoading({ emitEvent: false });
    this.markAsUntouched({ emitEvent: false });
    super.setValue(data, opts);

    // Convert fields to ExtractFields, removing hidden fields
    await this.fieldTable.setValue(data.fields?.filter((field) => !data.hiddenFields.includes(field.name)).map(ExtractField.fromObject));

    this.checkOrderItemTypeField(data.fields);
    this.checkUserPersonalDataTypeField(data.fields);
    this.markAsPristine({ emitEvent: false });
    this.markAsLoaded({ emitEvent: false });
    this.refreshComponents();
  }

  checkOrderItemTypeField(fields: IExtractField[]) {
    this.orderItemTypeFilterEnabled = !!fields?.find(ExtractFieldUtils.isOrderItemType);
    this.validatorService.updateFormGroup(this.form, this.value, { orderItemTypeFilterRequired: this.orderItemTypeFilterEnabled });
  }

  checkUserPersonalDataTypeField(fields: IExtractField[]) {
    this.withUserPersonalDataEnabled = !!fields?.find(ExtractFieldUtils.isUserPersonalDataType);
  }

  async fieldTableChanged(dirty: boolean) {
    if (this.loaded && dirty) {
      const fields = this.fieldTable.getRows().map((row) => row.currentData);
      this.checkOrderItemTypeField(fields);
      this.checkUserPersonalDataTypeField(fields);
      this.markAsDirty();
      this.refreshComponents();
    }
  }

  // Make setError public
  setError(value: string, opts?: { emitEvent?: boolean }) {
    super.setError(value, opts);
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this.fieldTable?.enable(opts);
    super.enable(opts);
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this.fieldTable?.disable(opts);
    super.disable(opts);
  }

  markAsLoading(opts?: { emitEvent?: boolean }) {
    super.markAsLoading(opts);
    this.fieldTable?.markAsLoading(opts);
  }

  markAsLoaded(opts?: { emitEvent?: boolean }) {
    super.markAsLoaded(opts);
    this.fieldTable?.markAsLoaded(opts);
  }

  afterFieldRemoved() {
    this.refreshComponents();
  }
}
