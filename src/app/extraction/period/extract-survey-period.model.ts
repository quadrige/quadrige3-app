import { EntityClass, fromDateISOString, isNil, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { Moment } from 'moment/moment';
import { Dates } from '@app/shared/dates';

@EntityClass({ typename: 'ExtractSurveyPeriodVO' })
export class ExtractSurveyPeriod extends Referential<ExtractSurveyPeriod> {
  static entityName = 'ExtractSurveyPeriod';
  static fromObject: (source: any, opts?: any) => ExtractSurveyPeriod;

  startDate: Moment;
  endDate: Moment;

  constructor(startDate?: Moment, endDate?: Moment) {
    super(ExtractSurveyPeriod.TYPENAME);
    this.entityName = ExtractSurveyPeriod.entityName;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = ExtractSurveyPeriod.entityName;
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.startDate = Dates.toLocalDateString(this.startDate);
    target.endDate = Dates.toLocalDateString(this.endDate);
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.statusId;
    return target;
  }

  equals(other: ExtractSurveyPeriod): boolean {
    return !!other && this.dateEquals(other);
  }

  dateEquals(other: ExtractSurveyPeriod): boolean {
    return (
      ((isNil(this.startDate) && isNil(other.startDate)) || this.startDate?.isSame(other.startDate, 'date')) &&
      ((isNil(this.endDate) && isNil(other.endDate)) || this.endDate?.isSame(other.endDate, 'date'))
    );
  }
}
