import { ExtractSurveyPeriod } from '@app/extraction/period/extract-survey-period.model';
import { AppListForm, isNil, LocalSettingsService } from '@sumaris-net/ngx-components';
import { DateAdapter } from '@angular/material/core';
import { AbstractControl, UntypedFormBuilder } from '@angular/forms';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, Input } from '@angular/core';
import { Moment } from 'moment';
import { ExtractSurveyPeriodValidatorService } from '@app/extraction/period/extract-survey-period.validator';
import { Alerts } from '@app/shared/alerts';
import { AlertController } from '@ionic/angular';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-extract-survey-period-form',
  templateUrl: 'extract-survey-period.form.html',
  styleUrls: ['./extract-survey-period.form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtractSurveyPeriodForm extends AppListForm<ExtractSurveyPeriod> {
  @Input() canEdit?: boolean;
  focus = false;

  constructor(
    protected injector: Injector,
    protected formBuilder: UntypedFormBuilder,
    protected dateAdapter: DateAdapter<Moment>,
    protected settings: LocalSettingsService,
    protected cd: ChangeDetectorRef,
    protected validatorService: ExtractSurveyPeriodValidatorService,
    protected alertController: AlertController
  ) {
    super(injector, formBuilder, dateAdapter, settings, cd, undefined);

    this.equalsFn = (v1, v2) => (isNil(v1) && isNil(v2)) || (v1 && v2 && v1.equals(v2));

    this.i18nFieldPrefix = 'EXTRACTION.EXTRACT_FILTER.PERIODS.';
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.canEdit === false) {
      this.disable(opts);
      return;
    }
    super.enable(opts);
  }

  add(value: ExtractSurveyPeriod, opts?: { emitEvent?: boolean }) {
    super.add(value, opts);
    this.focus = true;
    setTimeout(() => (this.focus = false));
  }

  async remove(index: number) {
    // As confirmation
    if (!(await Alerts.askDeleteConfirmation(this.alertController, this.translate))) {
      return;
    }
    this.removeAt(index);
  }

  protected createControl(data?: any): AbstractControl {
    return this.validatorService.getFormGroup(data);
  }
}
