import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { ExtractSurveyPeriod } from '@app/extraction/period/extract-survey-period.model';

@Injectable({ providedIn: 'root' })
export class ExtractSurveyPeriodValidatorService extends ReferentialValidatorService<ExtractSurveyPeriod> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ExtractSurveyPeriod, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return {
      id: [data?.id || null],
      startDate: [data?.startDate || null],
      endDate: [data?.endDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: ExtractSurveyPeriod, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [SharedFormGroupValidators.dateRange('startDate', 'endDate', { fieldOnly: true })],
    };
  }
}
