import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialGroupAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { User } from '@app/referential/user/user.model';
import { UserService } from '@app/referential/user/user.service';

@Injectable({ providedIn: 'root' })
export class UserValidatorService extends ReferentialValidatorService<User> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: UserService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: User, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      label: [
        data?.label || null,
        Validators.maxLength(lengthLabel),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data, 'label'),
      ],
      name: [data?.name || null, Validators.compose([Validators.required, Validators.maxLength(lengthName)])],
      firstName: [data?.firstName || null, Validators.compose([Validators.required, Validators.maxLength(lengthName)])],
      intranetLogin: [data?.intranetLogin || null, Validators.maxLength(lengthLabel)],
      extranetLogin: [data?.extranetLogin || null, Validators.maxLength(lengthLabel)],
      email: [
        data?.email || null,
        Validators.compose([Validators.email, Validators.maxLength(lengthDescription)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data, 'email'),
      ],
      address: [data?.address || null, Validators.maxLength(lengthDescription)],
      phone: [data?.phone || null, Validators.maxLength(lengthDescription)],
      organism: [data?.organism || null, Validators.maxLength(lengthDescription)],
      center: [data?.center || null, Validators.maxLength(lengthDescription)],
      site: [data?.site || null, Validators.maxLength(lengthDescription)],
      ldapPresent: [data?.ldapPresent || false],
      department: [data?.department || null, Validators.compose([Validators.required, SharedValidators.entity])],
      privilegeIds: [data?.privilegeIds || null],
      trainings: [data?.trainings || null],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      rightsLoaded: [data?.rightsLoaded || null],
      programRights: [data?.programRights || null],
      metaProgramRights: [data?.metaProgramRights || null],
      ruleListRights: [data?.ruleListRights || null],
    };
  }

  getFormGroupOptions(data?: User, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      asyncValidators: ReferentialGroupAsyncValidators.checkAttributesAlreadyExists(
        this.service,
        this.dataSource,
        opts,
        data,
        ['name', 'firstName'],
        'REFERENTIAL.USER.ERROR.NAME_ALREADY_EXISTS'
      ),
    };
  }
}
