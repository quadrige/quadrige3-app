import { ChangeDetectionStrategy, Component, Injector, OnInit, Optional, viewChild } from '@angular/core';
import {
  AppFormUtils,
  isNotEmptyArray,
  isNotNil,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StatusIds,
} from '@sumaris-net/ngx-components';
import { User } from '@app/referential/user/user.model';
import { UserValidatorService } from '@app/referential/user/user.validator';
import { UserService } from '@app/referential/user/user.service';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { UserLdapAddModal } from '@app/referential/user/ldap/user-ldap.add.modal';
import { UserLdapService } from '@app/referential/user/ldap/user-ldap.service';
import { Alerts } from '@app/shared/alerts';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { coreConfigOptions } from '@app/core/config/core.config';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { IStatusMultiEditModalOptions, StatusComposite, StatusMultiEditModal } from '@app/shared/component/multi-edit/status.multi-edit.modal';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { UserFilter, UserFilterCriteria } from '@app/referential/user/filter/user.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { ProgramRightTable } from '@app/referential/right/program/program-right.table';
import { MetaProgramRightTable } from '@app/referential/right/meta-program/meta-program-right.table';
import { RuleListRightTable } from '@app/referential/right/rule-list/rule-list-right.table';
import { BaseRightTable } from '@app/referential/right/base-right.table';
import { IRightModel, RightFetchOptions } from '@app/referential/right/right.model';
import { UserTrainingTable } from '@app/referential/user/training/user-training.table';
import { IAddResult, IModalOptions } from '@app/shared/model/options.model';
import { ExportOptions } from '@app/shared/table/table.model';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { Utils } from '@app/shared/utils';

type UserView =
  | 'privilegeTable'
  | 'programRightTable'
  | 'metaProgramRightTable'
  | 'ruleListRightTable'
  | 'userTrainingTable'
  | 'transcribingItemTable';

@Component({
  selector: 'app-user-table',
  templateUrl: './user.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserTable extends ReferentialTable<User, number, UserFilter, UserFilterCriteria, UserValidatorService, UserView> implements OnInit {
  privilegeTable = viewChild<GenericSelectTable>('privilegeTable');
  programRightTable = viewChild<ProgramRightTable>('programRightTable');
  metaProgramRightTable = viewChild<MetaProgramRightTable>('metaProgramRightTable');
  ruleListRightTable = viewChild<RuleListRightTable>('ruleListRightTable');
  userTrainingTable = viewChild<UserTrainingTable>('userTrainingTable');

  rightAreaEnabled = false;
  rightFetchOptions: RightFetchOptions;
  defaultDepartmentId: string;
  canExport: boolean;
  preloadRightsEnabled = true;

  privilegeMenuItem: IEntityMenuItem<User, UserView> = {
    title: 'REFERENTIAL.USER.PRIVILEGES',
    view: 'privilegeTable',
  };

  userRightMenuItem: IEntityMenuItem<User, UserView> = {
    title: 'REFERENTIAL.RIGHT.TITLE',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.PROGRAM_STRATEGIES',
        viewTitle: 'REFERENTIAL.ENTITY.PROGRAMS',
        view: 'programRightTable',
      },
      {
        title: 'REFERENTIAL.ENTITY.META_PROGRAMS',
        view: 'metaProgramRightTable',
      },
      {
        title: 'REFERENTIAL.ENTITY.RULE_LISTS',
        view: 'ruleListRightTable',
      },
    ],
  };

  userTrainingMenuItem: IEntityMenuItem<User, UserView> = {
    title: 'REFERENTIAL.USER.USER_TRAININGS',
    attribute: 'trainings',
    view: 'userTrainingTable',
  };

  constructor(
    protected injector: Injector,
    protected _entityService: UserService,
    protected validatorService: UserValidatorService,
    @Optional() protected ldapService?: UserLdapService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'name',
        'firstName',
        'department',
        'ldapPresent',
        'intranetLogin',
        'extranetLogin',
        'email',
        'label',
        'address',
        'phone',
        'organism',
        'center',
        'site',
        'comments',
        'statusId',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      User,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.USER.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[user-table]';
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data = this.singleSelectedRow.currentData;
      return (
        isNotEmptyArray(data.privilegeIds) ||
        isNotEmptyArray(data.programRights) ||
        isNotEmptyArray(data.metaProgramRights) ||
        isNotEmptyArray(data.ruleListRights)
      );
    }
  }

  ngOnInit() {
    super.ngOnInit();

    // department combo
    this.registerAutocompleteField('department', {
      ...this.referentialOptions.department,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Department',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Listen configuration
    this.registerSubscription(
      this.configService.config.subscribe((config) => {
        this.defaultDepartmentId = config.getProperty(coreConfigOptions.defaultDepartment);
        if (this.debug && !!this.defaultDepartmentId) {
          console.debug(`${this.logPrefix} Imported user from LDAP will have default department id = ${this.defaultDepartmentId}`);
        }
      })
    );
  }

  updatePermission() {
    this.canExport = this.permissionService.hasSomeManagementPermission;
    super.updatePermission();
  }

  async openAddFromLdapModal(event: any) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IModalOptions, IAddResult>(
      UserLdapAddModal,
      {
        titleI18n: 'REFERENTIAL.USER.LDAP.ADD',
      },
      'modal-medium'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      for (const ldapUser of data.added) {
        if (this.defaultDepartmentId) {
          // Affect default department if none
          if (!ldapUser.department) {
            ldapUser.department = await this.referentialGenericService.load(this.defaultDepartmentId, { entityName: 'Department' });
          }
        } else {
          // If department is missing or flagged as disabled, warn user and cancel import
          if (!ldapUser.department) {
            await Alerts.showError('REFERENTIAL.USER.LDAP.MISSING_DEPARTMENT', this.alertCtrl, this.translate, undefined, {
              user: referentialToString(ldapUser, ['firstName', 'name']),
            });
            return;
          } else if (ldapUser.department.statusId === StatusIds.DISABLE) {
            await Alerts.showError('REFERENTIAL.USER.LDAP.UNKNOWN_DEPARTMENT', this.alertCtrl, this.translate, undefined, {
              user: referentialToString(ldapUser, ['firstName', 'name']),
              department: ldapUser.department.label,
            });
            return;
          }
        }

        const newRow = await this.addRowToTable();
        newRow.validator.patchValue({ ...ldapUser, id: null, ldapPresent: true, statusId: 1 });
        newRow.validator.markAsDirty();

        let rowValid = await this.confirmEditCreate(event, newRow);
        if (!rowValid) {
          if (newRow.validator.pending) {
            await AppFormUtils.waitWhilePending(newRow.validator);
            rowValid = await this.confirmEditCreate(event, newRow);
          }
        }
        if (!rowValid) {
          console.warn(`${this.logPrefix} This user cannot be imported due to validation error`, newRow);

          break;
        }
      }
      this.markAsDirty();
    }
  }

  isReadOnly(): ReadOnlyIfFn {
    return (row) => row.currentData?.ldapPresent;
  }

  canUpdateFromLdap(): boolean {
    return this.singleSelectedRow?.currentData.ldapPresent || false;
  }

  async updateFromLdap(event: any) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate()) || !this.canUpdateFromLdap()) {
      return;
    }

    const row = this.singleSelectedRow;
    const ldapUser = await this.ldapService?.loadByLabel(row.currentData.label);
    if (!ldapUser) {
      await Alerts.showError('REFERENTIAL.USER.LDAP.NOT_FOUND', this.alertCtrl, this.translate);
      return;
    }

    const partialLdapUser = {
      label: ldapUser.label,
      name: ldapUser.name,
      firstName: ldapUser.firstName,
      department: ldapUser.department,
      intranetLogin: ldapUser.intranetLogin,
      extranetLogin: ldapUser.extranetLogin,
      email: ldapUser.email,
      phone: ldapUser.phone,
      organism: ldapUser.organism,
      center: ldapUser.center,
      site: ldapUser.site,
    };

    // Update default department if provided (Mantis #56505)
    if (!partialLdapUser.department && !!this.defaultDepartmentId) {
      const defaultDepartment = await this.referentialGenericService.load(this.defaultDepartmentId, { entityName: 'Department' });
      partialLdapUser.department = defaultDepartment.asObject();
    }

    // check difference
    if (!EntityUtils.deepEquals(row.currentData, partialLdapUser, Object.keys(partialLdapUser))) {
      // difference found: patch row
      row.validator.patchValue({ ...partialLdapUser });

      await Alerts.showError('REFERENTIAL.USER.LDAP.UPDATED', this.alertCtrl, this.translate, { titleKey: 'INFO.ALERT_HEADER' });
      this.markRowAsDirty(row);
    } else {
      await Alerts.showError('REFERENTIAL.USER.LDAP.NOT_UPDATED', this.alertCtrl, this.translate, { titleKey: 'INFO.ALERT_HEADER' });
    }
  }

  privilegeTableChanged(values: string[]) {
    if (this.canEdit) {
      this.patchRow('privilegeIds', values);
    }
  }

  async multiEditRows(event: MouseEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IStatusMultiEditModalOptions, StatusComposite>(
      StatusMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.USER.MULTI_EDIT.TITLE',
        selection: this.selection.selected.map((row) => row.currentData),
      },
      'modal-200'
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // Patch selected rows
      this.selection.selected.forEach((row) => {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    }
  }

  protected async loadRightArea(row?: AsyncTableElement<User>): Promise<void> {
    if (this.subTable) return;

    // Preload rights for current row
    if (this.preloadRightsEnabled) {
      await this.preloadRights(row);
    }

    switch (this.rightMenuItem?.view) {
      case 'privilegeTable': {
        this.detectChanges();
        if (this.privilegeTable()) {
          this.registerSubForm(this.privilegeTable());
          await this.loadPrivilegeTable(row);
        }
        break;
      }
      case 'programRightTable': {
        this.detectChanges();
        this.registerSubForm(this.programRightTable());
        await this.loadRightTable(this.programRightTable(), 'programRights', (userIds) => this._entityService.getProgramRights(userIds));
        break;
      }
      case 'metaProgramRightTable': {
        this.detectChanges();
        this.registerSubForm(this.metaProgramRightTable());
        await this.loadRightTable(this.metaProgramRightTable(), 'metaProgramRights', (userIds) => this._entityService.getMetaProgramRights(userIds));
        break;
      }
      case 'ruleListRightTable': {
        this.detectChanges();
        this.registerSubForm(this.ruleListRightTable());
        await this.loadRightTable(this.ruleListRightTable(), 'ruleListRights', (userIds) => this._entityService.getRuleListRights(userIds));
        break;
      }
      case 'userTrainingTable': {
        this.detectChanges();
        if (this.userTrainingTable()) {
          this.registerSubForm(this.userTrainingTable());
          await this.loadUserTrainings(row);
        }
        break;
      }
    }
    return super.loadRightArea(row);
  }

  protected async loadPrivilegeTable(row?: AsyncTableElement<User>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.privilegeTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected privileges
    await this.privilegeTable().setSelectedIds((row.currentData?.privilegeIds || []).slice());
    this.rightAreaEnabled = this.canEdit;
  }

  protected async loadUserTrainings(row?: AsyncTableElement<User>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.userTrainingTable().setValue(undefined);
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected privileges
    await this.userTrainingTable().setValue((row.currentData.trainings || []).slice());
    this.rightAreaEnabled = this.canEdit;
  }

  protected async loadRightTable(
    rightTable: BaseRightTable<any>,
    rightsProperty: 'programRights' | 'metaProgramRights' | 'ruleListRights',
    load: (userIds: number[]) => Promise<IRightModel[]>
  ) {
    await rightTable.setValue(undefined);
    if (this.selection.isEmpty()) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      return;
    }

    await rightTable.waitIdle();
    rightTable.markAsLoading();
    const userIds = this.selection.selected.map((value) => value.currentData.id).filter(isNotNil);
    const row = this.singleEditingRow || this.singleSelectedRow;
    let rights: IRightModel[];
    if (userIds.length === 1 && userIds[0] === row.currentData.id && row.currentData.rightsLoaded) {
      rights = row.currentData[rightsProperty];
    } else {
      rights = await load(userIds);
    }
    await rightTable.setValue(rights);
  }

  protected async preloadRights(row?: AsyncTableElement<User>) {
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!!row) {
      if (!row.currentData.rightsLoaded) {
        const id = row.currentData.id;
        const [programRights, metaProgramRights, ruleListRights] = await Promise.all([
          this._entityService.getProgramRights([id]),
          this._entityService.getMetaProgramRights([id]),
          this._entityService.getRuleListRights([id]),
        ]);
        row.validator.patchValue({ programRights, metaProgramRights, ruleListRights, rightsLoaded: true });
      }
    }
  }

  protected async canRightMenuItemChange(
    previousMenuItem: IEntityMenuItem<User, TranscribingItemView | UserView>,
    nextMenuItem: IEntityMenuItem<User, TranscribingItemView | UserView>
  ): Promise<boolean> {
    return Utils.promiseEveryTrue([
      super.canRightMenuItemChange(previousMenuItem, nextMenuItem),
      this.saveRightView(this.userTrainingMenuItem, this.userTrainingTable()),
    ]);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<User>): Promise<boolean> {
    row = row || this.singleEditingRow;

    if (row && !(await this.saveRightView(this.userTrainingMenuItem, this.userTrainingTable(), { row }))) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  // async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
  //   const continueSave = await this.saveRightView(this.userTrainingMenuItem, this.userTrainingTable());
  //   if (continueSave) {
  //     return super.save(opts);
  //   }
  //   return false;
  // }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { ...opts, rowCountWarningThreshold: 50, rightsEnabled: true });
  }

  protected async defaultNewRowValue() {
    return {
      ...(await super.defaultNewRowValue()),
      ldapPresent: false,
    };
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.USERS' });
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<User, UserView>[] {
    return [this.privilegeMenuItem, this.userRightMenuItem, this.userTrainingMenuItem];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<User, UserView> {
    return this.privilegeMenuItem;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('label', 'address', 'phone', 'organism', 'center', 'site', 'comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'firstName', 'department');
  }
}
