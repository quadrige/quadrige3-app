import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { Department } from '@app/referential/department/department.model';
import { UserTraining } from '@app/referential/user/training/user-training.model';
import { ProgramRight } from '@app/referential/right/program/program-right.model';
import { MetaProgramRight } from '@app/referential/right/meta-program/meta-program-right.model';
import { RuleListRight } from '@app/referential/right/rule-list/rule-list-right.model';

@EntityClass({ typename: 'UserVO' })
export class User extends Referential<User> {
  static entityName = 'User';
  static fromObject: (source: any, opts?: any) => User;

  firstName: string = null;
  intranetLogin: string = null;
  extranetLogin: string = null;
  email: string = null;
  address: string = null;
  phone: string = null;
  organism: string = null;
  center: string = null;
  site: string = null;
  ldapPresent: boolean = null;
  department: Department = null;
  privilegeIds: string[] = null;
  trainings: UserTraining[] = null;

  rightsLoaded = false;
  programRights: ProgramRight[] = null;
  metaProgramRights: MetaProgramRight[] = null;
  ruleListRights: RuleListRight[] = null;

  constructor() {
    super(User.TYPENAME);
    this.entityName = User.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = User.entityName;
    this.firstName = source.firstName;
    this.intranetLogin = source.intranetLogin;
    this.extranetLogin = source.extranetLogin;
    this.email = source.email;
    this.address = source.address;
    this.phone = source.phone;
    this.organism = source.organism;
    this.center = source.center;
    this.site = source.site;
    this.ldapPresent = source.ldapPresent;
    this.department = Department.fromObject(source.department);
    this.privilegeIds = source.privilegeIds || [];
    this.trainings = source.trainings?.map(UserTraining.fromObject) || [];
  }

  asObject(options?: ReferentialAsObjectOptions): any {
    const target = super.asObject(options);
    target.department = this.department?.asObject(options);
    target.privilegeIds = this.privilegeIds || undefined;
    target.trainings = this.trainings?.map((value) => value.asObject(options));
    delete target.rightsLoaded;
    delete target.programRights;
    delete target.metaProgramRights;
    delete target.ruleListRights;
    return target;
  }
}
