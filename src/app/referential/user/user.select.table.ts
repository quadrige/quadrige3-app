import { UserTable } from '@app/referential/user/user.table';
import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { IRightSelectTable } from '@app/shared/table/table.model';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { isEmptyArray, isNotNilOrBlank } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { UserValidatorService } from '@app/referential/user/user.validator';
import { UserService, UserServiceWatchOptions } from '@app/referential/user/user.service';
import { UserFilter } from '@app/referential/user/filter/user.filter.model';
import { User } from '@app/referential/user/user.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { UserFilterForm } from '@app/referential/user/filter/user.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { UserTrainingTable } from '@app/referential/user/training/user-training.table';
import { RightFetchOptions } from '@app/referential/right/right.model';
import { ProgramRightTable } from '@app/referential/right/program/program-right.table';
import { MetaProgramRightTable } from '@app/referential/right/meta-program/meta-program-right.table';
import { RuleListRightTable } from '@app/referential/right/rule-list/rule-list-right.table';

@Component({
  selector: 'app-user-select-table',
  templateUrl: './user.table.html',
  standalone: true,
  imports: [ReferentialModule, UserFilterForm, GenericSelectTable, ProgramRightTable, MetaProgramRightTable, RuleListRightTable, UserTrainingTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserSelectTable extends UserTable implements IRightSelectTable<User> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria = {};
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();
  @Output() selectionChanged = new EventEmitter<any[]>();

  set rightOptions(value: RightFetchOptions) {
    this.rightFetchOptions = value;

    if (isNotNilOrBlank(this.rightFetchOptions?.entityName)) {
      // Apply specific fetch options
      this.dataSource.watchAllOptions = <UserServiceWatchOptions>{
        ...this.dataSource.watchAllOptions,
        options: this.rightFetchOptions,
        fetchPolicy: 'no-cache',
      };
      // Hide unwanted columns
      this.setShowColumn('label', false);
      this.setShowColumn('ldapPresent', false);
      this.setShowColumn('intranetLogin', false);
      this.setShowColumn('extranetLogin', false);
      this.setShowColumn('email', false);
      this.setShowColumn('address', false);
      this.setShowColumn('phone', false);
      this.setShowColumn('organism', false);
      this.setShowColumn('center', false);
      this.setShowColumn('site', false);
      this.setShowColumn('comments', false);
      this.setShowColumn('updateDate', false);
    }
  }

  constructor(
    protected injector: Injector,
    protected _entityService: UserService,
    protected validatorService: UserValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[user-select-table]';
    this.selectTable = true;
  }

  async setSelectedIds(values: any[]) {
    this.selectCriteria.includedIds = values;

    await this.ready();
    await this.resetFilter();
  }

  async resetFilter(filter?: UserFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit(<ISelectModalOptions>{ selectCriteria: this.selectCriteria });
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete).map((id) => id.toString());
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    this.selectionChanged.emit(this.selectCriteria.includedIds);
    await this.resetFilter();
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  getI18nColumnName(columnName: string): string {
    if (!!this.rightFetchOptions && columnName === 'creationDate') {
      return `${this.i18nColumnPrefix}RIGHT_CREATION_DATE`;
    }
    return super.getI18nColumnName(columnName);
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('ldapPresent', 'intranetLogin', 'extranetLogin', 'email');
  }
}
