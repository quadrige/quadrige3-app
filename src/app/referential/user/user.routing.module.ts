import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { UserTable } from '@app/referential/user/user.table';
import { UserModule } from '@app/referential/user/user.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: UserTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), UserModule],
})
export class UserRoutingModule {}
