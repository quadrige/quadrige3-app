import { Injectable, Injector } from '@angular/core';
import {
  BaseReferentialService,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
  ReferentialServiceLoadOptions,
  ReferentialServiceWatchOptions,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { User } from '@app/referential/user/user.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { jobFragments } from '@app/social/job/job.service';
import { UserFilter, UserFilterCriteria } from '@app/referential/user/filter/user.filter.model';
import { programRightFragment } from '@app/referential/right/program/program-right.service';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { ProgramRight } from '@app/referential/right/program/program-right.model';
import { isEmptyArray, Page } from '@sumaris-net/ngx-components';
import { metaProgramRightFragment } from '@app/referential/right/meta-program/meta-program-right.service';
import { MetaProgramRight } from '@app/referential/right/meta-program/meta-program-right.model';
import { ruleListRightFragment } from '@app/referential/right/rule-list/rule-list-right.service';
import { RuleListRight } from '@app/referential/right/rule-list/rule-list-right.model';
import { departmentLightFragment } from '@app/referential/department/department.service';
import { RightFetchOptions } from '@app/referential/right/right.model';

const userTrainingFragment = gql`
  fragment UserTrainingFragment on UserTrainingVO {
    userId
    training {
      ...ReferentialFragment
    }
    date
    comments
  }
  ${referentialFragments.light}
`;

const userFragment = gql`
  fragment UserFragment on UserVO {
    id
    label
    name
    firstName
    intranetLogin
    extranetLogin
    email
    address
    phone
    organism
    center
    site
    ldapPresent
    privilegeIds
    department {
      ...DepartmentLightFragment
    }
    trainings {
      ...UserTrainingFragment
    }
    pubkey
    hasAvatar
    avatar
    comments
    updateDate
    creationDate
    statusId
    __typename
  }
  ${departmentLightFragment}
  ${userTrainingFragment}
`;

const userLightFragment = gql`
  fragment UserLightFragment on UserVO {
    id
    label
    name
    firstName
    department {
      ...DepartmentLightFragment
    }
    creationDate
    statusId
    __typename
  }
  ${departmentLightFragment}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Users($page: PageInput, $filter: UserFilterVOInput, $options: RightFetchOptionsInput) {
      data: users(page: $page, filter: $filter, options: $options) {
        ...UserFragment
      }
    }
    ${userFragment}
  `,

  loadAllLight: gql`
    query UsersLight($page: PageInput, $filter: UserFilterVOInput, $options: RightFetchOptionsInput) {
      data: users(page: $page, filter: $filter, options: $options) {
        ...UserLightFragment
      }
    }
    ${userLightFragment}
  `,

  loadAllWithTotal: gql`
    query UsersWithTotal($page: PageInput, $filter: UserFilterVOInput, $options: RightFetchOptionsInput) {
      data: users(page: $page, filter: $filter, options: $options) {
        ...UserFragment
      }
      total: usersCount(filter: $filter)
    }
    ${userFragment}
  `,

  loadAllLightWithTotal: gql`
    query UsersLightWithTotal($page: PageInput, $filter: UserFilterVOInput, $options: RightFetchOptionsInput) {
      data: users(page: $page, filter: $filter, options: $options) {
        ...UserLightFragment
      }
      total: usersCount(filter: $filter)
    }
    ${userLightFragment}
  `,

  countAll: gql`
    query UsersCount($filter: UserFilterVOInput) {
      total: usersCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportUsersAsync($filter: UserFilterVOInput, $context: ExportContextInput) {
      data: exportUsersAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveUsers($data: [UserVOInput]) {
      data: saveUsers(users: $data) {
        ...UserFragment
      }
    }
    ${userFragment}
  `,

  deleteAll: gql`
    mutation DeleteUsers($ids: [Int]) {
      deleteUsers(ids: $ids)
    }
  `,
};

const otherQueries = {
  programRights: gql`
    query UsersProgramRights($ids: [Int]) {
      data: usersProgramRights(ids: $ids) {
        ...ProgramRightFragment
      }
    }
    ${programRightFragment}
  `,
  metaProgramRights: gql`
    query UsersMetaProgramRights($ids: [Int]) {
      data: usersMetaProgramRights(ids: $ids) {
        ...MetaProgramRightFragment
      }
    }
    ${metaProgramRightFragment}
  `,
  ruleListRights: gql`
    query UsersRuleListRights($ids: [Int]) {
      data: usersRuleListRights(ids: $ids) {
        ...RuleListRightFragment
      }
    }
    ${ruleListRightFragment}
  `,
};

export interface UserServiceWatchOptions extends ReferentialServiceWatchOptions {
  options: RightFetchOptions;
}

interface UserServiceLoadOptions extends ReferentialServiceLoadOptions {
  options: RightFetchOptions;
}

@Injectable({ providedIn: 'root' })
export class UserService extends BaseReferentialService<
  User,
  UserFilter,
  UserFilterCriteria,
  number,
  UserServiceWatchOptions,
  UserServiceLoadOptions
> {
  constructor(protected injector: Injector) {
    super(injector, User, UserFilter, { queries, mutations });
    this._logPrefix = '[user-service]';
  }

  protected buildWatchVariables(page: Partial<Page>, filter?: Partial<UserFilter>, opts?: UserServiceWatchOptions): any {
    return {
      ...super.buildWatchVariables(page, filter, opts),
      options: opts?.options,
    };
  }

  protected buildLoadVariables(page: Partial<Page>, filter?: Partial<UserFilter>, opts?: UserServiceLoadOptions): any {
    return {
      ...super.buildLoadVariables(page, filter, opts),
      options: opts?.options,
    };
  }

  async getProgramRights(userIds: number[]): Promise<ProgramRight[]> {
    if (isEmptyArray(userIds)) return [];
    const variables: any = { ids: userIds };
    if (this._debug) console.debug(`${this._logPrefix} Loading program rights...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[] }>({
      query: otherQueries.programRights,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(ProgramRight.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} Program rights loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async getMetaProgramRights(userIds: number[]): Promise<MetaProgramRight[]> {
    if (isEmptyArray(userIds)) return [];
    const variables: any = { ids: userIds };
    if (this._debug) console.debug(`${this._logPrefix} Loading meta-program rights...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[] }>({
      query: otherQueries.metaProgramRights,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(MetaProgramRight.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} Meta-program rights loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async getRuleListRights(userIds: number[]): Promise<RuleListRight[]> {
    if (isEmptyArray(userIds)) return [];
    const variables: any = { ids: userIds };
    if (this._debug) console.debug(`${this._logPrefix} Loading rule list rights...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[] }>({
      query: otherQueries.ruleListRights,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(RuleListRight.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} Rule list rights loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
