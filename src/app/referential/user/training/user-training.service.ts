import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { UserTraining } from '@app/referential/user/training/user-training.model';
import { UserTrainingFilter } from '@app/referential/user/training/filter/user-training.filter.model';

@Injectable({ providedIn: 'root' })
export class UserTrainingService extends EntitiesMemoryService<UserTraining, UserTrainingFilter, string> {
  constructor() {
    super(UserTraining, UserTrainingFilter);
  }
}
