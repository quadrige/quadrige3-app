import { Entity, EntityAsObjectOptions, EntityClass, fromDateISOString } from '@sumaris-net/ngx-components';
import { IntReferential } from '@app/referential/model/referential.model';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';

@EntityClass({ typename: 'UserTrainingVO' })
export class UserTraining extends Entity<UserTraining, string> {
  static fromObject: (source: any, opts?: any) => UserTraining;

  userId: number = null;
  training: IntReferential = null;
  date: Moment = null;
  comments: string = null;

  constructor() {
    super(UserTraining.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.userId = source.userId;
    this.training = IntReferential.fromObject(source.training);
    this.date = fromDateISOString(source.date);
    this.comments = source.comments;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.userId = this.userId;
    target.training = this.training.asObject(opts);
    target.date = Dates.toLocalDateString(this.date);
    return target;
  }
}
