import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { UserTraining } from '@app/referential/user/training/user-training.model';
import { UserTrainingValidatorService } from '@app/referential/user/training/user-training.validator';
import { UserTrainingFilter, UserTrainingFilterCriteria } from '@app/referential/user/training/filter/user-training.filter.model';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { UserTrainingService } from '@app/referential/user/training/user-training.service';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { UserTrainingFilterForm } from '@app/referential/user/training/filter/user-training.filter.form';

@Component({
  selector: 'app-user-training-table',
  templateUrl: './user-training.table.html',
  standalone: true,
  imports: [ReferentialModule, UserTrainingFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserTrainingTable
  extends BaseMemoryTable<UserTraining, string, UserTrainingFilter, UserTrainingFilterCriteria, UserTrainingValidatorService>
  implements OnInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: UserTrainingService,
    protected validatorService: UserTrainingValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['training', 'date', 'comments']).concat(RESERVED_END_COLUMNS),
      UserTraining,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.USER_TRAINING.';
    this.defaultSortBy = 'date';
    this.logPrefix = '[user-training-table]';
  }

  ngOnInit() {
    this.subTable = true;

    super.ngOnInit();

    // training combo
    this.registerAutocompleteField('training', {
      ...this.referentialOptions.training,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Training',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('training', 'date');
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }
}
