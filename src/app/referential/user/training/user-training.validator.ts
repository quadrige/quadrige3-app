import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators } from '@sumaris-net/ngx-components';
import { UserTraining } from '@app/referential/user/training/user-training.model';
import { UserTrainingService } from '@app/referential/user/training/user-training.service';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { ReferentialGroupAsyncValidators, ReferentialValidatorOptions } from '@app/referential/service/referential-validator.service';

@Injectable({ providedIn: 'root' })
export class UserTrainingValidatorService extends BaseValidatorService<UserTraining, ReferentialValidatorOptions> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: UserTrainingService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: UserTraining, opts?: any): { [key: string]: any } {
    return {
      id: [data?.id || null],
      training: [data?.training || null, Validators.compose([Validators.required, SharedValidators.entity])],
      date: [data?.date || null, Validators.required],
      comments: [data?.comments || null],
    };
  }

  getFormGroupOptions(data?: UserTraining, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      asyncValidators: ReferentialGroupAsyncValidators.checkAttributesAlreadyExists(
        undefined,
        this.dataSource,
        opts,
        data,
        ['training?.id', 'date'],
        'REFERENTIAL.USER.ERROR.TRAINING_ALREADY_EXISTS'
      ),
    };
  }
}
