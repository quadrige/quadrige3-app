import { EntityAsObjectOptions, FilterFn } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { BaseEntityFilter, BaseEntityFilterCriteria, BaseFilterUtils } from '@app/shared/model/filter.model';
import { UserTraining } from '@app/referential/user/training/user-training.model';
import { DateFilter } from '@app/shared/model/date-filter';
import { Dates } from '@app/shared/dates';

export class UserTrainingFilterCriteria extends BaseEntityFilterCriteria<UserTraining, string> {
  trainingFilter: IntReferentialFilterCriteria = null;
  dateFilter: DateFilter = null;

  static fromObject(source: any, opts?: any): UserTrainingFilterCriteria {
    const target = new UserTrainingFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.trainingFilter = IntReferentialFilterCriteria.fromObject(source.trainingFilter || {});
    this.dateFilter = DateFilter.fromObject(source.dateFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.trainingFilter = this.trainingFilter?.asObject(opts);
    target.dateFilter = this.dateFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'trainingFilter') return !BaseFilterUtils.isCriteriaEmpty(this.trainingFilter, true);
    if (key === 'dateFilter') return !this.dateFilter.isEmpty();
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  protected buildFilter(): FilterFn<UserTraining>[] {
    const target = super.buildFilter();

    if (this.trainingFilter) {
      const filter = this.trainingFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.training));
    }
    if (this.dateFilter && !this.dateFilter?.isEmpty()) {
      target.push((data) => Dates.isBetween(data.date, { start: this.dateFilter.startLowerBound, end: this.dateFilter.startUpperBound }));
    }

    return target;
  }
}

export class UserTrainingFilter extends BaseEntityFilter<UserTrainingFilter, UserTrainingFilterCriteria, UserTraining, string> {
  static fromObject(source: any, opts?: any): UserTrainingFilter {
    const target = new UserTrainingFilter();
    target.fromObject(source, opts);
    return target;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  criteriaFromObject(source: any, opts: any): UserTrainingFilterCriteria {
    return UserTrainingFilterCriteria.fromObject(source, opts);
  }
}
