import { ChangeDetectionStrategy, Component, QueryList, ViewChildren } from '@angular/core';
import { UserTrainingFilter, UserTrainingFilterCriteria } from '@app/referential/user/training/filter/user-training.filter.model';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';
import { ReferentialModule } from '@app/referential/referential.module';
import { DateRangeFilterFormField } from '@app/shared/component/filter/date-range-filter-form-field.component';

@Component({
  selector: 'app-user-training-filter-form',
  templateUrl: './user-training.filter.form.html',
  standalone: true,
  imports: [ReferentialModule, DateRangeFilterFormField],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserTrainingFilterForm extends BaseCriteriaFormComponent<UserTrainingFilter, UserTrainingFilterCriteria> {
  @ViewChildren(ReferentialFilterFormField) referentialFilterFormFields: QueryList<ReferentialFilterFormField>;

  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected panelOpened() {
    super.panelOpened();
    this.referentialFilterFormFields?.forEach((item) => item.onBlur());
  }
}
