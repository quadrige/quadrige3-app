import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { User } from '@app/referential/user/user.model';
import { UserLdapService } from '@app/referential/user/ldap/user-ldap.service';
import { UserFilter, UserFilterCriteria } from '@app/referential/user/filter/user.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-user-ldap-table',
  templateUrl: './user-ldap.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserLdapTable extends ReferentialTable<User, number, UserFilter, UserFilterCriteria> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: UserLdapService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'label',
        'name',
        'firstName',
        'department',
        'intranetLogin',
        'extranetLogin',
        'email',
        'phone',
        'organism',
        'center',
        'site',
      ]).concat(RESERVED_END_COLUMNS),
      User,
      _entityService,
      undefined
    );

    this.i18nColumnPrefix = 'REFERENTIAL.USER.';
    this.logPrefix = '[user-ldap-table]';
    this.defaultSortBy = 'name';
  }

  ngOnInit() {
    // default properties
    this.subTable = true;
    // change toolbar color
    this.toolbarColor = 'secondary900';

    this.inlineEdition = false;
    this.checkBoxSelection = true;
    this.toggleOnlySelection = true;
    this.permanentSelectionAllowed = true;

    super.ngOnInit();

    setTimeout(() => {
      this.filterFormComponent()?.open();
    }, 300); // not less than 300ms !
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('phone', 'organism', 'center', 'site');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name', 'firstName', 'department');
  }
}
