import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { UserFilter, UserFilterCriteria } from '@app/referential/user/filter/user.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-user-ldap-filter-form',
  templateUrl: './user-ldap.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserLdapFilterForm extends ReferentialCriteriaFormComponent<UserFilter, UserFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.name.concat('firstName').concat(attributes.label);
  }
}
