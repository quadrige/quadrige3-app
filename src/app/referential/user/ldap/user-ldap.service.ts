import { Injectable, Injector } from '@angular/core';
import { BaseEntityGraphqlQueries, LoadResult, Page } from '@sumaris-net/ngx-components';
import { BaseReferentialService, ReferentialServiceWatchOptions } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { User } from '@app/referential/user/user.model';
import { Observable } from 'rxjs';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { UserFilter, UserFilterCriteria } from '@app/referential/user/filter/user.filter.model';
import { departmentLightFragment } from '@app/referential/department/department.service';

const fragments = {
  ldapUser: gql`
    fragment LdapUserFragment on UserVO {
      id
      label
      name
      firstName
      intranetLogin
      extranetLogin
      email
      phone
      organism
      center
      site
      department {
        ...DepartmentLightFragment
      }
      __typename
    }
    ${departmentLightFragment}
  `,
};

const queries: BaseEntityGraphqlQueries = {
  load: gql`
    query LdapUser($id: String) {
      data: ldapUser(id: $id) {
        ...LdapUserFragment
      }
    }
    ${fragments.ldapUser}
  `,

  loadAll: gql`
    query LdapUsers($sortBy: String, $sortDirection: String, $filter: UserFilterVOInput) {
      data: ldapUsers(sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter) {
        ...LdapUserFragment
      }
    }
    ${fragments.ldapUser}
  `,

  loadAllWithTotal: undefined,
  countAll: undefined,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: undefined,
  deleteAll: undefined,
};

@Injectable({ providedIn: 'root' })
export class UserLdapService extends BaseReferentialService<User, UserFilter, UserFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, User, UserFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[user-ldap-service]';
  }

  async loadByLabel(label: string): Promise<User> {
    const now = Date.now();
    if (this._debug) console.debug(`[referential-service] Loading ${this._logTypeName} {${label}}...`);
    const query = this.queries.load;

    const { data } = await this.graphql.query<{ data: any }>({
      query,
      variables: {
        id: label,
      },
      fetchPolicy: 'no-cache',
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
    });
    if (this._debug) console.debug(`[referential-service] ${this._logTypeName} item loaded in ${Date.now() - now}ms`, data);

    // Convert to entity
    return data && this.fromObject(data);
  }

  watchPage(page: Partial<Page>, filter?: UserFilter, opts?: ReferentialServiceWatchOptions): Observable<LoadResult<User>> {
    return super.watchPage(page, filter, { ...opts, withTotal: false });
  }

  async save(entity: User, opts?: EntitySaveOptions): Promise<User> {
    throw new Error(`forbidden`);
  }

  async saveAll(entities: User[], opts?: EntitySaveOptions): Promise<User[]> {
    throw new Error(`forbidden`);
  }

  async delete(entity: User, opts?: any): Promise<any> {
    throw new Error(`forbidden`);
  }

  async deleteAll(entities: User[], opts?: any): Promise<any> {
    throw new Error(`forbidden`);
  }
}
