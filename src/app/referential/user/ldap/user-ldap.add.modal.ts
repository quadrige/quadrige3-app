import { ChangeDetectionStrategy, Component, Injector, ViewChild } from '@angular/core';
import { UserLdapTable } from '@app/referential/user/ldap/user-ldap.table';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IAddResult } from '@app/shared/model/options.model';

@Component({
  selector: 'app-user-ldap-add-modal',
  templateUrl: './user-ldap.add.modal.html',
  styleUrls: ['./user-ldap.add.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserLdapAddModal extends ModalComponent<IAddResult> {
  @ViewChild('addTable', { static: true }) addTable: UserLdapTable;

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled() {
    return isEmptyArray(this.addTable.selectedEntities);
  }

  protected afterInit() {}

  protected dataToValidate(): Promise<IAddResult> | IAddResult {
    return { added: this.addTable.selectedEntities };
  }

  protected onError(e: any) {
    super.onError(e);
    this.addTable.markAsError(e?.message || e);
  }
}
