import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { UserFilter, UserFilterCriteria } from '@app/referential/user/filter/user.filter.model';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-user-filter-form',
  templateUrl: './user.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserFilterForm extends ReferentialCriteriaFormComponent<UserFilter, UserFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName.concat('firstName').concat(attributes.label);
  }

  protected criteriaToQueryParams(criteria: UserFilterCriteria): PartialRecord<keyof UserFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.ldapPresent)) params.ldapPresent = criteria.ldapPresent;
    this.subCriteriaToQueryParams(params, criteria, 'departmentFilter');
    this.subCriteriaToQueryParams(params, criteria, 'privilegeFilter');
    return params;
  }
}
