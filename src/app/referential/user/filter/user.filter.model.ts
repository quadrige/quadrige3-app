import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { User } from '@app/referential/user/user.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'UserFilterCriteriaVO' })
export class UserFilterCriteria extends ReferentialFilterCriteria<User, number> {
  static fromObject: (source: any, opts?: any) => UserFilterCriteria;

  ldapPresent: boolean = null;
  departmentFilter: IntReferentialFilterCriteria = null;
  privilegeFilter: StrReferentialFilterCriteria = null;

  constructor() {
    super(UserFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.ldapPresent = source.ldapPresent;
    this.departmentFilter = IntReferentialFilterCriteria.fromObject(source.departmentFilter || {});
    this.privilegeFilter = StrReferentialFilterCriteria.fromObject(source.privilegeFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.searchAttributes = target.searchAttributes || ['id', 'name', 'firstName', 'label'];
    target.departmentFilter = this.departmentFilter?.asObject(opts);
    target.privilegeFilter = this.privilegeFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'departmentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.departmentFilter, true);
    if (key === 'privilegeFilter') return !BaseFilterUtils.isCriteriaEmpty(this.privilegeFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'UserFilterVO' })
export class UserFilter extends ReferentialFilter<UserFilter, UserFilterCriteria, User> {
  static fromObject: (source: any, opts?: any) => UserFilter;

  constructor() {
    super(UserFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): UserFilterCriteria {
    return UserFilterCriteria.fromObject(source, opts);
  }
}
