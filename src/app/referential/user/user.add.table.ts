import { UserTable } from '@app/referential/user/user.table';
import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { IAddTable } from '@app/shared/table/table.model';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { UserValidatorService } from '@app/referential/user/user.validator';
import { UserService } from '@app/referential/user/user.service';
import { UserFilter } from '@app/referential/user/filter/user.filter.model';
import { User } from '@app/referential/user/user.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { UserFilterForm } from '@app/referential/user/filter/user.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { UserTrainingTable } from '@app/referential/user/training/user-training.table';
import { ProgramRightTable } from '@app/referential/right/program/program-right.table';
import { MetaProgramRightTable } from '@app/referential/right/meta-program/meta-program-right.table';
import { RuleListRightTable } from '@app/referential/right/rule-list/rule-list-right.table';

@Component({
  selector: 'app-user-add-table',
  templateUrl: './user.table.html',
  standalone: true,
  imports: [ReferentialModule, UserFilterForm, GenericSelectTable, ProgramRightTable, MetaProgramRightTable, RuleListRightTable, UserTrainingTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserAddTable extends UserTable implements IAddTable<User> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: UserService,
    protected validatorService: UserValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[user-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: UserFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('ldapPresent', 'intranetLogin', 'extranetLogin', 'email');
  }
}
