import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { UserTable } from '@app/referential/user/user.table';
import { UserLdapTable } from '@app/referential/user/ldap/user-ldap.table';
import { UserLdapAddModal } from '@app/referential/user/ldap/user-ldap.add.modal';
import { UserFilterForm } from '@app/referential/user/filter/user.filter.form';
import { UserLdapFilterForm } from '@app/referential/user/ldap/filter/user-ldap.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { UserTrainingTable } from '@app/referential/user/training/user-training.table';
import { UserTrainingFilterForm } from '@app/referential/user/training/filter/user-training.filter.form';
import { ProgramRightTable } from '@app/referential/right/program/program-right.table';
import { MetaProgramRightTable } from '@app/referential/right/meta-program/meta-program-right.table';
import { RuleListRightTable } from '@app/referential/right/rule-list/rule-list-right.table';

@NgModule({
  imports: [
    ReferentialModule,
    UserFilterForm,
    GenericSelectTable,
    UserTrainingTable,
    UserTrainingFilterForm,
    ProgramRightTable,
    MetaProgramRightTable,
    RuleListRightTable,
  ],
  declarations: [UserTable, UserLdapTable, UserLdapFilterForm, UserLdapAddModal],
  exports: [UserTable],
})
export class UserModule {}
