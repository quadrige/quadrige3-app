import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { TrainingTable } from '@app/referential/training/training.table';
import { TrainingFilterForm } from '@app/referential/training/filter/training.filter.form';

@NgModule({
  imports: [ReferentialModule, TrainingFilterForm],
  declarations: [TrainingTable],
  exports: [TrainingTable],
})
export class TrainingModule {}
