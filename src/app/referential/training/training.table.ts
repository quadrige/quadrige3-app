import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { Training } from '@app/referential/training/training.model';
import { TrainingValidatorService } from '@app/referential/training/training.validator';
import { TrainingService } from '@app/referential/training/training.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';

@Component({
  selector: 'app-training-table',
  templateUrl: './training.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingTable
  extends ReferentialTable<Training, number, IntReferentialFilter, IntReferentialFilterCriteria, TrainingValidatorService>
  implements OnInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: TrainingService,
    protected validatorService: TrainingValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['label', 'name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      Training,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.TRAINING.';
    this.defaultSortBy = 'label';
    this.logPrefix = '[training-table]';
  }

  ngOnInit() {
    super.ngOnInit();
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.TRAININGS' });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name');
  }
}
