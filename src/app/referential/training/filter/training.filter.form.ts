import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { ReferentialModule } from '@app/referential/referential.module';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-training-filter-form',
  templateUrl: './training.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingFilterForm extends ReferentialCriteriaFormComponent<IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabelName;
  }
}
