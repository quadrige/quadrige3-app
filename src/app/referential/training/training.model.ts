import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'TrainingVO' })
export class Training extends Referential<Training> {
  static entityName = 'Training';
  static fromObject: (source: any, opts?: any) => Training;

  constructor() {
    super(Training.TYPENAME);
    this.entityName = Training.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Training.entityName;
  }
}
