import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { Training } from '@app/referential/training/training.model';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';

const trainingFragment = gql`
  fragment TrainingFragment on TrainingVO {
    id
    label
    name
    description
    comments
    statusId
    creationDate
    updateDate
    __typename
  }
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Trainings($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: trainings(page: $page, filter: $filter) {
        ...TrainingFragment
      }
    }
    ${trainingFragment}
  `,

  loadAllWithTotal: gql`
    query TrainingsWithTotal($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: trainings(page: $page, filter: $filter) {
        ...TrainingFragment
      }
      total: trainingsCount(filter: $filter)
    }
    ${trainingFragment}
  `,

  countAll: gql`
    query TrainingsCount($filter: IntReferentialFilterVOInput) {
      total: trainingsCount(filter: $filter)
    }
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveTrainings($data: [TrainingVOInput]) {
      data: saveTrainings(trainings: $data) {
        ...TrainingFragment
      }
    }
    ${trainingFragment}
  `,

  deleteAll: gql`
    mutation DeleteTrainings($ids: [Int]) {
      deleteTrainings(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class TrainingService extends BaseReferentialService<Training, IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Training, IntReferentialFilter, { queries, mutations });
    this._logPrefix = '[training-service]';
  }
}
