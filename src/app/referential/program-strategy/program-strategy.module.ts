import { NgModule } from '@angular/core';
import { ProgramModule } from '@app/referential/program-strategy/program/program.module';
import { StrategyModule } from '@app/referential/program-strategy/strategy/strategy.module';

@NgModule({
  imports: [ProgramModule, StrategyModule],
})
export class ProgramStrategyModule {}
