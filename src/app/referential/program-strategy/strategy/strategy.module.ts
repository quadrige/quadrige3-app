import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { StrategyTable } from '@app/referential/program-strategy/strategy/strategy.table';
import { AppliedStrategyTable } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.table';
import { PmfmuStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.table';
import { PmfmuAppliedStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.table';
import { AppliedStrategyAddModal } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.add.modal';
import { PmfmuAppliedStrategyAddModal } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.add.modal';
import { PmfmuStrategyMultiEditModal } from '@app/referential/program-strategy/strategy/pmfmu-strategy/multi-edit/pmfmu-strategy.multi-edit.modal';
import { AppliedStrategyMultiEditModal } from '@app/referential/program-strategy/strategy/applied-strategy/multi-edit/applied-strategy.multi-edit.modal';
import { AppliedPeriodMultiEditModal } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.modal';
import { AppliedPeriodMultiEditTable } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.table';
import { PmfmuAppliedStrategyMultiEditModal } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/multi-edit/pmfmu-applied-strategy.multi-edit.modal';
import { StrategyDuplicateModal } from '@app/referential/program-strategy/strategy/duplicate/strategy.duplicate.modal';
import { StrategyHistoryModal } from '@app/referential/program-strategy/strategy/history/strategy-history.modal';
import { StrategyFilterForm } from '@app/referential/program-strategy/strategy/filter/strategy.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { ProgramAddTable } from '@app/referential/program-strategy/program/program.add.table';
import { AppliedStrategyAddTable } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.add.table';
import { AppliedPeriodTable } from '@app/referential/program-strategy/strategy/applied-period/applied-period.table';
import { PmfmuStrategyAddTable } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.add.table';
import { MonitoringLocationAddTable } from '@app/referential/monitoring-location/monitoring-location.add.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { StrategyHistoryFilterForm } from '@app/referential/program-strategy/strategy/history/filter/strategy-history.filter.form';
import { StrategyHistoryTable } from '@app/referential/program-strategy/strategy/history/strategy-history.table';

@NgModule({
  imports: [
    ReferentialModule,
    StrategyFilterForm,
    AppliedStrategyTable,
    PmfmuStrategyTable,
    PmfmuAppliedStrategyTable,
    GenericSelectTable,
    ProgramAddTable,
    AppliedStrategyAddTable,
    AppliedPeriodTable,
    PmfmuStrategyAddTable,
    MonitoringLocationAddTable,
    DepartmentSelectTable,
    UserSelectTable,
    StrategyHistoryFilterForm,
    StrategyHistoryTable,
  ],
  declarations: [
    StrategyTable,
    StrategyDuplicateModal,
    StrategyHistoryModal,
    AppliedStrategyAddModal,
    AppliedStrategyMultiEditModal,
    AppliedPeriodMultiEditModal,
    AppliedPeriodMultiEditTable,
    PmfmuStrategyMultiEditModal,
    PmfmuAppliedStrategyAddModal,
    PmfmuAppliedStrategyMultiEditModal,
  ],
  exports: [StrategyTable],
})
export class StrategyModule {}
