import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import {
  lengthComment,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { toBoolean } from '@sumaris-net/ngx-components';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class StrategyValidatorService extends ReferentialValidatorService<Strategy> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: StrategyService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Strategy, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      programId: [data?.programId || null, Validators.required],
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.compose([Validators.required, Validators.maxLength(lengthComment)])],
      responsibleDepartmentIds: [data?.responsibleDepartmentIds || null],
      responsibleUserIds: [data?.responsibleUserIds || null],
      appliedStrategies: [data?.appliedStrategies || null],
      appliedStrategiesLoaded: [toBoolean(data?.appliedStrategiesLoaded, false)],
      pmfmuStrategies: [data?.pmfmuStrategies || null],
      pmfmuStrategiesLoaded: [toBoolean(data?.pmfmuStrategiesLoaded, false)],
      pmfmuAppliedStrategies: [data?.pmfmuAppliedStrategies || null],
      pmfmuAppliedStrategiesLoaded: [toBoolean(data?.pmfmuAppliedStrategiesLoaded, false)],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
      mostRecent: [data?.mostRecent || false],
    };
  }

  getFormGroupOptions(data?: Strategy, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredOneArrayNotEmpty(
          ['responsibleDepartmentIds', 'responsibleUserIds'],
          'REFERENTIAL.STRATEGY.ERROR.MISSING_MANAGER'
        ),
      ],
    };
  }
}
