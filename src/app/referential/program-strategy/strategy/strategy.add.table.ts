import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { StrategyTable } from '@app/referential/program-strategy/strategy/strategy.table';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { StrategyValidatorService } from '@app/referential/program-strategy/strategy/strategy.validator';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { StrategyFilterForm } from '@app/referential/program-strategy/strategy/filter/strategy.filter.form';
import { AppliedStrategyTable } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.table';
import { PmfmuStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.table';
import { PmfmuAppliedStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';

@Component({
  selector: 'app-strategy-add-table',
  templateUrl: './strategy.table.html',
  standalone: true,
  imports: [
    ReferentialModule,
    StrategyFilterForm,
    AppliedStrategyTable,
    PmfmuStrategyTable,
    PmfmuAppliedStrategyTable,
    DepartmentSelectTable,
    UserSelectTable,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyAddTable extends StrategyTable implements IAddTable<Strategy> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: StrategyService,
    protected validatorService: StrategyValidatorService,
    protected programService: ProgramService
  ) {
    super(injector, _entityService, validatorService, programService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[strategy-add-table]';
    this.defaultBackHref = undefined;
    this.addTable = true;
  }

  async resetFilter(filter?: StrategyFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  updatePermission() {
    // Don't update permissions
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
