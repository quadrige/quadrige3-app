import { Injectable } from '@angular/core';
import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { AppliedPeriodComposite } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.model';

@Injectable({ providedIn: 'root' })
export class AppliedPeriodCompositeService extends UnfilteredEntitiesMemoryService<AppliedPeriodComposite, string> {
  constructor() {
    super(AppliedPeriodComposite);
  }
}
