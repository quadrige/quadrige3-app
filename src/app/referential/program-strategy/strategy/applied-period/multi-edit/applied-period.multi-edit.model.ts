import { Moment } from 'moment';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { IntReferential } from '@app/referential/model/referential.model';

export class AppliedPeriodComposite extends AppliedPeriod {
  monitoringLocation: IntReferential;

  constructor(appliedStrategyId?: number, monitoringLocation?: IntReferential, startDate?: Moment, endDate?: Moment) {
    super(appliedStrategyId, startDate, endDate);
    this.monitoringLocation = monitoringLocation;
  }

  clone(opts?: any): AppliedPeriodComposite {
    return this;
  }
}
