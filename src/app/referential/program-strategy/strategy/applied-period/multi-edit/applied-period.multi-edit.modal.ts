import { ChangeDetectionStrategy, Component, EventEmitter, inject, Injector, Input, Output, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AppliedPeriodMultiEditTable } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.table';
import { AppliedPeriodComposite } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.model';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { emitPromiseEvent, fromDateISOString, isEmptyArray, PromiseEvent, SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { CheckOverlappingPeriodsOptions, CheckPeriodsToDeleteOptions } from '@app/referential/program-strategy/strategy/strategy.table';
import { Alerts } from '@app/shared/alerts';
import { asyncEvery, asyncSome } from '@app/shared/utils';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { Dates } from '@app/shared/dates';
import { DateFilter } from '@app/shared/model/date-filter';
import { ObjectMultiMap, splitToMultiMap } from '@app/shared/multimap';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IAppliedPeriodMultiEditModalOptions extends IModalOptions {
  i18nPrefix: string;
  appliedStrategies: AppliedStrategy[];
  checkOverlappingPeriodsEventEmitter: EventEmitter<any>;
  checkPeriodsToDeleteEventEmitter: EventEmitter<any>;
}

@Component({
  selector: 'app-applied-period-multi-edit-modal',
  templateUrl: './applied-period.multi-edit.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppliedPeriodMultiEditModal extends ModalComponent<AppliedPeriodComposite[]> implements IAppliedPeriodMultiEditModalOptions {
  @ViewChild('multiEditTable') multiEditTable: AppliedPeriodMultiEditTable;

  @Input() i18nPrefix: string;

  @Output() checkOverlappingPeriodsEventEmitter = new EventEmitter<PromiseEvent<boolean, CheckOverlappingPeriodsOptions>>();
  @Output() checkPeriodsToDeleteEventEmitter = new EventEmitter<PromiseEvent<boolean, CheckPeriodsToDeleteOptions>>();

  private readonly alertCtrl = inject(AlertController);
  protected periodChanged = false;
  private _appliedStrategies: AppliedStrategy[];
  private _selectedRows: AsyncTableElement<AppliedPeriodComposite>[];

  constructor(protected injector: Injector) {
    super(injector);
    this.setForm(
      this.formBuilder.group(
        {
          startDate: [null],
          endDate: [null],
        },
        {
          validators: [SharedFormGroupValidators.dateRange('startDate', 'endDate')],
        }
      )
    );
  }

  get disabled() {
    return !this.periodChanged;
  }

  get canAddPeriod(): boolean {
    return !!this.form.value.startDate && !!this.form.value.endDate;
  }

  get canEditPeriod(): boolean {
    return !!this.form.value.startDate || !!this.form.value.endDate;
  }

  @Input() set appliedStrategies(value: AppliedStrategy[]) {
    this._appliedStrategies = value?.map((_appliedStrategy) => AppliedStrategy.fromObject(_appliedStrategy).clone({ minify: false }));
  }

  protected afterInit(): Promise<void> | void {
    this.registerSubscription(
      this.multiEditTable.selection.changed.subscribe(() => {
        if (this.multiEditTable.selection.isEmpty()) {
          this.form.disable();
        } else {
          this.form.enable();
        }
      })
    );
    this.buildComposite();
  }

  async deletePeriod(rowsToDelete: AsyncTableElement<AppliedPeriodComposite>[]) {
    if (isEmptyArray(rowsToDelete)) {
      return;
    }

    // Split composites to delete by monitoring location id
    const rowsToDeleteByMonitoringLocationId: ObjectMultiMap<AppliedPeriodComposite> = splitToMultiMap(
      rowsToDelete.map((row) => row.currentData),
      (value) => value.monitoringLocation.id
    );

    // Split all composites by monitoring location id
    const allCompositesByMonitoringLocationId: ObjectMultiMap<AppliedPeriodComposite> = splitToMultiMap(
      this.multiEditTable.value,
      (value) => value.monitoringLocation.id
    );

    // Check if last period of location
    const lastPeriod = Object.keys(rowsToDeleteByMonitoringLocationId).some(
      (monitoringLocationId) =>
        rowsToDeleteByMonitoringLocationId[monitoringLocationId].length === allCompositesByMonitoringLocationId[monitoringLocationId].length
    );
    if (lastPeriod) {
      await Alerts.showError('REFERENTIAL.APPLIED_PERIOD.MULTI_EDIT.ERROR.LAST_PERIOD', this.alertCtrl, this.translate);
      return;
    }

    // Check if the periods can be deleted
    if (!this.checkPeriodsToDeleteEventEmitter || !this.checkPeriodsToDeleteEventEmitter.observed) {
      console.error(`[applied-period-multi-edit] Unable to check periods to delete. Aborting`);
      return;
    }

    const deletePossible = await asyncEvery(rowsToDelete, async (rowToDelete: AsyncTableElement<AppliedPeriodComposite>) => {
      return emitPromiseEvent(this.checkPeriodsToDeleteEventEmitter, 'checkPeriodsToDelete', {
        detail: {
          rows: [rowToDelete],
          monitoringLocationId: rowToDelete.currentData.monitoringLocation.id,
        },
      });
    });

    if (deletePossible) {
      rowsToDelete.forEach((rowToDelete) => {
        // Find applied strategy by monitoring location id
        const appliedStrategyToUpdate = this._appliedStrategies.find(
          (appliedStrategy) => appliedStrategy.monitoringLocation.id.toString() === rowToDelete.currentData.monitoringLocation.id.toString()
        );
        if (appliedStrategyToUpdate) {
          appliedStrategyToUpdate.appliedPeriods = appliedStrategyToUpdate.appliedPeriods.filter(
            (appliedPeriod) => !appliedPeriod.dateEquals(rowToDelete.currentData)
          );
        } else {
          console.error(`[applied-period-multi-edit] Unable to find applied strategy to update. Skipping`);
        }
      });
      // Reset
      this.buildComposite();
      this.markAsDirty();
    }
  }

  async validAdd(event: UIEvent) {
    event?.stopPropagation();

    // Get selected rows
    this._selectedRows = this.filterUniqueMonitoringLocation(this.multiEditTable.selection.selected);

    // First check if there is no overlapping period
    if (!(await this.checkOverlappingPeriods())) {
      await Alerts.showError('REFERENTIAL.APPLIED_PERIOD.MULTI_EDIT.ERROR.OVERLAPPING_PERIODS', this.alertCtrl, this.translate);
      return;
    }

    this._selectedRows.forEach((selectedRow) => {
      // Find applied strategy by monitoring location id
      const appliedStrategyToUpdate = this._appliedStrategies.find(
        (appliedStrategy) => appliedStrategy.monitoringLocation.id.toString() === selectedRow.currentData.monitoringLocation.id.toString()
      );
      if (appliedStrategyToUpdate) {
        // Add this applied period
        appliedStrategyToUpdate.appliedPeriods.push(
          new AppliedPeriod(appliedStrategyToUpdate.id, fromDateISOString(this.form.value.startDate), fromDateISOString(this.form.value.endDate))
        );
        this.periodChanged = true;
      } else {
        console.error(`[applied-period-multi-edit] Unable to find applied strategy to update. Skipping`);
      }
    });

    // Build table
    this.buildComposite();
    this.markAsDirty();
  }

  async validEdit(event: UIEvent) {
    event?.stopPropagation();
    this._selectedRows = this.multiEditTable.selection.selected;

    // First check if there is no overlapping period
    if (!(await this.checkOverlappingPeriods())) {
      await Alerts.showError('REFERENTIAL.APPLIED_PERIOD.MULTI_EDIT.ERROR.OVERLAPPING_PERIODS', this.alertCtrl, this.translate);
      return;
    }

    if (!this.checkValidDates()) {
      await Alerts.showError('REFERENTIAL.APPLIED_PERIOD.MULTI_EDIT.ERROR.INVALID_PERIOD', this.alertCtrl, this.translate);
      return;
    }
    this._selectedRows.forEach((selectedRow) => {
      // Find applied strategy by monitoring location id
      const appliedStrategyToUpdate = this._appliedStrategies.find(
        (appliedStrategy) => appliedStrategy.monitoringLocation.id.toString() === selectedRow.currentData.monitoringLocation.id.toString()
      );
      if (appliedStrategyToUpdate) {
        // Find applied period to update
        const appliedPeriodToUpdate = appliedStrategyToUpdate.appliedPeriods.find((appliedPeriod) =>
          appliedPeriod.dateEquals(selectedRow.currentData)
        );
        if (appliedPeriodToUpdate) {
          appliedPeriodToUpdate.startDate = fromDateISOString(this.form.value.startDate) || appliedPeriodToUpdate.startDate;
          appliedPeriodToUpdate.endDate = fromDateISOString(this.form.value.endDate) || appliedPeriodToUpdate.endDate;
          this.periodChanged = true;
        } else {
          console.error(`[applied-period-multi-edit] Unable to find applied period to update. Skipping`);
        }
      } else {
        console.error(`[applied-period-multi-edit] Unable to find applied strategy to update. Skipping`);
      }
    });

    // Build table
    this.buildComposite();
    this.markAsDirty();
  }

  protected dataToValidate(): Promise<AppliedPeriodComposite[]> | AppliedPeriodComposite[] {
    return this.multiEditTable.value;
  }

  private buildComposite() {
    const data: AppliedPeriodComposite[] = [];
    this._appliedStrategies?.forEach((appliedStrategy) =>
      appliedStrategy.appliedPeriods.forEach((appliedPeriod) =>
        data.push(new AppliedPeriodComposite(appliedStrategy.id, appliedStrategy.monitoringLocation, appliedPeriod.startDate, appliedPeriod.endDate))
      )
    );
    this.multiEditTable.setValue(data); // todo await ?

    this.form.reset();
    this.form.disable();
  }

  protected filterUniqueMonitoringLocation(rows: AsyncTableElement<AppliedPeriodComposite>[]): AsyncTableElement<AppliedPeriodComposite>[] {
    return rows.filter(
      (value, index, array) => array.map((ref) => ref.currentData.monitoringLocation.id).indexOf(value.currentData.monitoringLocation.id) === index
    );
  }

  private pendingComposites(): AppliedPeriodComposite[] {
    const startDate = fromDateISOString(this.form.value.startDate);
    const endDate = fromDateISOString(this.form.value.endDate);
    return this._selectedRows
      .map((row) => row.currentData)
      .map(
        (composite) =>
          new AppliedPeriodComposite(
            composite.appliedStrategyId,
            composite.monitoringLocation,
            startDate || composite.startDate,
            endDate || composite.endDate
          )
      );
  }

  private async checkOverlappingPeriods(): Promise<boolean> {
    const allComposites = this.multiEditTable.value;
    const pendingComposites = this.pendingComposites();

    // Check in local data

    let hasOverlap = pendingComposites.some(
      (pendingComposite) =>
        allComposites.filter(
          (composite) =>
            composite.monitoringLocation.id === pendingComposite.monitoringLocation.id &&
            Dates.overlaps(
              { start: composite.startDate, end: composite.endDate },
              { start: pendingComposite.startDate, end: pendingComposite.endDate }
            )
        ).length > 1
    );
    if (hasOverlap) {
      console.warn(`[applied-period-multi-edit] Overlapping period found in local table`);
      return false;
    }

    // Check in all strategies (local and remote)
    hasOverlap = await asyncSome(pendingComposites, async (pendingComposite: AppliedPeriodComposite) => {
      return !(await emitPromiseEvent(this.checkOverlappingPeriodsEventEmitter, 'checkOverlappingPeriods', {
        detail: {
          monitoringLocationIds: [pendingComposite.monitoringLocation.id],
          dateFilters: [DateFilter.fromObject({ startLowerBound: pendingComposite.startDate, endUpperBound: pendingComposite.endDate })],
        },
      }));
    });

    if (hasOverlap) {
      console.warn(`[applied-period-multi-edit] Overlapping period found in another strategy`);
      return false;
    }
    return true;
  }

  private checkValidDates(): boolean {
    return this.pendingComposites().every((composite) => composite.endDate.isSameOrAfter(composite.startDate));
  }
}
