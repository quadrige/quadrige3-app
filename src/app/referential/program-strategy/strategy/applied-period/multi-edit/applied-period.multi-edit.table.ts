import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, OnInit, Output } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { AppliedPeriodComposite } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.model';
import { AppliedPeriodCompositeService } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.service';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';

@Component({
  selector: 'app-applied-period-multi-edit-table',
  templateUrl: './applied-period.multi-edit.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppliedPeriodMultiEditTable extends ReferentialMemoryTable<AppliedPeriodComposite, string, any, any> implements OnInit, AfterViewInit {
  @Output() deletePeriod = new EventEmitter<AsyncTableElement<AppliedPeriodComposite>[]>();

  constructor(
    protected injector: Injector,
    protected _entityService: AppliedPeriodCompositeService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['monitoringLocation', 'startDate', 'endDate']).concat(RESERVED_END_COLUMNS),
      AppliedPeriodComposite,
      _entityService,
      null
    );

    this.i18nColumnPrefix = 'REFERENTIAL.APPLIED_PERIOD.';
    this.logPrefix = '[applied-period-multi-edit-table]';
    this.selectTable = true;
  }

  ngOnInit() {
    this.showPaginator = false;
    this.checkBoxSelection = true;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;
  }

  protected getUserColumns(): string[] {
    return []; // Force no column position changed
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('monitoringLocation', 'startDate', 'endDate');
  }
}
