import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import {
  AppliedPeriodValidatorOptions,
  AppliedPeriodValidatorService,
} from '@app/referential/program-strategy/strategy/applied-period/applied-period.validator';
import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { createPromiseEventEmitter, isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { AppliedPeriodService } from '@app/referential/program-strategy/strategy/applied-period/applied-period.service';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { UntypedFormGroup } from '@angular/forms';
import { CheckOverlappingPeriodsOptions } from '@app/referential/program-strategy/strategy/strategy.table';
import {
  AppliedPeriodMultiEditModal,
  IAppliedPeriodMultiEditModalOptions,
} from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.modal';
import { AppliedPeriodComposite } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { Utils } from '@app/shared/utils';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-applied-period-table',
  templateUrl: './applied-period.table.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AppliedPeriodService,
      useExisting: false,
    },
  ],
})
export class AppliedPeriodTable
  extends ReferentialMemoryTable<AppliedPeriod, string, any, any, AppliedPeriodValidatorService>
  implements OnInit, AfterViewInit
{
  @Input() strategyDuplication = false;
  @Input() programId: string;

  @Output() checkOverlappingPeriodsEventEmitter = createPromiseEventEmitter<boolean, CheckOverlappingPeriodsOptions>();
  @Output() periodsMultiEditChanged = new EventEmitter<AppliedPeriodComposite[]>();

  private _appliedStrategies: AppliedStrategy[];
  private _monitoringLocationIds: number[];
  private _multiplePeriods = false;

  constructor(
    protected injector: Injector,
    protected _entityService: AppliedPeriodService,
    protected validatorService: AppliedPeriodValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['startDate', 'endDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      AppliedPeriod,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.ENTITY.APPLIED_PERIODS';
    this.i18nColumnPrefix = 'REFERENTIAL.APPLIED_PERIOD.';
    this.defaultSortBy = 'startDate';
    this.logPrefix = '[applied-period-table]';
  }

  // used for checking overlapping periods
  @Input() set monitoringLocationIds(value: number[]) {
    const mustReset =
      (isNotEmptyArray(this._monitoringLocationIds) || isNotEmptyArray(value)) && !Utils.arrayEquals(this._monitoringLocationIds, value);
    this._monitoringLocationIds = value;
    if (mustReset) {
      // Must clear table to ensure row validator recreation
      this.setValue(undefined, { emitEvent: false });
    }
  }

  get monitoringLocationIds(): number[] {
    return this._monitoringLocationIds;
  }

  get multiplePeriods(): boolean {
    return this._multiplePeriods;
  }

  set multiplePeriods(value: boolean) {
    this._multiplePeriods = value;
    this.inlineEdition = this.canEdit && !value;
  }

  ngOnInit() {
    this.subTable = true;
    this.showPaginator = false;

    super.ngOnInit();
  }

  async setAppliedStrategy(appliedStrategy: AppliedStrategy) {
    this._appliedStrategies = [appliedStrategy];
    this.multiplePeriods = false;

    await this.setValue(appliedStrategy?.appliedPeriods?.slice());
  }

  async setAppliedStrategies(appliedStrategies: AppliedStrategy[]) {
    this._appliedStrategies = appliedStrategies;
    this.multiplePeriods = true;

    // compute only common periods
    const distinctPeriods: AppliedPeriod[] = [];
    appliedStrategies?.forEach((appliedStrategy) => {
      appliedStrategy.appliedPeriods.forEach((appliedPeriod) => {
        if (!distinctPeriods.find((period) => period.dateEquals(appliedPeriod))) distinctPeriods.push(appliedPeriod);
      });
    });

    await this.setValue(distinctPeriods);
  }

  async multiEditRows(event: UIEvent) {
    if (!this.canEdit) return;

    const { role, data } = await this.modalService.openModal<IAppliedPeriodMultiEditModalOptions, AppliedPeriodComposite[]>(
      AppliedPeriodMultiEditModal,
      {
        canEdit: true,
        titleI18n: 'REFERENTIAL.APPLIED_PERIOD.MULTI_EDIT.TITLE',
        i18nPrefix: this.i18nColumnPrefix,
        appliedStrategies: this._appliedStrategies,
        checkOverlappingPeriodsEventEmitter: this.checkOverlappingPeriodsEventEmitter,
        checkPeriodsToDeleteEventEmitter: this.onBeforeDeleteRows,
      },
      'modal-full-height'
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // send to AppliedStrategy table
      this.periodsMultiEditChanged.emit(data);
    }
  }

  protected getRowValidator(row: AsyncTableElement<AppliedPeriod>): UntypedFormGroup {
    return super.getRowValidator(row, <AppliedPeriodValidatorOptions>{
      monitoringLocationIds:
        this.monitoringLocationIds || this._appliedStrategies?.map((appliedStrategy) => appliedStrategy.monitoringLocation.id) || [],
      checkOverlappingPeriodsEventEmitter: this.checkOverlappingPeriodsEventEmitter,
      overlappingErrorMessage: isNotEmptyArray(this.monitoringLocationIds)
        ? 'REFERENTIAL.APPLIED_PERIOD.ERROR.OVERLAPPING_PERIODS_SELECTED_LOCATIONS'
        : 'REFERENTIAL.APPLIED_PERIOD.ERROR.OVERLAPPING_PERIODS',
      fromStrategyDuplication: this.strategyDuplication,
      programId: this.programId,
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('startDate', 'endDate');
  }
}
