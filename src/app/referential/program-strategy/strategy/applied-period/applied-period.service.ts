import { Injectable } from '@angular/core';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';

@Injectable()
export class AppliedPeriodService extends UnfilteredEntitiesMemoryService<AppliedPeriod, string> {
  constructor() {
    super(AppliedPeriod);
  }
}
