import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { EventEmitter, Injectable } from '@angular/core';
import { AbstractControlOptions, AsyncValidatorFn, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, Validators } from '@angular/forms';
import { emitPromiseEvent, IEntitiesTableDataSource, SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { Observable, timer } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { CheckOverlappingPeriodsOptions } from '@app/referential/program-strategy/strategy/strategy.table';
import { Dates } from '@app/shared/dates';

export interface AppliedPeriodValidatorOptions extends ReferentialValidatorOptions {
  monitoringLocationIds?: number[];
  checkOverlappingPeriodsEventEmitter?: EventEmitter<any>;
  overlappingErrorMessage?: string;
  programId?: string;
  fromStrategyDuplication?: boolean;
}

@Injectable({ providedIn: 'root' })
export class AppliedPeriodValidatorService extends ReferentialValidatorService<AppliedPeriod, string, AppliedPeriodValidatorOptions> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: AppliedPeriod, opts?: AppliedPeriodValidatorOptions): { [p: string]: any } {
    return {
      id: [data?.id || null],
      appliedStrategyId: [data?.appliedStrategyId || null],
      startDate: [data?.startDate || null], // no validators here because mat-date-field owns them
      endDate: [data?.endDate || null], // no validators here because mat-date-field owns them
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: AppliedPeriod, opts?: AppliedPeriodValidatorOptions): AbstractControlOptions {
    return {
      validators: [SharedFormGroupValidators.dateRange('startDate', 'endDate')],
      asyncValidators: this.checkOverlappingPeriods(this.dataSource, opts),
    };
  }

  checkOverlappingPeriods(dataSource: IEntitiesTableDataSource<any>, opts?: AppliedPeriodValidatorOptions): AsyncValidatorFn {
    return (group: UntypedFormGroup): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => group),
        mergeMap(async (_group) => {
          const value = _group.value;
          if (_group.dirty && !!value.startDate && !!value.endDate) {
            // first check in current rows
            if (
              dataSource
                .getRows()
                .filter((row) =>
                  Dates.overlaps({ start: row.currentData.startDate, end: row.currentData.endDate }, { start: value.startDate, end: value.endDate })
                ).length > 1
            ) {
              return this.getOverlappingError(opts);
            }

            // then check by checking the PromiseEvent result
            if (opts && opts.checkOverlappingPeriodsEventEmitter?.observed && opts.monitoringLocationIds?.length > 0) {
              try {
                const periodValid = await emitPromiseEvent(opts.checkOverlappingPeriodsEventEmitter, 'checkOverlappingPeriods', {
                  detail: <CheckOverlappingPeriodsOptions>{
                    monitoringLocationIds: opts.monitoringLocationIds,
                    dateFilters: [{ startLowerBound: value.startDate, endUpperBound: value.endDate }],
                    programId: opts.programId,
                    fromStrategyDuplication: opts.fromStrategyDuplication,
                  },
                });

                return !periodValid ? this.getOverlappingError(opts) : null;
              } catch (err) {
                console.error('Error while checking overlapping periods', err);
                throw err;
              }
            }
          }

          return null;
        })
      );
  }

  protected getOverlappingError(opts?: AppliedPeriodValidatorOptions): any {
    return {
      overlappingErrorMsg: opts?.overlappingErrorMessage || 'REFERENTIAL.APPLIED_PERIOD.ERROR.OVERLAPPING_PERIODS',
    };
  }
}
