import { EntityClass, fromDateISOString, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'AppliedPeriodVO' })
export class AppliedPeriod extends Referential<AppliedPeriod, string> {
  static entityName = 'AppliedPeriod';
  static fromObject: (source: any, opts?: any) => AppliedPeriod;

  appliedStrategyId: number = null;
  startDate: Moment = null;
  endDate: Moment = null;

  constructor(appliedStrategyId?: number, startDate?: Moment, endDate?: Moment) {
    super(AppliedPeriod.TYPENAME);
    this.entityName = AppliedPeriod.entityName;
    this.appliedStrategyId = appliedStrategyId;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = AppliedPeriod.entityName;
    this.appliedStrategyId = source.appliedStrategyId;
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.startDate = Dates.toLocalDateString(this.startDate);
    target.endDate = Dates.toLocalDateString(this.endDate);
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.statusId;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    return target;
  }

  dateEquals(other: AppliedPeriod): boolean {
    return this.startDate.isSame(other.startDate, 'date') && this.endDate.isSame(other.endDate, 'date');
  }
}
