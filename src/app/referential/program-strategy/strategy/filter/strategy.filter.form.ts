import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { isNotNil, isNotNilOrNaN } from '@sumaris-net/ngx-components';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { StrategyFilter, StrategyFilterCriteria } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { DateRangeFilterFormField } from '@app/shared/component/filter/date-range-filter-form-field.component';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-strategy-filter-form',
  templateUrl: './strategy.filter.form.html',
  standalone: true,
  imports: [ReferentialModule, DateRangeFilterFormField],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyFilterForm extends ReferentialCriteriaFormComponent<StrategyFilter, StrategyFilterCriteria> {
  @Input() programId: string;

  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idNameDescription;
  }

  protected criteriaToQueryParams(criteria: StrategyFilterCriteria): PartialRecord<keyof StrategyFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.onlyActive)) params.onlyActive = criteria.onlyActive;
    if (isNotNilOrNaN(criteria.pmfmuId)) params.pmfmuId = criteria.pmfmuId;
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    this.subCriteriaToQueryParams(params, criteria, 'departmentFilter');
    this.dateCriteriaToQueryParams(params, criteria, 'dateFilter');
    return params;
  }
}
