import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { DateFilter } from '@app/shared/model/date-filter';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';

@EntityClass({ typename: 'StrategyFilterCriteriaVO' })
export class StrategyFilterCriteria extends ReferentialFilterCriteria<Strategy, number> {
  static fromObject: (source: any, opts?: any) => StrategyFilterCriteria;

  onlyActive: boolean = null;
  programFilter: StrReferentialFilterCriteria = null;
  monitoringLocationFilter: IntReferentialFilterCriteria = null;
  departmentFilter: IntReferentialFilterCriteria = null;
  pmfmuFilter: PmfmuFilterCriteria = null;
  pmfmuId: number = null;
  dateFilter: DateFilter = null;
  responsibleUserFilter: IntReferentialFilterCriteria = null;
  responsibleDepartmentFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(StrategyFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.onlyActive = source.onlyActive || false;
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.departmentFilter = IntReferentialFilterCriteria.fromObject(source.departmentFilter || {});
    this.pmfmuFilter = PmfmuFilterCriteria.fromObject(source.pmfmuFilter);
    this.pmfmuId = source.pmfmuId;
    this.dateFilter = DateFilter.fromObject(source.dateFilter || {});
    this.responsibleUserFilter = IntReferentialFilterCriteria.fromObject(source.responsibleUserFilter || {});
    this.responsibleDepartmentFilter = IntReferentialFilterCriteria.fromObject(source.responsibleDepartmentFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.onlyActive = this.onlyActive || false;
    target.programFilter = this.programFilter?.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    target.departmentFilter = this.departmentFilter?.asObject(opts);
    if (!!this.pmfmuFilter) {
      target.pmfmuFilter = this.pmfmuFilter?.asObject(opts);
    } else if (!!this.pmfmuId) {
      target.pmfmuFilter = { id: this.pmfmuId };
    }
    target.dateFilter = this.dateFilter?.asObject();
    target.parentId = this.parentId; // = programId
    target.responsibleUserFilter = this.responsibleUserFilter?.asObject(opts);
    target.responsibleDepartmentFilter = this.responsibleDepartmentFilter?.asObject(opts);
    if (opts?.minify) {
      delete target.pmfmuId;
    }
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    if (key === 'departmentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.departmentFilter, true);
    if (key === 'pmfmuFilter') return !BaseFilterUtils.isCriteriaEmpty(this.pmfmuFilter, true);
    if (key === 'responsibleUserFilter') return !BaseFilterUtils.isCriteriaEmpty(this.responsibleUserFilter, true);
    if (key === 'responsibleDepartmentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.responsibleDepartmentFilter, true);
    if (key === 'dateFilter') return !this.dateFilter.isEmpty();
    if (key === 'parentId') return false;
    if (key === 'onlyActive') return this.onlyActive === true;
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'StrategyFilterVO' })
export class StrategyFilter extends ReferentialFilter<StrategyFilter, StrategyFilterCriteria, Strategy> {
  static fromObject: (source: any, opts?: any) => StrategyFilter;

  constructor() {
    super(StrategyFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): StrategyFilterCriteria {
    return StrategyFilterCriteria.fromObject(source, opts);
  }
}
