import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';

@EntityClass({ typename: 'StrategyVO' })
export class Strategy extends Referential<Strategy> {
  static entityName = 'Strategy';
  static fromObject: (source: any, opts?: any) => Strategy;

  programId: string = null;
  responsibleDepartmentIds: number[] = null;
  responsibleUserIds: number[] = null;
  appliedStrategies: AppliedStrategy[] = null;
  appliedStrategiesLoaded = false;
  pmfmuStrategies: PmfmuStrategy[] = null;
  pmfmuStrategiesLoaded = false;
  pmfmuAppliedStrategies: PmfmuAppliedStrategy[] = null;
  pmfmuAppliedStrategiesLoaded = false;
  mostRecent: boolean = null;

  constructor() {
    super(Strategy.TYPENAME);
    this.entityName = Strategy.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Strategy.entityName;
    this.programId = source.programId;
    this.responsibleDepartmentIds = source.responsibleDepartmentIds || [];
    this.responsibleUserIds = source.responsibleUserIds || [];
    this.appliedStrategies = source.appliedStrategies?.map(AppliedStrategy.fromObject) || [];
    this.appliedStrategiesLoaded = source.appliedStrategiesLoaded;
    this.pmfmuStrategies = source.pmfmuStrategies?.map(PmfmuStrategy.fromObject) || [];
    this.pmfmuStrategiesLoaded = source.pmfmuStrategiesLoaded;
    this.pmfmuAppliedStrategies = source.pmfmuAppliedStrategies?.map(PmfmuAppliedStrategy.fromObject) || [];
    this.pmfmuAppliedStrategiesLoaded = source.pmfmuAppliedStrategiesLoaded;
    this.mostRecent = source.mostRecent;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.responsibleDepartmentIds = this.responsibleDepartmentIds || undefined;
    target.responsibleUserIds = this.responsibleUserIds || undefined;

    if (!opts || opts.minify !== false) {
      // Split pmfmuAppliedStrategies into appliedStrategies
      this.appliedStrategies?.forEach((appliedStrategy) => {
        appliedStrategy.pmfmuAppliedStrategies = this.pmfmuAppliedStrategies?.filter((pmfmuAppliedStrategy) =>
          this.ownedBy(appliedStrategy, pmfmuAppliedStrategy)
        );
      });
      delete target.pmfmuAppliedStrategies;
    } else {
      target.pmfmuAppliedStrategies = this.pmfmuAppliedStrategies?.map((value) => value.asObject(opts));
    }

    target.appliedStrategies = this.appliedStrategies?.map((value) => value.asObject(opts));
    target.pmfmuStrategies = this.pmfmuStrategies?.map((value) => value.asObject(opts));
    delete target.appliedStrategiesLoaded;
    delete target.pmfmuStrategiesLoaded;
    delete target.pmfmuAppliedStrategiesLoaded;
    return target;
  }

  protected ownedBy(appliedStrategy: AppliedStrategy, pmfmAppliedStrategy: PmfmuAppliedStrategy): boolean {
    if (appliedStrategy.id) {
      return appliedStrategy.id === pmfmAppliedStrategy.appliedStrategyId || appliedStrategy.id === pmfmAppliedStrategy.appliedStrategy?.id;
    }
    return (
      appliedStrategy.monitoringLocation.id.toString() === pmfmAppliedStrategy.monitoringLocationId?.toString() ||
      appliedStrategy.monitoringLocation.id.toString() === pmfmAppliedStrategy.appliedStrategy?.monitoringLocation.id.toString()
    );
  }
}
