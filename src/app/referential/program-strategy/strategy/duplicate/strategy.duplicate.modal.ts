import { ChangeDetectionStrategy, Component, EventEmitter, inject, Injector, Input, Output, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { AppliedPeriodTable } from '@app/referential/program-strategy/strategy/applied-period/applied-period.table';
import { MatAutocompleteFieldConfig, ObjectMap, SharedValidators, StatusIds } from '@sumaris-net/ngx-components';
import { autocompleteWidthFull } from '@app/shared/constants';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { AppliedStrategyAddTable } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.add.table';
import { PmfmuStrategyAddTable } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.add.table';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { IModalOptions, ISelectCriteria } from '@app/shared/model/options.model';
import { ProgramAddTable } from '@app/referential/program-strategy/program/program.add.table';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { ProgramFilterCriteria } from '@app/referential/program-strategy/program/filter/program.filter.model';

export interface IStrategyDuplicateModalOptions extends IModalOptions {
  originStrategy: Strategy;
  programAddCriteria: Partial<ProgramFilterCriteria>;
  programAddFirstCriteria: ISelectCriteria;
  departmentConfig: MatAutocompleteFieldConfig;
  frequencyConfig: MatAutocompleteFieldConfig;
  taxonGroupConfig: MatAutocompleteFieldConfig;
  referenceTaxonConfig: MatAutocompleteFieldConfig;
  checkOverlappingPeriodsEventEmitter: EventEmitter<any>;
}

@Component({
  selector: 'app-strategy-duplicate-modal',
  templateUrl: './strategy.duplicate.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyDuplicateModal extends ModalComponent<Strategy> implements IStrategyDuplicateModalOptions {
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  @ViewChild('programAddTable') programAddTable: ProgramAddTable;
  @ViewChild('appliedStrategyAddTable') appliedStrategyAddTable: AppliedStrategyAddTable;
  @ViewChild('appliedPeriodTable') appliedPeriodTable: AppliedPeriodTable;
  @ViewChild('pmfmuStrategyAddTable') pmfmuStrategyAddTable: PmfmuStrategyAddTable;

  @Input() originStrategy: Strategy;
  @Input() programAddCriteria: Partial<ProgramFilterCriteria>;
  @Input() programAddFirstCriteria: ISelectCriteria;
  @Input() departmentConfig: MatAutocompleteFieldConfig;
  @Input() frequencyConfig: MatAutocompleteFieldConfig;
  @Input() taxonGroupConfig: MatAutocompleteFieldConfig;
  @Input() referenceTaxonConfig: MatAutocompleteFieldConfig;

  @Output() checkOverlappingPeriodsEventEmitter = new EventEmitter();

  i18nPrefix = 'REFERENTIAL.STRATEGY.DUPLICATE_MODAL.';
  monitoringLocationAddCriteria: ISelectCriteria = {};
  autocompleteWidth = autocompleteWidthFull;
  statusIds = StatusIds;

  readonly appliedStrategyStepIndex = 1;
  readonly appliedPeriodStepIndex = 2;
  readonly pmfmuStrategyStepIndex = 3;
  readonly otherPropertiesStepIndex = 4;

  private readonly programService = inject(ProgramService);

  constructor(protected injector: Injector) {
    super(injector);
    this.setForm(
      this.formBuilder.group({
        keepOriginalData: [true],
        samplingDepartment: [null, SharedValidators.entity],
        analystDepartment: [null, SharedValidators.entity],
        frequency: [null, SharedValidators.entity],
        taxonGroup: [null, SharedValidators.entity],
        referenceTaxon: [null, SharedValidators.entity],
      })
    );
  }

  get currentProgramSelected(): boolean {
    return this.targetProgramId === this.originStrategy.programId;
  }

  get targetProgramId(): string {
    return this.programAddTable?.selectedEntityIds[0];
  }

  get selectedAppliedStrategies(): AppliedStrategy[] {
    return this.appliedStrategyAddTable?.selectedEntities || [];
  }

  get selectedMonitoringLocationIds(): number[] {
    return this.selectedAppliedStrategies.map((value) => value.monitoringLocation.id);
  }

  get selectedPmfmuStrategies(): PmfmuStrategy[] {
    return this.pmfmuStrategyAddTable?.selectedEntities || [];
  }

  get selectedPmfmuIds(): number[] {
    return this.selectedPmfmuStrategies.map((value) => value.pmfmu.id);
  }

  get canGoNext() {
    return this.stepper.selected?.completed;
  }

  get showValidate() {
    return this.stepper.selectedIndex === this.otherPropertiesStepIndex;
  }

  get disabled(): boolean {
    return this.loading || !this.canGoNext;
  }

  protected async afterInit() {
    this.registerSubscription(
      this.form.valueChanges
        .pipe(
          map((value) => value.keepOriginalData),
          distinctUntilChanged()
        )
        .subscribe((value) => {
          if (value) this.disableOtherControls();
          else this.enableOtherControls();
        })
    );

    this.programAddTable.toolbarColor = 'secondary900';
    this.appliedStrategyAddTable.toolbarColor = 'secondary900';
    this.pmfmuStrategyAddTable.toolbarColor = 'secondary900';
    this.appliedPeriodTable.toolbarColor = 'secondary900';
    await this.appliedPeriodTable.setValue([]);
    this.disableOtherControls();
  }

  async stepChanged(event: StepperSelectionEvent) {
    // Save the applied periods table when step leaves the table
    if (event.previouslySelectedIndex === this.appliedPeriodStepIndex) {
      if (!(await this.appliedPeriodTable.save())) this.stepper.selectedIndex = this.appliedPeriodStepIndex;
    }

    // Prepare monitoring location table
    if (event.selectedIndex === this.appliedStrategyStepIndex && !this.selectedAppliedStrategies.length) {
      let availableAppliedStrategies = this.originStrategy.appliedStrategies.slice();
      if (this.currentProgramSelected) {
        // Same program
      } else {
        // Load the program
        const program = await this.programService.load(this.targetProgramId);
        const monitoringLocationIds = program.programLocations.map((value) => value.monitoringLocationId.toString());
        // Union with available monitoring location ids
        availableAppliedStrategies = availableAppliedStrategies.filter((value) =>
          monitoringLocationIds.includes(value.monitoringLocation.id.toString())
        );
      }
      // Build table
      await this.appliedStrategyAddTable.setValue(availableAppliedStrategies);
    }

    // Prepare Pmfmu table
    if (event.selectedIndex === this.pmfmuStrategyStepIndex && !this.selectedPmfmuStrategies.length) {
      await this.pmfmuStrategyAddTable.setValue(this.originStrategy.pmfmuStrategies.slice());
    }

    // Add a row when step is on applied periods
    if (event.selectedIndex === this.appliedPeriodStepIndex && this.appliedPeriodTable.totalRowCount === 0) {
      await this.appliedPeriodTable.addRow();
    }
  }

  protected dataToValidate(): Promise<Strategy> | Strategy {
    return this.buildStrategy();
  }

  private buildStrategy(): Strategy {
    const strategy: Partial<Strategy> = { ...this.originStrategy };
    strategy.id = undefined;
    strategy.creationDate = undefined;
    strategy.updateDate = undefined;
    strategy.programId = this.targetProgramId;

    strategy.appliedStrategies = this.selectedAppliedStrategies.map((source) => {
      const target: Partial<AppliedStrategy> = { ...source };
      target.id = undefined;
      target.creationDate = undefined;
      target.updateDate = undefined;
      target.appliedPeriods = this.appliedPeriodTable.value;
      if (!this.form.value.keepOriginalData) {
        target.department = this.form.value.samplingDepartment;
        target.frequency = this.form.value.frequency;
        target.taxonGroup = this.form.value.taxonGroup;
        target.referenceTaxon = this.form.value.referenceTaxon;
      }
      return AppliedStrategy.fromObject(target);
    });
    strategy.appliedStrategiesLoaded = true;
    const appliedStrategiesObjectMap = strategy.appliedStrategies.reduce((acc, item) => {
      acc[item.monitoringLocation.id] = item;
      return acc;
    }, {} as ObjectMap<AppliedStrategy>);

    strategy.pmfmuStrategies = this.selectedPmfmuStrategies.map((source) => {
      const target: Partial<PmfmuStrategy> = { ...source };
      target.id = undefined;
      target.creationDate = undefined;
      target.updateDate = undefined;
      return PmfmuStrategy.fromObject(target);
    });
    strategy.pmfmuStrategiesLoaded = true;
    const pmfmuStrategiesObjectMap = strategy.pmfmuStrategies.reduce((acc, item) => {
      acc[item.pmfmu.id] = item;
      return acc;
    }, {} as ObjectMap<PmfmuStrategy>);

    strategy.pmfmuAppliedStrategies = this.filteredPmfmuAppliedStrategies().map((source) => {
      const target: Partial<PmfmuAppliedStrategy> = { ...source };
      target.id = undefined;
      target.creationDate = undefined;
      target.updateDate = undefined;
      target.appliedStrategyId = undefined;
      target.appliedStrategy = appliedStrategiesObjectMap[source.monitoringLocationId];
      target.pmfmuStrategyId = undefined;
      target.pmfmuStrategy = pmfmuStrategiesObjectMap[source.pmfmuId];
      if (!this.form.value.keepOriginalData) {
        target.department = this.form.value.analystDepartment;
      }
      return PmfmuAppliedStrategy.fromObject(target);
    });

    return Strategy.fromObject(strategy);
  }

  private filteredPmfmuAppliedStrategies(): PmfmuAppliedStrategy[] {
    const monitoringLocationIds = this.selectedMonitoringLocationIds;
    const pmfmuIds = this.selectedPmfmuIds;
    return this.originStrategy.pmfmuAppliedStrategies?.filter(
      (value) =>
        monitoringLocationIds.map((id) => id.toString()).includes(value.appliedStrategy.monitoringLocation.id.toString()) &&
        pmfmuIds.includes(value.pmfmuStrategy.pmfmu.id)
    );
  }

  private disableOtherControls() {
    this.form.get('samplingDepartment').disable({ onlySelf: true });
    this.form.get('analystDepartment').disable({ onlySelf: true });
    this.form.get('frequency').disable({ onlySelf: true });
    this.form.get('taxonGroup').disable({ onlySelf: true });
    this.form.get('referenceTaxon').disable({ onlySelf: true });
  }

  private enableOtherControls() {
    this.form.get('samplingDepartment').enable({ onlySelf: true });
    this.form.get('analystDepartment').enable({ onlySelf: true });
    this.form.get('frequency').enable({ onlySelf: true });
    this.form.get('taxonGroup').enable({ onlySelf: true });
    this.form.get('referenceTaxon').enable({ onlySelf: true });
  }
}
