import { EntityClass, ReferentialAsObjectOptions, ReferentialUtils } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'PmfmuAppliedStrategyVO' })
export class PmfmuAppliedStrategy extends Referential<PmfmuAppliedStrategy, string> {
  static entityName = 'PmfmuAppliedStrategy';
  static fromObject: (source: any, opts?: any) => PmfmuAppliedStrategy;

  appliedStrategyId: number = null;
  appliedStrategy: AppliedStrategy = null;
  monitoringLocationId: number = null;
  pmfmuStrategyId: number = null;
  pmfmuStrategy: PmfmuStrategy = null;
  pmfmuId: number = null;
  analysisInstrument: IntReferential = null;
  department: IntReferential = null;

  constructor() {
    super(PmfmuAppliedStrategy.TYPENAME);
    this.entityName = PmfmuAppliedStrategy.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = PmfmuAppliedStrategy.entityName;
    this.appliedStrategy = source.appliedStrategy;
    this.appliedStrategyId = source.appliedStrategy?.id || source.appliedStrategyId;
    this.monitoringLocationId = source.appliedStrategy?.monitoringLocation?.id || source.monitoringLocationId;
    this.pmfmuStrategy = source.pmfmuStrategy;
    this.pmfmuStrategyId = source.pmfmuStrategy?.id || source.pmfmuStrategyId;
    this.pmfmuId = source.pmfmuStrategy?.pmfmu?.id || source.pmfmuId;
    this.analysisInstrument = IntReferential.fromObject(source.analysisInstrument);
    this.department = IntReferential.fromObject(source.department);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.appliedStrategyId = this.appliedStrategyId || this.appliedStrategy?.id;
    target.monitoringLocationId = this.monitoringLocationId || this.appliedStrategy?.monitoringLocation?.id;
    target.pmfmuStrategyId = this.pmfmuStrategyId || this.pmfmuStrategy?.id;
    target.pmfmuId = this.pmfmuId || this.pmfmuStrategy?.pmfmu?.id;
    if (!opts || opts.minify !== false) {
      delete target.appliedStrategy;
      delete target.pmfmuStrategy;
    }
    target.analysisInstrument = EntityUtils.asMinifiedObject(this.analysisInstrument, opts);
    target.department = EntityUtils.asMinifiedObject(this.department, opts);
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.label;
    delete target.name;
    delete target.description;
    delete target.comments;
    delete target.statusId;
    return target;
  }

  equals(other: PmfmuAppliedStrategy): boolean {
    return (
      super.equals(other) ||
      // has same applied strategy or same monitoring location
      ((this.appliedStrategyId === other.appliedStrategyId ||
        ReferentialUtils.equals(this.appliedStrategy.monitoringLocation, other.appliedStrategy?.monitoringLocation) ||
        this.appliedStrategy.monitoringLocation.id.toString() === other.monitoringLocationId.toString()) &&
        // has same pmfmu strategy or same pmfmu
        (this.pmfmuStrategyId === other.pmfmuStrategyId ||
          ReferentialUtils.equals(this.pmfmuStrategy.pmfmu, other.pmfmuStrategy?.pmfmu) ||
          this.pmfmuStrategy.pmfmu.id.toString() === other.pmfmuId.toString()))
    );
  }
}
