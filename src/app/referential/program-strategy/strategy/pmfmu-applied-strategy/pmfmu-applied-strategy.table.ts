import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { isEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { Alerts } from '@app/shared/alerts';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { PmfmuAppliedStrategyValidatorService } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.validator';
import { PmfmuAppliedStrategyService } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.service';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import {
  IPmfmuAppliedStrategyAddModalOptions,
  PmfmuAppliedStrategyAddModal,
} from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.add.modal';
import {
  IPmfmuAppliedStrategyMultiEditModalOptions,
  PmfmuAppliedStrategyMultiEditModal,
} from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/multi-edit/pmfmu-applied-strategy.multi-edit.modal';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import {
  PmfmuAppliedStrategyFilter,
  PmfmuAppliedStrategyFilterCriteria,
} from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/filter/pmfmu-applied-strategy.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { PmfmuAppliedStrategyFilterForm } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/filter/pmfmu-applied-strategy.filter.form';
import { PmfmuAppliedStrategyComposite } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/multi-edit/pmfmu-applied-strategy.multi-edit.model';

@Component({
  selector: 'app-pmfmu-applied-strategy-table',
  templateUrl: './pmfmu-applied-strategy.table.html',
  standalone: true,
  imports: [ReferentialModule, PmfmuAppliedStrategyFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuAppliedStrategyTable
  extends ReferentialMemoryTable<
    PmfmuAppliedStrategy,
    string,
    PmfmuAppliedStrategyFilter,
    PmfmuAppliedStrategyFilterCriteria,
    PmfmuAppliedStrategyValidatorService
  >
  implements OnInit, AfterViewInit
{
  appliedStrategies: AppliedStrategy[];
  pmfmuStrategies: PmfmuStrategy[];

  constructor(
    protected injector: Injector,
    protected _entityService: PmfmuAppliedStrategyService,
    protected validatorService: PmfmuAppliedStrategyValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'appliedStrategy.monitoringLocation',
        'pmfmuStrategy.pmfmu.id',
        'pmfmuStrategy.pmfmu.parameter',
        'pmfmuStrategy.pmfmu.matrix',
        'pmfmuStrategy.pmfmu.fraction',
        'pmfmuStrategy.pmfmu.method',
        'pmfmuStrategy.pmfmu.unit',
        'department',
        'analysisInstrument',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      PmfmuAppliedStrategy,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.ENTITY.PMFMU_APPLIED_STRATEGIES';
    this.i18nColumnPrefix = 'REFERENTIAL.PMFMU_APPLIED_STRATEGY.';
    this.defaultSortBy = 'appliedStrategy.monitoringLocation'; // todo should be 'appliedStrategy.monitoringLocation','pmfmuStrategy.pmfmu.parameter','pmfmuStrategy.pmfmu.matrix','pmfmuStrategy.pmfmu.fraction','pmfmuStrategy.pmfmu.method','pmfmuStrategy.pmfmu.unit'
    this.logPrefix = '[pmfmu-applied-strategy-table]';
  }

  ngOnInit() {
    this.showTitle = false;
    this.subTable = true;

    super.ngOnInit();

    // department combo
    this.registerAutocompleteField('department', {
      ...this.referentialOptions.department,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Department',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // analysis instrument combo
    this.registerAutocompleteField('analysisInstrument', {
      ...this.referentialOptions.analysisInstrument,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'AnalysisInstrument',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  async setValue(value: PmfmuAppliedStrategy[], opts?: { emitEvent?: boolean; resetFilter?: boolean }): Promise<void> {
    // clear selection
    this.selection.clear();
    if ((!opts || opts.resetFilter !== false) && !this.filterIsEmpty) {
      await this.resetFilter(undefined, { emitEvent: false });
    }
    await super.setValue(value, opts);
  }

  async addRow(event?: Event): Promise<boolean> {
    await this.openAddModal(event);
    return false;
  }

  async openAddModal(event: Event) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }
    await this.save();
    this.selection.clear();

    const { role, data } = await this.modalService.openModal<IPmfmuAppliedStrategyAddModalOptions, PmfmuAppliedStrategy[]>(
      PmfmuAppliedStrategyAddModal,
      {
        appliedStrategies: this.appliedStrategies,
        pmfmuStrategies: this.pmfmuStrategies,
        departmentConfig: this.autocompleteFields.department,
        analysisInstrumentConfig: this.autocompleteFields.analysisInstrument,
      },
      'modal-large'
    );
    if (role === 'validate' && data) {
      const newSelected = this.value?.slice() || [];
      const associationsToAdd: PmfmuAppliedStrategy[] = [];
      const associationsToReplace: { existing: PmfmuAppliedStrategy; association: PmfmuAppliedStrategy }[] = [];
      // Find if associations already exists
      data.forEach((association) => {
        const existing = newSelected.find(
          (row) => row.appliedStrategyId === association.appliedStrategyId && row.pmfmuStrategyId === association.pmfmuStrategyId
        );
        if (existing) {
          associationsToReplace.push({ existing, association });
        } else {
          associationsToAdd.push(association);
        }
      });
      // Replace existing associations: department and analysisInstrument
      if (associationsToReplace.length) {
        const confirmReplace = await Alerts.askConfirmation('REFERENTIAL.PMFMU_APPLIED_STRATEGY.CONFIRM_REPLACE', this.alertCtrl, this.translate);
        if (confirmReplace) {
          associationsToReplace.forEach((replace) => {
            replace.existing.department = replace.association.department;
            replace.existing.analysisInstrument = replace.association.analysisInstrument;
          });
        }
      }
      // Add new associations
      if (associationsToAdd.length) {
        newSelected.push(...associationsToAdd);
      }

      // affect values and mark as dirty !
      await this.setValue(newSelected);
      this.markAsDirty();
    }
  }

  async multiEditRows(event: MouseEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IPmfmuAppliedStrategyMultiEditModalOptions, PmfmuAppliedStrategyComposite>(
      PmfmuAppliedStrategyMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.PMFMU_APPLIED_STRATEGY.MULTI_EDIT.TITLE',
        i18nPrefix: this.i18nColumnPrefix,
        selection: this.selection.selected.map((row) => row.currentData),
        defaultReferentialCriteria: this.defaultReferentialCriteria,
      },
      'modal-300'
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // Patch selected rows
      this.selection.selected.forEach((row) => {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    }
  }

  async cleanOrphanRows() {
    if (isEmptyArray(this.value)) return;
    if (this.debug) console.debug(`${this.logPrefix} Clean orphan rows`);

    const monitoringLocationIds = this.appliedStrategies?.map((value) => value.monitoringLocation.id.toString()) || [];
    const pmfmuIds = this.pmfmuStrategies?.map((value) => value.pmfmu.id) || [];
    const values = this.value
      .slice()
      .filter(
        (value) =>
          monitoringLocationIds.includes(value.appliedStrategy.monitoringLocation.id.toString()) && pmfmuIds.includes(value.pmfmuStrategy.pmfmu.id)
      );
    await this.setValue(values);
  }

  // Make it public
  public rightSplitResized() {
    super.rightSplitResized();
  }
}
