import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { PmfmuAppliedStrategyComposite } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/multi-edit/pmfmu-applied-strategy.multi-edit.model';
import { CompositeUtils } from '@app/shared/model/composite';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { GenericService } from '@app/referential/generic/generic.service';
import { PmfmuAppliedStrategyValidatorService } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.validator';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { StatusIds } from '@sumaris-net/ngx-components';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';

export interface IPmfmuAppliedStrategyMultiEditModalOptions extends IModalOptions {
  i18nPrefix: string;
  selection: PmfmuAppliedStrategy[];
}

@Component({
  selector: 'app-pmfmu-applied-strategy-multi-edit-modal',
  templateUrl: './pmfmu-applied-strategy.multi-edit.modal.html',
  styleUrl: './pmfmu-applied-strategy.multi-edit.modal.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuAppliedStrategyMultiEditModal
  extends ModalComponent<PmfmuAppliedStrategyComposite>
  implements IPmfmuAppliedStrategyMultiEditModalOptions, OnInit
{
  @Input() i18nPrefix: string;
  @Input() selection: PmfmuAppliedStrategy[];

  statusIds = StatusIds;
  panelWidth = autocompleteWidthExtraLarge;

  constructor(
    protected injector: Injector,
    protected referentialGenericService: GenericService,
    protected pmfmuAppliedStrategyValidator: PmfmuAppliedStrategyValidatorService
  ) {
    super(injector);
    // Get PmfmuAppliedStrategyComposite form config
    const formConfig = CompositeUtils.getFormGroupConfig(new PmfmuAppliedStrategyComposite(), pmfmuAppliedStrategyValidator.getFormGroupConfig());
    this.setForm(this.formBuilder.group(formConfig));
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerAutocompleteField('department', {
      ...referentialOptions.department,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Department',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // analysis instrument combo
    this.registerAutocompleteField('analysisInstrument', {
      ...referentialOptions.analysisInstrument,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'AnalysisInstrument',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    // Listen for changes
    CompositeUtils.listenCompositeFormChanges(this.form.controls, (subscription) => this.registerSubscription(subscription));
  }

  protected afterInit() {
    this.value = CompositeUtils.build(this.selection, (candidate) => PmfmuAppliedStrategyComposite.fromPmfmAppliedStrategy(candidate));
  }

  protected dataToValidate(): Promise<PmfmuAppliedStrategyComposite> | PmfmuAppliedStrategyComposite {
    return CompositeUtils.value(this.value);
  }
}
