import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { IComposite } from '@app/shared/model/composite';
import { IntReferential } from '@app/referential/model/referential.model';

export class PmfmuAppliedStrategyComposite implements IComposite {
  analysisInstrument: IntReferential = null;
  analysisInstrumentMultiple = false;
  department: IntReferential = null;
  departmentMultiple = false;

  static fromPmfmAppliedStrategy(source: PmfmuAppliedStrategy): PmfmuAppliedStrategyComposite {
    const target = new PmfmuAppliedStrategyComposite();

    target.analysisInstrument = IntReferential.fromObject(source.analysisInstrument);
    target.department = IntReferential.fromObject(source.department);

    return target;
  }
}
