import { ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { AppliedStrategyTable } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.table';
import { PmfmuStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.table';
import { MatAutocompleteFieldConfig, SharedValidators } from '@sumaris-net/ngx-components';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { MatStepper } from '@angular/material/stepper';
import { autocompleteWidthFull } from '@app/shared/constants';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IPmfmuAppliedStrategyAddModalOptions extends IModalOptions {
  appliedStrategies: AppliedStrategy[];
  pmfmuStrategies: PmfmuStrategy[];
  departmentConfig: MatAutocompleteFieldConfig;
  analysisInstrumentConfig: MatAutocompleteFieldConfig;
}

@Component({
  selector: 'app-pmfmu-applied-strategy-add-modal',
  templateUrl: './pmfmu-applied-strategy.add.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuAppliedStrategyAddModal extends ModalComponent<PmfmuAppliedStrategy[]> implements IPmfmuAppliedStrategyAddModalOptions {
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  @ViewChild('appliedStrategyTable') appliedStrategyTable: AppliedStrategyTable;
  @ViewChild('pmfmuStrategyTable') pmfmuStrategyTable: PmfmuStrategyTable;

  @Input() appliedStrategies: AppliedStrategy[];
  @Input() pmfmuStrategies: PmfmuStrategy[];
  @Input() departmentConfig: MatAutocompleteFieldConfig;
  @Input() analysisInstrumentConfig: MatAutocompleteFieldConfig;

  autocompleteWidth = autocompleteWidthFull;

  readonly otherPropertiesStepIndex = 2;

  constructor(protected injector: Injector) {
    super(injector);
    this.setForm(
      this.formBuilder.group({
        department: [null, SharedValidators.entity],
        analysisInstrument: [null, SharedValidators.entity],
      })
    );
    this.validIfPristine = true;
  }

  get appliedStrategyTableValid(): boolean {
    return !this.loading && !this.appliedStrategyTable?.selection.isEmpty();
  }

  get pmfmuStrategyTableValid(): boolean {
    return !this.loading && !this.pmfmuStrategyTable?.selection.isEmpty();
  }

  get canGoNext() {
    return this.stepper.selected?.completed;
  }

  get canValidate() {
    return this.stepper.selectedIndex === this.otherPropertiesStepIndex;
  }

  protected async afterInit() {
    this.appliedStrategyTable.toolbarColor = 'secondary900';
    this.pmfmuStrategyTable.toolbarColor = 'secondary900';
    // Load applied strategies and pmfmu strategies
    await this.appliedStrategyTable.setValue(this.appliedStrategies);
    await this.pmfmuStrategyTable.setValue(this.pmfmuStrategies);
  }

  async onNext(event: any) {
    event?.stopPropagation();
    this.stepper.next();
  }

  protected dataToValidate(): Promise<PmfmuAppliedStrategy[]> | PmfmuAppliedStrategy[] {
    // Build pmfmu applied strategies results
    const associations: PmfmuAppliedStrategy[] = [];

    this.appliedStrategyTable.selection.selected
      .map((row) => row.currentData)
      .forEach((appliedStrategy) => {
        this.pmfmuStrategyTable.selection.selected
          .map((row) => row.currentData)
          .forEach((pmfmuStrategy) => {
            const association = new PmfmuAppliedStrategy();
            association.appliedStrategy = appliedStrategy;
            association.appliedStrategyId = appliedStrategy.id;
            association.pmfmuStrategy = pmfmuStrategy;
            association.pmfmuStrategyId = pmfmuStrategy.id;
            association.department = this.form.value.department;
            association.analysisInstrument = this.form.value.analysisInstrument;
            associations.push(association);
          });
      });
    return associations;
  }
}
