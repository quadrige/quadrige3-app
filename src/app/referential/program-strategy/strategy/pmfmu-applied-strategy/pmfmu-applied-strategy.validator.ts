import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { SharedValidators } from '@sumaris-net/ngx-components';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';

@Injectable({ providedIn: 'root' })
export class PmfmuAppliedStrategyValidatorService extends ReferentialValidatorService<PmfmuAppliedStrategy, string> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: PmfmuAppliedStrategy, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      appliedStrategyId: [data?.appliedStrategyId || null],
      appliedStrategy: [data?.appliedStrategy || null],
      pmfmuStrategyId: [data?.pmfmuStrategyId || null],
      pmfmuStrategy: [data?.pmfmuStrategy || null],
      analysisInstrument: [data?.analysisInstrument || null, SharedValidators.entity],
      department: [data?.department || null, SharedValidators.entity],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }
}
