import { EntityAsObjectOptions, FilterFn } from '@sumaris-net/ngx-components';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export class PmfmuAppliedStrategyFilterCriteria extends ReferentialFilterCriteria<PmfmuAppliedStrategy, string> {
  monitoringLocationFilter: IntReferentialFilterCriteria = null;
  parameterFilter: ParameterFilterCriteria = null;
  departmentFilter: IntReferentialFilterCriteria = null;

  static fromObject(source: any, opts?: any): PmfmuAppliedStrategyFilterCriteria {
    const target = new PmfmuAppliedStrategyFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.parameterFilter = ParameterFilterCriteria.fromObject(source.parameterFilter || {});
    this.departmentFilter = IntReferentialFilterCriteria.fromObject(source.departmentFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    target.parameterFilter = this.parameterFilter?.asObject(opts);
    target.departmentFilter = this.departmentFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    if (key === 'parameterFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterFilter, true);
    if (key === 'departmentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.departmentFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  // todo use buildCriteria
  protected buildFilter(): FilterFn<PmfmuAppliedStrategy>[] {
    const target = super.buildFilter();

    if (this.monitoringLocationFilter) {
      // Add 'id' to searchAttributes (Mantis #60124)
      this.monitoringLocationFilter.searchAttributes = ['id', ...referentialOptions.monitoringLocation.attributes];
      const filter = this.monitoringLocationFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.appliedStrategy.monitoringLocation));
    }
    if (this.parameterFilter) {
      const filter = this.parameterFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.pmfmuStrategy.pmfmu.parameter));
    }
    if (this.departmentFilter) {
      const filter = this.departmentFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.department));
    }

    return target;
  }
}

export class PmfmuAppliedStrategyFilter extends ReferentialFilter<
  PmfmuAppliedStrategyFilter,
  PmfmuAppliedStrategyFilterCriteria,
  PmfmuAppliedStrategy,
  string
> {
  static fromObject(source: any, opts?: any): PmfmuAppliedStrategyFilter {
    const target = new PmfmuAppliedStrategyFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): PmfmuAppliedStrategyFilterCriteria {
    return PmfmuAppliedStrategyFilterCriteria.fromObject(source, opts);
  }
}
