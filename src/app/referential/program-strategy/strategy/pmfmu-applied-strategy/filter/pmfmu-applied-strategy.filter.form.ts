import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import {
  PmfmuAppliedStrategyFilter,
  PmfmuAppliedStrategyFilterCriteria,
} from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/filter/pmfmu-applied-strategy.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-pmfmu-applied-strategy-filter-form',
  templateUrl: './pmfmu-applied-strategy.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuAppliedStrategyFilterForm extends ReferentialCriteriaFormComponent<PmfmuAppliedStrategyFilter, PmfmuAppliedStrategyFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected criteriaToQueryParams(criteria: PmfmuAppliedStrategyFilterCriteria): PartialRecord<keyof PmfmuAppliedStrategyFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    this.subCriteriaToQueryParams(params, criteria, 'departmentFilter');
    this.subCriteriaToQueryParams(params, criteria, 'parameterFilter');
    return params;
  }
}
