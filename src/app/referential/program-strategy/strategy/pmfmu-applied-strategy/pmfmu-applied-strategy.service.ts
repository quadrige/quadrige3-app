import { Injectable } from '@angular/core';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { PmfmuAppliedStrategyFilter } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/filter/pmfmu-applied-strategy.filter.model';

@Injectable({ providedIn: 'root' })
export class PmfmuAppliedStrategyService extends EntitiesMemoryService<PmfmuAppliedStrategy, PmfmuAppliedStrategyFilter, string> {
  constructor() {
    super(PmfmuAppliedStrategy, PmfmuAppliedStrategyFilter, {
      sortByReplacement: { id: 'appliedStrategy.monitoringLocation.id' },
    });
  }
}
