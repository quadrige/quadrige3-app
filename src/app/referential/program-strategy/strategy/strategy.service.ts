import { Injectable, Injector } from '@angular/core';
import { isEmptyArray, isNil, isNotNil, LoadResult } from '@sumaris-net/ngx-components';
import {
  BaseReferentialService,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
  ReferentialSaveOptions,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { pmfmuFragments } from '@app/referential/pmfmu/pmfmu.service';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { DateFilter } from '@app/shared/model/date-filter';
import { StrategyHistory } from '@app/referential/program-strategy/strategy/history/strategy-history.model';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { StrategyFilter, StrategyFilterCriteria } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { jobFragments } from '@app/social/job/job.service';

const pmfmuAppliedStrategyFragment = gql`
  fragment PmfmuAppliedStrategyFragment on PmfmuAppliedStrategyVO {
    id
    appliedStrategyId
    monitoringLocationId
    pmfmuStrategyId
    pmfmuId
    analysisInstrument {
      ...ReferentialFragment
    }
    department {
      ...ReferentialFragment
    }
    updateDate
    __typename
  }
  ${referentialFragments.light}
`;

const appliedPeriodFragment = gql`
  fragment AppliedPeriodFragment on AppliedPeriodVO {
    id
    appliedStrategyId
    startDate
    endDate
    updateDate
    __typename
  }
`;

const appliedStrategyFragments = {
  read: gql`
    fragment AppliedStrategyReadFragment on AppliedStrategyVO {
      id
      monitoringLocation {
        ...ReferentialFragment
      }
      frequency {
        ...ReferentialFragment
      }
      department {
        ...ReferentialFragment
      }
      taxonGroup {
        ...ReferentialFragment
      }
      referenceTaxon {
        ...ReferentialFragment
      }
      appliedPeriods {
        ...AppliedPeriodFragment
      }
      updateDate
      __typename
    }
    ${referentialFragments.light}
    ${appliedPeriodFragment}
  `,
  write: gql`
    fragment AppliedStrategyWriteFragment on AppliedStrategyVO {
      id
      monitoringLocation {
        ...ReferentialFragment
      }
      frequency {
        ...ReferentialFragment
      }
      department {
        ...ReferentialFragment
      }
      taxonGroup {
        ...ReferentialFragment
      }
      referenceTaxon {
        ...ReferentialFragment
      }
      appliedPeriods {
        ...AppliedPeriodFragment
      }
      pmfmuAppliedStrategies {
        ...PmfmuAppliedStrategyFragment
      }
      updateDate
      __typename
    }
    ${referentialFragments.light}
    ${appliedPeriodFragment}
    ${pmfmuAppliedStrategyFragment}
  `,
};

const strategyHistoryFragment = gql`
  fragment StrategyHistoryFragment on StrategyHistoryVO {
    id
    appliedPeriod {
      ...AppliedPeriodFragment
    }
    program {
      ...ReferentialFragment
    }
    strategy {
      ...ReferentialFragment
    }
    department {
      ...ReferentialFragment
    }
    __typename
  }
  ${appliedPeriodFragment}
  ${referentialFragments.light}
`;

const pmfmuStrategyFragment = gql`
  fragment PmfmuStrategyFragment on PmfmuStrategyVO {
    id
    strategyId
    pmfmu {
      ...LightPmfmuFragment
    }
    rankOrder
    acquisitionNumber
    individual
    uniqueByTaxon
    precisionType {
      ...ReferentialFragment
    }
    acquisitionLevelIds
    uiFunctionIds
    qualitativeValueIds
    updateDate
    __typename
  }
  ${pmfmuFragments.lightPmfmu}
  ${referentialFragments.light}
`;

// fragment used to load strategy
const strategyFragment = gql`
  fragment StrategyFragment on StrategyVO {
    programId
    id
    name
    description
    responsibleDepartmentIds
    responsibleUserIds
    updateDate
    creationDate
    statusId
    mostRecent
    __typename
  }
`;

// fragment used for saving strategy
const fullStrategyFragment = gql`
  fragment FullStrategyFragment on StrategyVO {
    programId
    id
    name
    description
    responsibleDepartmentIds
    responsibleUserIds
    appliedStrategies {
      ...AppliedStrategyWriteFragment
    }
    pmfmuStrategies {
      ...PmfmuStrategyFragment
    }
    updateDate
    creationDate
    statusId
    __typename
  }
  ${appliedStrategyFragments.write}
  ${pmfmuStrategyFragment}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Strategies($page: PageInput, $filter: StrategyFilterVOInput) {
      data: strategies(page: $page, filter: $filter) {
        ...StrategyFragment
      }
    }
    ${strategyFragment}
  `,

  loadAllWithTotal: gql`
    query StrategiesWithTotal($page: PageInput, $filter: StrategyFilterVOInput) {
      data: strategies(page: $page, filter: $filter) {
        ...StrategyFragment
      }
      total: strategiesCount(filter: $filter)
    }
    ${strategyFragment}
  `,

  countAll: gql`
    query StrategiesCount($filter: StrategyFilterVOInput) {
      total: strategiesCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportStrategiesAsync($filter: StrategyFilterVOInput, $context: ExportContextInput) {
      data: exportStrategiesAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveStrategies($data: [StrategyVOInput], $options: StrategySaveOptionsInput) {
      data: saveStrategies(strategies: $data, options: $options) {
        ...FullStrategyFragment
      }
    }
    ${fullStrategyFragment}
  `,

  deleteAll: gql`
    mutation DeleteStrategies($ids: [Int]) {
      deleteStrategies(ids: $ids)
    }
  `,
};

const otherQueries = {
  appliedStrategies: gql`
    query AppliedStrategies($strategyId: Int) {
      data: appliedStrategies(strategyId: $strategyId) {
        ...AppliedStrategyReadFragment
      }
    }
    ${appliedStrategyFragments.read}
  `,

  strategiesHistory: gql`
    query StrategiesHistory($monitoringLocationId: Int, $exceptProgramIds: [String]) {
      data: strategiesHistory(monitoringLocationId: $monitoringLocationId, exceptProgramIds: $exceptProgramIds) {
        ...StrategyHistoryFragment
      }
    }
    ${strategyHistoryFragment}
  `,

  saveStrategiesHistory: gql`
    mutation SaveStrategiesHistory($data: [StrategyHistoryVOInput]) {
      data: saveStrategiesHistory(strategiesHistory: $data) {
        ...StrategyHistoryFragment
      }
    }
    ${strategyHistoryFragment}
  `,

  hasOverlappingPeriods: gql`
    query HasOverlappingPeriods($programId: String, $monitoringLocationIds: [Int], $dateFilters: [DateFilterVOInput], $excludeStrategyIds: [Int]) {
      data: hasOverlappingPeriods(
        programId: $programId
        monitoringLocationIds: $monitoringLocationIds
        dateFilters: $dateFilters
        excludeStrategyIds: $excludeStrategyIds
      )
    }
  `,

  canDeleteStrategies: gql`
    query CanDeleteStrategies($ids: [Int]) {
      data: canDeleteStrategies(ids: $ids)
    }
  `,

  canDeleteAppliedStrategies: gql`
    query CanDeleteAppliedStrategies($appliedStrategyIds: [Int]) {
      data: canDeleteAppliedStrategies(appliedStrategyIds: $appliedStrategyIds)
    }
  `,

  canDeleteAppliedPeriod: gql`
    query CanDeleteAppliedPeriod($programId: String, $locationId: Int, $startDate: LocalDate, $endDate: LocalDate) {
      data: canDeleteAppliedPeriod(programId: $programId, locationId: $locationId, startDate: $startDate, endDate: $endDate)
    }
  `,

  pmfmuStrategies: gql`
    query PmfmuStrategies($strategyId: Int) {
      data: pmfmuStrategies(strategyId: $strategyId) {
        ...PmfmuStrategyFragment
      }
    }
    ${pmfmuStrategyFragment}
  `,

  pmfmuAppliedStrategies: gql`
    query PmfmuAppliedStrategies($appliedStrategyIds: [Int]) {
      data: pmfmuAppliedStrategies(appliedStrategyIds: $appliedStrategyIds) {
        ...PmfmuAppliedStrategyFragment
      }
    }
    ${pmfmuAppliedStrategyFragment}
  `,
};

interface StrategySaveOptions extends ReferentialSaveOptions {
  withPmfmuStrategies: boolean;
  withAppliedStrategies: boolean;
  withPmfmuAppliedStrategies: boolean;
  withPrivileges: boolean;
}

const defaultSaveOptions: StrategySaveOptions = {
  withPmfmuStrategies: true,
  withAppliedStrategies: true,
  withPmfmuAppliedStrategies: true,
  withPrivileges: true,
};

@Injectable({ providedIn: 'root' })
export class StrategyService extends BaseReferentialService<Strategy, StrategyFilter, StrategyFilterCriteria, number, any, any, StrategySaveOptions> {
  private _strategyToDuplicate: Strategy;

  constructor(protected injector: Injector) {
    super(injector, Strategy, StrategyFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[strategy-service]';
  }

  get strategyToDuplicate(): Strategy {
    const value = this._strategyToDuplicate;
    this._strategyToDuplicate = undefined;
    return value;
  }

  set strategyToDuplicate(value: Strategy) {
    this._strategyToDuplicate = value;
  }

  async save(entity: Strategy, opts?: StrategySaveOptions): Promise<Strategy> {
    if (!entity.pmfmuAppliedStrategiesLoaded) {
      opts = { ...opts, withPmfmuAppliedStrategies: false };
    }

    return super.save(entity, { ...defaultSaveOptions, ...opts });
  }

  async saveAll(entities: Strategy[], opts?: StrategySaveOptions): Promise<Strategy[]> {
    // Split save operations depending on pmfmuAppliedStrategies are loaded or not
    const strategiesWithPmfmuAppliedStrategies: Strategy[] = entities?.filter((entity) => entity.pmfmuAppliedStrategiesLoaded) || [];
    const strategiesWithoutPmfmuAppliedStrategies: Strategy[] = entities?.filter((entity) => !entity.pmfmuAppliedStrategiesLoaded) || [];
    if (this._debug) {
      console.debug(
        `${this._logPrefix} Saving ${strategiesWithPmfmuAppliedStrategies.length} strategies with pmfmu applied strategies`,
        strategiesWithPmfmuAppliedStrategies
      );
      console.debug(
        `${this._logPrefix} Saving ${strategiesWithoutPmfmuAppliedStrategies.length} strategies without pmfmu applied strategies`,
        strategiesWithoutPmfmuAppliedStrategies
      );
    }
    const [result1, result2] = await Promise.all([
      super.saveAll(strategiesWithPmfmuAppliedStrategies, { ...defaultSaveOptions, ...opts }),
      super.saveAll(strategiesWithoutPmfmuAppliedStrategies, { ...defaultSaveOptions, ...opts, withPmfmuAppliedStrategies: false }),
    ]);
    return [].concat(...result1).concat(...result2);
  }

  async loadAppliedStrategies(strategyId: number): Promise<AppliedStrategy[]> {
    if (isNil(strategyId)) {
      return [];
    }

    const variables: any = { strategyId };
    if (this._debug) console.debug(`${this._logPrefix} Loading applied strategies...`, variables);
    const now = Date.now();
    const query = otherQueries.appliedStrategies;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(AppliedStrategy.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} applied strategies loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadStrategiesHistory(monitoringLocationId: number, exceptProgramIs: string[]): Promise<StrategyHistory[]> {
    const variables: any = { monitoringLocationId, exceptProgramIs };
    if (this._debug) console.debug(`${this._logPrefix} Loading strategies history...`, variables);
    const now = Date.now();
    const query = otherQueries.strategiesHistory;
    const res = await this.graphql.query<{ data: any[] }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(StrategyHistory.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} strategies history loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async saveStrategiesHistory(entities: StrategyHistory[]): Promise<StrategyHistory[]> {
    // Nothing to save: skip
    if (isEmptyArray(entities)) return entities;

    const now = Date.now();

    // Check type name
    if (entities[0].__typename !== 'StrategyHistoryVO') {
      throw new Error(`Cannot save a type '${entities[0].__typename}' in this service`);
    }

    // Transform to json
    const json = entities.map((entity) => {
      return entity.asObject();
    });
    const variables = { data: json };
    if (this._debug) console.debug(`${this._logPrefix} Saving all strategies history...`, variables);

    await this.graphql.mutate<LoadResult<any>>({
      mutation: otherQueries.saveStrategiesHistory,
      variables,
      error: { code: referentialErrorCodes.save, message: 'REFERENTIAL.ERROR.SAVE_REFERENTIAL_ERROR' },
    });
    if (this._debug) console.debug(`[referential-service] strategies history saved in ${Date.now() - now}ms`, entities);
    // no entities update, return as is
    return entities;
  }

  async hasOverlappingPeriods(
    programId: string,
    monitoringLocationIds: number[],
    dateFilters: DateFilter[],
    excludeStrategyIds?: number[]
  ): Promise<boolean> {
    const variables: any = {
      programId,
      monitoringLocationIds,
      dateFilters: dateFilters.map((dateFilter) => DateFilter.fromObject(dateFilter).asObject()),
      excludeStrategyIds,
    };
    if (this._debug) console.debug(`${this._logPrefix} Check has overlapping periods...`, variables);
    const now = Date.now();
    const query = otherQueries.hasOverlappingPeriods;
    const res = await this.graphql.query<{ data: boolean }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Check has overlapping periods in ${Date.now() - now}ms`, data);
    return data;
  }

  async canDeleteStrategies(ids: number[]): Promise<boolean> {
    ids = ids.filter((id) => isNotNil(id));
    if (isEmptyArray(ids)) {
      return true;
    }
    const variables: any = { ids };
    if (this._debug) console.debug(`${this._logPrefix} Check can delete strategies...`, variables);
    const now = Date.now();
    const query = otherQueries.canDeleteStrategies;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Can delete strategies in ${Date.now() - now}ms`, data);
    return data;
  }

  async canDeleteAppliedStrategies(appliedStrategyIds: number[]): Promise<boolean> {
    appliedStrategyIds = appliedStrategyIds.filter((id) => isNotNil(id));
    if (isEmptyArray(appliedStrategyIds)) {
      return true;
    }
    const variables: any = { appliedStrategyIds };
    if (this._debug) console.debug(`${this._logPrefix} Check can delete applied strategies...`, variables);
    const now = Date.now();
    const query = otherQueries.canDeleteAppliedStrategies;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Can delete applied strategies in ${Date.now() - now}ms`, data);
    return data;
  }

  async canDeleteAppliedPeriod(programId: string, locationId: number, startDate: Moment, endDate: Moment): Promise<boolean> {
    if (!startDate || !endDate) {
      return true;
    }
    const variables: any = {
      programId,
      locationId,
      startDate: Dates.toLocalDateString(startDate),
      endDate: Dates.toLocalDateString(endDate),
    };
    if (this._debug) console.debug(`${this._logPrefix} Check can delete applied period...`, variables);
    const now = Date.now();
    const query = otherQueries.canDeleteAppliedPeriod;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Can delete applied period in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadPmfmuStrategies(strategyId: number): Promise<PmfmuStrategy[]> {
    if (isNil(strategyId)) {
      return [];
    }

    const variables: any = { strategyId };
    if (this._debug) console.debug(`${this._logPrefix} Loading pmfmu strategies...`, variables);
    const now = Date.now();
    const query = otherQueries.pmfmuStrategies;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(PmfmuStrategy.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} pmfmu strategies loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadPmfmuAppliedStrategies(appliedStrategyIds: number[]): Promise<PmfmuAppliedStrategy[]> {
    if (isEmptyArray(appliedStrategyIds)) {
      return [];
    }

    const variables: any = { appliedStrategyIds };
    if (this._debug) console.debug(`${this._logPrefix} Loading pmfmu applied strategies...`, variables);
    const now = Date.now();
    const query = otherQueries.pmfmuAppliedStrategies;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(PmfmuAppliedStrategy.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} pmfmu applied strategies loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async checkUsersAreResponsible(programId: string, userIds: number[]): Promise<boolean> {
    const criteria = StrategyFilterCriteria.fromObject({
      parentId: programId,
      responsibleUserFilter: { includedIds: userIds },
    });
    return (await this.countByCriteria(criteria)) > 0;
  }

  async checkDepartmentsAreResponsible(programId: string, departmentIds: number[]): Promise<boolean> {
    const criteria = StrategyFilterCriteria.fromObject({
      parentId: programId,
      responsibleDepartmentFilter: { includedIds: departmentIds },
    });
    return (await this.countByCriteria(criteria)) > 0;
  }

  protected async countByCriteria(criteria: StrategyFilterCriteria): Promise<number> {
    return this.countAll(StrategyFilter.fromObject({ criterias: [criteria] }));
  }

  copyIdAndUpdateDate(source: Strategy, target: Strategy) {
    if (!source) return;
    super.copyIdAndUpdateDate(source, target);

    // Copy applied strategies and pmfmu associations
    if (source.appliedStrategies && target.appliedStrategies) {
      target.appliedStrategies.forEach((appliedStrategy) => {
        const savedAppliedStrategy = source.appliedStrategies.find((value) => appliedStrategy.equals(value));
        EntityUtils.copyIdAndUpdateDate(savedAppliedStrategy, appliedStrategy);
        // Associations (applied directly on target)
        if (savedAppliedStrategy.pmfmuAppliedStrategies && target.pmfmuAppliedStrategies) {
          target.pmfmuAppliedStrategies.forEach((pmfmuAppliedStrategy) => {
            const savedPmfmuAppliedStrategy = savedAppliedStrategy.pmfmuAppliedStrategies.find((value) => pmfmuAppliedStrategy.equals(value));
            if (savedPmfmuAppliedStrategy) {
              EntityUtils.copyIdAndUpdateDate(savedPmfmuAppliedStrategy, pmfmuAppliedStrategy);
              pmfmuAppliedStrategy.appliedStrategyId = savedPmfmuAppliedStrategy.appliedStrategyId;
              pmfmuAppliedStrategy.pmfmuStrategyId = savedPmfmuAppliedStrategy.pmfmuStrategyId;
            }
          });
        }
      });
    }

    // Copy pmfmu strategies
    if (source.pmfmuStrategies && target.pmfmuStrategies) {
      target.pmfmuStrategies.forEach((pmfmuStrategy) => {
        const savedPmfmuStrategy = source.pmfmuStrategies.find((value) => pmfmuStrategy.equals(value));
        EntityUtils.copyIdAndUpdateDate(savedPmfmuStrategy, pmfmuStrategy);
      });
    }
  }
}
