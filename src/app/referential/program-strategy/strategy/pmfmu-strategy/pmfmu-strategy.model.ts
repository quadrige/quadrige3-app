import { EntityClass, ReferentialAsObjectOptions, ReferentialUtils } from '@sumaris-net/ngx-components';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { acquisitionLevels, uiFunctions } from '@app/referential/model/referential.constants';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'PmfmuStrategyVO' })
export class PmfmuStrategy extends Referential<PmfmuStrategy> {
  static entityName = 'PmfmuStrategy';
  static fromObject: (source: any, opts?: any) => PmfmuStrategy;

  acquisitionNumber = 1;
  individual = false;
  uniqueByTaxon = false;
  precisionType: IntReferential = null;
  pmfmu: Pmfmu = null;
  rankOrder = -1;

  // acquisition levels
  survey = false;
  samplingOperation = false;
  sample = false;

  // ui functions
  sum = false;
  average = false;
  deviation = false;
  ic95 = false;

  qualitativeValueIds: number[] = null;

  constructor(pmfmu?: Pmfmu) {
    super(PmfmuStrategy.TYPENAME);
    this.entityName = PmfmuStrategy.entityName;
    this.pmfmu = pmfmu;
    this.qualitativeValueIds = pmfmu?.qualitativeValueIds;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = PmfmuStrategy.entityName;
    this.acquisitionNumber = source.acquisitionNumber;
    this.individual = source.individual || false;
    this.uniqueByTaxon = source.uniqueByTaxon || false;
    this.precisionType = IntReferential.fromObject(source.precisionType);
    this.pmfmu = Pmfmu.fromObject(source.pmfmu);
    this.rankOrder = source.rankOrder;
    this.survey = source.survey || (source.acquisitionLevelIds || []).includes(acquisitionLevels[0]);
    this.samplingOperation = source.samplingOperation || (source.acquisitionLevelIds || []).includes(acquisitionLevels[1]);
    this.sample = source.sample || (source.acquisitionLevelIds || []).includes(acquisitionLevels[2]);
    this.sum = source.sum || (source.uiFunctionIds || []).includes(uiFunctions[0]);
    this.average = source.average || (source.uiFunctionIds || []).includes(uiFunctions[1]);
    this.deviation = source.deviation || (source.uiFunctionIds || []).includes(uiFunctions[2]);
    this.ic95 = source.ic95 || (source.uiFunctionIds || []).includes(uiFunctions[3]);
    this.qualitativeValueIds = source.qualitativeValueIds || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.pmfmu = EntityUtils.asMinifiedObject(this.pmfmu, opts);
    target.precisionType = EntityUtils.asMinifiedObject(this.precisionType, opts);
    if (!opts || opts.minify !== false) {
      target.acquisitionLevelIds = [];
      if (this.survey) target.acquisitionLevelIds.push(acquisitionLevels[0]);
      if (this.samplingOperation) target.acquisitionLevelIds.push(acquisitionLevels[1]);
      if (this.sample) target.acquisitionLevelIds.push(acquisitionLevels[2]);
      delete target.survey;
      delete target.samplingOperation;
      delete target.sample;
      target.uiFunctionIds = [];
      if (this.sum) target.uiFunctionIds.push(uiFunctions[0]);
      if (this.average) target.uiFunctionIds.push(uiFunctions[1]);
      if (this.deviation) target.uiFunctionIds.push(uiFunctions[2]);
      if (this.ic95) target.uiFunctionIds.push(uiFunctions[3]);
      delete target.sum;
      delete target.average;
      delete target.deviation;
      delete target.ic95;
    }
    target.qualitativeValueIds = this.qualitativeValueIds || undefined;
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.statusId;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    return target;
  }

  equals(other: PmfmuStrategy): boolean {
    return super.equals(other) || ReferentialUtils.equals(this.pmfmu, other.pmfmu);
  }
}
