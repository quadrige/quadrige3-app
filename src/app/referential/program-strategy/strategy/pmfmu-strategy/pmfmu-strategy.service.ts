import { Injectable } from '@angular/core';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { PmfmuStrategyFilter } from '@app/referential/program-strategy/strategy/pmfmu-strategy/filter/pmfmu-strategy.filter.model';

@Injectable()
export class PmfmuStrategyService extends EntitiesMemoryService<PmfmuStrategy, PmfmuStrategyFilter> {
  constructor() {
    super(PmfmuStrategy, PmfmuStrategyFilter);
  }
}
