import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { IComposite } from '@app/shared/model/composite';
import { IntReferential } from '@app/referential/model/referential.model';

export class PmfmuStrategyComposite implements IComposite {
  acquisitionNumber: number = null;
  acquisitionNumberMultiple = false;
  individual: boolean = null;
  individualMultiple = false;
  uniqueByTaxon: boolean = null;
  uniqueByTaxonMultiple = false;
  precisionType: IntReferential = null;
  precisionTypeMultiple = false;
  survey: boolean = null;
  surveyMultiple = false;
  samplingOperation: boolean = null;
  samplingOperationMultiple = false;
  sample: boolean = null;
  sampleMultiple = false;
  sum: boolean = null;
  sumMultiple = false;
  average: boolean = null;
  averageMultiple = false;
  deviation: boolean = null;
  deviationMultiple = false;
  ic95: boolean = null;
  ic95Multiple = false;

  static fromPmfmuStrategy(source: PmfmuStrategy): PmfmuStrategyComposite {
    const target = new PmfmuStrategyComposite();

    target.acquisitionNumber = source.acquisitionNumber;
    target.individual = source.individual;
    target.uniqueByTaxon = source.uniqueByTaxon;
    target.precisionType = IntReferential.fromObject(source.precisionType);
    target.survey = source.survey;
    target.samplingOperation = source.samplingOperation;
    target.sample = source.sample;
    target.sum = source.sum;
    target.average = source.average;
    target.deviation = source.deviation;
    target.ic95 = source.ic95;

    return target;
  }
}
