import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { PmfmuStrategyComposite } from '@app/referential/program-strategy/strategy/pmfmu-strategy/multi-edit/pmfmu-strategy.multi-edit.model';
import { FormErrorTranslator, StatusIds } from '@sumaris-net/ngx-components';
import { CompositeUtils } from '@app/shared/model/composite';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { GenericService } from '@app/referential/generic/generic.service';
import { PmfmuStrategyValidatorService } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.validator';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';

export interface IPmfmuStrategyMultiEditModalOptions extends IModalOptions {
  i18nPrefix: string;
  selection: PmfmuStrategy[];
}

@Component({
  selector: 'app-pmfmu-strategy-multi-edit-modal',
  templateUrl: './pmfmu-strategy.multi-edit.modal.html',
  styleUrls: ['./pmfmu-strategy.multi-edit.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuStrategyMultiEditModal extends ModalComponent<PmfmuStrategyComposite> implements IPmfmuStrategyMultiEditModalOptions, OnInit {
  @Input() i18nPrefix: string;
  @Input() selection: PmfmuStrategy[];

  statusIds = StatusIds;
  panelWidth = autocompleteWidthExtraLarge;

  constructor(
    protected injector: Injector,
    protected formErrorTranslator: FormErrorTranslator,
    protected referentialGenericService: GenericService,
    protected pmfmuStrategyValidator: PmfmuStrategyValidatorService
  ) {
    super(injector);
    // Get PmfmuStrategyComposite form config
    const formConfig = CompositeUtils.getFormGroupConfig(new PmfmuStrategyComposite(), pmfmuStrategyValidator.getFormGroupConfig());
    this.setForm(this.formBuilder.group(formConfig, pmfmuStrategyValidator.getFormGroupOptions(undefined, { multipleEnabled: true })));
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerAutocompleteField('precisionType', {
      ...referentialOptions.precisionType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'PrecisionType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    // Listen for changes
    CompositeUtils.listenCompositeFormChanges(this.form.controls, (subscription) => this.registerSubscription(subscription));
  }

  protected afterInit() {
    this.value = CompositeUtils.build(this.selection, (candidate) => PmfmuStrategyComposite.fromPmfmuStrategy(candidate));
  }

  protected dataToValidate(): PmfmuStrategyComposite {
    // Check form valid
    if (!this.valid) {
      throw new Error(this.formErrorTranslator.translateFormErrors(this.form, { i18nPrefix: this.i18nPrefix }));
    }

    return this.value;
  }
}
