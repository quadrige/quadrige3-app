import { EntityAsObjectOptions, FilterFn } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export class PmfmuStrategyFilterCriteria extends ReferentialFilterCriteria<PmfmuStrategy, number> {
  pmfmuId: number = null;
  parameterFilter: ParameterFilterCriteria = null;
  matrixFilter: IntReferentialFilterCriteria = null;
  fractionFilter: IntReferentialFilterCriteria = null;
  methodFilter: IntReferentialFilterCriteria = null;
  unitFilter: IntReferentialFilterCriteria = null;

  static fromObject(source: any, opts?: any): PmfmuStrategyFilterCriteria {
    const target = new PmfmuStrategyFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.pmfmuId = source.pmfmuId;
    this.parameterFilter = ParameterFilterCriteria.fromObject(source.parameterFilter || {});
    this.matrixFilter = IntReferentialFilterCriteria.fromObject(source.matrixFilter || {});
    this.fractionFilter = IntReferentialFilterCriteria.fromObject(source.fractionFilter || {});
    this.methodFilter = IntReferentialFilterCriteria.fromObject(source.methodFilter || {});
    this.unitFilter = IntReferentialFilterCriteria.fromObject(source.unitFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parameterFilter = this.parameterFilter?.asObject(opts);
    target.matrixFilter = this.matrixFilter?.asObject(opts);
    target.fractionFilter = this.fractionFilter?.asObject(opts);
    target.methodFilter = this.methodFilter?.asObject(opts);
    target.unitFilter = this.unitFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'parameterFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterFilter, true);
    if (key === 'matrixFilter') return !BaseFilterUtils.isCriteriaEmpty(this.matrixFilter, true);
    if (key === 'fractionFilter') return !BaseFilterUtils.isCriteriaEmpty(this.fractionFilter, true);
    if (key === 'methodFilter') return !BaseFilterUtils.isCriteriaEmpty(this.methodFilter, true);
    if (key === 'unitFilter') return !BaseFilterUtils.isCriteriaEmpty(this.unitFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  protected buildFilter(): FilterFn<PmfmuStrategy>[] {
    const target = super.buildFilter();

    if (this.pmfmuId) {
      target.push((data) => data.pmfmu.id === this.pmfmuId);
    }
    if (this.parameterFilter) {
      const filter = this.parameterFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.pmfmu.parameter));
    }
    if (this.matrixFilter) {
      const filter = this.matrixFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.pmfmu.matrix));
    }
    if (this.fractionFilter) {
      const filter = this.fractionFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.pmfmu.fraction));
    }
    if (this.methodFilter) {
      this.methodFilter.searchAttributes = ['id', 'name', 'description'];
      const filter = this.methodFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.pmfmu.method));
    }
    if (this.unitFilter) {
      const filter = this.unitFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.pmfmu.unit));
    }

    return target;
  }
}

export class PmfmuStrategyFilter extends ReferentialFilter<PmfmuStrategyFilter, PmfmuStrategyFilterCriteria, PmfmuStrategy> {
  static fromObject(source: any, opts?: any): PmfmuStrategyFilter {
    const target = new PmfmuStrategyFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): PmfmuStrategyFilterCriteria {
    return PmfmuStrategyFilterCriteria.fromObject(source, opts);
  }
}
