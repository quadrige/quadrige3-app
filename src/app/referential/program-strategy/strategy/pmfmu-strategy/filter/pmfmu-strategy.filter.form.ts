import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import {
  PmfmuStrategyFilter,
  PmfmuStrategyFilterCriteria,
} from '@app/referential/program-strategy/strategy/pmfmu-strategy/filter/pmfmu-strategy.filter.model';
import { isNotNilOrNaN } from '@sumaris-net/ngx-components';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-pmfmu-strategy-filter-form',
  templateUrl: './pmfmu-strategy.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuStrategyFilterForm extends ReferentialCriteriaFormComponent<PmfmuStrategyFilter, PmfmuStrategyFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected criteriaToQueryParams(criteria: PmfmuStrategyFilterCriteria): PartialRecord<keyof PmfmuStrategyFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNilOrNaN(criteria.pmfmuId)) params.pmfmuId = criteria.pmfmuId;
    this.subCriteriaToQueryParams(params, criteria, 'parameterFilter');
    this.subCriteriaToQueryParams(params, criteria, 'matrixFilter');
    this.subCriteriaToQueryParams(params, criteria, 'fractionFilter');
    this.subCriteriaToQueryParams(params, criteria, 'methodFilter');
    this.subCriteriaToQueryParams(params, criteria, 'unitFilter');
    return params;
  }
}
