import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, input, Input, OnInit, viewChild } from '@angular/core';
import { arraySize, isEmptyArray, isNotEmptyArray, isNotNilOrBlank, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { PmfmuStrategyService } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.service';
import { PmfmuStrategyValidatorService } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.validator';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import {
  IPmfmuStrategyMultiEditModalOptions,
  PmfmuStrategyMultiEditModal,
} from '@app/referential/program-strategy/strategy/pmfmu-strategy/multi-edit/pmfmu-strategy.multi-edit.modal';
import { IAddResult } from '@app/shared/model/options.model';
import { ColumnWidthSetting } from '@app/shared/table/table.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { asyncEvery } from '@app/shared/utils';
import { IPmfmuAddOptions } from '@app/referential/pmfmu/pmfmu.add.table';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { PmfmuStrategyComposite } from '@app/referential/program-strategy/strategy/pmfmu-strategy/multi-edit/pmfmu-strategy.multi-edit.model';
import { CompositeUtils } from '@app/shared/model/composite';
import {
  PmfmuStrategyFilter,
  PmfmuStrategyFilterCriteria,
} from '@app/referential/program-strategy/strategy/pmfmu-strategy/filter/pmfmu-strategy.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { PmfmuStrategyFilterForm } from '@app/referential/program-strategy/strategy/pmfmu-strategy/filter/pmfmu-strategy.filter.form';
import { SortDirection } from '@angular/material/sort';

@Component({
  selector: 'app-pmfmu-strategy-table',
  templateUrl: './pmfmu-strategy.table.html',
  standalone: true,
  imports: [ReferentialModule, PmfmuStrategyFilterForm, GenericSelectTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PmfmuStrategyService,
      useExisting: false,
    },
  ],
})
export class PmfmuStrategyTable
  extends ReferentialMemoryTable<PmfmuStrategy, number, PmfmuStrategyFilter, PmfmuStrategyFilterCriteria, PmfmuStrategyValidatorService>
  implements OnInit, AfterViewInit
{
  qualitativeValueTable = viewChild<GenericSelectTable>('qualitativeValueTable');

  @Input() titleI18n = 'REFERENTIAL.STRATEGY.PMFMU';
  canShowQualitativeValues = input<boolean>(true);
  sortable = input<boolean>(false);

  qualitativeValueAreaEnabled = false;

  constructor(
    protected injector: Injector,
    protected _entityService: PmfmuStrategyService,
    protected validatorService: PmfmuStrategyValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'pmfmu.parameter',
        'pmfmu.matrix',
        'pmfmu.fraction',
        'pmfmu.method',
        'pmfmu.unit',
        'acquisitionNumber',
        'individual',
        'uniqueByTaxon',
        'precisionType',
        'survey',
        'samplingOperation',
        'sample',
        'sum',
        'average',
        'deviation',
        'ic95',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      PmfmuStrategy,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.STRATEGY.PMFMU';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.PMFMU_STRATEGY.';
    this.logPrefix = '[pmfmu-strategy-table]';
    this.defaultSortBy = 'rankOrder';
  }

  get rightPanelButtonAccent(): boolean {
    return !this.rightPanelVisible && isNotEmptyArray(this.singleSelectedRow?.currentData?.qualitativeValueIds);
  }

  get sortActive(): string {
    return this.sortable() ? super.sortActive : 'rankOrder';
  }

  get sortDirection(): SortDirection {
    return this.sortable() ? super.sortDirection : 'asc';
  }

  ngOnInit() {
    this.subTable = true;

    super.ngOnInit();

    // precision type combo
    this.registerAutocompleteField('precisionType', {
      ...this.referentialOptions.precisionType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'PrecisionType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Listen sort change
    this.registerSubscription(
      this.onSort.subscribe(() => this.markAsLoading()) // prevent selection change on sort
    );
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    if (this.canShowQualitativeValues()) {
      this.restoreRightPanel(false, 40);
    } else {
      this.rightPanelAllowed = false;
    }
  }

  async onAfterSelectionChange(row?: AsyncTableElement<PmfmuStrategy>) {
    if (this.qualitativeValueTable()) {
      this.registerSubForm(this.qualitativeValueTable());
      await this.loadQualitativeValues(row);
    }
    await super.onAfterSelectionChange(row);
  }

  async addRow(event?: Event): Promise<boolean> {
    await this.openAddModal(event);
    return false;
  }

  async openAddModal(event: Event) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }
    await this.resetFilter();
    await this.save();
    this.selection.clear();

    const { role, data } = await this.modalService.openModal<IPmfmuAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: Pmfmu.entityName,
        titleI18n: 'REFERENTIAL.PMFMU_STRATEGY.ADD_PMFMU',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          excludedIds: this.value.map((value) => value.pmfmu.id),
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      this.markAsDirty();

      // todo if 1 result: add directly and focus on it

      // todo if >1: open multi edit modal here and create rows if validated

      const newValue = this.value?.slice() || [];
      newValue.push(...data.added.map(Pmfmu.fromObject).map((pmfmu) => new PmfmuStrategy(pmfmu)));
      await this.setValue(newValue, { pmfmuIds: data.addedIds });
    }
  }

  async removeSelection(event?: UIEvent, opts?: { interactive?: boolean }) {
    if (event) {
      event.stopPropagation();
    }
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    if (opts?.interactive !== false) {
      // Save before delete
      const saved = await this.saveBeforeAction('delete');
      if (!saved) {
        // Stop if save cancelled or save failed
        return;
      }

      // Check if can delete
      const canDelete = await this.canDeleteRows(toDelete);
      if (!canDelete) return; // Cannot delete
    }

    // remove from selection
    const removedIds = toDelete.map((row) => row.currentData.pmfmu.id);
    const newValue = this.value.slice().filter((pmfmuStrategy) => !removedIds.includes(pmfmuStrategy.pmfmu.id));

    this.markAsDirty();
    await this.setValue(newValue, { resetFilter: false });
    this.onAfterDeletedRows.emit();
  }

  async multiEditRows(event: MouseEvent, newRows?: AsyncTableElement<PmfmuStrategy>[]) {
    const deleteNewRowsIfCancelled = isNotEmptyArray(newRows);
    const rows = newRows || this.selection.selected;
    if (arraySize(rows) < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IPmfmuStrategyMultiEditModalOptions, PmfmuStrategyComposite>(
      PmfmuStrategyMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.PMFMU_STRATEGY.MULTI_EDIT.TITLE',
        i18nPrefix: this.i18nColumnPrefix,
        selection: rows.map((row) => row.currentData),
        defaultReferentialCriteria: this.defaultReferentialCriteria,
      },
      'modal-full-height'
    );

    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // Patch selected rows
      const patch = CompositeUtils.value(data);
      rows.forEach((row) => {
        row.validator.patchValue(patch);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    } else if (deleteNewRowsIfCancelled) {
      // Remove new rows if multiedit cancelled or invalid (Mantis #61276)
      this.selection.setSelection(...newRows);
      await this.removeSelection(event, { interactive: false });
    }
  }

  canGoUp(): boolean {
    return this.filterIsEmpty && (this.minSelectedRowId() > 0 || false);
  }

  canGoDown(): boolean {
    return this.filterIsEmpty && (this.maxSelectedRowId() < this.visibleRowCount - 1 || false);
  }

  async goUp() {
    if (await asyncEvery(this.selection.selected, async (row) => this.confirmEditCreate(undefined, row))) {
      for (const row of this.selection.selected
        .slice()
        // ensure forward order
        .sort((row1, row2) => row1.id - row2.id)) {
        await this.moveRow(row.id, -1);
        this.markAsDirty();
      }
    }
  }

  async goDown() {
    if (await asyncEvery(this.selection.selected, async (row) => this.confirmEditCreate(undefined, row))) {
      for (const row of this.selection.selected
        .slice()
        // ensure reverse order
        .sort((row1, row2) => row2.id - row1.id)) {
        await this.moveRow(row.id, 1);
        this.markAsDirty();
      }
    }
  }

  qualitativeValueSelectionChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow('qualitativeValueIds', values);
    }
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    // Get errors
    const error = this.getRowError();
    if (isNotNilOrBlank(error)) {
      this.markAsError(error);
      return false;
    }

    // Calculate rank order
    this.getRows().forEach((row, index) => row.validator.patchValue({ rankOrder: index }));

    return super.save(opts);
  }

  async setValue(value: PmfmuStrategy[], opts?: { pmfmuIds?: number[]; resetFilter?: boolean; emitEvent?: boolean }): Promise<void> {
    // clear selection
    this.selection.clear();
    if ((!opts || opts.resetFilter !== false) && !this.filterIsEmpty) {
      await this.resetFilter(undefined, { emitEvent: false });
    }
    await super.setValue(value, opts);
    if (isNotEmptyArray(opts?.pmfmuIds)) {
      const selectedRows = this.getRows().filter((row) => opts.pmfmuIds.includes(row.currentData.pmfmu.id));
      // Force dirty state
      selectedRows.forEach((row) => this.markRowAsDirty(row));
      if (selectedRows.length > 1) {
        // Open multi-edit modal (Mantis #56708)
        await this.multiEditRows(undefined, selectedRows);
      } else if (selectedRows.length === 1) {
        this.focusRow(selectedRows[0]);
        await this.selectRowById(selectedRows[0].id);
      }
    }
  }

  // managing qualitative values

  async loadQualitativeValues(row?: AsyncTableElement<PmfmuStrategy>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.qualitativeValueTable().setSelectedIds(undefined);
      this.qualitativeValueAreaEnabled = false;
      return;
    }
    // Load selected qualitative values
    this.qualitativeValueTable().addCriteria = { parentId: row.currentData.pmfmu?.id.toString() };
    await this.qualitativeValueTable().setSelectedIds((row.currentData?.qualitativeValueIds || []).slice());
    this.qualitativeValueAreaEnabled = this.canEdit && row.currentData?.pmfmu?.parameter?.qualitative;
  }

  // protected methods

  protected getColumnWidthSettings(): ColumnWidthSetting[] {
    // remap old column name (from 'parameter' to 'pmfmu.parameter', etc...)
    // todo: this code can be removed in future release > 1.0.6 deployed
    return super.getColumnWidthSettings().map((setting) => {
      if (['parameter', 'matrix', 'fraction', 'method', 'unit'].includes(setting.column)) {
        return {
          ...setting,
          column: 'pmfmu.' + setting.column,
        };
      }
      return setting;
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('acquisitionNumber');
  }
}
