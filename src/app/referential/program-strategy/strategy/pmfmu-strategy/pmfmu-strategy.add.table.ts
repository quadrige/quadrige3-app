import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { PmfmuStrategyService } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.service';
import { PmfmuStrategyValidatorService } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.validator';
import { PmfmuStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.table';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { PmfmuStrategyFilter } from '@app/referential/program-strategy/strategy/pmfmu-strategy/filter/pmfmu-strategy.filter.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { PmfmuStrategyFilterForm } from '@app/referential/program-strategy/strategy/pmfmu-strategy/filter/pmfmu-strategy.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';

@Component({
  selector: 'app-pmfmu-strategy-add-table',
  templateUrl: './pmfmu-strategy.table.html',
  standalone: true,
  imports: [ReferentialModule, PmfmuStrategyFilterForm, GenericSelectTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PmfmuStrategyService,
      useExisting: false,
    },
  ],
})
export class PmfmuStrategyAddTable extends PmfmuStrategyTable implements IAddTable<PmfmuStrategy> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: PmfmuStrategyService,
    protected validatorService: PmfmuStrategyValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[pmfmu-strategy-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: PmfmuStrategyFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
