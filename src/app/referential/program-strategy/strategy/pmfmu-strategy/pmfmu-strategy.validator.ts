import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { SharedValidators, toNumber } from '@sumaris-net/ngx-components';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { multipleAttributeSuffix } from '@app/shared/model/composite';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

export interface PmfmuStrategyValidatorOptions extends ReferentialValidatorOptions {
  multipleEnabled?: boolean;
}

@Injectable({ providedIn: 'root' })
export class PmfmuStrategyValidatorService extends ReferentialValidatorService<PmfmuStrategy, number, PmfmuStrategyValidatorOptions> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: PmfmuStrategy, opts?: PmfmuStrategyValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      rankOrder: [data?.rankOrder || null],
      acquisitionNumber: [
        toNumber(data?.acquisitionNumber, null),
        Validators.compose([Validators.required, SharedValidators.integer, Validators.min(1)]),
      ],
      individual: [data?.individual || false],
      uniqueByTaxon: [data?.uniqueByTaxon || false],
      precisionType: [data?.precisionType || null, SharedValidators.entity],
      pmfmu: [data?.pmfmu || null, Validators.compose([Validators.required, SharedValidators.entity])],
      survey: [data?.survey || false],
      samplingOperation: [data?.samplingOperation || false],
      sample: [data?.sample || false],
      sum: [data?.sum || false],
      average: [data?.average || false],
      deviation: [data?.deviation || false],
      ic95: [data?.ic95 || false],
      qualitativeValueIds: [data?.qualitativeValueIds || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: PmfmuStrategy, opts?: PmfmuStrategyValidatorOptions): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredAnyOf(
          opts?.multipleEnabled
            ? [
                'survey',
                'survey' + multipleAttributeSuffix,
                'samplingOperation',
                'samplingOperation' + multipleAttributeSuffix,
                'sample',
                'sample' + multipleAttributeSuffix,
              ]
            : ['survey', 'samplingOperation', 'sample'],
          true,
          'REFERENTIAL.PMFMU_STRATEGY.ERROR.MISSING_ACQUISITION_LEVEL'
        ),
      ],
    };
  }
}
