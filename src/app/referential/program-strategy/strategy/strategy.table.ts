import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, OnInit, viewChild } from '@angular/core';
import {
  Alerts,
  isEmptyArray,
  isNilOrBlank,
  isNotEmptyArray,
  PromiseEvent,
  PropertyMap,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  toBoolean,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { StrategyValidatorService } from '@app/referential/program-strategy/strategy/strategy.validator';
import { AppliedStrategyTable } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.table';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { UntypedFormGroup } from '@angular/forms';
import { PmfmuStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.table';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { PmfmuAppliedStrategyTable } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.table';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { PmfmuStrategy } from '@app/referential/program-strategy/strategy/pmfmu-strategy/pmfmu-strategy.model';
import { Dates } from '@app/shared/dates';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { lengthComment, ReferentialValidatorOptions } from '@app/referential/service/referential-validator.service';
import { MatTabNav } from '@angular/material/tabs';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import {
  IStrategyDuplicateModalOptions,
  StrategyDuplicateModal,
} from '@app/referential/program-strategy/strategy/duplicate/strategy.duplicate.modal';
import { DateFilter } from '@app/shared/model/date-filter';
import { entityId, EntityUtils } from '@app/shared/entity.utils';
import { IStrategyHistoryModalOptions, StrategyHistoryModal } from '@app/referential/program-strategy/strategy/history/strategy-history.modal';
import { IAddResult, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { IntReferential } from '@app/referential/model/referential.model';
import { StrategyFilter, StrategyFilterCriteria } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { accountConfigOptions } from '@app/core/config/account.config';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { ExportOptions, IRightSelectTable } from '@app/shared/table/table.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

type StrategyView = 'userTable' | 'departmentTable';
type StrategyDetailView = 'appliedStrategyTable' | 'pmfmuStrategyTable' | 'pmfmuAppliedStrategyTable';

export interface CheckOverlappingPeriodsOptions {
  monitoringLocationIds: number[];
  dateFilters: DateFilter[];
  programId?: string;
  fromStrategyDuplication?: boolean;
}

export interface CheckPeriodsToDeleteOptions {
  rows: AsyncTableElement<AppliedPeriod>[];
  monitoringLocationId?: number;
}

@Component({
  selector: 'app-strategy-table',
  templateUrl: './strategy.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyTable
  extends ReferentialTable<Strategy, number, StrategyFilter, StrategyFilterCriteria, StrategyValidatorService, StrategyView>
  implements OnInit, AfterViewInit
{
  userTable = viewChild<UserSelectTable>('userTable');
  departmentTable = viewChild<DepartmentSelectTable>('departmentTable');
  detailAreaTabs = viewChild<MatTabNav>('detailAreaTabs');
  appliedStrategyTable = viewChild<AppliedStrategyTable>('appliedStrategyTable');
  pmfmuStrategyTable = viewChild<PmfmuStrategyTable>('pmfmuStrategyTable');
  pmfmuAppliedStrategyTable = viewChild<PmfmuAppliedStrategyTable>('pmfmuAppliedStrategyTable');

  readonly responsibleMenuItems: IEntityMenuItem<Strategy, StrategyView>[] = [
    {
      title: 'REFERENTIAL.STRATEGY.RESPONSIBLE.TITLE',
      children: [
        {
          title: 'REFERENTIAL.ENTITY.USERS',
          entityName: 'User',
          attribute: 'responsibleUserIds',
          view: 'userTable',
          viewTitle: 'REFERENTIAL.STRATEGY.RESPONSIBLE.USER',
        },
        {
          title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
          entityName: 'Department',
          attribute: 'responsibleDepartmentIds',
          view: 'departmentTable',
          viewTitle: 'REFERENTIAL.STRATEGY.RESPONSIBLE.DEPARTMENT',
        },
      ],
    },
  ];

  responsibleTableEnabled = false;

  readonly appliedStrategyMenuItem: IEntityMenuItem<Strategy, StrategyDetailView> = {
    title: 'REFERENTIAL.STRATEGY.MONITORING_LOCATION',
    attribute: 'appliedStrategies',
    view: 'appliedStrategyTable',
  };
  readonly pmfmuStrategyMenuItem: IEntityMenuItem<Strategy, StrategyDetailView> = {
    title: 'REFERENTIAL.STRATEGY.PMFMU',
    attribute: 'pmfmuStrategies',
    view: 'pmfmuStrategyTable',
  };
  readonly pmfmuAppliedStrategyMenuItem: IEntityMenuItem<Strategy, StrategyDetailView> = {
    title: 'REFERENTIAL.STRATEGY.PMFMU_ASSOCIATION',
    attribute: 'pmfmuAppliedStrategies',
    view: 'pmfmuAppliedStrategyTable',
  };
  readonly detailAreaMenuItems: IEntityMenuItem<Strategy, StrategyDetailView>[] = [
    this.appliedStrategyMenuItem,
    this.pmfmuStrategyMenuItem,
    this.pmfmuAppliedStrategyMenuItem,
  ];

  selectedDetailAreaMenuItem: IEntityMenuItem<Strategy, StrategyDetailView> = this.detailAreaMenuItems[0];

  detailAreaEnabled = false;
  detailAreaLoading = false;

  defaultBackHref = '/management/Program';
  descriptionMaxLength = lengthComment;

  private _programId: string;
  private _program: Program;
  private _checkOverlappingPeriodsEventEmitter = new EventEmitter();

  constructor(
    protected injector: Injector,
    protected _entityService: StrategyService,
    protected validatorService: StrategyValidatorService,
    protected programService: ProgramService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      Strategy,
      _entityService,
      validatorService
    );

    // Get program id from route
    // this._programId = this.route.snapshot.params.programId;
    this.listenToQueryParams = false;

    // register custom error message
    const customErrors: PropertyMap = {};
    customErrors[referentialErrorCodes.deleteForbidden] = 'REFERENTIAL.STRATEGY.ERROR.ATTACHED_DATA';
    customErrors[referentialErrorCodes.attachedData] = 'REFERENTIAL.STRATEGY.ERROR.ATTACHED_DATA';
    this.registerCustomErrors(customErrors);

    this.i18nColumnPrefix = 'REFERENTIAL.STRATEGY.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[strategy-table]';
    this.setShowColumn('statusId', false);
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: Strategy = this.singleSelectedRow.currentData;
      return isNotEmptyArray(data.responsibleDepartmentIds) || isNotEmptyArray(data.responsibleUserIds);
    }
    return false;
  }

  get programId(): string {
    return this._programId;
  }

  ngOnInit() {
    super.ngOnInit();

    {
      // todo send this to appliedStrategyTable via binding
      // department combo
      this.registerAutocompleteField('department', {
        ...this.referentialOptions.department,
        service: this.referentialGenericService,
        filter: GenericReferentialFilter.fromObject({
          entityName: 'Department',
          criterias: [this.defaultReferentialCriteria],
        }),
      });

      // frequency combo
      this.registerAutocompleteField('frequency', {
        ...this.referentialOptions.frequency,
        service: this.referentialGenericService,
        filter: GenericReferentialFilter.fromObject({
          entityName: 'Frequency',
          criterias: [this.defaultReferentialCriteria],
        }),
      });

      // taxon group combo
      this.registerAutocompleteField('taxonGroup', {
        ...this.referentialOptions.taxonGroup,
        service: this.referentialGenericService,
        filter: GenericReferentialFilter.fromObject({
          entityName: 'TaxonGroup',
          criterias: [this.defaultReferentialCriteria],
        }),
      });

      // reference taxon combo
      this.registerAutocompleteField('referenceTaxon', {
        ...this.referentialOptions.referenceTaxon,
        service: this.referentialGenericService,
        filter: GenericReferentialFilter.fromObject({
          entityName: 'ReferenceTaxon',
          criterias: [this.defaultReferentialCriteria],
        }),
      });
    }

    if (!this.subTable) {
      this.registerSubscription(this.onBeforeDeleteRows.subscribe((event) => this.checkCanDeleteStrategies(event)));
    }

    this.registerSubscription(
      this.route.paramMap.subscribe((paramMap) => {
        this._programId = paramMap.get('programId');
        // Load program
        this.loadProgram();
      })
    );
  }

  protected async loadProgram() {
    if (this.programId) {
      this._program = await this.programService.load(this.programId);
      this.updatePermission();
      this.updateTitle();

      // Apply filter
      const restored = await this.restoreLastFilter();
      await this.setFilter(this.filterFormComponent().value, { updateQueryParams: restored, firstTime: !restored, emitEvent: true });

      // Some post load actions, only for standard table
      if (!this.subTable) {
        await this.dataSource.waitIdle();

        // Check if a strategy is waiting for duplication
        const strategyToDuplicate = this._entityService.strategyToDuplicate;
        if (strategyToDuplicate) {
          await this.addStrategy(strategyToDuplicate);
          return;
        }

        // Try to select the first active strategy (Mantis #56782)
        const mostRecent = this.getRows().find((row) => row.currentData.mostRecent);
        if (mostRecent) {
          this.focusRow(mostRecent);
          await this.selectRowById(mostRecent.id);
        } else {
          this.selection.clear();
        }
      }
    } else {
      this.inlineEdition = false;
      console.warn(`${this.logPrefix} No 'programId' provided.`);
    }
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.registerSubscription(this._checkOverlappingPeriodsEventEmitter.subscribe((promiseEvent) => this.checkOverlappingPeriods(promiseEvent)));

    this.restoreBottomPanel(true, 50);

    this.markForCheck();
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(true, 40);
  }

  async onAfterSelectionChange(row?: AsyncTableElement<Strategy>) {
    this.refreshButtons();
    await super.onAfterSelectionChange(row);
    await this.loadDetailArea(row);
  }

  updatePermission() {
    // Manage user rights :
    // By default, all user have edition write to allow further finer checks
    this.canEdit = true;
    // Refresh other controls
    this.refreshButtons();
    this.refreshTabs();

    this.markForCheck();
  }

  asFilter(source?: any): StrategyFilter {
    const filter = super.asFilter(source);
    // Force parentId
    filter.patch({ parentId: this.programId });
    return filter;
  }

  async resetFilter(filter?: StrategyFilter, opts?: ISetFilterOptions) {
    filter = this.asFilter(filter);
    filter.patch({ onlyActive: false });
    await super.resetFilter(filter, opts);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<Strategy>): Promise<boolean> {
    row = row || this.singleEditingRow;

    // confirm current detail table row
    if (!(await this.confirmDetail(row))) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  async confirmDetail(row?: AsyncTableElement<Strategy>): Promise<boolean> {
    if (row) {
      if (this.appliedStrategyTable().dirty) {
        if (await this.appliedStrategyTable().save()) {
          await this.patchAppliedStrategies(this.appliedStrategyTable().value, row);
        } else {
          this.selectedDetailAreaMenuItem = this.appliedStrategyMenuItem;
          this.refreshTabs();
          return false;
        }
      }

      if (this.pmfmuStrategyTable().dirty) {
        if (await this.pmfmuStrategyTable().save()) {
          await this.patchPmfmuStrategies(this.pmfmuStrategyTable().value, row);
        } else {
          this.selectedDetailAreaMenuItem = this.pmfmuStrategyMenuItem;
          this.refreshTabs();
          return false;
        }
      }

      if (this.pmfmuAppliedStrategyTable().dirty) {
        if (await this.pmfmuAppliedStrategyTable().save()) {
          this.patchPmfmuAppliedStrategies(this.pmfmuAppliedStrategyTable().value, row);
        } else {
          this.selectedDetailAreaMenuItem = this.pmfmuAppliedStrategyMenuItem;
          this.refreshTabs();
          return false;
        }
      }
    }
    return true;
  }

  async patchAppliedStrategies(values: AppliedStrategy[], row?: AsyncTableElement<Strategy>, cleanPmfmuAppliedStrategies?: boolean) {
    if (this.canEdit) {
      this.patchRow(this.appliedStrategyMenuItem.attribute, values, row);
      this.pmfmuAppliedStrategyTable().appliedStrategies = values;
      if (cleanPmfmuAppliedStrategies !== false) {
        await this.pmfmuAppliedStrategyTable().cleanOrphanRows();
      }
    }
  }

  async patchPmfmuStrategies(values: PmfmuStrategy[], row?: AsyncTableElement<Strategy>, cleanPmfmuAppliedStrategies?: boolean) {
    if (this.canEdit) {
      this.patchRow(this.pmfmuStrategyMenuItem.attribute, values, row);
      this.pmfmuAppliedStrategyTable().pmfmuStrategies = values;
      if (cleanPmfmuAppliedStrategies !== false) {
        await this.pmfmuAppliedStrategyTable().cleanOrphanRows();
      }
    }
  }

  patchPmfmuAppliedStrategies(values: PmfmuAppliedStrategy[], row?: AsyncTableElement<Strategy>) {
    if (this.canEdit) {
      this.patchRow(this.pmfmuAppliedStrategyMenuItem.attribute, values, row);
    }
  }

  async detailAreaTabChange(event: MouseEvent, detailTab: IEntityMenuItem<Strategy, StrategyDetailView>) {
    if (!detailTab) {
      throw new Error(`Cannot determinate the type of bottom area`);
    }
    if (this.selectedDetailAreaMenuItem !== detailTab) {
      if (!(await this.confirmDetail(this.singleEditingRow))) {
        return;
      }
    }
    this.selectedDetailAreaMenuItem = detailTab;

    if (this.selectedDetailAreaMenuItem === this.pmfmuAppliedStrategyMenuItem) {
      // Force a change detection to read paginator height in rightSplitResized()
      this.detectChanges();
      this.pmfmuAppliedStrategyTable()?.rightSplitResized();

      // Load pmfmu applied strategies when entering the tab
      if (!!this.singleSelectedRow && !this.singleSelectedRow.currentData.pmfmuAppliedStrategiesLoaded) {
        await this.loadDetailArea(this.singleSelectedRow);
      }
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { ...opts, transcribingItemEnabled: true, forceAllColumns: true });
  }

  protected getRightMenuItems(): IEntityMenuItem<Strategy, StrategyView>[] {
    return this.responsibleMenuItems;
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Strategy, StrategyView> {
    return this.responsibleMenuItems[0].children[0];
  }

  responsibleTableSelectionChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow(this.rightMenuItem.attribute, values);
    }
  }

  async checkCanDeleteStrategies(event: PromiseEvent<boolean, { rows: AsyncTableElement<Strategy>[] }>) {
    const ids: number[] = EntityUtils.ids(event.detail.rows);
    let canDelete = true;
    if (isNotEmptyArray(ids)) {
      canDelete = await this._entityService.canDeleteStrategies(ids);
    }
    event.detail.success(canDelete);
    if (!canDelete) {
      await Alerts.showError('REFERENTIAL.STRATEGY.ERROR.CANNOT_DELETE', this.alertCtrl, this.translate, {
        titleKey: 'ERROR.CANNOT_DELETE',
      });
    }
  }

  async checkCanDeleteAppliedStrategy(event: PromiseEvent<boolean, { rows: AsyncTableElement<AppliedStrategy>[] }>) {
    const appliedStrategyIds: number[] = EntityUtils.ids(event.detail.rows);
    let canDelete = true;
    if (isNotEmptyArray(appliedStrategyIds)) {
      canDelete = await this._entityService.canDeleteAppliedStrategies(appliedStrategyIds);
    }
    event.detail.success(canDelete);
    if (!canDelete) {
      await Alerts.showError('REFERENTIAL.APPLIED_STRATEGY.ERROR.CANNOT_DELETE', this.alertCtrl, this.translate, {
        titleKey: 'ERROR.CANNOT_DELETE',
      });
    }
  }

  /**
   * Check if there is no overlapping periods for provided monitoring locations
   */
  async checkOverlappingPeriods(event: PromiseEvent<boolean, CheckOverlappingPeriodsOptions>) {
    // event will success if no overlapping period found in tables or in database
    event.detail.success(!(await this.hasOverlappingPeriods(event.detail)));
  }

  async hasOverlappingPeriods(opts: CheckOverlappingPeriodsOptions): Promise<boolean> {
    const excludeStrategyIds = [];
    const currentStrategy = this.singleSelectedRow?.currentData;
    const sameProgram = isNilOrBlank(opts.programId) || opts.programId === currentStrategy.programId || opts.programId === this.programId;
    if (!!currentStrategy.id && !toBoolean(opts.fromStrategyDuplication, false) && sameProgram) {
      excludeStrategyIds.push(currentStrategy.id);
    }

    let hasOverlappingPeriods = false;

    // Check in current table
    if (sameProgram) {
      this.getRows().forEach((row) => {
        if (
          !hasOverlappingPeriods && // not already found
          row.currentData.appliedStrategiesLoaded && // applied strategies are loaded
          !(row.currentData.id === currentStrategy.id || row.currentData.name === currentStrategy.name)
        ) {
          // not current strategy
          // exclude this strategy
          if (!!row.currentData.id) {
            excludeStrategyIds.push(row.currentData.id);
          }
          // get applied strategies for provided monitoring locations
          const appliedStrategies =
            row.currentData.appliedStrategies?.filter(
              (appliedStrategy) =>
                opts.monitoringLocationIds.map((id) => id.toString()).includes(appliedStrategy.monitoringLocation.id.toString()) &&
                isNotEmptyArray(appliedStrategy.appliedPeriods)
            ) || [];
          // find at least one overlapping period
          hasOverlappingPeriods = appliedStrategies.some((appliedStrategy) =>
            appliedStrategy.appliedPeriods.some((appliedPeriod) =>
              opts.dateFilters.some((dateFilter) =>
                Dates.overlaps(
                  { start: dateFilter.startLowerBound, end: dateFilter.endUpperBound },
                  { start: appliedPeriod.startDate, end: appliedPeriod.endDate }
                )
              )
            )
          );
        }
      });
    }

    // Check via service
    if (!hasOverlappingPeriods || !sameProgram) {
      hasOverlappingPeriods = await this._entityService.hasOverlappingPeriods(
        sameProgram ? this.programId : opts.programId,
        opts.monitoringLocationIds,
        opts.dateFilters,
        excludeStrategyIds
      );
    }

    return hasOverlappingPeriods;
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<Strategy>): Promise<boolean> {
    // following lines comes from super.editRow(event, row), don't remove
    if (!this._enabled) return false;
    if (this.singleEditingRow === row) return true; // Already the edited row
    if (event?.defaultPrevented) return false;
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // User without right to edit, don't turn on row edition
    if (!this.hasRightOnStrategy(row.currentData)) {
      return true;
    }

    return super.editRow(event, row);
  }

  async duplicateStrategy(event: MouseEvent) {
    event?.stopPropagation();
    if (this.selection.selected.length !== 1) return;

    const row = this.singleSelectedRow;
    if (!row || !(await this.confirmEditCreate(event, row))) return false;

    // Be sure to load all data
    this.markAsLoading();
    await this.loadAppliedStrategies(row);
    await this.loadPmfmuStrategies(row);
    await this.loadPmfmuAppliedStrategies(row);
    await this.loadDetailArea(row);
    this.markAsLoaded();

    // Open select program modal
    const { role, data } = await this.modalService.openModal<IStrategyDuplicateModalOptions, Strategy>(
      StrategyDuplicateModal,
      {
        originStrategy: row.currentData,
        programAddCriteria: { managedOnly: true },
        programAddFirstCriteria: {
          statusId: this.statusIds.ENABLE,
          searchText: this.hasRightOnProgram() ? this.programId : undefined,
        },
        defaultReferentialCriteria: this.defaultReferentialCriteria,
        departmentConfig: this.autocompleteFields.department,
        frequencyConfig: this.autocompleteFields.frequency,
        taxonGroupConfig: this.autocompleteFields.taxonGroup,
        referenceTaxonConfig: this.autocompleteFields.referenceTaxon,
        checkOverlappingPeriodsEventEmitter: this._checkOverlappingPeriodsEventEmitter,
      },
      'modal-large'
    );
    if (role === 'validate' && data?.programId) {
      if (data.programId === this.programId) {
        // duplicate current row on same program
        await this.addStrategy(data, row.id + 1);
      } else {
        // Keep the selected row, open the other program strategy page and duplicate in it
        this._entityService.strategyToDuplicate = data;
        // Go to strategies table of selected program
        await this.router.navigate(['../..', data.programId, 'strategy'], {
          relativeTo: this.route,
        });
      }
    }
  }

  async openStrategiesHistoryModal(monitoringLocation: IntReferential) {
    if (this.selection.selected.length > 1) {
      console.warn(`${this.logPrefix} More than one row selected.`);
      return;
    }
    const selectedId = this.singleSelectedRow?.id;
    if (this.dirty) {
      const confirmed = await Alerts.askConfirmation('CONFIRM.SAVE', this.alertCtrl, this.translate);
      if (!confirmed) {
        return;
      }
      if (!(await this.save())) {
        return;
      }
    }

    // Open modal
    const { role, data } = await this.modalService.openModal<IStrategyHistoryModalOptions, boolean>(
      StrategyHistoryModal,
      {
        programId: this.programId,
        monitoringLocation,
        checkOverlappingPeriodsEventEmitter: this._checkOverlappingPeriodsEventEmitter,
      },
      'modal-large'
    );
    if (role === 'validate' && data) {
      // If history saved, reload all
      await this.cancel(undefined, { selectPreviousSelectedRow: false });
      await this.selectRowById(selectedId);
    }
  }

  detailCount(detailTab: IEntityMenuItem<Strategy, StrategyDetailView>) {
    switch (detailTab?.view) {
      case 'appliedStrategyTable':
        return this.appliedStrategyTable()?.totalRowCount || undefined;
      case 'pmfmuStrategyTable':
        return this.pmfmuStrategyTable()?.totalRowCount || undefined;
      case 'pmfmuAppliedStrategyTable':
        return this.pmfmuAppliedStrategyTable()?.totalRowCount || undefined;
      default:
        return undefined;
    }
  }

  // protected methods

  protected getFilterSettingId(): string {
    return super.getFilterSettingId() + (this.programId ? `_${this.programId}` : '');
  }

  protected onStartEditRow(row: AsyncTableElement<Strategy>) {
    super.onStartEditRow(row);

    // add manager department and user from program on new row
    if (row.id === -1 && (isEmptyArray(row.currentData.responsibleDepartmentIds) || isEmptyArray(row.currentData.responsibleUserIds))) {
      row.validator.patchValue({ responsibleDepartmentIds: this._program.managerDepartmentIds, responsibleUserIds: this._program.managerUserIds });
    }
  }

  protected async loadRightArea(row?: AsyncTableElement<Strategy>) {
    if (this.subTable) return; // don't load if sub table
    switch (this.rightMenuItem?.view) {
      case 'userTable': {
        this.detectChanges();
        if (this.userTable()) {
          this.registerSubForm(this.userTable());
          await this.loadUserTable(row);
        }
        break;
      }
      case 'departmentTable': {
        this.detectChanges();
        if (this.departmentTable()) {
          this.registerSubForm(this.departmentTable());
          await this.loadDepartmentTable(row);
        }
        break;
      }
    }
    await super.loadRightArea(row);
  }

  protected async loadUserTable(row?: AsyncTableElement<Strategy>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.userTable(), row);
      await this.userTable().setSelectedIds(undefined);
      this.responsibleTableEnabled = false;
      return;
    }

    // Load selected users
    this.applyRightOptions(this.userTable(), row);
    await this.userTable().setSelectedIds((row.currentData?.responsibleUserIds || []).slice());
    this.responsibleTableEnabled = this.hasRightOnStrategy(row?.currentData);
  }

  protected async addUser(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'User',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.userTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.userTable());
      await this.userTable().setSelectedIds(includedIds);
      this.responsibleTableSelectionChanged(includedIds);
    }
  }

  protected async loadDepartmentTable(row?: AsyncTableElement<Strategy>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.departmentTable(), row);
      await this.departmentTable().setSelectedIds(undefined);
      this.responsibleTableEnabled = false;
      return;
    }

    // Load selected departments
    this.applyRightOptions(this.departmentTable(), row);
    await this.departmentTable().setSelectedIds((row.currentData?.responsibleDepartmentIds || []).slice());
    this.responsibleTableEnabled = this.hasRightOnStrategy(row?.currentData);
  }

  protected async addDepartment(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'Department',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.departmentTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.departmentTable());
      await this.departmentTable().setSelectedIds(includedIds);
      this.responsibleTableSelectionChanged(includedIds);
    }
  }

  protected async loadDetailArea(row?: AsyncTableElement<Strategy>) {
    if (this.subTable) return; // don't load if sub table
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.detailAreaEnabled = false;
      return;
    }

    // Load details data
    await this.loadDetailData(row);
    this.registerSubForm(this.appliedStrategyTable());
    this.registerSubForm(this.pmfmuStrategyTable());
    this.registerSubForm(this.pmfmuAppliedStrategyTable());

    this.appliedStrategyTable().markAsLoading();
    this.pmfmuStrategyTable().markAsLoading();
    this.pmfmuAppliedStrategyTable().markAsLoading();
    this.markForCheck();

    // Load selected data
    await this.appliedStrategyTable().setValue((row.currentData?.appliedStrategies || []).slice(), { resetFilter: false });
    await this.pmfmuStrategyTable().setValue((row.currentData?.pmfmuStrategies || []).slice(), { resetFilter: false });
    await this.pmfmuAppliedStrategyTable().setValue((row.currentData?.pmfmuAppliedStrategies || []).slice(), { resetFilter: false });
    this.pmfmuAppliedStrategyTable().appliedStrategies = (row.currentData?.appliedStrategies || []).slice();
    this.pmfmuAppliedStrategyTable().pmfmuStrategies = (row.currentData?.pmfmuStrategies || []).slice();
    this.detailAreaEnabled = this.hasRightOnStrategy(row.currentData);

    this.appliedStrategyTable().markAsLoaded();
    this.pmfmuStrategyTable().markAsLoaded();
    this.pmfmuAppliedStrategyTable().markAsLoaded();

    // Listen to applied strategies and pmfmu filters to propagate to pmfmu applied strategies filter
    if (!this.appliedStrategyTable().afterFilterSet.observed) {
      this.registerSubscription(
        this.appliedStrategyTable().afterFilterSet.subscribe((filter) => {
          this.pmfmuAppliedStrategyTable().filterFormComponent().patchCriterias({
            monitoringLocationFilter: filter?.criterias?.[0]?.monitoringLocationFilter,
          });
        })
      );
    }
    if (!this.pmfmuStrategyTable().afterFilterSet.observed) {
      this.registerSubscription(
        this.pmfmuStrategyTable().afterFilterSet.subscribe((filter) => {
          this.pmfmuAppliedStrategyTable().filterFormComponent().patchCriterias({
            parameterFilter: filter?.criterias?.[0]?.parameterFilter,
          });
        })
      );
    }
  }

  protected async loadDetailData(row: AsyncTableElement<Strategy>) {
    try {
      this.detailAreaLoading = true;
      await this.loadAppliedStrategies(row);
      await this.loadPmfmuStrategies(row);
      if (!!row && this.selectedDetailAreaMenuItem === this.pmfmuAppliedStrategyMenuItem) {
        await this.loadPmfmuAppliedStrategies(row);
      }
    } finally {
      this.detailAreaLoading = false;
    }
  }

  protected async loadAppliedStrategies(row: AsyncTableElement<Strategy>) {
    // Call service if detail data is not loaded yet
    if (!row.currentData.appliedStrategiesLoaded) {
      this.appliedStrategyTable().markAsLoading();

      // Get applied strategies
      const appliedStrategies = await this._entityService.loadAppliedStrategies(row.currentData.id);

      row.validator.patchValue({ appliedStrategies, appliedStrategiesLoaded: true });
    }
  }

  protected async loadPmfmuStrategies(row: AsyncTableElement<Strategy>) {
    // Call service if detail data is not loaded yet
    if (!row.currentData.pmfmuStrategiesLoaded) {
      this.pmfmuStrategyTable().markAsLoading();

      // Get pmfmu strategies
      const pmfmuStrategies = await this._entityService.loadPmfmuStrategies(row.currentData.id);

      row.validator.patchValue({ pmfmuStrategies, pmfmuStrategiesLoaded: true });
    }
  }

  protected async loadPmfmuAppliedStrategies(row: AsyncTableElement<Strategy>) {
    // Call service if detail data is not loaded yet
    if (!row.currentData.pmfmuAppliedStrategiesLoaded) {
      this.pmfmuAppliedStrategyTable().markAsLoading();
      await this.loadAppliedStrategies(row);
      await this.loadPmfmuStrategies(row);

      // Get pmfmu applied strategies
      const pmfmuAppliedStrategies: PmfmuAppliedStrategy[] = await this._entityService.loadPmfmuAppliedStrategies(
        row.currentData.appliedStrategies?.map(entityId)
      );

      // Affect applied strategy and pmfmu strategy
      const appliedStrategiesMap = row.currentData.appliedStrategies?.reduce(
        (map, appliedStrategy) => ({ ...map, [appliedStrategy.id]: appliedStrategy }),
        {}
      );
      const pmfmuStrategiesMap = row.currentData.pmfmuStrategies?.reduce((map, pmfmuStrategy) => ({ ...map, [pmfmuStrategy.id]: pmfmuStrategy }), {});
      pmfmuAppliedStrategies.forEach((pmfmuAppliedStrategy) => {
        pmfmuAppliedStrategy.appliedStrategy = appliedStrategiesMap[pmfmuAppliedStrategy.appliedStrategyId];
        pmfmuAppliedStrategy.pmfmuStrategy = pmfmuStrategiesMap[pmfmuAppliedStrategy.pmfmuStrategyId];
      });

      row.validator.patchValue({ pmfmuAppliedStrategies, pmfmuAppliedStrategiesLoaded: true });
    }
  }

  protected getRowValidator(row?: AsyncTableElement<Strategy>): UntypedFormGroup {
    return super.getRowValidator(row, <ReferentialValidatorOptions>{
      criteria: { parentId: this.programId },
    });
  }

  protected async defaultNewRowValue() {
    return {
      ...(await super.defaultNewRowValue()),
      programId: this.programId,
      // Consider all sub entities loaded
      appliedStrategiesLoaded: true,
      pmfmuStrategiesLoaded: true,
      pmfmuAppliedStrategiesLoaded: true,
    };
  }

  protected hasRightOnProgram(): boolean {
    if (!this._program) {
      return false;
    }

    // Admins or users with manager right
    return (
      this.accountService.isAdmin() ||
      (this.accountService.isUser() &&
        (this._program.managerUserIds.includes(this.accountService.person.id) ||
          this._program.managerDepartmentIds.includes(this.accountService.department.id)))
    );
  }

  protected hasRightOnStrategy(strategy: Strategy): boolean {
    if (!strategy) {
      return false;
    }

    // Admins or program managers or users with responsible right
    return (
      this.accountService.isAdmin() ||
      this.hasRightOnProgram() ||
      strategy.responsibleUserIds.includes(this.accountService.person.id) ||
      strategy.responsibleDepartmentIds.includes(this.accountService.department.id)
    );
  }

  /**
   * Refresh buttons state according to user profile or program rights
   *
   * @protected
   */
  protected refreshButtons() {
    // Only admins or program managers can add strategies
    const hasRightOnProgram = this.hasRightOnProgram();
    this.tableButtons.canAdd = hasRightOnProgram;
    this.tableButtons.canDuplicate = this.permissionService.hasSomeManagementPermission;
    this.tableButtons.canDelete =
      (this.selection.hasValue() && this.selection.selected.every((row) => this.hasRightOnStrategy(row.currentData))) || false;

    // Update default referential filter (Mantis #65560)
    this.showDisabledReferential =
      this.settings.getPropertyAsBoolean(accountConfigOptions.showDisabledReferential) &&
      (this.accountService.isAdmin() || hasRightOnProgram || this.hasRightOnStrategy(this.singleEditingRow?.currentData));
  }

  protected refreshTabs() {
    setTimeout(() => {
      this.detailAreaTabs()?.updatePagination();
      this.detailAreaTabs()?._alignInkBarToSelectedTab();
    });
  }

  protected updateTitle() {
    if (!this.programId) return;
    this.title = ['MENU.REFERENTIAL.MANAGEMENT', 'REFERENTIAL.PROGRAM.TITLE', 'REFERENTIAL.PROGRAM.STRATEGIES']
      .map((value) => this.translate.instant(value, { programId: this.programId }))
      .join(' &rArr; ');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'description');
  }

  protected applyRightOptions(table: IRightSelectTable<any>, row?: AsyncTableElement<Strategy>) {
    row = row || this.singleEditingRow;
    table.rightOptions = {
      entityName: Strategy.entityName,
      strategyId: row?.currentData.id,
    };
  }

  private async addStrategy(strategy: Strategy, insertAt?: number) {
    const newRow = await this.addRowToTable(insertAt);
    newRow.validator.patchValue(strategy);
  }
}
