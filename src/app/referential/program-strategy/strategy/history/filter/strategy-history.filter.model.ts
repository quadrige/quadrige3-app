import { EntityAsObjectOptions, EntityClass, FilterFn, isNotEmptyArray } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { BaseEntityFilter, BaseEntityFilterCriteria, BaseFilterUtils } from '@app/shared/model/filter.model';
import { StrategyHistory } from '@app/referential/program-strategy/strategy/history/strategy-history.model';
import { Dates } from '@app/shared/dates';
import moment from 'moment';

@EntityClass({ typename: 'StrategyHistoryFilterCriteriaVO' })
export class StrategyHistoryFilterCriteria extends BaseEntityFilterCriteria<StrategyHistory, string> {
  static fromObject: (source: any, opts?: any) => StrategyHistoryFilterCriteria;

  programIds: string[];
  onlyActive: boolean = null;
  departmentFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(StrategyHistoryFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.programIds = source.programIds;
    this.onlyActive = source.onlyActive || false;
    this.departmentFilter = IntReferentialFilterCriteria.fromObject(source.departmentFilter || {});
    this.searchAttributes = ['strategy.id', 'strategy.name', 'strategy.description'];
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.onlyActive = this.onlyActive || false;
    target.departmentFilter = this.departmentFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'programIds') return false;
    if (key === 'departmentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.departmentFilter, true);
    if (key === 'onlyActive') return this.onlyActive === true;
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  protected buildFilter(): FilterFn<StrategyHistory>[] {
    const filterFns = super.buildFilter();

    if (isNotEmptyArray(this.programIds)) {
      filterFns.push((strategyHistory) => this.programIds.includes(strategyHistory.program?.id));
    }
    if (this.onlyActive) {
      filterFns.push((strategyHistory) => Dates.isBetween(moment(), { start: strategyHistory.startDate, end: strategyHistory.endDate }));
    }
    if (this.departmentFilter) {
      const filter = this.departmentFilter.asFilterFn();
      if (filter) filterFns.push((strategyHistory) => filter(strategyHistory.department));
    }

    return filterFns;
  }
}

@EntityClass({ typename: 'StrategyHistoryFilterVO' })
export class StrategyHistoryFilter extends BaseEntityFilter<StrategyHistoryFilter, StrategyHistoryFilterCriteria, StrategyHistory, string> {
  static fromObject: (source: any, opts?: any) => StrategyHistoryFilter;

  constructor() {
    super(StrategyHistoryFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  criteriaFromObject(source: any, opts: any): StrategyHistoryFilterCriteria {
    return StrategyHistoryFilterCriteria.fromObject(source, opts);
  }
}
