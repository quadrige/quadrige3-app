import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import {
  StrategyHistoryFilter,
  StrategyHistoryFilterCriteria,
} from '@app/referential/program-strategy/strategy/history/filter/strategy-history.filter.model';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-strategy-history-filter-form',
  templateUrl: './strategy-history.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyHistoryFilterForm extends BaseCriteriaFormComponent<StrategyHistoryFilter, StrategyHistoryFilterCriteria> {
  constructor() {
    super();
  }

  get i18nColumnPrefix(): string {
    return 'REFERENTIAL.STRATEGY.';
  }

  protected getSearchAttributes(): string[] {
    return attributes.idNameDescription;
  }
}
