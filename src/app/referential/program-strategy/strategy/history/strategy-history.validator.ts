import { Injectable } from '@angular/core';
import { AbstractControlOptions, AsyncValidatorFn, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { IEntitiesTableDataSource, SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { StrategyHistory } from '@app/referential/program-strategy/strategy/history/strategy-history.model';
import { Observable, timer } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { Dates } from '@app/shared/dates';

export interface StrategyHistoryValidatorOptions extends ReferentialValidatorOptions {
  programId: string;
}

@Injectable({ providedIn: 'root' })
export class StrategyHistoryValidatorService extends ReferentialValidatorService<StrategyHistory, string, StrategyHistoryValidatorOptions> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: StrategyHistory, opts?: StrategyHistoryValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      appliedPeriod: [data?.appliedPeriod || null],
      program: [data?.program || null],
      strategy: [data?.strategy || null],
      department: [data?.department || null],
      startDate: [data?.startDate || null], // no validators here because mat-date-field owns them
      endDate: [data?.endDate || null], // no validators here because mat-date-field owns them
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: StrategyHistory, opts?: StrategyHistoryValidatorOptions): AbstractControlOptions {
    return {
      validators: [SharedFormGroupValidators.dateRange('startDate', 'endDate')],
      asyncValidators: this.checkOverlappingPeriods(this.dataSource, opts),
    };
  }

  checkOverlappingPeriods(dataSource: IEntitiesTableDataSource<any>, opts?: StrategyHistoryValidatorOptions): AsyncValidatorFn {
    return (group: UntypedFormGroup): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => group),
        mergeMap(async (_group) => {
          const value = _group.value;
          if (_group.dirty && !!value.startDate && !!value.endDate) {
            // first check in current rows
            if (
              dataSource
                .getRows()
                .filter(
                  (row) =>
                    row.currentData.program.id === opts.programId &&
                    Dates.overlaps({ start: row.currentData.startDate, end: row.currentData.endDate }, { start: value.startDate, end: value.endDate })
                ).length > 1
            ) {
              return { overlappingPeriodsMsg: 'REFERENTIAL.STRATEGY_HISTORY.ERROR.OVERLAPPING_PERIODS' };
            }
          }
          return null;
        })
      );
  }
}
