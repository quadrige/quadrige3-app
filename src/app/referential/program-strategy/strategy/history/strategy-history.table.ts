import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { referentialToString, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { StrategyHistory } from '@app/referential/program-strategy/strategy/history/strategy-history.model';
import {
  StrategyHistoryValidatorOptions,
  StrategyHistoryValidatorService,
} from '@app/referential/program-strategy/strategy/history/strategy-history.validator';
import { StrategyHistoryService } from '@app/referential/program-strategy/strategy/history/strategy-history.service';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { UntypedFormGroup } from '@angular/forms';
import { attributes } from '@app/referential/model/referential.constants';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { IntReferential } from '@app/referential/model/referential.model';
import {
  StrategyHistoryFilter,
  StrategyHistoryFilterCriteria,
} from '@app/referential/program-strategy/strategy/history/filter/strategy-history.filter.model';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { StrategyHistoryFilterForm } from '@app/referential/program-strategy/strategy/history/filter/strategy-history.filter.form';
import { IBaseTable } from '@app/shared/table/table.model';
import { map, startWith } from 'rxjs';

@Component({
  selector: 'app-strategy-history-table',
  templateUrl: './strategy-history.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule, StrategyHistoryFilterForm],
})
export class StrategyHistoryTable
  extends BaseMemoryTable<StrategyHistory, string, StrategyHistoryFilter, StrategyHistoryFilterCriteria, StrategyHistoryValidatorService>
  implements OnInit
{
  @Input() programId: string;
  @Input() monitoringLocation: IntReferential;
  @Input() canFilter = false;
  @Input() programTable: IBaseTable<any, any>;
  @Input() showDates = true;

  @Output() checkOverlappingPeriodsEventEmitter = new EventEmitter();

  constructor(
    protected injector: Injector,
    protected _entityService: StrategyHistoryService,
    protected validatorService: StrategyHistoryValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['program', 'strategy', 'startDate', 'endDate']).concat(RESERVED_END_COLUMNS),
      StrategyHistory,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.STRATEGY_HISTORY.';
    this.logPrefix = '[strategy-history-table]';
  }

  ngOnInit() {
    this.subTable = true;
    this.showPaginator = false;

    super.ngOnInit();

    if (this.programTable) {
      this.registerSubscription(
        this.programTable.selection.changed
          .pipe(
            map((value) => value.source),
            startWith(this.programTable.selection)
          )
          .subscribe((selection) => this.setShowColumn('program', selection.selected?.length > 1))
      );
    }

    if (!this.showDates) {
      this.setShowColumn('startDate', false);
      this.setShowColumn('endDate', false);
    }
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<StrategyHistory>): Promise<boolean> {
    if (this.isDateReadOnly()(row)) {
      return false;
    }
    return super.editRow(event, row);
  }

  isDateReadOnly(): ReadOnlyIfFn {
    return (row) => !this.canEdit || row?.currentData?.program?.id !== this.programId;
  }

  protected getRowValidator(row?: AsyncTableElement<StrategyHistory>): UntypedFormGroup {
    return super.getRowValidator(row, <StrategyHistoryValidatorOptions>{
      programId: this.programId,
    });
  }

  // protected methods

  protected updateTitle() {
    if (this.monitoringLocation) {
      this.title = this.translate.instant('REFERENTIAL.STRATEGY_HISTORY.TITLE', {
        location: referentialToString(this.monitoringLocation, attributes.labelName),
      });
    }
  }

  protected getRequiredColumns(): string[] {
    const requiredColumns = super.getRequiredColumns().slice();
    if (this.getShowColumn('program') && !requiredColumns.includes('program')) {
      requiredColumns.push('program');
    }
    requiredColumns.push('strategy');
    if (this.showDates) {
      requiredColumns.push('startDate', 'endDate');
    }
    return requiredColumns;
  }
}
