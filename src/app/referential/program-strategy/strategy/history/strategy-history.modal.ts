import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output, ViewChild } from '@angular/core';
import { StrategyHistoryTable } from '@app/referential/program-strategy/strategy/history/strategy-history.table';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { StrategyHistory } from '@app/referential/program-strategy/strategy/history/strategy-history.model';
import { IntReferential } from '@app/referential/model/referential.model';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IStrategyHistoryModalOptions extends IModalOptions {
  programId: string;
  monitoringLocation: IntReferential;
  checkOverlappingPeriodsEventEmitter: EventEmitter<any>;
}

@Component({
  selector: 'app-strategy-history-modal',
  templateUrl: './strategy-history.modal.html',
  styleUrls: ['./strategy-history.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyHistoryModal extends ModalComponent<boolean> implements IStrategyHistoryModalOptions {
  @ViewChild('historyTable') historyTable: StrategyHistoryTable;

  @Input() programId: string;
  @Input() monitoringLocation: IntReferential;

  @Output() checkOverlappingPeriodsEventEmitter = new EventEmitter();

  constructor(
    protected injector: Injector,
    protected strategyService: StrategyService
  ) {
    super(injector);
  }

  get disabled() {
    return this.loading || !this.historyTable?.dirty;
  }

  protected async afterInit() {
    await this.loadHistory();
  }

  async validate(event: UIEvent): Promise<void> {
    // If table is invalid, don't dismiss
    if (!this.historyTable.dirty || !(await this.historyTable.confirmEditCreate())) {
      return;
    }
    return super.validate(event);
  }

  protected async dataToValidate(): Promise<boolean> {
    try {
      this.setError(undefined, { emitEvent: false });
      this.historyTable.markAsSaving();
      return await this.saveHistory();
    } finally {
      this.historyTable.markAsSaved();
    }
  }

  private async loadHistory() {
    this.historyTable.markAsLoading();
    await this.historyTable.setValue(await this.strategyService.loadStrategiesHistory(this.monitoringLocation.id, undefined));
    this.historyTable.markAsLoaded();
  }

  private async saveHistory(): Promise<boolean> {
    // Get modified only
    const modifiedRows = this.historyTable.dataSource.getRows().filter((row) => row.validator.dirty);
    if (isEmptyArray(modifiedRows)) {
      console.warn('[strategy-history-modal] no modified rows to save');
      return false;
    }
    await this.strategyService.saveStrategiesHistory(modifiedRows.map((row) => StrategyHistory.fromObject(row.currentData)));
    return true;
  }
}
