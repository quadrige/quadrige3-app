import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { StrategyHistory } from '@app/referential/program-strategy/strategy/history/strategy-history.model';
import { StrategyHistoryFilter } from '@app/referential/program-strategy/strategy/history/filter/strategy-history.filter.model';

@Injectable({ providedIn: 'root' })
export class StrategyHistoryService extends EntitiesMemoryService<StrategyHistory, StrategyHistoryFilter, string> {
  constructor() {
    super(StrategyHistory, StrategyHistoryFilter, {
      sortByReplacement: { strategy: 'strategy.name' },
    });
  }
}
