import { EntityClass, fromDateISOString, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'StrategyHistoryVO' })
export class StrategyHistory extends Referential<StrategyHistory, string> {
  static entityName = 'StrategyHistory';
  static fromObject: (source: any, opts?: any) => StrategyHistory;

  program: GenericReferential = null;
  strategy: IntReferential = null;
  department: IntReferential = null;
  appliedPeriod: AppliedPeriod = null;
  startDate: Moment = null;
  endDate: Moment = null;

  constructor() {
    super(StrategyHistory.TYPENAME);
    this.entityName = StrategyHistory.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = StrategyHistory.entityName;
    this.program = GenericReferential.fromObject(source.program);
    this.strategy = IntReferential.fromObject(source.strategy);
    this.department = IntReferential.fromObject(source.department);
    this.appliedPeriod = AppliedPeriod.fromObject(source.appliedPeriod);
    this.startDate = fromDateISOString(source.startDate || this.appliedPeriod.startDate);
    this.endDate = fromDateISOString(source.endDate || this.appliedPeriod.endDate);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.program = EntityUtils.asMinifiedObject(this.program, opts);
    target.strategy = EntityUtils.asMinifiedObject(this.strategy, opts);
    target.department = EntityUtils.asMinifiedObject(this.department, opts);
    target.appliedPeriod = this.appliedPeriod?.asObject(opts);
    target.appliedPeriod.startDate = Dates.toLocalDateString(this.startDate);
    target.appliedPeriod.endDate = Dates.toLocalDateString(this.endDate);
    delete target.startDate;
    delete target.endDate;
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.statusId;
    return target;
  }
}
