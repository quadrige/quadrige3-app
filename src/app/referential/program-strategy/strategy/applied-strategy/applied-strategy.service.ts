import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { AppliedStrategyFilter } from '@app/referential/program-strategy/strategy/applied-strategy/filter/applied-strategy.filter.model';

@Injectable()
export class AppliedStrategyService extends EntitiesMemoryService<AppliedStrategy, AppliedStrategyFilter> {
  constructor() {
    super(AppliedStrategy, AppliedStrategyFilter);
  }
}
