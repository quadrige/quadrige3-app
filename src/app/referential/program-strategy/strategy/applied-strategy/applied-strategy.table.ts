import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
  arraySize,
  isEmptyArray,
  isNotEmptyArray,
  isNotNilOrBlank,
  PromiseEvent,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { AppliedStrategyValidatorService } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.validator';
import { AppliedStrategyService } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.service';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { AppliedPeriodTable } from '@app/referential/program-strategy/strategy/applied-period/applied-period.table';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { Alerts } from '@app/shared/alerts';
import { DateAdapter } from '@angular/material/core';
import { Moment } from 'moment';
import {
  AppliedStrategyAddModal,
  IAppliedStrategyAddModalOptions,
  IAppliedStrategyAddModalResult,
} from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.add.modal';
import {
  AppliedStrategyMultiEditModal,
  IAppliedStrategyMultiEditModalOptions,
} from '@app/referential/program-strategy/strategy/applied-strategy/multi-edit/applied-strategy.multi-edit.modal';
import { CheckPeriodsToDeleteOptions } from '@app/referential/program-strategy/strategy/strategy.table';
import { AppliedPeriodComposite } from '@app/referential/program-strategy/strategy/applied-period/multi-edit/applied-period.multi-edit.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { IntReferential } from '@app/referential/model/referential.model';
import {
  AppliedStrategyFilter,
  AppliedStrategyFilterCriteria,
} from '@app/referential/program-strategy/strategy/applied-strategy/filter/applied-strategy.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { AppliedStrategyFilterForm } from '@app/referential/program-strategy/strategy/applied-strategy/filter/applied-strategy.filter.form';
import { AppliedStrategyComposite } from '@app/referential/program-strategy/strategy/applied-strategy/multi-edit/applied-strategy.multi-edit.model';
import { MonitoringLocationFilterCriteria } from '@app/referential/monitoring-location/filter/monitoring-location.filter.model';
import { entityId } from '@app/shared/entity.utils';

@Component({
  selector: 'app-applied-strategy-table',
  templateUrl: './applied-strategy.table.html',
  standalone: true,
  imports: [ReferentialModule, AppliedStrategyFilterForm, AppliedPeriodTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AppliedStrategyService,
      useExisting: false,
    },
  ],
})
export class AppliedStrategyTable
  extends ReferentialMemoryTable<AppliedStrategy, number, AppliedStrategyFilter, AppliedStrategyFilterCriteria, AppliedStrategyValidatorService>
  implements OnInit, AfterViewInit
{
  @ViewChild('appliedPeriodTable') appliedPeriodTable: AppliedPeriodTable;

  @Input() titleI18n = 'REFERENTIAL.STRATEGY.MONITORING_LOCATION';
  @Input() appliedPeriodsEnabled = true;
  @Input() programId: string;

  @Output() checkOverlappingPeriodsEventEmitter = new EventEmitter();
  @Output() openStrategiesHistoryEventEmitter = new EventEmitter<IntReferential>();

  periodAreaEnabled = false;

  constructor(
    protected injector: Injector,
    protected _entityService: AppliedStrategyService,
    protected validatorService: AppliedStrategyValidatorService,
    protected strategyService: StrategyService,
    protected dateAdapter: DateAdapter<Moment>
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'monitoringLocation.label',
        'monitoringLocation.name',
        'frequency',
        'department',
        'taxonGroup',
        'referenceTaxon',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      AppliedStrategy,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.STRATEGY.MONITORING_LOCATION';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.APPLIED_STRATEGY.';
    this.defaultSortBy = 'monitoringLocation.label';
    this.logPrefix = '[applied-strategy-table]';
  }

  ngOnInit() {
    this.subTable = true;

    super.ngOnInit();

    // department combo
    this.registerAutocompleteField('department', {
      ...this.referentialOptions.department,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Department',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // frequency combo
    this.registerAutocompleteField('frequency', {
      ...this.referentialOptions.frequency,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Frequency',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // taxon group combo
    this.registerAutocompleteField('taxonGroup', {
      ...this.referentialOptions.taxonGroup,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'TaxonGroup',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // reference taxon combo
    this.registerAutocompleteField('referenceTaxon', {
      ...this.referentialOptions.referenceTaxon,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ReferenceTaxon',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.registerSubForm(this.appliedPeriodTable);

    if (this.appliedPeriodsEnabled) {
      this.restoreRightPanel(true, 20);
    } else {
      this.rightPanelAllowed = false;
    }
  }

  async removeSelection(event?: UIEvent) {
    if (event) {
      event.stopPropagation();
    }
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Save before delete
    const saved = await this.saveBeforeAction('delete');
    if (!saved) {
      // Stop if save cancelled or save failed
      return;
    }

    // Check if can delete
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // remove from selection
    const removedIds = toDelete.map((row) => row.currentData.monitoringLocation.id.toString());
    const newValue = this.value.slice().filter((appliedStrategy) => !removedIds.includes(appliedStrategy.monitoringLocation.id.toString()));

    this.markAsDirty();
    await this.setValue(newValue, { resetFilter: false });
    this.onAfterDeletedRows.emit();
  }

  async multiEditRows(event: MouseEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IAppliedStrategyMultiEditModalOptions, AppliedStrategyComposite>(
      AppliedStrategyMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.APPLIED_STRATEGY.MULTI_EDIT.TITLE',
        i18nPrefix: this.i18nColumnPrefix,
        selection: this.selection.selected.map((row) => row.currentData),
        defaultReferentialCriteria: this.defaultReferentialCriteria,
      }
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }
    if (role === 'validate' && data) {
      // Patch selected rows
      this.selection.selected.forEach((row) => {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    }
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<AppliedStrategy>): Promise<boolean> {
    row = row || this.singleEditingRow;

    // First, confirm and save applied periods
    if (row && this.appliedPeriodTable.dirty && this.appliedPeriodsEnabled) {
      if (await this.appliedPeriodTable.save()) {
        this.patchRow('appliedPeriods', this.appliedPeriodTable.value, row);
      } else {
        return false;
      }
    }

    return super.confirmEditCreate(event, row);
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    try {
      this.markAsSaving();
      let continueSave = true;
      if (this.appliedPeriodTable.dirty && this.appliedPeriodsEnabled) {
        // Wait for applied period table saved and patch directly
        continueSave = await this.appliedPeriodTable.save();
        if (continueSave) {
          this.patchRow('appliedPeriods', this.appliedPeriodTable.value, undefined, { emitEvent: false });
        }
      }

      if (continueSave) {
        const error = this.getRowError();
        if (isNotNilOrBlank(error)) {
          this.markAsError(error);
          continueSave = false;
        }
      }

      if (continueSave) {
        return await super.save(opts);
      } else {
        return false;
      }
    } finally {
      this.markAsSaved();
    }
  }

  async setValue(value: AppliedStrategy[], opts?: { monitoringLocationIds?: number[]; resetFilter?: boolean; emitEvent?: boolean }): Promise<void> {
    this.selection.clear();
    if ((!opts || opts.resetFilter !== false) && !this.filterIsEmpty) {
      await this.resetFilter(undefined, { emitEvent: false });
    }
    await super.setValue(value, opts);

    if (isNotEmptyArray(opts?.monitoringLocationIds)) {
      let rowToFocus: AsyncTableElement<AppliedStrategy>;
      this.getRows()
        .filter((row) => opts.monitoringLocationIds.map((id) => id.toString()).includes(row.currentData.monitoringLocation.id.toString()))
        .forEach((row) => {
          this.markRowAsDirty(row);
          if (!rowToFocus) rowToFocus = row;
        });
      this.focusRow(rowToFocus);
      await this.selectRowById(rowToFocus?.id);
    }
  }

  async addRow(event?: Event): Promise<boolean> {
    await this.openAddModal(event);
    return false;
  }

  async openAddModal(event: any) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }
    await this.save();
    this.selection.clear();

    // Open modal
    const { role, data } = await this.modalService.openModal<IAppliedStrategyAddModalOptions, IAppliedStrategyAddModalResult>(
      AppliedStrategyAddModal,
      {
        monitoringLocationAddCriteria: MonitoringLocationFilterCriteria.fromObject({
          ...this.defaultReferentialCriteria,
          programFilter: { includedIds: [this.programId] },
          excludedIds: this.value.map((value) => value.monitoringLocation.id.toString()),
        }),
        titleI18n: 'REFERENTIAL.STRATEGY.MONITORING_LOCATION',
        departmentConfig: this.autocompleteFields.department,
        frequencyConfig: this.autocompleteFields.frequency,
        taxonGroupConfig: this.autocompleteFields.taxonGroup,
        referenceTaxonConfig: this.autocompleteFields.referenceTaxon,
        checkOverlappingPeriodsEventEmitter: this.checkOverlappingPeriodsEventEmitter,
      },
      'modal-large'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.monitoringLocations)) {
      // Create new applied strategies
      this.markAsDirty();
      const newValue = this.value?.slice() || [];
      newValue.push(
        ...data.monitoringLocations
          .map(IntReferential.fromObject)
          .map(
            (monitoringLocation) =>
              new AppliedStrategy(monitoringLocation, data.frequency, data.department, data.taxonGroup, data.referenceTaxon, data.appliedPeriods)
          )
      );
      await this.setValue(newValue, { monitoringLocationIds: data.monitoringLocations.map(entityId) });
    }
  }

  async openHistoryModal(event: UIEvent) {
    event?.preventDefault();

    const row = this.singleSelectedRow;
    if (row?.currentData?.monitoringLocation) {
      this.openStrategiesHistoryEventEmitter.emit(row.currentData.monitoringLocation);
    }
  }

  // managing applied periods

  async loadRightArea(row?: AsyncTableElement<AppliedStrategy>) {
    if (!this.appliedPeriodsEnabled) return;
    // multiple selection allowed
    if (this.selection.selected.length > 1) {
      if (this.debug) {
        console.debug(`${this.logPrefix} multiple applied strategy selected`);
      }
      await this.appliedPeriodTable?.setAppliedStrategies(this.selection.selected.map((selectedRow) => selectedRow.currentData));
      this.periodAreaEnabled = this.canEdit;
      return;
    }

    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.appliedPeriodTable?.setAppliedStrategy(undefined);
      this.periodAreaEnabled = false;
      return;
    }
    // Load selected periods
    if (this.appliedPeriodTable) {
      await this.appliedPeriodTable.setAppliedStrategy(row.currentData);
    } else {
      throw new Error(`the appliedPeriodTable is not present in DOM`);
    }

    this.periodAreaEnabled = this.canEdit;
  }

  appliedPeriodTableChanged() {
    if (this.canEdit && this.appliedPeriodsEnabled) {
      this.appliedPeriodTable.save().then((saved) => {
        if (saved) {
          this.patchRow('appliedPeriods', this.appliedPeriodTable.value);
        } else {
          throw new Error('The applied periods table has not been saved, should not be possible');
        }
      });
      this.markRowAsDirty();
    }
  }

  async checkCanDeletePeriod(event: PromiseEvent<boolean, CheckPeriodsToDeleteOptions>) {
    const editingRow = this.singleEditingRow;
    if (!editingRow && !event.detail.monitoringLocationId) throw new Error(`There is no edited applied strategy row`);

    if (!editingRow?.currentData.id) {
      // No id = new row => no need to check
      event.detail.success(true);
      return;
    }
    const monitoringLocationId = editingRow?.currentData.monitoringLocation.id || event.detail.monitoringLocationId;
    const notDeletablePeriods: string[] = [];
    const datePattern = this.translate.instant('COMMON.DATE_PATTERN');
    for (const row of event.detail.rows) {
      if (
        !(await this.strategyService.canDeleteAppliedPeriod(this.programId, monitoringLocationId, row.currentData.startDate, row.currentData.endDate))
      ) {
        notDeletablePeriods.push(
          `${this.dateAdapter.format(row.currentData.startDate, datePattern)} -> ${this.dateAdapter.format(row.currentData.endDate, datePattern)}`
        );
      }
    }
    const canDelete = notDeletablePeriods.length === 0;
    event.detail.success(canDelete);
    if (!canDelete) {
      await Alerts.showDetailedMessage('REFERENTIAL.APPLIED_PERIOD.ERROR.CANNOT_DELETE', this.alertCtrl, this.translate, {
        titleKey: 'ERROR.CANNOT_DELETE',
        detailedMessage: notDeletablePeriods.join('<br/>'),
      });
    }
  }

  async periodsMultiEditChanged(allComposites: AppliedPeriodComposite[]) {
    // patch selected rows
    this.selection.selected.forEach((row) => {
      const composites = allComposites.filter(
        (composite) =>
          composite.appliedStrategyId === row.currentData.id ||
          composite.monitoringLocation.id.toString() === row.currentData.monitoringLocation.id.toString()
      );
      const periods = composites.map((composite) => new AppliedPeriod(composite.appliedStrategyId, composite.startDate, composite.endDate));
      this.patchRow('appliedPeriods', periods, row);
    });
    await this.loadRightArea();
  }

  patchRow(attribute: keyof AppliedStrategy, values: any[], row?: AsyncTableElement<AppliedStrategy>, opts?: { emitEvent: boolean }): boolean {
    row = row || this.singleEditingRow;
    if (!row) return false;

    if (attribute === 'appliedPeriods' && this.periodsEquals(row.currentData.appliedPeriods, values)) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no difference found, aborting patch`);
      }
      return false;
    }

    return super.patchRow(attribute, values, row, opts);
  }

  // protected methods

  protected periodsEquals(periods1: AppliedPeriod[], periods2: AppliedPeriod[]): boolean {
    return arraySize(periods1) === arraySize(periods2) && periods1?.every((period1) => !!periods2.find((period2) => period2.dateEquals(period1)));
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('monitoringLocation.label', 'monitoringLocation.name');
  }
}
