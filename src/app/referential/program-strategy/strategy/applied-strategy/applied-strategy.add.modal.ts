import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output, ViewChild } from '@angular/core';
import { AppliedPeriodTable } from '@app/referential/program-strategy/strategy/applied-period/applied-period.table';
import { MatAutocompleteFieldConfig, SharedValidators, StatusIds } from '@sumaris-net/ngx-components';
import { MatStepper } from '@angular/material/stepper';
import { autocompleteWidthFull } from '@app/shared/constants';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { MonitoringLocationAddTable } from '@app/referential/monitoring-location/monitoring-location.add.table';
import { IModalOptions } from '@app/shared/model/options.model';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import { IntReferential, StrReferential } from '@app/referential/model/referential.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { MonitoringLocationFilterCriteria } from '@app/referential/monitoring-location/filter/monitoring-location.filter.model';

export interface IAppliedStrategyAddModalOptions extends IModalOptions {
  monitoringLocationAddCriteria: Partial<MonitoringLocationFilterCriteria>;
  departmentConfig: MatAutocompleteFieldConfig;
  frequencyConfig: MatAutocompleteFieldConfig;
  taxonGroupConfig: MatAutocompleteFieldConfig;
  referenceTaxonConfig: MatAutocompleteFieldConfig;
  checkOverlappingPeriodsEventEmitter: EventEmitter<any>;
}

export interface IAppliedStrategyAddModalResult {
  monitoringLocations: MonitoringLocation[];
  appliedPeriods: AppliedPeriod[];
  department: IntReferential;
  frequency: StrReferential;
  taxonGroup: IntReferential;
  referenceTaxon: IntReferential;
}

@Component({
  selector: 'app-applied-strategy-add-modal',
  templateUrl: './applied-strategy.add.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppliedStrategyAddModal extends ModalComponent<IAppliedStrategyAddModalResult> implements IAppliedStrategyAddModalOptions {
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  @ViewChild('monitoringLocationTable') monitoringLocationTable: MonitoringLocationAddTable;
  @ViewChild('appliedPeriodTable') appliedPeriodTable: AppliedPeriodTable;

  @Input() monitoringLocationAddCriteria: Partial<MonitoringLocationFilterCriteria>;
  @Input() departmentConfig: MatAutocompleteFieldConfig;
  @Input() frequencyConfig: MatAutocompleteFieldConfig;
  @Input() taxonGroupConfig: MatAutocompleteFieldConfig;
  @Input() referenceTaxonConfig: MatAutocompleteFieldConfig;

  @Output() checkOverlappingPeriodsEventEmitter = new EventEmitter();

  autocompleteWidth = autocompleteWidthFull;
  statusIds = StatusIds;

  readonly appliedPeriodStepIndex = 1;
  readonly otherPropertiesStepIndex = 2;

  constructor(protected injector: Injector) {
    super(injector);
    this.setForm(
      this.formBuilder.group({
        department: [null, SharedValidators.entity],
        frequency: [null, SharedValidators.entity],
        taxonGroup: [null, SharedValidators.entity],
        referenceTaxon: [null, SharedValidators.entity],
      })
    );
    this.validIfPristine = true;
  }

  get canGoNext() {
    return this.stepper.selected?.completed;
  }

  get canValidate() {
    return this.stepper.selectedIndex === this.otherPropertiesStepIndex;
  }

  protected async afterInit() {
    await this.monitoringLocationTable.resetFilter(undefined, { firstTime: true });
    this.monitoringLocationTable.filterFormComponent().disableControlPermanent('metaProgramFilter');
    this.monitoringLocationTable.filterFormComponent().disableControlPermanent('programFilter');
    this.appliedPeriodTable.toolbarColor = 'secondary900';
    await this.appliedPeriodTable.setValue([]);
  }

  async stepChanged(event: StepperSelectionEvent) {
    if (event.selectedIndex === this.appliedPeriodStepIndex && this.appliedPeriodTable.totalRowCount === 0) {
      // Add a row when step is on applied periods
      await this.appliedPeriodTable.addRow();
    }
    if (event.previouslySelectedIndex === this.appliedPeriodStepIndex) {
      // Save the applied periods table when step leaves the table
      if (!(await this.appliedPeriodTable.save())) this.stepper.selectedIndex = this.appliedPeriodStepIndex;
    }
  }

  async onNext(event: any) {
    event?.stopPropagation();
    this.stepper.next();
  }

  protected dataToValidate(): Promise<IAppliedStrategyAddModalResult> | IAppliedStrategyAddModalResult {
    return {
      monitoringLocations: this.monitoringLocationTable.selectedEntities,
      appliedPeriods: this.appliedPeriodTable.value,
      ...this.value,
    };
  }
}
