import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { SharedValidators } from '@sumaris-net/ngx-components';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class AppliedStrategyValidatorService extends ReferentialValidatorService<AppliedStrategy> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: AppliedStrategy, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      monitoringLocation: [data?.monitoringLocation || null, Validators.compose([Validators.required, SharedValidators.entity])],
      frequency: [data?.frequency || null, SharedValidators.entity],
      department: [data?.department || null, SharedValidators.entity],
      taxonGroup: [data?.taxonGroup || null, SharedValidators.entity],
      referenceTaxon: [data?.referenceTaxon || null, SharedValidators.entity],
      appliedPeriods: [data?.appliedPeriods || null],
      pmfmuAppliedStrategies: [data?.pmfmuAppliedStrategies || null],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: AppliedStrategy, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [BaseGroupValidators.requiredAllArraysNotEmpty(['appliedPeriods'], 'REFERENTIAL.APPLIED_STRATEGY.ERROR.MISSING_APPLIED_PERIOD')],
    };
  }
}
