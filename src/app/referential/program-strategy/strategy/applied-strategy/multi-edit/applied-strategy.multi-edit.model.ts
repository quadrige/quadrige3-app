import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { IComposite } from '@app/shared/model/composite';
import { IntReferential, StrReferential } from '@app/referential/model/referential.model';

export class AppliedStrategyComposite implements IComposite {
  frequency: StrReferential = null;
  frequencyMultiple = false;
  department: IntReferential = null;
  departmentMultiple = false;
  taxonGroup: IntReferential = null;
  taxonGroupMultiple = false;
  referenceTaxon: IntReferential = null;
  referenceTaxonMultiple = false;

  static fromAppliedStrategy(source: AppliedStrategy): AppliedStrategyComposite {
    const target = new AppliedStrategyComposite();

    target.frequency = StrReferential.fromObject(source.frequency);
    target.department = IntReferential.fromObject(source.department);
    target.taxonGroup = IntReferential.fromObject(source.taxonGroup);
    target.referenceTaxon = IntReferential.fromObject(source.referenceTaxon);

    return target;
  }
}
