import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { AppliedStrategyComposite } from '@app/referential/program-strategy/strategy/applied-strategy/multi-edit/applied-strategy.multi-edit.model';
import { CompositeUtils } from '@app/shared/model/composite';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { AppliedStrategyValidatorService } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.validator';
import { StatusIds } from '@sumaris-net/ngx-components';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { GenericService } from '@app/referential/generic/generic.service';
import { IModalOptions } from '@app/shared/model/options.model';

export interface IAppliedStrategyMultiEditModalOptions extends IModalOptions {
  i18nPrefix: string;
  selection: AppliedStrategy[];
}

@Component({
  selector: 'app-applied-strategy-multi-edit-modal',
  templateUrl: './applied-strategy.multi-edit.modal.html',
  styleUrl: './applied-strategy.multi-edit.modal.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppliedStrategyMultiEditModal extends ModalComponent<AppliedStrategyComposite> implements IAppliedStrategyMultiEditModalOptions, OnInit {
  @Input() i18nPrefix: string;
  @Input() selection: AppliedStrategy[];

  statusIds = StatusIds;
  panelWidth = autocompleteWidthExtraLarge;

  constructor(
    protected injector: Injector,
    protected referentialGenericService: GenericService,
    protected appliedStrategyValidator: AppliedStrategyValidatorService
  ) {
    super(injector);
    // Get AppliedStrategyComposite form config
    const formConfig = CompositeUtils.getFormGroupConfig(new AppliedStrategyComposite(), appliedStrategyValidator.getFormGroupConfig());
    this.setForm(this.formBuilder.group(formConfig));
  }

  ngOnInit() {
    super.ngOnInit();

    // department combo
    this.registerAutocompleteField('department', {
      ...referentialOptions.department,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Department',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // frequency combo
    this.registerAutocompleteField('frequency', {
      ...referentialOptions.frequency,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Frequency',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // taxon group combo
    this.registerAutocompleteField('taxonGroup', {
      ...referentialOptions.taxonGroup,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'TaxonGroup',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // reference taxon combo
    this.registerAutocompleteField('referenceTaxon', {
      ...referentialOptions.referenceTaxon,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ReferenceTaxon',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    // Listen for changes
    CompositeUtils.listenCompositeFormChanges(this.form.controls, (subscription) => this.registerSubscription(subscription));
  }

  protected afterInit() {
    this.value = CompositeUtils.build(this.selection, (candidate) => AppliedStrategyComposite.fromAppliedStrategy(candidate));
  }

  protected dataToValidate(): Promise<AppliedStrategyComposite> | AppliedStrategyComposite {
    return CompositeUtils.value(this.value);
  }
}
