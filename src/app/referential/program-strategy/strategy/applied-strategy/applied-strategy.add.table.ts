import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { AppliedStrategyValidatorService } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.validator';
import { AppliedStrategyService } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { DateAdapter } from '@angular/material/core';
import { Moment } from 'moment';
import { AppliedStrategyTable } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.table';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { AppliedStrategyFilter } from '@app/referential/program-strategy/strategy/applied-strategy/filter/applied-strategy.filter.model';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { AppliedStrategyFilterForm } from '@app/referential/program-strategy/strategy/applied-strategy/filter/applied-strategy.filter.form';
import { AppliedPeriodTable } from '@app/referential/program-strategy/strategy/applied-period/applied-period.table';

@Component({
  selector: 'app-applied-strategy-add-table',
  templateUrl: './applied-strategy.table.html',
  standalone: true,
  imports: [ReferentialModule, AppliedStrategyFilterForm, AppliedPeriodTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AppliedStrategyService,
      useExisting: false,
    },
  ],
})
export class AppliedStrategyAddTable extends AppliedStrategyTable implements IAddTable<AppliedStrategy> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: AppliedStrategyService,
    protected validatorService: AppliedStrategyValidatorService,
    protected strategyService: StrategyService,
    protected dateAdapter: DateAdapter<Moment>
  ) {
    super(injector, _entityService, validatorService, strategyService, dateAdapter);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[applied-strategy-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: AppliedStrategyFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('monitoringLocation');
  }
}
