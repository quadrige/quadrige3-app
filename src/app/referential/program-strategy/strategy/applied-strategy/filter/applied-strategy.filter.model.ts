import { EntityAsObjectOptions, FilterFn } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { AppliedStrategy } from '@app/referential/program-strategy/strategy/applied-strategy/applied-strategy.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export class AppliedStrategyFilterCriteria extends ReferentialFilterCriteria<AppliedStrategy, number> {
  monitoringLocationFilter: IntReferentialFilterCriteria = null;
  frequencyFilter: StrReferentialFilterCriteria = null;
  departmentFilter: IntReferentialFilterCriteria = null;
  taxonGroupFilter: IntReferentialFilterCriteria = null;
  referenceTaxonFilter: IntReferentialFilterCriteria = null;

  static fromObject(source: any, opts?: any): AppliedStrategyFilterCriteria {
    const target = new AppliedStrategyFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.frequencyFilter = StrReferentialFilterCriteria.fromObject(source.frequencyFilter || {});
    this.departmentFilter = IntReferentialFilterCriteria.fromObject(source.departmentFilter || {});
    this.taxonGroupFilter = IntReferentialFilterCriteria.fromObject(source.taxonGroupFilter || {});
    this.referenceTaxonFilter = IntReferentialFilterCriteria.fromObject(source.referenceTaxonFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    target.frequencyFilter = this.frequencyFilter?.asObject(opts);
    target.departmentFilter = this.departmentFilter?.asObject(opts);
    target.taxonGroupFilter = this.taxonGroupFilter?.asObject(opts);
    target.referenceTaxonFilter = this.referenceTaxonFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    if (key === 'frequencyFilter') return !BaseFilterUtils.isCriteriaEmpty(this.frequencyFilter, true);
    if (key === 'departmentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.departmentFilter, true);
    if (key === 'taxonGroupFilter') return !BaseFilterUtils.isCriteriaEmpty(this.taxonGroupFilter, true);
    if (key === 'referenceTaxonFilter') return !BaseFilterUtils.isCriteriaEmpty(this.referenceTaxonFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  protected buildFilter(): FilterFn<AppliedStrategy>[] {
    const target = super.buildFilter();

    if (this.monitoringLocationFilter) {
      const filter = this.monitoringLocationFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.monitoringLocation));
    }
    if (this.frequencyFilter) {
      const filter = this.frequencyFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.frequency));
    }
    if (this.departmentFilter) {
      const filter = this.departmentFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.department));
    }
    if (this.taxonGroupFilter) {
      const filter = this.taxonGroupFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.taxonGroup));
    }
    if (this.referenceTaxonFilter) {
      const filter = this.referenceTaxonFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.referenceTaxon));
    }

    return target;
  }
}

export class AppliedStrategyFilter extends ReferentialFilter<AppliedStrategyFilter, AppliedStrategyFilterCriteria, AppliedStrategy> {
  static fromObject(source: any, opts?: any): AppliedStrategyFilter {
    const target = new AppliedStrategyFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): AppliedStrategyFilterCriteria {
    return AppliedStrategyFilterCriteria.fromObject(source, opts);
  }
}
