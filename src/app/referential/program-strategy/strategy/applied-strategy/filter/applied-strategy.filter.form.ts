import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import {
  AppliedStrategyFilter,
  AppliedStrategyFilterCriteria,
} from '@app/referential/program-strategy/strategy/applied-strategy/filter/applied-strategy.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-applied-strategy-filter-form',
  templateUrl: './applied-strategy.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppliedStrategyFilterForm extends ReferentialCriteriaFormComponent<AppliedStrategyFilter, AppliedStrategyFilterCriteria> {
  @Input() programId: string;

  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected criteriaToQueryParams(criteria: AppliedStrategyFilterCriteria): PartialRecord<keyof AppliedStrategyFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    this.subCriteriaToQueryParams(params, criteria, 'departmentFilter');
    this.subCriteriaToQueryParams(params, criteria, 'frequencyFilter');
    this.subCriteriaToQueryParams(params, criteria, 'taxonGroupFilter');
    this.subCriteriaToQueryParams(params, criteria, 'referenceTaxonFilter');
    return params;
  }
}
