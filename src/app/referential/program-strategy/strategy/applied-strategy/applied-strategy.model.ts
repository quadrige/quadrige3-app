import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential, StrReferential } from '@app/referential/model/referential.model';
import { PmfmuAppliedStrategy } from '@app/referential/program-strategy/strategy/pmfmu-applied-strategy/pmfmu-applied-strategy.model';
import { AppliedPeriod } from '@app/referential/program-strategy/strategy/applied-period/applied-period.model';
import moment from 'moment';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'AppliedStrategyVO' })
export class AppliedStrategy extends Referential<AppliedStrategy> {
  static entityName = 'AppliedStrategy';
  static fromObject: (source: any, opts?: any) => AppliedStrategy;

  monitoringLocation: IntReferential = null;
  frequency: StrReferential = null;
  department: IntReferential = null;
  taxonGroup: IntReferential = null;
  referenceTaxon: IntReferential = null;
  appliedPeriods: AppliedPeriod[] = null;
  pmfmuAppliedStrategies: PmfmuAppliedStrategy[] = null;

  constructor(
    monitoringLocation?: IntReferential,
    frequency?: StrReferential,
    department?: IntReferential,
    taxonGroup?: IntReferential,
    referenceTaxon?: IntReferential,
    appliedPeriods?: AppliedPeriod[]
  ) {
    super(AppliedStrategy.TYPENAME);
    this.entityName = AppliedStrategy.entityName;
    this.monitoringLocation = monitoringLocation;
    this.frequency = frequency;
    this.department = department;
    this.taxonGroup = taxonGroup;
    this.referenceTaxon = referenceTaxon;
    this.appliedPeriods = appliedPeriods || [];
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = AppliedStrategy.entityName;
    this.monitoringLocation = IntReferential.fromObject(source.monitoringLocation);
    this.frequency = StrReferential.fromObject(source.frequency);
    this.department = IntReferential.fromObject(source.department);
    this.taxonGroup = IntReferential.fromObject(source.taxonGroup);
    this.referenceTaxon = IntReferential.fromObject(source.referenceTaxon);
    this.appliedPeriods = source.appliedPeriods?.map(AppliedPeriod.fromObject) || [];
    this.pmfmuAppliedStrategies = source.pmfmuAppliedStrategies?.map(PmfmuAppliedStrategy.fromObject) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.monitoringLocation = EntityUtils.asMinifiedObject(this.monitoringLocation, opts);
    target.frequency = EntityUtils.asMinifiedObject(this.frequency, opts);
    target.department = EntityUtils.asMinifiedObject(this.department, opts);
    target.taxonGroup = EntityUtils.asMinifiedObject(this.taxonGroup, opts);
    target.referenceTaxon = EntityUtils.asMinifiedObject(this.referenceTaxon, opts);
    target.pmfmuAppliedStrategies = this.pmfmuAppliedStrategies?.map((value) => value.asObject(opts));
    target.appliedPeriods = this.appliedPeriods?.map((value) => value.asObject(opts));
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.statusId;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.creationDate;
    delete target.validityStatusId;
    delete target.levelId;
    delete target.parentId;
    return target;
  }

  equals(other: AppliedStrategy): boolean {
    // Change equality on monitoring location with both converting to string (Mantis #)
    return super.equals(other) || this.monitoringLocation.id.toString() === other.monitoringLocation.id.toString();
  }
}

export class AppliedStrategyUtils {
  static activePeriod(periods: AppliedPeriod[]): AppliedPeriod {
    const now = moment();
    return (
      /* active period */ (periods || []).find((period) => now.isBetween(period.startDate, period.endDate, 'day', '[]')) ||
      /* or the last (= first sorting by desc endDate) */ (periods || []).sort((a, b) => b.endDate.diff(a.endDate))[0] ||
      undefined
    );
  }
}
