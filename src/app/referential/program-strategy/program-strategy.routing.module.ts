import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { StrategyTable } from '@app/referential/program-strategy/strategy/strategy.table';
import { ProgramStrategyModule } from '@app/referential/program-strategy/program-strategy.module';
import { ProgramTable } from '@app/referential/program-strategy/program/program.table';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ProgramTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
  {
    path: ':programId/strategy',
    runGuardsAndResolvers: 'pathParamsChange',
    component: StrategyTable,
    data: {
      profile: 'ADMIN',
      pathIdParam: 'programId',
    },
  },
  {
    path: ':programId/moratorium',
    runGuardsAndResolvers: 'pathParamsChange',
    loadChildren: () => import('../moratorium/moratorium.routing.module').then((m) => m.MoratoriumRoutingModule),
    data: {
      profile: 'ADMIN',
      pathIdParam: 'programId',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ProgramStrategyModule],
})
export class ProgramStrategyRoutingModule {}
