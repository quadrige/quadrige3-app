import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { ProgramValidatorService } from '@app/referential/program-strategy/program/program.validator';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { ProgramTable } from '@app/referential/program-strategy/program/program.table';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { ISelectTable } from '@app/shared/table/table.model';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { ProgramFilter } from '@app/referential/program-strategy/program/filter/program.filter.model';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { ProgramFilterForm } from '@app/referential/program-strategy/program/filter/program.filter.form';
import { ProgramLocationTable } from '@app/referential/program-strategy/program/location/program-location.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';

@Component({
  selector: 'app-program-select-table',
  templateUrl: './program.table.html',
  styleUrls: ['./program.table.scss'],
  standalone: true,
  imports: [ReferentialModule, ProgramFilterForm, ProgramLocationTable, UserSelectTable, DepartmentSelectTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgramSelectTable extends ProgramTable implements OnInit, AfterViewInit, ISelectTable<Program> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria = {};
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();

  constructor(
    protected injector: Injector,
    protected _entityService: ProgramService,
    protected validatorService: ProgramValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[program-select-table]';
    this.selectTable = true;
  }

  ngOnInit() {
    super.ngOnInit();

    this.setShowColumn('strategies', false);
    this.setShowColumn('moratoriums', false);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  async resetFilter(filter?: ProgramFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit();
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete);
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id));
    await this.resetFilter();
  }

  updatePermission() {
    // Don't update permissions
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected refreshButtons() {
    this.tableButtons.canAdd = true;
    this.tableButtons.canDuplicate = false;
    this.tableButtons.canDelete = true;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('strategies', 'moratoriums');
  }
}
