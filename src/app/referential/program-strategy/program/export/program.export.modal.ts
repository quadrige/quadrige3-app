import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { ObjectMap } from '@sumaris-net/ngx-components';
import { ExportModal } from '@app/shared/component/export/export.modal';
import { IProgramExportContext, IProgramExportModel } from '@app/referential/program-strategy/program/program.model';

@Component({
  selector: 'app-program-export-modal',
  templateUrl: './program.export.modal.html',
  styleUrls: ['./program.export.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgramExportModal extends ExportModal<IProgramExportModel, IProgramExportContext> {
  constructor(protected injector: Injector) {
    super(injector);
  }

  protected getFormConfig(): Record<keyof IProgramExportModel, any> {
    return <Record<keyof IProgramExportModel, any>>{
      ...super.getFormConfig(),
      withStrategies: [false],
      withMoratoriums: [false],
    };
  }

  protected getDefaultValue(): IProgramExportModel {
    return {
      ...super.getDefaultValue(),
      withStrategies: false,
      withMoratoriums: false,
    };
  }

  protected buildExportContext(userOptions: IProgramExportModel, headers: string[], additionalHeaders: ObjectMap<string[]>): IProgramExportContext {
    return {
      ...super.buildExportContext(userOptions, headers, additionalHeaders),
      withStrategies: userOptions.withStrategies,
      withMoratoriums: userOptions.withMoratoriums,
    };
  }
}
