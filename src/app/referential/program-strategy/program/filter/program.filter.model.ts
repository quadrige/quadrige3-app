import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { DateFilter, IDateFilter } from '@app/shared/model/date-filter';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'ProgramFilterCriteriaVO' })
export class ProgramFilterCriteria extends ReferentialFilterCriteria<Program, string> implements IProgramFilterCriteria {
  static fromObject: (source: any, opts?: any) => ProgramFilterCriteria;

  metaProgramFilter: StrReferentialFilterCriteria = null;
  strategyFilter: IntReferentialFilterCriteria = null;
  monitoringLocationFilter: IntReferentialFilterCriteria = null;
  dateFilter: DateFilter = null;
  managedOnly: boolean = null;
  writableOnly: boolean = null;

  constructor() {
    super(ProgramFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.metaProgramFilter = StrReferentialFilterCriteria.fromObject(source.metaProgramFilter || {});
    this.strategyFilter = IntReferentialFilterCriteria.fromObject(source.strategyFilter || {});
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.dateFilter = DateFilter.fromObject(source.dateFilter || {});
    this.managedOnly = source.managedOnly;
    this.writableOnly = source.writableOnly;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.metaProgramFilter = this.metaProgramFilter?.asObject(opts);
    target.strategyFilter = { ...this.strategyFilter?.asObject(opts), searchAttributes: ['id', 'name', 'description'] };
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    target.dateFilter = this.dateFilter?.asObject();
    target.managedOnly = this.managedOnly || false;
    target.writableOnly = this.writableOnly || false;
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'metaProgramFilter') return !BaseFilterUtils.isCriteriaEmpty(this.metaProgramFilter, true);
    if (key === 'strategyFilter') return !BaseFilterUtils.isCriteriaEmpty(this.strategyFilter, true);
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    if (key === 'dateFilter') return this.dateFilter && !this.dateFilter.isEmpty();
    // always consider managedOnly, writableOnly as empty
    if (key === 'managedOnly' || key === 'writableOnly') return false;
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

export interface IProgramFilterCriteria extends IReferentialFilterCriteria<string> {
  metaProgramFilter: IReferentialFilterCriteria<string>;
  strategyFilter: IReferentialFilterCriteria<number>;
  monitoringLocationFilter: IReferentialFilterCriteria<number>;
  dateFilter: IDateFilter;
  managedOnly: boolean;
  writableOnly: boolean;
}

@EntityClass({ typename: 'ProgramFilterVO' })
export class ProgramFilter extends ReferentialFilter<ProgramFilter, ProgramFilterCriteria, Program, string> {
  static fromObject: (source: any, opts?: any) => ProgramFilter;

  constructor() {
    super(ProgramFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): ProgramFilterCriteria {
    return ProgramFilterCriteria.fromObject(source, opts);
  }
}
