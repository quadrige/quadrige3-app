import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '../../../component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { ProgramFilter, ProgramFilterCriteria } from './program.filter.model';
import { Params } from '@angular/router';
import { StrategyFilterCriteria } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { isNotEmptyArray, isNotNilOrBlank } from '@sumaris-net/ngx-components';
import { ReferentialModule } from '@app/referential/referential.module';
import { DateRangeFilterFormField } from '@app/shared/component/filter/date-range-filter-form-field.component';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-program-filter-form',
  templateUrl: './program.filter.form.html',
  standalone: true,
  imports: [ReferentialModule, DateRangeFilterFormField],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgramFilterForm extends ReferentialCriteriaFormComponent<ProgramFilter, ProgramFilterCriteria> {
  constructor() {
    super();
  }

  toStrategyQueryParam(): Params {
    const criterias = this.base().criterias?.map((criteria) => {
      // Build strategy filter from the current filter
      const params: PartialRecord<keyof StrategyFilterCriteria, any> = {};
      if (isNotNilOrBlank(criteria.strategyFilter?.searchText)) params.searchText = criteria.strategyFilter.searchText;
      if (isNotEmptyArray(criteria.strategyFilter?.includedIds)) {
        params.includedIds = criteria.strategyFilter.includedIds;
      }
      if (isNotEmptyArray(criteria.strategyFilter?.excludedIds)) {
        params.excludedIds = criteria.strategyFilter.excludedIds;
      }
      // Add monitoring location filter
      this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
      // Add date filter
      this.dateCriteriaToQueryParams(params, criteria, 'dateFilter');
      return params;
    });
    const criteriaArray = criterias?.filter((criteria) => isNotEmptyArray(Object.keys(criteria))).map((criteria) => JSON.stringify(criteria)) || [];
    return isNotEmptyArray(criteriaArray) ? { criterias: criteriaArray } : undefined;
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof ProgramFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias.concat('monitoringLocationFilter', 'dateFilter');
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: ProgramFilterCriteria): PartialRecord<keyof ProgramFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'strategyFilter');
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    this.dateCriteriaToQueryParams(params, criteria, 'dateFilter');
    return params;
  }
}
