import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class ProgramValidatorService extends ReferentialValidatorService<Program, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: ProgramService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Program, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.compose([Validators.required, Validators.maxLength(lengthDescription)])],
      strategyCount: [data?.strategyCount || null],
      moratoriumCount: [data?.moratoriumCount || null],
      programLocations: [data?.programLocations || null],
      managerDepartmentIds: [data?.managerDepartmentIds || null],
      recorderDepartmentIds: [data?.recorderDepartmentIds || null],
      fullViewerDepartmentIds: [data?.fullViewerDepartmentIds || null],
      viewerDepartmentIds: [data?.viewerDepartmentIds || null],
      validatorDepartmentIds: [data?.validatorDepartmentIds || null],
      managerUserIds: [data?.managerUserIds || null],
      recorderUserIds: [data?.recorderUserIds || null],
      fullViewerUserIds: [data?.fullViewerUserIds || null],
      viewerUserIds: [data?.viewerUserIds || null],
      validatorUserIds: [data?.validatorUserIds || null],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: Program, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredOneArrayNotEmpty(['managerDepartmentIds', 'managerUserIds'], 'REFERENTIAL.PROGRAM.ERROR.MISSING_MANAGER'),
      ],
    };
  }
}
