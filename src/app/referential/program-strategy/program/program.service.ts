import { Injectable, Injector } from '@angular/core';
import { isEmptyArray, isNotNilOrBlank } from '@sumaris-net/ngx-components';
import {
  BaseReferentialService,
  defaultReferentialSaveOption,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { jobFragments } from '@app/social/job/job.service';
import { ProgramFilter, ProgramFilterCriteria } from '@app/referential/program-strategy/program/filter/program.filter.model';

const programLocationFragment = gql`
  fragment ProgramLocationFragment on ProgramLocationVO {
    id
    programId
    monitoringLocationId
    monitoringLocation {
      ...ReferentialFragment
    }
    updateDate
  }
  ${referentialFragments.light}
`;

export const programFragments = {
  program: gql`
    fragment ProgramFragment on ProgramVO {
      id
      name
      description
      strategyCount
      moratoriumCount
      programLocations {
        ...ProgramLocationFragment
      }
      departmentIdsByPrivileges
      userIdsByPrivileges
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
    ${programLocationFragment}
  `,
  programLight: gql`
    fragment ProgramLightFragment on ProgramVO {
      id
      name
      description
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  load: gql`
    query Program($id: String) {
      data: program(id: $id) {
        ...ProgramFragment
      }
    }
    ${programFragments.program}
  `,

  loadAll: gql`
    query Programs($page: PageInput, $filter: ProgramFilterVOInput) {
      data: programs(page: $page, filter: $filter) {
        ...ProgramFragment
      }
    }
    ${programFragments.program}
  `,

  loadAllLight: gql`
    query ProgramsLight($page: PageInput, $filter: ProgramFilterVOInput) {
      data: programs(page: $page, filter: $filter) {
        ...ProgramLightFragment
      }
    }
    ${programFragments.programLight}
  `,

  loadAllWithTotal: gql`
    query ProgramsWithTotal($page: PageInput, $filter: ProgramFilterVOInput) {
      data: programs(page: $page, filter: $filter) {
        ...ProgramFragment
      }
      total: programsCount(filter: $filter)
    }
    ${programFragments.program}
  `,

  loadAllLightWithTotal: gql`
    query ProgramsLightWithTotal($page: PageInput, $filter: ProgramFilterVOInput) {
      data: programs(page: $page, filter: $filter) {
        ...ProgramLightFragment
      }
      total: programsCount(filter: $filter)
    }
    ${programFragments.programLight}
  `,

  countAll: gql`
    query ProgramsCount($filter: ProgramFilterVOInput) {
      total: programsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportProgramsAsync($filter: ProgramFilterVOInput, $context: ProgramExportContextInput) {
      data: exportProgramsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SavePrograms($data: [ProgramVOInput], $options: ProgramSaveOptionsInput) {
      data: savePrograms(programs: $data, options: $options) {
        ...ProgramFragment
      }
    }
    ${programFragments.program}
  `,

  deleteAll: gql`
    mutation DeletePrograms($ids: [String]) {
      deletePrograms(ids: $ids)
    }
  `,
};

const otherQueries = {
  writableProgramIds: gql`
    query WritableProgramIds($userId: Int) {
      data: writableProgramIds(userId: $userId)
    }
  `,
  managedProgramIds: gql`
    query ManagedProgramIds($userId: Int) {
      data: managedProgramIds(userId: $userId)
    }
  `,
  canDeletePrograms: gql`
    query CanDeletePrograms($ids: [String]) {
      data: canDeletePrograms(ids: $ids)
    }
  `,
  canDeleteProgramLocations: gql`
    query CanDeleteProgramLocations($ids: [Int]) {
      data: canDeleteProgramLocations(ids: $ids)
    }
  `,
};

const defaultSaveOptions = {
  ...defaultReferentialSaveOption,
  withStrategies: false,
  withMoratoriums: false,
  withLocations: true,
  withPrivileges: true,
};

@Injectable({ providedIn: 'root' })
export class ProgramService extends BaseReferentialService<Program, ProgramFilter, ProgramFilterCriteria, string> {
  constructor(protected injector: Injector) {
    super(injector, Program, ProgramFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[program-service]';
  }

  async saveAll(entities: Program[], opts?: EntitySaveOptions): Promise<Program[]> {
    return super.saveAll(entities, { ...defaultSaveOptions, ...opts });
  }

  async save(entity: Program, opts?: EntitySaveOptions): Promise<Program> {
    return super.save(entity, { ...defaultSaveOptions, ...opts });
  }

  async canDeletePrograms(ids: string[]): Promise<boolean> {
    ids = ids.filter((id) => isNotNilOrBlank(id));
    if (isEmptyArray(ids)) {
      return true;
    }
    const variables: any = { ids };
    if (this._debug) console.debug(`[program-service] Check can delete programs...`, variables);
    const now = Date.now();
    const query = otherQueries.canDeletePrograms;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`[program-service] Can delete programs in ${Date.now() - now}ms`, data);
    return data;
  }

  async getWritableProgramIds(userId?: number): Promise<string[]> {
    const variables: any = { userId };
    if (this._debug) console.debug(`${this._logPrefix} Loading writable program ids...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: string[] }>({
      query: otherQueries.writableProgramIds,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data || [];
    if (this._debug) console.debug(`${this._logPrefix} writable program ids loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async getManagedProgramIds(userId?: number): Promise<string[]> {
    const variables: any = { userId };
    if (this._debug) console.debug(`${this._logPrefix} Loading managed program ids...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: string[] }>({
      query: otherQueries.managedProgramIds,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data || [];
    if (this._debug) console.debug(`${this._logPrefix} managed program ids loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async canDeleteProgramLocations(ids: number[]): Promise<boolean> {
    ids = ids.filter((id) => isNotNilOrBlank(id));
    if (isEmptyArray(ids)) {
      return true;
    }
    const variables: any = { ids };
    if (this._debug) console.debug(`[program-service] Check can delete program locations...`, variables);
    const now = Date.now();
    const query = otherQueries.canDeleteProgramLocations;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`[program-service] Can delete program locations in ${Date.now() - now}ms`, data);
    return data;
  }
}
