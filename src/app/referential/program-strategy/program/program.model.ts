import { EntityClass, isNotEmptyArray, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { programPrivilegeEnum } from '@app/referential/model/referential.constants';
import { ProgramLocation } from '@app/referential/program-strategy/program/location/program-location.model';
import { IExportModel } from '@app/shared/component/export/export.model';
import { IExportContext } from '@app/shared/service/base-entity.service';

@EntityClass({ typename: 'ProgramVO' })
export class Program extends Referential<Program, string> {
  static entityName = 'Program';
  static fromObject: (source: any, opts?: any) => Program;

  strategyCount: number = null;
  moratoriumCount: number = null;
  programLocations: ProgramLocation[] = null;
  managerDepartmentIds: number[] = null;
  recorderDepartmentIds: number[] = null;
  fullViewerDepartmentIds: number[] = null;
  viewerDepartmentIds: number[] = null;
  validatorDepartmentIds: number[] = null;
  managerUserIds: number[] = null;
  recorderUserIds: number[] = null;
  fullViewerUserIds: number[] = null;
  viewerUserIds: number[] = null;
  validatorUserIds: number[] = null;

  constructor() {
    super(Program.TYPENAME);
    this.entityName = Program.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Program.entityName;
    this.strategyCount = source.strategyCount;
    this.moratoriumCount = source.moratoriumCount;
    this.programLocations = source.programLocations?.map(ProgramLocation.fromObject) || [];
    this.managerDepartmentIds = source.managerDepartmentIds || source.departmentIdsByPrivileges?.[programPrivilegeEnum.manager] || [];
    this.recorderDepartmentIds = source.recorderDepartmentIds || source.departmentIdsByPrivileges?.[programPrivilegeEnum.recorder] || [];
    this.fullViewerDepartmentIds = source.fullViewerDepartmentIds || source.departmentIdsByPrivileges?.[programPrivilegeEnum.fullViewer] || [];
    this.viewerDepartmentIds = source.viewerDepartmentIds || source.departmentIdsByPrivileges?.[programPrivilegeEnum.viewer] || [];
    this.validatorDepartmentIds = source.validatorDepartmentIds || source.departmentIdsByPrivileges?.[programPrivilegeEnum.validator] || [];
    this.managerUserIds = source.managerUserIds || source.userIdsByPrivileges?.[programPrivilegeEnum.manager] || [];
    this.recorderUserIds = source.recorderUserIds || source.userIdsByPrivileges?.[programPrivilegeEnum.recorder] || [];
    this.fullViewerUserIds = source.fullViewerUserIds || source.userIdsByPrivileges?.[programPrivilegeEnum.fullViewer] || [];
    this.viewerUserIds = source.viewerUserIds || source.userIdsByPrivileges?.[programPrivilegeEnum.viewer] || [];
    this.validatorUserIds = source.validatorUserIds || source.userIdsByPrivileges?.[programPrivilegeEnum.validator] || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.programLocations = this.programLocations?.map((value) => value.asObject(opts)) || undefined;
    if (!opts || opts.minify !== false) {
      const departmentIdsByPrivileges = {};
      if (isNotEmptyArray(this.managerDepartmentIds)) {
        departmentIdsByPrivileges[programPrivilegeEnum.manager] = this.managerDepartmentIds;
      }
      if (isNotEmptyArray(this.recorderDepartmentIds)) {
        departmentIdsByPrivileges[programPrivilegeEnum.recorder] = this.recorderDepartmentIds;
      }
      if (isNotEmptyArray(this.fullViewerDepartmentIds)) {
        departmentIdsByPrivileges[programPrivilegeEnum.fullViewer] = this.fullViewerDepartmentIds;
      }
      if (isNotEmptyArray(this.viewerDepartmentIds)) {
        departmentIdsByPrivileges[programPrivilegeEnum.viewer] = this.viewerDepartmentIds;
      }
      if (isNotEmptyArray(this.validatorDepartmentIds)) {
        departmentIdsByPrivileges[programPrivilegeEnum.validator] = this.validatorDepartmentIds;
      }
      target.departmentIdsByPrivileges = departmentIdsByPrivileges;
      const userIdsByPrivileges = {};
      if (isNotEmptyArray(this.managerUserIds)) {
        userIdsByPrivileges[programPrivilegeEnum.manager] = this.managerUserIds;
      }
      if (isNotEmptyArray(this.recorderUserIds)) {
        userIdsByPrivileges[programPrivilegeEnum.recorder] = this.recorderUserIds;
      }
      if (isNotEmptyArray(this.fullViewerUserIds)) {
        userIdsByPrivileges[programPrivilegeEnum.fullViewer] = this.fullViewerUserIds;
      }
      if (isNotEmptyArray(this.viewerUserIds)) {
        userIdsByPrivileges[programPrivilegeEnum.viewer] = this.viewerUserIds;
      }
      if (isNotEmptyArray(this.validatorUserIds)) {
        userIdsByPrivileges[programPrivilegeEnum.validator] = this.validatorUserIds;
      }
      target.userIdsByPrivileges = userIdsByPrivileges;
      delete target.managerDepartmentIds;
      delete target.recorderDepartmentIds;
      delete target.fullViewerDepartmentIds;
      delete target.viewerDepartmentIds;
      delete target.validatorDepartmentIds;
      delete target.managerUserIds;
      delete target.recorderUserIds;
      delete target.fullViewerUserIds;
      delete target.viewerUserIds;
      delete target.validatorUserIds;
    }
    return target;
  }
}

export interface IProgramExportModel extends IExportModel {
  withStrategies: boolean;
  withMoratoriums: boolean;
}

export interface IProgramExportContext extends IExportContext {
  withStrategies: boolean;
  withMoratoriums: boolean;
}
