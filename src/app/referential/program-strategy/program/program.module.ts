import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { ProgramTable } from '@app/referential/program-strategy/program/program.table';
import { ProgramLocationTable } from '@app/referential/program-strategy/program/location/program-location.table';
import { ProgramFilterForm } from '@app/referential/program-strategy/program/filter/program.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { ProgramExportModal } from '@app/referential/program-strategy/program/export/program.export.modal';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';

@NgModule({
  imports: [ReferentialModule, ProgramFilterForm, ProgramLocationTable, GenericSelectTable, UserSelectTable, DepartmentSelectTable],
  declarations: [ProgramTable, ProgramExportModal],
  exports: [ProgramTable],
})
export class ProgramModule {}
