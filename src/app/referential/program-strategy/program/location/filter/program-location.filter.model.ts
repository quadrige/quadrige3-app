import { FilterFn, isNotNil } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ProgramLocation } from '@app/referential/program-strategy/program/location/program-location.model';

export class ProgramLocationFilterCriteria extends ReferentialFilterCriteria<ProgramLocation, number> {
  static fromObject(source: any, opts?: any): ProgramLocationFilterCriteria {
    const target = new ProgramLocationFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  protected buildFilter(): FilterFn<ProgramLocation>[] {
    const target: FilterFn<ProgramLocation>[] = []; // don't call super.buildFilter();

    // Filter by status
    if (isNotNil(this.statusId)) {
      target.push((entity) => this.statusId === entity.monitoringLocation?.statusId);
    }

    // Filter by text search
    const searchTextFilter = EntityUtils.searchTextFilter(
      ['monitoringLocation.id', 'monitoringLocation.label', 'monitoringLocation.name'],
      this.searchText
    );
    if (searchTextFilter) target.push(searchTextFilter);

    return target;
  }
}

export class ProgramLocationFilter extends ReferentialFilter<ProgramLocationFilter, ProgramLocationFilterCriteria, ProgramLocation> {
  static fromObject(source: any, opts?: any): ProgramLocationFilter {
    const target = new ProgramLocationFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): ProgramLocationFilterCriteria {
    return ProgramLocationFilterCriteria.fromObject(source, opts);
  }
}
