import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import {
  ProgramLocationFilter,
  ProgramLocationFilterCriteria,
} from '@app/referential/program-strategy/program/location/filter/program-location.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-program-location-filter-form',
  templateUrl: './program-location.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgramLocationFilterForm extends ReferentialCriteriaFormComponent<ProgramLocationFilter, ProgramLocationFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabelName.map((attribute) => `monitoringLocation.${attribute}`);
  }
}
