import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, OnInit, Output } from '@angular/core';
import { isEmptyArray, isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { filter } from 'rxjs/operators';
import { ProgramLocation } from '@app/referential/program-strategy/program/location/program-location.model';
import { ProgramLocationService } from '@app/referential/program-strategy/program/location/program-location.service';
import { IAddResult } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { IntReferential } from '@app/referential/model/referential.model';
import { IMonitoringLocationAddOptions } from '@app/referential/monitoring-location/monitoring-location.add.table';
import {
  ProgramLocationFilter,
  ProgramLocationFilterCriteria,
} from '@app/referential/program-strategy/program/location/filter/program-location.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { ProgramLocationFilterForm } from '@app/referential/program-strategy/program/location/filter/program-location.filter.form';

@Component({
  selector: 'app-program-location-table',
  templateUrl: './program-location.table.html',
  standalone: true,
  imports: [ReferentialModule, ProgramLocationFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgramLocationTable
  extends ReferentialMemoryTable<ProgramLocation, number, ProgramLocationFilter, ProgramLocationFilterCriteria /*, ProgramLocationValidatorService*/>
  implements OnInit, AfterViewInit
{
  @Output() selectionChanged = new EventEmitter<ProgramLocation[]>();

  programId: string;

  constructor(
    protected injector: Injector,
    protected _entityService: ProgramLocationService // protected validatorService: ProgramLocationValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['monitoringLocation.label', 'monitoringLocation.name', 'monitoringLocation.statusId', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      ProgramLocation,
      _entityService,
      undefined
    );

    this.titleI18n = 'REFERENTIAL.PROGRAM_LOCATION.TITLE';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.PROGRAM_LOCATION.';
    this.defaultSortBy = 'monitoringLocation.label';
    this.logPrefix = '[program-location-table]';
  }

  ngOnInit() {
    this.subTable = true;
    this.inlineEdition = false;
    // this.showPaginator = false;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;

    // Listen end of loading to refresh total row count
    this.registerSubscription(
      this.loadingSubject.pipe(filter((value) => !value)).subscribe(() => {
        if (this.value === undefined) {
          // this will remove the 'no result' template if the selection is voluntarily empty (=== undefined)
          this.totalRowCount = null;
        }
      })
    );
  }

  async addRow(event?: Event): Promise<boolean> {
    await this.openAddModal(event);
    return false;
  }

  async openAddModal(event: Event) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }
    await this.save();
    this.selection.clear();

    const { role, data } = await this.modalService.openModal<IMonitoringLocationAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: MonitoringLocation.entityName,
        titleI18n: 'REFERENTIAL.PROGRAM_LOCATION.ADD',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          excludedIds: this.value.map((value) => value.monitoringLocation.id),
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      this.markAsDirty();
      const newValue = this.value?.slice() || [];
      newValue.push(
        ...data.added.map(IntReferential.fromObject).map((monitoringLocation) => new ProgramLocation(this.programId, monitoringLocation))
      );
      await this.setSelected(newValue);
      this.selectionChanged.emit(this.value);
      await this.resetFilter();
    }
  }

  async removeSelection(event?: UIEvent) {
    if (event) {
      event.stopPropagation();
    }
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Check if can delete
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // remove from selection
    const removedIds = toDelete.map((row) => row.currentData.monitoringLocation.id);
    const newValue = this.value.slice().filter((programLocation) => !removedIds.includes(programLocation.monitoringLocation.id));

    this.markAsDirty();
    await this.setSelected(newValue, { resetFilter: false });
    this.selectionChanged.emit(this.value);
    await this.resetFilter();
  }

  async setSelected(value: ProgramLocation[], opts?: { resetFilter?: boolean; emitEvent?: boolean }) {
    if (!opts || opts.resetFilter !== false) {
      await this.resetFilter(undefined, { emitEvent: false });
    }
    await this.setValue(value);
  }
}
