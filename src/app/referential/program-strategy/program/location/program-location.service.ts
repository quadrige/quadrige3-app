import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { ProgramLocation } from '@app/referential/program-strategy/program/location/program-location.model';
import { ProgramLocationFilter } from '@app/referential/program-strategy/program/location/filter/program-location.filter.model';

@Injectable({ providedIn: 'root' })
export class ProgramLocationService extends EntitiesMemoryService<ProgramLocation, ProgramLocationFilter> {
  constructor() {
    super(ProgramLocation, ProgramLocationFilter, { sortByReplacement: { id: 'monitoringLocation.id' } });
  }
}
