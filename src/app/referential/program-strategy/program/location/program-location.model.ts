import { EntityClass, MINIFY_ENTITY_FOR_POD, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { Feature } from 'geojson';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'ProgramLocationVO' })
export class ProgramLocation extends Referential<ProgramLocation> {
  static entityName = 'ProgramLocation';
  static fromObject: (source: any, opts?: any) => ProgramLocation;

  programId: string = null;
  monitoringLocationId: number = null;
  monitoringLocation: IntReferential = null;
  geometry: Feature = null;
  geometryLoaded = false;

  constructor(programId?: string, monitoringLocation?: IntReferential) {
    super(ProgramLocation.TYPENAME);
    this.entityName = ProgramLocation.entityName;
    this.programId = programId;
    this.monitoringLocation = monitoringLocation;
    this.monitoringLocationId = monitoringLocation?.id;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = ProgramLocation.entityName;
    this.programId = source.programId;
    this.monitoringLocation = IntReferential.fromObject(source.monitoringLocation);
    this.monitoringLocationId = this.monitoringLocation?.id || source.monitoringLocationId;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject({ ...MINIFY_ENTITY_FOR_POD, ...opts });
    target.programId = this.programId;
    target.monitoringLocation = EntityUtils.asMinifiedObject(this.monitoringLocation, opts);
    target.monitoringLocationId = this.monitoringLocation?.id || this.monitoringLocationId;
    delete target.entityName;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.geometry;
    delete target.geometryLoaded;
    return target;
  }
}
