import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { ProgramValidatorService } from '@app/referential/program-strategy/program/program.validator';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { ProgramTable } from '@app/referential/program-strategy/program/program.table';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { isNotNilOrBlank } from '@sumaris-net/ngx-components';
import { IAddTable } from '@app/shared/table/table.model';
import { ProgramFilter } from '@app/referential/program-strategy/program/filter/program.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { ProgramFilterForm } from '@app/referential/program-strategy/program/filter/program.filter.form';
import { ProgramLocationTable } from '@app/referential/program-strategy/program/location/program-location.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';

@Component({
  selector: 'app-program-add-table',
  templateUrl: './program.table.html',
  styleUrls: ['./program.table.scss'],
  standalone: true,
  imports: [ReferentialModule, ProgramFilterForm, ProgramLocationTable, UserSelectTable, DepartmentSelectTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgramAddTable extends ProgramTable implements OnInit, AfterViewInit, IAddTable<Program> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;
  @Input() currentProgramId: string;

  constructor(
    protected injector: Injector,
    protected _entityService: ProgramService,
    protected validatorService: ProgramValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[program-add-table]';
    this.addTable = true;
  }

  ngOnInit() {
    super.ngOnInit();
    this.autoSaveFilter = false;

    this.setShowColumn('strategies', false);
    this.setShowColumn('moratoriums', false);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    // Try to select the entity with searched id
    if (isNotNilOrBlank(this.currentProgramId)) {
      this.ready().then(async () => {
        await this.resetFilter();
        await this.waitIdle();
        this.selectRowByData(Program.fromObject({ id: this.currentProgramId })).then(() => this.markForCheck());
      });
    }
  }

  async resetFilter(filter?: ProgramFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  updatePermission() {
    // Don't update permissions
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('strategies', 'moratoriums');
  }
}
