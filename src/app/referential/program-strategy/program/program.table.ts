import { ChangeDetectionStrategy, Component, inject, Injector, OnInit, viewChild } from '@angular/core';
import {
  isEmptyArray,
  isNilOrBlank,
  isNotEmptyArray,
  PromiseEvent,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { IProgramExportModel, Program } from '@app/referential/program-strategy/program/program.model';
import { ProgramValidatorService } from '@app/referential/program-strategy/program/program.validator';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { ProgramLocationTable } from '@app/referential/program-strategy/program/location/program-location.table';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { entityId, EntityUtils } from '@app/shared/entity.utils';
import { MapComponent } from '@app/shared/component/map/map.component';
import { ProgramLocation } from '@app/referential/program-strategy/program/location/program-location.model';
import { MonitoringLocationService } from '@app/referential/monitoring-location/monitoring-location.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { attributes, programPrivilegeEnum, referentialOptions } from '@app/referential/model/referential.constants';
import { FeatureRecord } from '@app/shared/geometries';
import { Feature } from 'geojson';
import { ProgramFilter, ProgramFilterCriteria } from '@app/referential/program-strategy/program/filter/program.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ProgramFilterForm } from '@app/referential/program-strategy/program/filter/program.filter.form';
import { ExportOptions, IRightSelectTable } from '@app/shared/table/table.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { TranscribingItemUtils } from '@app/referential/transcribing-item/transcribing-item.model';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { IAddResult, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { IExportModalOptions } from '@app/shared/component/export/export.modal';
import { ProgramExportModal } from '@app/referential/program-strategy/program/export/program.export.modal';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { User } from '@app/referential/user/user.model';
import { Alerts } from '@app/shared/alerts';
import { Department } from '@app/referential/department/department.model';

type ProgramView = 'userTable' | 'departmentTable' | 'locationTable' | 'locationMap' | TranscribingItemView;

@Component({
  selector: 'app-program-table',
  templateUrl: './program.table.html',
  styleUrls: ['./program.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgramTable
  extends ReferentialTable<Program, string, ProgramFilter, ProgramFilterCriteria, ProgramValidatorService, ProgramView>
  implements OnInit
{
  userTable = viewChild<UserSelectTable>('userTable');
  departmentTable = viewChild<DepartmentSelectTable>('departmentTable');
  programLocationTable = viewChild<ProgramLocationTable>('programLocationTable');
  map = viewChild<MapComponent>('map');

  readonly managerMenuItem: IEntityMenuItem<Program, ProgramView> = {
    title: 'REFERENTIAL.RIGHT.PROGRAM.MANAGERS',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.USERS',
        attribute: 'managerUserIds',
        view: 'userTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.manager },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.MANAGER.USER',
      },
      {
        title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
        attribute: 'managerDepartmentIds',
        view: 'departmentTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.manager },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.MANAGER.DEPARTMENT',
      },
    ],
  };
  readonly recorderMenuItem: IEntityMenuItem<Program, ProgramView> = {
    title: 'REFERENTIAL.RIGHT.PROGRAM.RECORDERS',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.USERS',
        attribute: 'recorderUserIds',
        view: 'userTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.recorder },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.RECORDER.USER',
      },
      {
        title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
        attribute: 'recorderDepartmentIds',
        view: 'departmentTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.recorder },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.RECORDER.DEPARTMENT',
      },
    ],
  };
  readonly fullViewerMenuItem: IEntityMenuItem<Program, ProgramView> = {
    title: 'REFERENTIAL.RIGHT.PROGRAM.FULL_VIEWERS',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.USERS',
        attribute: 'fullViewerUserIds',
        view: 'userTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.fullViewer },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.FULL_VIEWER.USER',
      },
      {
        title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
        attribute: 'fullViewerDepartmentIds',
        view: 'departmentTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.fullViewer },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.FULL_VIEWER.DEPARTMENT',
      },
    ],
  };
  readonly viewerMenuItem: IEntityMenuItem<Program, ProgramView> = {
    title: 'REFERENTIAL.RIGHT.PROGRAM.VIEWERS',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.USERS',
        attribute: 'viewerUserIds',
        view: 'userTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.viewer },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.VIEWER.USER',
      },
      {
        title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
        attribute: 'viewerDepartmentIds',
        view: 'departmentTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.viewer },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.VIEWER.DEPARTMENT',
      },
    ],
  };
  readonly validatorMenuItem: IEntityMenuItem<Program, ProgramView> = {
    title: 'REFERENTIAL.RIGHT.PROGRAM.VALIDATORS',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.USERS',
        attribute: 'validatorUserIds',
        view: 'userTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.validator },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.VALIDATOR.USER',
      },
      {
        title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
        attribute: 'validatorDepartmentIds',
        view: 'departmentTable',
        viewArgs: { programPrivilegeId: programPrivilegeEnum.validator },
        viewTitle: 'REFERENTIAL.PROGRAM.PRIVILEGE.VALIDATOR.DEPARTMENT',
      },
    ],
  };
  readonly locationMenuItem: IEntityMenuItem<Program, ProgramView> = {
    title: 'REFERENTIAL.PROGRAM.PROGRAM_LOCATION.TITLE',
    children: [
      {
        title: 'COMMON.TABLE',
        attribute: 'programLocations',
        view: 'locationTable',
        viewTitle: 'REFERENTIAL.PROGRAM.PROGRAM_LOCATION.TITLE',
      },
      {
        title: 'COMMON.MAP',
        attribute: 'programLocations',
        view: 'locationMap',
        viewTitle: 'REFERENTIAL.PROGRAM.PROGRAM_LOCATION.MAP',
      },
    ],
  };

  rightAreaEnabled = false;

  protected monitoringLocationService = inject(MonitoringLocationService);
  protected strategyService = inject(StrategyService);

  constructor(
    protected injector: Injector,
    protected _entityService: ProgramService,
    protected validatorService: ProgramValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'name',
        'description',
        'strategies',
        'moratoriums',
        'comments',
        'statusId',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      Program,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.PROGRAM.';
    this.logPrefix = '[program-table]';
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: Program = this.singleSelectedRow.currentData;
      return (
        isNotEmptyArray(data.managerDepartmentIds) ||
        isNotEmptyArray(data.managerUserIds) ||
        isNotEmptyArray(data.recorderDepartmentIds) ||
        isNotEmptyArray(data.recorderUserIds) ||
        isNotEmptyArray(data.fullViewerDepartmentIds) ||
        isNotEmptyArray(data.fullViewerUserIds) ||
        isNotEmptyArray(data.viewerDepartmentIds) ||
        isNotEmptyArray(data.viewerUserIds) ||
        isNotEmptyArray(data.validatorDepartmentIds) ||
        isNotEmptyArray(data.validatorUserIds) ||
        isNotEmptyArray(data.programLocations) ||
        TranscribingItemUtils.hasValidTranscribingItems(data)
      );
    }
    return false;
  }

  ngOnInit() {
    super.ngOnInit();
    if (!this.subTable) {
      this.registerSubscription(this.onBeforeDeleteRows.subscribe((event) => this.checkCanDeletePrograms(event)));
    }
  }

  async onAfterSelectionChange(row?: AsyncTableElement<Program>): Promise<void> {
    this.refreshButtons();
    await super.onAfterSelectionChange(row);
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(true, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<Program, TranscribingItemView | ProgramView>[] {
    return [
      this.managerMenuItem,
      this.recorderMenuItem,
      this.fullViewerMenuItem,
      this.viewerMenuItem,
      this.validatorMenuItem,
      this.locationMenuItem,
      ...super.getRightMenuItems(),
    ];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Program, TranscribingItemView | ProgramView> {
    return this.managerMenuItem.children[0];
  }

  updatePermission() {
    // Manage user rights :
    // By default, all user have edition write to allow further finer checks
    this.canEdit = true;
    // Refresh controls
    this.refreshButtons();
    this.markForCheck();
  }

  async setFilter(f: ProgramFilter, opts?: ISetFilterOptions) {
    let programIdToSelect: string;
    if (this.previousUrl) {
      // Try to reselect previous program from url (Mantis #59263)
      const test = /\/referential\/Program\/(\w+)\/(strategy|moratorium)/g.exec(this.previousUrl);
      programIdToSelect = test?.[1];
    }
    if (!!programIdToSelect) {
      // Don't emit to prevent page change (Mantis #59919)
      opts = { ...opts, emitEvent: false };
    }

    await super.setFilter(f, opts);

    if (!!programIdToSelect) {
      // Refresh manually
      this.emitRefresh();
      await this.waitIdle();
      if (this.debug) console.debug(`${this.logPrefix} Try to reselect previous program: ${programIdToSelect}`);
      await this.selectRowByData(<Program>{ id: programIdToSelect });
    }
  }

  openStrategies(event: UIEvent, row: AsyncTableElement<Program>) {
    if (event) event.stopPropagation();
    if (!row.validator.invalid) {
      // Build strategy filter from the current filter
      const queryParams = (this.filterFormComponent() as ProgramFilterForm).toStrategyQueryParam();

      // Go to strategies table
      this.router.navigate([row.currentData.id, 'strategy'], {
        relativeTo: this.route,
        queryParams,
      });
    }
  }

  openMoratoriums(event: UIEvent, row: AsyncTableElement<Program>) {
    if (event) event.stopPropagation();
    if (!row.validator.invalid) {
      // todo no filter for the moment
      // // Build strategy filter from the current filter
      // const queryParams = this.filterToQueryParam(this.filterForm.value.strategyFilter);
      // // Add monitoring location filter
      // this.referentialFilterToQueryParam(queryParams, MONITORING_LOCATION_QUERY_PARAM, this.filterForm.value.monitoringLocationFilter);
      // // Add date filter
      // this.dateFilterToQueryParam(queryParams, DATE_QUERY_PARAM, this.filterForm.value.dateFilter);

      // Go to strategies table
      this.router.navigate([row.currentData.id, 'moratorium'], {
        relativeTo: this.route,
        // queryParams,
      });
    }
  }

  rightTableSelectionChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow(this.rightMenuItem.attribute, values);
    }
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<Program>): Promise<boolean> {
    // following lines comes from super.editRow(event, row), don't remove
    if (!this._enabled) return false;
    if (this.singleEditingRow === row) return true; // Already the edited row
    if (event?.defaultPrevented) return false;
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // User without right to edit, don't turn on row edition
    if (!this.hasRightOnProgram(row.currentData)) {
      return true;
    }

    return super.editRow(event, row);
  }

  async checkCanDeletePrograms(event: PromiseEvent<boolean, { rows: AsyncTableElement<Program>[] }>) {
    let canDelete = true;
    let skipConfirm = false;
    if (isNotEmptyArray(event.detail?.rows)) {
      // Check strategies or moratorium count
      if (event.detail.rows.some((row) => row.currentData.strategyCount > 0 || row.currentData.moratoriumCount > 0)) {
        canDelete = await Alerts.askConfirmation('REFERENTIAL.PROGRAM.CONFIRM.DELETE_CHILDREN', this.alertCtrl, this.translate);
        skipConfirm = canDelete;
      }

      // Check data usage
      if (canDelete) {
        this.markAsLoading();
        canDelete = await this._entityService.canDeletePrograms(EntityUtils.ids(event.detail.rows));
        this.markAsLoaded();

        if (!canDelete) {
          await Alerts.showError('REFERENTIAL.PROGRAM.ERROR.CANNOT_DELETE', this.alertCtrl, this.translate, {
            titleKey: 'ERROR.CANNOT_DELETE',
          });
        }
      }
    }
    this.confirmBeforeDelete = !skipConfirm;
    event.detail.success(canDelete);
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    event?.preventDefault();

    await this.modalService.openModal<IExportModalOptions, IProgramExportModel>(ProgramExportModal, {
      table: this,
      locale: this.settings.locale,
      sortBy: this.sortActive,
      sortDirection: this.sortDirection,
      rowCountWarningThreshold: 50,
      transcribingItemEnabled: true,
      ...opts,
    });
  }

  async checkCanDeleteProgramLocation(event: PromiseEvent<boolean, { rows: AsyncTableElement<ProgramLocation>[] }>) {
    const programLocations: ProgramLocation[] = event.detail.rows.map((row) => row.currentData);
    const programLocationIds: number[] = programLocations.map(entityId);
    let canDelete = true;

    if (isNotEmptyArray(programLocationIds)) {
      this.markAsLoading();
      canDelete = await this._entityService.canDeleteProgramLocations(programLocationIds);
      this.markAsLoaded();

      if (!canDelete) {
        await Alerts.showError('REFERENTIAL.PROGRAM_LOCATION.ERROR.CANNOT_DELETE', this.alertCtrl, this.translate, {
          titleKey: 'ERROR.CANNOT_DELETE',
        });
        return;
      }

      const programIds: string[] = programLocations.map((progLoc) => progLoc.programId);
      let usedOnStrategy = false;

      //Check if location is used on strategy
      this.markAsLoading();
      for (const programId of programIds) {
        if (!usedOnStrategy) {
          usedOnStrategy =
            (await this.strategyService.countAll(
              StrategyFilter.fromObject({
                criterias: [
                  {
                    parentId: programId,
                    monitoringLocationFilter: { includedIds: programLocations.map((progLoc) => progLoc.monitoringLocationId) },
                  },
                ],
              })
            )) > 0;
        }
      }
      this.markAsLoaded();

      if (usedOnStrategy) {
        canDelete = await Alerts.askConfirmation(
          'REFERENTIAL.PROGRAM_LOCATION.CONFIRM.DELETE_APPLIED_STRATEGIES',
          this.alertCtrl,
          this.translate,
          undefined,
          {
            list: `<div class="scroll-content"><ul>${programLocations
              .map((p) => referentialToString(p.monitoringLocation, attributes.labelName))
              .map((s) => `<li>${s}</li>`)
              .join('')}</ul></div>`,
          }
        );
      }
      if (canDelete) {
        // Does not ask confirmation if it's already done
        this.programLocationTable().confirmBeforeDelete = !usedOnStrategy;
        event.detail.success(canDelete);
      }
    }
  }

  // protected methods

  protected updateTitle() {
    this.title = this.titleI18n
      ? this.translate.instant(this.titleI18n)
      : ['MENU.REFERENTIAL.MANAGEMENT', 'REFERENTIAL.ENTITY.PROGRAMS'].map((value) => this.translate.instant(value)).join(' &rArr; ');
  }

  protected async patchDuplicateEntity(entity: Program, opts?: any): Promise<any> {
    // Remove not duplicable children
    return { ...(await super.patchDuplicateEntity(entity, opts)), strategyCount: undefined, moratoriumCount: undefined };
  }

  protected async loadRightArea(row?: AsyncTableElement<Program>): Promise<void> {
    if (this.subTable) return; // don't load if sub table
    // if (!this.singleSelectedRow) return;
    switch (this.rightMenuItem?.view) {
      case 'userTable': {
        this.detectChanges();
        if (this.userTable()) {
          this.registerSubForm(this.userTable());
          await this.loadUserTable(row);
        }
        break;
      }
      case 'departmentTable': {
        this.detectChanges();
        if (this.departmentTable()) {
          this.registerSubForm(this.departmentTable());
          await this.loadDepartmentTable(row);
        }
        break;
      }
      case 'locationTable': {
        this.detectChanges();
        if (this.programLocationTable()) {
          this.registerSubForm(this.programLocationTable());
          await this.loadLocationTable(row);
        }
        break;
      }
      case 'locationMap': {
        this.detectChanges();
        if (this.map()) {
          await this.loadLocationMap(row);
        }
        break;
      }
    }
    await super.loadRightArea(row);
  }

  protected async loadLocationTable(row?: AsyncTableElement<Program>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.programLocationTable().programId = undefined;
      await this.programLocationTable().setValue(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    const attribute = this.rightMenuItem.attribute;
    if (isNilOrBlank(attribute)) {
      throw Error(`[program-table] no program attribute for type ${this.rightMenuItem}`);
    }

    // Load selected data
    this.programLocationTable().programId = row.currentData.id;
    await this.programLocationTable().setValue(((row.currentData?.[attribute] || []) as any[]).slice());
    this.rightAreaEnabled = this.hasRightOnProgram(row.currentData);
  }

  protected async loadLocationMap(row?: AsyncTableElement<Program>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.map().loadGeoData(undefined);
      this.rightAreaEnabled = false;
      return;
    }

    const programLocations = row.currentData.programLocations || [];
    await this.loadGeometryData(programLocations);
    await this.map().loadGeoData(programLocations.map((programLocation) => programLocation.geometry));
    this.rightAreaEnabled = this.hasRightOnProgram(row.currentData);
  }

  protected async loadGeometryData(programLocations: ProgramLocation[]) {
    this.map().markAsLoading();

    // Load missing geometries
    const geometryIdsToLoad = programLocations.filter((value) => !value.geometryLoaded).map((value) => value.monitoringLocationId);
    const geometries: FeatureRecord = await this.monitoringLocationService.loadGeometryFeatures(geometryIdsToLoad);
    if (geometries) {
      // update program locations
      Object.keys(geometries).forEach((monitoringLocationId) => {
        const programLocation = programLocations.find((value) => value.monitoringLocationId.toString() === monitoringLocationId);
        const geometry: Feature = geometries[monitoringLocationId];
        if (!!programLocation && !!geometry) {
          // Add properties
          geometry.properties = {
            ...geometry.properties,
            name: referentialToString(programLocation.monitoringLocation, referentialOptions.monitoringLocation.attributes),
          };
          programLocation.geometry = geometry;
          programLocation.geometryLoaded = true;
        } else {
          console.error(`${this.logPrefix} Unable to map monitoring location geometry`, programLocation, geometry);
        }
      });
    }

    this.map().markAsLoaded();
  }

  protected async loadUserTable(row?: AsyncTableElement<Program>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.userTable(), row);
      await this.userTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    const attribute = this.rightMenuItem.attribute;
    if (isNilOrBlank(attribute)) {
      throw Error(`${this.logPrefix} no program attribute for type ${this.rightMenuItem}`);
    }

    // Load selected data
    this.applyRightOptions(this.userTable(), row);
    await this.userTable().setSelectedIds(((row.currentData?.[attribute] || []) as any[]).slice());
    this.rightAreaEnabled = this.hasRightOnProgram(row.currentData);
  }

  protected async addUser(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'User',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.userTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.userTable());
      await this.userTable().setSelectedIds(includedIds);
      this.rightTableSelectionChanged(includedIds);
    }
  }

  protected async loadDepartmentTable(row?: AsyncTableElement<Program>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.departmentTable(), row);
      await this.departmentTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    const attribute = this.rightMenuItem.attribute;
    if (isNilOrBlank(attribute)) {
      throw Error(`${this.logPrefix} no program attribute for type ${this.rightMenuItem}`);
    }

    // Load selected data
    this.applyRightOptions(this.departmentTable(), row);
    await this.departmentTable().setSelectedIds(((row.currentData?.[attribute] || []) as any[]).slice());
    this.rightAreaEnabled = this.hasRightOnProgram(row.currentData);
  }

  protected async addDepartment(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'Department',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.departmentTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.departmentTable());
      await this.departmentTable().setSelectedIds(includedIds);
      this.rightTableSelectionChanged(includedIds);
    }
  }

  protected hasRightOnProgram(program: Program): boolean {
    if (!program) {
      return false;
    }

    // Admins and users with manager right can edit
    return (
      this.accountService.isAdmin() ||
      (this.accountService.isUser() &&
        (program.managerUserIds.includes(this.accountService.person.id) || program.managerDepartmentIds.includes(this.accountService.department.id)))
    );
  }

  /**
   * Refresh buttons state according to user profile or program rights
   *
   * @protected
   */
  protected refreshButtons() {
    // Only admins can add program
    this.tableButtons.canAdd = !this.subTable && this.accountService.isAdmin();
    this.tableButtons.canDuplicate = !this.subTable && this.accountService.isAdmin();
    this.tableButtons.canDelete =
      (!this.subTable && this.selection.hasValue() && this.selection.selected.every((row) => this.hasRightOnProgram(row.currentData))) || false;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'description', 'strategies', 'moratoriums');
  }

  protected rightSplitResized() {
    super.rightSplitResized();
    this.map()?.containerResize();
  }

  protected applyRightOptions(table: IRightSelectTable<any>, row?: AsyncTableElement<Program>) {
    row = row || this.singleEditingRow;
    table.rightOptions = {
      entityName: Program.entityName,
      programId: row?.currentData.id,
      programPrivilegeId: this.rightMenuItem.viewArgs.programPrivilegeId,
    };
  }

  async canRemoveUser(event: PromiseEvent<boolean, { rows: AsyncTableElement<User>[] }>) {
    if (this.rightMenuItem.viewArgs?.programPrivilegeId !== programPrivilegeEnum.manager) {
      event.detail.success(true);
      return;
    }
    // Warn if users to remove are responsible for any strategy
    const userIds = EntityUtils.ids(event.detail.rows);
    if (isEmptyArray(userIds)) {
      event.detail.error('No user selected');
      return;
    }
    const showConfirm = await this.strategyService.checkUsersAreResponsible(this.singleSelectedRow.currentData.id, userIds);
    let canRemove = !showConfirm;
    if (showConfirm) {
      canRemove = await Alerts.askConfirmation('REFERENTIAL.PROGRAM.CONFIRM.REMOVE_RESPONSIBLE_USERS', this.alertCtrl, this.translate);
    }
    event.detail.success(canRemove);
  }

  async canRemoveDepartment(event: PromiseEvent<boolean, { rows: AsyncTableElement<Department>[] }>) {
    if (this.rightMenuItem.viewArgs?.programPrivilegeId !== programPrivilegeEnum.manager) {
      event.detail.success(true);
      return;
    }
    // Warn if departments to remove are responsible for any strategy
    const departmentIds = EntityUtils.ids(event.detail.rows);
    if (isEmptyArray(departmentIds)) {
      event.detail.error('No department selected');
      return;
    }
    const showConfirm = await this.strategyService.checkDepartmentsAreResponsible(this.singleSelectedRow.currentData.id, departmentIds);
    let canRemove = !showConfirm;
    if (showConfirm) {
      canRemove = await Alerts.askConfirmation('REFERENTIAL.PROGRAM.CONFIRM.REMOVE_RESPONSIBLE_DEPARTMENTS', this.alertCtrl, this.translate);
    }
    event.detail.success(canRemove);
  }
}
