import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { DredgingAreaType } from '@app/referential/dredging-area-type/dredging-area-type.model';
import { Referential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'DredgingTargetAreaVO' })
export class DredgingTargetArea extends Referential<DredgingTargetArea, string> {
  static entityName = 'DredgingTargetArea';
  static fromObject: (source: any, opts?: any) => DredgingTargetArea;

  type: DredgingAreaType = null;

  constructor() {
    super(DredgingTargetArea.TYPENAME);
    this.entityName = DredgingTargetArea.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = DredgingTargetArea.entityName;
    this.type = (source.type && DredgingAreaType.fromObject(source.type)) || undefined;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.type = EntityUtils.asMinifiedObject(this.type, opts);
    return target;
  }
}
