import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { DredgingTargetArea } from '@app/referential/dredging-target-area/dredging-target-area.model';
import { DredgingTargetAreaService } from '@app/referential/dredging-target-area/dredging-target-area.service';

@Injectable({ providedIn: 'root' })
export class DredgingTargetAreaValidatorService extends ReferentialValidatorService<DredgingTargetArea, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: DredgingTargetAreaService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: DredgingTargetArea, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      type: [data?.type || null, Validators.compose([Validators.required, SharedValidators.entity])],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
