import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { DredgingTargetArea } from '@app/referential/dredging-target-area/dredging-target-area.model';
import { DredgingTargetAreaService } from '@app/referential/dredging-target-area/dredging-target-area.service';
import { DredgingTargetAreaValidatorService } from '@app/referential/dredging-target-area/dredging-target-area.validator';
import { DredgingAreaTypeService } from '@app/referential/dredging-area-type/dredging-area-type.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-dredging-target-area-table',
  templateUrl: './dredging-target-area.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DredgingTargetAreaTable
  extends ReferentialTable<DredgingTargetArea, string, StrReferentialFilter, StrReferentialFilterCriteria, DredgingTargetAreaValidatorService>
  implements OnInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: DredgingTargetAreaService,
    protected typeReferentialService: DredgingAreaTypeService,
    protected validatorService: DredgingTargetAreaValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['type', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      DredgingTargetArea,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.DREDGING_TARGET_AREA.';
    this.logPrefix = '[dredging-target-area-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    // type combo
    this.registerAutocompleteField('type', {
      service: this.typeReferentialService,
      attributes: ['description', 'id'],
      columnNames: ['REFERENTIAL.DESCRIPTION', 'REFERENTIAL.CODE'],
      columnSizes: [8, 4],
      filter: StrReferentialFilter.fromObject({ criterias: [this.defaultReferentialCriteria] }),
    });
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.DREDGING_TARGET_AREAS' });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('type');
  }
}
