import { NgModule } from '@angular/core';
import { DredgingTargetAreaTable } from '@app/referential/dredging-target-area/dredging-target-area.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { DredgingTargetAreaFilterForm } from '@app/referential/dredging-target-area/filter/dredging-target-area.filter.form';

@NgModule({
  imports: [ReferentialModule],
  declarations: [DredgingTargetAreaTable, DredgingTargetAreaFilterForm],
  exports: [DredgingTargetAreaTable, DredgingTargetAreaFilterForm],
})
export class DredgingTargetAreaModule {}
