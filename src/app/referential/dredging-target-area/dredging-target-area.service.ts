import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { dredgingAreaTypeFragments } from '@app/referential/dredging-area-type/dredging-area-type.service';
import { DredgingTargetArea } from '@app/referential/dredging-target-area/dredging-target-area.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { jobFragments } from '@app/social/job/job.service';

const fragments = {
  dredgingTargetArea: gql`
    fragment DredgingTargetAreaFragment on DredgingTargetAreaVO {
      id
      type {
        ...DredgingAreaTypeFragment
      }
      description
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query DredgingTargetAreas($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: dredgingTargetAreas(page: $page, filter: $filter) {
        ...DredgingTargetAreaFragment
      }
    }
    ${fragments.dredgingTargetArea}
    ${dredgingAreaTypeFragments.dredgingAreaType}
  `,

  loadAllWithTotal: gql`
    query DredgingTargetAreasWithTotal($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: dredgingTargetAreas(page: $page, filter: $filter) {
        ...DredgingTargetAreaFragment
      }
      total: dredgingTargetAreasCount(filter: $filter)
    }
    ${fragments.dredgingTargetArea}
    ${dredgingAreaTypeFragments.dredgingAreaType}
  `,

  countAll: gql`
    query DredgingTargetAreasCount($filter: StrReferentialFilterVOInput) {
      total: dredgingTargetAreasCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportDredgingTargetAreasAsync($filter: StrReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportDredgingTargetAreasAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveDredgingTargetAreas($data: [DredgingTargetAreaVOInput]) {
      data: saveDredgingTargetAreas(dredgingTargetAreas: $data) {
        ...DredgingTargetAreaFragment
      }
    }
    ${fragments.dredgingTargetArea}
    ${dredgingAreaTypeFragments.dredgingAreaType}
  `,

  deleteAll: gql`
    mutation DeleteDredgingTargetAreas($ids: [String]) {
      deleteDredgingTargetAreas(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class DredgingTargetAreaService extends BaseReferentialService<
  DredgingTargetArea,
  StrReferentialFilter,
  StrReferentialFilterCriteria,
  string
> {
  constructor(protected injector: Injector) {
    super(injector, DredgingTargetArea, StrReferentialFilter, { queries, mutations });
    this._logPrefix = '[dredging-target-area-service]';
  }
}
