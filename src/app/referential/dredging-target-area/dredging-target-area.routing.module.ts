import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { DredgingTargetAreaTable } from '@app/referential/dredging-target-area/dredging-target-area.table';
import { DredgingTargetAreaModule } from '@app/referential/dredging-target-area/dredging-target-area.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DredgingTargetAreaTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), DredgingTargetAreaModule],
})
export class DredgingTargetAreaRoutingModule {}
