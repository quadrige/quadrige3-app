import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { TaxonName } from '@app/referential/taxon-name/taxon-name.model';
import { TaxonNameService } from '@app/referential/taxon-name/taxon-name.service';
import { TaxonomicLevelService } from '@app/referential/taxon-name/taxonomic-level.service';
import { TaxonNameFilter, TaxonNameFilterCriteria } from '@app/referential/taxon-name/filter/taxon-name.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-taxon-name-table',
  templateUrl: './taxon-name.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonNameTable extends ReferentialTable<TaxonName, number, TaxonNameFilter, TaxonNameFilterCriteria> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: TaxonNameService,
    protected taxonomicLevelService: TaxonomicLevelService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'completeName',
        // 'name',
        'comments',
        'taxonomicLevel',
        // 'referenceTaxon.id',
        'referenceTaxon.completeName',
        'citationName',
        // 'rankOrder',
        // 'naming',
        'referent',
        'virtual',
        'obsolete',
        'temporary',
        // 'creationDate',
        // 'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      TaxonName,
      _entityService,
      undefined
    );

    this.i18nColumnPrefix = 'REFERENTIAL.TAXON_NAME.';
    this.defaultSortBy = 'completeName';
    this.logPrefix = '[taxon-name-table]';
    this.setShowColumn('statusId', false);
  }

  ngOnInit() {
    super.ngOnInit();

    // Type combo
    this.registerAutocompleteField('taxonomicLevel', {
      ...this.referentialOptions.taxonomicLevel,
      service: this.taxonomicLevelService,
      filter: {},
    });
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.TAXON_NAMES' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('name', 'comments', 'citationName', 'rankOrder', 'referenceTaxon.id', 'naming');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('completeName', 'taxonomicLevel');
  }
}
