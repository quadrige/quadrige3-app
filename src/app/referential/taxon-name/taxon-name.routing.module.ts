import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { TaxonNameModule } from '@app/referential/taxon-name/taxon-name.module';
import { TaxonNameTable } from '@app/referential/taxon-name/taxon-name.table';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: TaxonNameTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), TaxonNameModule],
})
export class TaxonNameRoutingModule {}
