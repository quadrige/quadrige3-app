import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { TaxonNameService } from '@app/referential/taxon-name/taxon-name.service';
import { TaxonNameTable } from '@app/referential/taxon-name/taxon-name.table';
import { TaxonomicLevelService } from '@app/referential/taxon-name/taxonomic-level.service';
import { TaxonNameFilter } from '@app/referential/taxon-name/filter/taxon-name.filter.model';
import { TaxonName } from '@app/referential/taxon-name/taxon-name.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { TaxonNameFilterForm } from '@app/referential/taxon-name/filter/taxon-name.filter.form';

@Component({
  selector: 'app-taxon-name-add-table',
  templateUrl: './taxon-name.table.html',
  standalone: true,
  imports: [ReferentialModule, TaxonNameFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonNameAddTable extends TaxonNameTable implements IAddTable<TaxonName> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: TaxonNameService,
    protected taxonomicLevelService: TaxonomicLevelService
  ) {
    super(injector, _entityService, taxonomicLevelService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[taxon-name-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: TaxonNameFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
