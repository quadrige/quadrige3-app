import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { TaxonNameTable } from '@app/referential/taxon-name/taxon-name.table';
import { TaxonNameFilterForm } from '@app/referential/taxon-name/filter/taxon-name.filter.form';

@NgModule({
  imports: [ReferentialModule, TaxonNameFilterForm],
  declarations: [TaxonNameTable],
  exports: [TaxonNameTable],
})
export class TaxonNameModule {}
