import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { TaxonNameFilter, TaxonNameFilterCriteria } from '@app/referential/taxon-name/filter/taxon-name.filter.model';
import { PartialRecord } from '@app/shared/model/interface';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-taxon-name-filter-form',
  templateUrl: './taxon-name.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonNameFilterForm extends ReferentialCriteriaFormComponent<TaxonNameFilter, TaxonNameFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.id.concat('completeName');
  }

  protected criteriaToQueryParams(criteria: TaxonNameFilterCriteria): PartialRecord<keyof TaxonNameFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'taxonGroupFilter');
    this.subCriteriaToQueryParams(params, criteria, 'taxonomicLevel');
    if (isNotNil(criteria.referent)) params.referent = criteria.referent;
    if (isNotNil(criteria.virtual)) params.virtual = criteria.virtual;
    if (isNotNil(criteria.obsolete)) params.obsolete = criteria.obsolete;
    if (isNotNil(criteria.temporary)) params.temporary = criteria.temporary;
    return params;
  }
}
