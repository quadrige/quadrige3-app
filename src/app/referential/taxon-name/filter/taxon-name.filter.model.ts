import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { TaxonomicLevel } from '@app/referential/taxon-name/taxonomic-level.model';
import { TaxonName } from '@app/referential/taxon-name/taxon-name.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'TaxonNameFilterCriteriaVO' })
export class TaxonNameFilterCriteria extends ReferentialFilterCriteria<TaxonName, number> {
  static fromObject: (source: any, opts?: any) => TaxonNameFilterCriteria;

  taxonGroupFilter: IntReferentialFilterCriteria = null;
  taxonomicLevel: TaxonomicLevel = null;
  taxonomicLevelId: string = null;
  referent: boolean = null;
  virtual: boolean = null;
  obsolete: boolean = null;
  temporary: boolean = null;

  constructor() {
    super(TaxonNameFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.taxonGroupFilter = IntReferentialFilterCriteria.fromObject(source.taxonGroupFilter || {});
    this.taxonomicLevelId = source.taxonomicLevelId || source.taxonomicLevel?.id;
    this.referent = source.referent;
    this.virtual = source.virtual;
    this.obsolete = source.obsolete;
    this.temporary = source.temporary;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.taxonGroupFilter = this.taxonGroupFilter?.asObject(opts);
    target.taxonomicLevelId = this.taxonomicLevelId || this.taxonomicLevel?.id;
    delete target.taxonomicLevel;
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'taxonGroupFilter') return !BaseFilterUtils.isCriteriaEmpty(this.taxonGroupFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'TaxonNameFilterVO' })
export class TaxonNameFilter extends ReferentialFilter<TaxonNameFilter, TaxonNameFilterCriteria, TaxonName> {
  static fromObject: (source: any, opts?: any) => TaxonNameFilter;

  constructor() {
    super(TaxonNameFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): TaxonNameFilterCriteria {
    return TaxonNameFilterCriteria.fromObject(source, opts);
  }
}
