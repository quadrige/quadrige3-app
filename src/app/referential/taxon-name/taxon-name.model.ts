import { EntityClass, fromDateISOString, ReferentialAsObjectOptions, toNumber } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';
import { TaxonomicLevel } from '@app/referential/taxon-name/taxonomic-level.model';

@EntityClass({ typename: 'TaxonNameVO' })
export class TaxonName extends Referential<TaxonName> {
  static entityName = 'TaxonName';
  static fromObject: (source: any, opts?: any) => TaxonName;

  parent: IntReferential = null;
  completeName: string = null;
  taxonomicLevel: TaxonomicLevel = null;
  referenceTaxon: TaxonName = null;
  citationName: string = null;
  rankOrder: number = null;
  naming = false;
  referent = false;
  virtual = false;
  obsolete = false;
  temporary = false;
  startDate: Moment = null;
  endDate: Moment = null;

  constructor() {
    super(TaxonName.TYPENAME);
    this.entityName = TaxonName.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = TaxonName.entityName;
    this.parent = IntReferential.fromObject(source.parent);
    this.completeName = source.completeName;
    this.taxonomicLevel = TaxonomicLevel.fromObject(source.taxonomicLevel);
    this.referenceTaxon = TaxonName.fromObject(source.referenceTaxon);
    this.citationName = source.citationName;
    this.rankOrder = toNumber(source.rankOrder);
    this.naming = source.naming || false;
    this.referent = source.referent || false;
    this.virtual = source.virtual || false;
    this.obsolete = source.obsolete || false;
    this.temporary = source.temporary || false;
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parent = this.parent?.asObject(opts);
    target.taxonomicLevel = this.taxonomicLevel?.asObject(opts);
    target.startDate = Dates.toLocalDateString(this.startDate);
    target.endDate = Dates.toLocalDateString(this.endDate);
    delete target.statusId;
    return target;
  }
}
