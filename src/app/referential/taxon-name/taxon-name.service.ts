import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import { Injectable, Injector } from '@angular/core';
import { Page } from '@sumaris-net/ngx-components';
import { TaxonName } from '@app/referential/taxon-name/taxon-name.model';
import { taxonomicLevelFragment } from '@app/referential/taxon-name/taxonomic-level.service';
import { TaxonNameFilter, TaxonNameFilterCriteria } from '@app/referential/taxon-name/filter/taxon-name.filter.model';

export const refTaxonNameFragment = gql`
  fragment RefTaxonNameFragment on TaxonNameVO {
    id
    updateDate
    creationDate
    statusId
    name
    completeName
    citationName
    comments
    taxonomicLevel {
      ...TaxonomicLevelFragment
    }
    rankOrder
    naming
    referent
    virtual
    obsolete
    temporary
    __typename
  }
  ${taxonomicLevelFragment}
`;

export const taxonNameLightFragment = gql`
  fragment TaxonNameLightFragment on TaxonNameVO {
    id
    updateDate
    creationDate
    statusId
    name
    completeName
    citationName
    comments
    taxonomicLevel {
      ...TaxonomicLevelFragment
    }
    referenceTaxon {
      ...RefTaxonNameFragment
    }
    rankOrder
    naming
    referent
    virtual
    obsolete
    temporary
    __typename
  }
  ${taxonomicLevelFragment}
  ${refTaxonNameFragment}
`;

export const taxonNameFragment = gql`
  fragment TaxonName on TaxonNameVO {
    id
    updateDate
    creationDate
    statusId
    name
    completeName
    citationName
    comments
    taxonomicLevel {
      ...TaxonomicLevelFragment
    }
    parent {
      ...TaxonNameLightFragment
    }
    referenceTaxon {
      ...RefTaxonNameFragment
    }
    rankOrder
    naming
    referent
    virtual
    obsolete
    temporary
    startDate
    endDate
    __typename
  }
  ${taxonomicLevelFragment}
  ${taxonNameLightFragment}
  ${refTaxonNameFragment}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query TaxonNames($page: PageInput, $filter: TaxonNameFilterVOInput) {
      data: taxonNames(page: $page, filter: $filter) {
        ...TaxonNameLightFragment
      }
    }
    ${taxonNameLightFragment}
  `,
  countAll: gql`
    query TaxonNamesCount($filter: TaxonNameFilterVOInput) {
      total: taxonNamesCount(filter: $filter)
    }
  `,
  loadAllWithTotal: gql`
    query TaxonNamesWithTotal($page: PageInput, $filter: TaxonNameFilterVOInput) {
      data: taxonNames(page: $page, filter: $filter) {
        ...TaxonNameLightFragment
      }
      total: taxonNamesCount(filter: $filter)
    }
    ${taxonNameLightFragment}
  `,
};

@Injectable({ providedIn: 'root' })
export class TaxonNameService extends BaseReferentialService<TaxonName, TaxonNameFilter, TaxonNameFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, TaxonName, TaxonNameFilter, { queries, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[taxon-name-service]';
  }

  protected asPage(page: Partial<Page>, filter?: Partial<TaxonNameFilter>): Page {
    // Convert sort 'citationName' (Mantis #61627)
    if (page?.sortBy === 'citationName') {
      page.sortBy = 'citation.name';
    }

    return super.asPage(page, filter);
  }
}
