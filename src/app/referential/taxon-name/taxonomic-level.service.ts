import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import { Injectable, Injector } from '@angular/core';
import { TaxonomicLevel } from '@app/referential/taxon-name/taxonomic-level.model';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';

export const taxonomicLevelFragment = gql`
  fragment TaxonomicLevelFragment on TaxonomicLevelVO {
    id
    updateDate
    creationDate
    statusId
    label
    name
    comments
    rankOrder
    __typename
  }
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query TaxonomicLevels($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: taxonomicLevels(page: $page, filter: $filter) {
        ...TaxonomicLevelFragment
      }
    }
    ${taxonomicLevelFragment}
  `,
  countAll: gql`
    query TaxonomicLevelsCount($filter: StrReferentialFilterVOInput) {
      total: taxonomicLevelsCount(filter: $filter)
    }
  `,
  loadAllWithTotal: gql`
    query TaxonomicLevelsWithTotal($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: taxonomicLevels(page: $page, filter: $filter) {
        ...TaxonomicLevelFragment
      }
      total: taxonomicLevelsCount(filter: $filter)
    }
    ${taxonomicLevelFragment}
  `,
};

@Injectable({ providedIn: 'root' })
export class TaxonomicLevelService extends BaseReferentialService<TaxonomicLevel, StrReferentialFilter, StrReferentialFilterCriteria, string> {
  constructor(protected injector: Injector) {
    super(injector, TaxonomicLevel, StrReferentialFilter, { queries, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[taxonomic-level-service]';
  }
}
