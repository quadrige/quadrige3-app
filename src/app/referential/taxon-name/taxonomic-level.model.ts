import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'TaxonomicLevelVO' })
export class TaxonomicLevel extends Referential<TaxonomicLevel, string> {
  static entityName = 'TaxonomicLevel';
  static fromObject: (source: any, opts?: any) => TaxonomicLevel;

  rankOrder: number;

  constructor() {
    super(TaxonomicLevel.TYPENAME);
    this.entityName = TaxonomicLevel.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = TaxonomicLevel.entityName;
    this.rankOrder = source.rankOrder;
  }
}
