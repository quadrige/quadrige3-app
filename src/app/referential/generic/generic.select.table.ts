import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { GenericService } from '@app/referential/generic/generic.service';
import { ReferentialGenericValidatorService } from '@app/referential/generic/generic.validator';
import { filter } from 'rxjs/operators';
import { IAddResult, ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { isEmptyArray, isNotEmptyArray } from '@sumaris-net/ngx-components';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { ISelectTable } from '@app/shared/table/table.model';
import { GenericTable } from '@app/referential/generic/generic.table';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { GenericFilterForm } from '@app/referential/generic/filter/generic.filter.form';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';

@Component({
  selector: 'app-generic-select-table',
  templateUrl: './generic.table.html',
  standalone: true,
  imports: [QuadrigeCoreModule, TranscribingItemRowTable, GenericFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericSelectTable extends GenericTable implements OnInit, AfterViewInit, ISelectTable<GenericReferential> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria = {};
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() addTitle: string;

  @Output() selectionChanged = new EventEmitter<any[]>(); // todo add to ISelectTable and cal it from EntitySelectModal

  constructor(
    protected injector: Injector,
    protected referentialGenericService: GenericService,
    protected validatorService: ReferentialGenericValidatorService
  ) {
    super(injector, referentialGenericService, validatorService);
    this.logPrefix = '[generic-select-table]';
    // default properties: don't use this.selectTable = true; this table is also used as is
    this.subTable = true;
  }

  get selectedIds(): any[] {
    return this.selectCriteria.includedIds || [];
  }

  ngOnInit() {
    this.inlineEdition = false;
    this.forceNoInlineEdition = true;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;

    // Listen end of loading to refresh total row count
    this.registerSubscription(
      this.loadingSubject.pipe(filter((value) => !value)).subscribe(() => {
        if (this.selectedIds === undefined) {
          // this will remove the 'no result' template if the selection is voluntarily empty (=== undefined)
          this.totalRowCount = null;
        }
      })
    );

    this.updateTitle();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.canDuplicate = false;
  }

  async setEntityName(entityName: string, opts?: { emitEvent?: boolean }) {
    return super.setEntityName(entityName, { emitEvent: false }); // Don't load (refresh) table
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    await this.openAddModal(event);
    return true;
  }

  // Override the default deleteSelection method to remove only from selectedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Check if can delete
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete).map((id) => id.toString());
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    this.selectionChanged.emit(this.selectCriteria.includedIds);
    await this.resetFilter();
  }

  async setSelectedIds(values: any[]) {
    this.selectCriteria.includedIds = values;
    await this.ready();
    await this.resetFilter();
  }

  async resetFilter(f?: GenericReferentialFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(f, {
      ...opts,
      firstTime: true, // Always set firstTime to prevent saving filter
      staticCriteria: {
        ...this.selectCriteria, // always use default filter
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds) && !this.selectCriteria.parentId,
      },
    });
  }

  protected async openAddModal(event?: any) {
    event?.preventDefault();

    if (!this.canEdit) {
      return;
    }
    this.selection.clear();

    // Open modal
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: this.entityName,
        addCriteria: { ...this.addCriteria, excludedIds: this.selectCriteria.includedIds || [] },
        addFirstCriteria: this.addFirstCriteria,
        title: this.addTitle,
        titleI18n: this.titleI18n,
        titlePrefixI18n: this.titlePrefixI18n,
        titleAddPrefixI18n: 'COMMON.ADD',
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      this.selectCriteria.includedIds = [...(this.selectCriteria.includedIds || []), ...data.addedIds];
      this.selectionChanged.emit(this.selectCriteria.includedIds);
      await this.resetFilter();
    }
  }

  protected updateTitle() {
    // change title
    if (this.showTitle && this.titleI18n) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected refreshBehavior() {
    this.inlineEdition = false;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }
}
