import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { GenericService } from '@app/referential/generic/generic.service';
import { ReferentialGenericValidatorService } from '@app/referential/generic/generic.validator';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { GenericTable } from '@app/referential/generic/generic.table';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { GenericFilterForm } from '@app/referential/generic/filter/generic.filter.form';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';

@Component({
  selector: 'app-generic-add-table',
  templateUrl: './generic.table.html',
  standalone: true,
  imports: [QuadrigeCoreModule, TranscribingItemRowTable, GenericFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericAddTable extends GenericTable implements OnInit, AfterViewInit, IAddTable<GenericReferential> {
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;

  constructor(
    protected injector: Injector,
    protected referentialGenericService: GenericService,
    protected validatorService: ReferentialGenericValidatorService
  ) {
    super(injector, referentialGenericService, validatorService);
    this.logPrefix = '[generic-add-table]';
    this.addTable = true;
  }

  ngOnInit() {
    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;

    this.updateTitle();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.canDuplicate = false;
  }

  async setEntityName(entityName: string, opts?: { emitEvent?: boolean }) {
    return await super.setEntityName(entityName, { emitEvent: false }); // Don't load (refresh) table
  }

  async resetFilter(filter?: GenericReferentialFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // reset first filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected refreshBehavior() {
    this.inlineEdition = false;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }
}
