import { Injectable, Injector } from '@angular/core';
import { arraySize, EntityServiceLoadOptions, isEmptyArray, LoadResult, Page } from '@sumaris-net/ngx-components';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { EntityType } from '@app/referential/model/referential.model';
import { referentialErrorCodes } from '@app/referential/service/errors';
import {
  BaseReferentialService,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
  ReferentialServiceLoadOptions,
  ReferentialServiceWatchOptions,
} from '@app/referential/service/base-referential.service';
import { uniqueValue } from '@app/shared/utils';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { map } from 'rxjs/operators';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { User } from '@app/referential/user/user.model';
import { DredgingTargetArea } from '@app/referential/dredging-target-area/dredging-target-area.model';
import { jobFragments } from '@app/social/job/job.service';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { GenericReferentialFilter, GenericReferentialFilterCriteria } from '@app/referential/generic/filter/generic.filter.model';

import { transcribingItemWithChildrenFragment } from '@app/referential/transcribing-item/transcribing-item.model';

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Referentials($entityName: String, $page: PageInput, $filter: GenericReferentialFilterVOInput) {
      data: referentials(entityName: $entityName, page: $page, filter: $filter) {
        ...FullReferentialFragment
      }
    }
    ${referentialFragments.full}
  `,

  loadAllWithTotal: gql`
    query ReferentialsWithTotal($entityName: String, $page: PageInput, $filter: GenericReferentialFilterVOInput) {
      data: referentials(entityName: $entityName, page: $page, filter: $filter) {
        ...FullReferentialFragment
      }
      total: referentialsCount(entityName: $entityName, filter: $filter)
    }
    ${referentialFragments.full}
  `,

  countAll: gql`
    query ReferentialsCount($entityName: String, $filter: GenericReferentialFilterVOInput) {
      total: referentialsCount(entityName: $entityName, filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportReferentialsAsync($context: ExportContextInput, $filter: GenericReferentialFilterVOInput) {
      data: exportReferentialsAsync(context: $context, filter: $filter) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveReferentials($data: [ReferentialVOInput]) {
      data: saveReferentials(referentials: $data) {
        ...FullReferentialFragment
        transcribingItems {
          ...TranscribingItemWithChildrenFragment
        }
      }
    }
    ${referentialFragments.full}
    ${transcribingItemWithChildrenFragment}
  `,

  deleteAll: gql`
    mutation DeleteReferentials($entityName: String, $ids: [String]) {
      deleteReferentials(entityName: $entityName, ids: $ids)
    }
  `,
};

const otherQueries = {
  loadEntityTypesQuery: gql`
    query EntityTypes {
      entityTypes {
        name
        idIsString
        idIsComposite
        labelPresent
        namePresent
        descriptionPresent
        commentPresent
        creationDatePresent
        statusPresent
        __typename
      }
    }
  `,
  loadServerLog: gql`
    query LoadServerLog {
      serverLog
    }
  `,
};

const allTypes: EntityType[] = [];

@Injectable({ providedIn: 'root' })
export class GenericService extends BaseReferentialService<GenericReferential, GenericReferentialFilter, GenericReferentialFilterCriteria, string> {
  constructor(protected injector: Injector) {
    super(injector, GenericReferential, GenericReferentialFilter, { queries, mutations });
    this._logPrefix = '[referential-service]';
  }

  watchPage(
    page: Partial<Page>,
    filter?: GenericReferentialFilter,
    opts?: ReferentialServiceWatchOptions
  ): Observable<LoadResult<GenericReferential>> {
    const entityName = filter?.entityName;
    if (!entityName) {
      console.error(`${this._logPrefix} Missing filter.entityName`);
      throw { code: referentialErrorCodes.missingEntityName, message: 'REFERENTIAL.ERROR.MISSING_ENTITY_NAME' };
    }

    return super.watchPage({ ...page, sortBy: this.fixSortBy(entityName, page.sortBy) }, filter, { ...opts, entityName }).pipe(
      map(({ data, total }) => {
        // Affect entity name
        data.forEach((r) => (r.entityName = entityName));
        return { data, total };
      })
    );
  }

  async loadPage(
    page: Partial<Page>,
    filter?: GenericReferentialFilter,
    opts?: ReferentialServiceLoadOptions
  ): Promise<LoadResult<GenericReferential>> {
    await this.ready();
    const entityName = filter?.entityName;
    if (!entityName) {
      console.error(`${this._logPrefix} Missing filter.entityName`);
      throw { code: referentialErrorCodes.missingEntityName, message: 'REFERENTIAL.ERROR.MISSING_ENTITY_NAME' };
    }

    const result = await super.loadPage({ ...page, sortBy: this.fixSortBy(entityName, page.sortBy) }, filter, { ...opts, entityName });
    // Affect entity name
    result.data.forEach((r) => (r.entityName = entityName));
    return result;
  }

  async load(id: string, opts?: EntityServiceLoadOptions & { query?: any; entityName: string }): Promise<GenericReferential> {
    const entityName = opts?.entityName;
    if (!entityName) {
      console.error(`${this._logPrefix} Missing opts.entityName`);
      throw { code: referentialErrorCodes.missingEntityName, message: 'REFERENTIAL.ERROR.MISSING_ENTITY_NAME' };
    }

    const result = await this.loadPage({ offset: 0, size: 1 }, GenericReferentialFilter.fromObject({ entityName, criterias: [{ id }] }), {
      withTotal: false,
    });
    return result?.data?.[0];
  }

  async exists(criteria: GenericReferentialFilterCriteria, entityName?: string): Promise<boolean> {
    if (!entityName) {
      console.error(`${this._logPrefix} Missing filter.entityName`);
      throw { code: referentialErrorCodes.missingEntityName, message: 'REFERENTIAL.ERROR.MISSING_ENTITY_NAME' };
    }

    return super.exists(criteria, entityName);
  }

  async saveAll(entities: GenericReferential[], opts?: EntitySaveOptions): Promise<GenericReferential[]> {
    // Nothing to save: skip
    if (isEmptyArray(entities)) return;

    if (!entities[0].entityName) {
      console.error(`${this._logPrefix} Could not save referential: missing entityName`);
      throw { code: referentialErrorCodes.missingEntityName, message: 'REFERENTIAL.ERROR.MISSING_ENTITY_NAME' };
    }

    const entityNames = entities.map((e) => e.entityName).filter(uniqueValue);
    if (arraySize(entityNames) !== 1) {
      console.error(`${this._logPrefix} Could not save referential: only one entityName is allowed, found: ${entityNames}`);
      throw { code: referentialErrorCodes.invalidEntityName, message: 'REFERENTIAL.ERROR.INVALID_ENTITY_NAME' };
    }

    return super.saveAll(entities, opts);
  }

  async deleteAll(entities: GenericReferential[], opts?: any): Promise<any> {
    // Nothing to save: skip
    if (isEmptyArray(entities)) return;

    if (!entities[0].entityName) {
      console.error(`${this._logPrefix} Could not delete referential: missing entityName`);
      throw { code: referentialErrorCodes.missingEntityName, message: 'REFERENTIAL.ERROR.MISSING_ENTITY_NAME' };
    }

    // Check that all entities have the same entityName
    const entityNames = entities.map((e) => e.entityName).filter(uniqueValue);
    if (arraySize(entityNames) !== 1) {
      console.error(`${this._logPrefix} Could not delete referential: only one entityName is allowed, found: ${entityNames}`);
      throw { code: referentialErrorCodes.invalidEntityName, message: 'REFERENTIAL.ERROR.INVALID_ENTITY_NAME' };
    }

    return super.deleteAll(entities, opts);
  }

  getType(entityName: string): EntityType {
    return allTypes.find((type) => type.name === entityName);
  }

  getDefaultSortBy(type: EntityType): string {
    if (!type) return undefined;
    if ([Program.entityName, DredgingTargetArea.entityName].includes(type.name)) {
      return 'id';
    } else if ([User.entityName, 'Ship', 'Harbour'].includes(type.name)) {
      return 'name';
    } else {
      return type.labelPresent ? 'label' : type.idIsString ? 'id' : 'name';
    }
  }

  async loadServerLog(): Promise<string> {
    if (this._debug) console.debug(`${this._logPrefix} Loading server log...`);
    const res = await this.graphql.query<{ serverLog: string }>({
      query: otherQueries.loadServerLog,
      variables: null,
      fetchPolicy: 'network-only',
      error: { code: referentialErrorCodes.loadEntities, message: 'REFERENTIAL.ERROR.LOAD_ENTITY_TYPES_ERROR' },
    });
    if (this._debug) console.debug(`${this._logPrefix} Server log loaded`, res);
    return res.serverLog;
  }

  // protected methods

  protected async ngOnStart() {
    // Load all types
    const types = await this.loadTypes();
    allTypes.push(...types);
  }

  protected fixSortBy(entityName: string, sortBy?: string): string {
    const type: EntityType = this.getType(entityName);
    if (sortBy) {
      if (!type.labelPresent && sortBy === 'label') {
        // Fix if a sort is tried on an entity without label
        sortBy = this.getDefaultSortBy(type);
      }
    } else {
      // Default sort if unavailable
      sortBy = this.getDefaultSortBy(type);
    }
    return sortBy;
  }

  /**
   * Load referential types
   */
  private async loadTypes(): Promise<EntityType[]> {
    if (this._debug) console.debug(`${this._logPrefix} Loading entity types...`);
    const res = await this.graphql.query<{ entityTypes: EntityType[] }>({
      query: otherQueries.loadEntityTypesQuery,
      variables: null,
      fetchPolicy: 'cache-first',
      error: { code: referentialErrorCodes.loadEntities, message: 'REFERENTIAL.ERROR.LOAD_ENTITY_TYPES_ERROR' },
    });
    if (this._debug) console.debug(`${this._logPrefix} Entity types loaded`, res);
    return res.entityTypes || [];
  }
}
