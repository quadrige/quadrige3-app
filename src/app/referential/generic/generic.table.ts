import { AfterViewInit, ChangeDetectionStrategy, Component, inject, Injector, Input, OnInit } from '@angular/core';
import {
  arraySize,
  changeCaseToUnderscore,
  isNil,
  isNilOrBlank,
  isNotNil,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  SETTINGS_SORTED_COLUMN,
  TranslateContextService,
} from '@sumaris-net/ngx-components';
import { EntityType } from '@app/referential/model/referential.model';
import { ReferentialGenericValidatorOptions, ReferentialGenericValidatorService } from '@app/referential/generic/generic.validator';
import { GenericService } from '@app/referential/generic/generic.service';
import { BehaviorSubject } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { UntypedFormGroup } from '@angular/forms';
import { readOnlyEntities } from '@app/referential/model/referential.constants';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { Utils } from '@app/shared/utils';
import { User } from '@app/referential/user/user.model';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';
import { Params } from '@angular/router';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { GenericReferentialFilter, GenericReferentialFilterCriteria } from '@app/referential/generic/filter/generic.filter.model';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericTable
  extends ReferentialTable<GenericReferential, string, GenericReferentialFilter, GenericReferentialFilterCriteria, ReferentialGenericValidatorService>
  implements OnInit, AfterViewInit
{
  entityType: EntityType;
  idIsString$ = new BehaviorSubject<boolean>(undefined);
  idLabel$ = new BehaviorSubject<string>(undefined);
  canDuplicate = true;
  autocompleteWidthExtraLarge = autocompleteWidthExtraLarge;

  protected readonly translateContext = inject(TranslateContextService);

  constructor(
    protected injector: Injector,
    protected referentialGenericService: GenericService,
    protected validatorService: ReferentialGenericValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['label', 'name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      GenericReferential,
      referentialGenericService,
      validatorService
    );

    this.logPrefix = '[generic-table]';
  }

  get entityName(): string {
    return super.entityName;
  }

  @Input() set entityName(entityName: string) {
    // noinspection JSIgnoredPromiseFromCall
    this.setEntityName(entityName);
  }

  ngOnInit() {
    super.ngOnInit();

    if (this.subTable) {
      this.rightPanelAllowed = false;
    }

    // todo: check usage !!
    // OrderItemType combo for OrderItem filter
    this.registerAutocompleteField('orderItemType', {
      ...this.referentialOptions.orderItemType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'OrderItemType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  ngAfterViewInit() {
    // Prevent default listener on route parameters
    this.listenToQueryParams = false;

    // Listen route parameters
    if (!this.subTable) {
      this.registerSubscription(
        this.route.params
          .pipe(filter(({ entity }) => isNotNil(entity) && this.entityName !== entity))
          .subscribe(({ entity }) => this.setEntityName(entity, { queryParams: this.route.snapshot.queryParams }))
      );
      this.registerSubscription(
        this.route.queryParams
          .pipe(
            filter(() => this.updateFilterOnQueryParams && !!this.entityName),
            // Restore filter
            mergeMap((queryParams) => this.restoreLastFilter(queryParams))
          )
          .subscribe((restored) => {
            // Apply initial filter
            this.setFilter(this.filterFormComponent().value, { updateQueryParams: restored, firstTime: !restored, emitEvent: true });
          })
      );
    }

    super.ngAfterViewInit();
  }

  async setEntityName(entityName: string, opts?: { emitEvent?: boolean; queryParams?: Params }) {
    // No entityName: error
    if (isNilOrBlank(entityName)) {
      throw new Error(`${this.logPrefix} Entity name not provided !`);
    }
    // No change or abstract (default) referential entity: skip
    if (this.entityName === entityName || 'Referential' === entityName) return;

    // Reset selection
    this.selection.clear();

    // Wait service ready
    await this.referentialGenericService.ready();
    await this.transcribingItemTypeService.ready();
    // Wait table init
    await this.waitInit();

    opts = opts || { emitEvent: true };

    // Affect subject directly, don't use entityName setter
    this.entityName$.next(entityName);
    this.updateSettingsId();
    const type = this.referentialGenericService.getType(entityName);
    if (!type) {
      throw new Error(`${this.logPrefix} Entity type for '${entityName}' not found !`);
    }
    this.entityType = type;

    // update title
    this.updateTitle(type);

    // update id header
    this.idIsString$.next(type.idIsString);
    this.idLabel$.next(this.getI18nColumnName('id'));

    // update columns visibility
    this.updateEntityColumns(type);

    // update default sort by
    this.updateDefaultSort(type);

    // update filter component
    await this.filterFormComponent().updateEntityName(entityName);

    // Initialize right menu with current entity name
    await this.initRightView();

    this.markForCheck();

    // prevent save on setFilter
    this.markAsPristine();

    // Restore or load last filter (if not sub table)
    const restored = !this.subTable && (await this.restoreLastFilter(opts.queryParams));
    await this.setFilter(this.filterFormComponent().value, { emitEvent: false, updateQueryParams: restored, firstTime: !restored });
    this.refreshBehavior();
    this.markAsReady();

    if (opts.emitEvent !== false) {
      console.info(`${this.logPrefix} Loading ${entityName}...`);
      // Emit onRefresh in a timeout to allow MatSort to refresh defaultSort and defaultDirection updated by updateDefaultSortBy
      setTimeout(() => this.emitRefresh());
    }
  }

  protected async initRightView() {
    // Before init, reinitialize some properties
    this.rightPanelAllowed = true;
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  asFilter(source: any): GenericReferentialFilter {
    const f = super.asFilter(source);
    f.entityName = this.entityName;
    return f;
  }

  async setFilter(f: GenericReferentialFilter, opts?: ISetFilterOptions) {
    // specific case for User: patch search attributes for department (parent) (Mantis #56350)
    if (this.entityName === User.entityName) {
      f = this.asFilter(f);
      f.patch({ parentFilter: GenericReferentialFilterCriteria.fromObject({ searchAttributes: this.referentialOptions.department.attributes }) });
    }
    await super.setFilter(f, opts);
  }

  protected fromObject(entity: any): GenericReferential {
    return GenericReferential.fromObject(entity);
  }

  protected generateTableId(): string {
    return super.generateTableId() + `_${this.entityName}`;
  }

  protected updateTitle(entityType?: EntityType) {
    if (!entityType) return;
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: this.titleI18n, entityName: entityType.name, withRoot: this.titleWithRoot });
  }

  protected updateEntityColumns(entityType: EntityType) {
    this.setShowColumn('label', entityType.labelPresent);
    this.setShowColumn('description', entityType.descriptionPresent);
    this.setShowColumn('comment', entityType.commentPresent);
    this.setShowColumn('creationDate', entityType.creationDatePresent);
    this.setShowColumn('statusId', entityType.statusPresent);
    super.updateColumns();
  }

  getI18nColumnName(columnName: string): string {
    const context = changeCaseToUnderscore(this.entityName).toUpperCase();
    if (columnName === 'id') {
      return this.idIsString$.value ? `${this.i18nColumnPrefix}CODE` : `${this.i18nColumnPrefix}ID`;
    } else if (columnName === 'name') {
      return Utils.contextualI18nKey(this.translateContext, `${this.i18nColumnPrefix}NAME`, context);
    } else if (columnName === 'label') {
      return Utils.contextualI18nKey(this.translateContext, `${this.i18nColumnPrefix}LABEL`, context);
    } else if (columnName === 'description') {
      return Utils.contextualI18nKey(this.translateContext, `${this.i18nColumnPrefix}DESCRIPTION`, context);
    }
    return super.getI18nColumnName(columnName);
  }

  protected updateDefaultSort(entityType: EntityType) {
    // Initialize default sort if nothing found in settings
    const data = this.settings.getPageSettings(this.settingsId, SETTINGS_SORTED_COLUMN);
    if (data) {
      const parts = data.split(':');
      if (arraySize(parts) === 2 && this.columns.includes(parts[0])) {
        this.defaultSortBy = parts[0];
        this.defaultSortDirection = parts[1] === 'desc' ? 'desc' : 'asc';
      }
    } else {
      this.defaultSortBy = this.referentialGenericService.getDefaultSortBy(entityType);
      this.defaultSortDirection = 'asc';
    }
    // Apply it
    this.applySort({ id: this.defaultSortBy, start: this.defaultSortDirection === '' ? 'asc' : this.defaultSortDirection, disableClear: true });
  }

  protected getRowValidator(row: AsyncTableElement<GenericReferential>): UntypedFormGroup {
    if (isNil(this.entityType)) throw Error(`${this.logPrefix} Can't edit a row with undefined entity type`);

    return super.getRowValidator(row, <ReferentialGenericValidatorOptions>{
      entityName: this.entityType.name,
      type: this.entityType,
    });
  }

  protected refreshBehavior() {
    // Set read only for specific entity
    this.readOnly = readOnlyEntities.includes(this.entityName);
    this.inlineEdition = this.canEdit;
  }

  protected getRequiredColumns(): string[] {
    if (this.entityType?.labelPresent) {
      // todo check if this is updated correctly
      return super.getRequiredColumns().concat('name', 'label');
    }
    return super.getRequiredColumns().concat('name');
  }
}
