import { inject, NgModule } from '@angular/core';
import { GenericTable } from '@app/referential/generic/generic.table';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { GenericReferentialModule } from '@app/referential/generic/generic.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: GenericTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), GenericReferentialModule],
  exports: [RouterModule],
})
export class GenericRoutingModule {}
