import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { EntityType } from '@app/referential/model/referential.model';
import { GenericService } from '@app/referential/generic/generic.service';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { GenericReferential } from '@app/referential/generic/generic.model';

export interface ReferentialGenericValidatorOptions extends ReferentialValidatorOptions {
  type: EntityType;
  // fixme: not sure it is really useful:
  checkNotUniqueName?: boolean; // default undefined means that the check will be performed depending referential type
}

@Injectable({ providedIn: 'root' })
export class ReferentialGenericValidatorService extends ReferentialValidatorService<GenericReferential, string, ReferentialGenericValidatorOptions> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialService: GenericService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: GenericReferential, opts?: ReferentialGenericValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        opts?.type.idIsString ? Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]) : null,
        opts?.type.idIsString ? ReferentialAsyncValidators.checkIdAlreadyExists(this.referentialService, this.dataSource, opts, data) : null,
      ],
      name: [
        (data && data.name) || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        opts && (opts.checkNotUniqueName === true || (!opts.type.idIsString && opts.checkNotUniqueName !== false))
          ? ReferentialAsyncValidators.checkAttributeAlreadyExists(this.referentialService, this.dataSource, opts, data)
          : null,
      ],
      label: [data?.label || null, opts?.type.labelPresent ? Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]) : null],
      description: [data?.description || null, opts?.type.descriptionPresent ? Validators.maxLength(lengthDescription) : null],
      comments: [data?.comments || null, opts?.type.commentPresent ? Validators.maxLength(lengthComment) : null],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
