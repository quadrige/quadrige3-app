import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { entitiesWithoutTranscribingItems } from '@app/referential/model/referential.constants';

export interface IGenericReferentialFilterCriteria extends IReferentialFilterCriteria<string> {
  parentFilter?: IGenericReferentialFilterCriteria;
}

@EntityClass({ typename: 'GenericReferentialFilterCriteriaVO' })
export class GenericReferentialFilterCriteria
  extends ReferentialFilterCriteria<GenericReferential, string>
  implements IGenericReferentialFilterCriteria
{
  static fromObject: (source: any, opts?: any) => GenericReferentialFilterCriteria;

  parentFilter?: StrReferentialFilterCriteria = null;

  constructor() {
    super(GenericReferentialFilterCriteria.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.parentFilter = StrReferentialFilterCriteria.fromObject(source.parentFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parentFilter = this.parentFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'parentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parentFilter, true);
    if (key === 'parentId') return false;
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'GenericReferentialFilterVO' })
export class GenericReferentialFilter extends ReferentialFilter<
  GenericReferentialFilter,
  GenericReferentialFilterCriteria,
  GenericReferential,
  string
> {
  static fromObject: (source: any, opts?: any) => GenericReferentialFilter;

  entityName: string;

  constructor() {
    super(GenericReferentialFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts?: any): GenericReferentialFilterCriteria {
    return GenericReferentialFilterCriteria.fromObject(source || {}, opts);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.entityName = source.entityName;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    if (opts?.minify) {
      delete target.entityName;
    }
    // For entity without transcribing, remove unhandled systemId
    if (entitiesWithoutTranscribingItems.includes(this.entityName)) {
      delete target.systemId;
    }
    return target;
  }
}
