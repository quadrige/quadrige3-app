import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { GenericReferentialFilter, GenericReferentialFilterCriteria } from '@app/referential/generic/filter/generic.filter.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { BaseFilterFormComponent } from '@app/shared/component/filter/base-filter-form.component';
import { GenericTable } from '@app/referential/generic/generic.table';
import { Strategy } from '@app/referential/program-strategy/strategy/strategy.model';
import { EntityType } from '@app/referential/model/referential.model';
import { UntypedFormBuilder } from '@angular/forms';

@Component({
  selector: 'app-generic-filter-form',
  templateUrl: './generic.filter.form.html',
  standalone: true,
  imports: [QuadrigeCoreModule, BaseFilterFormComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericFilterForm extends ReferentialCriteriaFormComponent<GenericReferentialFilter, GenericReferentialFilterCriteria> {
  private readonly formBuilder = inject(UntypedFormBuilder);

  constructor() {
    super();
  }

  async updateEntityName(entityName: string): Promise<void> {
    await super.updateEntityName(entityName);

    // Add entityName form control
    this.base().form.addControl('entityName', this.formBuilder.control(entityName));
  }

  protected getSearchAttributes(): string[] {
    const entityType: EntityType = (this.table() as GenericTable).entityType;
    if (!entityType) return undefined;
    const fields = [entityType.idIsString ? 'code' : 'id'];
    if (
      entityType.labelPresent &&
      // Ignore label (=program) of Strategy (Mantis 55796)
      entityType.name !== Strategy.entityName
    ) {
      fields.push('label');
    }
    fields.push('name');
    return fields;
  }

  protected criteriaToQueryParams(criteria: GenericReferentialFilterCriteria): PartialRecord<keyof GenericReferentialFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'parentFilter');
    return params;
  }
}
