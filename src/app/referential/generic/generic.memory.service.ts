import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { StrReferentialFilter } from '@app/referential/model/referential.filter.model';
import { GenericReferential } from '@app/referential/generic/generic.model';

@Injectable({ providedIn: 'root' })
export class GenericMemoryService extends EntitiesMemoryService<GenericReferential, StrReferentialFilter, string> {
  constructor() {
    super(GenericReferential, StrReferentialFilter);
  }
}
