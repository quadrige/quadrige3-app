import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

/** GENERIC REFERENTIAL OBJECT */
@EntityClass({ typename: 'ReferentialVO' })
export class GenericReferential extends Referential<GenericReferential, string> {
  static fromObject: (source: any, opts?: any) => GenericReferential;

  constructor() {
    super(GenericReferential.TYPENAME);
  }
}
