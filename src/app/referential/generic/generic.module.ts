import { NgModule } from '@angular/core';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { ReferentialFilterComponentModule } from '@app/referential/component/referential-filter-component.module';
import { GenericTable } from '@app/referential/generic/generic.table';
import { GenericFilterForm } from '@app/referential/generic/filter/generic.filter.form';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';

@NgModule({
  imports: [QuadrigeCoreModule, TranscribingItemRowTable, ReferentialFilterComponentModule, GenericFilterForm],
  declarations: [GenericTable],
  exports: [ReferentialFilterComponentModule, GenericTable],
})
export class GenericReferentialModule {}
