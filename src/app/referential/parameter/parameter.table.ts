import { ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import {
  isNotEmptyArray,
  isNotNilOrBlank,
  ObjectMap,
  PromiseEvent,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StatusIds,
} from '@sumaris-net/ngx-components';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { ParameterValidatorService } from '@app/referential/parameter/parameter.validator';
import { ParameterService } from '@app/referential/parameter/parameter.service';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { Alerts } from '@app/shared/alerts';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { QualitativeValueTable } from '@app/referential/parameter/qualitative-value/qualitative-value.table';
import { EntityUtils } from '@app/shared/entity.utils';
import { autocompleteWidthLarge } from '@app/shared/constants';
import { IParameterMultiEditModalOptions, ParameterMultiEditModal } from '@app/referential/parameter/multi-edit/parameter.multi-edit.modal';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { filter } from 'rxjs/operators';
import { QualitativeValue } from '@app/referential/parameter/qualitative-value/qualitative-value.model';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { ParameterFilter, ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { ParameterComposite } from '@app/referential/parameter/multi-edit/parameter.multi-edit.model';
import { BaseColumnItem } from '@app/shared/table/table.model';
import { Utils } from '@app/shared/utils';

type ParameterView = 'qualitativeValuesTable' | TranscribingItemView;

@Component({
  selector: 'app-parameter-table',
  templateUrl: './parameter.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterTable
  extends ReferentialTable<Parameter, string, ParameterFilter, ParameterFilterCriteria, ParameterValidatorService, ParameterView>
  implements OnInit
{
  qualitativeValuesTable = viewChild<QualitativeValueTable>('qualitativeValuesTable');

  readonly qualitativeValuesMenuItem: IEntityMenuItem<Parameter, ParameterView> = {
    title: 'REFERENTIAL.PARAMETER.QUALITATIVE_VALUES',
    attribute: 'qualitativeValues',
    view: 'qualitativeValuesTable',
  };

  qualitativeValuesTableEnabled: boolean;
  autocompleteWidthLarge = autocompleteWidthLarge;

  protected previousSelectedQualitativeValue: AsyncTableElement<QualitativeValue>;

  constructor(
    protected injector: Injector,
    protected _entityService: ParameterService,
    protected validatorService: ParameterValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'name',
        'parameterGroup',
        'comments',
        'statusId',
        'description',
        'qualitative',
        'taxonomic',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      Parameter,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.PARAMETER.';
    this.logPrefix = '[parameter-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent || (!this.rightPanelVisible && !!this.singleSelectedRow?.currentData?.qualitative);
  }

  ngOnInit() {
    super.ngOnInit();

    // Parameter group combo
    this.registerAutocompleteField('parameterGroup', {
      ...this.referentialOptions.parameterGroup,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ParameterGroup',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Listen id attribute change -> update qualitative value table filter (mandatory for qualitative value validator)
    this.registerSubscription(
      this.registerCellValueChanges('id', 'id', false)
        .pipe(filter((_) => this.rightMenuItem === this.qualitativeValuesMenuItem))
        .subscribe((value) => {
          this.qualitativeValuesTableEnabled = isNotNilOrBlank(value);
          if (this.qualitativeValuesTable()) {
            this.qualitativeValuesTable().selectCriteria = { parentId: value };
          }
        })
    );
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<Parameter, TranscribingItemView | ParameterView>[] {
    return [this.qualitativeValuesMenuItem, ...super.getRightMenuItems()];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Parameter, TranscribingItemView | ParameterView> {
    return this.qualitativeValuesMenuItem;
  }

  protected async canRightMenuItemChange(
    previousMenuItem: IEntityMenuItem<Parameter, TranscribingItemView | ParameterView>,
    nextMenuItem: IEntityMenuItem<Parameter, TranscribingItemView | ParameterView>
  ): Promise<boolean> {
    return Utils.promiseEveryTrue([
      super.canRightMenuItemChange(previousMenuItem, nextMenuItem),
      this.saveRightView(this.qualitativeValuesMenuItem, this.qualitativeValuesTable()),
    ]);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<Parameter>): Promise<boolean> {
    row = row || this.singleEditingRow;

    if (row && !(await this.saveRightView(this.qualitativeValuesMenuItem, this.qualitativeValuesTable(), { row }))) {
      return false;
    }

    // Force deselection on qualitative value table
    await this.forceUnselectQualitativeValuesTable();

    return super.confirmEditCreate(event, row);
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    this.previousSelectedQualitativeValue = this.qualitativeValuesTable()?.singleSelectedRow;

    return await super.save(opts);
  }

  async canAddQualitativeValue(event: PromiseEvent<boolean>) {
    if (!this.singleEditingRow) {
      throw new Error('No edited row: should not be possible');
    }

    const canAdd = this.singleEditingRow.currentData.qualitative === true;
    event.detail.success(canAdd);
    if (!canAdd) {
      await Alerts.showError('REFERENTIAL.PARAMETER.ERROR.CANNOT_ADD_QUALITATIVE_VALUE', this.alertCtrl, this.translate, {
        titleKey: 'ERROR.CANNOT_ADD',
      });
    }
  }

  async buildExportAdditionalColumns(): Promise<ObjectMap<BaseColumnItem[]>> {
    let previousRightMenuItem = undefined;
    if (this.rightMenuItem !== this.qualitativeValuesMenuItem) {
      previousRightMenuItem = this.rightMenuItem;
      this.rightMenuItem = this.qualitativeValuesMenuItem;
      await this.loadRightArea();
    }
    const columns = await this.qualitativeValuesTable().buildExportColumns();
    if (!!previousRightMenuItem) {
      this.rightMenuItem = previousRightMenuItem;
      await this.loadRightArea();
    }

    const toAppend = this.translate.instant('REFERENTIAL.PARAMETER.QUALITATIVE_VALUE');
    columns.forEach((column) => {
      column.name = toAppend + ' - ' + column.name;
    });

    const result = {};
    result[QualitativeValue.entityName] = columns;
    return result;
  }

  async multiEditRows(event: MouseEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IParameterMultiEditModalOptions, ParameterComposite>(
      ParameterMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.PARAMETER.MULTI_EDIT.TITLE',
        i18nPrefix: this.i18nColumnPrefix,
        selection: this.selection.selected.map((row) => row.currentData),
        defaultReferentialCriteria: this.defaultReferentialCriteria,
      },
      'modal-300'
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // Patch selected rows
      this.selection.selected.forEach((row) => {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    }
  }

  protected async loadRightArea(row?: AsyncTableElement<Parameter>) {
    if (this.subTable) return; // don't load if sub table

    if (this.rightMenuItem?.view === 'qualitativeValuesTable') {
      this.detectChanges();
      if (this.qualitativeValuesTable()) {
        this.registerSubForm(this.qualitativeValuesTable());
        await this.loadQualitativeValuesTable(row);
      }
      return;
    }

    await super.loadRightArea(row);
  }

  protected async loadQualitativeValuesTable(row: AsyncTableElement<Parameter>) {
    // Force deselection on qualitative value table
    await this.forceUnselectQualitativeValuesTable();

    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.qualitativeValuesTable().setValue(undefined);
      this.qualitativeValuesTable().selectCriteria = undefined;
      this.qualitativeValuesTableEnabled = false;
      return;
    }
    // Load actual qualitative values
    await this.qualitativeValuesTable().setValue((row.currentData.qualitativeValues || []).slice());
    this.qualitativeValuesTable().selectCriteria = { parentId: row.currentData.id };
    this.qualitativeValuesTableEnabled = this.canEdit && isNotNilOrBlank(row.currentData.id);

    // Try to reselect previously selected qualitative value
    await this.qualitativeValuesTable().selectRowByData(this.previousSelectedQualitativeValue?.currentData);
  }

  protected async forceUnselectQualitativeValuesTable() {
    if (this.qualitativeValuesTable()) {
      this.qualitativeValuesTable().selection.clear(true);
      await this.qualitativeValuesTable().onAfterSelectionChange(undefined);
    }
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<Parameter>[]): Promise<boolean> {
    let confirmed = true;
    const parameterIds = EntityUtils.ids(dirtyRowsWithDisabledStatusId);

    // Check enabled pmfmu with this parameter
    const exists = await this.pmfmuService.exists(
      PmfmuFilterCriteria.fromObject({
        statusId: StatusIds.ENABLE,
        parameterFilter: { includedIds: parameterIds },
      })
    );
    if (exists) {
      confirmed = await Alerts.askConfirmation('REFERENTIAL.PARAMETER.CONFIRM.DISABLE_PMFMU', this.alertCtrl, this.translate);
    }

    if (confirmed) {
      // Check if used in pmfmu applied strategies
      const res = await this.strategyService.loadPage(
        { offset: 0, size: 100 },
        StrategyFilter.fromObject({
          criterias: [
            {
              programFilter: { statusId: StatusIds.ENABLE },
              onlyActive: true,
              pmfmuFilter: {
                parameterFilter: {
                  includedIds: parameterIds,
                },
              },
            },
          ],
        })
      );

      if (isNotEmptyArray(res.data)) {
        confirmed = await Alerts.askConfirmation(
          'REFERENTIAL.PARAMETER.CONFIRM.DISABLE_APPLIED_STRATEGIES',
          this.alertCtrl,
          this.translate,
          undefined,
          {
            list: `<div class="scroll-content"><ul>${res.data
              .map((r) => referentialToString(r, ['programId', 'name']))
              .map((s) => `<li>${s}</li>`)
              .join('')}</ul></div>`,
          }
        );
      }
    }

    return confirmed;
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.PARAMETERS' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('description', 'comments', 'qualitative', 'taxonomic');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'parameterGroup');
  }
}
