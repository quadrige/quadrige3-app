import { Injectable, Injector } from '@angular/core';
import { EntityUtils } from '@sumaris-net/ngx-components';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { QualitativeValue } from '@app/referential/parameter/qualitative-value/qualitative-value.model';
import { jobFragments } from '@app/social/job/job.service';
import { ParameterFilter, ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';

const qualitativeValueFragment = gql`
  fragment QualitativeValueFragment on QualitativeValueVO {
    id
    name
    description
    comments
    updateDate
    creationDate
    parameterId
    statusId
    __typename
  }
`;

export const parameterFragments = {
  parameter: gql`
    fragment ParameterFragment on ParameterVO {
      id
      name
      description
      comments
      qualitative
      taxonomic
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
  fullParameter: gql`
    fragment FullParameterFragment on ParameterVO {
      id
      name
      description
      comments
      parameterGroup {
        ...ReferentialFragment
      }
      qualitative
      taxonomic
      qualitativeValues {
        ...QualitativeValueFragment
      }
      updateDate
      creationDate
      statusId
      __typename
    }
    ${referentialFragments.light}
    ${qualitativeValueFragment}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Parameters($page: PageInput, $filter: ParameterFilterVOInput) {
      data: parameters(page: $page, filter: $filter) {
        ...ParameterFragment
      }
    }
    ${parameterFragments.parameter}
  `,

  loadAllWithTotal: gql`
    query ParametersWithTotal($page: PageInput, $filter: ParameterFilterVOInput) {
      data: parameters(page: $page, filter: $filter) {
        ...FullParameterFragment
      }
      total: parametersCount(filter: $filter)
    }
    ${parameterFragments.fullParameter}
  `,

  countAll: gql`
    query ParametersCount($filter: ParameterFilterVOInput) {
      total: parametersCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportParametersAsync($filter: ParameterFilterVOInput, $context: ExportContextInput) {
      data: exportParametersAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const otherQueries = {
  qualitativeValues: gql`
    query QualitativeValues($parameterId: String) {
      data: qualitativeValues(parameterId: $parameterId) {
        ...QualitativeValueFragment
      }
    }
    ${qualitativeValueFragment}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveParameters($data: [ParameterVOInput]) {
      data: saveParameters(parameters: $data) {
        ...FullParameterFragment
      }
    }
    ${parameterFragments.fullParameter}
  `,

  deleteAll: gql`
    mutation DeleteParameters($ids: [String]) {
      deleteParameters(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class ParameterService extends BaseReferentialService<Parameter, ParameterFilter, ParameterFilterCriteria, string> {
  constructor(protected injector: Injector) {
    super(injector, Parameter, ParameterFilter, { queries, mutations });
    this._logPrefix = '[parameter-service]';
  }

  async loadQualitativeValues(parameterId: any): Promise<QualitativeValue[]> {
    const variables: any = { parameterId };
    if (this._debug) console.debug(`${this._logPrefix} Loading qualitative values items...`, variables);
    const now = Date.now();
    const query = otherQueries.qualitativeValues;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(QualitativeValue.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} qualitative values items loaded in ${Date.now() - now}ms`);
    return data;
  }

  copyIdAndUpdateDate(source: Parameter | undefined, target: Parameter) {
    super.copyIdAndUpdateDate(source, target);

    // Propagate to qualitative values
    if (source.qualitativeValues && target.qualitativeValues) {
      target.qualitativeValues.forEach((targetQualitativeValue) => {
        const sourceQualitativeValue = source.qualitativeValues.find(
          (value) => value.id === targetQualitativeValue.id || value.name === targetQualitativeValue.name
        );
        EntityUtils.copyIdAndUpdateDate(sourceQualitativeValue, targetQualitativeValue);
        targetQualitativeValue.transcribingItemsLoaded = false;
      });
    }
  }
}
