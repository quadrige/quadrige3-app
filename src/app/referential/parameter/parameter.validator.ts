import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { ParameterService } from '@app/referential/parameter/parameter.service';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

export interface ParameterValidatorOptions extends ReferentialValidatorOptions {
  multipleEnabled?: boolean;
}

@Injectable({ providedIn: 'root' })
export class ParameterValidatorService extends ReferentialValidatorService<Parameter, string, ParameterValidatorOptions> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: ParameterService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Parameter, opts?: ParameterValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        opts?.multipleEnabled ? undefined : ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        opts?.multipleEnabled ? undefined : ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      parameterGroup: [
        data?.parameterGroup || null,
        opts?.multipleEnabled ? SharedValidators.entity : Validators.compose([Validators.required, SharedValidators.entity]),
      ],
      qualitative: [data?.qualitative || false],
      taxonomic: [data?.taxonomic || false],
      qualitativeValues: [data?.qualitativeValues || null],
      statusId: [toNumber(data?.statusId, null), opts?.multipleEnabled ? undefined : Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: Parameter, opts?: ParameterValidatorOptions): AbstractControlOptions {
    return {
      validators: opts?.multipleEnabled
        ? []
        : [
            BaseGroupValidators.requiredIfArrayNotEmpty(
              'qualitative',
              true,
              'qualitativeValues',
              'REFERENTIAL.PARAMETER.ERROR.QUALITATIVE_MUST_BE_TRUE'
            ),
          ],
    };
  }
}
