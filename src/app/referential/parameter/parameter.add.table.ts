import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { ParameterTable } from '@app/referential/parameter/parameter.table';
import { ParameterService } from '@app/referential/parameter/parameter.service';
import { ParameterValidatorService } from '@app/referential/parameter/parameter.validator';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { IParameterFilterCriteria, ParameterFilter, ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { ParameterFilterForm } from '@app/referential/parameter/filter/parameter.filter.form';
import { QualitativeValueTable } from '@app/referential/parameter/qualitative-value/qualitative-value.table';

export type IParameterAddOptions = ISelectModalOptions<IParameterFilterCriteria>;

@Component({
  selector: 'app-parameter-add-table',
  templateUrl: './parameter.table.html',
  standalone: true,
  imports: [ReferentialModule, ParameterFilterForm, QualitativeValueTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterAddTable extends ParameterTable implements IAddTable<Parameter> {
  @Input() addCriteria: IParameterFilterCriteria;
  @Input() addFirstCriteria: IParameterFilterCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: ParameterService,
    protected validatorService: ParameterValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(injector, _entityService, validatorService, pmfmuService, strategyService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[parameter-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: ParameterFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: ParameterFilterCriteria.fromObject(this.addCriteria), // todo: why convert to object ?
      defaultCriteria: ParameterFilterCriteria.fromObject(this.addFirstCriteria),
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
