import { IComposite } from '@app/shared/model/composite';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { IntReferential } from '@app/referential/model/referential.model';

export class ParameterComposite implements IComposite {
  statusId: number = null;
  statusIdMultiple = false;
  parameterGroup: IntReferential = null;
  parameterGroupMultiple = false;

  static fromParameter(source: Parameter): ParameterComposite {
    const target = new ParameterComposite();

    target.statusId = source.statusId;
    target.parameterGroup = IntReferential.fromObject(source.parameterGroup);

    return target;
  }
}
