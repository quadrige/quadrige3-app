import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { CompositeUtils } from '@app/shared/model/composite';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { ParameterComposite } from '@app/referential/parameter/multi-edit/parameter.multi-edit.model';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { GenericService } from '@app/referential/generic/generic.service';
import { ParameterValidatorService } from '@app/referential/parameter/parameter.validator';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { StatusById, StatusIds, StatusList } from '@sumaris-net/ngx-components';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';

export interface IParameterMultiEditModalOptions extends IModalOptions {
  i18nPrefix: string;
  selection: Parameter[];
}

@Component({
  selector: 'app-parameter-multi-edit-modal',
  templateUrl: './parameter.multi-edit.modal.html',
  styleUrl: './parameter.multi-edit.modal.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterMultiEditModal extends ModalComponent<ParameterComposite> implements IParameterMultiEditModalOptions, OnInit {
  @Input() i18nPrefix: string;
  @Input() selection: Parameter[];

  statusList = StatusList;
  statusById = StatusById;
  statusIds = StatusIds;
  panelWidth = autocompleteWidthExtraLarge;

  constructor(
    protected injector: Injector,
    protected referentialGenericService: GenericService,
    protected parameterValidator: ParameterValidatorService
  ) {
    super(injector);
    // Get ParameterComposite form config
    const formConfig = CompositeUtils.getFormGroupConfig(
      new ParameterComposite(),
      parameterValidator.getFormGroupConfig(undefined, { multipleEnabled: true })
    );
    this.setForm(this.formBuilder.group(formConfig, parameterValidator.getFormGroupOptions(undefined, { multipleEnabled: true })));
  }

  ngOnInit() {
    super.ngOnInit();

    // Parameter group combo
    this.registerAutocompleteField('parameterGroup', {
      ...referentialOptions.parameterGroup,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ParameterGroup',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    // Listen for changes
    CompositeUtils.listenCompositeFormChanges(this.form.controls, (subscription) => this.registerSubscription(subscription));
  }

  protected afterInit() {
    this.value = CompositeUtils.build(this.selection, (candidate) => ParameterComposite.fromParameter(candidate));
  }

  protected dataToValidate(): Promise<ParameterComposite> | ParameterComposite {
    return CompositeUtils.value(this.value);
  }
}
