import { ChangeDetectionStrategy, Component } from '@angular/core';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { ParameterFilter, ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-parameter-filter-form',
  templateUrl: './parameter.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterFilterForm extends ReferentialCriteriaFormComponent<ParameterFilter, ParameterFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof ParameterFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias.concat('parameterGroupFilter', 'qualitativeValueFilter', 'programFilter');
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: ParameterFilterCriteria): PartialRecord<keyof ParameterFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.qualitative)) params.qualitative = criteria.qualitative;
    if (isNotNil(criteria.taxonomic)) params.taxonomic = criteria.taxonomic;
    this.subCriteriaToQueryParams(params, criteria, 'parameterGroupFilter');
    this.subCriteriaToQueryParams(params, criteria, 'qualitativeValueFilter');
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'strategyFilter');
    return params;
  }
}
