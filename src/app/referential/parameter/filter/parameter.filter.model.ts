import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export interface IParameterFilterCriteria extends IReferentialFilterCriteria<string> {
  qualitative?: boolean;
  taxonomic?: boolean;
  parameterGroupFilter?: IReferentialFilterCriteria<number>;
  qualitativeValueFilter?: IReferentialFilterCriteria<number>;
  programFilter?: IReferentialFilterCriteria<string>;
  strategyFilter?: IReferentialFilterCriteria<number>;
}

@EntityClass({ typename: 'ParameterFilterCriteriaVO' })
export class ParameterFilterCriteria extends ReferentialFilterCriteria<Parameter, string> implements IParameterFilterCriteria {
  static fromObject: (source: any, opts?: any) => ParameterFilterCriteria;

  qualitative: boolean = null;
  taxonomic: boolean = null;
  parameterGroupFilter: IntReferentialFilterCriteria = null;
  qualitativeValueFilter: IntReferentialFilterCriteria = null;
  programFilter: StrReferentialFilterCriteria = null;
  strategyFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(ParameterFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.qualitative = source.qualitative;
    this.taxonomic = source.taxonomic;
    this.parameterGroupFilter = IntReferentialFilterCriteria.fromObject(source.parameterGroupFilter || {});
    this.qualitativeValueFilter = IntReferentialFilterCriteria.fromObject(source.qualitativeValueFilter || {});
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.strategyFilter = IntReferentialFilterCriteria.fromObject(source.strategyFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parameterGroupFilter = this.parameterGroupFilter?.asObject(opts);
    target.qualitativeValueFilter = this.qualitativeValueFilter?.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    target.strategyFilter = { ...this.strategyFilter?.asObject(opts), searchAttributes: ['id', 'name', 'description'] };
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'parameterGroupFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterGroupFilter, true);
    if (key === 'qualitativeValueFilter') return !BaseFilterUtils.isCriteriaEmpty(this.qualitativeValueFilter, true);
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'strategyFilter') return !BaseFilterUtils.isCriteriaEmpty(this.strategyFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'ParameterFilterVO' })
export class ParameterFilter extends ReferentialFilter<ParameterFilter, ParameterFilterCriteria, Parameter, string> {
  static fromObject: (source: any, opts?: any) => ParameterFilter;

  constructor() {
    super(ParameterFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): ParameterFilterCriteria {
    return ParameterFilterCriteria.fromObject(source, opts);
  }
}
