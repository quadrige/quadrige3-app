import { NgModule } from '@angular/core';
import { ParameterTable } from '@app/referential/parameter/parameter.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { QualitativeValueTable } from '@app/referential/parameter/qualitative-value/qualitative-value.table';
import { ParameterMultiEditModal } from '@app/referential/parameter/multi-edit/parameter.multi-edit.modal';
import { ParameterFilterForm } from '@app/referential/parameter/filter/parameter.filter.form';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';

@NgModule({
  imports: [ReferentialModule, ParameterFilterForm, QualitativeValueTable, FractionMatrixTable],
  declarations: [ParameterTable, ParameterMultiEditModal],
  exports: [ParameterTable],
})
export class ParameterModule {}
