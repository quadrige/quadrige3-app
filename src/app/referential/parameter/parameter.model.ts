import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { QualitativeValue } from '@app/referential/parameter/qualitative-value/qualitative-value.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'ParameterVO' })
export class Parameter extends Referential<Parameter, string> {
  static entityName = 'Parameter';
  static fromObject: (source: any, opts?: any) => Parameter;

  qualitative: boolean = null;
  taxonomic: boolean = null;
  parameterGroup: IntReferential = null;
  qualitativeValues: QualitativeValue[] = null;

  constructor() {
    super(Parameter.TYPENAME);
    this.entityName = Parameter.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Parameter.entityName;
    this.qualitative = source.qualitative;
    this.taxonomic = source.taxonomic;
    this.parameterGroup = IntReferential.fromObject(source.parameterGroup);
    this.qualitativeValues = source.qualitativeValues?.map(QualitativeValue.fromObject);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parameterGroup = EntityUtils.asMinifiedObject(this.parameterGroup, opts);
    target.qualitativeValues = this.qualitativeValues?.map((qv) => qv.asObject(opts));
    return target;
  }
}
