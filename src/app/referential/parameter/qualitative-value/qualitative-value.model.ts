import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'QualitativeValueVO' })
export class QualitativeValue extends Referential<QualitativeValue> {
  static entityName = 'QualitativeValue';
  static fromObject: (source: any, opts?: any) => QualitativeValue;

  parameterId: string = null;

  constructor(parameterId?: string) {
    super(QualitativeValue.TYPENAME);
    this.entityName = QualitativeValue.entityName;
    this.parameterId = parameterId;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = QualitativeValue.entityName;
    this.parameterId = source.parameterId;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.entityName;
    return target;
  }
}
