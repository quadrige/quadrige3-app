import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { QualitativeValueFilterForm } from '@app/referential/parameter/qualitative-value/filter/qualitative-value.filter.form';
import { GenericService } from '@app/referential/generic/generic.service';
import { ReferentialGenericValidatorService } from '@app/referential/generic/generic.validator';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-qualitative-value-select-table',
  templateUrl: './qualitative-value.table.html',
  standalone: true,
  imports: [ReferentialModule, QualitativeValueFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QualitativeValueSelectTable extends GenericSelectTable implements OnInit {
  constructor(
    protected injector: Injector,
    protected referentialGenericService: GenericService,
    protected validatorService: ReferentialGenericValidatorService
  ) {
    super(injector, referentialGenericService, validatorService);

    this.titleI18n = 'REFERENTIAL.ENTITY.QUALITATIVE_VALUES';
    this.i18nColumnPrefix = 'REFERENTIAL.QUALITATIVE_VALUE.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[qualitative-value-select-table]';
  }

  ngOnInit() {
    super.ngOnInit();
    this.rightPanelAllowed = true;
    setTimeout(() => this.setEntityName('QualitativeValue'));
  }

  protected async initRightView() {
    await super.initRightView();
    this.transcribingItemEnabled.set(true);
    this.restoreRightPanel(true, 40);
  }

  protected async loadRightArea(row?: AsyncTableElement<GenericReferential>): Promise<void> {
    await super.loadRightArea(row);
    // Force no edition on transcribing table
    this.transcribingItemTable().canEdit = false;
  }
}
