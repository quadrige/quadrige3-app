import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { QualitativeValue } from '@app/referential/parameter/qualitative-value/qualitative-value.model';
import { IntReferentialFilter } from '@app/referential/model/referential.filter.model';

@Injectable({ providedIn: 'root' })
export class QualitativeValueMemoryService extends EntitiesMemoryService<QualitativeValue, IntReferentialFilter> {
  constructor() {
    super(QualitativeValue, IntReferentialFilter);
  }
}
