import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { GenericService } from '@app/referential/generic/generic.service';
import {
  lengthComment,
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { QualitativeValue } from '@app/referential/parameter/qualitative-value/qualitative-value.model';

@Injectable({ providedIn: 'root' })
export class QualitativeValueValidatorService extends ReferentialValidatorService<QualitativeValue> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialService: GenericService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: QualitativeValue, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.referentialService, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
