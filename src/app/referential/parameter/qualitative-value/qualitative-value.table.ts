import { AfterViewInit, ChangeDetectionStrategy, Component, inject, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { QualitativeValue } from '@app/referential/parameter/qualitative-value/qualitative-value.model';
import { QualitativeValueMemoryService } from '@app/referential/parameter/qualitative-value/qualitative-value-memory.service';
import { QualitativeValueValidatorService } from '@app/referential/parameter/qualitative-value/qualitative-value.validator';
import { UntypedFormGroup } from '@angular/forms';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ISelectCriteria } from '@app/shared/model/options.model';
import { ReferentialValidatorOptions } from '@app/referential/service/referential-validator.service';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';
import { TranscribingItemTypeService } from '@app/referential/transcribing-item-type/transcribing-item-type.service';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { ReferentialModule } from '@app/referential/referential.module';
import { QualitativeValueFilterForm } from '@app/referential/parameter/qualitative-value/filter/qualitative-value.filter.form';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-qualitative-value-table',
  templateUrl: './qualitative-value.table.html',
  standalone: true,
  imports: [ReferentialModule, QualitativeValueFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QualitativeValueTable
  extends ReferentialMemoryTable<QualitativeValue, number, IntReferentialFilter, IntReferentialFilterCriteria, QualitativeValueValidatorService>
  implements OnInit, AfterViewInit
{
  @ViewChild('transcribingItemTable') transcribingItemTable: TranscribingItemRowTable;
  protected transcribingItemTypeService = inject(TranscribingItemTypeService);

  @Input() selectCriteria: ISelectCriteria;

  transcribingMenuItems: IEntityMenuItem<QualitativeValue, TranscribingItemView>[] = [];

  constructor(
    protected injector: Injector,
    protected _entityService: QualitativeValueMemoryService,
    protected validatorService: QualitativeValueValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      QualitativeValue,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.ENTITY.QUALITATIVE_VALUES';
    this.i18nColumnPrefix = 'REFERENTIAL.QUALITATIVE_VALUE.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[qualitative-value-table]';
  }

  ngOnInit() {
    this.subTable = true;
    super.ngOnInit();

    // Initialize right menu
    this.transcribingItemTypeService.initMenus(this.entityName).then((transcribingMenuItems) => {
      this.transcribingMenuItems = transcribingMenuItems;
      this.rightMenuItems = this.transcribingMenuItems;
      this.rightMenuItem = this.transcribingMenuItems?.[0]?.children?.[0];
      this.restoreRightPanel(true, 50);
      this.markForCheck();
    });
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<QualitativeValue>): Promise<boolean> {
    row = row || this.singleEditingRow || this.singleSelectedRow;

    // Save transcribing items table first
    if (row && this.transcribingItemTable.dirty) {
      if (!(await this.transcribingItemTable.save())) {
        return false;
      }
    }

    return super.confirmEditCreate(event, row);
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    // Save transcribing items table first
    const continueSave = await this.transcribingItemTable.save();
    if (continueSave) {
      return super.save(opts);
    }
    return false;
  }

  protected async canRightMenuItemChange(
    previousMenuItem: IEntityMenuItem<QualitativeValue, TranscribingItemView>,
    nextMenuItem: IEntityMenuItem<QualitativeValue, TranscribingItemView>
  ): Promise<boolean> {
    if (previousMenuItem.view === 'transcribingItemTable' && this.transcribingItemTable?.dirty) {
      if (!(await this.transcribingItemTable.save())) {
        return false;
      }
    }
    return true;
  }

  protected async loadRightArea() {
    await this.transcribingItemTypeService.ready();
    if (this.rightMenuItem?.view === 'transcribingItemTable') {
      if (!this.transcribingItemTable) {
        this.cd.detectChanges();
      }
      if (this.transcribingItemTable) {
        this.registerSubForm(this.transcribingItemTable);
      } else {
        console.error(`${this.logPrefix} Transcribing item table not detected. Please check the template.`);
        return;
      }
      console.debug(`${this.logPrefix} load transcribing item table`);
      await this.transcribingItemTable.setSelection(this.selection.selected, this.rightMenuItem?.params);
      this.transcribingItemTable.updateTitle();
      this.transcribingItemTable.canEdit = this.canEdit && this.inlineEdition;
    }
  }

  protected getRowValidator(row?: AsyncTableElement<QualitativeValue>): UntypedFormGroup {
    return super.getRowValidator(row, <ReferentialValidatorOptions>{
      criteria: this.selectCriteria,
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name');
  }
}
