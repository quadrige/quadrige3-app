import { Injectable } from '@angular/core';
import { BaseValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';
import { IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { AbstractControlOptions, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { SharedValidators } from '@sumaris-net/ngx-components';

export interface ReferentialFilterFormFieldValidatorOptions {
  required?: boolean;
}

@Injectable({ providedIn: 'root' })
export class ReferentialFilterFormFieldValidator extends BaseValidatorService<
  IReferentialFilterCriteria<any>,
  ReferentialFilterFormFieldValidatorOptions
> {
  private static requiredValidator = BaseValidators.criteriaRequired;

  public static hasRequiredValidator(formGroup: UntypedFormGroup): boolean {
    return formGroup?.hasValidator(ReferentialFilterFormFieldValidator.requiredValidator);
  }

  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: IReferentialFilterCriteria<any>, opts?: ReferentialFilterFormFieldValidatorOptions): { [p: string]: any } {
    return <Record<keyof IReferentialFilterCriteria<any>, any>>{
      searchText: [data?.searchText || null],
      searchAttributes: [data?.searchAttributes || null],
      includedIds: [data?.includedIds || null],
      excludedIds: [data?.excludedIds || null],
      systemId: [data?.systemId || null],
    };
  }

  getFormGroupOptions(data?: IReferentialFilterCriteria<any>, opts?: ReferentialFilterFormFieldValidatorOptions): AbstractControlOptions {
    const options = super.getFormGroupOptions(data, opts);
    if (opts?.required) {
      options.validators = ReferentialFilterFormFieldValidator.requiredValidator;
    }
    return options;
  }

  updateFormGroup(formGroup: UntypedFormGroup, opts?: ReferentialFilterFormFieldValidatorOptions) {
    if (opts?.required) {
      if (!formGroup.hasValidator(ReferentialFilterFormFieldValidator.requiredValidator)) {
        formGroup.addValidators(ReferentialFilterFormFieldValidator.requiredValidator);
      }
    } else {
      if (formGroup.hasValidator(ReferentialFilterFormFieldValidator.requiredValidator)) {
        formGroup.removeValidators(ReferentialFilterFormFieldValidator.requiredValidator);
        SharedValidators.clearError(formGroup, 'required');
      }
    }
  }
}
