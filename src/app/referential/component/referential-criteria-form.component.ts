import { Directive, viewChildren } from '@angular/core';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';
import { isNotNil, StatusList } from '@sumaris-net/ngx-components';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { PartialRecord } from '@app/shared/model/interface';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

/**
 * Base component for referential table filter templates
 * Should be in shared directory but still use a reference to a referential component
 */
@Directive()
export abstract class ReferentialCriteriaFormComponent<
  F extends ReferentialFilter<F, C, any, any>,
  C extends ReferentialFilterCriteria<any, any>,
> extends BaseCriteriaFormComponent<F, C> {
  referentialFilterFormFields = viewChildren(ReferentialFilterFormField);

  statusList = StatusList;
  autocompleteWidthExtraLarge = autocompleteWidthExtraLarge;

  protected constructor() {
    super();
  }

  updateView() {
    super.updateView();
    this.referentialFilterFormFields()?.forEach((item) => item.updateView());
  }

  // todo: implement this to add global referential criteria to activate
  // protected filterCriteriaBySystem(criteria: string, systemId: TranscribingSystemType): boolean {
  //   return super.filterCriteriaBySystem(criteria, systemId);
  // }

  protected criteriaToQueryParams(criteria: C): PartialRecord<keyof C, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria?.statusId)) params.statusId = criteria.statusId;
    return params;
  }

  protected updateSystemId(systemId: TranscribingSystemType, reset?: boolean) {
    super.updateSystemId(systemId, reset);
    this.referentialFilterFormFields()?.forEach((item) => item.systemId.set(systemId));
  }
}
