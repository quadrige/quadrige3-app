import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injectable, Injector, Input, OnInit, Output } from '@angular/core';
import { Entity, EntityAsObjectOptions, fromDateISOString, isNil, isNilOrNaN, SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { FilterOperatorType } from '@app/referential/filter/model/filter-operator-type';
import { Optional } from 'typescript-optional';
import { EntityUtils } from '@app/shared/entity.utils';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { BaseForm } from '@app/shared/form/base.form';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';

export interface IDateFilterValue {
  operator: FilterOperatorType;
  value: Moment;
  value2?: Moment;
}

export class DateFilterValue extends Entity<DateFilterValue> implements IDateFilterValue {
  operator: FilterOperatorType;
  value: Moment;
  value2: Moment;

  constructor() {
    super('DateFilterValue');
  }

  static default(): DateFilterValue {
    return DateFilterValue.fromObject({ operator: 'DATE_EQUAL', value: undefined, value2: undefined });
  }

  static isEmpty(v: IDateFilterValue): boolean {
    return isNil(v) || DateFilterValue.equals(DateFilterValue.default(), v);
  }

  static equals(v1: IDateFilterValue, v2: IDateFilterValue): boolean {
    return EntityUtils.deepEquals(v1, v2, ['operator', 'value', 'value2']);
  }

  static fromObject(source: any): DateFilterValue {
    if (!source) return DateFilterValue.default();
    const target = new DateFilterValue();
    target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.operator = source.operator;
    this.value = fromDateISOString(source.value);
    this.value2 = fromDateISOString(source.value2);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.value = Dates.toLocalDateString(this.value);
    target.value2 = Dates.toLocalDateString(this.value2);
    delete target.updateDate;
    return target;
  }
}

@Injectable({ providedIn: 'root' })
export class DateFilterValidator extends BaseValidatorService<DateFilterValue> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: DateFilterValue, opts?: any): { [p: string]: any } {
    return <Record<keyof IDateFilterValue, any>>{
      operator: ['DATE_EQUAL', Validators.required],
      value: [null],
      value2: [null],
    };
  }

  updateFormGroup(form: UntypedFormGroup, operator?: FilterOperatorType) {
    form.clearValidators();
    operator = operator || form.value.operator;

    if (operator === 'DATE_BETWEEN') {
      form.setValidators([
        SharedFormGroupValidators.requiredIf('value2', 'value'),
        SharedFormGroupValidators.requiredIf('value', 'value2'),
        SharedFormGroupValidators.dateRange('value', 'value2'),
      ]);
    } else {
      form.patchValue({ value2: null });
    }
  }
}

@Component({
  selector: 'app-date-filter-form-field',
  templateUrl: './date-filter-form-field.component.html',
  styleUrls: ['./date-filter-form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateFilterFormField extends BaseForm<IDateFilterValue> implements OnInit, AfterViewInit {
  @Input() i18nPrefix = 'REFERENTIAL.FILTER.';
  @Input() placeholderI18n: string;
  @Input() clearable = true;
  @Input() appAutofocus = false;

  @Output() textSubmit = new EventEmitter<any>();

  operators: FilterOperatorType[] = ['DATE_EQUAL', 'DATE_GREATER', 'DATE_GREATER_OR_EQUAL', 'DATE_LESS', 'DATE_LESS_OR_EQUAL', 'DATE_BETWEEN'];

  constructor(
    protected injector: Injector,
    protected validator: DateFilterValidator
  ) {
    super(injector, validator.getFormGroup(null));
    this.logPrefix = '[date-filter-form-field]';
  }

  get empty(): boolean {
    return isNilOrNaN(this.form.get('value').value);
  }

  get isBetween(): boolean {
    return this.value.operator === 'DATE_BETWEEN';
  }

  ngAfterViewInit() {
    Optional.ofNullable(this.form.get('operator')).ifPresent((control) =>
      this.registerSubscription(
        control.valueChanges.subscribe((operator) => {
          this.validator.updateFormGroup(this.form, operator);
        })
      )
    );
    this.enable();
  }

  setValue(data: IDateFilterValue, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> | void {
    return super.setValue(data, opts);
  }

  clear(event: MouseEvent) {
    event.stopPropagation();
    this.form.patchValue({ operator: 'DATE_EQUAL', value: null, value2: null });
    this.markAsDirty();
  }
}
