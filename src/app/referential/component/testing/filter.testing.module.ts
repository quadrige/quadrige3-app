import { inject, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRouteSnapshot, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { FilterTestPage } from './filter.test.page';
import { ReferentialModule } from '../../referential.module';
import { QuadrigeSharedModule } from '@app/shared/quadrige.shared.module';
import { AUTH_GUARD } from '@app/core/service/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FilterTestPage,
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
  },
];

@NgModule({
  imports: [CommonModule, QuadrigeSharedModule, RouterModule.forChild(routes), ReferentialModule],
  declarations: [FilterTestPage],
  exports: [FilterTestPage],
})
export class FilterTestingModule {}
