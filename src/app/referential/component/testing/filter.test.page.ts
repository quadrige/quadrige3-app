import { AfterViewInit, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NumberFilterFormField, NumberFilterValidator } from '@app/referential/component/number-filter-form-field.component';
import { BaseForm } from '@app/shared/form/base.form';

@Component({
  selector: 'app-filter-test-page',
  templateUrl: './filter.test.page.html',
})
export class FilterTestPage extends BaseForm<any> implements OnInit, AfterViewInit {
  @ViewChild('numberFilterFormField') numberFilterFormField: NumberFilterFormField;

  stringify = JSON.stringify;

  constructor(
    protected injector: Injector,
    protected numberFilterValidator: NumberFilterValidator
  ) {
    super(injector);
    this.setForm(
      this.formBuilder.group({
        filterDefault: [null],
        filterWithMode: [null],
        filterListOnly: [null],
        number: [null],
        numberForm: numberFilterValidator.getFormGroup(null),
      })
    );
  }

  ngAfterViewInit(): void {
    this.enable();

    this.numberFilterFormField.form.valueChanges.subscribe((value) => {
      this.form.patchValue({ number: value });
    });
    // this.numberFilterFormField.form.statusChanges.subscribe((value) => this.form.updateValueAndValidity());
  }
}
