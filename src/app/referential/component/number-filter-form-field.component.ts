import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injectable, Injector, Input, OnInit, Output } from '@angular/core';
import { Entity, EntityAsObjectOptions, isNil, isNilOrNaN, SharedFormGroupValidators, SharedValidators } from '@sumaris-net/ngx-components';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { FilterOperatorType } from '@app/referential/filter/model/filter-operator-type';
import { Optional } from 'typescript-optional';
import { EntityUtils } from '@app/shared/entity.utils';
import { BaseGroupValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';
import { BaseForm } from '@app/shared/form/base.form';

export interface INumberFilterValue {
  operator: FilterOperatorType;
  value: number;
  value2?: number;
}

export class NumberFilterValue extends Entity<NumberFilterValue> implements INumberFilterValue {
  operator: FilterOperatorType;
  value: number;
  value2: number;

  constructor() {
    super('NumberFilterValue');
  }

  static default(): NumberFilterValue {
    return NumberFilterValue.fromObject({ operator: 'DOUBLE_EQUAL', value: null, value2: null });
  }

  static isEmpty(v: INumberFilterValue): boolean {
    return isNil(v) || NumberFilterValue.equals(NumberFilterValue.default(), v);
  }

  static equals(v1: INumberFilterValue, v2: INumberFilterValue): boolean {
    return EntityUtils.deepEquals(v1, v2, ['operator', 'value', 'value2']);
  }

  static fromObject(source: any): NumberFilterValue {
    const target = new NumberFilterValue();
    target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.operator = source.operator;
    this.value = source.value;
    this.value2 = source.value2;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.updateDate;
    return target;
  }
}

@Injectable({ providedIn: 'root' })
export class NumberFilterValidator extends BaseValidatorService<NumberFilterValue> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: NumberFilterValue, opts?: any): { [p: string]: any } {
    return <Record<keyof INumberFilterValue, any>>{
      operator: ['DOUBLE_EQUAL', Validators.required],
      value: [null, SharedValidators.decimal()],
      value2: [null, SharedValidators.decimal()],
    };
  }

  updateFormGroup(form: UntypedFormGroup, operator?: FilterOperatorType) {
    form.clearValidators();
    operator = operator || form.value.operator;

    if (operator === 'DOUBLE_BETWEEN') {
      form.setValidators([
        SharedFormGroupValidators.requiredIf('value2', 'value'),
        SharedFormGroupValidators.requiredIf('value', 'value2'),
        BaseGroupValidators.minMax('value', 'value2'),
      ]);
    } else {
      form.patchValue({ value2: null });
    }
  }
}

@Component({
  selector: 'app-number-filter-form-field',
  templateUrl: './number-filter-form-field.component.html',
  styleUrls: ['./number-filter-form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumberFilterFormField extends BaseForm<INumberFilterValue> implements OnInit, AfterViewInit {
  @Input() i18nPrefix = 'REFERENTIAL.FILTER.';
  @Input() placeholderI18n: string;
  @Input() clearable = true;
  @Input() appAutofocus = false;

  @Output() textSubmit = new EventEmitter<any>();

  operators: FilterOperatorType[] = [
    'DOUBLE_EQUAL',
    'DOUBLE_GREATER',
    'DOUBLE_GREATER_OR_EQUAL',
    'DOUBLE_LESS',
    'DOUBLE_LESS_OR_EQUAL',
    'DOUBLE_BETWEEN',
  ];

  constructor(
    protected injector: Injector,
    protected validator: NumberFilterValidator
  ) {
    super(injector, validator.getFormGroup(null));
    this.logPrefix = '[number-filter-form-field]';
  }

  get empty(): boolean {
    return isNilOrNaN(this.form.get('value').value);
  }

  get isBetween(): boolean {
    return this.value.operator === 'DOUBLE_BETWEEN';
  }

  ngAfterViewInit() {
    Optional.ofNullable(this.form.get('operator')).ifPresent((control) =>
      this.registerSubscription(
        control.valueChanges.subscribe((operator) => {
          this.validator.updateFormGroup(this.form, operator);
        })
      )
    );
    this.enable();
  }

  setValue(data: INumberFilterValue, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> | void {
    return super.setValue(data, opts);
  }

  clear(event: MouseEvent) {
    event.stopPropagation();
    this.form.patchValue({ operator: 'DOUBLE_EQUAL', value: null, value2: null });
    this.markAsDirty();
  }
}
