import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, inject, Input, OnInit, Output } from '@angular/core';
import { IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { GenericService } from '@app/referential/generic/generic.service';
import { TranslateContextService } from '@sumaris-net/ngx-components';
import { FilterHint } from '@app/shared/model/filter.model';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { ReferentialFilterUtils } from '@app/referential/model/referential.filter.utils';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

export interface IReferentialFilterCriteriaHintOptions {
  font?: string;
  maxWidth?: number;
  systemId?: TranscribingSystemType;
}

@Component({
  selector: 'app-referential-filter-hint',
  templateUrl: './referential-filter-hint.component.html',
  styleUrls: ['./referential-filter-hint.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule],
})
export class ReferentialFilterHintComponent implements OnInit {
  @Input() entityName: string;
  @Input() displayWith: (filterCriteria: IReferentialFilterCriteria<any>, opts?: IReferentialFilterCriteriaHintOptions) => Promise<FilterHint>;
  @Input() showPrefix: boolean;

  @Output() rendered = new EventEmitter<void>();

  hint: FilterHint;
  renderingHint = false;
  private logPrefix = '[referential-filter-hint]';
  private readonly cd = inject(ChangeDetectorRef);
  private readonly genericService = inject(GenericService);
  private readonly translate = inject(TranslateContextService);

  constructor() {}

  ngOnInit(): void {
    this.genericService.ready().then(() => {
      // Default values
      const type = this.genericService.getType(this.entityName);
      if (!type) {
        throw new Error(`${this.logPrefix} Entity type for {${this.entityName}} not found !`);
      }
      this.displayWith =
        this.displayWith ||
        ((filterCriteria, opts) =>
          ReferentialFilterUtils.toString(filterCriteria, {
            type,
            service: this.genericService,
            translate: this.translate,
            ...opts,
          }));
      this.render(undefined);
    });
  }

  render(filterCriteria: IReferentialFilterCriteria<any>, opts?: IReferentialFilterCriteriaHintOptions) {
    if (this.displayWith) {
      this.renderingHint = true;
      this.cd.markForCheck();
      this.displayWith(filterCriteria, opts).then((hint) => {
        this.hint = hint;
        this.renderingHint = false;
        this.cd.markForCheck();
        this.rendered.emit();
      });
    }
  }
}
