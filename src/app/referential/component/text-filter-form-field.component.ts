import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injectable, Injector, Input, OnInit, Output } from '@angular/core';
import { arraySize, Entity, EntityAsObjectOptions, isNil, isNilOrBlank } from '@sumaris-net/ngx-components';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { FilterOperatorType } from '@app/referential/filter/model/filter-operator-type';
import { EntityUtils } from '@app/shared/entity.utils';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { BaseForm } from '@app/shared/form/base.form';

export interface ITextFilterValue {
  operator: FilterOperatorType;
  value: string;
}

export class TextFilterValue extends Entity<TextFilterValue> implements ITextFilterValue {
  operator: FilterOperatorType;
  value: string;

  constructor() {
    super('TextFilterValue');
  }

  static default(operator?: FilterOperatorType): TextFilterValue {
    return TextFilterValue.fromObject({ operator: operator || 'TEXT_EQUAL', value: null });
  }

  static isEmpty(v: ITextFilterValue): boolean {
    return isNil(v) || TextFilterValue.equals(TextFilterValue.default(v.operator), v);
  }

  static equals(v1: ITextFilterValue, v2: ITextFilterValue): boolean {
    return EntityUtils.deepEquals(v1, v2, ['operator', 'value']);
  }

  static fromObject(source: any): TextFilterValue {
    const target = new TextFilterValue();
    target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.operator = source.operator;
    this.value = source.value;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.updateDate;
    return target;
  }
}

@Injectable({ providedIn: 'root' })
export class TextFilterValidator extends BaseValidatorService<TextFilterValue> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: TextFilterValue, opts?: any): { [p: string]: any } {
    return <Record<keyof ITextFilterValue, any>>{
      operator: ['TEXT_EQUAL', Validators.required],
      value: [null, Validators.maxLength(2000)],
    };
  }

  updateFormGroup(form: UntypedFormGroup, operators?: FilterOperatorType[]) {
    if (arraySize(operators) === 1) {
      form.patchValue({ operator: operators[0] });
    }
  }
}

@Component({
  selector: 'app-text-filter-form-field',
  templateUrl: './text-filter-form-field.component.html',
  styleUrls: ['./text-filter-form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextFilterFormField extends BaseForm<ITextFilterValue> implements OnInit, AfterViewInit {
  @Input() i18nPrefix = 'REFERENTIAL.FILTER.';
  @Input() placeholderI18n: string;
  @Input() clearable = true;
  @Input() appAutofocus = false;
  @Input() operators: FilterOperatorType[] = ['TEXT_EQUAL', 'TEXT_CONTAINS', 'TEXT_STARTS', 'TEXT_ENDS'];

  @Output() textSubmit = new EventEmitter<any>();

  constructor(
    protected injector: Injector,
    protected validator: TextFilterValidator
  ) {
    super(injector, validator.getFormGroup(null));
    this.logPrefix = '[text-filter-form-field]';
  }

  get empty(): boolean {
    return isNilOrBlank(this.form.get('value').value);
  }

  ngAfterViewInit() {
    this.validator.updateFormGroup(this.form, this.operators);
    this.enable();
    // fixme: can't disable operator because form.value will delete it
    // if (this.operators?.length < 2) {
    //   this.form.get('operator').disable();
    // }
  }

  setValue(data: ITextFilterValue, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> | void {
    return super.setValue(data, opts);
  }

  clear(event: MouseEvent) {
    event.stopPropagation();
    this.form.patchValue({ operator: arraySize(this.operators) === 1 ? this.operators[0] : 'TEXT_EQUAL', value: null });
    this.markAsDirty();
  }
}
