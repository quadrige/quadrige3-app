import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injectable, Injector, Input, OnInit, Output } from '@angular/core';
import { Entity, EntityAsObjectOptions, isNil, isNilOrBlank, isNilOrNaN, SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { FilterOperatorType } from '@app/referential/filter/model/filter-operator-type';
import { Optional } from 'typescript-optional';
import { EntityUtils } from '@app/shared/entity.utils';
import { BaseGroupValidators, BaseValidatorService } from '@app/shared/service/base-validator.service';
import { BaseForm } from '@app/shared/form/base.form';

export interface ITimeFilterValue {
  operator: FilterOperatorType;
  value: string;
  value2?: string;
}

export class TimeFilterValue extends Entity<TimeFilterValue> implements ITimeFilterValue {
  operator: FilterOperatorType;
  value: string;
  value2: string;

  constructor() {
    super('TimeFilterValue');
  }

  static default(): TimeFilterValue {
    return TimeFilterValue.fromObject({ operator: 'HOUR_EQUAL', value: null, value2: null });
  }

  static isEmpty(v: ITimeFilterValue): boolean {
    return isNil(v) || TimeFilterValue.equals(TimeFilterValue.default(), v);
  }

  static equals(v1: ITimeFilterValue, v2: ITimeFilterValue): boolean {
    return EntityUtils.deepEquals(v1, v2, ['operator', 'value', 'value2']);
  }

  static fromObject(source: any): TimeFilterValue {
    const target = new TimeFilterValue();
    target.fromObject(source);
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.operator = source.operator;
    this.value = source.value;
    this.value2 = source.value2;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.updateDate;
    return target;
  }
}

@Injectable({ providedIn: 'root' })
export class TimeFilterValidator extends BaseValidatorService<TimeFilterValue> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: TimeFilterValue, opts?: any): { [p: string]: any } {
    return <Record<keyof ITimeFilterValue, any>>{
      operator: ['HOUR_EQUAL', Validators.required],
      value: [null],
      value2: [null],
    };
  }

  updateFormGroup(form: UntypedFormGroup, operator?: FilterOperatorType) {
    form.clearValidators();
    operator = operator || form.value.operator;

    if (operator === 'HOUR_BETWEEN') {
      form.setValidators([
        SharedFormGroupValidators.requiredIf('value2', 'value'),
        SharedFormGroupValidators.requiredIf('value', 'value2'),
        BaseGroupValidators.minMax('value', 'value2'), // todo this validator should not work properly
      ]);
    } else {
      form.patchValue({ value2: null });
    }
  }
}

@Component({
  selector: 'app-time-filter-form-field',
  templateUrl: './time-filter-form-field.component.html',
  styleUrls: ['./time-filter-form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeFilterFormField extends BaseForm<ITimeFilterValue> implements OnInit, AfterViewInit {
  @Input() i18nPrefix = 'REFERENTIAL.FILTER.';
  @Input() placeholderI18n: string;
  @Input() clearable = true;
  @Input() appAutofocus = false;

  @Output() textSubmit = new EventEmitter<any>();

  operators: FilterOperatorType[] = ['HOUR_EQUAL', 'HOUR_GREATER', 'HOUR_GREATER_OR_EQUAL', 'HOUR_LESS', 'HOUR_LESS_OR_EQUAL', 'HOUR_BETWEEN'];

  constructor(
    protected injector: Injector,
    protected validator: TimeFilterValidator
  ) {
    super(injector, validator.getFormGroup(null));
    this.logPrefix = '[time-filter-form-field]';
  }

  get empty(): boolean {
    return isNilOrNaN(this.form.get('value').value);
  }

  get isBetween(): boolean {
    return this.value.operator === 'HOUR_BETWEEN';
  }

  ngAfterViewInit() {
    Optional.ofNullable(this.form.get('operator')).ifPresent((control) =>
      this.registerSubscription(
        control.valueChanges.subscribe((operator) => {
          this.validator.updateFormGroup(this.form, operator);
        })
      )
    );
    this.enable();
  }

  setValue(data: ITimeFilterValue, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> | void {
    return super.setValue(data, opts);
  }

  clear(event: MouseEvent) {
    event.stopPropagation();
    this.form.patchValue({ operator: 'HOUR_EQUAL', value: null, value2: null });
    this.markAsDirty();
  }

  valueChange(event: KeyboardEvent) {
    if (isNilOrBlank(this.form.get('value').value) && event.key.match(/\d/)) {
      this.form.patchValue({ value: `0${event.key}:00:00` });
      this.markAsDirty();
    }
  }

  value2Change(event: KeyboardEvent) {
    if (isNilOrBlank(this.form.get('value2').value) && event.key.match(/\d/)) {
      this.form.patchValue({ value2: `0${event.key}:00:00` });
      this.markAsDirty();
    }
  }
}
