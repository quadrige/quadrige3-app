import { NgModule } from '@angular/core';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { NumberFilterFormField } from '@app/referential/component/number-filter-form-field.component';
import { SelectionModule } from '@app/selection/selection.module';
import { ReferentialFilterHintComponent } from '@app/referential/component/referential-filter-hint.component';
import { TextFilterFormField } from '@app/referential/component/text-filter-form-field.component';
import { TimeFilterFormField } from '@app/referential/component/time-filter-form-field.component';
import { DateFilterFormField } from '@app/referential/component/date-filter-form-field.component';
import { BaseFilterFormComponent } from '@app/shared/component/filter/base-filter-form.component';

const filterComponents = [DateFilterFormField, NumberFilterFormField, TextFilterFormField, TimeFilterFormField];

@NgModule({
  imports: [QuadrigeCoreModule, SelectionModule, BaseFilterFormComponent, ReferentialFilterFormField, ReferentialFilterHintComponent],
  declarations: filterComponents,
  exports: filterComponents,
})
export class ReferentialFilterComponentModule {}
