import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  inject,
  Injector,
  input,
  model,
  OnInit,
  output,
  viewChild,
} from '@angular/core';
import { focusInput, InputElement, isNilOrBlank, isNotEmptyArray, isNotNil, isNotNilOrBlank } from '@sumaris-net/ngx-components';
import { FloatLabelType } from '@angular/material/form-field';
import { IAddResult, ISelectCriteria, ISelectModalOptions, ISelectResult } from '@app/shared/model/options.model';
import { IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { BaseForm } from '@app/shared/form/base.form';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { EntitySelectModal } from '@app/selection/entity.select.modal';
import { ReferentialFilterHintComponent } from '@app/referential/component/referential-filter-hint.component';
import { entitiesTableNeedingMoreWidth } from '@app/referential/model/referential.constants';
import {
  TranscribingFunctionTypeBySystemType,
  transcribingSystemsForFilter,
  TranscribingSystemType,
} from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { StrReferential } from '@app/referential/model/referential.model';
import { uniqueValue, Utils } from '@app/shared/utils';
import { TranscribingItemTypeService } from '@app/referential/transcribing-item-type/transcribing-item-type.service';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { debounceTime, distinctUntilChanged, filter, mergeMap, pairwise, startWith } from 'rxjs/operators';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';
import { ErrorStateMatcher } from '@angular/material/core';
import { AbstractControl, FormGroupDirective, NgForm } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';
import { Alerts } from '@app/shared/alerts';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-referential-filter-form-field',
  templateUrl: './referential-filter-form-field.component.html',
  styleUrls: ['./referential-filter-form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule, ReferentialFilterHintComponent],
})
export class ReferentialFilterFormField extends BaseForm<IReferentialFilterCriteria<any>> implements InputElement, OnInit, ErrorStateMatcher {
  floatLabel = input<FloatLabelType>('auto');
  placeholderI18n = input<string>();
  readonly = input<boolean>(false); // fixme not used
  clearable = input<boolean>(true);
  entityName = input<string>();
  addCriteria = input<any>();
  listOnly = input<boolean>(false);
  showMode = input<boolean>(false);
  i18nPrefix = input<string>('REFERENTIAL.FILTER.');
  appAutofocus = input<boolean>(false);
  systemId = model<TranscribingSystemType>();
  showSystemSelect = input<boolean>(false);
  functionIdsMapBySystemId = input<TranscribingFunctionTypeBySystemType>(undefined);

  textSubmit = output<UIEvent>();

  matInput = viewChild('matInput', { read: ElementRef });
  hintRef = viewChild('hintRef', { read: ElementRef });
  innerHintRef = viewChild('innerHintRef', { read: ElementRef });
  hintComponent = viewChild<ReferentialFilterHintComponent>('hintComponent');
  transcribingSystemSelect = viewChild<MatSelect>('transcribingSystemSelect');

  protected transcribingSystemIds: TranscribingSystemType[];
  protected transcribingSystemsByIds: Record<TranscribingSystemType, StrReferential>;
  protected transcribingSystemSelectVisible = model(false);

  protected readonly transcribingItemTypeService = inject(TranscribingItemTypeService);
  protected readonly alertController = inject(AlertController);
  protected readonly translateService = inject(TranslateService);

  private _inputFocused = false;
  private triggerRenderHint = new EventEmitter<any>();
  private settingValue = false;

  get selectedSystemId(): TranscribingSystemType {
    return this.systemId() ?? this.value?.systemId;
  }

  get required(): boolean {
    return ReferentialFilterFormFieldValidator.hasRequiredValidator(this.form);
  }

  constructor(protected injector: Injector) {
    super(injector);
    this._enabled = true;
    this.logPrefix = '[referential-filter-form-field]';
  }

  isErrorState(control: AbstractControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return this.required && !this.hasValue;
  }

  get hasValue(): boolean {
    return isNotNilOrBlank(this.value?.searchText) || this.hasIds;
  }

  get hasIds(): boolean {
    return isNotEmptyArray(this.value?.includedIds) || isNotEmptyArray(this.value?.excludedIds);
  }

  get include(): boolean {
    return this.hasIds ? isNotEmptyArray(this.value?.includedIds) : true;
  }

  get inputFocused(): boolean {
    return this._inputFocused;
  }

  set inputFocused(value: boolean) {
    if (this.listOnly()) return;
    this._inputFocused = value;
    this.emitRenderHint();
  }

  get value(): IReferentialFilterCriteria<any> {
    return super.value;
  }

  get disabled(): any {
    return super.disabled || this.readonly();
  }

  @HostListener('blur')
  onBlur() {
    this.emitRenderHint();
  }

  @HostListener('window:resize', ['$event.target'])
  resize() {
    this.emitRenderHint();
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.form.controls.searchText.valueChanges
        .pipe(filter((searchText) => this.required && isNilOrBlank(searchText) && this.hasIds))
        .subscribe(() => {
          // Remove required validator and potential error
          this.form.controls.searchText.clearValidators();
          this.form.controls.searchText.setErrors(null);
        })
    );

    // Listen to systemId changes
    this.registerSubscription(
      this.form.controls.systemId.valueChanges
        .pipe(
          startWith(this.selectedSystemId),
          pairwise(),
          filter(() => !this.settingValue && this.enabled),
          filter(([previousSystemId, nextSystemId]) => previousSystemId !== nextSystemId),
          mergeMap(async ([previousSystemId, nextSystemId]) => {
            this.settingValue = true;
            // Ignore system change when previous and next are default
            if (TranscribingItemTypeUtils.isDefaultSystem(previousSystemId) && TranscribingItemTypeUtils.isDefaultSystem(nextSystemId)) {
              return [null, null];
            }
            // Ask confirmation by external check to allow system change
            const nextSystemIdValid = await this.confirmSystemIdChange(nextSystemId);
            if (nextSystemIdValid) {
              return [previousSystemId, nextSystemId];
            } else {
              return [previousSystemId, null];
            }
          })
        )
        .subscribe(([previousSystemId, nextSystemId]) => {
          if (isNotNil(nextSystemId)) {
            // Reset included and excluded ids
            this.form.patchValue({ includedIds: null, excludedIds: null });
          } else if (isNotNil(previousSystemId)) {
            // Restore previous value silently
            this.form.patchValue({ systemId: previousSystemId });
          }
          this.settingValue = false;
          this.emitRenderHint();
        })
    );

    // Render on form value changes
    this.registerSubscription(
      this.form.valueChanges
        .pipe(
          filter(() => !this.settingValue && this.enabled),
          distinctUntilChanged()
        )
        .subscribe(() => this.emitRenderHint())
    );

    // Reflect form control status to component
    this.registerSubscription(
      this.form.statusChanges.pipe(distinctUntilChanged()).subscribe((status) => {
        if (status === 'DISABLED') {
          this.disable();
        } else {
          this.enable();
        }
      })
    );

    // Render hint when signal emitted
    this.registerSubscription(this.triggerRenderHint.pipe(debounceTime(20)).subscribe(() => this.renderHint()));

    // Emit render signal on property change
    this.transcribingSystemSelectVisible.subscribe(() => this.emitRenderHint());
    this.systemId.subscribe(() => this.emitRenderHint());

    // Init transcribing types
    this.loadTranscribingSystems(this.entityName()).then(() => this._updateView());
  }

  async openModal(event: UIEvent) {
    event?.preventDefault();

    const modalComponent = !this.hasIds ? EntityAddModal : EntitySelectModal;
    const selectedIds = ((this.include ? this.value?.includedIds : this.value?.excludedIds) || []).map((id) => id.toString());
    const commonCriteria: ISelectCriteria = {
      systemId: this.selectedSystemId,
      functionIds: this.functionIdsMapBySystemId()?.[this.selectedSystemId],
    };
    const selectCriteria: ISelectCriteria = { includedIds: selectedIds, ...commonCriteria };
    const addCriteria: ISelectCriteria = { ...this.addCriteria(), excludedIds: selectedIds, ...commonCriteria };
    let entityName = this.entityName();
    if (!TranscribingItemTypeUtils.isDefaultSystem(this.selectedSystemId)) {
      entityName = TranscribingItemRow.entityName;
      selectCriteria.referentialEntityName = this.entityName();
      addCriteria.referentialEntityName = this.entityName();
    }

    // Open modal
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, ISelectResult & IAddResult>(
      modalComponent,
      {
        entityName,
        selectCriteria,
        addCriteria,
        titleI18n: this.placeholderI18n(),
        titlePrefixI18n: `${this.i18nPrefix()}TITLE`,
        titleAddPrefixI18n: 'COMMON.ADD',
        canEdit: !this.disabled,
        showMode: this.showMode(),
        mode: this.include ? 'include' : 'exclude',
      },
      entitiesTableNeedingMoreWidth.includes(entityName) ? 'modal-large' : 'modal-medium'
    );

    // Apply new value
    if (!this.disabled && role === 'validate' && data) {
      const ids = data.selectedIds || data.addedIds || undefined;
      if (data.mode === 'exclude') {
        this.form.patchValue({ excludedIds: ids, includedIds: null });
      } else {
        this.form.patchValue({ includedIds: ids, excludedIds: null });
      }
      this.markAsDirty();
      this.emitRenderHint();
    }
  }

  focus() {
    focusInput(this.matInput());
  }

  setValue(data: IReferentialFilterCriteria<any>, opts?: { emitEvent?: boolean; onlySelf?: boolean }): void {
    this.settingValue = true;
    super.setValue(data, opts);
    this.settingValue = false;
    this._updateView();
  }

  clearValue(event: UIEvent) {
    this.settingValue = true;
    this.setValue({ systemId: 'QUADRIGE' }, { emitEvent: true, onlySelf: false }); // Don't use reset()
    this.settingValue = false;
    this.markAsDirty();
    this._updateView(true);
    event.stopPropagation();
  }

  updateView(): void {
    this._updateView(false);
  }

  resetView(): void {
    this._updateView(true);
  }

  /* -- protected method -- */

  protected showAndOpenTranscribingSystemSelect() {
    // Set default system if none
    if (!this.selectedSystemId) {
      this.form.patchValue({ systemId: 'QUADRIGE' });
    }
    this.transcribingSystemSelectVisible.set(true);
    this.detectChanges();
    this.transcribingSystemSelect().open();
  }

  protected _updateView(reset?: boolean) {
    if (isNotEmptyArray(this.transcribingSystemIds)) {
      this.transcribingSystemSelectVisible.update((value) => (!reset && value) || !TranscribingItemTypeUtils.isDefaultSystem(this.value.systemId));
    }
    this.emitRenderHint();
  }

  protected async confirmSystemIdChange(nextSystemId: TranscribingSystemType): Promise<boolean> {
    if (!this.hasIds) return true;
    return await Alerts.askConfirmation('REFERENTIAL.FILTER.CONFIRM.CHANGE_SYSTEM', this.alertController, this.translateService);
  }

  private emitRenderHint() {
    this.triggerRenderHint.emit();
  }

  private renderHint() {
    this.hintComponent()?.render(this.value, {
      font: window.getComputedStyle(this.inputFocused ? this.hintRef().nativeElement : this.innerHintRef().nativeElement).getPropertyValue('font'),
      maxWidth: (this.inputFocused ? this.hintRef() : this.innerHintRef())?.nativeElement?.parentElement?.offsetWidth,
      systemId: this.selectedSystemId,
    });
  }

  private async loadTranscribingSystems(entityName: string) {
    const transcribingSystemIds = [<TranscribingSystemType>'QUADRIGE']
      .concat(
        (await this.transcribingItemTypeService.loadTranscribingItemTypesByEntityName(entityName, false))
          .filter((type) => transcribingSystemsForFilter.includes(type.systemId))
          .map((type) => type.systemId)
      )
      .filter(uniqueValue);
    const systems = await this.transcribingItemTypeService.getSystems();
    this.transcribingSystemsByIds = Utils.toRecord(
      systems.filter((system) => transcribingSystemIds.includes(<TranscribingSystemType>system.id)),
      'id'
    );
    // Affect this array at last, because of template update
    this.transcribingSystemIds = transcribingSystemIds;
  }
}
