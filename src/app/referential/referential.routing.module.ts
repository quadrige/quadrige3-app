import { Routes } from '@angular/router';
import { ReferentialHomePage } from '@app/referential/menu/referential-home.page';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { inject } from '@angular/core';
import { ManagementHomePage } from '@app/referential/menu/management-home.page';

export const referentialRoutes: Routes = [
  {
    path: '',
    component: ReferentialHomePage,
    children: [
      {
        path: 'Pmfmu',
        loadChildren: () => import('./pmfmu/pmfmu.routing.module').then((m) => m.PmfmuRoutingModule),
      },
      {
        path: 'MonitoringLocation',
        loadChildren: () => import('./monitoring-location/monitoring-location.routing.module').then((m) => m.MonitoringLocationRoutingModule),
      },
      {
        path: 'OrderItemType',
        loadChildren: () => import('./order-item-type/order-item-type.routing.module').then((m) => m.OrderItemTypeRoutingModule),
      },
      {
        path: 'OrderItem',
        loadChildren: () => import('./order-item/order-item.routing.module').then((m) => m.OrderItemRoutingModule),
      },
      {
        path: 'ParameterGroup',
        loadChildren: () => import('./parameter-group/parameter-group.routing.module').then((m) => m.ParameterGroupRoutingModule),
      },
      {
        path: 'Parameter',
        loadChildren: () => import('./parameter/parameter.routing.module').then((m) => m.ParameterRoutingModule),
      },
      {
        path: 'Matrix',
        loadChildren: () => import('./matrix/matrix.routing.module').then((m) => m.MatrixRoutingModule),
      },
      {
        path: 'Fraction',
        loadChildren: () => import('./fraction/fraction.routing.module').then((m) => m.FractionRoutingModule),
      },
      {
        path: 'Method',
        loadChildren: () => import('./method/method.routing.module').then((m) => m.MethodRoutingModule),
      },
      {
        path: 'Unit',
        loadChildren: () => import('./unit/unit.routing.module').then((m) => m.UnitRoutingModule),
      },
      {
        path: 'SamplingEquipment',
        loadChildren: () => import('./sampling-equipment/sampling-equipment.routing.module').then((m) => m.SamplingEquipmentRoutingModule),
      },
      {
        path: 'PositioningSystem',
        loadChildren: () => import('./positioning-system/positioning-system.routing.module').then((m) => m.PositioningSystemRoutingModule),
      },
      {
        path: 'DredgingAreaType',
        loadChildren: () => import('./dredging-area-type/dredging-area-type.routing.module').then((m) => m.DredgingAreaTypeRoutingModule),
      },
      {
        path: 'DredgingTargetArea',
        loadChildren: () => import('./dredging-target-area/dredging-target-area.routing.module').then((m) => m.DredgingTargetAreaRoutingModule),
      },
      {
        path: 'Department',
        loadChildren: () => import('./department/department.routing.module').then((m) => m.DepartmentRoutingModule),
      },
      {
        path: 'User',
        loadChildren: () => import('./user/user.routing.module').then((m) => m.UserRoutingModule),
      },
      {
        path: 'Privilege',
        loadChildren: () => import('./privilege/privilege.routing.module').then((m) => m.PrivilegeRoutingModule),
      },
      {
        path: 'Training',
        loadChildren: () => import('./training/training.routing.module').then((m) => m.TrainingRoutingModule),
      },
      {
        path: 'TaxonGroup',
        loadChildren: () => import('./taxon-group/taxon-group.routing.module').then((m) => m.TaxonGroupRoutingModule),
      },
      {
        path: 'TaxonName',
        loadChildren: () => import('./taxon-name/taxon-name.routing.module').then((m) => m.TaxonNameRoutingModule),
      },
      {
        path: 'Campaign',
        loadChildren: () => import('./campaign/campaign.routing.module').then((m) => m.CampaignRoutingModule),
      },
      {
        path: 'Event',
        loadChildren: () => import('./event/event.routing.module').then((m) => m.EventRoutingModule),
      },
      {
        path: 'Batch',
        loadChildren: () => import('./batch/batch-ref.routing.module').then((m) => m.BatchRefRoutingModule),
      },
      {
        path: ':entity',
        loadChildren: () => import('./generic/generic.routing.module').then((m) => m.GenericRoutingModule),
        canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
      },
    ],
  },
];

export const managementRoutes: Routes = [
  {
    path: '',
    component: ManagementHomePage,
    children: [
      {
        path: 'Program',
        loadChildren: () => import('./program-strategy/program-strategy.routing.module').then((m) => m.ProgramStrategyRoutingModule),
      },
      {
        path: 'MetaProgram',
        loadChildren: () => import('./meta-program/meta-program.routing.module').then((m) => m.MetaProgramRoutingModule),
      },
      {
        path: 'RuleList',
        loadChildren: () => import('./rule-list/rule-list.routing.module').then((m) => m.RuleListRoutingModule),
      },
      {
        path: 'Moratorium',
        loadChildren: () => import('./moratorium/moratorium.routing.module').then((m) => m.MoratoriumRoutingModule),
      },
    ],
  },
];
