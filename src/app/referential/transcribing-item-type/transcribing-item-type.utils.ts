import { isNil } from '@sumaris-net/ngx-components';
import { EntityType } from '@app/referential/model/referential.model';
import { ReferentialFilterUtils } from '@app/referential/model/referential.filter.utils';
import { attributes } from '@app/referential/model/referential.constants';
import { ITranscribingItemType, TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

export class TranscribingItemTypeUtils {
  public static getChildrenTranscribingTypeIds(transcribingItemTypes: ITranscribingItemType[], parentTypeId: number): number[] {
    return transcribingItemTypes.filter((type) => type.parentId === parentTypeId).map((type) => type.id);
  }

  public static isDefaultSystem(systemId: TranscribingSystemType): boolean {
    return isNil(systemId) || systemId === 'QUADRIGE';
  }

  public static defaultSearchAttributesForSystem(type: EntityType, systemId: TranscribingSystemType): string[] {
    if (TranscribingItemTypeUtils.isDefaultSystem(systemId)) {
      return ReferentialFilterUtils.defaultSearchAttributes(type);
    }
    switch (systemId) {
      case 'SANDRE':
        return attributes.idName;
      case 'CAS':
      case 'TAXREF':
      case 'WORMS':
        return attributes.id;
    }
  }
}
