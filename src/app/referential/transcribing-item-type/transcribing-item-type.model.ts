import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { IReferentialRef } from '@sumaris-net/ngx-components/src/app/core/services/model/referential.model';
import { ISimpleType, PartialRecord } from '@app/shared/model/interface';
import { Utils } from '@app/shared/utils';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';

export type TranscribingItemView = 'transcribingItemTable';

export type TranscribingSideType = 'IN' | 'OUT' | 'IN_OUT';
export type ITranscribingSideType = ISimpleType<TranscribingSideType>;
export const allTranscribingSides: ITranscribingSideType[] = Utils.buildSimpleTypeArray(['IN', 'OUT', 'IN_OUT'], 'REFERENTIAL.TRANSCRIBING_SIDE');

export type TranscribingSystemType = 'QUADRIGE' | 'DALI' | 'REEFDB' | 'TAXREF' | 'WORMS' | 'PAMPA' | 'SANDRE' | 'CAS';
export const transcribingSystemsForFilter: TranscribingSystemType[] = ['SANDRE', 'CAS', 'TAXREF', 'WORMS'];

export type TranscribingFunctionType = 'SCREEN' | 'EXTRACT' | 'IMPORT' | 'EXPORT';
export const transcribingFunctionsForTranscribing: TranscribingFunctionType[] = ['IMPORT', 'EXPORT'];
export const transcribingFunctionsForTranslations: TranscribingFunctionType[] = ['SCREEN', 'EXTRACT'];

export type TranscribingFunctionTypeBySystemType = PartialRecord<TranscribingSystemType, TranscribingFunctionType[]>;
export const transcribingFunctionsForExtraction: TranscribingFunctionTypeBySystemType = { SANDRE: ['EXPORT'] };

export type TranscribingLanguageType = 'FR' | 'GB' | 'ES';
export type ITranscribingLanguageType = ISimpleType<TranscribingLanguageType>;
export const allTranscribingLanguages: ITranscribingLanguageType[] = Utils.buildSimpleTypeArray(
  ['FR', 'GB', 'ES'],
  'REFERENTIAL.TRANSCRIBING_LANGUAGE'
);

export const pmfmuSandreAttributes = ['PAR_CD', 'MATRIX_ID', 'FRACTION_ID', 'METHOD_ID', 'UNIT_ID'];

export interface ITranscribingItemTableOptions {
  systemId?: TranscribingSystemType;
  functionIds?: TranscribingFunctionType[];
  referentialEntityName?: string;
  forceEntityName?: boolean;
}

export interface ITranscribingItemType extends IReferentialRef<ITranscribingItemType> {
  mandatory: boolean;
  hidden: boolean;
  targetEntityName: string;
  targetAttribute: string;
  parentId: number;
  sideId: TranscribingSideType;
  systemId: TranscribingSystemType;
  functionId: TranscribingFunctionType;
  languageId: TranscribingLanguageType;
  filterQuery: string;
}

@EntityClass({ typename: 'TranscribingItemTypeVO' })
export class TranscribingItemType extends Referential<TranscribingItemType> implements ITranscribingItemType {
  static entityName = 'TranscribingItemType';
  static fromObject: (source: any, opts?: any) => TranscribingItemType;

  mandatory: boolean = null;
  hidden: boolean = null;
  targetEntityName: string = null;
  targetAttribute: string = null;
  sideId: TranscribingSideType = null;
  systemId: TranscribingSystemType = null;
  functionId: TranscribingFunctionType = null;
  languageId: TranscribingLanguageType = null;
  filterQuery: string = null;

  constructor() {
    super(TranscribingItemType.TYPENAME);
    this.entityName = TranscribingItemType.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = TranscribingItemType.entityName;
    this.mandatory = source.mandatory;
    this.hidden = source.hidden;
    this.targetEntityName = source.targetEntityName;
    this.targetAttribute = source.targetAttribute;
    this.sideId = source.sideId;
    this.systemId = source.systemId;
    this.functionId = source.functionId;
    this.languageId = source.languageId;
    this.filterQuery = source.filterQuery;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parentId = this.parentId; // Restore parentId deleted by super.asObject
    return target;
  }
}

@EntityClass({ typename: 'TranscribingItemTypeFilterCriteriaVO' })
export class TranscribingItemTypeFilterCriteria extends ReferentialFilterCriteria<TranscribingItemType, number> {
  static fromObject: (source: any, opts?: any) => TranscribingItemTypeFilterCriteria;

  targetEntityName: string = null;

  constructor() {
    super(TranscribingItemTypeFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.targetEntityName = source.targetEntityName;
  }
}

@EntityClass({ typename: 'TranscribingItemTypeFilterVO' })
export class TranscribingItemTypeFilter extends ReferentialFilter<
  TranscribingItemTypeFilter,
  TranscribingItemTypeFilterCriteria,
  TranscribingItemType
> {
  static fromObject: (source: any, opts?: any) => TranscribingItemTypeFilter;

  constructor() {
    super(TranscribingItemTypeFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): TranscribingItemTypeFilterCriteria {
    return TranscribingItemTypeFilterCriteria.fromObject(source, opts);
  }
}
