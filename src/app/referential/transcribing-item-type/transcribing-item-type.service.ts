import { inject, Injectable, Injector } from '@angular/core';
import { isNil, isNilOrBlank, isNotEmptyArray, StatusIds, toBoolean } from '@sumaris-net/ngx-components';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import {
  ITranscribingItemTableOptions,
  transcribingFunctionsForTranscribing,
  transcribingFunctionsForTranslations,
  TranscribingFunctionType,
  TranscribingItemType,
  TranscribingItemTypeFilter,
  TranscribingItemTypeFilterCriteria,
  TranscribingItemView,
  TranscribingSystemType,
} from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { TranscribingItem, transcribingItemFragment } from '@app/referential/transcribing-item/transcribing-item.model';
import { GenericService } from '@app/referential/generic/generic.service';
import { IWithTranscribingItems, StrReferential } from '@app/referential/model/referential.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { uniqueValue } from '@app/shared/utils';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { EntityUtils } from '@app/shared/entity.utils';
import { EntityServiceLoadOptions } from '@sumaris-net/ngx-components/src/app/shared/services/entity-service.class';
import { TranscribingItemRow, transcribingItemRowFragment } from '@app/referential/transcribing-item/transcribing-item-row.model';

export const transcribingItemTypeFragment = gql`
  fragment TranscribingItemTypeFragment on TranscribingItemTypeVO {
    id
    name
    label
    description
    comments
    mandatory
    hidden
    targetEntityName
    targetAttribute
    sideId
    systemId
    functionId
    languageId
    filterQuery
    updateDate
    creationDate
    statusId
    parentId
    __typename
  }
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query TranscribingItemTypes($page: PageInput, $filter: TranscribingItemTypeFilterVOInput) {
      data: transcribingItemTypes(page: $page, filter: $filter) {
        ...TranscribingItemTypeFragment
      }
    }
    ${transcribingItemTypeFragment}
  `,

  loadAllWithTotal: gql`
    query TranscribingItemTypesWithTotal($page: PageInput, $filter: TranscribingItemTypeFilterVOInput) {
      data: transcribingItemTypes(page: $page, filter: $filter) {
        ...TranscribingItemTypeFragment
      }
      total: transcribingItemTypesCount(filter: $filter)
    }
    ${transcribingItemTypeFragment}
  `,

  countAll: gql`
    query TranscribingItemTypesCount($filter: TranscribingItemTypeFilterVOInput) {
      total: transcribingItemTypesCount(filter: $filter)
    }
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveTranscribingItemTypes($data: [TranscribingItemTypeVOInput]) {
      data: saveTranscribingItemTypes(transcribingItemTypes: $data) {
        ...TranscribingItemTypeFragment
      }
    }
    ${transcribingItemTypeFragment}
  `,

  deleteAll: gql`
    mutation DeleteTranscribingItemTypes($ids: [Int]) {
      deleteTranscribingItemTypes(ids: $ids)
    }
  `,
};

const otherQueries = {
  loadTranscribingItemTypes: gql`
    query TranscribingItemTypesByEntityName($entityName: String, $forceEntityName: Boolean!) {
      data: transcribingItemTypesByEntityName(entityName: $entityName, forceEntityName: $forceEntityName) {
        ...TranscribingItemTypeFragment
      }
    }
    ${transcribingItemTypeFragment}
  `,

  loadTranscribingItems: gql`
    query TranscribingItems($entityName: String, $entityId: String) {
      data: transcribingItems(entityName: $entityName, entityId: $entityId) {
        ...TranscribingItemFragment
      }
    }
    ${transcribingItemFragment}
  `,

  loadTranscribingItemRows: gql`
    query TranscribingItemRows(
      $entityName: String
      $includedExternalCodes: [String]
      $excludedExternalCodes: [String]
      $systemId: TranscribingSystemEnum
      $functionIds: [TranscribingFunctionEnum]
    ) {
      data: transcribingItemRows(
        entityName: $entityName
        includedExternalCodes: $includedExternalCodes
        excludedExternalCodes: $excludedExternalCodes
        systemId: $systemId
        functionIds: $functionIds
      ) {
        ...TranscribingItemRowFragment
      }
    }
    ${transcribingItemRowFragment}
  `,
};

export interface ILoadTranscribingItemOptions extends EntityServiceLoadOptions {
  entityName: string;
  entityId: string;
}

export interface ILoadTranscribingItemRowOptions extends EntityServiceLoadOptions {
  entityName: string;
  systemId: TranscribingSystemType;
  functionIds?: TranscribingFunctionType[];
  includedExternalCodes?: string[];
  excludedExternalCodes?: string[];
}

@Injectable({ providedIn: 'root' })
export class TranscribingItemTypeService extends BaseReferentialService<
  TranscribingItemType,
  TranscribingItemTypeFilter,
  TranscribingItemTypeFilterCriteria
> {
  protected readonly genericService = inject(GenericService);

  protected systems: StrReferential[];
  protected functions: StrReferential[];
  protected languages: StrReferential[];
  protected codificationTypes: StrReferential[];

  constructor(protected injector: Injector) {
    super(injector, TranscribingItemType, TranscribingItemTypeFilter, { queries, mutations });
    this._logPrefix = '[transcribing-item-type-service]';
    this.start();
  }

  protected async ngOnStart(): Promise<void> {
    // Load sub referential
    this.systems = (
      await this.genericService.loadPage(
        {}, // no page
        GenericReferentialFilter.fromObject({
          entityName: 'TranscribingSystem',
        })
      )
    ).data;
    this.functions = (
      await this.genericService.loadPage(
        {}, // no page
        GenericReferentialFilter.fromObject({
          entityName: 'TranscribingFunction',
        })
      )
    ).data;
    this.languages = (
      await this.genericService.loadPage(
        {}, // no page
        GenericReferentialFilter.fromObject({
          entityName: 'TranscribingLanguage',
        })
      )
    ).data;
    this.codificationTypes = (
      await this.genericService.loadPage(
        {}, // no page
        GenericReferentialFilter.fromObject({
          entityName: 'TranscribingCodificationType',
        })
      )
    ).data;

    return super.ngOnStart();
  }

  asFilter(source: any): TranscribingItemTypeFilter {
    const filter = super.asFilter(source);
    // Get active transcribing types
    filter.patch({ statusId: StatusIds.ENABLE });
    return filter;
  }

  async initMenus(entityName: string): Promise<IEntityMenuItem<IWithTranscribingItems, TranscribingItemView>[]> {
    await this.ready();
    const menus: IEntityMenuItem<IWithTranscribingItems, TranscribingItemView>[] = [];
    const transcribingItemTypes = await this.loadTranscribingItemTypesByEntityName(entityName);
    if (isNotEmptyArray(transcribingItemTypes)) {
      // Build transcribing menu
      const transcribingMenuItem: IEntityMenuItem<IWithTranscribingItems, TranscribingItemView> = {
        title: 'REFERENTIAL.TRANSCRIBING_ITEMS.TITLE',
        children: this.initMenuChildren(transcribingItemTypes, transcribingFunctionsForTranscribing, 'REFERENTIAL.TRANSCRIBING_ITEMS.VIEW'),
      };
      if (isNotEmptyArray(transcribingMenuItem.children)) {
        menus.push(transcribingMenuItem);
      }

      // Build translation menu
      const translationMenuItem: IEntityMenuItem<IWithTranscribingItems, TranscribingItemView> = {
        title: 'REFERENTIAL.TRANSLATION_ITEMS.TITLE',
        children: this.initMenuChildren(transcribingItemTypes, transcribingFunctionsForTranslations, 'REFERENTIAL.TRANSLATION_ITEMS.VIEW'),
      };
      if (isNotEmptyArray(translationMenuItem.children)) {
        menus.push(translationMenuItem);
      }
    }
    return menus;
  }

  protected initMenuChildren(
    transcribingItemTypes: TranscribingItemType[],
    functionTypes: TranscribingFunctionType[],
    titleI18n: string
  ): IEntityMenuItem<IWithTranscribingItems, TranscribingItemView>[] {
    const children: IEntityMenuItem<IWithTranscribingItems, TranscribingItemView>[] = [];
    const types = transcribingItemTypes.filter((type) => functionTypes.includes(type.functionId));
    // Get unique systems
    const systems = types
      .map((type) => this.systems.find((system) => system.id === type.systemId))
      .filter(uniqueValue)
      .sort(EntityUtils.entitySortComparator('id'));
    // Create menu items for each system
    for (const system of systems) {
      children.push({
        title: system.name.toUpperCase(),
        params: <ITranscribingItemTableOptions>{
          systemId: system.id,
          functionIds: functionTypes,
        },
        attribute: 'transcribingItems',
        view: 'transcribingItemTable',
        viewTitle: titleI18n,
        viewTitleArgs: {
          systemName: system.name.toUpperCase(),
        },
      });
    }
    return children;
  }

  async getSystems(): Promise<StrReferential[]> {
    await this.ready();
    return this.systems;
  }

  async getFunctions(): Promise<StrReferential[]> {
    await this.ready();
    return this.functions;
  }

  async getLanguages(): Promise<StrReferential[]> {
    await this.ready();
    return this.languages;
  }

  async getCodificationTypes(): Promise<StrReferential[]> {
    await this.ready();
    return this.codificationTypes;
  }

  async loadTranscribingItemTypesByEntityName(entityName: string, forceEntityName?: boolean): Promise<TranscribingItemType[]> {
    if (isNilOrBlank(entityName)) {
      return [];
    }
    const variables: any = { entityName, forceEntityName: toBoolean(forceEntityName, true) };
    if (this._debug) console.debug(`${this._logPrefix} Loading transcribing item types...`, variables);
    const now = Date.now();
    const query = otherQueries.loadTranscribingItemTypes;
    const res = await this.graphql.query<{ data: any[] }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
    });
    const data = (res?.data || []).map(TranscribingItemType.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} transcribing item types loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadTranscribingItemsTree(opts: ILoadTranscribingItemOptions): Promise<TranscribingItem[]> {
    if (isNilOrBlank(opts.entityName) || isNilOrBlank(opts.entityId))
      throw new Error(`${this._logPrefix} Cannot load transcribing items, no entity name or id`);
    // Call service if transcribing items are not loaded yet
    const flatTranscribingItems = await this.loadTranscribingItemsByEntityName(opts);
    // To hierarchical tree
    const now = Date.now();
    const transcribingItems = flatTranscribingItems.filter((item) => !item.parentId);
    transcribingItems.forEach((parentItem) => (parentItem.children = flatTranscribingItems.filter((item) => item.parentId === parentItem.id)));
    if (this._debug) console.debug(`${this._logPrefix} transcribing items converted to tree in ${Date.now() - now}ms`);
    return transcribingItems;
  }

  protected async loadTranscribingItemsByEntityName(opts: ILoadTranscribingItemOptions): Promise<TranscribingItem[]> {
    if (this._debug) console.debug(`${this._logPrefix} Loading transcribing items...`, opts);
    const now = Date.now();
    const query = otherQueries.loadTranscribingItems;
    const res = await this.graphql.query<{ data: any[] }>({
      query,
      variables: opts,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: opts?.fetchPolicy ?? 'network-only',
    });
    const data = (res?.data || []).map(TranscribingItem.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} transcribing items loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  public async loadTranscribingItemRows(opts: ILoadTranscribingItemRowOptions): Promise<TranscribingItemRow[]> {
    if (isNil(opts.entityName) || isNil(opts.systemId)) {
      return [];
    }
    if (this._debug) console.debug(`${this._logPrefix} Loading transcribing item rows...`, opts);
    const now = Date.now();
    const query = otherQueries.loadTranscribingItemRows;
    const res = await this.graphql.query<{ data: any[] }>({
      query,
      variables: opts,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: opts?.fetchPolicy ?? 'network-only',
    });
    const data = (res?.data || []).map(TranscribingItemRow.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} transcribing item rows loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
