import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { ISelectTable } from '@app/shared/table/table.model';
import { TaxonGroupService } from '@app/referential/taxon-group/taxon-group.service';
import { TaxonGroupTable } from '@app/referential/taxon-group/taxon-group.table';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { TaxonGroupFilter } from '@app/referential/taxon-group/filter/taxon-group.filter.model';
import { TaxonGroup } from '@app/referential/taxon-group/taxon-group.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { TaxonGroupFilterForm } from '@app/referential/taxon-group/filter/taxon-group.filter.form';

@Component({
  selector: 'app-taxon-group-select-table',
  templateUrl: './taxon-group.table.html',
  standalone: true,
  imports: [ReferentialModule, TaxonGroupFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonGroupSelectTable extends TaxonGroupTable implements ISelectTable<TaxonGroup> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria;
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();

  constructor(
    protected injector: Injector,
    protected _entityService: TaxonGroupService
  ) {
    super(injector, _entityService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[taxon-group-select-table]';
    this.selectTable = true;
  }

  async resetFilter(filter?: TaxonGroupFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit();
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete).map((id) => id.toString());
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    await this.resetFilter();
  }

  updatePermission() {
    this.tableButtons.canAdd = true;
    this.tableButtons.canDelete = true;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
