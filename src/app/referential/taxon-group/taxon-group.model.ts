import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential, StrReferential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'TaxonGroupVO' })
export class TaxonGroup extends Referential<TaxonGroup> {
  static entityName = 'TaxonGroup';
  static fromObject: (source: any, opts?: any) => TaxonGroup;

  type: StrReferential;
  parent: IntReferential;
  exclusive: boolean;
  update: boolean;

  constructor() {
    super(TaxonGroup.TYPENAME);
    this.entityName = TaxonGroup.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = TaxonGroup.entityName;
    this.type = StrReferential.fromObject(source.type);
    this.parent = IntReferential.fromObject(source.parent);
    this.exclusive = source.exclusive;
    this.update = source.update;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.type = this.type.asObject(opts);
    target.parent = this.parent?.asObject(opts);
    return target;
  }
}
