import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { TaxonGroup } from '@app/referential/taxon-group/taxon-group.model';
import { TaxonGroupService } from '@app/referential/taxon-group/taxon-group.service';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { TaxonGroupFilter, TaxonGroupFilterCriteria } from '@app/referential/taxon-group/filter/taxon-group.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-taxon-group-table',
  templateUrl: './taxon-group.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonGroupTable extends ReferentialTable<TaxonGroup, number, TaxonGroupFilter, TaxonGroupFilterCriteria> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: TaxonGroupService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['label', 'name', /*'comments',*/ 'statusId' /*, 'creationDate', 'updateDate'*/]).concat(RESERVED_END_COLUMNS),
      TaxonGroup,
      _entityService,
      undefined
    );

    this.i18nColumnPrefix = 'REFERENTIAL.TAXON_GROUP.';
    this.defaultSortBy = 'label';
    this.logPrefix = '[taxon-group-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    // Type combo
    this.registerAutocompleteField('type', {
      ...this.referentialOptions.taxonGroupType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'TaxonGroupType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.TAXON_GROUPS' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name');
  }
}
