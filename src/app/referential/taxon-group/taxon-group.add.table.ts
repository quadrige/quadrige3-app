import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { TaxonGroupService } from '@app/referential/taxon-group/taxon-group.service';
import { TaxonGroupTable } from '@app/referential/taxon-group/taxon-group.table';
import { TaxonGroupFilter } from '@app/referential/taxon-group/filter/taxon-group.filter.model';
import { TaxonGroup } from '@app/referential/taxon-group/taxon-group.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { TaxonGroupFilterForm } from '@app/referential/taxon-group/filter/taxon-group.filter.form';

@Component({
  selector: 'app-taxon-group-add-table',
  templateUrl: './taxon-group.table.html',
  standalone: true,
  imports: [ReferentialModule, TaxonGroupFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonGroupAddTable extends TaxonGroupTable implements IAddTable<TaxonGroup> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: TaxonGroupService
  ) {
    super(injector, _entityService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[taxon-group-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: TaxonGroupFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
