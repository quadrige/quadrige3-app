import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import { TaxonGroup } from '@app/referential/taxon-group/taxon-group.model';
import { Injectable, Injector } from '@angular/core';
import { TaxonGroupFilter, TaxonGroupFilterCriteria } from '@app/referential/taxon-group/filter/taxon-group.filter.model';

export const lightTaxonGroupFragment = gql`
  fragment LightTaxonGroupFragment on TaxonGroupVO {
    id
    updateDate
    creationDate
    statusId
    label
    name
    comments
    exclusive
    update
    __typename
  }
`;

export const taxonGroupFragment = gql`
  fragment TaxonGroupFragment on TaxonGroupVO {
    id
    updateDate
    creationDate
    statusId
    label
    name
    comments
    type {
      ...ReferentialFragment
    }
    parent {
      ...LightTaxonGroupFragment
    }
    exclusive
    update
    __typename
  }
  ${referentialFragments.light}
  ${lightTaxonGroupFragment}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query TaxonGroups($page: PageInput, $filter: TaxonGroupFilterVOInput) {
      data: taxonGroups(page: $page, filter: $filter) {
        ...LightTaxonGroupFragment
      }
    }
    ${lightTaxonGroupFragment}
  `,
  countAll: gql`
    query TaxonGroupsCount($filter: TaxonGroupFilterVOInput) {
      total: taxonGroupsCount(filter: $filter)
    }
  `,
  loadAllWithTotal: gql`
    query TaxonGroupsWithTotal($page: PageInput, $filter: TaxonGroupFilterVOInput) {
      data: taxonGroups(page: $page, filter: $filter) {
        ...LightTaxonGroupFragment
      }
      total: taxonGroupsCount(filter: $filter)
    }
    ${lightTaxonGroupFragment}
  `,
};

@Injectable({ providedIn: 'root' })
export class TaxonGroupService extends BaseReferentialService<TaxonGroup, TaxonGroupFilter, TaxonGroupFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, TaxonGroup, TaxonGroupFilter, { queries, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[taxon-group-service]';
  }
}
