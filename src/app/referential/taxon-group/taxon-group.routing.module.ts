import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { TaxonGroupTable } from '@app/referential/taxon-group/taxon-group.table';
import { TaxonGroupModule } from '@app/referential/taxon-group/taxon-group.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: TaxonGroupTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), TaxonGroupModule],
})
export class TaxonGroupRoutingModule {}
