import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { TaxonGroupFilter, TaxonGroupFilterCriteria } from '@app/referential/taxon-group/filter/taxon-group.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-taxon-group-filter-form',
  templateUrl: './taxon-group.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonGroupFilterForm extends ReferentialCriteriaFormComponent<TaxonGroupFilter, TaxonGroupFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabelName;
  }
}
