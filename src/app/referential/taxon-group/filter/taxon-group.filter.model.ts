import { EntityClass } from '@sumaris-net/ngx-components';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { TaxonGroup } from '@app/referential/taxon-group/taxon-group.model';

@EntityClass({ typename: 'TaxonGroupFilterCriteriaVO' })
export class TaxonGroupFilterCriteria extends ReferentialFilterCriteria<TaxonGroup, number> {
  static fromObject: (source: any, opts?: any) => TaxonGroupFilterCriteria;

  constructor() {
    super(TaxonGroupFilterCriteria.TYPENAME);
  }
}

@EntityClass({ typename: 'TaxonGroupFilterVO' })
export class TaxonGroupFilter extends ReferentialFilter<TaxonGroupFilter, TaxonGroupFilterCriteria, TaxonGroup> {
  static fromObject: (source: any, opts?: any) => TaxonGroupFilter;

  constructor() {
    super(TaxonGroupFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): TaxonGroupFilterCriteria {
    return TaxonGroupFilterCriteria.fromObject(source, opts);
  }
}
