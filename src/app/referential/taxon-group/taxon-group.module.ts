import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { TaxonGroupTable } from '@app/referential/taxon-group/taxon-group.table';
import { TaxonGroupFilterForm } from '@app/referential/taxon-group/filter/taxon-group.filter.form';

@NgModule({
  imports: [ReferentialModule, TaxonGroupFilterForm],
  declarations: [TaxonGroupTable],
  exports: [TaxonGroupTable],
})
export class TaxonGroupModule {}
