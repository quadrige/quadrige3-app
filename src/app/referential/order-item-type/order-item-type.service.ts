import { Injectable, Injector } from '@angular/core';
import {
  BaseReferentialService,
  defaultReferentialSaveOption,
  IImportShapefileService,
  ImportShapefileServiceOptions,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
  ReferentialSaveOptions,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { OrderItemType } from '@app/referential/order-item-type/order-item-type.model';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import { Job } from '@app/social/job/job.model';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { OrderItemService } from '@app/referential/order-item/order-item.service';
import { orderItemTypeFragments } from '@app/referential/order-item-type/order-item-type.fragment';
import { jobFragments } from '@app/social/job/job.service';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { arraySize } from '@sumaris-net/ngx-components';

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query OrderItemTypes($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: orderItemTypes(page: $page, filter: $filter) {
        ...OrderItemTypeFragment
      }
    }
    ${orderItemTypeFragments.orderItemType}
  `,

  loadAllWithTotal: gql`
    query OrderItemTypesWithTotal($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: orderItemTypes(page: $page, filter: $filter) {
        ...OrderItemTypeFragment
      }
      total: orderItemTypesCount(filter: $filter)
    }
    ${orderItemTypeFragments.orderItemType}
  `,

  countAll: gql`
    query OrderItemTypesCount($filter: StrReferentialFilterVOInput) {
      total: orderItemTypesCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportOrderItemsAsync($filter: StrReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportOrderItemsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveOrderItemTypes($data: [OrderItemTypeVOInput], $options: ReferentialSaveOptionsInput) {
      data: saveOrderItemTypes(orderItemTypes: $data, options: $options) {
        ...FullOrderItemTypeFragment
      }
    }
    ${orderItemTypeFragments.fullOrderItemType}
  `,

  deleteAll: gql`
    mutation DeleteOrderItemTypes($ids: [String]) {
      deleteOrderItemTypes(ids: $ids)
    }
  `,
};

const otherQueries = {
  importShapefileAsync: gql`
    query ImportOrderItemTypeAsync($orderItemTypeId: String!, $fileName: String!) {
      data: importOrderItemsAsync(orderItemTypeId: $orderItemTypeId, fileName: $fileName) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
  monitoringLocationsWithOrderItemException: gql`
    query MonitoringLocationsWithOrderItemException($orderItemId: Int) {
      data: monitoringLocationsWithOrderItemException(orderItemId: $orderItemId) {
        ...ReferentialFragment
      }
    }
    ${referentialFragments.light}
  `,
};

export interface ImportOrderItemOptions extends ImportShapefileServiceOptions {
  orderItemTypeId: string;
}

@Injectable({ providedIn: 'root' })
export class OrderItemTypeService
  extends BaseReferentialService<OrderItemType, StrReferentialFilter, StrReferentialFilterCriteria, string>
  implements IImportShapefileService
{
  constructor(
    protected injector: Injector,
    protected orderItemService: OrderItemService
  ) {
    super(injector, OrderItemType, StrReferentialFilter, { queries, mutations });
    this._logPrefix = '[order-item-type-service]';
  }

  async save(entity: OrderItemType, opts?: ReferentialSaveOptions): Promise<OrderItemType> {
    // Skip saving orderItems
    if (entity.skipSaveOrderItems) {
      opts = { ...opts, withChildrenEntities: false };
    }
    return super.save(entity, { ...defaultReferentialSaveOption, ...opts });
  }

  async saveAll(entities: OrderItemType[], opts?: ReferentialSaveOptions): Promise<OrderItemType[]> {
    // Specify option for a unique entity
    if (arraySize(entities) === 1 && entities[0].skipSaveOrderItems) {
      opts = { ...opts, withChildrenEntities: false };
    }
    return super.saveAll(entities, { ...defaultReferentialSaveOption, ...opts });
  }

  async loadOrderItemsByTypeId(orderItemTypeId: string): Promise<OrderItem[]> {
    if (!orderItemTypeId) {
      return undefined;
    }
    return this.orderItemService.loadAllByCriteria({ parentId: orderItemTypeId });
  }

  async importShapefileAsync(fileName: string, opts?: ImportOrderItemOptions): Promise<Job> {
    if (!fileName || !opts?.orderItemTypeId) {
      console.warn(`missing argument, check fileName and opts.orderItemTypeId`);
      return undefined;
    }

    const variables: any = { orderItemTypeId: opts.orderItemTypeId, fileName };
    if (this._debug) console.debug(`${this._logPrefix} Import shapefile...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: Job }>({
      query: otherQueries.importShapefileAsync,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Shapefile import job started in ${Date.now() - now}ms`, data);
    return data;
  }

  async getOrderItemsExceptions(orderItemId: number): Promise<GenericReferential[]> {
    const variables: any = { orderItemId };
    if (this._debug) console.debug(`${this._logPrefix} Get monitoring locations with order item exceptions...`, variables);
    const now = Date.now();
    const query = otherQueries.monitoringLocationsWithOrderItemException;
    const res = await this.graphql.query<{ data: GenericReferential[] }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data.map(GenericReferential.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} Monitoring locations with order item exceptions loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
