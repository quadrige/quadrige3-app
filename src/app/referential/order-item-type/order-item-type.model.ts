import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { OrderItem } from '@app/referential/order-item/order-item.model';

@EntityClass({ typename: 'OrderItemTypeVO' })
export class OrderItemType extends Referential<OrderItemType, string> {
  static entityName = 'OrderItemType';
  static fromObject: (source: any, opts?: any) => OrderItemType;

  mandatory: boolean = null;
  orderItems: OrderItem[] = null;
  detailLoaded = false;
  skipSaveOrderItems = false;

  constructor() {
    super(OrderItemType.TYPENAME);
    this.entityName = OrderItemType.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = OrderItemType.entityName;
    this.mandatory = source.mandatory;
    this.orderItems = source.orderItems?.map(OrderItem.fromObject);
    this.skipSaveOrderItems = source.skipSaveOrderItems;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.orderItems = this.orderItems?.map((qv) => qv.asObject(opts));
    delete target.detailLoaded;
    delete target.skipSaveOrderItems;
    return target;
  }
}
