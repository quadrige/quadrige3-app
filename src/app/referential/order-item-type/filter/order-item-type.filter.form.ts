import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-order-item-type-filter-form',
  templateUrl: './order-item-type.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemTypeFilterForm extends ReferentialCriteriaFormComponent<StrReferentialFilter, StrReferentialFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }
}
