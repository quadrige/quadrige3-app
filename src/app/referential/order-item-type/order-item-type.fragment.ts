import gql from 'graphql-tag';
import { orderItemFragments } from '@app/referential/order-item/order-item.fragment';

export const orderItemTypeFragments = {
  orderItemType: gql`
    fragment OrderItemTypeFragment on OrderItemTypeVO {
      id
      name
      mandatory
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
  fullOrderItemType: gql`
    fragment FullOrderItemTypeFragment on OrderItemTypeVO {
      id
      name
      mandatory
      orderItems {
        ...OrderItemFragment
      }
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
    ${orderItemFragments.orderItem}
  `,
};
