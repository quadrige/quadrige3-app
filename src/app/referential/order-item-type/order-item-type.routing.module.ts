import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { OrderItemTypeTable } from '@app/referential/order-item-type/order-item-type.table';
import { OrderItemTypeModule } from '@app/referential/order-item-type/order-item-type.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: OrderItemTypeTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), OrderItemTypeModule],
})
export class OrderItemTypeRoutingModule {}
