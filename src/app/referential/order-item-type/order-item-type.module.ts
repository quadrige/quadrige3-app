import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { OrderItemTypeTable } from '@app/referential/order-item-type/order-item-type.table';
import { MonLocOrderItemModule } from '@app/referential/mon-loc-order-item/mon-loc-order-item.module';
import { OrderItemModule } from '@app/referential/order-item/order-item.module';
import { OrderItemTypeFilterForm } from '@app/referential/order-item-type/filter/order-item-type.filter.form';

@NgModule({
  imports: [ReferentialModule, OrderItemModule, MonLocOrderItemModule],
  declarations: [OrderItemTypeTable, OrderItemTypeFilterForm],
  exports: [OrderItemTypeTable],
})
export class OrderItemTypeModule {}
