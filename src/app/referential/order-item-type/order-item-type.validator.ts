import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthLabel,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { TranslateService } from '@ngx-translate/core';
import { OrderItemType } from '@app/referential/order-item-type/order-item-type.model';
import { OrderItemTypeService } from '@app/referential/order-item-type/order-item-type.service';

@Injectable({ providedIn: 'root' })
export class OrderItemTypeValidatorService extends ReferentialValidatorService<OrderItemType, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: OrderItemTypeService,
    protected translate: TranslateService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: OrderItemType, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      mandatory: [data?.mandatory || false, Validators.required],
      orderItems: [data?.orderItems || null],
      detailLoaded: [toBoolean(data?.detailLoaded, false)],
      skipSaveOrderItems: [toBoolean(data?.skipSaveOrderItems, false)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  // getFormGroupOptions(data?: OrderItemType, opts?: ReferentialValidatorOptions): AbstractControlOptions {
  //   return {
  //     validators: [
  //       ReferentialGroupValidators.requiredIfArrayEmpty('detailLoaded', false, 'orderItems', 'REFERENTIAL.ORDER_ITEM_TYPE.ERROR.MISSING_ORDER_ITEM'),
  //     ],
  //   };
  // }
}
