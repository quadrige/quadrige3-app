import { ChangeDetectionStrategy, Component, inject, Injector, OnInit, viewChild } from '@angular/core';
import {
  FilesUtils,
  IEntity,
  isNotEmptyArray,
  PlatformService,
  PromiseEvent,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { FileTransferService } from '@app/shared/service/file-transfer.service';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { MapComponent } from '@app/shared/component/map/map.component';
import { OrderItemType } from '@app/referential/order-item-type/order-item-type.model';
import { OrderItemTypeValidatorService } from '@app/referential/order-item-type/order-item-type.validator';
import { OrderItemTypeService } from '@app/referential/order-item-type/order-item-type.service';
import { Alerts } from '@app/shared/alerts';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { FeatureRecord } from '@app/shared/geometries';
import { OrderItemMemoryTable } from '@app/referential/order-item/order-item.memory.table';
import { OrderItemService } from '@app/referential/order-item/order-item.service';
import { entityId } from '@app/shared/entity.utils';
import { OrderItem, OrderItemUtils } from '@app/referential/order-item/order-item.model';
import { JobService } from '@app/social/job/job.service';
import { ExportOptions } from '@app/shared/table/table.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { Utils } from '@app/shared/utils';

type OrderItemTypeView = 'table' | 'map' | TranscribingItemView;

@Component({
  selector: 'app-order-item-type-table',
  templateUrl: './order-item-type.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemTypeTable
  extends ReferentialTable<
    OrderItemType,
    string,
    StrReferentialFilter,
    StrReferentialFilterCriteria,
    OrderItemTypeValidatorService,
    OrderItemTypeView
  >
  implements OnInit
{
  orderItemTable = viewChild<OrderItemMemoryTable>('orderItemTable');
  orderItemMap = viewChild<MapComponent>('orderItemMap');

  readonly orderItemTableMenuItem: IEntityMenuItem<OrderItemType, OrderItemTypeView> = {
    title: 'REFERENTIAL.ORDER_ITEM_TYPE.ORDER_ITEM_TABLE',
    attribute: 'orderItems',
    view: 'table',
  };

  readonly orderItemMapMenuItem: IEntityMenuItem<OrderItemType, OrderItemTypeView> = {
    title: 'REFERENTIAL.ORDER_ITEM_TYPE.ORDER_ITEM_MAP',
    attribute: 'orderItems',
    view: 'map',
  };

  readonly orderItemTypeMenuItems: IEntityMenuItem<OrderItemType, OrderItemTypeView>[] = [this.orderItemTableMenuItem, this.orderItemMapMenuItem];

  rightAreaEnabled = false;

  protected readonly platform = inject(PlatformService);
  private readonly transferService = inject(FileTransferService);
  private readonly jobService = inject(JobService);

  constructor(
    protected injector: Injector,
    protected _entityService: OrderItemTypeService,
    protected orderItemService: OrderItemService,
    protected validatorService: OrderItemTypeValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'mandatory', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      OrderItemType,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.ORDER_ITEM_TYPE.';
    this.defaultSortBy = 'id';
    this.logPrefix = '[order-item-type-table]';
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: OrderItemType = this.singleSelectedRow.currentData;
      return isNotEmptyArray(data.orderItems);
    }
    return false;
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 50);
  }

  protected getRightMenuItems(): IEntityMenuItem<OrderItemType, OrderItemTypeView>[] {
    return this.orderItemTypeMenuItems;
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<OrderItemType, OrderItemTypeView> {
    return this.orderItemTableMenuItem;
  }

  protected async canRightMenuItemChange(
    previousMenuItem: IEntityMenuItem<OrderItemType, TranscribingItemView | OrderItemTypeView>,
    nextMenuItem: IEntityMenuItem<OrderItemType, TranscribingItemView | OrderItemTypeView>
  ): Promise<boolean> {
    return Utils.promiseEveryTrue([
      super.canRightMenuItemChange(previousMenuItem, nextMenuItem),
      this.saveRightView(this.orderItemTableMenuItem, this.orderItemTable()),
    ]);
  }

  async checkCanDeleteOrderItem(event: PromiseEvent<boolean, { rows: AsyncTableElement<OrderItem>[] }>) {
    let canDelete = true;
    if (isNotEmptyArray(event.detail.rows)) {
      const exceptions: string[] = [];
      for (const row of event.detail.rows) {
        const orderItem = row.currentData;
        const monitoringLocations = await this._entityService.getOrderItemsExceptions(orderItem.id);
        if (isNotEmptyArray(monitoringLocations)) {
          monitoringLocations.forEach((monitoringLocation) => {
            exceptions.push(`${orderItem.label} <-> ${referentialToString(monitoringLocation, referentialOptions.monitoringLocation.attributes)}`);
          });
        }
      }
      if (isNotEmptyArray(exceptions)) {
        canDelete = await Alerts.askDetailedConfirmation(
          'REFERENTIAL.ORDER_ITEM.CONFIRM.DELETE_EXCEPTIONS',
          this.alertCtrl,
          this.translate,
          undefined,
          {
            detailedMessage: exceptions.join('<br>'),
          }
        );
      }
    }
    event.detail.success(canDelete);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<OrderItemType>): Promise<boolean> {
    row = row || this.singleEditingRow;

    // First, confirm and save right tables
    if (row && !(await this.saveRightView(this.orderItemTableMenuItem, this.orderItemTable(), { row }))) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { forceAllColumns: true, showSelectType: true, rowCountWarningThreshold: 4, ...opts });
  }

  async downloadShapefile(event: MouseEvent, row?: AsyncTableElement<OrderItemType>) {
    row = row || this.singleSelectedRow;
    await super.export(event, {
      exportType: 'SHAPEFILE',
      forceAllColumns: true,
      defaultFileName: 'ORDER_ITEM_' + row.currentData.id,
      defaultFilter: StrReferentialFilter.fromObject({ criterias: [{ parentId: row.currentData.id }] }),
    });
  }

  async uploadShapefile(event: UIEvent, row?: AsyncTableElement<OrderItemType>) {
    row = row || this.singleEditingRow;
    if (!row || !row.editing || !row.validator.valid || !row.currentData.creationDate) {
      console.warn(`${this.logPrefix} Can't upload file: Invalid OrderItemType selected`, row);
      return;
    }

    const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
      title: '',
      fileExtension: '.zip',
      uniqueFile: true,
      uploadFn: (file: File) => this.transferService.uploadShapefile(file),
    });
    if (isNotEmptyArray(data)) {
      const filename = data[0].name;
      // Import shapefile asynchronously
      const job = await this._entityService.importShapefileAsync(filename, { orderItemTypeId: row.currentData.id });
      if (!job || (job as IEntity<any>).__typename !== 'JobVO') {
        console.error(`${this.logPrefix} a Job should be returned !`, job);
        return;
      }
      this.jobService.listenImportShapeJob(job.id, this.dirtySubject, this);
    }
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    const continueSave = await this.saveRightView(this.orderItemTableMenuItem, this.orderItemTable());
    if (continueSave) {
      return super.save(opts);
    }
    return false;
  }

  protected async loadRightArea(row?: AsyncTableElement<OrderItemType>) {
    switch (this.rightMenuItem?.view) {
      case 'table': {
        this.detectChanges();
        if (this.orderItemTable()) {
          this.registerSubForm(this.orderItemTable());
          await this.loadOrderItemTable(row);
        }
        break;
      }
      case 'map': {
        this.detectChanges();
        if (this.orderItemMap()) {
          await this.loadOrderItemMap(row);
        }
        break;
      }
    }
  }

  protected async loadOrderItemTable(row?: AsyncTableElement<OrderItemType>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.orderItemTable().setValue(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    await this.loadDetailData(row);
    await this.orderItemTable().setValue(row.currentData?.orderItems?.slice() || []);
    this.rightAreaEnabled = this.canEdit;
  }

  protected async loadDetailData(row?: AsyncTableElement<OrderItemType>) {
    // Call service if detail data is not loaded yet
    if (!row.currentData.detailLoaded) {
      this.orderItemTable()?.markAsLoading();

      // Get sub entities
      const orderItems = await this._entityService.loadOrderItemsByTypeId(row.currentData.id);

      row.validator.patchValue({ orderItems, detailLoaded: true });
    }
  }

  protected async loadOrderItemMap(row?: AsyncTableElement<OrderItemType>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.orderItemMap().loadGeoData(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    await this.loadDetailData(row);
    const orderItems = row.currentData.orderItems || [];
    await this.loadGeometryData(orderItems);
    await this.orderItemMap().loadGeoData(orderItems.map((orderItem) => orderItem.geometry));
    this.rightAreaEnabled = this.canEdit;
  }

  protected async loadGeometryData(orderItems: OrderItem[]) {
    this.orderItemMap().markAsLoading();

    // Load missing geometries
    const geometryIdsToLoad = orderItems.filter((value) => !value.geometryLoaded).map(entityId);
    const geometries: FeatureRecord = await this.orderItemService.loadGeometryFeatures(geometryIdsToLoad);
    OrderItemUtils.updateOrderItemGeometries(orderItems, geometries);

    this.orderItemMap().markAsLoaded();
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.ORDER_ITEM_TYPES' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'mandatory');
  }

  protected rightSplitResized() {
    super.rightSplitResized();
    this.orderItemMap()?.containerResize();
  }
}
