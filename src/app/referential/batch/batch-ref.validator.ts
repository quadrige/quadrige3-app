import { Injectable } from '@angular/core';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { BatchRef } from '@app/referential/batch/batch-ref.model';
import { UntypedFormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BatchRefService } from '@app/referential/batch/batch-ref.service';
import { PartialRecord } from '@app/shared/model/interface';
import { SharedValidators, toNumber } from '@sumaris-net/ngx-components';

@Injectable({ providedIn: 'root' })
export class BatchRefValidator extends ReferentialValidatorService<BatchRef> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: BatchRefService,
    protected translate: TranslateService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: BatchRef, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return <PartialRecord<keyof BatchRef, any>>{
      id: [data?.id || null],
      label: [data?.label || null],
      name: [data?.name || null],
      condition: [data?.condition || null],
      monitoringLocation: [data?.monitoringLocation || null, SharedValidators.entity],
      depthLevel: [data?.depthLevel || null, SharedValidators.entity],
      breedingSystem: [data?.breedingSystem || null, SharedValidators.entity],
      breedingSystemCount: [toNumber(data?.breedingSystemCount), SharedValidators.integer],
      breedingStructure: [data?.breedingStructure || null, SharedValidators.entity],
      breedingStructureCount: [toNumber(data?.breedingStructureCount), SharedValidators.integer],
      comments: [data?.comments || null],
      updateDate: [data?.updateDate || null],
    };
  }
}
