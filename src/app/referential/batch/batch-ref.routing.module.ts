import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { inject, NgModule } from '@angular/core';
import { BatchRefModule } from '@app/referential/batch/batch-ref.module';
import { BatchRefTable } from '@app/referential/batch/batch-ref.table';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: BatchRefTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), BatchRefModule],
})
export class BatchRefRoutingModule {}
