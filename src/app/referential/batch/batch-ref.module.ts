import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { BatchRefTable } from '@app/referential/batch/batch-ref.table';
import { BatchRefFilterForm } from '@app/referential/batch/filter/batch-ref.filter.form';

@NgModule({
  imports: [ReferentialModule, BatchRefFilterForm],
  declarations: [BatchRefTable],
  exports: [BatchRefTable],
})
export class BatchRefModule {}
