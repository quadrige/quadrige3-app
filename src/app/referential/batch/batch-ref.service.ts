import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { Injectable, Injector } from '@angular/core';
import { BatchRef } from '@app/referential/batch/batch-ref.model';
import { BatchRefFilter, BatchRefFilterCriteria } from '@app/referential/batch/filter/batch-ref.filter.model';

export const batchRefFragment = gql`
  fragment BatchRefFragment on BatchVO {
    id
    label
    name
    condition
    breedingStructure {
      ...ReferentialFragment
    }
    breedingStructureCount
    breedingSystem {
      ...ReferentialFragment
    }
    breedingSystemCount
    monitoringLocation {
      ...ReferentialFragment
    }
    depthLevel {
      ...ReferentialFragment
    }
    comments
    updateDate
  }
  ${referentialFragments.light}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Batchs($page: PageInput, $filter: BatchFilterVOInput) {
      data: batchs(page: $page, filter: $filter) {
        ...BatchRefFragment
      }
    }
    ${batchRefFragment}
  `,
  loadAllWithTotal: gql`
    query BatchsWithTotal($page: PageInput, $filter: BatchFilterVOInput) {
      data: batchs(page: $page, filter: $filter) {
        ...BatchRefFragment
      }
      total: batchsCount(filter: $filter)
    }
    ${batchRefFragment}
  `,
  countAll: gql`
    query BatchsCount($filter: BatchFilterVOInput) {
      total: batchsCount(filter: $filter)
    }
  `,
};

const mutations: BaseEntityGraphqlMutations = {};

@Injectable({ providedIn: 'root' })
export class BatchRefService extends BaseReferentialService<BatchRef, BatchRefFilter, BatchRefFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, BatchRef, BatchRefFilter, { queries, mutations });
    this._logPrefix = '[batch-ref-service]';
  }
}
