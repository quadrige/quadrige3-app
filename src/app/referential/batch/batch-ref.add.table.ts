import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { BatchRefService } from '@app/referential/batch/batch-ref.service';
import { BatchRefValidator } from '@app/referential/batch/batch-ref.validator';
import { BatchRefTable } from '@app/referential/batch/batch-ref.table';
import { IAddTable } from '@app/shared/table/table.model';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { BatchRefFilter } from '@app/referential/batch/filter/batch-ref.filter.model';
import { BatchRef } from '@app/referential/batch/batch-ref.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { BatchRefFilterForm } from '@app/referential/batch/filter/batch-ref.filter.form';

@Component({
  selector: 'app-batch-ref-add-table',
  templateUrl: './batch-ref.table.html',
  standalone: true,
  imports: [ReferentialModule, BatchRefFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BatchRefAddTable extends BatchRefTable implements IAddTable<BatchRef> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: BatchRefService,
    protected validatorService: BatchRefValidator
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[batch-ref-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: BatchRefFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
