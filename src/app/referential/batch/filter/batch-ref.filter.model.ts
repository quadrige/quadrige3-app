import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { BatchRef } from '@app/referential/batch/batch-ref.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'BatchRefFilterCriteriaVO' })
export class BatchRefFilterCriteria extends ReferentialFilterCriteria<BatchRef, number> {
  static fromObject: (source: any, opts?: any) => BatchRefFilterCriteria;

  monitoringLocationFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(BatchRefFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    // Remove unhandled properties (Mantis #63344)
    delete target.exactValues;
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'BatchFilterVO' })
export class BatchRefFilter extends ReferentialFilter<BatchRefFilter, BatchRefFilterCriteria, BatchRef> {
  static fromObject: (source: any, opts?: any) => BatchRefFilter;

  constructor() {
    super(BatchRefFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  criteriaFromObject(source: any, opts: any): BatchRefFilterCriteria {
    return BatchRefFilterCriteria.fromObject(source, opts);
  }
}
