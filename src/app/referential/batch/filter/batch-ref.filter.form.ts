import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { BatchRefFilter, BatchRefFilterCriteria } from './batch-ref.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-batch-ref-filter-form',
  templateUrl: './batch-ref.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BatchRefFilterForm extends ReferentialCriteriaFormComponent<BatchRefFilter, BatchRefFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabelName;
  }

  protected criteriaToQueryParams(criteria: BatchRefFilterCriteria): PartialRecord<keyof BatchRefFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    return params;
  }
}
