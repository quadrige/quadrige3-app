import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { BatchRef } from '@app/referential/batch/batch-ref.model';
import { BatchRefService } from '@app/referential/batch/batch-ref.service';
import { BatchRefValidator } from '@app/referential/batch/batch-ref.validator';
import { BatchRefFilter, BatchRefFilterCriteria } from '@app/referential/batch/filter/batch-ref.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-batch-ref-table',
  templateUrl: './batch-ref.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BatchRefTable extends ReferentialTable<BatchRef, number, BatchRefFilter, BatchRefFilterCriteria, BatchRefValidator> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: BatchRefService,
    protected validatorService: BatchRefValidator
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'label',
        'name',
        'condition',
        'monitoringLocation',
        'depthLevel',
        'breedingSystem',
        'breedingSystemCount',
        'breedingStructure',
        'breedingStructureCount',
        'comments',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      BatchRef,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.BATCH.';
    this.defaultSortBy = 'label';
    this.logPrefix = '[batch-ref-table]';
    this.setShowColumn('statusId', false);
  }

  protected getDefaultHiddenColumns(): string[] {
    return super
      .getDefaultHiddenColumns()
      .concat('condition', 'depthLevel', 'breedingSystem', 'breedingSystemCount', 'breedingStructure', 'breedingStructureCount', 'comments');
  }
}
