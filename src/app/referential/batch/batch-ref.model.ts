import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential, StrReferential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'BatchVO' })
export class BatchRef extends Referential<BatchRef> {
  static entityName = 'Batch';
  static fromObject: (source: any, opts?: any) => BatchRef;

  condition: string = null;
  breedingStructureCount: number = null;
  breedingSystemCount: number = null;
  monitoringLocation: IntReferential = null;
  breedingSystem: StrReferential = null;
  breedingStructure: StrReferential = null;
  depthLevel: IntReferential = null;

  constructor() {
    super(BatchRef.TYPENAME);
    this.entityName = BatchRef.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = BatchRef.entityName;
    this.condition = source.condition;
    this.breedingStructureCount = source.breedingStructureCount;
    this.breedingSystemCount = source.breedingSystemCount;
    this.breedingStructure = StrReferential.fromObject(source.breedingStructure);
    this.breedingSystem = StrReferential.fromObject(source.breedingSystem);
    this.monitoringLocation = IntReferential.fromObject(source.monitoringLocation);
    this.depthLevel = IntReferential.fromObject(source.depthLevel);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.breedingStructure = this.breedingStructure?.asObject(opts);
    target.breedingSystem = this.breedingSystem?.asObject(opts);
    target.monitoringLocation = this.monitoringLocation?.asObject(opts);
    target.depthLevel = this.depthLevel?.asObject(opts);
    return target;
  }
}
