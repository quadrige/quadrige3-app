import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SamplingEquipmentTable } from '@app/referential/sampling-equipment/sampling-equipment.table';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { SamplingEquipmentModule } from '@app/referential/sampling-equipment/sampling-equipment.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SamplingEquipmentTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SamplingEquipmentModule],
})
export class SamplingEquipmentRoutingModule {}
