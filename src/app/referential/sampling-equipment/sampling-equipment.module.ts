import { NgModule } from '@angular/core';
import { SamplingEquipmentTable } from '@app/referential/sampling-equipment/sampling-equipment.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { SamplingEquipmentFilterForm } from '@app/referential/sampling-equipment/filter/sampling-equipment.filter.form';

@NgModule({
  imports: [ReferentialModule],
  declarations: [SamplingEquipmentTable, SamplingEquipmentFilterForm],
  exports: [SamplingEquipmentTable, SamplingEquipmentFilterForm],
})
export class SamplingEquipmentModule {}
