import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedFormGroupValidators, SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { SamplingEquipment } from '@app/referential/sampling-equipment/sampling-equipment.model';
import { SamplingEquipmentService } from '@app/referential/sampling-equipment/sampling-equipment.service';
import {
  lengthComment,
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';

@Injectable({ providedIn: 'root' })
export class SamplingEquipmentValidatorService extends ReferentialValidatorService<SamplingEquipment> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: SamplingEquipmentService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: SamplingEquipment, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      size: [toNumber(data?.size, null), SharedValidators.decimal()],
      unit: [data?.unit || null, SharedValidators.entity],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: SamplingEquipment, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [SharedFormGroupValidators.requiredIf('size', 'unit'), SharedFormGroupValidators.requiredIf('unit', 'size')],
    };
  }
}
