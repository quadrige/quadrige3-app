import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { SamplingEquipment } from '@app/referential/sampling-equipment/sampling-equipment.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { jobFragments } from '@app/social/job/job.service';

const fragments = {
  samplingEquipment: gql`
    fragment SamplingEquipmentFragment on SamplingEquipmentVO {
      id
      name
      description
      comments
      size
      unit {
        ...ReferentialFragment
      }
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query SamplingEquipments($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: samplingEquipments(page: $page, filter: $filter) {
        ...SamplingEquipmentFragment
      }
    }
    ${fragments.samplingEquipment}
    ${referentialFragments.light}
  `,

  loadAllWithTotal: gql`
    query SamplingEquipmentsWithTotal($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: samplingEquipments(page: $page, filter: $filter) {
        ...SamplingEquipmentFragment
      }
      total: samplingEquipmentsCount(filter: $filter)
    }
    ${fragments.samplingEquipment}
    ${referentialFragments.light}
  `,

  countAll: gql`
    query SamplingEquipmentsCount($filter: IntReferentialFilterVOInput) {
      total: samplingEquipmentsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportSamplingEquipmentsAsync($filter: IntReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportSamplingEquipmentsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveSamplingEquipments($data: [SamplingEquipmentVOInput]) {
      data: saveSamplingEquipments(samplingEquipments: $data) {
        ...SamplingEquipmentFragment
      }
    }
    ${fragments.samplingEquipment}
    ${referentialFragments.light}
  `,

  deleteAll: gql`
    mutation DeleteSamplingEquipments($ids: [Int]) {
      deleteSamplingEquipments(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class SamplingEquipmentService extends BaseReferentialService<SamplingEquipment, IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, SamplingEquipment, IntReferentialFilter, { queries, mutations });
    this._logPrefix = '[sampling-equipment-service]';
  }
}
