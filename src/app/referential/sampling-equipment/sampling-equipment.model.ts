import { EntityClass, ReferentialAsObjectOptions, toNumber } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'SamplingEquipmentVO' })
export class SamplingEquipment extends Referential<SamplingEquipment> {
  static entityName = 'SamplingEquipment';
  static fromObject: (source: any, opts?: any) => SamplingEquipment;

  size: number = null;
  unit: IntReferential = null;

  constructor() {
    super(SamplingEquipment.TYPENAME);
    this.entityName = SamplingEquipment.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = SamplingEquipment.entityName;
    this.size = toNumber(source.size);
    this.unit = IntReferential.fromObject(source.unit);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.size = toNumber(this.size);
    target.unit = EntityUtils.asMinifiedObject(this.unit, opts);
    return target;
  }
}
