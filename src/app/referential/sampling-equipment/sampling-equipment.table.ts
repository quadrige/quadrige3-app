import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { SamplingEquipment } from '@app/referential/sampling-equipment/sampling-equipment.model';
import { SamplingEquipmentService } from '@app/referential/sampling-equipment/sampling-equipment.service';
import { SamplingEquipmentValidatorService } from '@app/referential/sampling-equipment/sampling-equipment.validator';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-sampling-equipment-table',
  templateUrl: './sampling-equipment.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SamplingEquipmentTable
  extends ReferentialTable<SamplingEquipment, number, IntReferentialFilter, IntReferentialFilterCriteria, SamplingEquipmentValidatorService>
  implements OnInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: SamplingEquipmentService,
    protected validatorService: SamplingEquipmentValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'size', 'unit', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      SamplingEquipment,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.SAMPLING_EQUIPMENT.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[sampling-equipment-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    // Unit combo
    this.registerAutocompleteField('unit', {
      ...this.referentialOptions.unit,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Unit',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 50);
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.SAMPLING_EQUIPMENTS' });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name'); // todo add 'size', 'unit' ? (because mutual required)
  }
}
