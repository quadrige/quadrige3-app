import { AfterViewInit, Directive, inject, Injector, model, OnDestroy, OnInit, viewChild } from '@angular/core';
import { isNilOrBlank, isNotEmptyArray, KeyType, StatusIds } from '@sumaris-net/ngx-components';
import { IWithTranscribingItems, Referential } from '@app/referential/model/referential.model';
import { CanLeave } from '@app/core/service/component-guard.service';
import { ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { UpdatableView } from '@app/shared/model/updatable-view';
import { EntityUtils } from '@app/shared/entity.utils';
import {
  IReferentialService,
  ReferentialServiceLoadOptions,
  ReferentialServiceWatchOptions,
} from '@app/referential/service/base-referential.service';
import { ITranscribingItem, TranscribingItem, TranscribingItemUtils } from '@app/referential/transcribing-item/transcribing-item.model';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { BaseTable } from '@app/shared/table/base.table';
import { BaseTableDataSourceConfig } from '@app/shared/table/base.datasource';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';
import { TranscribingItemTypeService } from '@app/referential/transcribing-item-type/transcribing-item-type.service';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { TranscribingItemView, TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { entitiesWithoutTranscribingItems } from '@app/referential/model/referential.constants';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { ExportOptions } from '@app/shared/table/table.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';

@Directive()
export abstract class ReferentialTable<
    E extends Referential<E, ID>,
    ID extends KeyType,
    F extends ReferentialFilter<F, C, E, ID>,
    C extends ReferentialFilterCriteria<E, ID>,
    V extends ReferentialValidatorService<E, ID> = any,
    RV = TranscribingItemView,
    WO extends ReferentialServiceWatchOptions = ReferentialServiceWatchOptions,
    LO extends ReferentialServiceLoadOptions = ReferentialServiceLoadOptions,
  >
  extends BaseTable<E, ID, F, C, V, RV, WO>
  implements OnInit, AfterViewInit, CanLeave, UpdatableView, OnDestroy
{
  transcribingItemTable = viewChild<TranscribingItemRowTable>('transcribingItemTable');
  transcribingItemEnabled = model<boolean>(true);
  protected transcribingItemTypeService = inject(TranscribingItemTypeService);

  transcribingMenuItems: IEntityMenuItem<IWithTranscribingItems, RV | TranscribingItemView>[] = [];

  private readonly genericEntityName: boolean;
  private _dirtyRowsWithDisabledStatusId: AsyncTableElement<E>[] = [];

  protected constructor(
    protected injector: Injector,
    protected columns: string[],
    public readonly dataType: new () => E,
    protected _entityService: IReferentialService<E, F, C, ID, WO, LO>,
    protected validatorService: V,
    options?: BaseTableDataSourceConfig<E, ID, WO>
  ) {
    super(injector, columns, dataType, _entityService, validatorService, options);

    this.genericEntityName = isNilOrBlank(this.entityName) || 'Referential' === this.entityName;

    this.i18nColumnPrefix = 'REFERENTIAL.';
    this.logPrefix = '[referential-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent && TranscribingItemUtils.hasValidTranscribingItems(this.singleSelectedRow?.currentData);
  }

  get subTable(): boolean {
    return super.subTable;
  }

  set subTable(value: boolean) {
    super.subTable = value;
    this.transcribingItemEnabled.set(!value);
  }

  ngOnInit() {
    super.ngOnInit();

    // Listen refresh event
    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this._dirtyRowsWithDisabledStatusId = [];
        this.openRightPanelOnSystemChange(this.filter.systemId);
      })
    );

    // Listen status cell changes
    this.registerSubscription(
      this.registerCellValueChanges('statusId').subscribe((value) => {
        const editingRow = this.singleEditingRow;
        if (!editingRow) return;
        if (value === StatusIds.DISABLE) {
          if (!this._dirtyRowsWithDisabledStatusId.includes(editingRow)) this._dirtyRowsWithDisabledStatusId.push(editingRow);
        } else {
          this._dirtyRowsWithDisabledStatusId = this._dirtyRowsWithDisabledStatusId.filter((row) => row !== editingRow);
        }
      })
    );

    // Initialize right menu if the entity is well-known
    if (!this.genericEntityName) {
      this.initRightView();
    }
  }

  async setFilter(f: F, opts?: ISetFilterOptions) {
    f = this.asFilter(f) || f; // this.asFilter(f) should return a filter class, but for UnfilteredEntityMemoryService, it returns undefined
    // Filter on statusId.ENABLED for default and add tables (Mantis #60625)
    if (
      BaseFilterUtils.isFilterEmpty(f) &&
      opts?.firstTime === true &&
      (!this.subTable || this.addTable) &&
      this.getShowColumn('statusId') &&
      (this.isManagementContext() || this.isReferentialContext()) /* Only on management & referential context */
    ) {
      f = <F>{ criterias: [{ statusId: StatusIds.ENABLE }] };
      await this.filterFormComponent()?.setValue(f);
      opts = { ...opts, updateQueryParams: true };
    }

    await super.setFilter(f, opts);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<E>): Promise<boolean> {
    row = row || this.singleEditingRow || this.singleSelectedRow;

    if (row && this.canEdit && this.transcribingItemEnabled() && this.transcribingItemTable()?.dirty) {
      // Save transcribing items table first
      if (!(await this.transcribingItemTable().save())) {
        return false;
      }
    }

    return super.confirmEditCreate(event, row);
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    // Check if saving referential with disabled status is allowed
    if (this._dirtyRowsWithDisabledStatusId.length && !(await this.canSaveRowsWithDisabledStatus(this._dirtyRowsWithDisabledStatusId))) {
      return false;
    }

    // Save transcribing items table first (if enabled and visible)
    let continueSave = true;
    if (this.transcribingItemEnabled() && this.transcribingItemTable()) {
      continueSave = await this.transcribingItemTable().save();
    }

    if (continueSave) {
      try {
        // Call save
        return await super.save(opts);
      } finally {
        this._dirtyRowsWithDisabledStatusId = [];
      }
    }
    return false;
  }

  async cancelOrDelete(event: Event, row: AsyncTableElement<E>, opts?: { interactive?: boolean; keepEditing?: boolean }): Promise<void> {
    await super.cancelOrDelete(event, row, opts);
    if (isNotEmptyArray(this._dirtyRowsWithDisabledStatusId)) {
      this._dirtyRowsWithDisabledStatusId = this._dirtyRowsWithDisabledStatusId.filter((r) => row !== r);
    }
  }

  async deleteRows(event: UIEvent | null, rows: AsyncTableElement<E>[], opts?: { interactive?: boolean }): Promise<number> {
    const nbDelete = await super.deleteRows(event, rows, opts);
    if (nbDelete > 0 && isNotEmptyArray(this._dirtyRowsWithDisabledStatusId)) {
      this._dirtyRowsWithDisabledStatusId = this._dirtyRowsWithDisabledStatusId.filter((row) => !rows.includes(row));
    }
    return nbDelete;
  }

  entityI18nName(entityName: string, i18nRoot?: string): string {
    return super.entityI18nName(entityName, 'REFERENTIAL.');
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    await super.export(event, { transcribingItemEnabled: this.transcribingItemEnabled(), ...opts });
  }

  // protected methods

  protected async initRightView() {
    // Init transcribing menu items with current entity name
    this.transcribingMenuItems = await this.transcribingItemTypeService.initMenus(this.entityName);

    // Allow transcribing if there is available menu and if the entity allows it
    this.transcribingItemEnabled.set(
      !this.subTable && isNotEmptyArray(this.transcribingMenuItems) && !entitiesWithoutTranscribingItems.includes(this.entityName)
    );

    // Populate right menu items (default is transcribing menu items)
    this.rightMenuItems = this.getRightMenuItems();
    this.rightMenuItem = this.getDefaultRightMenuItem();

    // Hide right panel if no right menu available
    this.rightPanelAllowed = this.rightPanelAllowed && isNotEmptyArray(this.rightMenuItems);

    this.markForCheck();
    this.loadRightArea();
  }

  protected getRightMenuItems(): IEntityMenuItem<E, RV>[] {
    return this.transcribingMenuItems as IEntityMenuItem<E, RV>[];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<E, RV> {
    return this.transcribingMenuItems?.[0]?.children?.[0] as IEntityMenuItem<E, RV>;
  }

  protected async canRightMenuItemChange(previousMenuItem: IEntityMenuItem<E, RV>, nextMenuItem: IEntityMenuItem<E, RV>): Promise<boolean> {
    if (this.transcribingItemEnabled() && previousMenuItem.view === 'transcribingItemTable' && this.transcribingItemTable()?.dirty) {
      if (!(await this.transcribingItemTable().save())) {
        return false;
      }
    }
    return true;
  }

  protected async openRightPanelOnSystemChange(systemId: TranscribingSystemType) {
    // Don't open if not allowed or on default system
    if (!this.rightPanelAllowed || TranscribingItemTypeUtils.isDefaultSystem(systemId)) return;

    // Find corresponding menu
    const transcribingMenuItem = this.transcribingMenuItems
      .flatMap((menuItem) => menuItem.children)
      .find((menuItem) => menuItem.view === 'transcribingItemTable' && menuItem.params?.systemId === systemId);

    if (transcribingMenuItem) {
      this.openRightPanel();
      await this.setRightMenuItem(transcribingMenuItem as IEntityMenuItem<E, RV>);
    }
  }

  protected async loadRightArea(row?: AsyncTableElement<E>) {
    // Load transcribing item table if enabled
    if (this.transcribingItemEnabled()) {
      await this.transcribingItemTypeService.ready();
      if (this.rightMenuItem?.view === 'transcribingItemTable') {
        if (!this.transcribingItemTable()) {
          this.cd.detectChanges();
        }
        if (this.transcribingItemTable()) {
          this.registerSubForm(this.transcribingItemTable());
        } else {
          console.error(`${this.logPrefix} Transcribing item table not detected. Please check the template.`);
          return;
        }
        console.debug(`${this.logPrefix} load transcribing item table`);
        await this.transcribingItemTable().setSelection(this.selection.selected, this.rightMenuItem?.params);
        this.transcribingItemTable().updateTitle();
        this.transcribingItemTable().canEdit = this.canEdit;
      }
    }
    setTimeout(() => this.rightSplitResized(), 50);
  }

  protected async saveRightView(
    menuItem: IEntityMenuItem<E, RV>,
    rightView: BaseMemoryTable<any, any, any, any>,
    opts?: {
      forceSave?: boolean;
      row?: AsyncTableElement<E>;
    }
  ): Promise<boolean> {
    if (this.canEdit && this.rightMenuItem?.view === menuItem.view && rightView) {
      if (opts?.forceSave || rightView.dirty) {
        const saved = await rightView.save();
        if (saved) {
          this.patchRow(menuItem.attribute, rightView.value, opts?.row);
          return true;
        } else {
          console.error(`${this.logPrefix} Sub table for ${rightView.entityName} has not been saved.`);
          return false;
        }
      }
    }
    return true; // noting to save, ok
  }

  protected async patchDuplicateEntity(entity: E, opts?: any): Promise<any> {
    return super.patchDuplicateEntity(entity, { ...opts, keepEntityName: true });
  }

  protected async patchDuplicateAdditionalProperties(source: E, target: any, opts?: any): Promise<void> {
    // Convert transcribing items here (not in AbstractReferential.fromObject: cause circular dependency) (Mantis #60355)
    let transcribingItems = source.transcribingItems;
    if (!source.transcribingItemsLoaded) {
      // Load transcribing items if not loaded yet
      transcribingItems = await this.transcribingItemTypeService.loadTranscribingItemsTree({
        entityName: source.entityName,
        entityId: source.id.toString(),
      });
    }
    if (isNotEmptyArray(transcribingItems)) {
      target.transcribingItems = this.patchTranscribingItems(transcribingItems);
      target.transcribingItemsLoaded = true;
    }
  }

  protected patchTranscribingItems(source: any[]): any[] {
    return (source || []).map((transcribingItem) => {
      return TranscribingItem.fromObject(<ITranscribingItem>{
        ...transcribingItem.asObject(),
        id: undefined,
        entityId: undefined,
        parentId: undefined,
        creationDate: undefined,
        updateDate: undefined,
        comments: undefined,
        children: this.patchTranscribingItems(transcribingItem.children),
      });
    });
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<E>[]): Promise<boolean> {
    // Default
    return true;
  }

  protected equals(t1: E, t2: E): boolean {
    return super.equals(t1, t2) || ((!t1?.id || !t2.id) && EntityUtils.equals(t1, t2, 'name'));
  }
}
