import { AfterViewInit, Component, Injectable, Injector, OnInit } from '@angular/core';
import {
  lengthComment,
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS, toNumber } from '@sumaris-net/ngx-components';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { GenericService } from '@app/referential/generic/generic.service';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';

@Injectable()
class ReferentialTestService extends EntitiesMemoryService<GenericReferential, StrReferentialFilter, string> {
  constructor() {
    super(GenericReferential, StrReferentialFilter);
  }
}

@Injectable()
class ReferentialTestValidatorService extends ReferentialValidatorService<GenericReferential, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialService: GenericService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: GenericReferential, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.referentialService, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}

@Component({
  selector: 'app-referential-table-test',
  templateUrl: './referential.test.table.html',
  providers: [{ provide: ReferentialTestService }, { provide: ReferentialTestValidatorService }],
})
export class ReferentialTableTestPage
  extends ReferentialMemoryTable<GenericReferential, string, StrReferentialFilter, StrReferentialFilterCriteria, ReferentialTestValidatorService>
  implements OnInit, AfterViewInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: ReferentialTestService,
    protected validatorService: ReferentialTestValidatorService
  ) {
    super(
      injector,
      RESERVED_START_COLUMNS.concat(['name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      GenericReferential,
      _entityService,
      validatorService
    );
  }

  ngAfterViewInit(): void {
    this.loadData();
  }

  // Load the form with data
  async loadData() {
    const data: GenericReferential[] = [];
    data.push(
      GenericReferential.fromObject({
        id: 1,
        name: 'name abc',
        description: 'description 1',
        comments: 'comments1',
        statusId: 1,
        entityName: 'QualitativeValue',
      })
    );
    data.push(
      GenericReferential.fromObject({
        id: 2,
        name: 'name def',
        description: 'description 2',
        comments: 'comments2',
        statusId: 0,
        entityName: 'QualitativeValue',
      })
    );

    await this.setValue(data);
  }

  /* -- protected methods -- */
}
