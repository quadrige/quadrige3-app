import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReferentialTableTestPage } from './referential.test.table';
import { ReferentialModule } from '../../referential.module';
import { QuadrigeSharedModule } from '@app/shared/quadrige.shared.module';
import { ReferentialTestFilterForm } from '@app/referential/table/testing/referential.test.filter.form';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ReferentialTableTestPage,
  },
];

@NgModule({
  imports: [CommonModule, QuadrigeSharedModule, RouterModule.forChild(routes), ReferentialModule],
  declarations: [ReferentialTableTestPage, ReferentialTestFilterForm],
  exports: [ReferentialTableTestPage, ReferentialTestFilterForm],
})
export class TableTestingModule {}
