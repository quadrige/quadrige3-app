import { AfterViewInit, Directive, Injector, OnDestroy, OnInit } from '@angular/core';
import { KeyType } from '@sumaris-net/ngx-components';
import { CanLeave } from '@app/core/service/component-guard.service';
import { ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { UpdatableView } from '@app/shared/model/updatable-view';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { BaseTableDataSourceConfig } from '@app/shared/table/base.datasource';
import { Referential } from '@app/referential/model/referential.model';

@Directive()
export abstract class ReferentialMemoryTable<
    E extends Referential<E, ID>,
    ID extends KeyType,
    F extends ReferentialFilter<F, C, E, ID>,
    C extends ReferentialFilterCriteria<E, ID>,
    V extends ReferentialValidatorService<E, ID> = any,
  >
  extends BaseMemoryTable<E, ID, F, C, V>
  implements OnInit, AfterViewInit, CanLeave, UpdatableView, OnDestroy
{
  protected constructor(
    protected injector: Injector,
    protected columns: string[],
    public readonly dataType: new () => E,
    protected _entityService: EntitiesMemoryService<E, F, ID>,
    protected validatorService: V,
    options?: BaseTableDataSourceConfig<E, ID>
  ) {
    super(injector, columns, dataType, _entityService, validatorService, options);

    this.i18nColumnPrefix = 'REFERENTIAL.';
    this.logPrefix = '[referential-memory-table]';
  }

  entityI18nName(entityName: string, i18nRoot?: string): string {
    return super.entityI18nName(entityName, 'REFERENTIAL.');
  }

  // protected methods

  protected equals(t1: E, t2: E): boolean {
    return super.equals(t1, t2) || ((!t1?.id || !t2.id) && EntityUtils.equals(t1, t2, 'name'));
  }

  protected async patchDuplicateEntity(entity: E, opts?: any): Promise<any> {
    return super.patchDuplicateEntity(entity, { ...opts, keepEntityName: true });
  }
}
