import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { MetaProgramPmfmu } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'MetaProgramLocationVO' })
export class MetaProgramLocation extends Referential<MetaProgramLocation> {
  static entityName = 'MetaProgramLocation';
  static fromObject: (source: any, opts?: any) => MetaProgramLocation;

  metaProgramId: string = null;
  monitoringLocation: IntReferential = null;
  metaProgramPmfmuIds: number[] = null;
  metaProgramPmfmus: MetaProgramPmfmu[] = null;

  constructor(metaProgramId?: string, monitoringLocation?: IntReferential, metaProgramPmfmus?: MetaProgramPmfmu[]) {
    super(MetaProgramLocation.TYPENAME);
    this.entityName = MetaProgramLocation.entityName;
    this.metaProgramId = metaProgramId;
    this.monitoringLocation = monitoringLocation;
    this.metaProgramPmfmus = metaProgramPmfmus;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = MetaProgramLocation.entityName;
    this.metaProgramId = source.metaProgramId;
    this.monitoringLocation = IntReferential.fromObject(source.monitoringLocation);
    this.metaProgramPmfmuIds = source.metaProgramPmfmuIds || [];
    this.metaProgramPmfmus = source.metaProgramPmfmus?.map(MetaProgramPmfmu.fromObject) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.monitoringLocation = EntityUtils.asMinifiedObject(this.monitoringLocation, opts);
    target.metaProgramPmfmus = this.metaProgramPmfmus?.map((value) => value.asObject(opts));
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.statusId;
    delete target.metaProgramPmfmuIds;
    return target;
  }
}
