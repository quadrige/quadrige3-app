import { FilterFn } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';

export class MetaProgramLocationFilterCriteria extends ReferentialFilterCriteria<MetaProgramLocation, number> {
  static fromObject(source: any, opts?: any): MetaProgramLocationFilterCriteria {
    const target = new MetaProgramLocationFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  protected buildFilter(): FilterFn<MetaProgramLocation>[] {
    const target: FilterFn<MetaProgramLocation>[] = []; // don't call super.buildFilter();

    // Filter by status
    if (this.statusId) {
      target.push((entity) => this.statusId === entity.monitoringLocation?.statusId);
    }

    // Filter by text search
    const searchTextFilter = EntityUtils.searchTextFilter(
      ['monitoringLocation.id', 'monitoringLocation.label', 'monitoringLocation.name'],
      this.searchText
    );
    if (searchTextFilter) target.push(searchTextFilter);

    return target;
  }
}

export class MetaProgramLocationFilter extends ReferentialFilter<MetaProgramLocationFilter, MetaProgramLocationFilterCriteria, MetaProgramLocation> {
  static fromObject(source: any, opts?: any): MetaProgramLocationFilter {
    const target = new MetaProgramLocationFilter();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
  }

  criteriaFromObject(source: any, opts: any): MetaProgramLocationFilterCriteria {
    return MetaProgramLocationFilterCriteria.fromObject(source, opts);
  }
}
