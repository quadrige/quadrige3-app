import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import {
  MetaProgramLocationFilter,
  MetaProgramLocationFilterCriteria,
} from '@app/referential/meta-program/location/filter/meta-program-location.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-meta-program-location-filter-form',
  templateUrl: './meta-program-location.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramLocationFilterForm extends ReferentialCriteriaFormComponent<MetaProgramLocationFilter, MetaProgramLocationFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabelName.map((attribute) => `monitoringLocation.${attribute}`);
  }
}
