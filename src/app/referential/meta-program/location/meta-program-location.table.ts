import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { isEmptyArray, isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { filter } from 'rxjs/operators';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { MetaProgramLocationService } from '@app/referential/meta-program/location/meta-program-location.service';
import { IAddResult } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { IntReferential } from '@app/referential/model/referential.model';
import {
  MetaProgramLocationFilter,
  MetaProgramLocationFilterCriteria,
} from '@app/referential/meta-program/location/filter/meta-program-location.filter.model';
import { IMonitoringLocationAddOptions } from '@app/referential/monitoring-location/monitoring-location.add.table';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { MetaProgramLocationFilterForm } from '@app/referential/meta-program/location/filter/meta-program-location.filter.form';

@Component({
  selector: 'app-meta-program-location-table',
  templateUrl: './meta-program-location.table.html',
  standalone: true,
  imports: [ReferentialModule, MetaProgramLocationFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MetaProgramLocationService,
      useExisting: false,
    },
  ],
})
export class MetaProgramLocationTable
  extends ReferentialMemoryTable<
    MetaProgramLocation,
    number,
    MetaProgramLocationFilter,
    MetaProgramLocationFilterCriteria /*, MetaProgramLocationValidatorService*/
  >
  implements OnInit, AfterViewInit
{
  @Output() selectionChanged = new EventEmitter<MetaProgramLocation[]>();

  @Input() metaProgramId: string;
  @Input() programIds: string[];

  constructor(
    protected injector: Injector,
    protected _entityService: MetaProgramLocationService // protected validatorService: MetaProgramLocationValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['monitoringLocation.label', 'monitoringLocation.name', 'monitoringLocation.statusId', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      MetaProgramLocation,
      _entityService,
      undefined
    );

    this.titleI18n = 'REFERENTIAL.META_PROGRAM_LOCATION.TITLE';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.META_PROGRAM_LOCATION.';
    this.defaultSortBy = 'monitoringLocation.label';
    this.logPrefix = '[meta-program-location-table]';
  }

  ngOnInit() {
    this.subTable = true;
    this.inlineEdition = false;
    this.forceNoInlineEdition = true;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;

    // Listen sort change
    this.registerSubscription(
      this.onSort.subscribe(() => this.markAsLoading()) // prevent selection change on sort
    );

    // Listen end of loading to refresh total row count
    this.registerSubscription(
      this.loadingSubject.pipe(filter((value) => !value)).subscribe(() => {
        if (this.value === undefined) {
          // this will remove the 'no result' template if the selection is voluntarily empty (=== undefined)
          this.totalRowCount = null;
        }
      })
    );
  }

  async addRow(event?: Event): Promise<boolean> {
    await this.openAddModal(event);
    return false;
  }

  async openAddModal(event: Event) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }
    await this.save();
    this.selection.clear();

    const { role, data } = await this.modalService.openModal<IMonitoringLocationAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: MonitoringLocation.entityName,
        titleI18n: 'REFERENTIAL.META_PROGRAM_LOCATION.ADD',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          excludedIds: this.value.map((value) => value.monitoringLocation.id),
          programFilter: { includedIds: this.programIds },
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      this.markAsDirty();
      const newValue = this.value?.slice() || [];
      newValue.push(
        ...data.added.map(IntReferential.fromObject).map((monitoringLocation) => new MetaProgramLocation(this.metaProgramId, monitoringLocation))
      );
      await this.setSelected(newValue);
      this.selectionChanged.emit(this.value);
      await this.resetFilter();
    }
  }

  async removeSelection(event?: UIEvent) {
    if (event) {
      event.stopPropagation();
    }
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Check if can delete
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // remove from selection
    const removedIds = toDelete.map((row) => row.currentData.monitoringLocation.id);
    const newValue = this.value.slice().filter((metaProgramLocation) => !removedIds.includes(metaProgramLocation.monitoringLocation.id));

    this.markAsDirty();
    await this.setSelected(newValue, { resetFilter: false });
    this.selectionChanged.emit(this.value);
    await this.resetFilter();
  }

  async setSelected(value: MetaProgramLocation[], opts?: { resetFilter?: boolean; emitEvent?: boolean }) {
    if (!opts || opts.resetFilter !== false) {
      await this.resetFilter(undefined, { emitEvent: false });
    }
    await this.setValue(value);
  }
}
