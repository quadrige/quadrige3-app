import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { MetaProgramLocationFilter } from '@app/referential/meta-program/location/filter/meta-program-location.filter.model';

@Injectable()
export class MetaProgramLocationService extends EntitiesMemoryService<MetaProgramLocation, MetaProgramLocationFilter> {
  constructor() {
    super(MetaProgramLocation, MetaProgramLocationFilter, { sortByReplacement: { id: 'monitoringLocation.id' } });
  }
}
