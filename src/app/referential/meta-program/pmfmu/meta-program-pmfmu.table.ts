import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { MetaProgramPmfmu, MetaProgramPmfmuUtils } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { MetaProgramPmfmuService } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.service';
import { MetaProgramPmfmuValidatorService } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.validator';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { IAddResult } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { IParameterAddOptions } from '@app/referential/parameter/parameter.add.table';
import { IPmfmuAddOptions } from '@app/referential/pmfmu/pmfmu.add.table';
import { uniqueValue } from '@app/shared/utils';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { MetaProgramPmfmuFilter, MetaProgramPmfmuFilterCriteria } from '@app/referential/meta-program/pmfmu/filter/meta-program-pmfmu.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { MetaProgramPmfmuFilterForm } from '@app/referential/meta-program/pmfmu/filter/meta-program-pmfmu.filter.form';

@Component({
  selector: 'app-meta-program-pmfmu-table',
  templateUrl: './meta-program-pmfmu.table.html',
  standalone: true,
  imports: [ReferentialModule, MetaProgramPmfmuFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MetaProgramPmfmuService,
      useExisting: false,
    },
  ],
})
export class MetaProgramPmfmuTable
  extends ReferentialMemoryTable<MetaProgramPmfmu, number, MetaProgramPmfmuFilter, MetaProgramPmfmuFilterCriteria, MetaProgramPmfmuValidatorService>
  implements OnInit, AfterViewInit
{
  @Input() metaProgramId: string;
  @Input() programIds: string[];

  constructor(
    protected injector: Injector,
    protected _entityService: MetaProgramPmfmuService,
    protected validatorService: MetaProgramPmfmuValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['pmfmuId', 'parameter', 'matrix', 'fraction', 'method', 'unit', 'updateDate']).concat(RESERVED_END_COLUMNS),
      MetaProgramPmfmu,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.META_PROGRAM_PMFMU.TITLE';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.META_PROGRAM_PMFMU.';
    this.defaultSortBy = 'parameter';
    this.logPrefix = '[meta-program-pmfmu-table]';
  }

  ngOnInit() {
    this.subTable = true;

    super.ngOnInit();

    // Parameter combo
    this.registerAutocompleteField('parameter', {
      ...this.referentialOptions.parameter,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Parameter',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Matrix combo
    this.registerAutocompleteField('matrix', {
      ...this.referentialOptions.matrix,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Matrix',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
    });

    // Fraction combo
    this.registerAutocompleteField('fraction', {
      ...this.referentialOptions.fraction,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Fraction',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
    });

    // Method combo
    this.registerAutocompleteField('method', {
      ...this.referentialOptions.method,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Method',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Unit combo
    this.registerAutocompleteField('unit', {
      ...this.referentialOptions.unit,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Unit',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    this.registerSubscription(
      this.registerCellValueChanges('matrix').subscribe((matrix) => (this.autocompleteFields.fraction.filter.criterias[0].parentId = matrix?.id))
    );

    this.registerSubscription(
      this.registerCellValueChanges('fraction').subscribe((fraction) => (this.autocompleteFields.matrix.filter.criterias[0].parentId = fraction?.id))
    );
  }

  async openAddPmfmuModal(event: MouseEvent) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IPmfmuAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: Pmfmu.entityName,
        titleI18n: 'REFERENTIAL.META_PROGRAM_PMFMU.ADD_PMFMU',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          programFilter: { includedIds: this.programIds },
          // parameterFilter: { excludedIds: this.existingParameterIds() }, // Filter removed because not consistent (Mantis #61215)
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      await this.save();
      const newValue = this.value?.slice() || [];
      const toAdd = data.added
        .map(Pmfmu.fromObject)
        .map(
          (pmfmu: Pmfmu) =>
            new MetaProgramPmfmu(this.metaProgramId, pmfmu.parameter, pmfmu.matrix, pmfmu.fraction, pmfmu.method, pmfmu.unit, pmfmu.id)
        );
      // Set new values (excluding already present PMFMU)
      newValue.push(...toAdd.filter((valueToAdd) => !newValue.find((value) => MetaProgramPmfmuUtils.samePmfmu(value, valueToAdd))));
      await this.setValue(newValue);
      this.markAsDirty();
    }
  }

  async openAddParameterModal(event: MouseEvent) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IParameterAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: Parameter.entityName,
        titleI18n: 'REFERENTIAL.META_PROGRAM_PMFMU.ADD_PARAMETER',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          programFilter: { includedIds: this.programIds },
          excludedIds: this.existingParameterIds(),
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      await this.save();
      const newValue = this.value?.slice() || [];
      const toAdd = data.added.map(Parameter.fromObject).map((parameter) => new MetaProgramPmfmu(this.metaProgramId, parameter));
      // Set new values (excluding already present (alone) parameter
      newValue.push(...toAdd.filter((valueToAdd) => !newValue.find((value) => MetaProgramPmfmuUtils.samePmfmu(value, valueToAdd))));
      await this.setValue(newValue);
      this.markAsDirty();
    }
  }

  // protected methods

  protected existingParameterIds(): string[] {
    return this.value.map((value) => value.parameter.id.toString()).filter(uniqueValue);
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('pmfmuId', 'parameter');
  }
}
