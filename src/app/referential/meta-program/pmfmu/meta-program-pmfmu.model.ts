import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { Parameter } from '@app/referential/parameter/parameter.model';

@EntityClass({ typename: 'MetaProgramPmfmuVO' })
export class MetaProgramPmfmu extends Referential<MetaProgramPmfmu> {
  static entityName = 'MetaProgramPmfmu';
  static fromObject: (source: any, opts?: any) => MetaProgramPmfmu;

  metaProgramId: string = null;
  pmfmuId?: number = null;
  parameter: Parameter = null;
  matrix?: IntReferential = null;
  fraction?: IntReferential = null;
  method?: IntReferential = null;
  unit?: IntReferential = null;

  constructor(
    metaProgramId?: string,
    parameter?: Parameter,
    matrix?: IntReferential,
    fraction?: IntReferential,
    method?: IntReferential,
    unit?: IntReferential,
    pmfmuId?: number
  ) {
    super(MetaProgramPmfmu.TYPENAME);
    this.entityName = MetaProgramPmfmu.entityName;
    this.metaProgramId = metaProgramId;
    this.parameter = parameter;
    this.matrix = matrix;
    this.fraction = fraction;
    this.method = method;
    this.unit = unit;
    this.pmfmuId = pmfmuId;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = MetaProgramPmfmu.entityName;
    this.metaProgramId = source.metaProgramId;
    this.pmfmuId = source.pmfmuId;
    this.parameter = Parameter.fromObject(source.parameter);
    this.matrix = IntReferential.fromObject(source.matrix);
    this.fraction = IntReferential.fromObject(source.fraction);
    this.method = IntReferential.fromObject(source.method);
    this.unit = IntReferential.fromObject(source.unit);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parameter = EntityUtils.asMinifiedObject(this.parameter, opts);
    target.matrix = EntityUtils.asMinifiedObject(this.matrix, opts);
    target.fraction = EntityUtils.asMinifiedObject(this.fraction, opts);
    target.method = EntityUtils.asMinifiedObject(this.method, opts);
    target.unit = EntityUtils.asMinifiedObject(this.unit, opts);
    delete target.entityName;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.statusId;
    return target;
  }
}

export class MetaProgramPmfmuUtils {
  static samePmfmu(m1: MetaProgramPmfmu, m2: MetaProgramPmfmu): boolean {
    return EntityUtils.deepEquals(m1, m2, ['parameter', 'matrix', 'fraction', 'method', 'unit']);
  }
}
