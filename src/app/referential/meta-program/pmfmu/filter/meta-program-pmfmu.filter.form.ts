import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { MetaProgramPmfmuFilter, MetaProgramPmfmuFilterCriteria } from '@app/referential/meta-program/pmfmu/filter/meta-program-pmfmu.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-meta-program-pmfmu-filter-form',
  templateUrl: './meta-program-pmfmu.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramPmfmuFilterForm extends ReferentialCriteriaFormComponent<MetaProgramPmfmuFilter, MetaProgramPmfmuFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected criteriaToQueryParams(criteria: MetaProgramPmfmuFilterCriteria): PartialRecord<keyof MetaProgramPmfmuFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'parameterFilter');
    this.subCriteriaToQueryParams(params, criteria, 'matrixFilter');
    this.subCriteriaToQueryParams(params, criteria, 'fractionFilter');
    this.subCriteriaToQueryParams(params, criteria, 'methodFilter');
    this.subCriteriaToQueryParams(params, criteria, 'unitFilter');
    return params;
  }
}
