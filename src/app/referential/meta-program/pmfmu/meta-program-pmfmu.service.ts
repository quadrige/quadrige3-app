import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { MetaProgramPmfmu } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { MetaProgramPmfmuFilter } from '@app/referential/meta-program/pmfmu/filter/meta-program-pmfmu.filter.model';

@Injectable()
export class MetaProgramPmfmuService extends EntitiesMemoryService<MetaProgramPmfmu, MetaProgramPmfmuFilter> {
  constructor() {
    super(MetaProgramPmfmu, MetaProgramPmfmuFilter);
  }
}
