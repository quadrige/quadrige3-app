import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { MetaProgramLocationTable } from '@app/referential/meta-program/location/meta-program-location.table';
import { MetaProgramPmfmuTable } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.table';
import { MetaProgramLocationPmfmuTable } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.table';
import { MetaProgramLocationPmfmuAddModal } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.add.modal';
import { MetaProgramTable } from '@app/referential/meta-program/meta-program.table';
import { MetaProgramFilterForm } from '@app/referential/meta-program/filter/meta-program.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';

@NgModule({
  imports: [
    ReferentialModule,
    MetaProgramFilterForm,
    MetaProgramLocationTable,
    MetaProgramPmfmuTable,
    MetaProgramLocationPmfmuTable,
    GenericSelectTable,
    DepartmentSelectTable,
    UserSelectTable,
  ],
  declarations: [MetaProgramLocationPmfmuAddModal, MetaProgramTable],
  exports: [MetaProgramTable],
})
export class MetaProgramModule {}
