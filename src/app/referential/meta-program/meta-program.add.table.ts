import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { MetaProgramValidatorService } from '@app/referential/meta-program/meta-program.validator';
import { MetaProgramService } from '@app/referential/meta-program/meta-program.service';
import { MetaProgramTable } from '@app/referential/meta-program/meta-program.table';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { MetaProgramFilter } from '@app/referential/meta-program/filter/meta-program.filter.model';
import { MetaProgram } from '@app/referential/meta-program/meta-program.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { MetaProgramFilterForm } from '@app/referential/meta-program/filter/meta-program.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { MetaProgramLocationTable } from '@app/referential/meta-program/location/meta-program-location.table';
import { MetaProgramPmfmuTable } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.table';
import { MetaProgramLocationPmfmuTable } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';

@Component({
  selector: 'app-meta-program-add-table',
  templateUrl: './meta-program.table.html',
  standalone: true,
  imports: [
    ReferentialModule,
    MetaProgramFilterForm,
    MetaProgramLocationTable,
    MetaProgramPmfmuTable,
    MetaProgramLocationPmfmuTable,
    GenericSelectTable,
    DepartmentSelectTable,
    UserSelectTable,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramAddTable extends MetaProgramTable implements IAddTable<MetaProgram> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: MetaProgramService,
    protected validatorService: MetaProgramValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[meta-program-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: MetaProgramFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'description');
  }
}
