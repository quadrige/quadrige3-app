import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { MetaProgramPmfmu } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { MetaProgramLocationPmfmu } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.model';

@EntityClass({ typename: 'MetaProgramVO' })
export class MetaProgram extends Referential<MetaProgram, string> {
  static entityName = 'MetaProgram';
  static fromObject: (source: any, opts?: any) => MetaProgram;

  programIds: string[] = null;
  responsibleDepartmentIds: number[] = null;
  responsibleUserIds: number[] = null;
  metaProgramLocations: MetaProgramLocation[] = null;
  metaProgramPmfmus: MetaProgramPmfmu[] = null;
  metaProgramLocationPmfmus: MetaProgramLocationPmfmu[] = null;
  detailLoaded = false;

  constructor() {
    super(MetaProgram.TYPENAME);
    this.entityName = MetaProgram.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = MetaProgram.entityName;
    this.programIds = source.programIds || [];
    this.responsibleDepartmentIds = source.responsibleDepartmentIds || [];
    this.responsibleUserIds = source.responsibleUserIds || [];
    this.metaProgramLocations = source.metaProgramLocations?.map(MetaProgramLocation.fromObject) || [];
    this.metaProgramPmfmus = source.metaProgramPmfmus?.map(MetaProgramPmfmu.fromObject) || [];
    this.metaProgramLocationPmfmus = source.metaProgramLocationPmfmus?.map(MetaProgramLocationPmfmu.fromObject) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);

    if (!opts || opts.minify !== false) {
      // Split associations into metaProgramLocations
      this.metaProgramLocations?.forEach((metaProgramLocation) => {
        metaProgramLocation.metaProgramPmfmus = this.metaProgramLocationPmfmus
          ?.filter((metaProgramLocationPmfmu) => this.ownedBy(metaProgramLocation, metaProgramLocationPmfmu))
          .map((value) => MetaProgramPmfmu.fromObject(value.metaProgramPmfmu));
      });
      delete target.metaProgramLocationPmfmus;
    } else {
      target.metaProgramLocationPmfmus = this.metaProgramLocationPmfmus?.map((value) => value.asObject(opts));
    }
    target.metaProgramLocations = this.metaProgramLocations?.map((value) => value.asObject(opts)) || undefined;
    target.metaProgramPmfmus = this.metaProgramPmfmus?.map((value) => value.asObject(opts)) || undefined;
    delete target.detailLoaded;
    return target;
  }

  protected ownedBy(metaProgramLocation: MetaProgramLocation, metaProgramLocationPmfmu: MetaProgramLocationPmfmu): boolean {
    // if (metaProgramLocation.id && metaProgramLocationPmfmu.metaProgramLocation.id) {
    //   return metaProgramLocation.id === metaProgramLocationPmfmu.metaProgramLocation.id;
    // }
    return (
      (!!metaProgramLocation.id &&
        !!metaProgramLocationPmfmu.metaProgramLocation.id &&
        metaProgramLocation.id === metaProgramLocationPmfmu.metaProgramLocation.id) ||
      metaProgramLocation.monitoringLocation.id === metaProgramLocationPmfmu.metaProgramLocation.monitoringLocation.id
    );
  }
}
