import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { MetaProgramModule } from '@app/referential/meta-program/meta-program.module';
import { MetaProgramTable } from '@app/referential/meta-program/meta-program.table';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MetaProgramTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), MetaProgramModule],
})
export class MetaProgramRoutingModule {}
