import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { MetaProgramValidatorService } from '@app/referential/meta-program/meta-program.validator';
import { MetaProgramService } from '@app/referential/meta-program/meta-program.service';
import { EntityUtils } from '@app/shared/entity.utils';
import { MetaProgramTable } from '@app/referential/meta-program/meta-program.table';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { ISelectTable } from '@app/shared/table/table.model';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { MetaProgramFilter } from '@app/referential/meta-program/filter/meta-program.filter.model';
import { MetaProgram } from '@app/referential/meta-program/meta-program.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { MetaProgramFilterForm } from '@app/referential/meta-program/filter/meta-program.filter.form';
import { MetaProgramLocationTable } from '@app/referential/meta-program/location/meta-program-location.table';
import { MetaProgramPmfmuTable } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.table';
import { MetaProgramLocationPmfmuTable } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.table';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';

@Component({
  selector: 'app-meta-program-select-table',
  templateUrl: './meta-program.table.html',
  standalone: true,
  imports: [
    ReferentialModule,
    MetaProgramFilterForm,
    MetaProgramLocationTable,
    MetaProgramPmfmuTable,
    MetaProgramLocationPmfmuTable,
    GenericSelectTable,
    DepartmentSelectTable,
    UserSelectTable,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramSelectTable extends MetaProgramTable implements ISelectTable<MetaProgram> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria;
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();

  constructor(
    protected injector: Injector,
    protected _entityService: MetaProgramService,
    protected validatorService: MetaProgramValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[meta-program-select-table]';
    this.selectTable = true;
  }

  async resetFilter(filter?: MetaProgramFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit();
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete).map((id) => id.toString());
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    await this.resetFilter();
  }

  updatePermission() {
    // Don't update permissions
  }

  protected refreshButtons() {
    this.tableButtons.canAdd = true;
    this.tableButtons.canDuplicate = false;
    this.tableButtons.canDelete = true;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'description');
  }
}
