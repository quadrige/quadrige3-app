import { Injectable, Injector } from '@angular/core';
import { isNil } from '@sumaris-net/ngx-components';
import {
  BaseReferentialService,
  defaultReferentialSaveOption,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { MetaProgram } from '@app/referential/meta-program/meta-program.model';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { MetaProgramPmfmu } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { parameterFragments } from '@app/referential/parameter/parameter.service';
import { jobFragments } from '@app/social/job/job.service';
import { MetaProgramFilter, MetaProgramFilterCriteria } from '@app/referential/meta-program/filter/meta-program.filter.model';

const metaProgramPmfmuFragment = gql`
  fragment MetaProgramPmfmuFragment on MetaProgramPmfmuVO {
    id
    metaProgramId
    pmfmuId
    parameter {
      ...ParameterFragment
    }
    matrix {
      ...ReferentialFragment
    }
    fraction {
      ...ReferentialFragment
    }
    method {
      ...ReferentialFragment
    }
    unit {
      ...ReferentialFragment
    }
    updateDate
    __typename
  }
  ${parameterFragments.parameter}
  ${referentialFragments.light}
`;

const metaProgramLocationFragments = {
  read: gql`
    fragment MetaProgramLocationReadFragment on MetaProgramLocationVO {
      id
      metaProgramId
      monitoringLocation {
        ...ReferentialFragment
      }
      metaProgramPmfmuIds
      updateDate
      __typename
    }
    ${referentialFragments.light}
  `,
  write: gql`
    fragment MetaProgramLocationWriteFragment on MetaProgramLocationVO {
      id
      metaProgramId
      monitoringLocation {
        ...ReferentialFragment
      }
      metaProgramPmfmus {
        ...MetaProgramPmfmuFragment
      }
      updateDate
      __typename
    }
    ${metaProgramPmfmuFragment}
    ${referentialFragments.light}
  `,
};

export const metaProgramFragments = {
  read: gql`
    fragment MetaProgramReadFragment on MetaProgramVO {
      id
      name
      description
      responsibleDepartmentIds
      responsibleUserIds
      programIds
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
  write: gql`
    fragment MetaProgramWriteFragment on MetaProgramVO {
      id
      name
      description
      responsibleDepartmentIds
      responsibleUserIds
      programIds
      metaProgramLocations {
        ...MetaProgramLocationWriteFragment
      }
      metaProgramPmfmus {
        ...MetaProgramPmfmuFragment
      }
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
    ${metaProgramLocationFragments.write}
    ${metaProgramPmfmuFragment}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  load: gql`
    query MetaProgram($id: String) {
      data: metaProgram(id: $id) {
        ...MetaProgramReadFragment
      }
    }
    ${metaProgramFragments.read}
  `,

  loadAll: gql`
    query MetaPrograms($page: PageInput, $filter: MetaProgramFilterVOInput) {
      data: metaPrograms(page: $page, filter: $filter) {
        ...MetaProgramReadFragment
      }
    }
    ${metaProgramFragments.read}
  `,

  loadAllWithTotal: gql`
    query MetaProgramsWithTotal($page: PageInput, $filter: MetaProgramFilterVOInput) {
      data: metaPrograms(page: $page, filter: $filter) {
        ...MetaProgramReadFragment
      }
      total: metaProgramsCount(filter: $filter)
    }
    ${metaProgramFragments.read}
  `,

  countAll: gql`
    query MetaProgramsCount($filter: MetaProgramFilterVOInput) {
      total: metaProgramsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportMetaProgramsAsync($filter: MetaProgramFilterVOInput, $context: ExportContextInput) {
      data: exportMetaProgramsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveMetaPrograms($data: [MetaProgramVOInput], $options: MetaProgramSaveOptionsInput) {
      data: saveMetaPrograms(metaPrograms: $data, options: $options) {
        ...MetaProgramWriteFragment
      }
    }
    ${metaProgramFragments.write}
  `,

  deleteAll: gql`
    mutation DeleteMetaPrograms($ids: [String]) {
      deleteMetaPrograms(ids: $ids)
    }
  `,
};

const otherQueries = {
  locations: gql`
    query MetaProgramLocations($metaProgramId: String) {
      data: metaProgramLocations(metaProgramId: $metaProgramId) {
        ...MetaProgramLocationReadFragment
      }
    }
    ${metaProgramLocationFragments.read}
  `,
  pmfmus: gql`
    query MetaProgramPmfmus($metaProgramId: String) {
      data: metaProgramPmfmus(metaProgramId: $metaProgramId) {
        ...MetaProgramPmfmuFragment
      }
    }
    ${metaProgramPmfmuFragment}
  `,
};

const defaultSaveOptions = {
  ...defaultReferentialSaveOption,
  withLocationsAndPmfmus: true,
  withPrivileges: true,
};

@Injectable({ providedIn: 'root' })
export class MetaProgramService extends BaseReferentialService<MetaProgram, MetaProgramFilter, MetaProgramFilterCriteria, string> {
  constructor(protected injector: Injector) {
    super(injector, MetaProgram, MetaProgramFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[meta-program-service]';
  }

  async saveAll(entities: MetaProgram[], opts?: EntitySaveOptions): Promise<MetaProgram[]> {
    return super.saveAll(entities, { ...defaultSaveOptions, ...opts });
  }

  async save(entity: MetaProgram, opts?: EntitySaveOptions): Promise<MetaProgram> {
    return super.save(entity, { ...defaultSaveOptions, ...opts });
  }

  async loadLocations(metaProgramId: string): Promise<MetaProgramLocation[]> {
    if (isNil(metaProgramId)) {
      return [];
    }
    const variables: any = { metaProgramId };
    if (this._debug) console.debug(`[meta-program-service] Loading locations...`, variables);
    const now = Date.now();
    const query = otherQueries.locations;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(MetaProgramLocation.fromObject);
    if (this._debug) console.debug(`[meta-program-service] locations loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadPmfmus(metaProgramId: string): Promise<MetaProgramPmfmu[]> {
    if (isNil(metaProgramId)) {
      return [];
    }
    const variables: any = { metaProgramId };
    if (this._debug) console.debug(`[meta-program-service] Loading pmfmus...`, variables);
    const now = Date.now();
    const query = otherQueries.pmfmus;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(MetaProgramPmfmu.fromObject);
    if (this._debug) console.debug(`[meta-program-service] pmfmus loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
