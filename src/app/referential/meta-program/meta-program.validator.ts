import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { MetaProgram } from '@app/referential/meta-program/meta-program.model';
import { MetaProgramService } from '@app/referential/meta-program/meta-program.service';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class MetaProgramValidatorService extends ReferentialValidatorService<MetaProgram, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: MetaProgramService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: MetaProgram, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        // ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data), Mantis #56901
      ],
      description: [data?.description || null, Validators.compose([Validators.required, Validators.maxLength(lengthDescription)])],
      responsibleDepartmentIds: [data?.responsibleDepartmentIds || null],
      responsibleUserIds: [data?.responsibleUserIds || null],
      programIds: [data?.programIds || null],
      metaProgramLocations: [data?.metaProgramLocations || null],
      metaProgramPmfmus: [data?.metaProgramPmfmus || null],
      metaProgramLocationPmfmus: [data?.metaProgramLocationPmfmus || null],
      detailLoaded: [toBoolean(data?.detailLoaded, false)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: MetaProgram, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredOneArrayNotEmpty(
          ['responsibleDepartmentIds', 'responsibleUserIds'],
          'REFERENTIAL.META_PROGRAM.ERROR.MISSING_MANAGER'
        ),
      ],
    };
  }
}
