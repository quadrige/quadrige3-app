import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { MetaProgramLocationPmfmu } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.model';
import { MetaProgramPmfmuUtils } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { MetaProgramLocationPmfmuFilter } from '@app/referential/meta-program/location-pmfmu/filter/meta-program-location-pmfmu.filter.model';

@Injectable({ providedIn: 'root' })
export class MetaProgramLocationPmfmuService extends EntitiesMemoryService<MetaProgramLocationPmfmu, MetaProgramLocationPmfmuFilter, string> {
  constructor() {
    super(MetaProgramLocationPmfmu, MetaProgramLocationPmfmuFilter);
  }

  equals(d1: MetaProgramLocationPmfmu, d2: MetaProgramLocationPmfmu): boolean {
    return (
      d1.metaProgramLocation?.monitoringLocation.id === d2.metaProgramLocation?.monitoringLocation.id &&
      MetaProgramPmfmuUtils.samePmfmu(d1.metaProgramPmfmu, d2.metaProgramPmfmu)
    );
  }
}
