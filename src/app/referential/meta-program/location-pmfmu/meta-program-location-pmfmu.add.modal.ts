import { ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { MetaProgramLocationTable } from '@app/referential/meta-program/location/meta-program-location.table';
import { MetaProgramPmfmuTable } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.table';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { MetaProgramPmfmu } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { MetaProgramLocationPmfmu } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.model';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IMetaProgramLocationPmfmuAddModalOptions extends IModalOptions {
  metaProgramLocations: MetaProgramLocation[];
  metaProgramPmfmus: MetaProgramPmfmu[];
}

@Component({
  selector: 'app-meta-program-location-pmfmu-add-modal',
  templateUrl: './meta-program-location-pmfmu.add.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramLocationPmfmuAddModal extends ModalComponent<MetaProgramLocationPmfmu[]> implements IMetaProgramLocationPmfmuAddModalOptions {
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  @ViewChild('locationTable') locationTable: MetaProgramLocationTable;
  @ViewChild('pmfmuTable') pmfmuTable: MetaProgramPmfmuTable;

  @Input() metaProgramLocations: MetaProgramLocation[];
  @Input() metaProgramPmfmus: MetaProgramPmfmu[];

  readonly pmfmuStepIndex = 1;

  constructor(protected injector: Injector) {
    super(injector);
  }

  get locationTableValid(): boolean {
    return !this.loading && !this.locationTable?.selection.isEmpty();
  }

  get pmfmuTableValid(): boolean {
    return !this.loading && !this.pmfmuTable?.selection.isEmpty();
  }

  get canGoNext() {
    return this.stepper.selected?.completed;
  }

  get canValidate() {
    return this.stepper.selectedIndex === this.pmfmuStepIndex;
  }

  get disabled(): boolean {
    return false;
  }

  protected async afterInit(): Promise<void> {
    this.locationTable.toolbarColor = 'secondary900';
    this.pmfmuTable.toolbarColor = 'secondary900';
    // Load locations and pmfmus
    await this.locationTable.setValue(this.metaProgramLocations);
    await this.pmfmuTable.setValue(this.metaProgramPmfmus);
  }

  async onNext(event: any) {
    event?.stopPropagation();
    this.stepper.next();
  }

  protected dataToValidate(): Promise<MetaProgramLocationPmfmu[]> | MetaProgramLocationPmfmu[] {
    // Build association results
    const associations: MetaProgramLocationPmfmu[] = [];

    this.locationTable.selection.selected
      .map((row) => row.currentData)
      .forEach((metaProgramLocation) => {
        this.pmfmuTable.selection.selected
          .map((row) => row.currentData)
          .forEach((metaProgramPmfmu) => {
            const association = new MetaProgramLocationPmfmu();
            association.metaProgramLocation = metaProgramLocation;
            association.metaProgramPmfmu = metaProgramPmfmu;
            associations.push(association);
          });
      });

    return associations;
  }
}
