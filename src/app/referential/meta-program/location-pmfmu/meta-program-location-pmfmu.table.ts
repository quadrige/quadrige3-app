import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { isEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { MetaProgramLocationPmfmu } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.model';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { MetaProgramPmfmu, MetaProgramPmfmuUtils } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { MetaProgramLocationPmfmuService } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.service';
import {
  IMetaProgramLocationPmfmuAddModalOptions,
  MetaProgramLocationPmfmuAddModal,
} from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.add.modal';
import {
  MetaProgramLocationPmfmuFilter,
  MetaProgramLocationPmfmuFilterCriteria,
} from '@app/referential/meta-program/location-pmfmu/filter/meta-program-location-pmfmu.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { MetaProgramLocationPmfmuFilterForm } from '@app/referential/meta-program/location-pmfmu/filter/meta-program-location-pmfmu.filter.form';

@Component({
  selector: 'app-meta-program-location-pmfmu-table',
  templateUrl: './meta-program-location-pmfmu.table.html',
  standalone: true,
  imports: [ReferentialModule, MetaProgramLocationPmfmuFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramLocationPmfmuTable
  extends ReferentialMemoryTable<MetaProgramLocationPmfmu, string, MetaProgramLocationPmfmuFilter, MetaProgramLocationPmfmuFilterCriteria>
  implements OnInit, AfterViewInit
{
  metaProgramLocations: MetaProgramLocation[];
  metaProgramPmfmus: MetaProgramPmfmu[];

  constructor(
    protected injector: Injector,
    protected _entityService: MetaProgramLocationPmfmuService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'metaProgramLocation.monitoringLocation',
        'metaProgramPmfmu.parameter',
        'metaProgramPmfmu.matrix',
        'metaProgramPmfmu.fraction',
        'metaProgramPmfmu.method',
        'metaProgramPmfmu.unit',
      ]).concat(RESERVED_END_COLUMNS),
      MetaProgramLocationPmfmu,
      _entityService,
      undefined
    );

    this.titleI18n = 'REFERENTIAL.META_PROGRAM_LOCATION_PMFMU.TITLE';
    this.i18nColumnPrefix = 'REFERENTIAL.META_PROGRAM_LOCATION_PMFMU.';
    this.defaultSortBy = 'metaProgramLocation.monitoringLocation'; // should be 'metaProgramLocation.monitoringLocation','metaProgramPmfmu.parameter','metaProgramPmfmu.matrix','metaProgramPmfmu.fraction','metaProgramPmfmu.method','metaProgramPmfmu.unit'
    this.logPrefix = '[meta-program-location-pmfmu-table]';
  }

  ngOnInit() {
    this.showTitle = false;
    this.subTable = true;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;
  }

  async addRow(event?: Event): Promise<boolean> {
    await this.openAddModal(event);
    return false;
  }

  async openAddModal(event: Event) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }
    await this.save();
    // clear selection
    this.selection.clear();

    const { role, data } = await this.modalService.openModal<IMetaProgramLocationPmfmuAddModalOptions, MetaProgramLocationPmfmu[]>(
      MetaProgramLocationPmfmuAddModal,
      {
        metaProgramLocations: this.metaProgramLocations,
        metaProgramPmfmus: this.metaProgramPmfmus,
      },
      'modal-large'
    );
    if (role === 'validate' && data) {
      const newSelected = this.value?.slice() || [];
      const associationsToAdd: MetaProgramLocationPmfmu[] = [];
      // Find if associations already exists
      data.forEach((association: MetaProgramLocationPmfmu) => {
        const existing = newSelected.find(
          (row) => row.metaProgramLocation.id === association.metaProgramLocation.id && row.metaProgramPmfmu.id === association.metaProgramPmfmu.id
        );
        if (!existing) {
          associationsToAdd.push(association);
        }
      });
      // Add new associations
      if (associationsToAdd.length) {
        newSelected.push(...associationsToAdd);
      }

      await this.resetFilter(undefined, { emitEvent: false });
      // affect values and mark as dirty !
      await this.setValue(newSelected);
      this.markAsDirty();
    }
  }

  async cleanOrphanRows(): Promise<MetaProgramLocationPmfmu[]> {
    if (isEmptyArray(this.value)) return;
    if (this.debug) console.debug(`${this.logPrefix} Clean orphan rows`);

    const monitoringLocationIds = this.metaProgramLocations?.map((value) => value.monitoringLocation.id) || [];
    const values = this.value
      .slice()
      .filter(
        (value) =>
          monitoringLocationIds.includes(value.metaProgramLocation.monitoringLocation.id) &&
          !!this.metaProgramPmfmus.find((pmfmu) => MetaProgramPmfmuUtils.samePmfmu(pmfmu, value.metaProgramPmfmu))
      );
    await this.setValue(values);
    return values;
  }
}
