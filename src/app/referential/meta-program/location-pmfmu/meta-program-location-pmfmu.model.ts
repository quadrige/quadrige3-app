import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { MetaProgramPmfmu } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';

@EntityClass({ typename: 'MetaProgramLocationPmfmuVO' })
export class MetaProgramLocationPmfmu extends Referential<MetaProgramLocationPmfmu, string> {
  static entityName = 'MetaProgramLocationPmfmu';
  static fromObject: (source: any, opts?: any) => MetaProgramLocationPmfmu;

  metaProgramLocation: MetaProgramLocation = null;
  metaProgramPmfmu: MetaProgramPmfmu = null;

  constructor(metaProgramLocation?: MetaProgramLocation, metaProgramPmfmu?: MetaProgramPmfmu) {
    super(MetaProgramLocationPmfmu.TYPENAME);
    this.entityName = MetaProgramLocationPmfmu.entityName;
    this.metaProgramLocation = metaProgramLocation;
    this.metaProgramPmfmu = metaProgramPmfmu;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = MetaProgramLocationPmfmu.entityName;
    this.metaProgramLocation = MetaProgramLocation.fromObject(source.metaProgramLocation);
    this.metaProgramPmfmu = MetaProgramPmfmu.fromObject(source.metaProgramPmfmu);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.metaProgramLocation = this.metaProgramLocation?.asObject(opts);
    target.metaProgramPmfmu = this.metaProgramPmfmu?.asObject(opts);
    if (opts?.keepEntityName !== true) {
      delete target.entityName;
    }
    delete target.statusId;
    return target;
  }
}
