import { EntityAsObjectOptions, FilterFn } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { MetaProgramLocationPmfmu } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export class MetaProgramLocationPmfmuFilterCriteria extends ReferentialFilterCriteria<MetaProgramLocationPmfmu, string> {
  monitoringLocationFilter: IntReferentialFilterCriteria = null;
  parameterFilter: StrReferentialFilterCriteria = null;

  static fromObject(source: any, opts?: any): MetaProgramLocationPmfmuFilterCriteria {
    const target = new MetaProgramLocationPmfmuFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.parameterFilter = StrReferentialFilterCriteria.fromObject(source.parameterFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    target.parameterFilter = this.parameterFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    if (key === 'parameterFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  protected buildFilter(): FilterFn<MetaProgramLocationPmfmu>[] {
    const target = super.buildFilter();

    if (this.monitoringLocationFilter) {
      const filter = this.monitoringLocationFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.metaProgramLocation.monitoringLocation));
    }

    if (this.parameterFilter) {
      const filter = this.parameterFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.metaProgramPmfmu.parameter));
    }

    return target;
  }
}

export class MetaProgramLocationPmfmuFilter extends ReferentialFilter<
  MetaProgramLocationPmfmuFilter,
  MetaProgramLocationPmfmuFilterCriteria,
  MetaProgramLocationPmfmu,
  string
> {
  static fromObject(source: any, opts?: any): MetaProgramLocationPmfmuFilter {
    const target = new MetaProgramLocationPmfmuFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): MetaProgramLocationPmfmuFilterCriteria {
    return MetaProgramLocationPmfmuFilterCriteria.fromObject(source, opts);
  }
}
