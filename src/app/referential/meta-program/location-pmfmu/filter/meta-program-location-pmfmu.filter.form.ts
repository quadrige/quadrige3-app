import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import {
  MetaProgramLocationPmfmuFilter,
  MetaProgramLocationPmfmuFilterCriteria,
} from '@app/referential/meta-program/location-pmfmu/filter/meta-program-location-pmfmu.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-meta-program-location-pmfmu-filter-form',
  templateUrl: './meta-program-location-pmfmu.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramLocationPmfmuFilterForm extends ReferentialCriteriaFormComponent<
  MetaProgramLocationPmfmuFilter,
  MetaProgramLocationPmfmuFilterCriteria
> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected criteriaToQueryParams(
    criteria: MetaProgramLocationPmfmuFilterCriteria
  ): PartialRecord<keyof MetaProgramLocationPmfmuFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    this.subCriteriaToQueryParams(params, criteria, 'parameterFilter');
    return params;
  }
}
