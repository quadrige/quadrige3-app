import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { MetaProgram } from '@app/referential/meta-program/meta-program.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'MetaProgramFilterCriteriaVO' })
export class MetaProgramFilterCriteria extends ReferentialFilterCriteria<MetaProgram, string> {
  static fromObject: (source: any, opts?: any) => MetaProgramFilterCriteria;

  programFilter: StrReferentialFilterCriteria = null;
  monitoringLocationFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(MetaProgramFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'MetaProgramFilterVO' })
export class MetaProgramFilter extends ReferentialFilter<MetaProgramFilter, MetaProgramFilterCriteria, MetaProgram, string> {
  static fromObject: (source: any, opts?: any) => MetaProgramFilter;

  constructor() {
    super(MetaProgramFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): MetaProgramFilterCriteria {
    return MetaProgramFilterCriteria.fromObject(source, opts);
  }
}
