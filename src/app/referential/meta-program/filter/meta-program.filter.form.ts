import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { MetaProgramFilter, MetaProgramFilterCriteria } from '@app/referential/meta-program/filter/meta-program.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-meta-program-filter-form',
  templateUrl: './meta-program.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramFilterForm extends ReferentialCriteriaFormComponent<MetaProgramFilter, MetaProgramFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idNameDescription;
  }

  protected criteriaToQueryParams(criteria: MetaProgramFilterCriteria): PartialRecord<keyof MetaProgramFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    return params;
  }
}
