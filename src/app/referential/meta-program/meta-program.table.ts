import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import {
  isEmptyArray,
  isNilOrBlank,
  isNotEmptyArray,
  PromiseEvent,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { MatTabNav } from '@angular/material/tabs';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { MetaProgram } from '@app/referential/meta-program/meta-program.model';
import { MetaProgramValidatorService } from '@app/referential/meta-program/meta-program.validator';
import { MetaProgramPmfmuTable } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.table';
import { MetaProgramLocationTable } from '@app/referential/meta-program/location/meta-program-location.table';
import { MetaProgramLocationPmfmuTable } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.table';
import { MetaProgramService } from '@app/referential/meta-program/meta-program.service';
import { MetaProgramLocationPmfmu } from '@app/referential/meta-program/location-pmfmu/meta-program-location-pmfmu.model';
import { MetaProgramPmfmu } from '@app/referential/meta-program/pmfmu/meta-program-pmfmu.model';
import { MetaProgramLocation } from '@app/referential/meta-program/location/meta-program-location.model';
import { Alerts } from '@app/shared/alerts';
import { attributes } from '@app/referential/model/referential.constants';
import { entityId, EntityUtils } from '@app/shared/entity.utils';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { MetaProgramFilter, MetaProgramFilterCriteria } from '@app/referential/meta-program/filter/meta-program.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ExportOptions, IRightSelectTable } from '@app/shared/table/table.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { IAddResult, ISelectModalOptions } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

type MetaProgramRightView = 'programTable' | 'userTable' | 'departmentTable' | TranscribingItemView;
type MetaProgramDetailView = 'locationTable' | 'pmfmuTable' | 'locationPmfmuTable';

@Component({
  selector: 'app-meta-program-table',
  templateUrl: './meta-program.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaProgramTable
  extends ReferentialTable<MetaProgram, string, MetaProgramFilter, MetaProgramFilterCriteria, MetaProgramValidatorService, MetaProgramRightView>
  implements OnInit, AfterViewInit
{
  programTable = viewChild<GenericSelectTable>('programTable');
  userTable = viewChild<UserSelectTable>('userTable');
  departmentTable = viewChild<DepartmentSelectTable>('departmentTable');

  detailAreaTabs = viewChild<MatTabNav>('detailAreaTabs');
  locationTable = viewChild<MetaProgramLocationTable>('locationTable');
  pmfmuTable = viewChild<MetaProgramPmfmuTable>('pmfmuTable');
  locationPmfmuTable = viewChild<MetaProgramLocationPmfmuTable>('locationPmfmuTable');

  readonly programMenuItem: IEntityMenuItem<MetaProgram, MetaProgramRightView> = {
    title: 'REFERENTIAL.META_PROGRAM.PROGRAMS',
    entityName: 'Program',
    attribute: 'programIds',
    view: 'programTable',
    viewTitle: 'REFERENTIAL.META_PROGRAM.PROGRAMS',
  };
  readonly managersMenuItem: IEntityMenuItem<MetaProgram, MetaProgramRightView> = {
    title: 'REFERENTIAL.RIGHT.META_PROGRAM.MANAGERS',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.USERS',
        entityName: 'User',
        attribute: 'responsibleUserIds',
        view: 'userTable',
        viewTitle: 'REFERENTIAL.META_PROGRAM.MANAGER.USER',
      },
      {
        title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
        entityName: 'Department',
        attribute: 'responsibleDepartmentIds',
        view: 'departmentTable',
        viewTitle: 'REFERENTIAL.META_PROGRAM.MANAGER.DEPARTMENT',
      },
    ],
  };
  readonly metaProgramMenuItems: IEntityMenuItem<MetaProgram, MetaProgramRightView>[] = [this.programMenuItem, this.managersMenuItem];

  rightAreaEnabled = false;

  readonly locationsMenuItem: IEntityMenuItem<MetaProgram, MetaProgramDetailView> = {
    title: 'REFERENTIAL.META_PROGRAM.LOCATIONS',
    attribute: 'metaProgramLocations',
    view: 'locationTable',
  };
  readonly pmfmusMenuItem: IEntityMenuItem<MetaProgram, MetaProgramDetailView> = {
    title: 'REFERENTIAL.META_PROGRAM.PMFMUS',
    attribute: 'metaProgramPmfmus',
    view: 'pmfmuTable',
  };
  readonly locationPmfmuMenuItem: IEntityMenuItem<MetaProgram, MetaProgramDetailView> = {
    title: 'REFERENTIAL.META_PROGRAM.PMFMU_ASSOCIATION',
    attribute: 'metaProgramLocationPmfmus',
    view: 'locationPmfmuTable',
  };
  readonly detailAreaMenuItems: IEntityMenuItem<MetaProgram, MetaProgramDetailView>[] = [
    this.locationsMenuItem,
    this.pmfmusMenuItem,
    this.locationPmfmuMenuItem,
  ];

  selectedDetailAreaMenuItem: IEntityMenuItem<MetaProgram, MetaProgramDetailView> = this.locationsMenuItem;

  detailAreaEnabled = false;

  constructor(
    protected injector: Injector,
    protected _entityService: MetaProgramService,
    protected validatorService: MetaProgramValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      MetaProgram,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.META_PROGRAM.';
    this.logPrefix = '[meta-program-table]';
    this.transcribingItemEnabled.set(false);
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: MetaProgram = this.singleSelectedRow.currentData;
      return isNotEmptyArray(data.responsibleDepartmentIds) || isNotEmptyArray(data.responsibleUserIds) || isNotEmptyArray(data.programIds);
    }
    return false;
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.restoreBottomPanel(true, 50);
  }

  async onAfterSelectionChange(row?: AsyncTableElement<MetaProgram>) {
    this.refreshButtons();
    await super.onAfterSelectionChange(row);
    await this.loadDetailArea(row);
  }

  updatePermission() {
    // Manage user rights :
    // By default, all user have edition write to allow further finer checks
    this.canEdit = true;
    // Refresh other controls
    this.refreshButtons();
    this.markForCheck();
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<MetaProgram>): Promise<boolean> {
    // following lines comes from super.editRow(event, row), don't remove
    if (!this._enabled) return false;
    if (this.singleEditingRow === row) return true; // Already the edited row
    if (event?.defaultPrevented) return false;
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // User without right to edit, don't turn on row edition
    if (!this.hasRightOnMetaProgram(row.currentData)) {
      return true;
    }

    return super.editRow(event, row);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<MetaProgram>): Promise<boolean> {
    row = row || this.singleEditingRow;
    // confirm current detail table row
    if (!(await this.confirmDetail(row))) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  // RIGHT AREA

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(true, 25);
  }

  protected getRightMenuItems(): IEntityMenuItem<MetaProgram, TranscribingItemView | MetaProgramRightView>[] {
    return this.metaProgramMenuItems;
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<MetaProgram, TranscribingItemView | MetaProgramRightView> {
    return this.programMenuItem;
  }

  async canRemoveProgram(event: PromiseEvent<boolean, { rows: AsyncTableElement<GenericReferential>[] }>) {
    if (!this.singleEditingRow) {
      throw new Error('No edited meta-program row');
    }
    const metaProgram = this.singleEditingRow.currentData;

    if (isEmptyArray(event.detail.rows) || isEmptyArray(metaProgram.metaProgramLocations)) {
      this.programTable().confirmBeforeDelete = true;
      event.detail.success(true);
      return;
    }
    const currentMonitoringLocationIds = metaProgram.metaProgramLocations.map((value) => value.monitoringLocation.id.toString());

    // populate locations to remove
    this.programTable().markAsLoading();
    const programIdsToRemove = EntityUtils.ids(event.detail.rows);
    let locationsToRemove = (
      await this.referentialGenericService.loadPage(
        {}, // no page
        GenericReferentialFilter.fromObject({
          entityName: 'MonitoringLocation',
          criterias: [{ parentFilter: { includedIds: programIdsToRemove } }],
        })
      )
    ).data?.filter((location) => currentMonitoringLocationIds.includes(location.id));
    const remainingProgramIds = metaProgram.programIds.slice().filter((programId) => !programIdsToRemove.includes(programId));
    const remainingLocations = isNotEmptyArray(remainingProgramIds)
      ? (
          await this.referentialGenericService.loadPage(
            {}, // no page
            GenericReferentialFilter.fromObject({
              entityName: 'MonitoringLocation',
              criterias: [{ parentFilter: { includedIds: remainingProgramIds } }],
            })
          )
        ).data
      : [];
    this.programTable().markAsLoaded();
    locationsToRemove = locationsToRemove.filter((locationToRemove) => !remainingLocations.find((location) => location.id === locationToRemove.id));
    let confirmed = true;
    if (isNotEmptyArray(locationsToRemove)) {
      this.programTable().confirmBeforeDelete = false;
      confirmed = await Alerts.askConfirmation('REFERENTIAL.META_PROGRAM.CONFIRM.DELETE_PROGRAM', this.alertCtrl, this.translate, undefined, {
        list: `<div class="scroll-content"><ul>${locationsToRemove
          .map((r) => referentialToString(r, attributes.labelName))
          .map((s) => `<li>${s}</li>`)
          .join('')}</ul></div>`,
      });
      if (confirmed) {
        // remove directly these locations
        const removedLocationIds = locationsToRemove.map(entityId);
        const newLocations = metaProgram.metaProgramLocations.filter((value) => !removedLocationIds.includes(value.monitoringLocation.id.toString()));
        // Update locations table, association are cleaned
        this.patchLocations(newLocations);
        // Reload
        await this.loadDetailArea(this.singleEditingRow);
      }
    } else {
      this.programTable().confirmBeforeDelete = true;
    }
    event.detail.success(confirmed);
  }

  rightTableSelectionChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow(this.rightMenuItem.attribute, values);
    }
  }

  // DETAIL AREA

  detailAreaTabChange(event: UIEvent, detailTab: IEntityMenuItem<MetaProgram, MetaProgramDetailView>) {
    if (!detailTab) {
      throw new Error(`Cannot determinate the type of bottom area`);
    }
    if (this.selectedDetailAreaMenuItem !== detailTab) {
      if (!this.confirmDetail(this.singleEditingRow)) {
        return;
      }
    }
    this.selectedDetailAreaMenuItem = detailTab;
  }

  async confirmDetail(row?: AsyncTableElement<MetaProgram>): Promise<boolean> {
    if (row) {
      if (this.locationTable().dirty) {
        if (await this.locationTable().save()) {
          this.patchLocations(this.locationTable().value, row);
        } else {
          this.selectedDetailAreaMenuItem = this.locationsMenuItem;
          this.refreshTabs();
          return false;
        }
      }
      if (this.pmfmuTable().dirty) {
        if (await this.pmfmuTable().save()) {
          this.patchPmfmus(this.pmfmuTable().value, row);
        } else {
          this.selectedDetailAreaMenuItem = this.pmfmusMenuItem;
          this.refreshTabs();
          return false;
        }
      }
      if (this.locationPmfmuTable().dirty) {
        if (await this.locationPmfmuTable().save()) {
          this.patchLocationPmfmus(this.locationPmfmuTable().value, row);
        } else {
          this.selectedDetailAreaMenuItem = this.locationPmfmuMenuItem;
          this.refreshTabs();
          return false;
        }
      }
    }
    return true;
  }

  patchLocations(values: MetaProgramLocation[], row?: AsyncTableElement<MetaProgram>) {
    if (this.canEdit) {
      this.patchRow(this.locationsMenuItem.attribute, values, row);
      this.locationPmfmuTable().metaProgramLocations = values;
      this.locationPmfmuTable()
        .cleanOrphanRows()
        .then((values) => this.patchLocationPmfmus(values));
    }
  }

  patchPmfmus(values: MetaProgramPmfmu[], row?: AsyncTableElement<MetaProgram>) {
    if (this.canEdit) {
      this.patchRow(this.pmfmusMenuItem.attribute, values, row);
      this.locationPmfmuTable().metaProgramPmfmus = values;
      this.locationPmfmuTable()
        .cleanOrphanRows()
        .then((values) => this.patchLocationPmfmus(values));
    }
  }

  patchLocationPmfmus(values: MetaProgramLocationPmfmu[], row?: AsyncTableElement<MetaProgram>) {
    if (this.canEdit) {
      this.patchRow(this.locationPmfmuMenuItem.attribute, values, row);
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { forceAllColumns: true, rowCountWarningThreshold: 50, ...opts });
  }

  detailCount(detailTab: IEntityMenuItem<MetaProgram, MetaProgramDetailView>) {
    switch (detailTab?.view) {
      case 'locationTable':
        return this.locationTable()?.totalRowCount || undefined;
      case 'pmfmuTable':
        return this.pmfmuTable()?.totalRowCount || undefined;
      case 'locationPmfmuTable':
        return this.locationPmfmuTable()?.totalRowCount || undefined;
      default:
        return undefined;
    }
  }

  // protected methods

  protected async loadRightArea(row?: AsyncTableElement<MetaProgram>) {
    if (this.subTable) return; // don't load if sub table
    switch (this.rightMenuItem?.view) {
      case 'programTable': {
        this.detectChanges();
        if (this.programTable()) {
          this.registerSubForm(this.programTable());
          await this.loadProgramTable(row);
        }
        break;
      }
      case 'userTable': {
        this.detectChanges();
        if (this.userTable()) {
          this.registerSubForm(this.userTable());
          await this.loadUserTable(row);
        }
        break;
      }
      case 'departmentTable': {
        this.detectChanges();
        if (this.departmentTable()) {
          this.registerSubForm(this.departmentTable());
          await this.loadDepartmentTable(row);
        }
        break;
      }
    }
    await super.loadRightArea(row);
  }

  protected async loadProgramTable(row?: AsyncTableElement<MetaProgram>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.programTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    const attribute = this.rightMenuItem.attribute;
    if (isNilOrBlank(attribute)) {
      throw Error(`[program-table] no program attribute for type ${this.rightMenuItem}`);
    }

    // Load selected data
    await this.programTable().setSelectedIds(((row.currentData?.[attribute] || []) as any[]).slice());
    this.rightAreaEnabled = this.hasRightOnMetaProgram(row?.currentData);
  }

  protected async loadUserTable(row?: AsyncTableElement<MetaProgram>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.userTable(), row);
      await this.userTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected users
    this.applyRightOptions(this.userTable(), row);
    await this.userTable().setSelectedIds((row.currentData?.responsibleUserIds || []).slice());
    this.rightAreaEnabled = this.hasRightOnMetaProgram(row?.currentData);
  }

  protected async addUser(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'User',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.userTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.userTable());
      await this.userTable().setSelectedIds(includedIds);
      this.rightTableSelectionChanged(includedIds);
    }
  }

  protected async loadDepartmentTable(row?: AsyncTableElement<MetaProgram>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.departmentTable(), row);
      await this.departmentTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected departments
    this.applyRightOptions(this.departmentTable(), row);
    await this.departmentTable().setSelectedIds((row.currentData?.responsibleDepartmentIds || []).slice());
    this.rightAreaEnabled = this.hasRightOnMetaProgram(row?.currentData);
  }

  protected async addDepartment(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'Department',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.departmentTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.departmentTable());
      await this.departmentTable().setSelectedIds(includedIds);
      this.rightTableSelectionChanged(includedIds);
    }
  }

  protected async loadDetailArea(row?: AsyncTableElement<MetaProgram>) {
    if (this.subTable) return; // don't load if sub table
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.detailAreaEnabled = false;
      return;
    }

    // Load details data
    await this.loadDetailData(row);

    // Register sub tables
    this.registerSubForm(this.locationTable());
    this.registerSubForm(this.pmfmuTable());
    this.registerSubForm(this.locationPmfmuTable());

    // Load selected data
    const locations = (row.currentData?.metaProgramLocations || []).slice();
    await this.locationTable().setValue(locations);
    const pmfmus = (row.currentData?.metaProgramPmfmus || []).slice();
    await this.pmfmuTable().setValue(pmfmus);
    await this.locationPmfmuTable().setValue((row.currentData?.metaProgramLocationPmfmus || []).slice());
    this.locationPmfmuTable().metaProgramLocations = locations;
    this.locationPmfmuTable().metaProgramPmfmus = pmfmus;
    this.detailAreaEnabled = this.hasRightOnMetaProgram(row.currentData);
    this.markForCheck();
  }

  protected async loadDetailData(row: AsyncTableElement<MetaProgram>) {
    // Call service if detail data is not loaded yet
    if (!row.currentData.detailLoaded) {
      this.locationTable().markAsLoading();
      this.pmfmuTable().markAsLoading();
      this.locationPmfmuTable().markAsLoading();

      // Get children
      const [metaProgramLocations, metaProgramPmfmus] = await Promise.all([
        this._entityService.loadLocations(row.currentData.id),
        this._entityService.loadPmfmus(row.currentData.id),
      ]);

      // Build associations
      const metaProgramLocationPmfmus: MetaProgramLocationPmfmu[] = [];
      metaProgramLocations.forEach((metaProgramLocation) => {
        metaProgramLocation.metaProgramPmfmuIds.forEach((metaProgramPmfmuId) => {
          const association = new MetaProgramLocationPmfmu(
            metaProgramLocation,
            metaProgramPmfmus.find((value) => value.id === metaProgramPmfmuId)
          );
          metaProgramLocationPmfmus.push(association);
        });
      });

      row.validator.patchValue({ metaProgramLocations, metaProgramPmfmus, metaProgramLocationPmfmus, detailLoaded: true });
    }
  }

  protected async patchDuplicateEntity(entity: MetaProgram, opts?: any): Promise<any> {
    const patch = {
      ...(await super.patchDuplicateEntity(entity, opts)),
      detailLoaded: true,
    };
    // Clean details
    patch.metaProgramLocations = patch.metaProgramLocations.map((value) => {
      value.id = undefined;
      value.updateDate = undefined;
      value.metaProgramId = undefined;
      value.metaProgramPmfmuIds = [];
      return value;
    });
    patch.metaProgramPmfmus = patch.metaProgramPmfmus.map((value) => {
      value.id = undefined;
      value.updateDate = undefined;
      value.metaProgramId = undefined;
      return value;
    });
    patch.metaProgramLocationPmfmus = patch.metaProgramLocationPmfmus.map((value) => {
      value.id = undefined;
      value.updateDate = undefined;
      value.metaProgramLocation.id = undefined;
      value.metaProgramLocation.updateDate = undefined;
      value.metaProgramLocation.metaProgramId = undefined;
      value.metaProgramPmfmu.id = undefined;
      value.metaProgramPmfmu.updateDate = undefined;
      value.metaProgramPmfmu.metaProgramId = undefined;
      return value;
    });
    return patch;
  }

  protected hasRightOnMetaProgram(metaProgram: MetaProgram): boolean {
    if (!metaProgram) {
      return false;
    }

    // Admins or program managers
    return (
      this.accountService.isAdmin() ||
      (this.accountService.isUser() &&
        (metaProgram.responsibleUserIds.includes(this.accountService.person.id) ||
          metaProgram.responsibleDepartmentIds.includes(this.accountService.department.id)))
    );
  }

  /**
   * Refresh buttons state according to user profile or program rights
   *
   * @protected
   */
  protected refreshButtons() {
    // Only admins can add program
    this.tableButtons.canAdd = !this.subTable && this.accountService.isAdmin();
    this.tableButtons.canDuplicate = !this.subTable && this.accountService.isAdmin();
    this.tableButtons.canDelete =
      (!this.subTable && this.selection.hasValue() && this.selection.selected.every((row) => this.hasRightOnMetaProgram(row.currentData))) || false;
  }

  protected refreshTabs() {
    setTimeout(() => {
      this.detailAreaTabs()?.updatePagination();
      this.detailAreaTabs()?._alignInkBarToSelectedTab();
    });
  }

  protected updateTitle() {
    this.title = this.titleI18n
      ? this.translate.instant(this.titleI18n)
      : ['MENU.REFERENTIAL.MANAGEMENT', 'REFERENTIAL.ENTITY.META_PROGRAMS'].map((value) => this.translate.instant(value)).join(' &rArr; ');
  }

  protected applyRightOptions(table: IRightSelectTable<any>, row?: AsyncTableElement<MetaProgram>) {
    row = row || this.singleEditingRow;
    table.rightOptions = {
      entityName: MetaProgram.entityName,
      metaProgramId: row?.currentData.id,
    };
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'description');
  }
}
