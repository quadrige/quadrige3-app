import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { inject, NgModule } from '@angular/core';
import { CampaignTable } from '@app/referential/campaign/campaign.table';
import { CampaignModule } from '@app/referential/campaign/campaign.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CampaignTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), CampaignModule],
})
export class CampaignRoutingModule {}
