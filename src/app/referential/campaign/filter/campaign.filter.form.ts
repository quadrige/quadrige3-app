import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { CampaignFilter, CampaignFilterCriteria } from '@app/referential/campaign/filter/campaign.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-campaign-filter-form',
  templateUrl: './campaign.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignFilterForm extends ReferentialCriteriaFormComponent<CampaignFilter, CampaignFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }

  protected criteriaToQueryParams(criteria: CampaignFilterCriteria): PartialRecord<keyof CampaignFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'metaProgramFilter');
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    return params;
  }
}
