import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { ReferentialFilter, ReferentialFilterCriteria, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Campaign } from '@app/referential/campaign/campaign.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'CampaignFilterCriteriaVO' })
export class CampaignFilterCriteria extends ReferentialFilterCriteria<Campaign, number> {
  static fromObject: (source: any, opts?: any) => CampaignFilterCriteria;

  metaProgramFilter: StrReferentialFilterCriteria = null;
  programFilter: StrReferentialFilterCriteria = null;

  constructor() {
    super(CampaignFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.metaProgramFilter = StrReferentialFilterCriteria.fromObject(source.metaProgramFilter || {});
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.metaProgramFilter = this.metaProgramFilter?.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    // Remove unhandled properties (Mantis #63344)
    delete target.exactValues;
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'metaProgramFilter') return !BaseFilterUtils.isCriteriaEmpty(this.metaProgramFilter, true);
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'CampaignFilterVO' })
export class CampaignFilter extends ReferentialFilter<CampaignFilter, CampaignFilterCriteria, Campaign> {
  static fromObject: (source: any, opts?: any) => CampaignFilter;

  constructor() {
    super(CampaignFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  criteriaFromObject(source: any, opts: any): CampaignFilterCriteria {
    return CampaignFilterCriteria.fromObject(source, opts);
  }
}
