import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { Injectable, Injector } from '@angular/core';
import { Campaign } from '@app/referential/campaign/campaign.model';
import { CampaignFilter, CampaignFilterCriteria } from '@app/referential/campaign/filter/campaign.filter.model';

export const campaignFragment = gql`
  fragment CampaignFragment on CampaignVO {
    id
    name
    startDate
    endDate
    comments
    sismerLink
    positionComment
    userId
    recorderDepartmentId
    updateDate
  }
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Campaigns($page: PageInput, $filter: CampaignFilterVOInput) {
      data: campaigns(page: $page, filter: $filter) {
        ...CampaignFragment
      }
    }
    ${campaignFragment}
  `,
  loadAllWithTotal: gql`
    query CampaignsWithTotal($page: PageInput, $filter: CampaignFilterVOInput) {
      data: campaigns(page: $page, filter: $filter) {
        ...CampaignFragment
      }
      total: campaignsCount(filter: $filter)
    }
    ${campaignFragment}
  `,
  countAll: gql`
    query CampaignsCount($filter: CampaignFilterVOInput) {
      total: campaignsCount(filter: $filter)
    }
  `,
};

const mutations: BaseEntityGraphqlMutations = {};

@Injectable({ providedIn: 'root' })
export class CampaignService extends BaseReferentialService<Campaign, CampaignFilter, CampaignFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Campaign, CampaignFilter, { queries, mutations });
    this._logPrefix = '[campaign-service]';
  }
}
