import { NgModule } from '@angular/core';
import { CampaignTable } from '@app/referential/campaign/campaign.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { CampaignFilterForm } from '@app/referential/campaign/filter/campaign.filter.form';

@NgModule({
  imports: [ReferentialModule, CampaignFilterForm],
  declarations: [CampaignTable],
  exports: [CampaignTable],
})
export class CampaignModule {}
