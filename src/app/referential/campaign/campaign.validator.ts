import { Injectable } from '@angular/core';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { Campaign } from '@app/referential/campaign/campaign.model';
import { AbstractControlOptions, UntypedFormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { CampaignService } from '@app/referential/campaign/campaign.service';
import { PartialRecord } from '@app/shared/model/interface';
import { SharedFormGroupValidators } from '@sumaris-net/ngx-components';

@Injectable({ providedIn: 'root' })
export class CampaignValidator extends ReferentialValidatorService<Campaign> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: CampaignService,
    protected translate: TranslateService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Campaign, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return <PartialRecord<keyof Campaign, any>>{
      id: [data?.id || null],
      name: [data?.name || null],
      startDate: [data?.startDate || null],
      endDate: [data?.endDate || null],
      sismerLink: [data?.sismerLink || null],
      comments: [data?.comments || null],
      positionComment: [data?.positionComment || null],
      userId: [data?.userId || null],
      recorderDepartmentId: [data?.recorderDepartmentId || null],
      updateDate: [data?.updateDate || null],
    };
  }

  getFormGroupOptions(data?: Campaign, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [SharedFormGroupValidators.dateRange('startDate', 'endDate')],
    };
  }
}
