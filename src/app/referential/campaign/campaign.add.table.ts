import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { CampaignService } from '@app/referential/campaign/campaign.service';
import { CampaignValidator } from '@app/referential/campaign/campaign.validator';
import { CampaignTable } from '@app/referential/campaign/campaign.table';
import { IAddTable } from '@app/shared/table/table.model';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { CampaignFilter } from '@app/referential/campaign/filter/campaign.filter.model';
import { Campaign } from '@app/referential/campaign/campaign.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { CampaignFilterForm } from '@app/referential/campaign/filter/campaign.filter.form';

@Component({
  selector: 'app-campaign-add-table',
  templateUrl: './campaign.table.html',
  standalone: true,
  imports: [ReferentialModule, CampaignFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignAddTable extends CampaignTable implements IAddTable<Campaign> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: CampaignService,
    protected validatorService: CampaignValidator
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[campaign-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: CampaignFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
