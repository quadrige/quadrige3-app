import { EntityClass, fromDateISOString, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';

@EntityClass({ typename: 'CampaignVO' })
export class Campaign extends Referential<Campaign> {
  static entityName = 'Campaign';
  static fromObject: (source: any, opts?: any) => Campaign;

  startDate: Moment = null;
  endDate: Moment = null;
  sismerLink: string = null;
  positionComment: string = null;
  userId: number = null;
  recorderDepartmentId: number = null;

  constructor() {
    super(Campaign.TYPENAME);
    this.entityName = Campaign.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Campaign.entityName;
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
    this.sismerLink = source.sismerLink;
    this.positionComment = source.positionComment;
    this.userId = source.userId;
    this.recorderDepartmentId = source.recorderDepartmentId;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.startDate = Dates.toLocalDateString(this.startDate);
    target.endDate = Dates.toLocalDateString(this.endDate);
    return target;
  }
}
