import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { Campaign } from '@app/referential/campaign/campaign.model';
import { CampaignService } from '@app/referential/campaign/campaign.service';
import { CampaignValidator } from '@app/referential/campaign/campaign.validator';
import { CampaignFilter, CampaignFilterCriteria } from '@app/referential/campaign/filter/campaign.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-campaign-table',
  templateUrl: './campaign.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CampaignTable extends ReferentialTable<Campaign, number, CampaignFilter, CampaignFilterCriteria, CampaignValidator> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: CampaignService,
    protected validatorService: CampaignValidator
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'startDate', 'endDate', 'sismerLink', /*'positioningComment',*/ 'comments', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      Campaign,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.CAMPAIGN.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[campaign-table]';
    this.setShowColumn('statusId', false);
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('sismerLink', 'positioningComment', 'comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name');
  }
}
