import { Entity, EntityUtils, IEntitiesTableDataSource, IEntity, isEmptyArray } from '@sumaris-net/ngx-components';
import { AsyncValidatorFn, UntypedFormGroup, ValidationErrors } from '@angular/forms';
import { Observable, timer } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { IReferentialService } from '@app/referential/service/base-referential.service';
import { Utils } from '@app/shared/utils';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';

export const lengthLabel = 40;
export const lengthName = 100;
export const lengthDescription = 2000;
export const lengthComment = 2000;

export interface ReferentialValidatorOptions {
  entityName?: string;
  criteria?: IReferentialFilterCriteria<any>;
  newData?: boolean;
}

export abstract class ReferentialValidatorService<
  E extends Entity<E, ID>,
  ID = number,
  O extends ReferentialValidatorOptions = ReferentialValidatorOptions,
> extends BaseValidatorService<E, O> {}

export class ReferentialAsyncValidators {
  static checkIdAlreadyExists(
    referentialService: IReferentialService<any, any, IReferentialFilterCriteria<any>, any, any, any>,
    dataSource: IEntitiesTableDataSource<any>,
    opts: ReferentialValidatorOptions,
    currentData?: IEntity<any, any>,
    errorMessage?: string
  ): AsyncValidatorFn {
    if (!opts) return null;
    if (!currentData && opts.newData !== true) {
      console.warn(`[referential-validator] can't check id already exists of this existing row : no current data`);
      return null;
    }

    return (control): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => control.value),
        mergeMap(async (value) => {
          if (
            // no value
            !value ||
            // or id is same as the edited data
            (!opts.newData && currentData?.id === value)
          ) {
            // don't check
            return null;
          }

          const error = errorMessage ? { checkIdAlreadyExistsMsg: errorMessage } : { alreadyExists: true };

          // First check in current data source
          if (dataSource) {
            if (dataSource.getRows().filter((row) => row.currentData?.id?.toString().toUpperCase() === value?.toString().toUpperCase()).length > 1) {
              return error;
            }
          }

          // Next, check via service
          const criteria: IReferentialFilterCriteria<any> = {
            ...opts.criteria,
            includedIds: [value],
          };
          return referentialService.exists(criteria, opts?.entityName).then((exists) => (exists ? error : null));
        })
      );
  }

  static checkAttributeAlreadyExists(
    referentialService: IReferentialService<any, any, IReferentialFilterCriteria<any>, any, any, any>,
    dataSource: IEntitiesTableDataSource<any>,
    opts: ReferentialValidatorOptions,
    currentData: IEntity<any, any>,
    attribute?: string, // simple attribute only
    errorMessage?: string
  ): AsyncValidatorFn {
    if (!opts) return null;
    attribute = attribute || 'name'; // default
    if (!currentData && opts.newData !== true) {
      console.warn(`[referential-validator] can't check ${attribute} already exists of this existing row : no current data`);
      return null;
    }

    return (control): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => control.value),
        mergeMap(async (value) => {
          if (
            // no value
            !value ||
            // or attribute is same as the edited data
            (!opts.newData && currentData?.[attribute] === value)
          ) {
            // don't check
            return null;
          }

          const error = errorMessage ? { checkAttributeAlreadyExistsMsg: errorMessage } : { alreadyExists: true };

          // First check in current data source
          if (dataSource) {
            if (dataSource.getRows().filter((row) => row.currentData?.[attribute] === value).length > 1) {
              return error;
            }
          }

          // Next, check via service
          const criteria: IReferentialFilterCriteria<any> = {
            ...opts.criteria,
            exactText: value,
            searchAttributes: [attribute],
            excludedIds: (currentData?.id && [currentData.id.toString()]) || undefined,
          };
          return referentialService.exists(criteria, opts?.entityName).then((exists) => (exists ? error : null));
        })
      );
  }
}

export class ReferentialGroupAsyncValidators {
  static checkAttributesAlreadyExists(
    referentialService: IReferentialService<any, any, IReferentialFilterCriteria<any>, any, any, any>,
    dataSource: IEntitiesTableDataSource<any>,
    opts: ReferentialValidatorOptions,
    currentData: IEntity<any, any>,
    attributes: string[],
    errorMessage?: string
  ): AsyncValidatorFn {
    if (!opts) return null;
    if (isEmptyArray(attributes)) {
      console.warn(`[referential-validator] can't check attributes already exists: no attributes provided`);
      return null;
    }
    if (!currentData && opts.newData !== true) {
      console.warn(`[referential-validator] can't check ${attributes} already exists of this existing row : no current data`);
      return null;
    }

    return (form: UntypedFormGroup): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => Utils.toObjectMap(attributes, (attribute) => EntityUtils.getPropertyByPath(form.value, attribute))),
        mergeMap(async (attributeValueMap) => {
          const searchAttributes = Object.keys(attributeValueMap);
          const values = searchAttributes.map((attribute) => attributeValueMap[attribute]);

          if (
            // no value
            values.every((value) => !value) ||
            // or attribute is same as the edited data
            (!opts.newData &&
              Utils.deepEquals(
                attributeValueMap,
                Utils.toObjectMap(searchAttributes, (attribute) => EntityUtils.getPropertyByPath(currentData, attribute))
              ))
          ) {
            // don't check
            return null;
          }

          const error = errorMessage ? { checkAttributesAlreadyExistsMsg: errorMessage } : { alreadyExists: true };

          // First check in current data source
          if (dataSource) {
            const nbRows = dataSource.getRows().filter((row) =>
              Utils.deepEquals(
                attributeValueMap,
                Utils.toObjectMap(searchAttributes, (attribute) => EntityUtils.getPropertyByPath(row.currentData, attribute))
              )
            ).length;
            if (nbRows > 1) {
              return error;
            }
          }

          // Next, check via service
          if (referentialService) {
            const criteria: IReferentialFilterCriteria<any> = {
              ...opts.criteria,
              searchAttributes,
              exactValues: values,
              excludedIds: (currentData?.id && [currentData.id.toString()]) || undefined,
            };
            return referentialService.exists(criteria, opts?.entityName).then((exists) => (exists ? error : null));
          }

          return null;
        })
      );
  }
}
