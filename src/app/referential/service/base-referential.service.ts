import { Observable, of } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { referentialErrorCodes } from './errors';
import { DocumentNode, FetchPolicy } from '@apollo/client/core';
import { SortDirection } from '@angular/material/sort';
import {
  arraySize,
  BaseEntityGraphqlQueries,
  BaseEntityGraphqlSubscriptions,
  BaseEntityService,
  BaseEntityServiceOptions,
  chainPromises,
  EntitiesServiceWatchOptions,
  EntityAsObjectOptions,
  EntityServiceListenChangesOptions,
  EntityServiceLoadOptions,
  ENVIRONMENT,
  IEntityService,
  isEmptyArray,
  isNil,
  isNotEmptyArray,
  isNotNil,
  LoadResult,
  Page,
  PlatformService,
  PropertyMap,
  SuggestService,
  toBoolean,
  toNumber,
} from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import gql from 'graphql-tag';
import { Injector } from '@angular/core';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { entityId, EntityUtils } from '@app/shared/entity.utils';
import { Entity } from '@sumaris-net/ngx-components/src/app/core/services/model/entity.model';
import { WatchQueryFetchPolicy } from '@apollo/client/core/watchQueryOptions';
import { Job } from '@app/social/job/job.model';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { StableGraphqlService } from '@app/shared/service/stable-graphql.service';
import { IBaseEntitiesService, IExportContext } from '@app/shared/service/base-entity.service';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { entitiesWithoutTranscribingItems } from '@app/referential/model/referential.constants';

export const referentialFragments = {
  light: gql`
    fragment ReferentialFragment on ReferentialVO {
      id
      label
      name
      description
      statusId
      entityName
      __typename
    }
  `,
  full: gql`
    fragment FullReferentialFragment on ReferentialVO {
      id
      label
      name
      description
      comments
      updateDate
      creationDate
      statusId
      entityName
      __typename
    }
  `,
};

export interface IReferentialService<E, F, C, ID, WO, LO> extends IBaseEntitiesService<E, F, WO>, IEntityService<E, ID, LO>, SuggestService<E, F> {
  exists(criteria: C, entityName?: string): Promise<boolean>;

  watchPage(page: Partial<Page>, filter: F, opts?: WO): Observable<LoadResult<E>>;

  loadPage(page: Partial<Page>, filter: Partial<F>, opts?: LO): Promise<LoadResult<E>>;
}

export type ImportShapefileServiceOptions = object;

export interface IImportShapefileService {
  importShapefileAsync(fileName: string | string[], opts?: ImportShapefileServiceOptions): Promise<Job>;
}

export interface ReferentialEntityGraphqlQueries extends BaseEntityGraphqlQueries {
  loadAllLight?: any;
  loadAllLightWithTotal?: any;
  exportAllAsync?: any;
}

export interface ReferentialServiceOptions<
  E extends Entity<E, ID>,
  ID = number,
  Q extends ReferentialEntityGraphqlQueries = ReferentialEntityGraphqlQueries,
  M extends BaseEntityGraphqlMutations = BaseEntityGraphqlMutations,
  S extends BaseEntityGraphqlSubscriptions = BaseEntityGraphqlSubscriptions,
> extends BaseEntityServiceOptions<E, ID, Q, M, S> {
  watchQueryFetchPolicy?: WatchQueryFetchPolicy;
  loadFetchPolicy?: FetchPolicy;
}

export interface ReferentialServiceWatchOptions extends EntitiesServiceWatchOptions {
  entityName?: string;
  query?: any;
  light?: boolean;
}

export interface ReferentialServiceLoadOptions extends EntityServiceLoadOptions {
  entityName?: string;
  query?: any;
  light?: boolean;
  debug?: boolean;
  withTotal?: boolean;
}

export interface ReferentialSaveOptions extends EntitySaveOptions {
  query?: any;
  withChildrenEntities?: boolean;
}

export const defaultReferentialSaveOption: ReferentialSaveOptions = {
  withChildrenEntities: true,
};

// todo try to factorize BaseReferentialService and BaseDataService
export abstract class BaseReferentialService<
    E extends Referential<E, ID>,
    F extends ReferentialFilter<F, C, E, ID>,
    C extends ReferentialFilterCriteria<E, ID>,
    ID = number,
    WO extends ReferentialServiceWatchOptions = ReferentialServiceWatchOptions,
    LO extends ReferentialServiceLoadOptions = ReferentialServiceLoadOptions,
    SO extends ReferentialSaveOptions = ReferentialSaveOptions,
    Q extends ReferentialEntityGraphqlQueries = ReferentialEntityGraphqlQueries,
    M extends BaseEntityGraphqlMutations = BaseEntityGraphqlMutations,
    S extends BaseEntityGraphqlSubscriptions = BaseEntityGraphqlSubscriptions,
  >
  extends BaseEntityService<E, F, ID, WO, LO, Q, M, S>
  implements IReferentialService<E, F, C, ID, WO, LO>
{
  private readonly watchQueryFetchPolicy: WatchQueryFetchPolicy;
  private readonly loadFetchPolicy: FetchPolicy;

  protected constructor(
    protected injector: Injector,
    protected dataType: new () => E,
    protected filterType: new () => F,
    options: ReferentialServiceOptions<E, ID, Q, M, S>
  ) {
    super(injector.get(StableGraphqlService), injector.get(PlatformService), dataType, filterType, {
      equals: (e1, e2) => this.equals(e1, e2),
      ...options,
    });
    this.watchQueryFetchPolicy = options?.watchQueryFetchPolicy || 'cache-and-network';
    this.loadFetchPolicy = options?.loadFetchPolicy || 'cache-first';

    // register custom error
    const customErrors: PropertyMap = {};
    customErrors[referentialErrorCodes.attachedData] = 'REFERENTIAL.ERROR.ATTACHED_DATA';
    customErrors[referentialErrorCodes.attachedAdministration] = 'REFERENTIAL.ERROR.ATTACHED_ADMINISTRATION';
    customErrors[referentialErrorCodes.attachedControlRule] = 'REFERENTIAL.ERROR.ATTACHED_CONTROL_RULE';
    customErrors[referentialErrorCodes.attachedFilter] = 'REFERENTIAL.ERROR.ATTACHED_FILTER';
    customErrors[referentialErrorCodes.attachedTranscribing] = 'REFERENTIAL.ERROR.ATTACHED_TRANSCRIBING';
    customErrors[referentialErrorCodes.attachedReferential] = 'REFERENTIAL.ERROR.ATTACHED_REFERENTIAL';
    this.graphql.registerCustomError(customErrors);

    this._logPrefix = '[base-referential-service]'; // Should be override by implementation class
    this._debug = toBoolean(!injector.get(ENVIRONMENT)?.production, true);
  }

  asFilter(source: any): F {
    const filter = super.asFilter(source || {});
    // For entity without transcribing, remove unhandled systemId
    if (entitiesWithoutTranscribingItems.includes(this._logTypeName)) {
      delete filter.systemId;
    }
    return filter;
  }

  /**
   * Export referential asynchronously
   *
   * @param context the export context
   * @param filter the current filter
   * @return the download url
   */
  async exportAllAsync(context: IExportContext, filter: F): Promise<Job> {
    if (!context) {
      console.warn(`${this._logPrefix} missing context argument, export aborted`);
      return undefined;
    }
    if (!this.queries.exportAllAsync) {
      console.warn(`${this._logPrefix} exportAllAsync graphql query undefined`);
      return undefined;
    }

    filter = this.asFilter(filter);
    const variables = { context, filter: filter.asPodObject() };
    if (this._debug) console.debug(`${this._logPrefix} Exporting referential`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: Job }>({
      query: this.queries.exportAllAsync,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} referential export job started in ${Date.now() - now}ms`, data);
    return data;
  }

  /** @deprecated use watchPage */
  watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: Partial<F>, opts?: WO): Observable<LoadResult<E>> {
    return this.watchPage({ offset, size, sortBy, sortDirection }, filter, opts);
  }

  watchPage(page: Partial<Page>, filter?: Partial<F>, opts?: WO): Observable<LoadResult<E>> {
    filter = this.asFilter(filter);
    page = this.asPage(page, filter);

    if (filter?.criterias?.every((criteria) => criteria.forceIncludedIds === true && isEmptyArray(criteria.includedIds))) {
      // return empty result
      return of({ data: [], total: 0 });
    }

    // Build variables
    const variables = this.buildWatchVariables(page, filter, opts);

    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Loading ${this._logTypeName}...`, variables);

    const withTotal = opts?.withTotal !== false && !!this.queries.loadAllWithTotal;
    const useLight = opts?.light && ((withTotal && !!this.queries.loadAllLightWithTotal) || (!withTotal && !!this.queries.loadAllLight));
    const query =
      opts?.query || // use given query
      // Or get loadAll or loadAllWithTotal query (with light version if exists)
      (withTotal
        ? useLight
          ? this.queries.loadAllLightWithTotal
          : this.queries.loadAllWithTotal
        : useLight
          ? this.queries.loadAllLight
          : this.queries.loadAll);
    return this.mutableWatchQuery<LoadResult<any>>({
      queryName: withTotal ? (useLight ? 'LoadAllLightWithTotal' : 'LoadAllWithTotal') : useLight ? 'LoadAllLight' : 'LoadAll',
      query,
      arrayFieldName: 'data',
      totalFieldName: withTotal ? 'total' : undefined,
      insertFilterFn: filter?.asFilterFn(),
      sortFn: EntityUtils.entitySortComparator(page.sortBy, page.sortDirection),
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: opts?.fetchPolicy || this.watchQueryFetchPolicy,
    }).pipe(
      distinctUntilChanged((x, y) => this.distinctResultComparator(x, y)),
      map((res) => {
        if (!res) {
          console.warn(`${this._logPrefix} no result for:`, query, variables);
          return { data: [], total: 0 };
        }
        const { data, total } = res;
        // Convert to entity (if needed)
        const entities = opts?.toEntity !== false ? (data || []).map((json) => this.fromObject(json)) : ((data || []) as E[]);
        if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} loaded in ${Date.now() - now}ms`, entities);
        return { data: entities, total };
      })
    );
  }

  async load(id: ID, opts?: LO): Promise<E> {
    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Loading ${this._logTypeName} {${id}}...`);
    const query = opts?.query || this.queries.load;

    const { data } = await this.graphql.query<{ data: any }>({
      query,
      variables: {
        id,
      },
      fetchPolicy: opts?.fetchPolicy || this.loadFetchPolicy,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
    });
    if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} item loaded in ${Date.now() - now}ms`, data);

    // Convert to entity
    return opts?.toEntity !== false ? data && this.fromObject(data) : (data as E);
  }

  /** @deprecated use loadPage */
  async loadAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<F>,
    opts?: LO
  ): Promise<LoadResult<E>> {
    return this.loadPage(this.asPage({ offset, size, sortBy, sortDirection }, filter), filter, opts);
  }

  async loadPage(
    page: Partial<Page>, // use page as-is (don't call asPage())
    filter?: Partial<F>,
    opts?: LO
  ): Promise<LoadResult<E>> {
    const debug = this._debug && opts?.debug !== false;
    filter = this.asFilter(filter);

    const variables: any = this.buildLoadVariables(page, filter, opts);

    const now = Date.now();
    if (debug) console.debug(`${this._logPrefix} Loading ${this._logTypeName} items...`, variables);

    const withTotal = opts?.withTotal !== false && !!this.queries.loadAllWithTotal;
    const useLight = opts?.light && ((withTotal && !!this.queries.loadAllLightWithTotal) || (!withTotal && !!this.queries.loadAllLight));
    const query =
      opts?.query || // use given query
      // Or get loadAll or loadAllWithTotal query (with light version if exists)
      (withTotal
        ? useLight
          ? this.queries.loadAllLightWithTotal
          : this.queries.loadAllWithTotal
        : useLight
          ? this.queries.loadAllLight
          : this.queries.loadAll);
    const { data, total } = await this.graphql.query<LoadResult<any>>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: opts?.fetchPolicy || this.loadFetchPolicy,
    });
    const entities = opts?.toEntity !== false ? (data || []).map((json) => this.fromObject(json)) : ((data || []) as E[]);
    if (debug) console.debug(`${this._logPrefix} ${this._logTypeName} items loaded in ${Date.now() - now}ms`, data);
    const res: LoadResult<E> = { data: entities, total };
    if (withTotal) {
      // Add fetch more function
      const nextOffset = page.offset + entities.length;
      if (nextOffset < total) {
        res.fetchMore = () => this.loadPage({ ...page, offset: nextOffset }, filter, opts);
      }
    }
    return res;
  }

  async exists(criteria: C, entityName?: string): Promise<boolean> {
    const now = Date.now();
    const filter = this.asFilter({ criterias: [criteria] });
    const variables: any = {
      entityName,
      filter: filter?.asPodObject(),
    };

    if (this._debug) console.debug(`${this._logPrefix} Counting ${this._logTypeName} items...`, variables);
    const res = await this.graphql.query<{ total?: number }>({
      query: this.queries.countAll,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.COUNT_REFERENTIAL_ERROR' },
      fetchPolicy: 'no-cache',
    });
    if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} items counted in ${Date.now() - now}ms`, res.total);
    return res.total > 0;
  }

  async suggest(value: any, filter?: F, sortBy?: string, sortDirection?: SortDirection): Promise<LoadResult<E>> {
    if (EntityUtils.isNotEmpty(value, 'id')) return { data: [value] };
    const searchText: string = (typeof value === 'string' && value !== '*' && value) || undefined;
    filter = this.asFilter(filter);
    filter.patch({ searchText });

    const res = await this.loadPage(
      {
        offset: 0,
        size: 30,
        sortBy,
        sortDirection,
      },
      filter,
      <LO>{ withTotal: true, fetchPolicy: 'network-only' }
    );
    res.data.forEach((ref) => {
      if (ref.statusId === 0) {
        // add specific css class for disabled referential (rendered in MatAutocompleteField)
        ref['classList'] = 'disabled-referential';
      }
    });
    return res;
  }

  async saveAll(entities: E[], opts?: SO): Promise<E[]> {
    // Nothing to save: skip
    if (isEmptyArray(entities)) return entities;

    if (!this.mutations.saveAll) {
      if (!this.mutations.save) throw new Error('Not implemented');
      // Save one by one
      return chainPromises((entities || []).map((entity) => () => this.save(entity, opts)));
    }

    const now = Date.now();

    // Check type name
    if (this._typename !== 'ReferentialVO' && entities[0].__typename !== this._typename) {
      throw new Error(`Cannot save a type '${entities[0].__typename}' in this service of '${this._typename}'`);
    }

    // Transform to json
    const json = entities.map((entity) => {
      this.fillDefaultProperties(entity);
      return this.asObject(entity);
    });
    const options: any = { ...opts };
    if (options) {
      delete options.saveOnlyDirtyRows;
      delete options.query;
    }
    const variables = { data: json, options };
    if (this._debug) console.debug(`${this._logPrefix} Saving all ${this._logTypeName}...`, variables);

    await this.graphql.mutate<LoadResult<any>>({
      mutation: opts?.query || this.mutations.saveAll,
      variables,
      error: { code: referentialErrorCodes.save, message: 'REFERENTIAL.ERROR.SAVE_REFERENTIAL_ERROR' },
      update: (cache, { data }, { context, variables }) => {
        if (data && data.data) {
          // Update entities (id and update date)
          entities.forEach((entity) => {
            const savedEntity = data.data.find((e) => this.equals(e, entity));
            this.copyIdAndUpdateDate(savedEntity, entity);

            // Force transcribing item reload (Mantis #61037)
            entity.transcribingItemsLoaded = false;
          });

          // Update the cache
          this.insertIntoMutableCachedQueries(cache, {
            query: this.queries.loadAll,
            data: data.data,
          });
        }

        if (opts?.update) {
          opts.update(cache, { data }, { context, variables });
        }

        if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} saved in ${Date.now() - now}ms`, entities);
      },
    });

    return entities;
  }

  async save(entity: E, opts?: SO): Promise<E> {
    if (!this.mutations.save) {
      if (!this.mutations.saveAll) throw new Error('Not implemented');
      const allData = await this.saveAll([entity], opts);
      return allData?.[0];
    }
    const now = Date.now();

    // Fill default properties
    this.fillDefaultProperties(entity);

    // Check entity name
    if (this._typename !== 'ReferentialVO' && entity.__typename !== this._typename) {
      throw new Error(`Cannot save a type '${entity.__typename}' in this service of '${this._typename}'`);
    }

    // Transform into json
    const json = this.asObject(entity);

    const isNew = isNil(json.id);

    const options: any = { ...opts };
    if (options) {
      delete options.saveOnlyDirtyRows;
      delete options.query;
    }
    const variables = { data: json, options };
    if (this._debug) console.debug(`${this._logPrefix} Saving ${this._logTypeName}...`, variables);

    await this.graphql.mutate<{ data: any }>({
      mutation: opts?.query || this.mutations.save,
      variables,
      refetchQueries: this.getRefetchQueriesForMutation(opts),
      awaitRefetchQueries: toBoolean(opts?.awaitRefetchQueries, false),
      error: { code: referentialErrorCodes.save, message: 'REFERENTIAL.ERROR.SAVE_REFERENTIAL_ERROR' },
      update: (cache, { data }, { context, variables }) => {
        // Update entity
        const savedEntity = data && data.data;
        this.copyIdAndUpdateDate(savedEntity, entity);

        // Force transcribing item reload (Mantis #61037)
        entity.transcribingItemsLoaded = false;

        if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} saved in ${Date.now() - now}ms`, entity);

        // Update the cache
        if (isNew) {
          this.insertIntoMutableCachedQueries(cache, {
            query: this.queries.loadAll,
            data: savedEntity,
          });
        }

        if (opts?.update) {
          opts.update(cache, { data }, { context, variables });
        }
      },
    });

    return entity;
  }

  async deleteAll(entities: E[], opts?: EntitySaveOptions): Promise<any> {
    if (!this.mutations.deleteAll) {
      if (!this.mutations.delete) throw new Error('Not implemented');
      // Delete one by one
      return chainPromises((entities || []).map((entity) => () => this.delete(entity, opts)));
    }

    // Filter saved entities
    entities = (entities && entities.filter((e) => !!e.id && !!e.entityName && !!e.updateDate)) || [];

    // Nothing to save: skip
    if (isEmptyArray(entities)) return;

    const ids = entities.map(entityId);
    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Deleting ${this._logTypeName}...`, ids);

    await this.graphql.mutate<any>({
      mutation: this.mutations.deleteAll,
      refetchQueries: this.getRefetchQueriesForMutation(opts),
      awaitRefetchQueries: opts?.awaitRefetchQueries,
      variables: {
        entityName: entities[0].entityName,
        ids,
      },
      error: { code: referentialErrorCodes.delete, message: 'REFERENTIAL.ERROR.DELETE_REFERENTIAL_ERROR' },
      update: (cache, res, { context, variables }) => {
        // Remove from cache
        if (this.watchQueriesUpdatePolicy === 'update-cache') {
          this.removeFromMutableCachedQueriesByIds(cache, {
            queries: this.getLoadQueries(),
            ids,
          });
        }

        if (opts?.update) {
          opts.update(cache, res, { context, variables });
        }

        if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} deleted in ${Date.now() - now}ms`);
      },
    });
  }

  async delete(entity: E, opts?: EntitySaveOptions): Promise<any> {
    if (!this.mutations.delete) {
      if (!this.mutations.deleteAll) throw new Error('Not implemented');
      const data = await this.deleteAll([entity], opts);
      return data && data[0];
    }

    // Nothing to save: skip
    if (!entity || isNil(entity.id)) return;

    const id = entity.id;
    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Deleting ${this._logTypeName} {${id}}...`);

    await this.graphql.mutate<any>({
      mutation: this.mutations.delete,
      refetchQueries: this.getRefetchQueriesForMutation(opts),
      awaitRefetchQueries: opts?.awaitRefetchQueries,
      variables: {
        entityName: entity.entityName,
        id,
      },
      error: { code: referentialErrorCodes.delete, message: 'REFERENTIAL.ERROR.DELETE_REFERENTIAL_ERROR' },
      update: (cache, res, { context, variables }) => {
        // Remove from cache
        if (this.watchQueriesUpdatePolicy === 'update-cache') {
          this.removeFromMutableCachedQueriesByIds(cache, {
            queries: this.getLoadQueries(),
            ids: [id],
          });
        }

        if (opts?.update) {
          opts.update(cache, res, { context, variables });
        }

        if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} deleted in ${Date.now() - now}ms`);
      },
    });
  }

  listenChanges(id: ID, opts?: EntityServiceListenChangesOptions): Observable<E> {
    if (isNil(id)) throw Error("Missing argument 'id' ");
    if (!this.subscriptions.listenChanges) throw Error(`${this._logPrefix} Not implemented for ${this._logTypeName}!`);

    const variables = opts?.variables || {
      id,
      interval: toNumber(opts?.interval || 10), // 10 seconds
    };
    if (this._debug) console.debug(`${this._logPrefix} [WS] Listening for changes on ${this._logTypeName} {${id}}...`);

    return this.graphql
      .subscribe<{ data: any }>({
        query: opts?.query || this.subscriptions.listenChanges,
        fetchPolicy: opts?.fetchPolicy || 'no-cache',
        variables,
        error: {
          code: referentialErrorCodes.subscribe,
          message: 'REFERENTIAL.ERROR.SUBSCRIBE_REFERENTIAL_ERROR',
        },
      })
      .pipe(
        map(({ data }) => {
          const entity = opts?.toEntity !== false ? data && this.fromObject(data) : data;
          if (entity && this._debug) console.debug(`${this._logPrefix} [WS] Received changes on ${this._logTypeName} {${id}}`, entity);

          // TODO: when missing = deleted ?
          if (!entity) console.warn(`${this._logPrefix} [WS] Received deletion on ${this._logTypeName} {${id}} - TODO check implementation`);

          return entity;
        })
      );
  }

  equals(e1: E, e2: E): boolean {
    return e1 && e2 && (e1.id === e2.id || (isNotNil(e1.name) && e1.name === e2.name) || (isNotNil(e1.label) && e1.label === e2.label));
  }

  // Protected methods

  protected asObject(entity: E, opts?: EntityAsObjectOptions): any {
    const target = super.asObject(entity, opts);
    if (entity.__typename !== GenericReferential.TYPENAME) {
      delete target.entityName;
    }
    return target;
  }

  protected asPage(page: Partial<Page>, filter?: Partial<F>): Page {
    return {
      offset: page.offset || 0,
      size: page.size || 100,
      sortBy:
        page.sortBy ||
        filter?.criterias?.find((criteria) => isNotEmptyArray(criteria.searchAttributes))?.searchAttributes[0] ||
        this.defaultSortBy.toString(),
      sortDirection: page.sortDirection || this.defaultSortDirection,
    };
  }

  protected buildWatchVariables(page: Partial<Page>, filter?: Partial<F>, opts?: WO): any {
    return {
      entityName: opts?.entityName,
      page: {
        ...page,
        sortDirection: page?.sortDirection?.toUpperCase(),
      },
      filter: filter?.asPodObject(),
    };
  }

  protected buildLoadVariables(page: Partial<Page>, filter?: Partial<F>, opts?: LO): any {
    return {
      entityName: opts?.entityName,
      page: {
        ...page,
        sortDirection: page?.sortDirection?.toUpperCase(),
      },
      filter: filter?.asPodObject(),
    };
  }

  protected distinctResultComparator(result1: LoadResult<E>, result2: LoadResult<E>): boolean {
    if ((!!result1.total && !!result2.total && result1.total !== result2.total) || arraySize(result1.data) !== arraySize(result2.data)) return false;

    return result1.data.every(
      (value, index) =>
        value.__typename === result2.data[index].__typename &&
        value.id === result2.data[index].id &&
        value.updateDate === result2.data[index].updateDate
    );
  }

  protected getLoadQueries(): DocumentNode[] {
    return [this.queries.loadAll, this.queries.loadAllLight, this.queries.loadAllWithTotal, this.queries.loadAllLightWithTotal].filter(isNotNil);
  }
}
