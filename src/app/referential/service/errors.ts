import { errorCodes } from '@app/shared/errors';

export const referentialErrorCodes = {
  ...errorCodes,

  missingEntityName: 0,
  invalidEntityName: 1,

  importReferentialError: 500,
};
