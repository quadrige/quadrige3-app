import { Directive, Injector, input, OnInit } from '@angular/core';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { IEntity, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { IBaseTable } from '@app/shared/table/table.model';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { map, startWith } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { IRightModel } from '@app/referential/right/right.model';

@Directive()
export abstract class BaseRightTable<E extends IEntity<E, string> & IRightModel> extends BaseMemoryTable<E, string, any, any> implements OnInit {
  parentTable = input.required<IBaseTable<any, any>>();
  protected parentI18n: string;
  protected parentAttributes: string[];

  protected constructor(
    protected injector: Injector,
    protected rightColumns: string[],
    public readonly dataType: new () => E,
    protected entityMemoryService: EntitiesMemoryService<E, any, string>
  ) {
    super(
      injector,
      RESERVED_START_COLUMNS.concat(['parent'])
        .concat(...rightColumns)
        .concat(RESERVED_END_COLUMNS),
      dataType,
      entityMemoryService,
      undefined
    );
  }

  ngOnInit() {
    if (!this.parentTable()) {
      throw new Error(`${this.logPrefix} No parent table found!`);
    }
    switch (this.parentTable().entityName) {
      case 'User': {
        this.parentI18n = 'REFERENTIAL.ENTITY.USER';
        this.parentAttributes = referentialOptions.user.attributes;
        break;
      }
      case 'Department': {
        this.parentI18n = 'REFERENTIAL.ENTITY.DEPARTMENT';
        this.parentAttributes = referentialOptions.department.attributes;
        break;
      }
      default: {
        throw new Error(`${this.logPrefix} Entity '${this.parentTable().entityName}' not handled!`);
      }
    }

    this.registerSubscription(
      this.parentTable()
        .selection.changed.pipe(
          map((value) => value.source),
          startWith(this.parentTable().selection)
        )
        .subscribe((selectionModel) => this.toggleParentColumn(selectionModel))
    );

    this.subTable = true;
    this.showPaginator = true;
    super.ngOnInit();
  }

  protected toggleParentColumn(selection: SelectionModel<any>) {
    this.setShowColumn('parent', selection.selected?.length > 1);
  }

  getI18nColumnName(columnName: string): string {
    if (columnName === 'parent') {
      return this.parentI18n;
    }
    return super.getI18nColumnName(columnName);
  }

  protected getRequiredColumns(): string[] {
    const requiredColumns = super.getRequiredColumns().slice();
    if (this.getShowColumn('parent') && !requiredColumns.includes('parent')) {
      requiredColumns.push('parent');
    }
    return requiredColumns.concat(...this.rightColumns);
  }
}
