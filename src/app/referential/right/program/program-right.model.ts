import { Entity, EntityAsObjectOptions, EntityClass, toBoolean } from '@sumaris-net/ngx-components';
import { StrReferential } from '@app/referential/model/referential.model';
import { StrategyRight } from '@app/referential/right/strategy/strategy-right.model';
import { IRightModel } from '@app/referential/right/right.model';

@EntityClass({ typename: 'ProgramRightVO' })
export class ProgramRight extends Entity<ProgramRight, string> implements IRightModel {
  static entityName = 'ProgramRight';
  static fromObject: (source: any, opts?: any) => ProgramRight;

  parent: StrReferential = null;
  program: StrReferential = null;
  manager: boolean = null;
  recorder: boolean = null;
  fullViewer: boolean = null;
  viewer: boolean = null;
  validator: boolean = null;
  strategyRights: StrategyRight[] = null;

  constructor() {
    super(ProgramRight.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.parent = StrReferential.fromObject(source.parent);
    this.program = StrReferential.fromObject(source.program);
    this.manager = toBoolean(source.manager, false);
    this.recorder = toBoolean(source.recorder, false);
    this.fullViewer = toBoolean(source.fullViewer, false);
    this.viewer = toBoolean(source.viewer, false);
    this.validator = toBoolean(source.validator, false);
    this.strategyRights = source.strategyRights?.map(StrategyRight.fromObject) || [];
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parent = this.parent?.asObject(opts);
    target.program = this.program?.asObject(opts);
    target.strategyRights = this.strategyRights?.map((value) => value.asObject(opts));
    return target;
  }
}
