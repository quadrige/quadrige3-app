import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, viewChild } from '@angular/core';
import { ProgramRightService } from '@app/referential/right/program/program-right.service';
import { ProgramRight } from '@app/referential/right/program/program-right.model';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { BaseRightTable } from '@app/referential/right/base-right.table';
import { StrategyRightTable } from '@app/referential/right/strategy/strategy-right.table';
import { StrategyRight } from '@app/referential/right/strategy/strategy-right.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-program-right-table',
  templateUrl: './program-right.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: ProgramRightService, useExisting: false }],
  standalone: true,
  imports: [QuadrigeCoreModule, StrategyRightTable],
})
export class ProgramRightTable extends BaseRightTable<ProgramRight> implements AfterViewInit {
  protected programAttributes = referentialOptions.program.attributes;

  strategyRightTable = viewChild<StrategyRightTable>('strategyRightTable');

  constructor(
    protected injector: Injector,
    protected _entityService: ProgramRightService
  ) {
    super(
      injector,
      // columns
      ['program', 'manager', 'recorder', 'fullViewer', 'viewer', 'validator'],
      ProgramRight,
      _entityService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.RIGHT.PROGRAM.';
    this.defaultSortBy = 'program';
    this.logPrefix = '[program-right-table]';
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.registerSubForm(this.strategyRightTable());
    this.restoreRightPanel(true, 50);
  }

  async loadRightArea() {
    if (this.selection.isEmpty()) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.strategyRightTable().setValue(undefined);
      return;
    }

    await this.strategyRightTable().waitIdle();
    this.strategyRightTable().markAsLoading();
    const strategyRights: StrategyRight[] = [];
    this.selection.selected
      .map((row) => row.currentData)
      .forEach((programRight) => {
        strategyRights.push(...programRight.strategyRights);
      });
    await this.strategyRightTable().setValue(strategyRights);
  }
}
