import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { ProgramRight } from '@app/referential/right/program/program-right.model';
import { Injectable } from '@angular/core';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';
import { strategyRightFragment } from '@app/referential/right/strategy/strategy-right.service';

export const programRightFragment = gql`
  fragment ProgramRightFragment on ProgramRightVO {
    id
    parent {
      ...ReferentialFragment
    }
    program {
      ...ReferentialFragment
    }
    manager
    recorder
    fullViewer
    viewer
    validator
    strategyRights {
      ...StrategyRightFragment
    }
  }
  ${referentialFragments.light}
  ${strategyRightFragment}
`;

@Injectable()
export class ProgramRightService extends UnfilteredEntitiesMemoryService<ProgramRight, string> {
  constructor() {
    super(ProgramRight);
  }
}
