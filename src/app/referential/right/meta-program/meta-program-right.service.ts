import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { Injectable } from '@angular/core';
import { MetaProgramRight } from '@app/referential/right/meta-program/meta-program-right.model';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';

export const metaProgramRightFragment = gql`
  fragment MetaProgramRightFragment on MetaProgramRightVO {
    id
    parent {
      ...ReferentialFragment
    }
    metaProgram {
      ...ReferentialFragment
    }
    manager
  }
  ${referentialFragments.light}
`;

@Injectable()
export class MetaProgramRightService extends UnfilteredEntitiesMemoryService<MetaProgramRight, string> {
  constructor() {
    super(MetaProgramRight);
  }
}
