import { Entity, EntityAsObjectOptions, EntityClass, toBoolean } from '@sumaris-net/ngx-components';
import { StrReferential } from '@app/referential/model/referential.model';
import { IRightModel } from '@app/referential/right/right.model';

@EntityClass({ typename: 'MetaProgramRightVO' })
export class MetaProgramRight extends Entity<MetaProgramRight, string> implements IRightModel {
  static entityName = 'MetaProgramRight';
  static fromObject: (source: any, opts?: any) => MetaProgramRight;

  parent: StrReferential = null;
  metaProgram: StrReferential = null;
  manager: boolean = null;

  constructor() {
    super(MetaProgramRight.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.parent = StrReferential.fromObject(source.parent);
    this.metaProgram = StrReferential.fromObject(source.metaProgram);
    this.manager = toBoolean(source.manager, false);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parent = this.parent?.asObject(opts);
    target.metaProgram = this.metaProgram?.asObject(opts);
    return target;
  }
}
