import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { MetaProgramRight } from '@app/referential/right/meta-program/meta-program-right.model';
import { MetaProgramRightService } from '@app/referential/right/meta-program/meta-program-right.service';
import { BaseRightTable } from '@app/referential/right/base-right.table';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-meta-program-right-table',
  templateUrl: './meta-program-right.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: MetaProgramRightService, useExisting: false }],
  standalone: true,
  imports: [QuadrigeCoreModule],
})
export class MetaProgramRightTable extends BaseRightTable<MetaProgramRight> {
  protected metaProgramAttributes = referentialOptions.metaProgram.attributes;

  constructor(
    protected injector: Injector,
    protected _entityService: MetaProgramRightService
  ) {
    super(
      injector,
      // columns
      ['metaProgram', 'manager'],
      MetaProgramRight,
      _entityService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.RIGHT.META_PROGRAM.';
    this.defaultSortBy = 'metaProgram';
    this.logPrefix = '[meta-program-right-table]';
  }
}
