import { StrReferential } from '@app/referential/model/referential.model';

export interface IRightModel {
  parent: StrReferential;
}

export interface RightFetchOptions {
  entityName: string;

  privilegeId?: string;

  programId?: string;
  programPrivilegeId?: number;

  strategyId?: number;

  metaProgramId?: string;

  ruleListId?: string;

  extractFilterId?: number;
}
