import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { RuleListRightService } from '@app/referential/right/rule-list/rule-list-right.service';
import { RuleListRight } from '@app/referential/right/rule-list/rule-list-right.model';
import { BaseRightTable } from '@app/referential/right/base-right.table';
import { Department } from '@app/referential/department/department.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-rule-list-right-table',
  templateUrl: './rule-list-right.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: RuleListRightService, useExisting: false }],
  standalone: true,
  imports: [QuadrigeCoreModule],
})
export class RuleListRightTable extends BaseRightTable<RuleListRight> implements OnInit {
  protected ruleListAttributes = referentialOptions.ruleList.attributes;

  constructor(
    protected injector: Injector,
    protected _entityService: RuleListRightService
  ) {
    super(
      injector,
      // columns
      ['ruleList', 'responsible', 'controlled'],
      RuleListRight,
      _entityService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.RIGHT.RULE_LIST.';
    this.defaultSortBy = 'ruleList';
    this.logPrefix = '[rule-list-right-table]';
  }

  ngOnInit() {
    super.ngOnInit();
    this.setShowColumn('controlled', this.parentTable().entityName === Department.entityName);
  }
}
