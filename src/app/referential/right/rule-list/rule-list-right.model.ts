import { Entity, EntityAsObjectOptions, EntityClass, toBoolean } from '@sumaris-net/ngx-components';
import { StrReferential } from '@app/referential/model/referential.model';
import { IRightModel } from '@app/referential/right/right.model';

@EntityClass({ typename: 'RuleListRightVO' })
export class RuleListRight extends Entity<RuleListRight, string> implements IRightModel {
  static entityName = 'RuleListRight';
  static fromObject: (source: any, opts?: any) => RuleListRight;

  parent: StrReferential = null;
  ruleList: StrReferential = null;
  responsible: boolean = null;
  controlled: boolean = null;

  constructor() {
    super(RuleListRight.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.parent = StrReferential.fromObject(source.parent);
    this.ruleList = StrReferential.fromObject(source.ruleList);
    this.responsible = toBoolean(source.responsible, false);
    this.controlled = toBoolean(source.controlled, false);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parent = this.parent?.asObject(opts);
    target.ruleList = this.ruleList?.asObject(opts);
    return target;
  }
}
