import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { Injectable } from '@angular/core';
import { RuleListRight } from '@app/referential/right/rule-list/rule-list-right.model';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';

export const ruleListRightFragment = gql`
  fragment RuleListRightFragment on RuleListRightVO {
    id
    parent {
      ...ReferentialFragment
    }
    ruleList {
      ...ReferentialFragment
    }
    responsible
    controlled
  }
  ${referentialFragments.light}
`;

@Injectable()
export class RuleListRightService extends UnfilteredEntitiesMemoryService<RuleListRight, string> {
  constructor() {
    super(RuleListRight);
  }
}
