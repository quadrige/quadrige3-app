import { ChangeDetectionStrategy, Component, Injector, input, OnInit } from '@angular/core';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { BaseRightTable } from '@app/referential/right/base-right.table';
import { StrategyRightService } from '@app/referential/right/strategy/strategy-right.service';
import { StrategyRight } from '@app/referential/right/strategy/strategy-right.model';
import { IBaseTable } from '@app/shared/table/table.model';
import { map, startWith } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { Department } from '@app/referential/department/department.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-strategy-right-table',
  templateUrl: './strategy-right.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: StrategyRightService, useExisting: false }],
  standalone: true,
  imports: [QuadrigeCoreModule],
})
export class StrategyRightTable extends BaseRightTable<StrategyRight> implements OnInit {
  programTable = input.required<IBaseTable<any, any>>();
  protected programAttributes = referentialOptions.program.attributes;
  protected strategyAttributes = referentialOptions.strategy.attributes;

  constructor(
    protected injector: Injector,
    protected _entityService: StrategyRightService
  ) {
    super(
      injector,
      // columns
      ['program', 'strategy', 'responsible', 'sampler', 'analyst'],
      StrategyRight,
      _entityService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.RIGHT.STRATEGY.';
    this.defaultSortBy = 'strategy';
    this.logPrefix = '[strategy-right-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    if (!this.programTable()) {
      throw new Error(`${this.logPrefix} No program table found!`);
    }

    this.registerSubscription(
      this.programTable()
        .selection.changed.pipe(
          map((value) => value.source),
          startWith(this.programTable().selection)
        )
        .subscribe((selectionModel) => this.toggleProgramColumn(selectionModel))
    );

    this.setShowColumn('sampler', this.parentTable().entityName === Department.entityName);
    this.setShowColumn('analyst', this.parentTable().entityName === Department.entityName);
  }

  protected toggleProgramColumn(selection: SelectionModel<any>) {
    this.setShowColumn('program', selection.selected?.length > 1);
  }

  protected getRequiredColumns(): string[] {
    const requiredColumns = super.getRequiredColumns().slice();
    if (this.getShowColumn('program') && !requiredColumns.includes('program')) {
      requiredColumns.push('program');
    }
    return requiredColumns.concat(...this.rightColumns);
  }

  async setValue(value: StrategyRight[], opts?: { emitEvent?: boolean }): Promise<void> {
    await super.setValue(value, opts);
    // Will update paginator height
    this.rightSplitResized();
  }
}
