import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { Injectable } from '@angular/core';
import { StrategyRight } from '@app/referential/right/strategy/strategy-right.model';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';

export const strategyRightFragment = gql`
  fragment StrategyRightFragment on StrategyRightVO {
    id
    parent {
      ...ReferentialFragment
    }
    program {
      ...ReferentialFragment
    }
    strategy {
      ...ReferentialFragment
    }
    responsible
    sampler
    analyst
  }
  ${referentialFragments.light}
`;

@Injectable()
export class StrategyRightService extends UnfilteredEntitiesMemoryService<StrategyRight, string> {
  constructor() {
    super(StrategyRight);
  }
}
