import { Entity, EntityAsObjectOptions, EntityClass, toBoolean } from '@sumaris-net/ngx-components';
import { StrReferential } from '@app/referential/model/referential.model';
import { IRightModel } from '@app/referential/right/right.model';

@EntityClass({ typename: 'StrategyRightVO' })
export class StrategyRight extends Entity<StrategyRight, string> implements IRightModel {
  static entityName = 'StrategyRight';
  static fromObject: (source: any, opts?: any) => StrategyRight;

  parent: StrReferential = null;
  program: StrReferential = null;
  strategy: StrReferential = null;
  responsible: boolean = null;
  sampler: boolean = null;
  analyst: boolean = null;

  constructor() {
    super(StrategyRight.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.parent = StrReferential.fromObject(source.parent);
    this.program = StrReferential.fromObject(source.program);
    this.strategy = StrReferential.fromObject(source.strategy);
    this.responsible = toBoolean(source.responsible, false);
    this.sampler = toBoolean(source.sampler, false);
    this.analyst = toBoolean(source.analyst, false);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parent = this.parent?.asObject(opts);
    target.program = this.program?.asObject(opts);
    target.strategy = this.strategy?.asObject(opts);
    return target;
  }
}
