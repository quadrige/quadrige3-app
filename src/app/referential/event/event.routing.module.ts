import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { inject, NgModule } from '@angular/core';
import { EventTable } from '@app/referential/event/event.table';
import { EventModule } from '@app/referential/event/event.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: EventTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), EventModule],
})
export class EventRoutingModule {}
