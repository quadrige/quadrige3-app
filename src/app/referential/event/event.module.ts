import { NgModule } from '@angular/core';
import { EventTable } from '@app/referential/event/event.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { EventFilterForm } from '@app/referential/event/filter/event.filter.form';

@NgModule({
  imports: [ReferentialModule, EventFilterForm],
  declarations: [EventTable],
  exports: [EventTable],
})
export class EventModule {}
