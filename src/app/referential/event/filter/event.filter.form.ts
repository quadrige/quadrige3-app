import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { EventFilter, EventFilterCriteria } from '@app/referential/event/filter/event.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-event-filter-form',
  templateUrl: './event.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventFilterForm extends ReferentialCriteriaFormComponent<EventFilter, EventFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idDescription;
  }

  protected criteriaToQueryParams(criteria: EventFilterCriteria): PartialRecord<keyof EventFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'type');
    return params;
  }
}
