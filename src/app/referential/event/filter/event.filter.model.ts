import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Event } from '@app/referential/event/event.model';
import { IntReferential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'EventFilterCriteriaVO' })
export class EventFilterCriteria extends ReferentialFilterCriteria<Event, number> {
  static fromObject: (source: any, opts?: any) => EventFilterCriteria;

  typeId: number = null;
  type: IntReferential = null;

  constructor() {
    super(EventFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.type = source.type;
    this.typeId = source.typeId || source.type?.id;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.searchAttributes = target.searchAttributes || ['id', 'description'];
    target.typeId = this.typeId || this.type?.id;
    if (opts?.minify) {
      delete target.type;
    }
    // Remove unhandled properties (Mantis #63344)
    delete target.exactValues;
    return target;
  }
}

@EntityClass({ typename: 'EventFilterVO' })
export class EventFilter extends ReferentialFilter<EventFilter, EventFilterCriteria, Event> {
  static fromObject: (source: any, opts?: any) => EventFilter;

  constructor() {
    super(EventFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  criteriaFromObject(source: any, opts: any): EventFilterCriteria {
    return EventFilterCriteria.fromObject(source, opts);
  }
}
