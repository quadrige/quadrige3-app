import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { Event } from '@app/referential/event/event.model';
import { EventService } from '@app/referential/event/event.service';
import { EventValidator } from '@app/referential/event/event.validator';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { EventFilter, EventFilterCriteria } from '@app/referential/event/filter/event.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-event-table',
  templateUrl: './event.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventTable extends ReferentialTable<Event, number, EventFilter, EventFilterCriteria, EventValidator> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: EventService,
    protected validatorService: EventValidator
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['description', 'startDate', 'endDate', 'type', 'comments', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      Event,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.EVENT.';
    this.defaultSortBy = 'description';
    this.logPrefix = '[event-table]';
    this.setShowColumn('statusId', false);
  }

  ngOnInit() {
    super.ngOnInit();

    // EventType combo
    this.registerAutocompleteField('type', {
      ...this.referentialOptions.eventType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'EventType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('description');
  }
}
