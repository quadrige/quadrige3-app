import { Injectable } from '@angular/core';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { Event } from '@app/referential/event/event.model';
import { AbstractControlOptions, UntypedFormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { EventService } from '@app/referential/event/event.service';
import { PartialRecord } from '@app/shared/model/interface';
import { SharedFormGroupValidators, SharedValidators } from '@sumaris-net/ngx-components';

@Injectable({ providedIn: 'root' })
export class EventValidator extends ReferentialValidatorService<Event> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: EventService,
    protected translate: TranslateService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Event, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return <PartialRecord<keyof Event, any>>{
      id: [data?.id || null],
      description: [data?.description || null],
      startDate: [data?.startDate || null],
      endDate: [data?.endDate || null],
      type: [data?.type || null, SharedValidators.entity],
      comments: [data?.comments || null],
      positioningSystem: [data?.positioningSystem || null, SharedValidators.entity],
      positionComment: [data?.positionComment || null],
      recorderDepartmentId: [data?.recorderDepartmentId || null],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
    };
  }

  getFormGroupOptions(data?: Event, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [SharedFormGroupValidators.dateRange('startDate', 'endDate')],
    };
  }
}
