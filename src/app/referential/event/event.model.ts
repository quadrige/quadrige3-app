import { EntityClass, fromDateISOString, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';

@EntityClass({ typename: 'EventVO' })
export class Event extends Referential<Event> {
  static entityName = 'Event';
  static fromObject: (source: any, opts?: any) => Event;

  type: IntReferential = null;
  startDate: Moment = null;
  endDate: Moment = null;
  positioningSystem: IntReferential = null;
  positionComment: string = null;
  recorderDepartmentId: number = null;

  constructor() {
    super(Event.TYPENAME);
    this.entityName = Event.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Event.entityName;
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
    this.type = IntReferential.fromObject(source.type);
    this.positioningSystem = IntReferential.fromObject(source.positioningSystem);
    this.positionComment = source.positionComment;
    this.recorderDepartmentId = source.recorderDepartmentId;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.startDate = Dates.toLocalDateString(this.startDate);
    target.endDate = Dates.toLocalDateString(this.endDate);
    target.type = this.type?.asObject(opts);
    target.positioningSystem = this.positioningSystem?.asObject(opts);
    return target;
  }
}
