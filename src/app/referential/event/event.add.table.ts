import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { EventService } from '@app/referential/event/event.service';
import { EventValidator } from '@app/referential/event/event.validator';
import { EventTable } from '@app/referential/event/event.table';
import { IAddTable } from '@app/shared/table/table.model';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { EventFilter } from '@app/referential/event/filter/event.filter.model';
import { Event } from '@app/referential/event/event.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { EventFilterForm } from '@app/referential/event/filter/event.filter.form';

@Component({
  selector: 'app-event-add-table',
  templateUrl: './event.table.html',
  standalone: true,
  imports: [ReferentialModule, EventFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventAddTable extends EventTable implements IAddTable<Event> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: EventService,
    protected validatorService: EventValidator
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[event-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: EventFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
