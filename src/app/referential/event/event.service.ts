import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { Injectable, Injector } from '@angular/core';
import { Event } from '@app/referential/event/event.model';
import { EventFilter, EventFilterCriteria } from '@app/referential/event/filter/event.filter.model';

export const eventFragment = gql`
  fragment EventFragment on EventVO {
    id
    type {
      ...ReferentialFragment
    }
    description
    startDate
    endDate
    comments
    positioningSystem {
      ...ReferentialFragment
    }
    positionComment
    recorderDepartmentId
    creationDate
    updateDate
  }
  ${referentialFragments.light}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Events($page: PageInput, $filter: EventFilterVOInput) {
      data: events(page: $page, filter: $filter) {
        ...EventFragment
      }
    }
    ${eventFragment}
  `,
  loadAllWithTotal: gql`
    query EventsWithTotal($page: PageInput, $filter: EventFilterVOInput) {
      data: events(page: $page, filter: $filter) {
        ...EventFragment
      }
      total: eventsCount(filter: $filter)
    }
    ${eventFragment}
  `,
  countAll: gql`
    query EventsCount($filter: EventFilterVOInput) {
      total: eventsCount(filter: $filter)
    }
  `,
};

const mutations: BaseEntityGraphqlMutations = {};

@Injectable({ providedIn: 'root' })
export class EventService extends BaseReferentialService<Event, EventFilter, EventFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Event, EventFilter, { queries, mutations });
    this._logPrefix = '[event-service]';
  }
}
