import { EntityType } from '@app/referential/model/referential.model';
import { GenericService } from '@app/referential/generic/generic.service';
import {
  arraySize,
  capitalizeFirstLetter,
  changeCaseToUnderscore,
  isNotEmptyArray,
  isNotNil,
  isNotNilOrBlank,
  TranslateContextService,
} from '@sumaris-net/ngx-components';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { FilterHint } from '@app/shared/model/filter.model';
import { Utils } from '@app/shared/utils';
import { attributes } from '@app/referential/model/referential.constants';
import { IReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';
import { ReferentialUtils } from '@app/referential/model/referential.utils';

export class ReferentialFilterUtils {
  static async toString(
    filter: IReferentialFilterCriteria<any>,
    opts: {
      type: EntityType;
      service: GenericService;
      translate: TranslateContextService;
      font?: string;
      maxWidth?: number;
      systemId?: TranscribingSystemType;
    }
  ): Promise<FilterHint> {
    const result: FilterHint = new FilterHint();

    if (isNotNil(filter)) {
      const hasSearchText = isNotNilOrBlank(filter.searchText);
      const hasIds = isNotEmptyArray(filter.includedIds) || isNotEmptyArray(filter.excludedIds);

      // Render searchText
      if (hasSearchText) {
        result.prefix = '&#8618;';
        const i18nContext = changeCaseToUnderscore(opts.type.name).toUpperCase();
        const searchAttributes: string[] = isNotEmptyArray(filter.searchAttributes)
          ? filter.searchAttributes
          : TranscribingItemTypeUtils.defaultSearchAttributesForSystem(opts.type, opts.systemId);
        result.fields = capitalizeFirstLetter(
          searchAttributes
            .map((searchAttribute) => {
              const attribute = changeCaseToUnderscore(opts.type.idIsString && searchAttribute === 'id' ? 'code' : searchAttribute).toUpperCase();
              const i18nKey = `REFERENTIAL.${attribute}`;
              return Utils.contextualI18nKey(opts.translate, i18nKey, i18nContext);
            })
            .map((key) => opts.translate.instant(key))
            .join(', ')
        );
        result.contains = opts.translate.instant('REFERENTIAL.FILTER.CONTAINS');
        result.searchText = filter.searchText;
      }
      // Render includedIds
      if (hasIds) {
        result.prefix = '&#8618;';
        const exclude = isNotEmptyArray(filter.excludedIds);
        const ids: any[] = exclude ? filter.excludedIds.slice() : filter.includedIds.slice();

        if (hasSearchText) {
          result.includeOrExclude = opts.translate.instant(exclude ? 'REFERENTIAL.FILTER.EXCLUDES' : 'REFERENTIAL.FILTER.INCLUDES');
          result.separator = exclude ? ', ' : ` ${opts.translate.instant('REFERENTIAL.FILTER.OR')} `;
        } else {
          result.includeOrExclude = capitalizeFirstLetter(
            opts.translate.instant(exclude ? 'REFERENTIAL.FILTER.ALL_EXCLUDES' : 'REFERENTIAL.FILTER.ALL_INCLUDES')
          );
        }
        let optimalRender = !!opts.font && !!opts.maxWidth && opts.type.name !== 'Pmfmu';
        const font = 'bold italic ' + opts.font;
        if (optimalRender) {
          // Use text width optimal rendering
          const values: string[] = [];
          do {
            let referentialString = '';
            if (TranscribingItemTypeUtils.isDefaultSystem(opts.systemId)) {
              // Load referential
              const id = ids[0];
              const referential = await opts.service.load(id.toString(), { entityName: opts.type.name });
              referentialString = ReferentialUtils.toString(referential, opts.type);
            } else {
              // Use direct id
              referentialString = ids[0];
            }
            const tempValues = [...values, referentialString];
            result.list = tempValues.join(', ');
            const tempResult = result.toString();
            const textWidth = Utils.textWidth(tempResult, font);
            if (textWidth < opts.maxWidth) {
              values.push(referentialString);
              ids.shift();
            } else {
              break;
            }
          } while (arraySize(ids) > 0);

          if (isNotEmptyArray(values)) {
            // If some values can be rendered
            result.list = values.join(', ');
            const resultString = result.toString();
            const remaining = arraySize(ids);
            result.remaining =
              remaining > 0 ? opts.translate.instant('REFERENTIAL.FILTER.VALUES_COUNT_REMAINING', undefined, { length: remaining }) : undefined;
            if (result.remaining) {
              if (Utils.textWidth(resultString.concat(result.remaining), font) > opts.maxWidth) {
                // remove last value if final text is too long
                values.pop();
                result.list = values.join(', ');
                result.remaining = opts.translate.instant('REFERENTIAL.FILTER.VALUES_COUNT_REMAINING', undefined, { length: remaining + 1 });
              }
            }
          } else {
            // If no value, fallback to display count only
            optimalRender = false;
          }
        }

        if (!optimalRender) {
          result.list = opts.translate.instant('REFERENTIAL.FILTER.VALUES_COUNT', undefined, {
            length: arraySize(ids),
          });
        }
      }
    }

    return result;
  }

  static defaultSearchAttributes(type: EntityType): string[] {
    const searchAttributes = attributes.id.slice();
    if (type?.labelPresent && type?.name !== 'Strategy') searchAttributes.push('label');
    searchAttributes.push('name');
    if (type?.name === 'Method' || type?.name === 'Strategy') {
      searchAttributes.push('description');
    }
    if (type?.name === 'User') {
      searchAttributes.push('firstName');
    }
    if (type?.name === 'Unit') {
      searchAttributes.push('symbol');
    }
    return searchAttributes;
  }
}
