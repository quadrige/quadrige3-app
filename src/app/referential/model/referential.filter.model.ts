import { BaseReferential, EntityAsObjectOptions, EntityClass, FilterFn, isNotNil } from '@sumaris-net/ngx-components';
import { IntReferential, StrReferential } from '@app/referential/model/referential.model';
import {
  BaseEntityFilter,
  BaseEntityFilterCriteria,
  BaseFilterAsObjectOptions,
  IBaseEntityFilter,
  IBaseEntityFilterCriteria,
} from '@app/shared/model/filter.model';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

export interface IReferentialFilterCriteria<ID> extends IBaseEntityFilterCriteria<ID> {
  parentId?: string;
  statusId?: number;
  exactValues?: any[];
  systemId?: TranscribingSystemType;
}

export abstract class ReferentialFilterCriteria<E extends BaseReferential<any, ID>, ID>
  extends BaseEntityFilterCriteria<E, ID>
  implements IReferentialFilterCriteria<ID>
{
  parentId?: string = null;
  statusId?: number = null;
  exactValues?: any[] = null;
  systemId?: TranscribingSystemType = null;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.parentId = source.parentId;
    this.statusId = source.statusId ?? source.statusIds?.[0];
    this.exactValues = source.exactValues;
    this.systemId = source.systemId ?? 'QUADRIGE';
  }

  asObject(opts?: BaseFilterAsObjectOptions): any {
    const target = super.asObject(opts);
    if (opts?.minify) {
      target.statusIds = isNotNil(this.statusId) ? [this.statusId] : [];
      delete target.statusId;
      if (!opts?.keepMinifiedSystemId) {
        delete target.systemId;
      }
    }
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'systemId') return false;
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  protected buildFilter(): FilterFn<E>[] {
    const filterFns = super.buildFilter() || [];

    // Filter by status
    if (isNotNil(this.statusId)) {
      filterFns.push((entity) => entity.statusId === this.statusId);
    }

    return filterFns;
  }
}

export type IReferentialFilter<ID, C extends IReferentialFilterCriteria<ID>> = IBaseEntityFilter<ID, C>;

export abstract class ReferentialFilter<
    F extends BaseEntityFilter<F, C, E, ID, AO, FO>,
    C extends ReferentialFilterCriteria<E, ID>,
    E extends BaseReferential<E, ID>,
    ID = number,
    AO extends EntityAsObjectOptions = EntityAsObjectOptions,
    FO = any,
  >
  extends BaseEntityFilter<F, C, E, ID, AO, FO>
  implements IReferentialFilter<ID, C> {}

@EntityClass({ typename: 'IntReferentialFilterCriteriaVO' })
export class IntReferentialFilterCriteria extends ReferentialFilterCriteria<IntReferential, number> {
  static fromObject: (source: any, opts?: any) => IntReferentialFilterCriteria;

  constructor() {
    super(IntReferentialFilterCriteria.TYPENAME);
  }
}

@EntityClass({ typename: 'IntReferentialFilterVO' })
export class IntReferentialFilter extends ReferentialFilter<IntReferentialFilter, IntReferentialFilterCriteria, IntReferential> {
  static fromObject: (source: any, opts?: any) => IntReferentialFilter;

  constructor() {
    super(IntReferentialFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts?: any): IntReferentialFilterCriteria {
    return IntReferentialFilterCriteria.fromObject(source || {}, opts);
  }
}

@EntityClass({ typename: 'StrReferentialFilterCriteriaVO' })
export class StrReferentialFilterCriteria extends ReferentialFilterCriteria<StrReferential, string> {
  static fromObject: (source: any, opts?: any) => StrReferentialFilterCriteria;

  constructor() {
    super(StrReferentialFilterCriteria.TYPENAME);
  }
}

@EntityClass({ typename: 'StrReferentialFilterVO' })
export class StrReferentialFilter extends ReferentialFilter<StrReferentialFilter, StrReferentialFilterCriteria, StrReferential, string> {
  static fromObject: (source: any, opts?: any) => StrReferentialFilter;

  constructor() {
    super(StrReferentialFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts?: any): StrReferentialFilterCriteria {
    return StrReferentialFilterCriteria.fromObject(source || {}, opts);
  }
}
