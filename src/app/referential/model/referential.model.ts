import { BaseReferential, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { TranscribingItem } from '@app/referential/transcribing-item/transcribing-item.model';
import { entitiesWithoutTranscribingItems } from '@app/referential/model/referential.constants';

export interface EntityType {
  name: string;
  idIsString: boolean;
  idIsComposite: boolean;
  labelPresent: boolean;
  namePresent: boolean;
  descriptionPresent: boolean;
  commentPresent: boolean;
  creationDatePresent: boolean;
  statusPresent: boolean;
}

export interface IWithTranscribingItems {
  transcribingItems: TranscribingItem[];
  transcribingItemsLoaded: boolean;
}

/**
 * Base class for referential objects with transcribing items
 */
export abstract class Referential<
    E extends Referential<any, ID, O>,
    ID = number,
    O extends ReferentialAsObjectOptions = ReferentialAsObjectOptions,
    FO = any,
  >
  extends BaseReferential<E, ID, O, FO>
  implements IWithTranscribingItems
{
  transcribingItems: TranscribingItem[] = null;
  transcribingItemsLoaded = false;

  protected constructor(typename?: string) {
    super(typename);
    this.id = null;
    this.name = null;
    this.label = null;
    this.description = null;
    this.comments = null;
    this.creationDate = null;
    this.statusId = null;
    this.validityStatusId = null;
    this.levelId = null;
    this.parentId = null;
    this.entityName = null;
  }

  fromObject(source: any, opts?: FO) {
    super.fromObject(source, opts);
    this.transcribingItems = source.transcribingItems?.map(TranscribingItem.fromObject);
    this.transcribingItemsLoaded = source.transcribingItemsLoaded;
  }

  asObject(opts?: O): any {
    const target = super.asObject(opts);
    if (entitiesWithoutTranscribingItems.includes(this.entityName)) {
      delete target.transcribingItems;
      delete target.transcribingItemsLoaded;
    } else {
      target.transcribingItems = this.transcribingItems?.map((value) => value.asObject(opts));
    }
    // Remove unused properties from Sumaris
    delete target.validityStatusId;
    delete target.levelId;
    delete target.parentId; // Usually not used in referential (except for TranscribingItem and TranscribingItemType)
    return target;
  }
}

/** Light referential object with numeric id */
export class IntReferential extends Referential<any> {
  static fromObject(source: any, opts?: any): IntReferential {
    if (!source) return source;
    const target = new IntReferential();
    target.fromObject(source, opts);
    return target;
  }
}

/** Light referential object with string id */
export class StrReferential extends Referential<any, string> {
  static fromObject(source: any, opts?: any): StrReferential {
    if (!source) return source;
    const target = new StrReferential();
    target.fromObject(source, opts);
    return target;
  }
}
