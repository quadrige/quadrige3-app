// TODO make this parameterizable because these ids are from database
import { MatAutocompleteFieldConfig } from '@sumaris-net/ngx-components';

export const privilegeEnum = {
  admin: '1',
};

// TODO make this parameterizable because these ids are from database
export const programPrivilegeEnum = {
  manager: 1,
  recorder: 2,
  fullViewer: 3,
  viewer: 4,
  validator: 5,
};

export type AcquisitionLevelType = 'PASS' | 'PREL' | 'ECHANT';
export const acquisitionLevels: AcquisitionLevelType[] = ['PASS', 'PREL', 'ECHANT'];

export const uiFunctions = ['SOMME', 'MOYENNE', 'ECART-TYPE', 'IC95'];

export const readOnlyEntities = ['QualityFlag', 'PrecisionType'];

export const entitiesTableWithoutSplit = ['Campaign', 'Event', 'Batch', 'Training'];

export const entitiesTableNeedingMoreWidth = ['TaxonName', 'Pmfmu'];

export const entitiesWithoutTranscribingItems = [
  'Account',
  'AppliedPeriod',
  'AppliedStrategy',
  'Configuration',
  'ControlRule',
  'DredgingAreaType',
  'DredgingTargetArea',
  'ExtractFilter',
  'Filter',
  'Fraction',
  'GeneralCondition',
  'Job',
  'Moratorium',
  'MetaProgramLocation',
  'OrderItem',
  'PmfmuAppliedStrategy',
  'RuleList',
  'Software',
  'Strategy',
  'Training',
  'TranscribingItemType',
  'TranscribingItem',
  'TranscribingItemRow',
  'User',
  'UserEvent',
  'UserTraining',
];

export const attributes = Object.freeze({
  id: ['id'],
  label: ['label'],
  name: ['name'],
  description: ['description'],
  nameId: ['name', 'id'],
  idName: ['id', 'name'],
  idDescription: ['id', 'description'],
  idNameDescription: ['id', 'name', 'description'],
  idLabel: ['id', 'label'],
  idLabelName: ['id', 'label', 'name'],
  labelName: ['label', 'name'],
  labelNameId: ['label', 'name', 'id'],
});

const defaultOptions = Object.freeze({
  nameOnly: <MatAutocompleteFieldConfig>{
    attributes: attributes.name,
    columnNames: ['REFERENTIAL.NAME'],
    columnSizes: [12],
  },
  nameFirst: <MatAutocompleteFieldConfig>{
    attributes: attributes.nameId,
    columnNames: ['REFERENTIAL.NAME', 'REFERENTIAL.ID'],
    columnSizes: [10, 2],
  },
  idFirst: <MatAutocompleteFieldConfig>{
    attributes: attributes.idName,
    columnNames: ['REFERENTIAL.CODE', 'REFERENTIAL.NAME'],
    columnSizes: [4, 8],
  },
  labelFirst: <MatAutocompleteFieldConfig>{
    attributes: attributes.labelNameId,
    columnNames: ['REFERENTIAL.LABEL', 'REFERENTIAL.NAME', 'REFERENTIAL.ID'],
    columnSizes: [3, 7, 2],
  },
});

export const referentialOptions = Object.freeze({
  resourceType: defaultOptions.nameFirst,
  taxonGroup: defaultOptions.labelFirst,
  taxonGroupType: defaultOptions.nameFirst,
  taxonomicLevel: defaultOptions.nameFirst,
  referenceTaxon: defaultOptions.nameFirst,
  positioningSystem: defaultOptions.nameFirst,
  positioningType: defaultOptions.idFirst,
  orderItemType: defaultOptions.idFirst,
  orderItem: defaultOptions.labelFirst,
  harbour: defaultOptions.idFirst,
  parameter: defaultOptions.idFirst,
  parameterGroup: defaultOptions.nameFirst,
  matrix: defaultOptions.nameFirst,
  fraction: defaultOptions.nameFirst,
  method: defaultOptions.nameFirst,
  qualitativeValue: defaultOptions.nameFirst,
  program: defaultOptions.idFirst,
  metaProgram: defaultOptions.idFirst,
  ruleList: defaultOptions.idFirst,
  strategy: defaultOptions.nameFirst,
  frequency: defaultOptions.nameFirst,
  analysisInstrument: defaultOptions.nameFirst,
  samplingEquipment: defaultOptions.nameFirst,
  precisionType: defaultOptions.nameFirst,
  depthLevel: defaultOptions.nameFirst,
  eventType: defaultOptions.nameFirst,
  codificationType: defaultOptions.nameOnly,
  transcribingSystem: defaultOptions.nameOnly,
  transcribingFunction: defaultOptions.nameOnly,
  transcribingLanguage: defaultOptions.nameOnly,

  department: <MatAutocompleteFieldConfig>{
    attributes: attributes.labelNameId,
    columnNames: ['REFERENTIAL.DEPARTMENT.LABEL', 'REFERENTIAL.DEPARTMENT.NAME', 'REFERENTIAL.ID'],
    columnSizes: [3, 7, 2],
  },
  training: <MatAutocompleteFieldConfig>{
    attributes: attributes.labelNameId,
    columnNames: ['REFERENTIAL.TRAINING.LABEL', 'REFERENTIAL.TRAINING.NAME', 'REFERENTIAL.ID'],
    columnSizes: [3, 7, 2],
  },
  user: <MatAutocompleteFieldConfig>{
    attributes: ['name', 'firstName', 'id'],
    columnNames: ['REFERENTIAL.USER.NAME', 'REFERENTIAL.USER.FIRST_NAME', 'REFERENTIAL.ID'],
    columnSizes: [5, 5, 2],
  },
  unit: <MatAutocompleteFieldConfig>{
    attributes: attributes.labelNameId,
    columnNames: ['REFERENTIAL.UNIT.SYMBOL', 'REFERENTIAL.UNIT.NAME', 'REFERENTIAL.ID'],
    columnSizes: [3, 7, 2],
  },
  monitoringLocation: <MatAutocompleteFieldConfig>{
    attributes: attributes.labelName,
    columnNames: ['REFERENTIAL.MONITORING_LOCATION.LABEL', 'REFERENTIAL.MONITORING_LOCATION.NAME'],
    columnSizes: [4, 8],
  },
  dredgingTargetArea: <MatAutocompleteFieldConfig>{
    attributes: ['name', 'description'],
    columnNames: ['REFERENTIAL.DREDGING_TARGET_AREA.TYPE', 'REFERENTIAL.DREDGING_TARGET_AREA.DESCRIPTION'],
    columnSizes: [6, 6],
  },
  pmfmu: <MatAutocompleteFieldConfig>{
    attributes: ['parameter.id', 'parameter.name', 'matrix.name', 'fraction.name', 'method.name', 'unit.label'],
    columnNames: ['REFERENTIAL.CODE', 'REFERENTIAL.NAME'], // todo not used yet but invalid
    columnSizes: [4, 8],
  },
});
