import { GenericReferential } from '@app/referential/generic/generic.model';
import { attributes, referentialOptions } from '@app/referential/model/referential.constants';
import { referentialToString, uncapitalizeFirstLetter } from '@sumaris-net/ngx-components';
import { IReferentialService } from '@app/referential/service/base-referential.service';
import { EntityType } from '@app/referential/model/referential.model';

export class ReferentialUtils {
  static toString(entity: GenericReferential, type: EntityType): string {
    let properties: string[] = referentialOptions[uncapitalizeFirstLetter(type.name)]?.attributes;
    if (!properties) {
      if (type.labelPresent) properties = attributes.label;
      else if (type.namePresent) properties = attributes.name;
      else if (type.descriptionPresent) properties = attributes.description;
      else properties = attributes.id;
    } else {
      // take only first
      properties = properties.slice(0, 1);
    }
    return referentialToString(entity, properties);
  }

  static async proposeUniqueName(service: IReferentialService<any, any, any, any, any, any>, criteria: any, name: string): Promise<string> {
    let suffix = 0;
    let uniqueName: string;
    // Check if initial name is unique
    let unique = !(await service.exists({ ...criteria, exactText: name }));
    // Loop with unique names
    while (!unique) {
      uniqueName = `${name} (${++suffix})`;
      unique = !(await service.exists({ ...criteria, exactText: uniqueName }));
    }
    return uniqueName || name;
  }
}
