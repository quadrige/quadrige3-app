import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { IStatus, splitById, StatusList } from '@sumaris-net/ngx-components';
import { ValidatorService } from '@e-is/ngx-material-table';
import { ReferentialGenericValidatorService } from '@app/referential/generic/generic.validator';
import { BaseForm } from '@app/shared/form/base.form';
import { GenericReferential } from '@app/referential/generic/generic.model';

@Component({
  selector: 'app-referential-form',
  templateUrl: './referential.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ValidatorService,
      useExisting: ReferentialGenericValidatorService,
    },
  ],
})
export class ReferentialForm extends BaseForm<GenericReferential> implements OnInit {
  @Input() showError = true;
  @Input() showDescription = true;
  @Input() showComments = true;
  @Input() entityName: string;

  statusById: { [id: number]: IStatus };
  private _statusList = StatusList;

  constructor(
    protected injector: Injector,
    protected validatorService: ValidatorService
  ) {
    super(injector, validatorService.getRowValidator());
  }

  get statusList(): Readonly<IStatus[]> {
    return this._statusList;
  }

  @Input()
  set statusList(values: Readonly<IStatus[]>) {
    this._statusList = values;

    // Fill statusById
    this.statusById = splitById(this._statusList);
  }

  ngOnInit() {
    super.ngOnInit();

    // Fill statusById
    if (this._statusList && !this.statusById) {
      this.statusById = splitById(this._statusList);
    }
  }

  setValue(data: GenericReferential, opts?: { emitEvent?: boolean; onlySelf?: boolean }) {
    super.setValue(data, opts);

    // Make sure to set entityName if set from Input()
    const entityNameControl = this.form.get('entityName');
    if (entityNameControl && this.entityName && entityNameControl.value !== this.entityName) {
      entityNameControl.setValue(this.entityName, opts);
    }
  }
}
