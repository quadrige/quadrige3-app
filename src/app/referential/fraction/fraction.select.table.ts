import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { FractionTable } from '@app/referential/fraction/fraction.table';
import { FractionService } from '@app/referential/fraction/fraction.service';
import { FractionValidatorService } from '@app/referential/fraction/fraction.validator';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { ISelectTable } from '@app/shared/table/table.model';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { FractionFilter } from '@app/referential/fraction/filter/fraction.filter.model';
import { Fraction } from '@app/referential/fraction/fraction.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { FractionFilterForm } from '@app/referential/fraction/filter/fraction.filter.form';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';

@Component({
  selector: 'app-fraction-select-table',
  templateUrl: './fraction.table.html',
  standalone: true,
  imports: [ReferentialModule, FractionFilterForm, FractionMatrixTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FractionSelectTable extends FractionTable implements ISelectTable<Fraction> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria;
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();

  constructor(
    protected injector: Injector,
    protected _entityService: FractionService,
    protected validatorService: FractionValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(injector, _entityService, validatorService, pmfmuService, strategyService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[fraction-select-table]';
    this.selectTable = true;
  }

  async resetFilter(filter?: FractionFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit();
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete).map((id) => id.toString());
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    await this.resetFilter();
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
