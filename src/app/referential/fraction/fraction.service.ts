import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Fraction } from '@app/referential/fraction/fraction.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { jobFragments } from '@app/social/job/job.service';
import { FractionFilter, FractionFilterCriteria } from '@app/referential/fraction/filter/fraction.filter.model';
import { fractionMatrixFragment, FractionMatrixUtils } from '@app/referential/fraction-matrix/fraction-matrix.model';
import { Matrix } from '@app/referential/matrix/matrix.model';
import { EntityUtils } from '@sumaris-net/ngx-components';

const fragments = {
  fraction: gql`
    fragment FractionFragment on FractionVO {
      id
      name
      description
      comments
      fractionMatrices {
        ...FractionMatrixFragment
      }
      updateDate
      creationDate
      statusId
      __typename
    }
    ${fractionMatrixFragment}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Fractions($page: PageInput, $filter: FractionFilterVOInput) {
      data: fractions(page: $page, filter: $filter) {
        ...FractionFragment
      }
    }
    ${fragments.fraction}
  `,

  loadAllWithTotal: gql`
    query FractionsWithTotal($page: PageInput, $filter: FractionFilterVOInput) {
      data: fractions(page: $page, filter: $filter) {
        ...FractionFragment
      }
      total: fractionsCount(filter: $filter)
    }
    ${fragments.fraction}
  `,

  countAll: gql`
    query FractionsCount($filter: FractionFilterVOInput) {
      total: fractionsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportFractionsAsync($filter: FractionFilterVOInput, $context: ExportContextInput) {
      data: exportFractionsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveFractions($data: [FractionVOInput]) {
      data: saveFractions(fractions: $data) {
        ...FractionFragment
      }
    }
    ${fragments.fraction}
  `,

  deleteAll: gql`
    mutation DeleteFractions($ids: [Int]) {
      deleteFractions(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class FractionService extends BaseReferentialService<Fraction, FractionFilter, FractionFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Fraction, FractionFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[fraction-service]';
  }

  copyIdAndUpdateDate(source: Matrix, target: Matrix) {
    super.copyIdAndUpdateDate(source, target);

    // Propagate to fraction-matrix
    if (source.fractionMatrices && target.fractionMatrices) {
      target.fractionMatrices.forEach((targetFractionMatrix) => {
        const sourceFractionMatrix = source.fractionMatrices.find((value) => FractionMatrixUtils.equals(value, targetFractionMatrix));
        EntityUtils.copyIdAndUpdateDate(sourceFractionMatrix, targetFractionMatrix);
      });
    }
  }
}
