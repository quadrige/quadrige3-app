import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { FractionTable } from '@app/referential/fraction/fraction.table';
import { FractionService } from '@app/referential/fraction/fraction.service';
import { FractionValidatorService } from '@app/referential/fraction/fraction.validator';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { FractionFilter } from '@app/referential/fraction/filter/fraction.filter.model';
import { Fraction } from '@app/referential/fraction/fraction.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { FractionFilterForm } from '@app/referential/fraction/filter/fraction.filter.form';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';

@Component({
  selector: 'app-fraction-add-table',
  templateUrl: './fraction.table.html',
  standalone: true,
  imports: [ReferentialModule, FractionFilterForm, FractionMatrixTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FractionAddTable extends FractionTable implements IAddTable<Fraction> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: FractionService,
    protected validatorService: FractionValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(injector, _entityService, validatorService, pmfmuService, strategyService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[fraction-add-table]';
    this.addTable = true;
  }

  protected getDefaultRightMenuItem(): any {
    return undefined;
  }

  async resetFilter(filter?: FractionFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
