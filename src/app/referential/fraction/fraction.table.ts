import { ChangeDetectionStrategy, Component, Injector, viewChild } from '@angular/core';
import {
  isNotEmptyArray,
  ObjectMap,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StatusIds,
} from '@sumaris-net/ngx-components';
import { Fraction } from '@app/referential/fraction/fraction.model';
import { FractionValidatorService } from '@app/referential/fraction/fraction.validator';
import { FractionService } from '@app/referential/fraction/fraction.service';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { Alerts } from '@app/shared/alerts';
import { EntityUtils } from '@app/shared/entity.utils';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { FractionFilter, FractionFilterCriteria } from '@app/referential/fraction/filter/fraction.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';
import { BaseColumnItem, ExportOptions } from '@app/shared/table/table.model';
import { Matrix } from '@app/referential/matrix/matrix.model';

type FractionView = 'fractionMatrixTable';

@Component({
  selector: 'app-fraction-table',
  templateUrl: './fraction.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FractionTable extends ReferentialTable<
  Fraction,
  number,
  FractionFilter,
  FractionFilterCriteria,
  FractionValidatorService,
  FractionView
> {
  fractionMatrixTable = viewChild<FractionMatrixTable>('fractionMatrixTable');

  readonly matrixMenuItem: IEntityMenuItem<Fraction, FractionView> = {
    title: 'REFERENTIAL.FRACTION.MATRICES',
    attribute: 'fractionMatrices',
    view: 'fractionMatrixTable',
  };
  rightAreaEnabled = false;

  constructor(
    protected injector: Injector,
    protected _entityService: FractionService,
    protected validatorService: FractionValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      Fraction,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.FRACTION.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[fraction-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent || (!this.rightPanelVisible && isNotEmptyArray(this.singleSelectedRow?.currentData?.fractionMatrices));
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<Fraction>): Promise<boolean> {
    row = row || this.singleEditingRow;

    if (row && !(await this.saveRightView(this.matrixMenuItem, this.fractionMatrixTable(), { row }))) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { ...opts, transcribingItemEnabled: true }); // Can export transcribing items
  }

  async buildExportAdditionalColumns(): Promise<ObjectMap<BaseColumnItem[]>> {
    let previousRightMenuItem = undefined;
    if (this.rightMenuItem !== this.matrixMenuItem) {
      previousRightMenuItem = this.rightMenuItem;
      this.rightMenuItem = this.matrixMenuItem;
      await this.loadRightArea();
    }
    const matrixPrefix = 'matrix.';
    const columns = (await this.fractionMatrixTable().buildExportColumns()).filter((column) => column.label.startsWith(matrixPrefix)); // Take matrix columns only
    if (!!previousRightMenuItem) {
      this.rightMenuItem = previousRightMenuItem;
      await this.loadRightArea();
    }

    const toAppend = this.translate.instant('REFERENTIAL.ENTITY.MATRIX');
    columns.forEach((column) => {
      column.label = column.label.substring(matrixPrefix.length);
      column.name = toAppend + ' - ' + column.name;
    });

    const result = {};
    result[Matrix.entityName] = columns;
    return result;
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(true, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<Fraction, FractionView>[] {
    return [this.matrixMenuItem];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Fraction, FractionView> {
    return this.matrixMenuItem;
  }

  protected async loadRightArea(row?: AsyncTableElement<Fraction>) {
    if (this.subTable) return; // don't load if sub table
    if (this.rightMenuItem?.view === 'fractionMatrixTable') {
      this.detectChanges();
      if (this.fractionMatrixTable()) {
        this.registerSubForm(this.fractionMatrixTable());
        await this.loadFractionMatrixTable(row);
      }
      return;
    }
    await super.loadRightArea(row);
  }

  protected async loadFractionMatrixTable(row?: AsyncTableElement<Fraction>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.fractionMatrixTable().setValue(undefined);
      this.fractionMatrixTable().parentId = undefined;
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected matrices
    await this.fractionMatrixTable().setValue((row.currentData.fractionMatrices || []).slice());
    this.fractionMatrixTable().parentId = row.currentData.id;
    this.rightAreaEnabled = this.canEdit;
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<Fraction>[]): Promise<boolean> {
    let confirmed = true;
    const fractionIds = EntityUtils.ids(dirtyRowsWithDisabledStatusId);

    // Check enabled pmfmu with this fraction
    const exists = await this.pmfmuService.exists(
      PmfmuFilterCriteria.fromObject({
        statusId: StatusIds.ENABLE,
        fractionFilter: { includedIds: fractionIds },
      })
    );
    if (exists) {
      confirmed = await Alerts.askConfirmation('REFERENTIAL.FRACTION.CONFIRM.DISABLE_PMFMU', this.alertCtrl, this.translate);
    }

    if (confirmed) {
      // Check if used in pmfmu applied strategies
      const res = await this.strategyService.loadPage(
        { offset: 0, size: 100 },
        StrategyFilter.fromObject({
          criterias: [
            {
              programFilter: { statusId: StatusIds.ENABLE },
              onlyActive: true,
              pmfmuFilter: {
                fractionFilter: {
                  includedIds: fractionIds,
                },
              },
            },
          ],
        })
      );

      if (isNotEmptyArray(res.data)) {
        confirmed = await Alerts.askConfirmation(
          'REFERENTIAL.FRACTION.CONFIRM.DISABLE_APPLIED_STRATEGIES',
          this.alertCtrl,
          this.translate,
          undefined,
          {
            list: `<div class="scroll-content"><ul>${res.data
              .map((r) => referentialToString(r, ['programId', 'name']))
              .map((s) => `<li>${s}</li>`)
              .join('')}</ul></div>`,
          }
        );
      }
    }

    return confirmed;
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.FRACTIONS' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('description', 'comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name');
  }
}
