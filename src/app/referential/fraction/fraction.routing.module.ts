import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { FractionTable } from '@app/referential/fraction/fraction.table';
import { FractionModule } from '@app/referential/fraction/fraction.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FractionTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), FractionModule],
})
export class FractionRoutingModule {}
