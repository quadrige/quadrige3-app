import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Fraction } from '@app/referential/fraction/fraction.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'FractionFilterCriteriaVO' })
export class FractionFilterCriteria extends ReferentialFilterCriteria<Fraction, number> {
  static fromObject: (source: any, opts?: any) => FractionFilterCriteria;

  matrixFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(FractionFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.matrixFilter = IntReferentialFilterCriteria.fromObject(source.matrixFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.matrixFilter = this.matrixFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'matrixFilter') return !BaseFilterUtils.isCriteriaEmpty(this.matrixFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'FractionFilterVO' })
export class FractionFilter extends ReferentialFilter<FractionFilter, FractionFilterCriteria, Fraction> {
  static fromObject: (source: any, opts?: any) => FractionFilter;

  constructor() {
    super(FractionFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): FractionFilterCriteria {
    return FractionFilterCriteria.fromObject(source, opts);
  }
}
