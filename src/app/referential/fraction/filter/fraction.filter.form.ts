import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { FractionFilter, FractionFilterCriteria } from '@app/referential/fraction/filter/fraction.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-fraction-filter-form',
  templateUrl: './fraction.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FractionFilterForm extends ReferentialCriteriaFormComponent<FractionFilter, FractionFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof FractionFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias.concat('matrixFilter');
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: FractionFilterCriteria): PartialRecord<keyof FractionFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'matrixFilter');
    return params;
  }
}
