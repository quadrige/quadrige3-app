import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { FractionTable } from '@app/referential/fraction/fraction.table';
import { FractionFilterForm } from '@app/referential/fraction/filter/fraction.filter.form';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';
import { QualitativeValueTable } from '@app/referential/parameter/qualitative-value/qualitative-value.table';

@NgModule({
  imports: [ReferentialModule, FractionFilterForm, FractionMatrixTable, QualitativeValueTable],
  declarations: [FractionTable],
  exports: [FractionTable],
})
export class FractionModule {}
