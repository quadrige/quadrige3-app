import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { FractionMatrix } from '@app/referential/fraction-matrix/fraction-matrix.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'FractionVO' })
export class Fraction extends Referential<Fraction> {
  static entityName = 'Fraction';
  static fromObject: (source: any, opts?: any) => Fraction;

  fractionMatrices: FractionMatrix[] = [];

  constructor() {
    super(Fraction.TYPENAME);
    this.entityName = Fraction.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Fraction.entityName;
    this.fractionMatrices = source.fractionMatrices?.map(FractionMatrix.fromObject) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.fractionMatrices = this.fractionMatrices?.map((value) => EntityUtils.asMinifiedObject(value, opts));
    return target;
  }
}
