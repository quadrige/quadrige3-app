import { ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import {
  arraySize,
  isNotEmptyArray,
  ObjectMap,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StatusIds,
} from '@sumaris-net/ngx-components';
import { Matrix } from '@app/referential/matrix/matrix.model';
import { MatrixValidatorService } from '@app/referential/matrix/matrix.validator';
import { MatrixService } from '@app/referential/matrix/matrix.service';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { Alerts } from '@app/shared/alerts';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { EntityUtils } from '@app/shared/entity.utils';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { MatrixFilter, MatrixFilterCriteria } from '@app/referential/matrix/filter/matrix.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';
import { Fraction } from '@app/referential/fraction/fraction.model';
import { FractionMatrix } from '@app/referential/fraction-matrix/fraction-matrix.model';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { BaseColumnItem } from '@app/shared/table/table.model';
import { Utils } from '@app/shared/utils';

type MatrixView = 'fractionMatrixTable' | TranscribingItemView;

@Component({
  selector: 'app-matrix-table',
  templateUrl: './matrix.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatrixTable
  extends ReferentialTable<Matrix, number, MatrixFilter, MatrixFilterCriteria, MatrixValidatorService, MatrixView>
  implements OnInit
{
  fractionMatrixTable = viewChild<FractionMatrixTable>('fractionMatrixTable');

  readonly fractionMenuItem: IEntityMenuItem<Matrix, MatrixView> = {
    title: 'REFERENTIAL.MATRIX.FRACTIONS',
    attribute: 'fractionMatrices',
    view: 'fractionMatrixTable',
  };
  rightAreaEnabled = false;

  protected previousSelectedFractionMatrix: FractionMatrix;

  constructor(
    protected injector: Injector,
    protected _entityService: MatrixService,
    protected validatorService: MatrixValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      Matrix,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.MATRIX.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[matrix-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent || (!this.rightPanelVisible && isNotEmptyArray(this.singleSelectedRow?.currentData?.fractionMatrices));
  }

  ngOnInit() {
    super.ngOnInit();

    // todo maybe listen to fraction selection change to set select criteria ?
  }

  protected async canRightMenuItemChange(
    previousMenuItem: IEntityMenuItem<Matrix, TranscribingItemView | MatrixView>,
    nextMenuItem: IEntityMenuItem<Matrix, TranscribingItemView | MatrixView>
  ): Promise<boolean> {
    return Utils.promiseEveryTrue([
      super.canRightMenuItemChange(previousMenuItem, nextMenuItem),
      this.saveRightView(this.fractionMenuItem, this.fractionMatrixTable()),
    ]);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<Matrix>): Promise<boolean> {
    row = row || this.singleEditingRow;

    if (row && !(await this.saveRightView(this.fractionMenuItem, this.fractionMatrixTable(), { row }))) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    this.previousSelectedFractionMatrix =
      arraySize(this.fractionMatrixTable()?.selection.selected) === 1 ? this.fractionMatrixTable()?.selection.selected[0].currentData : undefined;
    const continueSave = await this.saveRightView(this.fractionMenuItem, this.fractionMatrixTable());

    if (continueSave) {
      return await super.save(opts);
    }
    return false;
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(true, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<Matrix, TranscribingItemView | MatrixView>[] {
    return [this.fractionMenuItem, ...super.getRightMenuItems()];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Matrix, TranscribingItemView | MatrixView> {
    return this.fractionMenuItem;
  }

  protected async loadRightArea(row?: AsyncTableElement<Matrix>) {
    if (this.subTable) return; // don't load if sub table
    if (this.rightMenuItem?.view === 'fractionMatrixTable') {
      this.detectChanges();
      if (this.fractionMatrixTable()) {
        this.registerSubForm(this.fractionMatrixTable());
        await this.loadFractionMatrixTable(row);
      }
      return;
    }
    await super.loadRightArea(row);
  }

  protected async loadFractionMatrixTable(row?: AsyncTableElement<Matrix>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.fractionMatrixTable().setValue(undefined);
      this.fractionMatrixTable().parentId = undefined;
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected fractions
    await this.fractionMatrixTable().setValue((row.currentData?.fractionMatrices || []).slice());
    this.fractionMatrixTable().parentId = row.currentData.id;
    this.rightAreaEnabled = this.canEdit;

    // Try to reselect previously selected association
    await this.fractionMatrixTable().selectRowByData(this.previousSelectedFractionMatrix);
    this.previousSelectedFractionMatrix = undefined;
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<Matrix>[]): Promise<boolean> {
    let confirmed = true;
    const matrixIds = EntityUtils.ids(dirtyRowsWithDisabledStatusId);

    // Check enabled pmfmu with this matrix
    const exists = await this.pmfmuService.exists(
      PmfmuFilterCriteria.fromObject({
        statusId: StatusIds.ENABLE,
        matrixFilter: { includedIds: matrixIds },
      })
    );
    if (exists) {
      confirmed = await Alerts.askConfirmation('REFERENTIAL.MATRIX.CONFIRM.DISABLE_PMFMU', this.alertCtrl, this.translate);
    }

    if (confirmed) {
      // Check if used in pmfmu applied strategies
      const res = await this.strategyService.loadPage(
        { offset: 0, size: 100 },
        StrategyFilter.fromObject({
          criterias: [
            {
              programFilter: { statusId: StatusIds.ENABLE },
              onlyActive: true,
              pmfmuFilter: {
                matrixFilter: {
                  includedIds: matrixIds,
                },
              },
            },
          ],
        })
      );

      if (isNotEmptyArray(res.data)) {
        confirmed = await Alerts.askConfirmation('REFERENTIAL.MATRIX.CONFIRM.DISABLE_APPLIED_STRATEGIES', this.alertCtrl, this.translate, undefined, {
          list: `<div class="scroll-content"><ul>${res.data
            .map((r) => referentialToString(r, ['programId', 'name']))
            .map((s) => `<li>${s}</li>`)
            .join('')}</ul></div>`,
        });
      }
    }

    return confirmed;
  }

  async buildExportAdditionalColumns(): Promise<ObjectMap<BaseColumnItem[]>> {
    let previousRightMenuItem = undefined;
    if (this.rightMenuItem !== this.fractionMenuItem) {
      previousRightMenuItem = this.rightMenuItem;
      this.rightMenuItem = this.fractionMenuItem;
      await this.loadRightArea();
    }
    const fractionPrefix = 'fraction.';
    const columns = (await this.fractionMatrixTable().buildExportColumns()).filter((column) => column.label.startsWith(fractionPrefix)); // Take fraction columns only
    if (!!previousRightMenuItem) {
      this.rightMenuItem = previousRightMenuItem;
      await this.loadRightArea();
    }

    const toAppend = this.translate.instant('REFERENTIAL.ENTITY.FRACTION');
    columns.forEach((column) => {
      column.label = column.label.substring(fractionPrefix.length);
      column.name = toAppend + ' - ' + column.name;
    });

    const result = {};
    result[Fraction.entityName] = columns;
    return result;
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.MATRICES' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('description', 'comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name');
  }
}
