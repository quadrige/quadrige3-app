import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { FractionMatrix } from '@app/referential/fraction-matrix/fraction-matrix.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'MatrixVO' })
export class Matrix extends Referential<Matrix> {
  static entityName = 'Matrix';
  static fromObject: (source: any, opts?: any) => Matrix;

  fractionMatrices: FractionMatrix[] = [];

  constructor() {
    super(Matrix.TYPENAME);
    this.entityName = Matrix.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Matrix.entityName;
    this.fractionMatrices = source.fractionMatrices?.map(FractionMatrix.fromObject) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.fractionMatrices = this.fractionMatrices?.map((value) => EntityUtils.asMinifiedObject(value, opts));
    return target;
  }
}
