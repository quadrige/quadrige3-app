import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Matrix } from '@app/referential/matrix/matrix.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'MatrixFilterCriteriaVO' })
export class MatrixFilterCriteria extends ReferentialFilterCriteria<Matrix, number> {
  static fromObject: (source: any, opts?: any) => MatrixFilterCriteria;

  fractionFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(MatrixFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.fractionFilter = IntReferentialFilterCriteria.fromObject(source.fractionFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.fractionFilter = this.fractionFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'fractionFilter') return !BaseFilterUtils.isCriteriaEmpty(this.fractionFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'MatrixFilterVO' })
export class MatrixFilter extends ReferentialFilter<MatrixFilter, MatrixFilterCriteria, Matrix> {
  static fromObject: (source: any, opts?: any) => MatrixFilter;

  constructor() {
    super(MatrixFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): MatrixFilterCriteria {
    return MatrixFilterCriteria.fromObject(source, opts);
  }
}
