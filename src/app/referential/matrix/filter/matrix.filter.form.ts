import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { MatrixFilter, MatrixFilterCriteria } from '@app/referential/matrix/filter/matrix.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-matrix-filter-form',
  templateUrl: './matrix.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatrixFilterForm extends ReferentialCriteriaFormComponent<MatrixFilter, MatrixFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof MatrixFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias.concat('fractionFilter');
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: MatrixFilterCriteria): PartialRecord<keyof MatrixFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'fractionFilter');
    return params;
  }
}
