import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { MatrixTable } from '@app/referential/matrix/matrix.table';
import { MatrixFilterForm } from '@app/referential/matrix/filter/matrix.filter.form';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';

@NgModule({
  imports: [ReferentialModule, MatrixFilterForm, FractionMatrixTable],
  declarations: [MatrixTable],
  exports: [MatrixTable],
})
export class MatrixModule {}
