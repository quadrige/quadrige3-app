import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Matrix } from '@app/referential/matrix/matrix.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { jobFragments } from '@app/social/job/job.service';
import { MatrixFilter, MatrixFilterCriteria } from '@app/referential/matrix/filter/matrix.filter.model';
import { fractionMatrixFragment, FractionMatrixUtils } from '@app/referential/fraction-matrix/fraction-matrix.model';
import { EntityUtils } from '@sumaris-net/ngx-components';

const fragments = {
  matrix: gql`
    fragment MatrixFragment on MatrixVO {
      id
      name
      description
      comments
      fractionMatrices {
        ...FractionMatrixFragment
      }
      updateDate
      creationDate
      statusId
      __typename
    }
    ${fractionMatrixFragment}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Matrices($page: PageInput, $filter: MatrixFilterVOInput) {
      data: matrices(page: $page, filter: $filter) {
        ...MatrixFragment
      }
    }
    ${fragments.matrix}
  `,

  loadAllWithTotal: gql`
    query MatricesWithTotal($page: PageInput, $filter: MatrixFilterVOInput) {
      data: matrices(page: $page, filter: $filter) {
        ...MatrixFragment
      }
      total: matricesCount(filter: $filter)
    }
    ${fragments.matrix}
  `,

  countAll: gql`
    query MatricesCount($filter: MatrixFilterVOInput) {
      total: matricesCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportMatricesAsync($filter: MatrixFilterVOInput, $context: ExportContextInput) {
      data: exportMatricesAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveMatrices($data: [MatrixVOInput]) {
      data: saveMatrices(matrices: $data) {
        ...MatrixFragment
      }
    }
    ${fragments.matrix}
  `,

  deleteAll: gql`
    mutation DeleteMatrices($ids: [Int]) {
      deleteMatrices(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class MatrixService extends BaseReferentialService<Matrix, MatrixFilter, MatrixFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Matrix, MatrixFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[matrix-service]';
  }

  copyIdAndUpdateDate(source: Matrix, target: Matrix) {
    super.copyIdAndUpdateDate(source, target);

    // Propagate to fraction-matrix
    if (source.fractionMatrices && target.fractionMatrices) {
      target.fractionMatrices.forEach((targetFractionMatrix) => {
        // Copy matrix id to target
        targetFractionMatrix.matrixId = source.id;
        // Then copy id and updateDate
        const sourceFractionMatrix = source.fractionMatrices.find((value) => FractionMatrixUtils.equals(value, targetFractionMatrix));
        EntityUtils.copyIdAndUpdateDate(sourceFractionMatrix, targetFractionMatrix);
        targetFractionMatrix.transcribingItemsLoaded = false;
      });
    }
  }
}
