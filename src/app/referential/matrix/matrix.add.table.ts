import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { MatrixTable } from '@app/referential/matrix/matrix.table';
import { MatrixService } from '@app/referential/matrix/matrix.service';
import { MatrixValidatorService } from '@app/referential/matrix/matrix.validator';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { MatrixFilter } from '@app/referential/matrix/filter/matrix.filter.model';
import { Matrix } from '@app/referential/matrix/matrix.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { MatrixFilterForm } from '@app/referential/matrix/filter/matrix.filter.form';
import { FractionMatrixTable } from '@app/referential/fraction-matrix/fraction-matrix.table';

@Component({
  selector: 'app-matrix-add-table',
  templateUrl: './matrix.table.html',
  standalone: true,
  imports: [ReferentialModule, MatrixFilterForm, FractionMatrixTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatrixAddTable extends MatrixTable implements IAddTable<Matrix> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: MatrixService,
    protected validatorService: MatrixValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(injector, _entityService, validatorService, pmfmuService, strategyService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[matrix-add-table]';
    this.addTable = true;
  }

  protected getDefaultRightMenuItem(): any {
    return undefined;
  }

  async resetFilter(filter?: MatrixFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
