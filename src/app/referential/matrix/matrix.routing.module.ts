import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { MatrixTable } from '@app/referential/matrix/matrix.table';
import { MatrixModule } from '@app/referential/matrix/matrix.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MatrixTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), MatrixModule],
})
export class MatrixRoutingModule {}
