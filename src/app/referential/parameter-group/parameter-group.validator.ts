import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { ParameterGroup } from '@app/referential/parameter-group/parameter-group.model';
import { ParameterGroupService } from '@app/referential/parameter-group/parameter-group.service';
import {
  lengthComment,
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class ParameterGroupValidatorService extends ReferentialValidatorService<ParameterGroup> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: ParameterGroupService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: ParameterGroup, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      parent: [data?.parent || null, SharedValidators.entity],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: ParameterGroup, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [BaseGroupValidators.invalidIfSameEntity('parent')],
    };
  }
}
