import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { ParameterGroupTable } from '@app/referential/parameter-group/parameter-group.table';
import { ParameterGroupModule } from '@app/referential/parameter-group/parameter-group.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ParameterGroupTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ParameterGroupModule],
})
export class ParameterGroupRoutingModule {}
