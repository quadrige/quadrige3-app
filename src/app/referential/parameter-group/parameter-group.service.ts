import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { ParameterGroup } from '@app/referential/parameter-group/parameter-group.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { jobFragments } from '@app/social/job/job.service';

const fragments = {
  parameterGroup: gql`
    fragment ParameterGroupFragment on ParameterGroupVO {
      id
      name
      description
      comments
      parent {
        ...ReferentialFragment
      }
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query ParameterGroups($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: parameterGroups(page: $page, filter: $filter) {
        ...ParameterGroupFragment
      }
    }
    ${fragments.parameterGroup}
    ${referentialFragments.light}
  `,

  loadAllWithTotal: gql`
    query ParameterGroupsWithTotal($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: parameterGroups(page: $page, filter: $filter) {
        ...ParameterGroupFragment
      }
      total: parameterGroupsCount(filter: $filter)
    }
    ${fragments.parameterGroup}
    ${referentialFragments.light}
  `,

  countAll: gql`
    query ParameterGroupsCount($filter: IntReferentialFilterVOInput) {
      total: parameterGroupsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportParameterGroupsAsync($filter: IntReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportParameterGroupsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveParameterGroups($data: [ParameterGroupVOInput]) {
      data: saveParameterGroups(parameterGroups: $data) {
        ...ParameterGroupFragment
      }
    }
    ${fragments.parameterGroup}
    ${referentialFragments.light}
  `,

  deleteAll: gql`
    mutation DeleteParameterGroups($ids: [Int]) {
      deleteParameterGroups(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class ParameterGroupService extends BaseReferentialService<ParameterGroup, IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, ParameterGroup, IntReferentialFilter, { queries, mutations });
    this._logPrefix = '[parameter-group-service]';
  }
}
