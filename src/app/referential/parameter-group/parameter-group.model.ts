import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'ParameterGroupVO' })
export class ParameterGroup extends Referential<ParameterGroup> {
  static entityName = 'ParameterGroup';
  static fromObject: (source: any, opts?: any) => ParameterGroup;

  parent: IntReferential = null;

  constructor() {
    super(ParameterGroup.TYPENAME);
    this.entityName = ParameterGroup.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = ParameterGroup.entityName;
    this.parent = IntReferential.fromObject(source.parent);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parent = EntityUtils.asMinifiedObject(this.parent, opts);
    return target;
  }
}
