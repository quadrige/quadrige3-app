import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { ParameterGroup } from '@app/referential/parameter-group/parameter-group.model';
import { ParameterGroupValidatorService } from '@app/referential/parameter-group/parameter-group.validator';
import { ParameterGroupService } from '@app/referential/parameter-group/parameter-group.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { autocompleteWidthLarge } from '@app/shared/constants';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-parameter-group-table',
  templateUrl: './parameter-group.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterGroupTable
  extends ReferentialTable<ParameterGroup, number, IntReferentialFilter, IntReferentialFilterCriteria, ParameterGroupValidatorService>
  implements OnInit
{
  autocompleteWidthLarge = autocompleteWidthLarge;

  constructor(
    protected injector: Injector,
    protected _entityService: ParameterGroupService,
    protected validatorService: ParameterGroupValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'parent', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      ParameterGroup,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.PARAMETER_GROUP.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[parameter-group-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    // Parent combo
    this.registerAutocompleteField('parent', {
      ...this.referentialOptions.parameterGroup,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ParameterGroup',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.PARAMETER_GROUPS' });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'parent');
  }
}
