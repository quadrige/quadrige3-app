import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { ParameterGroupTable } from '@app/referential/parameter-group/parameter-group.table';
import { ParameterGroupValidatorService } from '@app/referential/parameter-group/parameter-group.validator';
import { ParameterGroupService } from '@app/referential/parameter-group/parameter-group.service';
import { ISelectTable } from '@app/shared/table/table.model';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { IntReferentialFilter } from '@app/referential/model/referential.filter.model';
import { ParameterGroup } from '@app/referential/parameter-group/parameter-group.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { ParameterGroupFilterForm } from '@app/referential/parameter-group/filter/parameter-group.filter.form';

@Component({
  selector: 'app-parameter-group-select-table',
  templateUrl: './parameter-group.table.html',
  standalone: true,
  imports: [ReferentialModule, ParameterGroupFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterGroupSelectTable extends ParameterGroupTable implements ISelectTable<ParameterGroup> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria;
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();

  constructor(
    protected injector: Injector,
    protected _entityService: ParameterGroupService,
    protected validatorService: ParameterGroupValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[parameter-group-select-table]';
    this.selectTable = true;
  }

  async resetFilter(filter?: IntReferentialFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit();
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete).map((id) => id.toString());
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    await this.resetFilter();
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
