import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { ParameterGroupTable } from '@app/referential/parameter-group/parameter-group.table';
import { ParameterGroupValidatorService } from '@app/referential/parameter-group/parameter-group.validator';
import { ParameterGroupService } from '@app/referential/parameter-group/parameter-group.service';
import { IAddTable } from '@app/shared/table/table.model';
import { IntReferentialFilter } from '@app/referential/model/referential.filter.model';
import { ParameterGroup } from '@app/referential/parameter-group/parameter-group.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { ParameterGroupFilterForm } from '@app/referential/parameter-group/filter/parameter-group.filter.form';

@Component({
  selector: 'app-parameter-group-add-table',
  templateUrl: './parameter-group.table.html',
  standalone: true,
  imports: [ReferentialModule, ParameterGroupFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParameterGroupAddTable extends ParameterGroupTable implements IAddTable<ParameterGroup> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: ParameterGroupService,
    protected validatorService: ParameterGroupValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[parameter-group-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: IntReferentialFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
