import { NgModule } from '@angular/core';
import { ParameterGroupTable } from '@app/referential/parameter-group/parameter-group.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { ParameterGroupFilterForm } from '@app/referential/parameter-group/filter/parameter-group.filter.form';

@NgModule({
  imports: [ReferentialModule, ParameterGroupFilterForm],
  declarations: [ParameterGroupTable],
  exports: [ParameterGroupTable],
})
export class ParameterGroupModule {}
