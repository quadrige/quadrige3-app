import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { Method } from '@app/referential/method/method.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'MethodFilterCriteriaVO' })
export class MethodFilterCriteria extends ReferentialFilterCriteria<Method, number> {
  static fromObject: (source: any, opts?: any) => MethodFilterCriteria;

  reference: string = null;
  programFilter: StrReferentialFilterCriteria = null;
  strategyFilter: IntReferentialFilterCriteria = null;

  constructor() {
    super(MethodFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.reference = source.reference;
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.strategyFilter = IntReferentialFilterCriteria.fromObject(source.strategyFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.searchAttributes = target.searchAttributes || ['id', 'name', 'description'];
    target.programFilter = this.programFilter?.asObject(opts);
    target.strategyFilter = { ...this.strategyFilter?.asObject(opts), searchAttributes: ['id', 'name', 'description'] };
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'strategyFilter') return !BaseFilterUtils.isCriteriaEmpty(this.strategyFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'MethodFilterVO' })
export class MethodFilter extends ReferentialFilter<MethodFilter, MethodFilterCriteria, Method> {
  static fromObject: (source: any, opts?: any) => MethodFilter;

  constructor() {
    super(MethodFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): MethodFilterCriteria {
    return MethodFilterCriteria.fromObject(source, opts);
  }
}
