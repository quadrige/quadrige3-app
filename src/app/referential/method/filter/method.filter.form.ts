import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { MethodFilter, MethodFilterCriteria } from '@app/referential/method/filter/method.filter.model';
import { isNotNilOrBlank } from '@sumaris-net/ngx-components';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-method-filter-form',
  templateUrl: './method.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MethodFilterForm extends ReferentialCriteriaFormComponent<MethodFilter, MethodFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idNameDescription;
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof MethodFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias.concat('programFilter');
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: MethodFilterCriteria): PartialRecord<keyof MethodFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNilOrBlank(criteria.reference)) params.reference = criteria.reference;
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'strategyFilter');
    return params;
  }
}
