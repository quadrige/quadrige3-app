import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { MethodTable } from '@app/referential/method/method.table';
import { MethodModule } from '@app/referential/method/method.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MethodTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), MethodModule],
})
export class MethodRoutingModule {}
