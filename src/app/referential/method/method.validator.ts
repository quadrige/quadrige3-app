import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { Method } from '@app/referential/method/method.model';
import { MethodService } from '@app/referential/method/method.service';
import {
  lengthComment,
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';

@Injectable({ providedIn: 'root' })
export class MethodValidatorService extends ReferentialValidatorService<Method> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: MethodService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Method, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      reference: [data?.reference || null, Validators.maxLength(lengthComment)],
      conditioning: [data?.conditioning || null, Validators.maxLength(lengthComment)],
      preparation: [data?.preparation || null, Validators.maxLength(lengthComment)],
      conservation: [data?.conservation || null, Validators.maxLength(lengthComment)],
      handbookPath: [data?.handbookPath || null, Validators.maxLength(lengthDescription)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
