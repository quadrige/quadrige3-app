import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'MethodVO' })
export class Method extends Referential<Method> {
  static entityName = 'Method';
  static fromObject: (source: any, opts?: any) => Method;

  reference: string = null;
  conditioning: string = null;
  preparation: string = null;
  conservation: string = null;
  handbookPath: string = null;

  constructor() {
    super(Method.TYPENAME);
    this.entityName = Method.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Method.entityName;
    this.reference = source.reference;
    this.conditioning = source.conditioning;
    this.preparation = source.preparation;
    this.conservation = source.conservation;
    this.handbookPath = source.handbookPath;
  }
}
