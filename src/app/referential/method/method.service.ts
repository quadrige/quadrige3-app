import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Method } from '@app/referential/method/method.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { jobFragments } from '@app/social/job/job.service';
import { MethodFilter, MethodFilterCriteria } from '@app/referential/method/filter/method.filter.model';

const fragments = {
  method: gql`
    fragment MethodFragment on MethodVO {
      id
      name
      description
      comments
      reference
      conditioning
      preparation
      conservation
      handbookPath
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Methods($page: PageInput, $filter: MethodFilterVOInput) {
      data: methods(page: $page, filter: $filter) {
        ...MethodFragment
      }
    }
    ${fragments.method}
  `,

  loadAllWithTotal: gql`
    query MethodsWithTotal($page: PageInput, $filter: MethodFilterVOInput) {
      data: methods(page: $page, filter: $filter) {
        ...MethodFragment
      }
      total: methodsCount(filter: $filter)
    }
    ${fragments.method}
  `,

  countAll: gql`
    query MethodsCount($filter: MethodFilterVOInput) {
      total: methodsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportMethodsAsync($filter: MethodFilterVOInput, $context: ExportContextInput) {
      data: exportMethodsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveMethods($data: [MethodVOInput]) {
      data: saveMethods(methods: $data) {
        ...MethodFragment
      }
    }
    ${fragments.method}
  `,

  deleteAll: gql`
    mutation DeleteMethods($ids: [Int]) {
      deleteMethods(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class MethodService extends BaseReferentialService<Method, MethodFilter, MethodFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Method, MethodFilter, { queries, mutations });
    this._logPrefix = '[method-service]';
  }
}
