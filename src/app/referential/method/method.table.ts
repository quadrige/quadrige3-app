import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import {
  FilesUtils,
  isNotEmptyArray,
  PlatformService,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StatusIds,
} from '@sumaris-net/ngx-components';
import { Method } from '@app/referential/method/method.model';
import { MethodValidatorService } from '@app/referential/method/method.validator';
import { MethodService } from '@app/referential/method/method.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { Alerts } from '@app/shared/alerts';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { FileTransferService } from '@app/shared/service/file-transfer.service';
import { EntityUtils } from '@app/shared/entity.utils';
import { IStatusMultiEditModalOptions, StatusComposite, StatusMultiEditModal } from '@app/shared/component/multi-edit/status.multi-edit.modal';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { MethodFilter, MethodFilterCriteria } from '@app/referential/method/filter/method.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-method-table',
  templateUrl: './method.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MethodTable extends ReferentialTable<Method, number, MethodFilter, MethodFilterCriteria, MethodValidatorService> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: MethodService,
    protected validatorService: MethodValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService,
    protected transferService?: FileTransferService,
    protected platform?: PlatformService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'name',
        'description',
        'reference',
        'handbookPath',
        'conditioning',
        'preparation',
        'conservation',
        'comments',
        'statusId',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      Method,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.METHOD.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[method-table]';
  }

  async downloadResource(event: MouseEvent, row?: AsyncTableElement<Method>) {
    row = row || this.singleSelectedRow;
    if (!row?.currentData?.handbookPath) {
      console.warn(`${this.logPrefix} Can't download file: No handbookPath in current row`, row);
    }
    this.platform.download({ uri: this.transferService.downloadResource(Method.entityName, row.currentData.handbookPath) });
  }

  async uploadResource(event: MouseEvent, row?: AsyncTableElement<Method>) {
    row = row || this.singleEditingRow;
    if (!row || !row.editing || !row.validator.valid || !row.currentData.id) {
      console.warn(`${this.logPrefix} Can't upload file: Invalid Method selected`, row);
    }

    const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
      title: 'modalTitle',
      uniqueFile: true,
      uploadFn: (file: File) =>
        this.transferService.uploadResource(file, { resourceType: Method.entityName, resourceId: row.currentData.id.toString(), replace: true }),
    });

    if (data && row.editing) {
      const filename = data[0].response.body.finalName;
      if (!filename) {
        throw new Error(`response final name not found in ${data}`);
      }
      row.validator.patchValue({ handbookPath: filename }, { onlySelf: false, emitEvent: true });
      this.markRowAsDirty(row);
      await this.save();
    }
  }

  // async deleteResource(event: MouseEvent, row?: AsyncTableElement<Method>) {
  //   event?.stopPropagation();
  //   row = row || this.singleEditingRow;
  //   if (!row || !row.editing || !row.validator.valid || !row.currentData.handbookPath) {
  //     console.warn(`${this.logPrefix} Can't upload file: Invalid Method selected`, row);
  //   }
  //
  //   // ask confirmation
  //   const confirmed = await Alerts.askConfirmation('REFERENTIAL.METHOD.CONFIRM.DELETE_RESOURCE', this.alertCtrl, this.translate, event);
  //   if (!confirmed) return;
  //
  //   const deleted = await this.transferService.deleteResource(Method.entityName, row.currentData.handbookPath);
  //   if (deleted) {
  //     row.validator.patchValue({ handbookPath: null }, { onlySelf: false, emitEvent: true });
  //     this.markRowAsDirty(row);
  //     await this.save();
  //   } else {
  //     this.error = this.translate.instant('FILE.DELETE.ERROR');
  //   }
  // }

  async multiEditRows(event: MouseEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IStatusMultiEditModalOptions, StatusComposite>(
      StatusMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.METHOD.MULTI_EDIT.TITLE',
        selection: this.selection.selected.map((row) => row.currentData),
      },
      'modal-200'
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // Patch selected rows
      this.selection.selected.forEach((row) => {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    }
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<Method>[]): Promise<boolean> {
    let confirmed = true;
    const methodIds = EntityUtils.ids(dirtyRowsWithDisabledStatusId);

    // Check enabled pmfmu with this method
    const exists = await this.pmfmuService.exists(
      PmfmuFilterCriteria.fromObject({
        statusId: StatusIds.ENABLE,
        methodFilter: { includedIds: methodIds },
      })
    );
    if (exists) {
      confirmed = await Alerts.askConfirmation('REFERENTIAL.METHOD.CONFIRM.DISABLE_PMFMU', this.alertCtrl, this.translate);
    }

    if (confirmed) {
      // Check if used in pmfmu applied strategies
      const res = await this.strategyService.loadPage(
        { offset: 0, size: 100 },
        StrategyFilter.fromObject({
          criterias: [
            {
              programFilter: { statusId: StatusIds.ENABLE },
              onlyActive: true,
              pmfmuFilter: {
                methodFilter: {
                  includedIds: methodIds,
                },
              },
            },
          ],
        })
      );

      if (isNotEmptyArray(res.data)) {
        confirmed = await Alerts.askConfirmation('REFERENTIAL.METHOD.CONFIRM.DISABLE_APPLIED_STRATEGIES', this.alertCtrl, this.translate, undefined, {
          list: `<div class="scroll-content"><ul>${res.data
            .map((r) => referentialToString(r, ['programId', 'name']))
            .map((s) => `<li>${s}</li>`)
            .join('')}</ul></div>`,
        });
      }
    }

    return confirmed;
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.METHODS' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('handbookPath', 'conditioning', 'preparation', 'conservation', 'comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name');
  }
}
