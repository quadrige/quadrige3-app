import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { MethodTable } from '@app/referential/method/method.table';
import { MethodService } from '@app/referential/method/method.service';
import { MethodValidatorService } from '@app/referential/method/method.validator';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { MethodFilter } from '@app/referential/method/filter/method.filter.model';
import { Method } from '@app/referential/method/method.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { MethodFilterForm } from '@app/referential/method/filter/method.filter.form';

@Component({
  selector: 'app-method-add-table',
  templateUrl: './method.table.html',
  standalone: true,
  imports: [ReferentialModule, MethodFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MethodAddTable extends MethodTable implements IAddTable<Method> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: MethodService,
    protected validatorService: MethodValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(injector, _entityService, validatorService, pmfmuService, strategyService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[method-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: MethodFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
