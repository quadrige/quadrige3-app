import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { MethodTable } from '@app/referential/method/method.table';
import { MethodFilterForm } from '@app/referential/method/filter/method.filter.form';

@NgModule({
  imports: [ReferentialModule, MethodFilterForm],
  declarations: [MethodTable],
  exports: [MethodTable],
})
export class MethodModule {}
