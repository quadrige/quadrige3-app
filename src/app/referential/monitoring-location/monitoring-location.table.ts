import { ChangeDetectionStrategy, Component, inject, Injector, OnInit, viewChild } from '@angular/core';
import {
  FileEvent,
  FileResponse,
  FilesUtils,
  IEntity,
  isEmptyArray,
  isInt,
  isNil,
  isNotEmptyArray,
  isNotNil,
  isNotNilString,
  PlatformService,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StatusIds,
  UploadFileComponent,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { MonitoringLocation, monitoringLocationIdShapeProperties } from '@app/referential/monitoring-location/monitoring-location.model';
import { MonitoringLocationValidatorService } from '@app/referential/monitoring-location/monitoring-location.validator';
import { MonitoringLocationService } from '@app/referential/monitoring-location/monitoring-location.service';
import { TaxonPositionTable } from '@app/referential/monitoring-location/taxon-position/taxon-position.table';
import { TaxonGroupPositionTable } from '@app/referential/monitoring-location/taxon-group-position/taxon-group-position.table';
import { PositioningSystemService } from '@app/referential/positioning-system/positioning-system.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { Alerts } from '@app/shared/alerts';
import { FileTransferService } from '@app/shared/service/file-transfer.service';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { EntityUtils } from '@app/shared/entity.utils';
import { MapComponent } from '@app/shared/component/map/map.component';
import {
  IMonitoringLocationReportModalOptions,
  MonitoringLocationReportModal,
} from '@app/referential/mon-loc-order-item/monitoring-location-report.modal';
import { MonLocOrderItemTable } from '@app/referential/mon-loc-order-item/mon-loc-order-item.table';
import { LocationProgramTable } from '@app/referential/monitoring-location/program/location-program.table';
import { TranscribingItemUtils } from '@app/referential/transcribing-item/transcribing-item.model';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { JobService } from '@app/social/job/job.service';
import {
  MonitoringLocationFilter,
  MonitoringLocationFilterCriteria,
} from '@app/referential/monitoring-location/filter/monitoring-location.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { IntReferentialFilter } from '@app/referential/model/referential.filter.model';
import { ExportOptions } from '@app/shared/table/table.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { SurveyService } from '@app/data/survey/survey.service';
import { Observable } from 'rxjs';
import { FeatureCollectionWithFilename, parseZip } from 'shpjs';
import { FileUtils } from '@app/shared/files';
import { catchError, filter, mergeMap } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { SurveyFilterCriteria } from '@app/data/survey/filter/survey.filter.model';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { Utils } from '@app/shared/utils';

type MonitoringLocationView =
  | 'programTable'
  | 'taxonPositionTable'
  | 'taxonGroupPositionTable'
  | 'monLocOrderItemTable'
  | 'map'
  | TranscribingItemView;

@Component({
  selector: 'app-monitoring-locations-table',
  templateUrl: './monitoring-location.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitoringLocationTable
  extends ReferentialTable<
    MonitoringLocation,
    number,
    MonitoringLocationFilter,
    MonitoringLocationFilterCriteria,
    MonitoringLocationValidatorService,
    MonitoringLocationView
  >
  implements OnInit
{
  programTable = viewChild<LocationProgramTable>('programTable');
  taxonPositionTable = viewChild<TaxonPositionTable>('taxonPositionTable');
  taxonGroupPositionTable = viewChild<TaxonGroupPositionTable>('taxonGroupPositionTable');
  monLocOrderItemTable = viewChild<MonLocOrderItemTable>('monLocOrderItemTable');
  map = viewChild<MapComponent>('map');

  readonly programMenuItem: IEntityMenuItem<MonitoringLocation, MonitoringLocationView> = {
    title: 'REFERENTIAL.MONITORING_LOCATION.PROGRAMS',
    attribute: 'locationPrograms',
    view: 'programTable',
  };
  readonly taxonPositionMenuItem: IEntityMenuItem<MonitoringLocation, MonitoringLocationView> = {
    title: 'REFERENTIAL.MONITORING_LOCATION.TAXON_POSITIONS',
    attribute: 'taxonPositions',
    view: 'taxonPositionTable',
  };
  readonly taxonGroupPositionMenuItem: IEntityMenuItem<MonitoringLocation, MonitoringLocationView> = {
    title: 'REFERENTIAL.MONITORING_LOCATION.TAXON_GROUP_POSITIONS',
    attribute: 'taxonGroupPositions',
    view: 'taxonGroupPositionTable',
  };
  readonly monLocOrderItemsMenuItem: IEntityMenuItem<MonitoringLocation, MonitoringLocationView> = {
    title: 'REFERENTIAL.MONITORING_LOCATION.MON_LOC_ORDER_ITEMS',
    attribute: 'monLocOrderItems',
    view: 'monLocOrderItemTable',
  };
  readonly mapMenuItem: IEntityMenuItem<MonitoringLocation, MonitoringLocationView> = {
    title: 'REFERENTIAL.MONITORING_LOCATION.MAP',
    attribute: 'geometry',
    view: 'map',
  };
  rightAreaEnabled = false;

  protected readonly platform = inject(PlatformService);
  private readonly strategyService = inject(StrategyService);
  private readonly transferService = inject(FileTransferService);
  private readonly jobService = inject(JobService);
  private readonly surveyService = inject(SurveyService);

  constructor(
    protected injector: Injector,
    protected _entityService: MonitoringLocationService,
    protected validatorService: MonitoringLocationValidatorService,
    protected positioningSystemService: PositioningSystemService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'label',
        'name',
        'bathymetry',
        'minLatitude',
        'minLongitude',
        'maxLatitude',
        'maxLongitude',
        'harbour',
        'positioningSystem',
        'positioningSystemPrecision',
        'utFormat',
        'daylightSavingTime',
        'positionPath',
        'comments',
        'statusId',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      MonitoringLocation,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.MONITORING_LOCATION.';
    this.defaultSortBy = 'label';
    this.logPrefix = '[monitoring-locations-table]';
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: MonitoringLocation = this.singleSelectedRow.currentData;
      return (
        isNotEmptyArray(data.locationPrograms) ||
        isNotEmptyArray(data.taxonPositions) ||
        isNotEmptyArray(data.taxonGroupPositions) ||
        isNotEmptyArray(data.transcribingItems) ||
        TranscribingItemUtils.hasValidTranscribingItems(data)
      );
    }
    return false;
  }

  ngOnInit() {
    super.ngOnInit();

    // harbour combo
    this.registerAutocompleteField('harbour', {
      ...this.referentialOptions.harbour,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Harbour',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // positioningSystem combo
    this.registerAutocompleteField('positioningSystem', {
      ...this.referentialOptions.positioningSystem,
      service: this.positioningSystemService,
      filter: IntReferentialFilter.fromObject({
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  positioningSystemPrecisionDisplayFn = (data: MonitoringLocation): string => data?.positioningSystem?.horizontalPrecision;

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 50);
  }

  protected getRightMenuItems(): IEntityMenuItem<MonitoringLocation, TranscribingItemView | MonitoringLocationView>[] {
    return [
      this.programMenuItem,
      this.taxonPositionMenuItem,
      this.taxonGroupPositionMenuItem,
      this.monLocOrderItemsMenuItem,
      this.mapMenuItem,
      ...super.getRightMenuItems(),
    ];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<MonitoringLocation, TranscribingItemView | MonitoringLocationView> {
    return this.programMenuItem;
  }

  protected async canRightMenuItemChange(
    previousMenuItem: IEntityMenuItem<MonitoringLocation, TranscribingItemView | MonitoringLocationView>,
    nextMenuItem: IEntityMenuItem<MonitoringLocation, TranscribingItemView | MonitoringLocationView>
  ): Promise<boolean> {
    return Utils.promiseEveryTrue([
      super.canRightMenuItemChange(previousMenuItem, nextMenuItem),
      this.saveRightView(this.taxonPositionMenuItem, this.taxonPositionTable()),
      this.saveRightView(this.taxonGroupPositionMenuItem, this.taxonGroupPositionTable()),
    ]);
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<MonitoringLocation>): Promise<boolean> {
    row = row || this.singleEditingRow;

    // First, confirm and save right tables
    if (row) {
      if (!(await this.saveRightView(this.taxonPositionMenuItem, this.taxonPositionTable(), { row }))) {
        return false;
      }
      if (!(await this.saveRightView(this.taxonGroupPositionMenuItem, this.taxonGroupPositionTable(), { row }))) {
        return false;
      }
    }

    return super.confirmEditCreate(event, row);
  }

  async downloadResource(event: UIEvent, row?: AsyncTableElement<MonitoringLocation>) {
    row = row || this.singleSelectedRow;
    if (!row?.currentData?.positionPath) {
      console.warn(`${this.logPrefix} Can't download file: No positionPath in current row`, row);
    }
    this.platform.download({ uri: this.transferService.downloadResource(MonitoringLocation.entityName, row.currentData.positionPath) });
  }

  async uploadResource(event: UIEvent, row?: AsyncTableElement<MonitoringLocation>) {
    row = row || this.singleEditingRow;
    if (!row || !row.editing || !row.validator.valid || !row.currentData.id) {
      console.warn(`${this.logPrefix} Can't upload file: Invalid MonitoringLocation selected`, row);
      return;
    }

    const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
      title: '',
      uniqueFile: true,
      uploadFn: (file: File) =>
        this.transferService.uploadResource(file, {
          resourceType: MonitoringLocation.entityName,
          resourceId: row.currentData.id.toString(),
          replace: true,
        }),
    });

    if (data && row.editing) {
      const filename = data[0].response.body.finalName;
      if (!filename) {
        throw new Error(`response final name not found in ${data}`);
      }
      row.validator.patchValue({ positionPath: filename }, { onlySelf: false, emitEvent: true });
      this.markRowAsDirty(row);
      await this.save();
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { forceAllColumns: true, showSelectType: true, rowCountWarningThreshold: 1000, ...opts });
  }

  // async downloadShapefile(event: MouseEvent, row?: AsyncTableElement<MonitoringLocation>) {
  //   row = row || this.singleSelectedRow;
  //   await super.export(event, {
  //     exportType: 'SHAPEFILE',
  //     forceAllColumns: true,
  //     defaultFileName: referentialToString(row.currentData, attributes.labelName),
  //     defaultFilter: MonitoringLocationFilter.fromObject({ criterias: [{ id: row.currentData.id }] }),
  //   });
  // }

  async uploadShapefile(event: UIEvent) {
    if (!(await this.confirmEditCreate())) {
      return;
    }

    const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
      title: '',
      fileExtension: '.zip',
      maxParallelUpload: 2,
      uploadFn: (file: File) => this.uploadShapefileLocally(file),
    });
    if (isNotEmptyArray(data)) {
      this.markAsLoading();
      try {
        // Read id properties
        const monitoringLocationIds: number[] = [];
        for (const featureCollections of data.map((file) => file.response.body)) {
          for (const featureCollection of featureCollections) {
            for (const feature of featureCollection.features) {
              monitoringLocationIdShapeProperties.forEach((monitoringLocationIdShapeProperty) => {
                if (isInt(feature.properties[monitoringLocationIdShapeProperty])) {
                  monitoringLocationIds.push(feature.properties[monitoringLocationIdShapeProperty]);
                }
              });
            }
          }
        }

        let updateInheritedGeometries = false;
        if (isNotEmptyArray(monitoringLocationIds)) {
          // Check if survey exists with these locations (by inheritance)
          const surveyExists = await this.surveyService.exists(
            SurveyFilterCriteria.fromObject({
              monitoringLocationFilter: { includedIds: monitoringLocationIds },
              inheritedGeometry: true,
            })
          );
          if (surveyExists) {
            // Ask to update inherited geometries
            updateInheritedGeometries = await Alerts.askYesNoCancelConfirmation(
              'REFERENTIAL.MONITORING_LOCATION.CONFIRM.UPDATE_INHERITED_GEOMETRIES',
              this.alertCtrl,
              this.translate
            );
            if (isNil(updateInheritedGeometries)) {
              // User has cancel
              return;
            }
          }
        }

        // Send shapefiles to server
        const uploader = new UploadFileComponent(this.cd, this.translate);
        uploader.maxParallelUpload = 2;
        uploader.uploadFn = (file) => this.transferService.uploadShapefile(file);
        const uploadedFiles = await uploader.uploadFiles(
          data.map((file) => {
            delete file.progress;
            delete file.error;
            delete file.response;
            return file;
          })
        );
        // Find any error
        const error = uploadedFiles.find((value) => isNotNilString(value.error))?.error;
        if (error) {
          this.markAsError(error);
          return;
        }

        // Import shapefile asynchronously
        const filenames = data.map((value) => value.name);
        const job = await this._entityService.importShapefileAsync(filenames, { updateInheritedGeometries });
        if (!job || (job as IEntity<any>).__typename !== 'JobVO') {
          console.error(`${this.logPrefix} a Job should be returned !`, job);
          return;
        }
        this.jobService.listenImportShapeJob(job.id, this.dirtySubject, this);
      } finally {
        this.markAsLoaded();
      }
    }
  }

  private uploadShapefileLocally(file: File): Observable<FileEvent<FeatureCollectionWithFilename[]>> {
    console.info(`${this.logPrefix} Importing Shapefile ${file.name}`);

    return FileUtils.readAsArrayBuffer(file).pipe(
      mergeMap(async (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          const loaded = Math.round(event.loaded * 0.8);
          return { ...event, loaded };
        } else if (event instanceof FileResponse) {
          const parsed = await parseZip(event.body);
          if (Array.isArray(parsed)) {
            return new FileResponse<FeatureCollectionWithFilename[]>({ body: parsed });
          }
          return new FileResponse<FeatureCollectionWithFilename[]>({ body: [parsed] });
        } else {
          // Unknown event: skip
          return null;
        }
      }),
      filter(isNotNil),
      catchError((err) => {
        console.error(`${this.logPrefix} Error while parsing shapefile`, err);
        throw new Error(this.translate.instant('REFERENTIAL.MONITORING_LOCATION.ERROR.INVALID_SHAPEFILE'));
      })
    );
  }

  async report(event: UIEvent, row?: AsyncTableElement<MonitoringLocation>) {
    row = row || this.singleEditingRow;
    if (!row || !row.editing || !row.validator.valid || !row.currentData.id) {
      console.warn(`${this.logPrefix} Can't execute report: Invalid MonitoringLocation selected`, row);
      return;
    }
    await this.modalService.openModal<IMonitoringLocationReportModalOptions, void>(
      MonitoringLocationReportModal,
      {
        titleI18n: 'REFERENTIAL.MONITORING_LOCATION.REPORT_TITLE',
        monitoringLocationId: row.currentData.id,
        service: this._entityService,
      },
      'modal-medium'
    );
  }

  // async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
  //   const continueSave =
  //     (await this.saveRightView(this.taxonPositionMenuItem, this.taxonPositionTable())) &&
  //     (await this.saveRightView(this.taxonGroupPositionMenuItem, this.taxonGroupPositionTable()));
  //
  //   if (continueSave) {
  //     return super.save(opts);
  //   }
  //   return false;
  // }

  protected updateTitle() {
    this.title = this.titleI18n
      ? this.translate.instant(this.titleI18n)
      : ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.MONITORING_LOCATIONS' });
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<MonitoringLocation>[]): Promise<boolean> {
    if (!this.strategyService) {
      throw new Error(`${this.logPrefix} No strategy service found, can't check if monitoring location can be disabled`);
    }

    // Check if used in applied strategies
    const res = await this.strategyService.loadPage(
      { offset: 0, size: 100 },
      StrategyFilter.fromObject({
        criterias: [
          {
            programFilter: { statusId: StatusIds.ENABLE },
            onlyActive: true,
            monitoringLocationFilter: { includedIds: EntityUtils.ids(dirtyRowsWithDisabledStatusId) },
          },
        ],
      })
    );

    if (isNotEmptyArray(res.data)) {
      return Alerts.askConfirmation('REFERENTIAL.MONITORING_LOCATION.CONFIRM.DISABLE_APPLIED_STRATEGIES', this.alertCtrl, this.translate, undefined, {
        list: `<div class="scroll-content"><ul>${res.data
          .map((r) => referentialToString(r, ['programId', 'name']))
          .map((s) => `<li>${s}</li>`)
          .join('')}</ul></div>`,
      });
    }

    return true;
  }

  protected async loadRightArea(row?: AsyncTableElement<MonitoringLocation>) {
    if (this.subTable) return; // don't load if sub table
    switch (this.rightMenuItem?.view) {
      case 'programTable': {
        this.detectChanges();
        if (this.programTable()) {
          this.registerSubForm(this.programTable());
          await this.loadProgramTable(row);
        }
        break;
      }
      case 'taxonPositionTable': {
        this.detectChanges();
        if (this.taxonPositionTable()) {
          this.registerSubForm(this.taxonPositionTable());
          await this.loadTaxonPositionTable(row);
        }
        break;
      }
      case 'taxonGroupPositionTable': {
        this.detectChanges();
        if (this.taxonGroupPositionTable()) {
          this.registerSubForm(this.taxonGroupPositionTable());
          await this.loadTaxonGroupPositionTable(row);
        }
        break;
      }
      case 'monLocOrderItemTable': {
        this.detectChanges();
        if (this.monLocOrderItemTable()) {
          this.registerSubForm(this.monLocOrderItemTable());
          await this.loadMonLocOrderItemTable(row);
        }
        break;
      }
      case 'map': {
        this.detectChanges();
        await this.loadMap(this.selection.selected);
        break;
      }
    }
    await super.loadRightArea(row);
  }

  protected async loadProgramTable(row?: AsyncTableElement<MonitoringLocation>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.programTable().setMonitoringLocation(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    await this.loadDetailData(row);
    await this.programTable().setMonitoringLocation(row.currentData);
    this.rightAreaEnabled = false; // always read only
  }

  protected async loadTaxonPositionTable(row?: AsyncTableElement<MonitoringLocation>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.taxonPositionTable().setValue(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    await this.loadDetailData(row);
    await this.taxonPositionTable().setValue(row.currentData?.taxonPositions?.slice() || []);
    this.rightAreaEnabled = this.canEdit;
  }

  protected async loadTaxonGroupPositionTable(row?: AsyncTableElement<MonitoringLocation>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.taxonGroupPositionTable().setValue(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    await this.loadDetailData(row);
    await this.taxonGroupPositionTable().setValue(row.currentData?.taxonGroupPositions?.slice() || []);
    this.rightAreaEnabled = this.canEdit;
  }

  protected async loadMonLocOrderItemTable(row?: AsyncTableElement<MonitoringLocation>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.monLocOrderItemTable().setSelected(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    await this.loadDetailData(row);
    await this.monLocOrderItemTable().setSelected(row.currentData?.monLocOrderItems?.slice() || []);
    this.rightAreaEnabled = false; // read only
  }

  protected async loadDetailData(row?: AsyncTableElement<MonitoringLocation>) {
    // Call service if detail data is not loaded yet
    if (!row.currentData.detailLoaded) {
      this.programTable()?.markAsLoading();
      this.taxonPositionTable()?.markAsLoading();
      this.taxonGroupPositionTable()?.markAsLoading();
      this.monLocOrderItemTable()?.markAsLoading();

      // Get sub entities
      const [locationPrograms, taxonPositions, taxonGroupPositions, monLocOrderItems] = await Promise.all([
        this._entityService.loadPrograms(row.currentData.id),
        this._entityService.loadTaxonPositions(row.currentData.id),
        this._entityService.loadTaxonGroupPositions(row.currentData.id),
        this._entityService.loadMonLocOrderItems(row.currentData.id),
      ]);

      row.validator.patchValue({ locationPrograms, taxonPositions, taxonGroupPositions, monLocOrderItems, detailLoaded: true });
    }
  }

  protected async loadMap(rows?: AsyncTableElement<MonitoringLocation>[]) {
    if (isEmptyArray(rows)) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.map().loadGeoData(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    await this.loadGeometryData(rows);
    await this.map().loadGeoData(rows.map((row) => row.currentData.geometry));
    this.rightAreaEnabled = this.canEdit;
  }

  protected async loadGeometryData(rows?: AsyncTableElement<MonitoringLocation>[]) {
    this.map().markAsLoading();

    for (const row of rows) {
      if (!row.currentData.geometryLoaded) {
        const geometry = await this._entityService.loadGeometryFeature(row.currentData.id);

        if (!!geometry) {
          // Add properties
          geometry.properties = {
            ...geometry.properties,
            name: referentialToString(row.currentData, referentialOptions.monitoringLocation.attributes),
          };
        }

        row.validator.patchValue({ geometry, geometryLoaded: true });
      }
    }

    this.map().markAsLoaded();
  }

  protected getDefaultHiddenColumns(): string[] {
    return super
      .getDefaultHiddenColumns()
      .concat('comments', 'bathymetry', 'harbour', 'positioningSystemPrecision', 'positionPath', 'daylightSavingTime');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name', 'utFormat', 'positioningSystem');
  }

  protected rightSplitResized() {
    super.rightSplitResized();
    this.map()?.containerResize();
  }
}
