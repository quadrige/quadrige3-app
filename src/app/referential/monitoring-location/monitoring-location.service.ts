import { Injectable, Injector } from '@angular/core';
import { isEmptyArray, isNil } from '@sumaris-net/ngx-components';
import {
  BaseReferentialService,
  defaultReferentialSaveOption,
  IImportShapefileService,
  ImportShapefileServiceOptions,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { positioningSystemFragments } from '@app/referential/positioning-system/positioning-system.service';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { TaxonPosition } from '@app/referential/monitoring-location/taxon-position/taxon-position.model';
import { TaxonGroupPosition } from '@app/referential/monitoring-location/taxon-group-position/taxon-group.position.model';
import { Feature } from 'geojson';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { MonitoringLocationReport, MonLocOrderItem, monLocOrderItemFragment } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { LocationProgram } from '@app/referential/monitoring-location/program/location-program.model';
import { Job } from '@app/social/job/job.model';
import { FeatureRecord, Geometries } from '@app/shared/geometries';
import { jobFragments } from '@app/social/job/job.service';
import {
  MonitoringLocationFilter,
  MonitoringLocationFilterCriteria,
} from '@app/referential/monitoring-location/filter/monitoring-location.filter.model';

import { transcribingItemWithChildrenFragment } from '@app/referential/transcribing-item/transcribing-item.model';

const harbourFragment = gql`
  fragment HarbourFragment on HarbourVO {
    id
    name
    statusId
    __typename
  }
`;

const locationProgramFragment = gql`
  fragment LocationProgramFragment on ProgramLocationVO {
    id
    monitoringLocationId
    programId
    program {
      ...ReferentialFragment
    }
    moratoriumType
  }
  ${referentialFragments.light}
`;

const taxonPositionFragment = gql`
  fragment TaxonPositionFragment on TaxonPositionVO {
    referenceTaxon {
      ...ReferentialFragment
    }
    resourceType {
      ...ReferentialFragment
    }
  }
  ${referentialFragments.light}
`;

const taxonGroupPositionFragment = gql`
  fragment TaxonGroupPositionFragment on TaxonGroupPositionVO {
    taxonGroup {
      ...ReferentialFragment
    }
    resourceType {
      ...ReferentialFragment
    }
  }
  ${referentialFragments.light}
`;

const monitoringLocationReportFragment = gql`
  fragment MonitoringLocationReportFragment on MonitoringLocationReportVO {
    label
    expectedLabel
    monLocOrderItems {
      ...MonLocOrderItemFragment
    }
    expectedMonLocOrderItems {
      ...MonLocOrderItemFragment
    }
  }
  ${monLocOrderItemFragment}
`;

const monitoringLocationFragment = gql`
  fragment MonitoringLocationFragment on MonitoringLocationVO {
    id
    label
    name
    bathymetry
    utFormat
    daylightSavingTime
    positionPath
    coordinate {
      minLatitude
      minLongitude
      maxLatitude
      maxLongitude
    }
    positioningSystem {
      ...PositioningSystemFragment
    }
    harbour {
      ...HarbourFragment
    }
    comments
    updateDate
    creationDate
    statusId
    __typename
  }
  ${positioningSystemFragments.positioningSystem}
  ${harbourFragment}
  ${referentialFragments.light}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query MonitoringLocations($page: PageInput, $filter: MonitoringLocationFilterVOInput) {
      data: monitoringLocations(page: $page, filter: $filter) {
        ...MonitoringLocationFragment
      }
    }
    ${monitoringLocationFragment}
  `,

  loadAllWithTotal: gql`
    query MonitoringLocationsWithTotal($page: PageInput, $filter: MonitoringLocationFilterVOInput) {
      data: monitoringLocations(page: $page, filter: $filter) {
        ...MonitoringLocationFragment
      }
      total: monitoringLocationsCount(filter: $filter)
    }
    ${monitoringLocationFragment}
  `,

  countAll: gql`
    query MonitoringLocationsCount($filter: MonitoringLocationFilterVOInput) {
      total: monitoringLocationsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportMonitoringLocationsAsync($filter: MonitoringLocationFilterVOInput, $context: ExportContextInput) {
      data: exportMonitoringLocationsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveMonitoringLocations($data: [MonitoringLocationVOInput], $options: MonitoringLocationSaveOptionsInput) {
      data: saveMonitoringLocations(monitoringLocations: $data, options: $options) {
        ...MonitoringLocationFragment
        taxonPositions {
          ...TaxonPositionFragment
        }
        taxonGroupPositions {
          ...TaxonGroupPositionFragment
        }
        transcribingItems {
          ...TranscribingItemWithChildrenFragment
        }
      }
    }
    ${monitoringLocationFragment}
    ${taxonPositionFragment}
    ${taxonGroupPositionFragment}
    ${transcribingItemWithChildrenFragment}
  `,

  deleteAll: gql`
    mutation DeleteMonitoringLocations($ids: [Int]) {
      deleteMonitoringLocations(ids: $ids)
    }
  `,
};

const otherQueries = {
  loadPrograms: gql`
    query MonitoringLocationPrograms($id: Int!) {
      data: monitoringLocationPrograms(id: $id) {
        ...LocationProgramFragment
      }
    }
    ${locationProgramFragment}
  `,
  taxonPositions: gql`
    query TaxonPositions($id: Int!) {
      data: taxonPositions(id: $id) {
        ...TaxonPositionFragment
      }
    }
    ${taxonPositionFragment}
  `,
  taxonGroupPositions: gql`
    query TaxonGroupPositions($id: Int!) {
      data: taxonGroupPositions(id: $id) {
        ...TaxonGroupPositionFragment
      }
    }
    ${taxonGroupPositionFragment}
  `,
  monLocOrderItems: gql`
    query MonLocOrderItems($id: Int!) {
      data: monLocOrderItems(id: $id) {
        ...MonLocOrderItemFragment
      }
    }
    ${monLocOrderItemFragment}
  `,
  geometry: gql`
    query MonitoringLocationGeometry($id: Int!) {
      data: monitoringLocationGeometry(id: $id)
    }
  `,
  geometries: gql`
    query MonitoringLocationGeometries($ids: [Int]) {
      data: monitoringLocationGeometries(ids: $ids)
    }
  `,
  importShapefileAsync: gql`
    query ImportMonitoringLocationAsync($fileNames: [String], $options: MonitoringLocationImportOptionsInput) {
      data: importMonitoringLocationAsync(fileNames: $fileNames, options: $options) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
  report: gql`
    query ReportMonitoringLocation($id: Int) {
      data: reportMonitoringLocation(monitoringLocationId: $id) {
        ...MonitoringLocationReportFragment
      }
    }
    ${monitoringLocationReportFragment}
  `,
};

const defaultSaveOptions = {
  ...defaultReferentialSaveOption,
  withTaxonPositions: true,
  withTaxonGroupPositions: true,
  withGeometry: false, // important
};

export interface ImportMonitoringLocationOptions extends ImportShapefileServiceOptions {
  updateInheritedGeometries: boolean;
}

@Injectable({ providedIn: 'root' })
export class MonitoringLocationService
  extends BaseReferentialService<MonitoringLocation, MonitoringLocationFilter, MonitoringLocationFilterCriteria>
  implements IImportShapefileService
{
  constructor(protected injector: Injector) {
    super(injector, MonitoringLocation, MonitoringLocationFilter, { queries, mutations });
    this._logPrefix = '[monitoring-location-service]';
  }

  async save(entity: MonitoringLocation, opts?: EntitySaveOptions): Promise<MonitoringLocation> {
    return super.save(entity, { ...defaultSaveOptions, ...opts });
  }

  async saveAll(entities: MonitoringLocation[], opts?: EntitySaveOptions): Promise<MonitoringLocation[]> {
    return super.saveAll(entities, { ...defaultSaveOptions, ...opts });
  }

  async loadGeometryFeature(id: number): Promise<Feature> {
    if (isNil(id)) {
      return undefined;
    }

    const variables: any = { id };
    if (this._debug) console.debug(`${this._logPrefix} Loading geometry...`, variables);
    const now = Date.now();
    const query = otherQueries.geometry;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} geometry loaded in ${Date.now() - now}ms`, data);
    return Geometries.toFeature(id, data);
  }

  async loadGeometryFeatures(ids: number[]): Promise<FeatureRecord> {
    if (isEmptyArray(ids)) {
      return undefined;
    }

    const variables: any = { ids };
    if (this._debug) console.debug(`${this._logPrefix} Loading geometries...`, variables);
    const now = Date.now();
    const query = otherQueries.geometries;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} geometry loaded in ${Date.now() - now}ms`, data);
    return Geometries.toFeatureRecord(data);
  }

  async importShapefileAsync(fileNames: string[], options?: ImportMonitoringLocationOptions): Promise<Job> {
    if (isEmptyArray(fileNames)) {
      console.warn(`missing argument, check fileNames`);
      return undefined;
    }

    const variables: any = { fileNames, options };
    if (this._debug) console.debug(`${this._logPrefix} Import shapefile...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: Job }>({
      query: otherQueries.importShapefileAsync,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Shapefile import job started in ${Date.now() - now}ms`, data);
    return data;
  }

  async getReport(id: number): Promise<MonitoringLocationReport> {
    if (isNil(id)) {
      return undefined;
    }
    const variables: any = { id };
    if (this._debug) console.debug(`${this._logPrefix} Generating report...`, variables);
    const now = Date.now();
    const query = otherQueries.report;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Report generated in ${Date.now() - now}ms`, data);
    return MonitoringLocationReport.fromObject(data);
  }

  async loadPrograms(id: number): Promise<LocationProgram[]> {
    if (isNil(id)) {
      return [];
    }

    const variables: any = { id };
    if (this._debug) console.debug(`${this._logPrefix} Loading program ...`, variables);
    const now = Date.now();
    const query = otherQueries.loadPrograms;
    const res = await this.graphql.query<{ data: string[] }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(LocationProgram.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} program loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadTaxonPositions(id: number): Promise<TaxonPosition[]> {
    if (isNil(id)) {
      return [];
    }

    const variables: any = { id };
    if (this._debug) console.debug(`${this._logPrefix} Loading taxon positions...`, variables);
    const now = Date.now();
    const query = otherQueries.taxonPositions;
    const res = await this.graphql.query<{ data: any[] }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(TaxonPosition.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} taxon positions loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadTaxonGroupPositions(id: number): Promise<TaxonGroupPosition[]> {
    if (isNil(id)) {
      return [];
    }

    const variables: any = { id };
    if (this._debug) console.debug(`${this._logPrefix} Loading taxon group positions...`, variables);
    const now = Date.now();
    const query = otherQueries.taxonGroupPositions;
    const res = await this.graphql.query<{ data: any[] }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(TaxonGroupPosition.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} taxon group positions loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadMonLocOrderItems(id: number): Promise<MonLocOrderItem[]> {
    if (isNil(id)) {
      return [];
    }

    const variables: any = { id };
    if (this._debug) console.debug(`${this._logPrefix} Loading monitoring location order items...`, variables);
    const now = Date.now();
    const query = otherQueries.monLocOrderItems;
    const res = await this.graphql.query<{ data: any[] }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(MonLocOrderItem.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} monitoring location order items loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
