import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { isNotNil } from '@sumaris-net/ngx-components';
import {
  MonitoringLocationFilter,
  MonitoringLocationFilterCriteria,
} from '@app/referential/monitoring-location/filter/monitoring-location.filter.model';
import { geometryTypes } from '@app/referential/monitoring-location/geometry-type';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-monitoring-location-filter-form',
  templateUrl: './monitoring-location.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitoringLocationFilterForm extends ReferentialCriteriaFormComponent<MonitoringLocationFilter, MonitoringLocationFilterCriteria> {
  readonly geometryTypes = geometryTypes;

  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabelName;
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof MonitoringLocationFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias.concat('programFilter');
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: MonitoringLocationFilterCriteria): PartialRecord<keyof MonitoringLocationFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'metaProgramFilter');
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'strategyFilter');
    if (isNotNil(criteria.geometryType)) params.geometryType = criteria.geometryType;
    this.subCriteriaToQueryParams(params, criteria, 'orderItemFilter');
    return params;
  }
}
