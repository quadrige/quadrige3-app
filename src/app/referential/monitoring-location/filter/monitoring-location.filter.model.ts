import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { GeometryType } from '@app/referential/monitoring-location/geometry-type';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export interface IMonitoringLocationFilterCriteria extends IReferentialFilterCriteria<number> {
  metaProgramFilter?: IReferentialFilterCriteria<string>;
  programFilter?: IReferentialFilterCriteria<string>;
  strategyFilter?: IReferentialFilterCriteria<number>;
  orderItemFilter?: IReferentialFilterCriteria<number>;
  geometryType?: GeometryType;
}

@EntityClass({ typename: 'MonitoringLocationFilterCriteriaVO' })
export class MonitoringLocationFilterCriteria
  extends ReferentialFilterCriteria<MonitoringLocation, number>
  implements IMonitoringLocationFilterCriteria
{
  static fromObject: (source: any, opts?: any) => MonitoringLocationFilterCriteria;

  metaProgramFilter: StrReferentialFilterCriteria = null;
  programFilter: StrReferentialFilterCriteria = null;
  strategyFilter: IntReferentialFilterCriteria = null;
  orderItemFilter: IntReferentialFilterCriteria = null;
  geometryType: GeometryType = null;

  constructor() {
    super(MonitoringLocationFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.metaProgramFilter = StrReferentialFilterCriteria.fromObject(source.metaProgramFilter || {});
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.strategyFilter = IntReferentialFilterCriteria.fromObject(source.strategyFilter || {});
    this.orderItemFilter = IntReferentialFilterCriteria.fromObject(source.orderItemFilter || {});
    this.geometryType = source.geometryType;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.metaProgramFilter = this.metaProgramFilter?.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    target.strategyFilter = { ...this.strategyFilter?.asObject(opts), searchAttributes: ['id', 'name', 'description'] };
    target.orderItemFilter = this.orderItemFilter?.asObject(opts);
    if (opts?.minify) {
      delete target.orderItemType;
    }
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'metaProgramFilter') return !BaseFilterUtils.isCriteriaEmpty(this.metaProgramFilter, true);
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'strategyFilter') return !BaseFilterUtils.isCriteriaEmpty(this.strategyFilter, true);
    if (key === 'orderItemFilter') return !BaseFilterUtils.isCriteriaEmpty(this.orderItemFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'MonitoringLocationFilterVO' })
export class MonitoringLocationFilter extends ReferentialFilter<MonitoringLocationFilter, MonitoringLocationFilterCriteria, MonitoringLocation> {
  static fromObject: (source: any, opts?: any) => MonitoringLocationFilter;

  constructor() {
    super(MonitoringLocationFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): MonitoringLocationFilterCriteria {
    return MonitoringLocationFilterCriteria.fromObject(source, opts);
  }
}
