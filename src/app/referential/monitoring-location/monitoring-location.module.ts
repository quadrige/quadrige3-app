import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { MonitoringLocationTable } from '@app/referential/monitoring-location/monitoring-location.table';
import { TaxonPositionTable } from '@app/referential/monitoring-location/taxon-position/taxon-position.table';
import { TaxonGroupPositionTable } from '@app/referential/monitoring-location/taxon-group-position/taxon-group-position.table';
import { LocationProgramTable } from '@app/referential/monitoring-location/program/location-program.table';
import { MonitoringLocationFilterForm } from '@app/referential/monitoring-location/filter/monitoring-location.filter.form';
import { MonLocOrderItemModule } from '@app/referential/mon-loc-order-item/mon-loc-order-item.module';

@NgModule({
  imports: [
    ReferentialModule,
    MonitoringLocationFilterForm,
    LocationProgramTable,
    TaxonPositionTable,
    TaxonGroupPositionTable,
    MonLocOrderItemModule,
  ],
  declarations: [MonitoringLocationTable],
  exports: [MonitoringLocationTable],
})
export class MonitoringLocationModule {}
