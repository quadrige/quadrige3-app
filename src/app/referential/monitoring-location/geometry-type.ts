export type GeometryType = 'POINT' | 'LINE' | 'AREA';

export interface IGeometryType {
  id: GeometryType;
  label: string;
}

export const geometryTypes: IGeometryType[] = [
  {
    id: 'POINT',
    label: 'REFERENTIAL.GEOMETRY_TYPE.POINT',
  },
  {
    id: 'LINE',
    label: 'REFERENTIAL.GEOMETRY_TYPE.LINE',
  },
  {
    id: 'AREA',
    label: 'REFERENTIAL.GEOMETRY_TYPE.AREA',
  },
];
