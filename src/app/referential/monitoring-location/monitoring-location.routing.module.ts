import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { MonitoringLocationTable } from '@app/referential/monitoring-location/monitoring-location.table';
import { MonitoringLocationModule } from '@app/referential/monitoring-location/monitoring-location.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MonitoringLocationTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), MonitoringLocationModule],
})
export class MonitoringLocationRoutingModule {}
