import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'TaxonGroupPositionVO' })
export class TaxonGroupPosition extends Referential<TaxonGroupPosition, string> {
  static entityName = 'TaxonGroupPosition';
  static fromObject: (source: any, opts?: any) => TaxonGroupPosition;

  taxonGroup: IntReferential = null;
  resourceType: IntReferential = null;

  constructor() {
    super(TaxonGroupPosition.TYPENAME);
    this.entityName = TaxonGroupPosition.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = TaxonGroupPosition.entityName;
    this.taxonGroup = IntReferential.fromObject(source.taxonGroup);
    this.resourceType = IntReferential.fromObject(source.resourceType);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.taxonGroup = EntityUtils.asMinifiedObject(this.taxonGroup, opts);
    target.resourceType = EntityUtils.asMinifiedObject(this.resourceType, opts);
    delete target.entityName;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.statusId;
    return target;
  }
}
