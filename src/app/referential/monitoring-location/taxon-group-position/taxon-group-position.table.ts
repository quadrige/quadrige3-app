import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { TaxonGroupPosition } from '@app/referential/monitoring-location/taxon-group-position/taxon-group.position.model';
import { TaxonGroupPositionValidatorService } from '@app/referential/monitoring-location/taxon-group-position/taxon-group-position.validator';
import { TaxonGroupPositionService } from '@app/referential/monitoring-location/taxon-group-position/taxon-group-position.service';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-taxon-group-position-table',
  templateUrl: './taxon-group-position.table.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonGroupPositionTable
  extends ReferentialMemoryTable<TaxonGroupPosition, string, StrReferentialFilter, StrReferentialFilterCriteria, TaxonGroupPositionValidatorService>
  implements OnInit, AfterViewInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: TaxonGroupPositionService,
    protected validatorService: TaxonGroupPositionValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['taxonGroup', 'resourceType']).concat(RESERVED_END_COLUMNS),
      TaxonGroupPosition,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.TAXON_GROUP_POSITION.';
    this.defaultSortBy = 'taxonGroup';
    this.logPrefix = '[taxon-group-position-table]';
  }

  ngOnInit() {
    this.subTable = true;
    this.showPaginator = false;

    super.ngOnInit();

    // taxon group combo
    this.registerAutocompleteField('taxonGroup', {
      ...this.referentialOptions.taxonGroup,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'TaxonGroup',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // resource type combo
    this.registerAutocompleteField('resourceType', {
      ...this.referentialOptions.resourceType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ResourceType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('taxonGroup');
  }
}
