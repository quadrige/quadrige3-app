import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators } from '@sumaris-net/ngx-components';
import { TaxonGroupPosition } from '@app/referential/monitoring-location/taxon-group-position/taxon-group.position.model';

@Injectable({ providedIn: 'root' })
export class TaxonGroupPositionValidatorService extends ReferentialValidatorService<TaxonGroupPosition, string> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: TaxonGroupPosition, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return {
      id: [data?.id || null],
      taxonGroup: [data?.taxonGroup || null, Validators.compose([Validators.required, SharedValidators.entity])],
      resourceType: [data?.resourceType || null, SharedValidators.entity],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }
}
