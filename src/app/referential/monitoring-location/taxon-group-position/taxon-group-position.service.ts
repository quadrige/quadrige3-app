import { Injectable } from '@angular/core';
import { TaxonGroupPosition } from '@app/referential/monitoring-location/taxon-group-position/taxon-group.position.model';
import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';

@Injectable({ providedIn: 'root' })
export class TaxonGroupPositionService extends UnfilteredEntitiesMemoryService<TaxonGroupPosition, string> {
  constructor() {
    super(TaxonGroupPosition);
  }
}
