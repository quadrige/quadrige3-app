import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { MonitoringLocationTable } from '@app/referential/monitoring-location/monitoring-location.table';
import { MonitoringLocationService } from '@app/referential/monitoring-location/monitoring-location.service';
import { MonitoringLocationValidatorService } from '@app/referential/monitoring-location/monitoring-location.validator';
import { PositioningSystemService } from '@app/referential/positioning-system/positioning-system.service';
import { ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import {
  IMonitoringLocationFilterCriteria,
  MonitoringLocationFilter,
  MonitoringLocationFilterCriteria,
} from '@app/referential/monitoring-location/filter/monitoring-location.filter.model';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { MonitoringLocationFilterForm } from '@app/referential/monitoring-location/filter/monitoring-location.filter.form';
import { LocationProgramTable } from '@app/referential/monitoring-location/program/location-program.table';
import { TaxonPositionTable } from '@app/referential/monitoring-location/taxon-position/taxon-position.table';
import { TaxonGroupPositionTable } from '@app/referential/monitoring-location/taxon-group-position/taxon-group-position.table';
import { MonLocOrderItemModule } from '@app/referential/mon-loc-order-item/mon-loc-order-item.module';

export type IMonitoringLocationAddOptions = ISelectModalOptions<IMonitoringLocationFilterCriteria>;

@Component({
  selector: 'app-monitoring-location-add-table',
  templateUrl: './monitoring-location.table.html',
  standalone: true,
  imports: [
    ReferentialModule,
    MonitoringLocationFilterForm,
    LocationProgramTable,
    TaxonPositionTable,
    TaxonGroupPositionTable,
    MonLocOrderItemModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitoringLocationAddTable extends MonitoringLocationTable implements IAddTable<MonitoringLocation> {
  @Input() addCriteria: Partial<MonitoringLocationFilterCriteria>;
  @Input() addFirstCriteria: Partial<MonitoringLocationFilterCriteria>;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: MonitoringLocationService,
    protected validatorService: MonitoringLocationValidatorService,
    protected positioningSystemService: PositioningSystemService
  ) {
    super(injector, _entityService, validatorService, positioningSystemService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[monitoring-location-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: MonitoringLocationFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('minLatitude', 'minLongitude', 'maxLatitude', 'maxLongitude', 'positioningSystem', 'utFormat');
  }
}
