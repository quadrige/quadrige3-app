import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { TaxonPosition } from '@app/referential/monitoring-location/taxon-position/taxon-position.model';
import { TaxonPositionValidatorService } from '@app/referential/monitoring-location/taxon-position/taxon-position.validator';
import { TaxonPositionService } from '@app/referential/monitoring-location/taxon-position/taxon-position.service';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ReferentialModule } from '@app/referential/referential.module';

@Component({
  selector: 'app-taxon-position-table',
  templateUrl: './taxon-position.table.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxonPositionTable
  extends ReferentialMemoryTable<TaxonPosition, string, StrReferentialFilter, StrReferentialFilterCriteria, TaxonPositionValidatorService>
  implements OnInit, AfterViewInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: TaxonPositionService,
    protected validatorService: TaxonPositionValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['referenceTaxon', 'resourceType']).concat(RESERVED_END_COLUMNS),
      TaxonPosition,
      _entityService,
      validatorService,
      undefined
    );

    this.i18nColumnPrefix = 'REFERENTIAL.TAXON_POSITION.';
    this.defaultSortBy = 'referenceTaxon';
    this.logPrefix = '[taxon-position-table]';
  }

  ngOnInit() {
    this.subTable = true;
    this.showPaginator = false;

    super.ngOnInit();

    // reference taxon combo
    this.registerAutocompleteField('referenceTaxon', {
      ...this.referentialOptions.referenceTaxon,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ReferenceTaxon',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // resource type combo
    this.registerAutocompleteField('resourceType', {
      ...this.referentialOptions.resourceType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'ResourceType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('referenceTaxon');
  }
}
