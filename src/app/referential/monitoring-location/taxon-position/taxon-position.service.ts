import { Injectable } from '@angular/core';
import { TaxonPosition } from '@app/referential/monitoring-location/taxon-position/taxon-position.model';
import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';

@Injectable({ providedIn: 'root' })
export class TaxonPositionService extends UnfilteredEntitiesMemoryService<TaxonPosition, string> {
  constructor() {
    super(TaxonPosition);
  }
}
