import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'TaxonPositionVO' })
export class TaxonPosition extends Referential<TaxonPosition, string> {
  static entityName = 'TaxonPosition';
  static fromObject: (source: any, opts?: any) => TaxonPosition;

  referenceTaxon: IntReferential = null;
  resourceType: IntReferential = null;

  constructor() {
    super(TaxonPosition.TYPENAME);
    this.entityName = TaxonPosition.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = TaxonPosition.entityName;
    this.referenceTaxon = IntReferential.fromObject(source.referenceTaxon);
    this.resourceType = IntReferential.fromObject(source.resourceType);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.referenceTaxon = EntityUtils.asMinifiedObject(this.referenceTaxon, opts);
    target.resourceType = EntityUtils.asMinifiedObject(this.resourceType, opts);
    delete target.entityName;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.statusId;
    return target;
  }
}
