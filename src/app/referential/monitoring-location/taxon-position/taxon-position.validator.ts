import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators } from '@sumaris-net/ngx-components';
import { TaxonPosition } from '@app/referential/monitoring-location/taxon-position/taxon-position.model';

@Injectable({ providedIn: 'root' })
export class TaxonPositionValidatorService extends ReferentialValidatorService<TaxonPosition, string> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: TaxonPosition, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return {
      id: [data?.id || null],
      referenceTaxon: [data?.referenceTaxon || null, Validators.compose([Validators.required, SharedValidators.entity])],
      resourceType: [data?.resourceType || null, SharedValidators.entity],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }
}
