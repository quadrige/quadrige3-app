import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential, StrReferential } from '@app/referential/model/referential.model';
import { PositioningSystem } from '@app/referential/positioning-system/positioning-system.model';
import { TaxonPosition } from '@app/referential/monitoring-location/taxon-position/taxon-position.model';
import { TaxonGroupPosition } from '@app/referential/monitoring-location/taxon-group-position/taxon-group.position.model';
import { Feature } from 'geojson';
import { MonLocOrderItem } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { LocationProgram } from '@app/referential/monitoring-location/program/location-program.model';
import { EntityUtils } from '@app/shared/entity.utils';

export const monitoringLocationIdShapeProperties = ['LS_ID', 'ID_LDS'];

@EntityClass({ typename: 'MonitoringLocationVO' })
export class MonitoringLocation extends Referential<MonitoringLocation> {
  static entityName = 'MonitoringLocation';
  static fromObject: (source: any, opts?: any) => MonitoringLocation;

  bathymetry: number = null;
  utFormat: number = null;
  daylightSavingTime: boolean = null;
  positionPath: string = null;
  positioningSystem: PositioningSystem = null;
  harbour: StrReferential = null;
  minLatitude: number = null;
  minLongitude: number = null;
  maxLatitude: number = null;
  maxLongitude: number = null;
  geometry: Feature = null;
  geometryLoaded = false;
  locationPrograms: LocationProgram[] = null;
  taxonPositions: TaxonPosition[] = null;
  taxonGroupPositions: TaxonGroupPosition[] = null;
  monLocOrderItems: MonLocOrderItem[] = null;
  detailLoaded = false;

  constructor() {
    super(MonitoringLocation.TYPENAME);
    this.entityName = MonitoringLocation.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = MonitoringLocation.entityName;
    this.bathymetry = source.bathymetry;
    this.utFormat = source.utFormat;
    this.daylightSavingTime = source.daylightSavingTime;
    this.positionPath = source.positionPath;
    this.positioningSystem = PositioningSystem.fromObject(source.positioningSystem);
    this.harbour = StrReferential.fromObject(source.harbour);
    this.minLatitude = source.minLatitude || source.coordinate?.minLatitude;
    this.minLongitude = source.minLongitude || source.coordinate?.minLongitude;
    this.maxLatitude = source.maxLatitude || source.coordinate?.maxLatitude;
    this.maxLongitude = source.maxLongitude || source.coordinate?.maxLongitude;
    this.locationPrograms = source.locationPrograms?.map(LocationProgram.fromObject) || [];
    this.taxonPositions = source.taxonPositions?.map(TaxonPosition.fromObject) || [];
    this.taxonGroupPositions = source.taxonGroupPositions?.map(TaxonGroupPosition.fromObject) || [];
    this.monLocOrderItems = source.monLocOrderItems?.map(MonLocOrderItem.fromObject) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    if (opts?.minify !== true) {
      target.positioningSystem = EntityUtils.asMinifiedObject(this.positioningSystem, opts);
      target.harbour = EntityUtils.asMinifiedObject(this.harbour, opts);
      target.taxonPositions = this.taxonPositions?.map((value) => value.asObject(opts));
      target.taxonGroupPositions = this.taxonGroupPositions?.map((value) => value.asObject(opts));
    }
    delete target.geometry;
    delete target.geometryLoaded;
    delete target.detailLoaded;
    delete target.minLatitude;
    delete target.minLongitude;
    delete target.maxLatitude;
    delete target.maxLongitude;
    delete target.monLocOrderItems;
    delete target.locationPrograms;
    return target;
  }
}
