import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { filter } from 'rxjs/operators';
import { LocationProgram } from './location-program.model';
import { LocationProgramService } from '@app/referential/monitoring-location/program/location-program.service';
import { allMoratoriumTypes } from '@app/referential/monitoring-location/program/moratorium-type';
import {
  LocationProgramFilter,
  LocationProgramFilterCriteria,
} from '@app/referential/monitoring-location/program/filter/location-program.filter.model';
import { Utils } from '@app/shared/utils';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { LocationProgramFilterForm } from '@app/referential/monitoring-location/program/filter/location-program.filter.form';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { StrategyHistoryTable } from '@app/referential/program-strategy/strategy/history/strategy-history.table';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { StrategyHistory } from '@app/referential/program-strategy/strategy/history/strategy-history.model';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';

@Component({
  selector: 'app-location-program-table',
  templateUrl: './location-program.table.html',
  standalone: true,
  imports: [ReferentialModule, LocationProgramFilterForm, StrategyHistoryTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LocationProgramTable
  extends ReferentialMemoryTable<LocationProgram, string, LocationProgramFilter, LocationProgramFilterCriteria /*, LocationProgramValidatorService*/>
  implements OnInit, AfterViewInit
{
  protected moratoriumTypesById = Utils.toRecord(allMoratoriumTypes, 'id');
  protected monitoringLocationId: number;
  protected strategyHistories: StrategyHistory[] = [];

  strategyHistoryTable = viewChild<StrategyHistoryTable>('strategyHistoryTable');

  constructor(
    protected injector: Injector,
    protected _entityService: LocationProgramService,
    protected strategyService: StrategyService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['program.name', 'program.statusId', 'moratoriumType']).concat(RESERVED_END_COLUMNS),
      LocationProgram,
      _entityService,
      undefined
    );

    this.titleI18n = 'REFERENTIAL.MONITORING_LOCATION_PROGRAM.TITLE';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.MONITORING_LOCATION_PROGRAM.';
    this.defaultSortBy = 'id';
    this.logPrefix = '[program-location-table]';
  }

  moratoriumTypesDisplayFn = (data: LocationProgram) => this.translate.instant(this.moratoriumTypesById[data?.moratoriumType]?.i18nLabel);

  ngOnInit() {
    this.subTable = true;
    this.inlineEdition = false;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;

    // Listen sort change
    this.registerSubscription(
      this.onSort.subscribe(() => this.markAsLoading()) // prevent selection change on sort
    );

    // Listen end of loading to refresh total row count
    this.registerSubscription(
      this.loadingSubject.pipe(filter((value) => !value)).subscribe(() => {
        if (this.value === undefined) {
          // this will remove the 'no result' template if the selection is voluntarily empty (=== undefined)
          this.totalRowCount = null;
        }
      })
    );
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.restoreRightPanel(true, 50);
  }

  async setMonitoringLocation(monitoringLocation: MonitoringLocation) {
    // clear selection
    this.selection.clear();
    // affect values
    await this.resetFilter(undefined, { emitEvent: false });
    await this.setValue(monitoringLocation?.locationPrograms?.slice());

    this.monitoringLocationId = monitoringLocation?.id;
    this.strategyHistories = this.monitoringLocationId ? await this.strategyService.loadStrategiesHistory(this.monitoringLocationId, undefined) : [];
  }

  async onAfterSelectionChange(row?: AsyncTableElement<LocationProgram>): Promise<void> {
    this.detectChanges();
    if (this.strategyHistoryTable()) {
      this.registerSubForm(this.strategyHistoryTable());
      await this.loadStrategyHistoryTable();
    }
    await super.onAfterSelectionChange(row);
  }

  private async loadStrategyHistoryTable() {
    if (!this.monitoringLocationId || this.selection.isEmpty()) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.strategyHistoryTable().setValue(undefined);
      await this.strategyHistoryTable().resetFilter();
      return;
    }

    await this.strategyHistoryTable().setValue(this.strategyHistories);
    await this.strategyHistoryTable().resetFilter(this.strategyHistoryTable().filter, { staticCriteria: { programIds: this.selectedEntityIds } });
  }
}
