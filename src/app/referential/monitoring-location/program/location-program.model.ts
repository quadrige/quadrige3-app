import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { MoratoriumType } from '@app/referential/monitoring-location/program/moratorium-type';
import { GenericReferential } from '@app/referential/generic/generic.model';

/**
 * Inverse usage of ProgramLocation
 * Read-only
 */
@EntityClass({ typename: 'ProgramLocationVO' })
export class LocationProgram extends Referential<LocationProgram, string> {
  static entityName = 'ProgramLocation';
  static fromObject: (source: any, opts?: any) => LocationProgram;

  program: GenericReferential = null;
  moratoriumType: MoratoriumType = null;
  monitoringLocationId: number = null;

  constructor() {
    super(LocationProgram.TYPENAME);
    this.entityName = LocationProgram.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = LocationProgram.entityName;
    this.program = GenericReferential.fromObject(source.program);
    this.id = this.program.id;
    this.moratoriumType = source.moratoriumType;
    this.monitoringLocationId = source.monitoringLocationId;
  }
}
