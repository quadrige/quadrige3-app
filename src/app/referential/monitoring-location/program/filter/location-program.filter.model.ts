import { EntityClass, FilterFn, isNotNil } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { MoratoriumType } from '@app/referential/monitoring-location/program/moratorium-type';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { LocationProgram } from '@app/referential/monitoring-location/program/location-program.model';

/**
 * Inverse usage of ProgramLocation
 * Read-only
 */

@EntityClass({ typename: 'ProgramLocationFilterCriteriaVO' })
export class LocationProgramFilterCriteria extends ReferentialFilterCriteria<LocationProgram, string> {
  static fromObject: (source: any, opts?: any) => LocationProgramFilterCriteria;

  moratoriumType: MoratoriumType = null;

  constructor() {
    super(LocationProgramFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.moratoriumType = source.moratoriumType;
  }

  protected buildFilter(): FilterFn<LocationProgram>[] {
    const target: FilterFn<LocationProgram>[] = []; // don't call super.buildFilter();

    // Filter by status
    if (isNotNil(this.statusId)) {
      target.push((entity) => this.statusId === entity.program?.statusId);
    }

    // Filter by text search
    const searchTextFilter = EntityUtils.searchTextFilter(['program.id', 'program.name'], this.searchText);
    if (searchTextFilter) target.push(searchTextFilter);

    // Filter by moratorium type
    if (this.moratoriumType) {
      target.push((entity) => {
        if (this.moratoriumType === 'UNDER_MORATORIUM') {
          return entity.moratoriumType === 'UNDER_MORATORIUM' || entity.moratoriumType === 'UNDER_ACTIVE_MORATORIUM';
        } else {
          return this.moratoriumType === entity.moratoriumType;
        }
      });
    }

    return target;
  }
}

@EntityClass({ typename: 'ProgramLocationFilterVO' })
export class LocationProgramFilter extends ReferentialFilter<LocationProgramFilter, LocationProgramFilterCriteria, LocationProgram, string> {
  static fromObject: (source: any, opts?: any) => LocationProgramFilter;

  constructor() {
    super(LocationProgramFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): LocationProgramFilterCriteria {
    return LocationProgramFilterCriteria.fromObject(source, opts);
  }
}
