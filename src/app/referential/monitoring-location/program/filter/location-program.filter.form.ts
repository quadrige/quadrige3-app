import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { isNotNil } from '@sumaris-net/ngx-components';
import {
  LocationProgramFilter,
  LocationProgramFilterCriteria,
} from '@app/referential/monitoring-location/program/filter/location-program.filter.model';
import { allMoratoriumTypes } from '@app/referential/monitoring-location/program/moratorium-type';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-location-program-filter-form',
  templateUrl: './location-program.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LocationProgramFilterForm extends ReferentialCriteriaFormComponent<LocationProgramFilter, LocationProgramFilterCriteria> {
  moratoriumTypes = allMoratoriumTypes;

  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName.map((attribute) => `program.${attribute}`);
  }

  protected criteriaToQueryParams(criteria: LocationProgramFilterCriteria): PartialRecord<keyof LocationProgramFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.moratoriumType)) params.moratoriumType = criteria.moratoriumType;
    return params;
  }
}
