import { ISimpleType } from '@app/shared/model/interface';
import { Utils } from '@app/shared/utils';

export type MoratoriumType = 'NO_MORATORIUM' | 'UNDER_MORATORIUM' | 'UNDER_ACTIVE_MORATORIUM';
export type IMoratoriumType = ISimpleType<MoratoriumType>;

export const allMoratoriumTypes: IMoratoriumType[] = Utils.buildSimpleTypeArray(
  ['NO_MORATORIUM', 'UNDER_MORATORIUM', 'UNDER_ACTIVE_MORATORIUM'],
  'REFERENTIAL.MORATORIUM_TYPE'
);
