import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { LocationProgram } from '@app/referential/monitoring-location/program/location-program.model';
import { LocationProgramFilter } from '@app/referential/monitoring-location/program/filter/location-program.filter.model';

@Injectable({ providedIn: 'root' })
export class LocationProgramService extends EntitiesMemoryService<LocationProgram, LocationProgramFilter, string> {
  constructor() {
    super(LocationProgram, LocationProgramFilter, { sortByReplacement: { id: 'program.id' } });
  }
}
