import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { MonitoringLocationTable } from '@app/referential/monitoring-location/monitoring-location.table';
import { MonitoringLocationService } from '@app/referential/monitoring-location/monitoring-location.service';
import { MonitoringLocationValidatorService } from '@app/referential/monitoring-location/monitoring-location.validator';
import { PositioningSystemService } from '@app/referential/positioning-system/positioning-system.service';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { ISelectTable } from '@app/shared/table/table.model';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { MonitoringLocationFilter } from '@app/referential/monitoring-location/filter/monitoring-location.filter.model';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { MonitoringLocationFilterForm } from '@app/referential/monitoring-location/filter/monitoring-location.filter.form';
import { LocationProgramTable } from '@app/referential/monitoring-location/program/location-program.table';
import { TaxonPositionTable } from '@app/referential/monitoring-location/taxon-position/taxon-position.table';
import { TaxonGroupPositionTable } from '@app/referential/monitoring-location/taxon-group-position/taxon-group-position.table';
import { MonLocOrderItemModule } from '@app/referential/mon-loc-order-item/mon-loc-order-item.module';

@Component({
  selector: 'app-monitoring-location-select-table',
  templateUrl: './monitoring-location.table.html',
  standalone: true,
  imports: [
    ReferentialModule,
    MonitoringLocationFilterForm,
    LocationProgramTable,
    TaxonPositionTable,
    TaxonGroupPositionTable,
    MonLocOrderItemModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitoringLocationSelectTable extends MonitoringLocationTable implements ISelectTable<MonitoringLocation> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria;
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();

  constructor(
    protected injector: Injector,
    protected _entityService: MonitoringLocationService,
    protected validatorService: MonitoringLocationValidatorService,
    protected positioningSystemService: PositioningSystemService
  ) {
    super(injector, _entityService, validatorService, positioningSystemService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[monitoring-location-select-table]';
    this.selectTable = true;
  }

  async resetFilter(filter?: MonitoringLocationFilter, opts?: ISetFilterOptions) {
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit();
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = EntityUtils.ids(toDelete).map((id) => id.toString());
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    await this.resetFilter();
  }

  updatePermission() {
    this.tableButtons.canAdd = true;
    this.tableButtons.canDelete = true;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('minLatitude', 'minLongitude', 'maxLatitude', 'maxLongitude', 'positioningSystem', 'utFormat');
  }
}
