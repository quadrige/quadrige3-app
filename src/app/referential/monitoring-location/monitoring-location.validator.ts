import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { TranslateService } from '@ngx-translate/core';
import { MonitoringLocation } from '@app/referential/monitoring-location/monitoring-location.model';
import { MonitoringLocationService } from '@app/referential/monitoring-location/monitoring-location.service';

@Injectable({ providedIn: 'root' })
export class MonitoringLocationValidatorService extends ReferentialValidatorService<MonitoringLocation> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: MonitoringLocationService,
    protected translate: TranslateService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: MonitoringLocation, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      label: [data?.label || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      bathymetry: [toNumber(data?.bathymetry, null), SharedValidators.decimal()],
      minLatitude: [data?.minLatitude || null],
      minLongitude: [data?.minLongitude || null],
      maxLatitude: [data?.maxLatitude || null],
      maxLongitude: [data?.maxLongitude || null],
      geometry: [data?.geometry || null],
      geometryLoaded: [toBoolean(data?.geometryLoaded, false)],
      utFormat: [
        toNumber(data?.utFormat, 1),
        Validators.compose([Validators.required, SharedValidators.decimal({ maxDecimals: 1 }), Validators.min(-12), Validators.max(12)]),
      ],
      daylightSavingTime: [data?.daylightSavingTime || false, Validators.required],
      positionPath: [data?.positionPath, Validators.maxLength(lengthDescription)],
      harbour: [data?.harbour || null, SharedValidators.entity],
      positioningSystem: [data?.positioningSystem || null, Validators.compose([Validators.required, SharedValidators.entity])],
      locationPrograms: [data?.locationPrograms || null],
      taxonPositions: [data?.taxonPositions || null],
      taxonGroupPositions: [data?.taxonGroupPositions || null],
      monLocOrderItems: [data?.monLocOrderItems || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      detailLoaded: [toBoolean(data?.detailLoaded, false)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
