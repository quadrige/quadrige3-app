import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { Injectable, Injector } from '@angular/core';
import { Filter, FilterFilter, FilterFilterCriteria } from '@app/referential/filter/model/filter.model';

const filterCriteriaValueFragment = gql`
  fragment FilterCriteriaValueFragment on FilterCriteriaValueVO {
    id
    value
    criteriaId
    updateDate
    __typename
  }
`;

const filterCriteriaFragment = gql`
  fragment FilterCriteriaFragment on FilterCriteriaVO {
    id
    filterCriteriaType
    filterOperatorType
    inverse
    systemId
    values {
      ...FilterCriteriaValueFragment
    }
    blockId
    updateDate
    __typename
  }
  ${filterCriteriaValueFragment}
`;

const filterBlockFragment = gql`
  fragment FilterBlockFragment on FilterBlockVO {
    id
    rankOrder
    criterias {
      ...FilterCriteriaFragment
    }
    filterId
    updateDate
    __typename
  }
  ${filterCriteriaFragment}
`;

export const filterFragment = gql`
  fragment FilterFragment on FilterVO {
    id
    name
    filterType
    defaultFilter
    extraction
    blocks {
      ...FilterBlockFragment
    }
    userId
    departmentId
    updateDate
    __typename
  }
  ${filterBlockFragment}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Filters($page: PageInput, $filter: FilterFilterVOInput) {
      data: filters(page: $page, filter: $filter) {
        ...FilterFragment
      }
    }
    ${filterFragment}
  `,
  loadAllWithTotal: gql`
    query FiltersWithCount($page: PageInput, $filter: FilterFilterVOInput) {
      data: filters(page: $page, filter: $filter) {
        ...FilterFragment
      }
      total: filtersCount(filter: $filter)
    }
    ${filterFragment}
  `,
  countAll: gql`
    query FiltersCount($filter: FilterFilterVOInput) {
      total: filtersCount(filter: $filter)
    }
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveFilters($data: [FilterVOInput]) {
      data: saveFilters(filters: $data) {
        ...FilterFragment
      }
    }
    ${filterFragment}
  `,
  deleteAll: gql`
    mutation DeleteFilters($ids: [Int]) {
      deleteFilters(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class FilterService extends BaseReferentialService<Filter, FilterFilter, FilterFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Filter, FilterFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[filter-service]';
  }
}
