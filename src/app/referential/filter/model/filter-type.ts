export type FilterType =
  // Used for result and in-situ extraction
  | 'EXTRACT_DATA_MAIN'
  | 'EXTRACT_DATA_PERIOD'
  | 'EXTRACT_DATA_SURVEY'
  | 'EXTRACT_DATA_SAMPLING_OPERATION'
  | 'EXTRACT_DATA_SAMPLE'
  | 'EXTRACT_DATA_MEASUREMENT'
  | 'EXTRACT_DATA_PHOTO'
  | 'EXTRACT_CAMPAIGN'
  | 'EXTRACT_OCCASION'
  | 'EXTRACT_EVENT'

  // Default
  | 'PROGRAM_STRATEGY'
  | 'MONITORING_LOCATION'
  | 'CAMPAIGN_OCCASION'
  | 'SURVEY'
  | 'EVENT'
  | 'INITIAL_POP_BATCH'
  | 'BATCH'
  | 'DEPARTMENT'
  | 'USER'
  | 'PMFMU'
  | 'TAXON_NAME'
  | 'TAXON_GROUP'
  | 'META_PROGRAM'
  | 'AUTHOR'
  | 'CITATION'
  | 'REFERENCE_DOCUMENT'
  | 'EXTRACT_FILTER'
  | 'AGE_GROUP'
  | 'PARAMETER_GROUP'
  | 'ANALYSIS_INSTRUMENT'
  | 'STATUS'
  | 'SAMPLING_EQUIPMENT'
  | 'UI_FUNCTION'
  | 'FREQUENCY'
  | 'SHIP'
  | 'DEPTH_LEVEL'
  | 'QUALITY_FLAG'
  | 'TAXONOMIC_LEVEL'
  | 'PLOIDY'
  | 'HARBOUR'
  | 'NUMERICAL_PRECISION'
  | 'PRIVILEGE'
  | 'PRODUCTION_SECTOR'
  | 'BREEDING_STRUCTURE'
  | 'BREEDING_SYSTEM'
  | 'PROJECTION_SYSTEM'
  | 'EVENT_TYPE'
  | 'PRECISION_TYPE'
  | 'DOCUMENT_TYPE'
  | 'TAXON_GROUP_TYPE'
  | 'BREEDING_PHASE_TYPE'
  | 'PHOTO_TYPE'
  | 'POSITIONING_TYPE'
  | 'RESOURCE_TYPE'
  | 'ORDER_ITEM_TYPE'
  | 'DREDGING_AREA_TYPE'
  | 'OBSERVATION_TYPOLOGY'
  | 'UNIT'
  | 'DREDGING_TARGET_AREA'
  | 'PROGRAM'
  | 'STRATEGY'
  | 'CAMPAIGN'
  | 'OCCASION'
  | 'PARAMETER'
  | 'MATRIX'
  | 'FRACTION'
  | 'METHOD';
