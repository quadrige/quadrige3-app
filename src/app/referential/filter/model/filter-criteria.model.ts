import { Entity, EntityClass, ReferentialAsObjectOptions, toBoolean } from '@sumaris-net/ngx-components';
import { FilterCriteriaValue } from '@app/referential/filter/model/filter-criteria-value.model';
import { FilterCriteriaType } from '@app/referential/filter/model/filter-criteria-type';
import { FilterOperatorType } from '@app/referential/filter/model/filter-operator-type';
import { EntityUtils } from '@app/shared/entity.utils';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@EntityClass({ typename: 'FilterCriteriaVO' })
export class FilterCriteria extends Entity<FilterCriteria> {
  static fromObject: (source: any, opts?: any) => FilterCriteria;

  filterCriteriaType: FilterCriteriaType = null;
  filterOperatorType: FilterOperatorType = null;
  inverse: boolean = null;
  systemId: TranscribingSystemType = null;
  values: FilterCriteriaValue[] = [];

  constructor() {
    super(FilterCriteria.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.filterCriteriaType = source.filterCriteriaType;
    this.filterOperatorType = source.filterOperatorType;
    this.inverse = toBoolean(source.inverse, false);
    this.systemId = source.systemId;
    this.values = (source.values || []).map(FilterCriteriaValue.fromObject).sort(EntityUtils.sortComparator('value'));
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.values = this.values?.map((value) => EntityUtils.asMinifiedObject(value, opts));
    target.inverse = toBoolean(this.inverse, false);
    return target;
  }

  equals(other: FilterCriteria): boolean {
    return super.equals(other) || this.filterCriteriaType === other.filterCriteriaType;
  }
}
