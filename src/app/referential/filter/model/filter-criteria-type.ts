export type FilterCriteriaType =
  // Options criteria type
  | 'EXTRACT_IN_SITU_LEVELS' // Do not confuse with EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL and EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL
  | 'EXTRACT_WITH_DATA_UNDER_MORATORIUM'
  | 'EXTRACT_WITH_USER_PERSONAL_DATA'
  // Main criteria types
  | 'EXTRACT_RESULT_META_PROGRAM_ID'
  | 'EXTRACT_RESULT_META_PROGRAM_NAME'
  | 'EXTRACT_RESULT_PROGRAM_ID'
  | 'EXTRACT_RESULT_PROGRAM_NAME'
  | 'EXTRACT_RESULT_MONITORING_LOCATION_ID'
  | 'EXTRACT_RESULT_MONITORING_LOCATION_NAME'
  | 'EXTRACT_RESULT_HARBOUR_ID'
  | 'EXTRACT_RESULT_HARBOUR_NAME'
  | 'EXTRACT_RESULT_DREDGING_TARGET_AREA_ID'
  | 'EXTRACT_RESULT_DREDGING_TARGET_AREA_NAME'
  | 'EXTRACT_RESULT_CAMPAIGN_ID'
  | 'EXTRACT_RESULT_CAMPAIGN_NAME'
  | 'EXTRACT_RESULT_OCCASION_ID'
  | 'EXTRACT_RESULT_OCCASION_NAME'
  | 'EXTRACT_RESULT_EVENT_ID'
  | 'EXTRACT_RESULT_EVENT_NAME'
  | 'EXTRACT_RESULT_BATCH_ID'
  | 'EXTRACT_RESULT_BATCH_NAME'
  | 'EXTRACT_RESULT_INITIAL_POPULATION_ID'
  | 'EXTRACT_RESULT_INITIAL_POPULATION_NAME'
  | 'EXTRACT_RESULT_ORDER_ITEM_TYPE_ID'
  | 'EXTRACT_RESULT_ORDER_ITEM_TYPE_NAME'
  // Survey criteria types
  | 'EXTRACT_RESULT_SURVEY_LABEL'
  | 'EXTRACT_RESULT_SURVEY_COMMENT'
  | 'EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE'
  | 'EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_ID'
  | 'EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_LABEL'
  // | 'EXTRACT_RESULT_SURVEY_STATUS' Deprecated since Mantis #65009
  | 'EXTRACT_RESULT_SURVEY_CONTROL'
  | 'EXTRACT_RESULT_SURVEY_VALIDATION'
  | 'EXTRACT_RESULT_SURVEY_QUALIFICATION'
  | 'EXTRACT_RESULT_SURVEY_GEOMETRY_STATUS'
  | 'EXTRACT_RESULT_SURVEY_UPDATE_DATE'
  | 'EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT'
  | 'EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE'
  // Sampling operation criteria types
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_LABEL'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_COMMENT'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_NAME'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_ID'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_LABEL'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_TIME'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_UT_FORMAT'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH'
  // | 'EXTRACT_RESULT_SAMPLING_OPERATION_STATUS' Deprecated since Mantis #65009
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_CONTROL'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_STATUS'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT'
  | 'EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT_FILE'
  // Sample criteria types
  | 'EXTRACT_RESULT_SAMPLE_MATRIX_ID'
  | 'EXTRACT_RESULT_SAMPLE_MATRIX_NAME'
  | 'EXTRACT_RESULT_SAMPLE_LABEL'
  | 'EXTRACT_RESULT_SAMPLE_COMMENT'
  | 'EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_ID'
  | 'EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_LABEL'
  | 'EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID'
  | 'EXTRACT_RESULT_SAMPLE_TAXON_NAME_CHILDREN'
  | 'EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID'
  | 'EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME'
  | 'EXTRACT_RESULT_SAMPLE_TAXON_GROUP_CHILDREN'
  // | 'EXTRACT_RESULT_SAMPLE_STATUS' Deprecated since Mantis #65009
  | 'EXTRACT_RESULT_SAMPLE_CONTROL'
  | 'EXTRACT_RESULT_SAMPLE_VALIDATION'
  | 'EXTRACT_RESULT_SAMPLE_QUALIFICATION'
  | 'EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT'
  | 'EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT_FILE'
  // Measurement criteria types
  | 'EXTRACT_RESULT_MEASUREMENT_TYPE' // the kinds
  | 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_NAME'
  | 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_CHILDREN'
  | 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_PARAMETER_NAME' // PAR_CD in database
  | 'EXTRACT_RESULT_MEASUREMENT_MATRIX_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_MATRIX_NAME'
  | 'EXTRACT_RESULT_MEASUREMENT_FRACTION_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_FRACTION_NAME'
  | 'EXTRACT_RESULT_MEASUREMENT_METHOD_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_METHOD_NAME'
  | 'EXTRACT_RESULT_MEASUREMENT_UNIT_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_UNIT_NAME'
  | 'EXTRACT_RESULT_MEASUREMENT_PMFMU_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN'
  | 'EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME'
  | 'EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN'
  | 'EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_LABEL'
  | 'EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_LABEL'
  | 'EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_ID'
  | 'EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_NAME'
  | 'EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL'
  // | 'EXTRACT_RESULT_MEASUREMENT_STATUS' Deprecated since Mantis #65009
  | 'EXTRACT_RESULT_MEASUREMENT_CONTROL'
  | 'EXTRACT_RESULT_MEASUREMENT_VALIDATION'
  | 'EXTRACT_RESULT_MEASUREMENT_QUALIFICATION'
  // Photo criteria types
  | 'EXTRACT_RESULT_PHOTO_INCLUDED'
  | 'EXTRACT_RESULT_PHOTO_TYPE_ID'
  | 'EXTRACT_RESULT_PHOTO_TYPE_NAME'
  | 'EXTRACT_RESULT_PHOTO_NAME'
  | 'EXTRACT_RESULT_PHOTO_RESOLUTION_TYPE' // file suffix $LOW',$MID',<none>
  | 'EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL'
  // | 'EXTRACT_RESULT_PHOTO_STATUS' Deprecated since Mantis #65009
  | 'EXTRACT_RESULT_PHOTO_VALIDATION'
  | 'EXTRACT_RESULT_PHOTO_QUALIFICATION'
  // Campaign extraction criteria type
  | 'EXTRACT_CAMPAIGN_PROGRAM_ID'
  | 'EXTRACT_CAMPAIGN_PROGRAM_NAME'
  | 'EXTRACT_CAMPAIGN_USER_ID'
  | 'EXTRACT_CAMPAIGN_USER_NAME'
  | 'EXTRACT_CAMPAIGN_ID'
  | 'EXTRACT_CAMPAIGN_NAME'
  | 'EXTRACT_CAMPAIGN_SISMER_NAME'
  | 'EXTRACT_CAMPAIGN_START_DATE'
  | 'EXTRACT_CAMPAIGN_END_DATE'
  | 'EXTRACT_CAMPAIGN_GEOMETRY_TYPE'
  | 'EXTRACT_CAMPAIGN_SHIP_ID'
  | 'EXTRACT_CAMPAIGN_SHIP_NAME'
  | 'EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_ID'
  | 'EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_NAME'
  // Occasion extraction criteria type
  | 'EXTRACT_OCCASION_ID'
  | 'EXTRACT_OCCASION_NAME'
  | 'EXTRACT_OCCASION_DATE'
  | 'EXTRACT_OCCASION_GEOMETRY_TYPE'
  | 'EXTRACT_OCCASION_SHIP_ID'
  | 'EXTRACT_OCCASION_SHIP_NAME'
  | 'EXTRACT_OCCASION_USER_ID'
  | 'EXTRACT_OCCASION_USER_NAME'
  | 'EXTRACT_OCCASION_RECORDER_DEPARTMENT_ID'
  | 'EXTRACT_OCCASION_RECORDER_DEPARTMENT_NAME'
  // Event extraction criteria type
  | 'EXTRACT_EVENT_TYPE_ID'
  | 'EXTRACT_EVENT_TYPE_NAME'
  | 'EXTRACT_EVENT_RECORDER_DEPARTMENT_ID'
  | 'EXTRACT_EVENT_RECORDER_DEPARTMENT_NAME'
  | 'EXTRACT_EVENT_START_DATE'
  | 'EXTRACT_EVENT_END_DATE'
  | 'EXTRACT_EVENT_GEOMETRY_TYPE'
  | 'EXTRACT_EVENT_DESCRIPTION';
