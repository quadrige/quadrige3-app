import { Entity, EntityClass } from '@sumaris-net/ngx-components';
import { FilterOperatorType } from '@app/referential/filter/model/filter-operator-type';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@EntityClass({ typename: 'FilterCriteriaValueVO' })
export class FilterCriteriaValue extends Entity<FilterCriteriaValue> {
  static fromObject: (source: any, opts?: any) => FilterCriteriaValue;

  value: string = null;

  constructor() {
    super(FilterCriteriaValue.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.value = source.value;
  }

  equals(other: FilterCriteriaValue): boolean {
    return super.equals(other) || this.value === other.value;
  }
}

export interface FilterValue {
  value: any;
  operator?: FilterOperatorType;
  systemId?: TranscribingSystemType;
}

export interface FilterValues {
  values: any[];
  operator?: FilterOperatorType;
  inverse?: boolean;
  systemId?: TranscribingSystemType;
}
