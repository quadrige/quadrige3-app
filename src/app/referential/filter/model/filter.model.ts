import { Referential } from '@app/referential/model/referential.model';
import { EntityClass, ReferentialAsObjectOptions, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { FilterBlock } from '@app/referential/filter/model/filter-block.model';
import { FilterType } from '@app/referential/filter/model/filter-type';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'FilterVO' })
export class Filter extends Referential<Filter> {
  static entityName = 'Filter';
  static fromObject: (source: any, opts?: any) => Filter;

  defaultFilter: boolean = null;
  extraction: boolean = null;

  filterType: FilterType = null;

  userId: number = null;
  departmentId: number = null;

  blocks: FilterBlock[] = [];

  constructor() {
    super(Filter.TYPENAME);
    this.entityName = Filter.entityName;
  }

  fromObject(source: any) {
    // Object.assign(this, source); // Copy all properties
    super.fromObject(source);
    this.entityName = Filter.entityName;
    this.defaultFilter = toBoolean(source.defaultFilter, false);
    this.extraction = toBoolean(source.extraction, false);
    this.filterType = source.filterType;
    this.userId = toNumber(source.userId);
    this.departmentId = toNumber(source.departmentId);
    this.blocks = (source.blocks || []).map(FilterBlock.fromObject);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.blocks = (this.blocks || []).map((value) => EntityUtils.asMinifiedObject(value, opts));
    delete target.entityName;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.statusId;
    return target;
  }

  equals(other: Filter): boolean {
    return super.equals(other) || this.filterType === other?.filterType;
  }
}

@EntityClass({ typename: 'FilterFilterCriteriaVO' })
export class FilterFilterCriteria extends ReferentialFilterCriteria<Filter, number> {
  static fromObject: (source: any, opts?: any) => FilterFilterCriteria;

  userId: number = null;
  filterType: FilterType = null;

  constructor() {
    super(FilterFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.userId = source.userId;
    this.filterType = source.filterType;
  }
}

@EntityClass({ typename: 'FilterFilterVO' })
export class FilterFilter extends ReferentialFilter<FilterFilter, FilterFilterCriteria, Filter> {
  static fromObject: (source: any, opts?: any) => FilterFilter;

  constructor() {
    super(FilterFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): FilterFilterCriteria {
    return FilterFilterCriteria.fromObject(source, opts);
  }
}
