import { Filter } from '@app/referential/filter/model/filter.model';
import { FilterType } from '@app/referential/filter/model/filter-type';
import { Optional } from 'typescript-optional';
import { FilterBlock } from '@app/referential/filter/model/filter-block.model';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { FilterValue, FilterValues } from '@app/referential/filter/model/filter-criteria-value.model';
import { arraySize } from '@sumaris-net/ngx-components';

export class FilterUtils {
  static getOrCreateFilter(filters: Filter[], filterType: FilterType): Filter {
    return Optional.ofNullable(filters.find((value) => value.filterType === filterType)).orElseGet(() => {
      const filter = new Filter();
      filter.filterType = filterType;
      filter.extraction = true;
      filters.push(filter);
      return filter;
    });
  }

  static getOrCreateBlock(blocks: FilterBlock[], index: number): FilterBlock {
    return Optional.ofNullable(blocks[index]).orElseGet(() => {
      const block = new FilterBlock();
      block.rankOrder = index + 1;
      blocks.push(block);
      return block;
    });
  }

  static setBlockRankOrders(blocks: FilterBlock[]) {
    blocks.forEach((block, index) => (block.rankOrder = index + 1));
  }

  static getValues(filterCriteria: FilterCriteria): FilterValues {
    return <FilterValues>{
      values: (filterCriteria?.values || []).map((value) => value.value),
      inverse: filterCriteria?.inverse || false,
      operator: filterCriteria.filterOperatorType,
      systemId: filterCriteria.systemId,
    };
  }

  static getSingleValue(filterCriteria: FilterCriteria): FilterValue {
    if (arraySize(filterCriteria?.values) > 1) {
      console.warn(`[filter-criteria] Multiple values in this criteria, will return first one`, filterCriteria);
    }
    return {
      value: filterCriteria?.values?.[0]?.value,
      operator: filterCriteria?.filterOperatorType,
      systemId: filterCriteria?.systemId,
    };
  }
}
