import { Entity, EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { FilterCriteria } from '@app/referential/filter/model/filter-criteria.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'FilterBlockVO' })
export class FilterBlock extends Entity<FilterBlock> {
  static fromObject: (source: any, opts?: any) => FilterBlock;

  rankOrder: number = null;
  criterias: FilterCriteria[] = [];

  constructor() {
    super(FilterBlock.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.rankOrder = source.rankOrder;
    this.criterias = (source.criterias || []).map(FilterCriteria.fromObject);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.criterias = this.criterias?.map((value) => EntityUtils.asMinifiedObject(value, opts));
    return target;
  }

  equals(other: FilterBlock): boolean {
    return super.equals(other) || this.rankOrder === other.rankOrder;
  }
}
