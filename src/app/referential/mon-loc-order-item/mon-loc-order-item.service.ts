import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { MonLocOrderItem } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { MonLocOrderItemFilter } from '@app/referential/mon-loc-order-item/filter/mon-loc-order-item.filter.model';

@Injectable({ providedIn: 'root' })
export class MonLocOrderItemService extends EntitiesMemoryService<MonLocOrderItem, MonLocOrderItemFilter, string> {
  constructor() {
    super(MonLocOrderItem, MonLocOrderItemFilter);
  }
}
