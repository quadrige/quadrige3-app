import { NgModule } from '@angular/core';
import { MonLocOrderItemTable } from '@app/referential/mon-loc-order-item/mon-loc-order-item.table';
import { MonitoringLocationReportModal } from '@app/referential/mon-loc-order-item/monitoring-location-report.modal';
import { OrderItemReportModal } from '@app/referential/mon-loc-order-item/order-item-report.modal';
import { ReferentialModule } from '@app/referential/referential.module';
import { MonLocOrderItemFilterForm } from '@app/referential/mon-loc-order-item/filter/mon-loc-order-item.filter.form';

const components = [MonLocOrderItemTable, MonitoringLocationReportModal, OrderItemReportModal, MonLocOrderItemFilterForm];

@NgModule({
  imports: [ReferentialModule],
  declarations: components,
  exports: components,
})
export class MonLocOrderItemModule {}
