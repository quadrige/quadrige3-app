import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { MonitoringLocationService } from '@app/referential/monitoring-location/monitoring-location.service';
import { MonitoringLocationReport } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { uniqueValue, Utils } from '@app/shared/utils';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IMonitoringLocationReportModalOptions extends IModalOptions {
  service: MonitoringLocationService;
  monitoringLocationId: number;
}

@Component({
  selector: 'app-monitoring-location-report-modal',
  templateUrl: './monitoring-location-report.modal.html',
  styleUrls: ['./monitoring-location-report.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitoringLocationReportModal extends ModalComponent<void> implements IMonitoringLocationReportModalOptions {
  @Input() service: MonitoringLocationService;
  @Input() monitoringLocationId: number;

  executing = false;
  report: MonitoringLocationReport;
  i18nPrefix = 'REFERENTIAL.MON_LOC_ORDER_ITEM.REPORT.';

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled(): boolean {
    return this.executing;
  }

  protected async afterInit(): Promise<void> {
    await this.executeReport();
  }

  protected dataToValidate(): Promise<void> | void {
    return undefined;
  }

  protected async executeReport() {
    try {
      try {
        this.executing = true;
        this.markForCheck();
        this.report = this.formatReport(await this.service.getReport(this.monitoringLocationId));
      } catch (e) {
        this.error = e?.message || e;
      }
    } finally {
      this.executing = false;
      this.markForCheck();
    }
  }

  protected formatReport(report: MonitoringLocationReport): MonitoringLocationReport {
    // Complete orderItems list each other
    const targetMonLocOrderItems = [];
    const targetExpectedMonLocOrderItems = [];

    const typeIds = (report.monLocOrderItems || []).map((value) => value.orderItem.orderItemTypeId);
    const expectedTypeIds = (report.expectedMonLocOrderItems || []).map((value) => value.orderItem.orderItemTypeId);
    const allTypeIds = Utils.naturalSort(typeIds.concat(expectedTypeIds).filter(uniqueValue));

    for (const item of allTypeIds) {
      targetMonLocOrderItems.push(report.monLocOrderItems[typeIds.indexOf(item)]);
      targetExpectedMonLocOrderItems.push(report.expectedMonLocOrderItems[expectedTypeIds.indexOf(item)]);
    }

    report.monLocOrderItems = targetMonLocOrderItems;
    report.expectedMonLocOrderItems = targetExpectedMonLocOrderItems;
    return report;
  }
}
