import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { uniqueValue, Utils } from '@app/shared/utils';
import { OrderItemReport } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { OrderItemService } from '@app/referential/order-item/order-item.service';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IOrderItemReportModalOptions extends IModalOptions {
  service: OrderItemService;
  orderItemId: number;
}

@Component({
  selector: 'app-order-item-report-modal',
  templateUrl: './order-item-report.modal.html',
  styleUrls: ['./order-item-report.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemReportModal extends ModalComponent<void> implements IOrderItemReportModalOptions {
  @Input() service: OrderItemService;
  @Input() orderItemId: number;

  executing = false;
  report: OrderItemReport;
  i18nPrefix = 'REFERENTIAL.MON_LOC_ORDER_ITEM.REPORT.';

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled(): boolean {
    return this.executing;
  }

  protected afterInit(): Promise<void> | void {
    this.executeReport(); // don't await
  }

  protected async executeReport() {
    try {
      try {
        this.executing = true;
        this.markForCheck();
        this.report = this.formatReport(await this.service.getReport(this.orderItemId));
      } catch (e) {
        this.error = e?.message || e;
      }
    } finally {
      this.executing = false;
      this.markForCheck();
    }
  }

  protected formatReport(report: OrderItemReport): OrderItemReport {
    // Complete orderItems list each other
    const targetMonLocOrderItems = [];
    const targetExpectedMonLocOrderItems = [];

    const labelIds = (report.monLocOrderItems || []).map((value) => value.monitoringLocation.label);
    const expectedLabelIds = (report.expectedMonLocOrderItems || []).map((value) => value.monitoringLocation.label);
    const allLabelIds = Utils.naturalSort(labelIds.concat(expectedLabelIds).filter(uniqueValue));

    for (const label of allLabelIds) {
      targetMonLocOrderItems.push(report.monLocOrderItems[labelIds.indexOf(label)]);
      targetExpectedMonLocOrderItems.push(report.expectedMonLocOrderItems[expectedLabelIds.indexOf(label)]);
    }

    report.monLocOrderItems = targetMonLocOrderItems;
    report.expectedMonLocOrderItems = targetExpectedMonLocOrderItems;
    return report;
  }

  protected dataToValidate(): Promise<void> | void {
    return undefined;
  }
}
