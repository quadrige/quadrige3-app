import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { MonLocOrderItem } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { MonLocOrderItemService } from '@app/referential/mon-loc-order-item/mon-loc-order-item.service';
import { MonLocOrderItemFilter, MonLocOrderItemFilterCriteria } from '@app/referential/mon-loc-order-item/filter/mon-loc-order-item.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';

@Component({
  selector: 'app-mon-loc-order-item-table',
  templateUrl: './mon-loc-order-item.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonLocOrderItemTable
  extends ReferentialMemoryTable<MonLocOrderItem, string, MonLocOrderItemFilter, MonLocOrderItemFilterCriteria>
  implements OnInit, AfterViewInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: MonLocOrderItemService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'orderItemType.id',
        'orderItemType.name',
        'orderItemType.statusId',
        'orderItem.label',
        'orderItem.name',
        'orderItem.id',
        'rankOrder',
        'exception',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      MonLocOrderItem,
      _entityService,
      undefined
    );

    this.i18nColumnPrefix = 'REFERENTIAL.MON_LOC_ORDER_ITEM.';
    this.defaultSortBy = 'orderItemType.id';
    this.logPrefix = '[mon-loc-order-item-table]';
  }

  ngOnInit() {
    this.showTitle = false;
    this.subTable = true;

    super.ngOnInit();

    // Override default options
    this.saveBeforeFilter = false;
    this.saveBeforeSort = false;
    this.saveBeforeDelete = false;
  }

  async setSelected(value: MonLocOrderItem[]) {
    // clear selection
    this.selection.clear();
    await this.resetFilter(undefined, { emitEvent: false });
    await this.setValue(value);
  }
}
