import { OrderItem } from '@app/referential/order-item/order-item.model';
import { OrderItemType } from '@app/referential/order-item-type/order-item-type.model';
import { EntityClass } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';
import { orderItemTypeFragments } from '@app/referential/order-item-type/order-item-type.fragment';
import { orderItemFragments } from '@app/referential/order-item/order-item.fragment';

export type GeometryRelationType = 'INSIDE' | 'OUTSIDE' | 'OVERLAPPED';

@EntityClass({ typename: 'MonLocOrderItemVO' })
export class MonLocOrderItem extends Referential<MonLocOrderItem, string> {
  static entityName = 'MonLocOrderItem';
  static fromObject: (source: any, opts?: any) => MonLocOrderItem;

  monitoringLocationId: number = null;
  monitoringLocation: IntReferential = null;
  orderItem: OrderItem = null;
  orderItemType: OrderItemType = null;
  rankOrder: number = null;
  exception: boolean = null;
  relationType?: GeometryRelationType = null;

  constructor() {
    super(MonLocOrderItem.TYPENAME);
    this.entityName = MonLocOrderItem.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = MonLocOrderItem.entityName;
    this.monitoringLocationId = source.monitoringLocationId;
    this.monitoringLocation = IntReferential.fromObject(source.monitoringLocation);
    this.orderItem = OrderItem.fromObject(source.orderItem);
    this.orderItemType = OrderItemType.fromObject(source.orderItemType);
    this.rankOrder = source.rankOrder;
    this.exception = source.exception;
    this.relationType = source.relationType;
  }
}

export class MonitoringLocationReport {
  label: string;
  expectedLabel: string;
  monLocOrderItems: MonLocOrderItem[];
  expectedMonLocOrderItems: MonLocOrderItem[];

  static fromObject(source: any): MonitoringLocationReport {
    const target = new MonitoringLocationReport();
    target.label = source.label;
    target.expectedLabel = source.expectedLabel;
    target.monLocOrderItems = source.monLocOrderItems?.map(MonLocOrderItem.fromObject) || [];
    target.expectedMonLocOrderItems = source.expectedMonLocOrderItems?.map(MonLocOrderItem.fromObject) || [];
    return target;
  }
}

export class OrderItemReport {
  monLocOrderItems: MonLocOrderItem[];
  expectedMonLocOrderItems: MonLocOrderItem[];

  static fromObject(source: any): OrderItemReport {
    const target = new OrderItemReport();
    target.monLocOrderItems = source.monLocOrderItems?.map(MonLocOrderItem.fromObject) || [];
    target.expectedMonLocOrderItems = source.expectedMonLocOrderItems?.map(MonLocOrderItem.fromObject) || [];
    return target;
  }
}

export const monLocOrderItemFragment = gql`
  fragment MonLocOrderItemFragment on MonLocOrderItemVO {
    monitoringLocationId
    monitoringLocation {
      ...ReferentialFragment
    }
    orderItem {
      ...OrderItemFragment
    }
    orderItemType {
      ...OrderItemTypeFragment
    }
    rankOrder
    exception
    relationType
    updateDate
  }
  ${referentialFragments.light}
  ${orderItemFragments.orderItem}
  ${orderItemTypeFragments.orderItemType}
`;
