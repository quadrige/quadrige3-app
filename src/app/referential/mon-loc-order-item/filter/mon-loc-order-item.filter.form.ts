import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { MonLocOrderItemFilter, MonLocOrderItemFilterCriteria } from './mon-loc-order-item.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-mon-loc-order-item-filter-form',
  templateUrl: './mon-loc-order-item.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonLocOrderItemFilterForm extends ReferentialCriteriaFormComponent<MonLocOrderItemFilter, MonLocOrderItemFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName.map((attribute) => `orderItemType.${attribute}`);
  }
}
