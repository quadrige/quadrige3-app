import { FilterFn, isNotNil } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { MonLocOrderItem } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';

export class MonLocOrderItemFilterCriteria extends ReferentialFilterCriteria<MonLocOrderItem, string> {
  static fromObject(source: any, opts?: any): MonLocOrderItemFilterCriteria {
    const target = new MonLocOrderItemFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  protected buildFilter(): FilterFn<MonLocOrderItem>[] {
    const filterFns: FilterFn<MonLocOrderItem>[] = [];

    // Filter by orderItemType status
    if (isNotNil(this.statusId)) {
      filterFns.push((entity) => this.statusId === entity.orderItemType.statusId);
    }

    const searchTextFilter = EntityUtils.searchTextFilter(['orderItemType.id', 'orderItemType.name'], this.searchText);
    if (searchTextFilter) filterFns.push(searchTextFilter);

    return filterFns;
  }
}

export class MonLocOrderItemFilter extends ReferentialFilter<MonLocOrderItemFilter, MonLocOrderItemFilterCriteria, MonLocOrderItem, string> {
  static fromObject(source: any, opts?: any): MonLocOrderItemFilter {
    const target = new MonLocOrderItemFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): MonLocOrderItemFilterCriteria {
    return MonLocOrderItemFilterCriteria.fromObject(source, opts);
  }
}
