import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'PmfmuVO' })
export class Pmfmu extends Referential<Pmfmu> {
  static entityName = 'Pmfmu';
  static fromObject: (source: any, opts?: any) => Pmfmu;

  detectionThreshold: number = null;
  maximumNumberDecimals: number = null;
  significantFiguresNumber: number = null;
  parameter: Parameter = null;
  matrix: IntReferential = null;
  fraction: IntReferential = null;
  method: IntReferential = null;
  unit: IntReferential = null;
  qualitativeValueIds: number[] = null;

  constructor() {
    super(Pmfmu.TYPENAME);
    this.entityName = Pmfmu.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Pmfmu.entityName;
    this.detectionThreshold = source.detectionThreshold;
    this.maximumNumberDecimals = source.maximumNumberDecimals;
    this.significantFiguresNumber = source.significantFiguresNumber;
    this.parameter = Parameter.fromObject(source.parameter);
    this.matrix = IntReferential.fromObject(source.matrix);
    this.fraction = IntReferential.fromObject(source.fraction);
    this.method = IntReferential.fromObject(source.method);
    this.unit = IntReferential.fromObject(source.unit);
    this.qualitativeValueIds = source.qualitativeValueIds || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parameter = EntityUtils.asMinifiedObject(this.parameter, opts);
    target.matrix = EntityUtils.asMinifiedObject(this.matrix, opts);
    target.fraction = EntityUtils.asMinifiedObject(this.fraction, opts);
    target.method = EntityUtils.asMinifiedObject(this.method, opts);
    target.unit = EntityUtils.asMinifiedObject(this.unit, opts);
    target.qualitativeValueIds = this.qualitativeValueIds || undefined;
    return target;
  }
}

export class PmfmuUtils {
  static equals(p1: Pmfmu, p2: Pmfmu) {
    return p1 === p2 || p1?.id === p2?.id || EntityUtils.deepEquals(p1, p2, ['parameter', 'matrix', 'fraction', 'method', 'unit']);
  }
}
