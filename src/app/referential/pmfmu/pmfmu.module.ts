import { NgModule } from '@angular/core';
import { PmfmuTable } from '@app/referential/pmfmu/pmfmu.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { PmfmuMultiEditModal } from '@app/referential/pmfmu/multi-edit/pmfmu.multi-edit.modal';
import { PmfmuFilterForm } from '@app/referential/pmfmu/filter/pmfmu.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { QualitativeValueSelectTable } from '@app/referential/parameter/qualitative-value/qualitative-value.select.table';

@NgModule({
  imports: [ReferentialModule, PmfmuFilterForm, GenericSelectTable, QualitativeValueSelectTable],
  declarations: [PmfmuTable, PmfmuMultiEditModal],
  exports: [PmfmuTable],
})
export class PmfmuModule {}
