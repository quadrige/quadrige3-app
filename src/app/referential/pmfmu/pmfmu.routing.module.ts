import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { PmfmuTable } from '@app/referential/pmfmu/pmfmu.table';
import { PmfmuModule } from '@app/referential/pmfmu/pmfmu.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PmfmuTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), PmfmuModule],
})
export class PmfmuRoutingModule {}
