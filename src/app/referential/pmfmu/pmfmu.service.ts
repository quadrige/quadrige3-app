import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Pmfmu, PmfmuUtils } from '@app/referential/pmfmu/pmfmu.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { parameterFragments } from '@app/referential/parameter/parameter.service';
import { jobFragments } from '@app/social/job/job.service';
import { PmfmuFilter, PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';

import { transcribingItemWithChildrenFragment } from '@app/referential/transcribing-item/transcribing-item.model';

export const pmfmuFragments = {
  pmfmu: gql`
    fragment PmfmuFragment on PmfmuVO {
      id
      comments
      parameter {
        ...ParameterFragment
      }
      matrix {
        ...ReferentialFragment
      }
      fraction {
        ...ReferentialFragment
      }
      method {
        ...ReferentialFragment
      }
      unit {
        ...ReferentialFragment
      }
      qualitativeValueIds
      detectionThreshold
      maximumNumberDecimals
      significantFiguresNumber
      updateDate
      creationDate
      statusId
      __typename
    }
    ${parameterFragments.parameter}
    ${referentialFragments.light}
  `,
  pmfmuWithoutQualitativeValues: gql`
    fragment PmfmuWithoutQualitativeValuesFragment on PmfmuVO {
      id
      comments
      parameter {
        ...ParameterFragment
      }
      matrix {
        ...ReferentialFragment
      }
      fraction {
        ...ReferentialFragment
      }
      method {
        ...ReferentialFragment
      }
      unit {
        ...ReferentialFragment
      }
      detectionThreshold
      maximumNumberDecimals
      significantFiguresNumber
      updateDate
      creationDate
      statusId
      __typename
    }
    ${parameterFragments.parameter}
    ${referentialFragments.light}
  `,
  lightPmfmu: gql`
    fragment LightPmfmuFragment on PmfmuVO {
      id
      parameter {
        ...ParameterFragment
      }
      matrix {
        ...ReferentialFragment
      }
      fraction {
        ...ReferentialFragment
      }
      method {
        ...ReferentialFragment
      }
      unit {
        ...ReferentialFragment
      }
      statusId
      __typename
    }
    ${parameterFragments.parameter}
    ${referentialFragments.light}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Pmfmus($page: PageInput, $filter: PmfmuFilterVOInput) {
      data: pmfmus(page: $page, filter: $filter) {
        ...PmfmuFragment
      }
    }
    ${pmfmuFragments.pmfmu}
  `,
  loadAllLight: gql`
    query LightPmfmus($page: PageInput, $filter: PmfmuFilterVOInput) {
      data: pmfmus(page: $page, filter: $filter) {
        ...PmfmuWithoutQualitativeValuesFragment
      }
    }
    ${pmfmuFragments.pmfmuWithoutQualitativeValues}
  `,

  loadAllWithTotal: gql`
    query PmfmusWithTotal($page: PageInput, $filter: PmfmuFilterVOInput) {
      data: pmfmus(page: $page, filter: $filter) {
        ...PmfmuFragment
      }
      total: pmfmusCount(filter: $filter)
    }
    ${pmfmuFragments.pmfmu}
  `,
  loadAllLightWithTotal: gql`
    query LightPmfmusWithTotal($page: PageInput, $filter: PmfmuFilterVOInput) {
      data: pmfmus(page: $page, filter: $filter) {
        ...PmfmuWithoutQualitativeValuesFragment
      }
      total: pmfmusCount(filter: $filter)
    }
    ${pmfmuFragments.pmfmuWithoutQualitativeValues}
  `,

  countAll: gql`
    query PmfmusCount($filter: PmfmuFilterVOInput) {
      total: pmfmusCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportPmfmusAsync($filter: PmfmuFilterVOInput, $context: ExportContextInput) {
      data: exportPmfmusAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SavePmfmus($data: [PmfmuVOInput]) {
      data: savePmfmus(pmfmus: $data) {
        ...PmfmuFragment
        transcribingItems {
          ...TranscribingItemWithChildrenFragment
        }
      }
    }
    ${pmfmuFragments.pmfmu}
    ${transcribingItemWithChildrenFragment}
  `,

  deleteAll: gql`
    mutation DeletePmfmus($ids: [Int]) {
      deletePmfmus(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class PmfmuService extends BaseReferentialService<Pmfmu, PmfmuFilter, PmfmuFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Pmfmu, PmfmuFilter, { queries, mutations });
    this._logPrefix = '[pmfmu-service]';
  }

  equals(e1: Pmfmu, e2: Pmfmu): boolean {
    return PmfmuUtils.equals(e1, e2);
  }
}
