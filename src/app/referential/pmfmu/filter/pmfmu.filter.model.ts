import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  IReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export interface IPmfmuFilterCriteria extends IReferentialFilterCriteria<number> {
  parameterFilter?: IReferentialFilterCriteria<string>;
  parameterGroupFilter?: IReferentialFilterCriteria<number>;
  matrixFilter?: IReferentialFilterCriteria<number>;
  fractionFilter?: IReferentialFilterCriteria<number>;
  methodFilter?: IReferentialFilterCriteria<number>;
  unitFilter?: IReferentialFilterCriteria<number>;
  qualitativeValueFilter?: IReferentialFilterCriteria<number>;
  programFilter?: IReferentialFilterCriteria<string>;
  strategyFilter?: IReferentialFilterCriteria<number>;
  qualitative?: boolean;
}

@EntityClass({ typename: 'PmfmuFilterCriteriaVO' })
export class PmfmuFilterCriteria extends ReferentialFilterCriteria<Pmfmu, number> implements IPmfmuFilterCriteria {
  static fromObject: (source: any, opts?: any) => PmfmuFilterCriteria;

  parameterFilter: ParameterFilterCriteria = null;
  parameterGroupFilter: IntReferentialFilterCriteria = null;
  matrixFilter: IntReferentialFilterCriteria = null;
  fractionFilter: IntReferentialFilterCriteria = null;
  methodFilter: IntReferentialFilterCriteria = null;
  unitFilter: IntReferentialFilterCriteria = null;
  qualitativeValueFilter: IntReferentialFilterCriteria = null;
  programFilter: StrReferentialFilterCriteria = null;
  strategyFilter: IntReferentialFilterCriteria = null;
  qualitative: boolean = null;

  constructor() {
    super(PmfmuFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.parameterFilter = ParameterFilterCriteria.fromObject(source.parameterFilter || {});
    this.parameterGroupFilter = IntReferentialFilterCriteria.fromObject(source.parameterGroupFilter || {});
    this.matrixFilter = IntReferentialFilterCriteria.fromObject(source.matrixFilter || {});
    this.fractionFilter = IntReferentialFilterCriteria.fromObject(source.fractionFilter || {});
    this.methodFilter = IntReferentialFilterCriteria.fromObject(source.methodFilter || {});
    this.unitFilter = IntReferentialFilterCriteria.fromObject(source.unitFilter || {});
    this.qualitativeValueFilter = IntReferentialFilterCriteria.fromObject(source.qualitativeValueFilter || {});
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.strategyFilter = IntReferentialFilterCriteria.fromObject(source.strategyFilter || {});
    this.qualitative = source.qualitative;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    if (this.qualitative !== undefined) {
      if (!this.parameterFilter) {
        this.parameterFilter = ParameterFilterCriteria.fromObject({});
      }
      this.parameterFilter.qualitative = this.qualitative;
    }
    delete target.qualitative;
    target.parameterFilter = this.parameterFilter?.asObject(opts);
    target.parameterGroupFilter = this.parameterGroupFilter?.asObject(opts);
    target.matrixFilter = this.matrixFilter?.asObject(opts);
    target.fractionFilter = this.fractionFilter?.asObject(opts);
    target.methodFilter = { ...this.methodFilter?.asObject(opts), searchAttributes: ['id', 'name', 'description'] };
    target.unitFilter = this.unitFilter?.asObject(opts);
    target.qualitativeValueFilter = this.qualitativeValueFilter?.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    target.strategyFilter = { ...this.strategyFilter?.asObject(opts), searchAttributes: ['id', 'name', 'description'] };
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'parameterFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterFilter, true);
    if (key === 'parameterGroupFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterGroupFilter, true);
    if (key === 'matrixFilter') return !BaseFilterUtils.isCriteriaEmpty(this.matrixFilter, true);
    if (key === 'fractionFilter') return !BaseFilterUtils.isCriteriaEmpty(this.fractionFilter, true);
    if (key === 'methodFilter') return !BaseFilterUtils.isCriteriaEmpty(this.methodFilter, true);
    if (key === 'unitFilter') return !BaseFilterUtils.isCriteriaEmpty(this.unitFilter, true);
    if (key === 'qualitativeValueFilter') return !BaseFilterUtils.isCriteriaEmpty(this.qualitativeValueFilter, true);
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'strategyFilter') return !BaseFilterUtils.isCriteriaEmpty(this.strategyFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'PmfmuFilterVO' })
export class PmfmuFilter extends ReferentialFilter<PmfmuFilter, PmfmuFilterCriteria, Pmfmu> {
  static fromObject: (source: any, opts?: any) => PmfmuFilter;

  constructor() {
    super(PmfmuFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): PmfmuFilterCriteria {
    return PmfmuFilterCriteria.fromObject(source, opts);
  }
}
