import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { PmfmuFilter, PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-pmfmu-filter-form',
  templateUrl: './pmfmu.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuFilterForm extends ReferentialCriteriaFormComponent<PmfmuFilter, PmfmuFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.id;
  }

  protected updateSearchAttributes(systemId?: TranscribingSystemType) {
    super.updateSearchAttributes(systemId);

    // Update component criteria search attributes
    const searchAttributes = systemId === 'SANDRE' ? attributes.id : null;
    this.updateCriteriaSearchAttributes('parameterFilter', searchAttributes);
    this.updateCriteriaSearchAttributes('matrixFilter', searchAttributes);
    this.updateCriteriaSearchAttributes('fractionFilter', searchAttributes);
    this.updateCriteriaSearchAttributes('methodFilter', searchAttributes);
    this.updateCriteriaSearchAttributes('unitFilter', searchAttributes);
  }

  protected updateCriteriaSearchAttributes(criteria: keyof PmfmuFilterCriteria, searchAttributes: string[]) {
    this.base().criteriaControls.forEach((criteriaForm) => {
      const control = criteriaForm.controls[criteria as string];
      if (control && control.value.searchAttributes !== searchAttributes) {
        control.patchValue({ searchAttributes }, { emitEvent: false, onlySelf: true });
      }
    });
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof PmfmuFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias
          .filter((criteria) => criteria !== 'searchText')
          .concat(
            'parameterFilter',
            'parameterGroupFilter',
            'matrixFilter',
            'fractionFilter',
            'methodFilter',
            'unitFilter',
            'qualitativeValueFilter',
            'programFilter'
          );
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: PmfmuFilterCriteria): PartialRecord<keyof PmfmuFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'parameterFilter');
    this.subCriteriaToQueryParams(params, criteria, 'parameterGroupFilter');
    this.subCriteriaToQueryParams(params, criteria, 'matrixFilter');
    this.subCriteriaToQueryParams(params, criteria, 'fractionFilter');
    this.subCriteriaToQueryParams(params, criteria, 'methodFilter');
    this.subCriteriaToQueryParams(params, criteria, 'unitFilter');
    this.subCriteriaToQueryParams(params, criteria, 'qualitativeValueFilter');
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'strategyFilter');
    if (isNotNil(criteria.qualitative)) params.qualitative = criteria.qualitative;
    return params;
  }
}
