import { ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import {
  isNotEmptyArray,
  isNotNil,
  ObjectMap,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StatusIds,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { Pmfmu, PmfmuUtils } from '@app/referential/pmfmu/pmfmu.model';
import { PmfmuValidatorService } from '@app/referential/pmfmu/pmfmu.validator';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { filter, map } from 'rxjs/operators';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { Alerts } from '@app/shared/alerts';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { IPmfmuMultiEditModalOptions, PmfmuMultiEditModal } from '@app/referential/pmfmu/multi-edit/pmfmu.multi-edit.modal';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { ParameterService } from '@app/referential/parameter/parameter.service';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { PmfmuFilter, PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ParameterFilter } from '@app/referential/parameter/filter/parameter.filter.model';
import { BaseColumnItem, ExportOptions } from '@app/shared/table/table.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { QualitativeValue } from '@app/referential/parameter/qualitative-value/qualitative-value.model';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { PmfmuComposite } from '@app/referential/pmfmu/multi-edit/pmfmu.multi-edit.model';
import { ComboColumnComponent } from '@app/shared/component/column/combo-column.component';

type PmfmuView = 'qualitativeValueTable' | TranscribingItemView;

@Component({
  selector: 'app-pmfmu-table',
  templateUrl: './pmfmu.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuTable
  extends ReferentialTable<Pmfmu, number, PmfmuFilter, PmfmuFilterCriteria, PmfmuValidatorService, PmfmuView>
  implements OnInit
{
  matrixColumn = viewChild<ComboColumnComponent>('matrixColumn');
  fractionColumn = viewChild<ComboColumnComponent>('fractionColumn');
  qualitativeValueTable = viewChild<GenericSelectTable>('qualitativeValueTable');

  readonly qualitativeValueMenuItem: IEntityMenuItem<Pmfmu, PmfmuView> = {
    title: 'REFERENTIAL.PMFMU.QUALITATIVE_VALUES',
    attribute: 'qualitativeValueIds',
    view: 'qualitativeValueTable',
  };

  constructor(
    protected injector: Injector,
    protected _entityService: PmfmuService,
    protected validatorService: PmfmuValidatorService,
    protected strategyService?: StrategyService,
    protected parameterService?: ParameterService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'parameter',
        'matrix',
        'fraction',
        'method',
        'unit',
        'detectionThreshold',
        'maximumNumberDecimals',
        'significantFiguresNumber',
        'comments',
        'statusId',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      Pmfmu,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.PMFMU.';
    this.defaultSortBy = 'parameter'; // todo should be 'parameter','matrix','fraction','method','unit'
    this.logPrefix = '[pmfmu-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent || (!this.rightPanelVisible && isNotEmptyArray(this.singleSelectedRow?.currentData?.qualitativeValueIds));
  }

  ngOnInit() {
    super.ngOnInit();

    // Parameter combo
    this.registerAutocompleteField('parameter', {
      ...this.referentialOptions.parameter,
      service: this.parameterService,
      filter: ParameterFilter.fromObject({
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Matrix combo
    this.registerAutocompleteField('matrix', {
      ...this.referentialOptions.matrix,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Matrix',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
      showAllOnFocus: true,
    });

    // Fraction combo
    this.registerAutocompleteField('fraction', {
      ...this.referentialOptions.fraction,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Fraction',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
      showAllOnFocus: true,
    });

    // Method combo
    this.registerAutocompleteField('method', {
      ...this.referentialOptions.method,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Method',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Unit combo
    this.registerAutocompleteField('unit', {
      ...this.referentialOptions.unit,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Unit',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Listen parameter attribute change -> update qualitative value enable state
    this.registerSubscription(
      this.registerCellValueChanges('parameter')
        .pipe(
          filter(() => this.singleEditingRow?.id === -1), // only on new row
          filter(isNotNil),
          filter(EntityUtils.isEntity),
          filter(() => !!this.qualitativeValueTable()),
          map((value) => value as unknown as Parameter)
        )
        .subscribe((parameter: Parameter) => {
          // Add all qualitative values
          const qvIds = parameter.qualitativeValues?.filter((qv) => qv.statusId === StatusIds.ENABLE)?.map((qv) => qv.id) || [];
          this.patchRow(this.qualitativeValueMenuItem.attribute, qvIds);
          this.qualitativeValueTable().canEdit = parameter.qualitative;
          this.qualitativeValueTable().addCriteria = { parentId: parameter.id };
          this.qualitativeValueTable().setSelectedIds(qvIds);
        })
    );

    this.registerSubscription(
      this.registerCellValueChanges('matrix').subscribe((matrix) => {
        this.autocompleteFields.fraction.filter.criterias[0].parentId = matrix?.id;
        this.fractionColumn()?.autocompleteField?.reloadItems();
      })
    );

    this.registerSubscription(
      this.registerCellValueChanges('fraction').subscribe((fraction) => {
        this.autocompleteFields.matrix.filter.criterias[0].parentId = fraction?.id;
        this.matrixColumn()?.autocompleteField?.reloadItems();
      })
    );
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<Pmfmu, TranscribingItemView | PmfmuView>[] {
    return [this.qualitativeValueMenuItem, ...super.getRightMenuItems()];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Pmfmu, TranscribingItemView | PmfmuView> {
    return this.qualitativeValueMenuItem;
  }

  isReadOnly(): ReadOnlyIfFn {
    return (row) => !!row.currentData?.id;
  }

  qualitativeValueTableChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow(this.qualitativeValueMenuItem.attribute, values);
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { rowCountWarningThreshold: 50, ...opts });
  }

  async buildExportAdditionalColumns(): Promise<ObjectMap<BaseColumnItem[]>> {
    let previousRightMenuItem = undefined;
    if (this.rightMenuItem !== this.qualitativeValueMenuItem) {
      previousRightMenuItem = this.rightMenuItem;
      this.rightMenuItem = this.qualitativeValueMenuItem;
      await this.loadRightArea();
    }
    const columns = await this.qualitativeValueTable().buildExportColumns();
    if (!!previousRightMenuItem) {
      this.rightMenuItem = previousRightMenuItem;
      await this.loadRightArea();
    }

    const toAppend = this.translate.instant('REFERENTIAL.PARAMETER.QUALITATIVE_VALUE');
    columns.forEach((column) => {
      column.name = toAppend + ' - ' + column.name;
    });

    const result = {};
    result[QualitativeValue.entityName] = columns;
    return result;
  }

  async multiEditRows(event: MouseEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IPmfmuMultiEditModalOptions, PmfmuComposite>(
      PmfmuMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.PMFMU.MULTI_EDIT.TITLE',
        i18nPrefix: this.i18nColumnPrefix,
        selection: this.selection.selected.map((row) => row.currentData),
      },
      'modal-400'
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // Patch selected rows
      this.selection.selected.forEach((row) => {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    }
  }

  protected async loadRightArea(row?: AsyncTableElement<Pmfmu>): Promise<void> {
    if (this.subTable) return; // don't load if sub table
    if (this.rightMenuItem?.view === 'qualitativeValueTable') {
      this.detectChanges();
      if (this.qualitativeValueTable()) {
        this.registerSubForm(this.qualitativeValueTable());
        await this.loadQualitativeValueTable(row);
      }
      return;
    }
    return super.loadRightArea(row);
  }

  protected async loadQualitativeValueTable(row?: AsyncTableElement<Pmfmu>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.qualitativeValueTable().setSelectedIds(undefined);
      this.qualitativeValueTable().canEdit = false;
      return;
    }
    // Load selected qualitative values
    this.qualitativeValueTable().addCriteria = { parentId: row.currentData.parameter?.id };
    await this.qualitativeValueTable().setSelectedIds((row.currentData?.qualitativeValueIds || []).slice());
    this.qualitativeValueTable().canEdit = this.canEdit && row.currentData?.parameter?.qualitative;
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<Pmfmu>[]): Promise<boolean> {
    if (!this.strategyService) {
      throw new Error(`${this.logPrefix} No strategy service found, can't check if PMFMU can be disabled`);
    }

    // Check if used in pmfmu applied strategies
    const res = await this.strategyService.loadPage(
      { offset: 0, size: 100 },
      StrategyFilter.fromObject({
        criterias: [
          {
            programFilter: { statusId: StatusIds.ENABLE },
            onlyActive: true,
            pmfmuFilter: { includedIds: EntityUtils.ids(dirtyRowsWithDisabledStatusId) },
          },
        ],
      })
    );

    if (isNotEmptyArray(res.data)) {
      return Alerts.askConfirmation('REFERENTIAL.PMFMU.CONFIRM.DISABLE_APPLIED_STRATEGIES', this.alertCtrl, this.translate, undefined, {
        list: `<div class="scroll-content"><ul>${res.data
          .map((r) => referentialToString(r, ['programId', 'name']))
          .map((s) => `<li>${s}</li>`)
          .join('')}</ul></div>`,
      });
    }

    return true;
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.PMFMU' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments', 'detectionThreshold', 'maximumNumberDecimals', 'significantFiguresNumber');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('parameter', 'matrix', 'fraction', 'method', 'unit');
  }

  protected equals(t1: Pmfmu, t2: Pmfmu): boolean {
    return PmfmuUtils.equals(t1, t2);
  }
}
