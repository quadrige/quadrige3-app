import { Injectable } from '@angular/core';
import { AbstractControlOptions, AsyncValidatorFn, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, Validators } from '@angular/forms';
import { IEntitiesTableDataSource, SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { lengthComment, ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { Observable, timer } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';

export interface PmfmuValidatorOptions extends ReferentialValidatorOptions {
  multipleEnabled?: boolean;
}

@Injectable({ providedIn: 'root' })
export class PmfmuValidatorService extends ReferentialValidatorService<Pmfmu, number, PmfmuValidatorOptions> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: PmfmuService,
    protected translate: TranslateService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Pmfmu, opts?: PmfmuValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      parameter: [data?.parameter || null, Validators.compose([Validators.required, SharedValidators.entity])],
      matrix: [data?.matrix || null, Validators.compose([Validators.required, SharedValidators.entity])],
      fraction: [data?.fraction || null, Validators.compose([Validators.required, SharedValidators.entity])],
      method: [data?.method || null, Validators.compose([Validators.required, SharedValidators.entity])],
      unit: [data?.unit || null, Validators.compose([Validators.required, SharedValidators.entity])],
      detectionThreshold: [toNumber(data?.detectionThreshold, null), SharedValidators.decimal()],
      maximumNumberDecimals: [toNumber(data?.maximumNumberDecimals, null), SharedValidators.integer],
      significantFiguresNumber: [toNumber(data?.significantFiguresNumber, null), SharedValidators.integer],
      qualitativeValueIds: [data?.qualitativeValueIds || null],
      statusId: [toNumber(data?.statusId, null), opts?.multipleEnabled ? undefined : Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: Pmfmu, opts?: PmfmuValidatorOptions): AbstractControlOptions {
    return {
      asyncValidators: opts?.newData === true ? this.checkPmfmuAlreadyExists(this.dataSource) : undefined,
    };
  }

  checkPmfmuAlreadyExists(dataSource: IEntitiesTableDataSource<any>): AsyncValidatorFn {
    return (group: UntypedFormGroup): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => group),
        mergeMap(async (_group) => {
          const value = _group.value;
          if (_group.dirty && !!value.parameter?.id && !!value.matrix?.id && !!value.fraction?.id && !!value.method?.id && !!value.unit?.id) {
            const error = { pmfmuAlreadyExistsMsg: 'REFERENTIAL.PMFMU.ERROR.ALREADY_EXISTS' };
            // First check in current data source
            if (
              dataSource
                .getRows()
                .filter(
                  (row) =>
                    row.currentData?.parameter.id === value.parameter.id &&
                    row.currentData?.matrix.id === value.matrix.id &&
                    row.currentData?.fraction.id === value.fraction.id &&
                    row.currentData?.method.id === value.method.id &&
                    row.currentData?.unit.id === value.unit.id
                ).length > 1
            ) {
              return error;
            }

            // Next, check via service
            const filterCriteria = PmfmuFilterCriteria.fromObject({
              excludedIds: value.id ? [value.id] : undefined,
              parameterFilter: { includedIds: [value.parameter.id] },
              matrixFilter: { includedIds: [value.matrix.id] },
              fractionFilter: { includedIds: [value.fraction.id] },
              methodFilter: { includedIds: [value.method.id] },
              unitFilter: { includedIds: [value.unit.id] },
            });
            return this.service.exists(filterCriteria).then((exists) => (exists ? error : null));
          } else {
            // pmfmu not fulfilled
            return null;
          }
        })
      );
  }
}
