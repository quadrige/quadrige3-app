import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { CompositeUtils } from '@app/shared/model/composite';
import { PmfmuComposite } from '@app/referential/pmfmu/multi-edit/pmfmu.multi-edit.model';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { StatusById, StatusList } from '@sumaris-net/ngx-components';
import { PmfmuValidatorService } from '@app/referential/pmfmu/pmfmu.validator';

export interface IPmfmuMultiEditModalOptions extends IModalOptions {
  i18nPrefix: string;
  selection: Pmfmu[];
}

@Component({
  selector: 'app-pmfmu-multi-edit-modal',
  templateUrl: './pmfmu.multi-edit.modal.html',
  styleUrl: './pmfmu.multi-edit.modal.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuMultiEditModal extends ModalComponent<PmfmuComposite> implements IPmfmuMultiEditModalOptions {
  @Input() i18nPrefix: string;
  @Input() selection: Pmfmu[];

  statusList = StatusList;
  statusById = StatusById;

  constructor(
    protected injector: Injector,
    protected pmfmuValidator: PmfmuValidatorService
  ) {
    super(injector);
    this.setForm(
      this.formBuilder.group(
        CompositeUtils.getFormGroupConfig(new PmfmuComposite(), pmfmuValidator.getFormGroupConfig(undefined, { multipleEnabled: true }))
      )
    );
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    // Listen for changes
    CompositeUtils.listenCompositeFormChanges(this.form.controls, (subscription) => this.registerSubscription(subscription));
  }

  protected afterInit(): Promise<void> | void {
    this.value = CompositeUtils.build(this.selection, (candidate) => PmfmuComposite.fromPmfmu(candidate));
  }

  protected dataToValidate(): Promise<PmfmuComposite> | PmfmuComposite {
    return CompositeUtils.value(this.value);
  }
}
