import { IComposite } from '@app/shared/model/composite';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';

export class PmfmuComposite implements IComposite {
  statusId: number = null;
  statusIdMultiple = false;
  detectionThreshold: number = null;
  detectionThresholdMultiple = false;
  maximumNumberDecimals: number = null;
  maximumNumberDecimalsMultiple = false;
  significantFiguresNumber: number = null;
  significantFiguresNumberMultiple = false;

  static fromPmfmu(source: Pmfmu): PmfmuComposite {
    const target = new PmfmuComposite();

    target.statusId = source.statusId;
    target.detectionThreshold = source.detectionThreshold;
    target.maximumNumberDecimals = source.maximumNumberDecimals;
    target.significantFiguresNumber = source.significantFiguresNumber;

    return target;
  }
}
