import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { PmfmuTable } from '@app/referential/pmfmu/pmfmu.table';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { PmfmuValidatorService } from '@app/referential/pmfmu/pmfmu.validator';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { IPmfmuFilterCriteria, PmfmuFilter } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { PmfmuFilterForm } from '@app/referential/pmfmu/filter/pmfmu.filter.form';
import { QualitativeValueSelectTable } from '@app/referential/parameter/qualitative-value/qualitative-value.select.table';

export type IPmfmuAddOptions = ISelectModalOptions<IPmfmuFilterCriteria>;

@Component({
  selector: 'app-pmfmu-add-table',
  templateUrl: './pmfmu.table.html',
  standalone: true,
  imports: [ReferentialModule, PmfmuFilterForm, QualitativeValueSelectTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PmfmuAddTable extends PmfmuTable implements IAddTable<Pmfmu> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: PmfmuService,
    protected validatorService: PmfmuValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[pmfmu-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: PmfmuFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }
}
