import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { PositioningSystem } from '@app/referential/positioning-system/positioning-system.model';
import { PositioningSystemService } from '@app/referential/positioning-system/positioning-system.service';
import { PositioningSystemValidatorService } from '@app/referential/positioning-system/positioning-system.validator';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-positioning-system-table',
  templateUrl: './positioning-system.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PositioningSystemTable
  extends ReferentialTable<PositioningSystem, number, IntReferentialFilter, IntReferentialFilterCriteria, PositioningSystemValidatorService>
  implements OnInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: PositioningSystemService,
    protected validatorService: PositioningSystemValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'name',
        'type',
        'horizontalPrecision',
        'verticalPrecision',
        'coordinatePrecision',
        'scale',
        'comments',
        'statusId',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      PositioningSystem,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.POSITIONING_SYSTEM.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[positioning-system-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    // type combo
    this.registerAutocompleteField('type', {
      ...this.referentialOptions.positioningType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'PositioningType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.POSITIONING_SYSTEMS' });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'horizontalPrecision', 'type');
  }
}
