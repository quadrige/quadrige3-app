import { NgModule } from '@angular/core';
import { PositioningSystemTable } from '@app/referential/positioning-system/positioning-system.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { PositioningSystemFilterForm } from '@app/referential/positioning-system/filter/positioning-system.filter.form';

@NgModule({
  imports: [ReferentialModule],
  declarations: [PositioningSystemTable, PositioningSystemFilterForm],
  exports: [PositioningSystemTable, PositioningSystemFilterForm],
})
export class PositioningSystemModule {}
