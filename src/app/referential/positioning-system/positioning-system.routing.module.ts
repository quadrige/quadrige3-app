import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { PositioningSystemTable } from '@app/referential/positioning-system/positioning-system.table';
import { PositioningSystemModule } from '@app/referential/positioning-system/positioning-system.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PositioningSystemTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), PositioningSystemModule],
})
export class PositioningSystemRoutingModule {}
