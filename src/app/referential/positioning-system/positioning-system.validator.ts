import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthDescription,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { PositioningSystem } from '@app/referential/positioning-system/positioning-system.model';
import { PositioningSystemService } from '@app/referential/positioning-system/positioning-system.service';

@Injectable({ providedIn: 'root' })
export class PositioningSystemValidatorService extends ReferentialValidatorService<PositioningSystem> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: PositioningSystemService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: PositioningSystem, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      type: [data?.type || null, Validators.compose([Validators.required, SharedValidators.entity])],
      horizontalPrecision: [data?.horizontalPrecision || null, Validators.compose([Validators.required, Validators.maxLength(lengthDescription)])],
      verticalPrecision: [data?.verticalPrecision || null, Validators.maxLength(lengthDescription)],
      coordinatePrecision: [data?.coordinatePrecision || null, Validators.maxLength(lengthDescription)],
      validationDate: [data?.validationDate || null, SharedValidators.validDate],
      scale: [toNumber(data?.scale, null), SharedValidators.integer],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
