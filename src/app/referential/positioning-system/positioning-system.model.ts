import { EntityClass, fromDateISOString, ReferentialAsObjectOptions, toDateISOString, toNumber } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { Referential, StrReferential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'PositioningSystemVO' })
export class PositioningSystem extends Referential<PositioningSystem> {
  static entityName = 'PositioningSystem';
  static fromObject: (source: any, opts?: any) => PositioningSystem;

  type: StrReferential = null;
  horizontalPrecision: string = null;
  verticalPrecision: string = null;
  coordinatePrecision: string = null;
  scale: number = null;
  validationDate: Moment = null;

  constructor() {
    super(PositioningSystem.TYPENAME);
    this.entityName = PositioningSystem.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = PositioningSystem.entityName;
    this.horizontalPrecision = source.horizontalPrecision;
    this.verticalPrecision = source.verticalPrecision;
    this.coordinatePrecision = source.coordinatePrecision;
    this.scale = toNumber(source.scale);
    this.validationDate = fromDateISOString(source.validationDate);
    this.type = StrReferential.fromObject(source.type);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.type = EntityUtils.asMinifiedObject(this.type, opts);
    target.validationDate = toDateISOString(this.validationDate);
    target.scale = toNumber(this.scale);
    return target;
  }
}
