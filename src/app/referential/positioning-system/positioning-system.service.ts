import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { PositioningSystem } from '@app/referential/positioning-system/positioning-system.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { jobFragments } from '@app/social/job/job.service';

export const positioningSystemFragments = {
  positioningSystem: gql`
    fragment PositioningSystemFragment on PositioningSystemVO {
      id
      name
      comments
      type {
        ...ReferentialFragment
      }
      horizontalPrecision
      verticalPrecision
      coordinatePrecision
      scale
      validationDate
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query PositioningSystems($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: positioningSystems(page: $page, filter: $filter) {
        ...PositioningSystemFragment
      }
    }
    ${positioningSystemFragments.positioningSystem}
    ${referentialFragments.light}
  `,

  loadAllWithTotal: gql`
    query PositioningSystemsWithTotal($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: positioningSystems(page: $page, filter: $filter) {
        ...PositioningSystemFragment
      }
      total: positioningSystemsCount(filter: $filter)
    }
    ${positioningSystemFragments.positioningSystem}
    ${referentialFragments.light}
  `,

  countAll: gql`
    query PositioningSystemsCount($filter: IntReferentialFilterVOInput) {
      total: positioningSystemsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportPositioningSystemsAsync($filter: IntReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportPositioningSystemsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SavePositioningSystems($data: [PositioningSystemVOInput]) {
      data: savePositioningSystems(positioningSystems: $data) {
        ...PositioningSystemFragment
      }
    }
    ${positioningSystemFragments.positioningSystem}
    ${referentialFragments.light}
  `,

  deleteAll: gql`
    mutation DeletePositioningSystems($ids: [Int]) {
      deletePositioningSystems(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class PositioningSystemService extends BaseReferentialService<PositioningSystem, IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, PositioningSystem, IntReferentialFilter, { queries, mutations });
    this._logPrefix = '[positioning-system-service]';
  }
}
