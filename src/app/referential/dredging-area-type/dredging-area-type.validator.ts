import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { DredgingAreaType } from '@app/referential/dredging-area-type/dredging-area-type.model';
import { DredgingAreaTypeService } from '@app/referential/dredging-area-type/dredging-area-type.service';

@Injectable({ providedIn: 'root' })
export class DredgingAreaTypeValidatorService extends ReferentialValidatorService<DredgingAreaType, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: DredgingAreaTypeService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: DredgingAreaType, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
