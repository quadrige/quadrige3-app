import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { DredgingAreaType } from '@app/referential/dredging-area-type/dredging-area-type.model';
import { DredgingAreaTypeValidatorService } from '@app/referential/dredging-area-type/dredging-area-type.validator';
import { DredgingAreaTypeService } from '@app/referential/dredging-area-type/dredging-area-type.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-dredging-area-type-table',
  templateUrl: './dredging-area-type.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DredgingAreaTypeTable extends ReferentialTable<
  DredgingAreaType,
  string,
  StrReferentialFilter,
  StrReferentialFilterCriteria,
  DredgingAreaTypeValidatorService
> {
  constructor(
    protected injector: Injector,
    protected _entityService: DredgingAreaTypeService,
    protected validatorService: DredgingAreaTypeValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      DredgingAreaType,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.DREDGING_AREA_TYPE.';
    this.logPrefix = '[dredging-area-type-table]';
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.DREDGING_AREA_TYPES' });
  }
}
