import { NgModule } from '@angular/core';
import { DredgingAreaTypeTable } from '@app/referential/dredging-area-type/dredging-area-type.table';
import { ReferentialModule } from '@app/referential/referential.module';
import { DredgingAreaTypeFilterForm } from '@app/referential/dredging-area-type/filter/dredging-area-type.filter.form';

@NgModule({
  imports: [ReferentialModule],
  declarations: [DredgingAreaTypeTable, DredgingAreaTypeFilterForm],
  exports: [DredgingAreaTypeTable, DredgingAreaTypeFilterForm],
})
export class DredgingAreaTypeModule {}
