import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'DredgingAreaTypeVO' })
export class DredgingAreaType extends Referential<DredgingAreaType, string> {
  static entityName = 'DredgingAreaType';
  static fromObject: (source: any, opts?: any) => DredgingAreaType;

  constructor() {
    super(DredgingAreaType.TYPENAME);
    this.entityName = DredgingAreaType.entityName;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.entityName = DredgingAreaType.entityName;
  }
}
