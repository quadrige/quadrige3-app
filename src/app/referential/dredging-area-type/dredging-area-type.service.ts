import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { DredgingAreaType } from '@app/referential/dredging-area-type/dredging-area-type.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { jobFragments } from '@app/social/job/job.service';

export const dredgingAreaTypeFragments = {
  dredgingAreaType: gql`
    fragment DredgingAreaTypeFragment on DredgingAreaTypeVO {
      id
      description
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query DredgingAreaTypes($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: dredgingAreaTypes(page: $page, filter: $filter) {
        ...DredgingAreaTypeFragment
      }
    }
    ${dredgingAreaTypeFragments.dredgingAreaType}
  `,

  loadAllWithTotal: gql`
    query DredgingAreaTypesWithTotal($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: dredgingAreaTypes(page: $page, filter: $filter) {
        ...DredgingAreaTypeFragment
      }
      total: dredgingAreaTypesCount(filter: $filter)
    }
    ${dredgingAreaTypeFragments.dredgingAreaType}
  `,

  countAll: gql`
    query DredgingAreaTypesCount($filter: StrReferentialFilterVOInput) {
      total: dredgingAreaTypesCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportDredgingAreaTypesAsync($filter: StrReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportDredgingAreaTypesAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveDredgingAreaTypes($data: [DredgingAreaTypeVOInput]) {
      data: saveDredgingAreaTypes(dredgingAreaTypes: $data) {
        ...DredgingAreaTypeFragment
      }
    }
    ${dredgingAreaTypeFragments.dredgingAreaType}
  `,

  deleteAll: gql`
    mutation DeleteDredgingAreaTypes($ids: [String]) {
      deleteDredgingAreaTypes(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class DredgingAreaTypeService extends BaseReferentialService<DredgingAreaType, StrReferentialFilter, StrReferentialFilterCriteria, string> {
  constructor(protected injector: Injector) {
    super(injector, DredgingAreaType, StrReferentialFilter, { queries, mutations });
    this._logPrefix = '[dredging-area-type-service]';
  }
}
