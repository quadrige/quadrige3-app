import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-dredging-area-type-filter-form',
  templateUrl: './dredging-area-type.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DredgingAreaTypeFilterForm extends ReferentialCriteriaFormComponent<StrReferentialFilter, StrReferentialFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.id;
  }
}
