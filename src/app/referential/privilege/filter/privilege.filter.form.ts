import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-privilege-filter-form',
  templateUrl: './privilege.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivilegeFilterForm extends ReferentialCriteriaFormComponent<StrReferentialFilter, StrReferentialFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }
}
