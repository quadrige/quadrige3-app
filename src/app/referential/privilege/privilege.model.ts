import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'PrivilegeVO' })
export class Privilege extends Referential<Privilege, string> {
  static entityName = 'Privilege';
  static fromObject: (source: any, opts?: any) => Privilege;

  userIds: number[] = null;
  // departmentIds: number[] = null;

  constructor() {
    super(Privilege.TYPENAME);
    this.entityName = Privilege.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Privilege.entityName;
    this.userIds = source.userIds || [];
    // this.departmentIds = source.departmentIds || [];
  }

  asObject(options?: EntityAsObjectOptions): any {
    const target = super.asObject(options);
    target.userIds = this.userIds || undefined;
    // target.departmentIds = this.departmentIds || undefined;
    return target;
  }
}
