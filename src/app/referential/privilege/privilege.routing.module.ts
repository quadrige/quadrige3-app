import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { PrivilegeTable } from '@app/referential/privilege/privilege.table';
import { PrivilegeModule } from '@app/referential/privilege/privilege.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PrivilegeTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), PrivilegeModule],
})
export class PrivilegeRoutingModule {}
