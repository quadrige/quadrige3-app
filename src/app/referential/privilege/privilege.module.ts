import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { PrivilegeTable } from '@app/referential/privilege/privilege.table';
import { PrivilegeFilterForm } from '@app/referential/privilege/filter/privilege.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';

@NgModule({
  imports: [ReferentialModule, GenericSelectTable, UserSelectTable],
  declarations: [PrivilegeTable, PrivilegeFilterForm],
  exports: [PrivilegeTable, PrivilegeFilterForm],
})
export class PrivilegeModule {}
