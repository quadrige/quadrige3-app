import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { Privilege } from '@app/referential/privilege/privilege.model';
import { PrivilegeService } from '@app/referential/privilege/privilege.service';

@Injectable({ providedIn: 'root' })
export class PrivilegeValidatorService extends ReferentialValidatorService<Privilege, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: PrivilegeService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Privilege, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      userIds: [data?.userIds || null],
      // departmentIds: [data?.departmentIds || null],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
