import { ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import { isEmptyArray, isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { Privilege } from '@app/referential/privilege/privilege.model';
import { PrivilegeValidatorService } from '@app/referential/privilege/privilege.validator';
import { PrivilegeService } from '@app/referential/privilege/privilege.service';
import { privilegeEnum } from '@app/referential/model/referential.constants';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { Alerts } from '@app/shared/alerts';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { IAddResult, ISelectModalOptions } from '@app/shared/model/options.model';
import { IRightSelectTable } from '@app/shared/table/table.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';

type PrivilegeView = 'userTable' /*| 'departmentTable'*/;

@Component({
  selector: 'app-privilege-table',
  templateUrl: './privilege.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivilegeTable
  extends ReferentialTable<Privilege, string, StrReferentialFilter, StrReferentialFilterCriteria, PrivilegeValidatorService, PrivilegeView>
  implements OnInit
{
  userTable = viewChild<UserSelectTable>('userTable');
  // @ViewChild('departmentTable') departmentTable: DepartmentSelectTable;

  readonly userMenuItem: IEntityMenuItem<Privilege, PrivilegeView> = {
    title: 'REFERENTIAL.ENTITY.USERS',
    attribute: 'userIds',
    view: 'userTable',
  };
  // readonly departmentMenuItem: IEntityMenuItem<Privilege, PrivilegeView> = {
  //   title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
  //   attribute: 'departmentIds',
  //   view: 'departmentTable',
  // };

  rightTableEnabled = false;

  constructor(
    protected injector: Injector,
    protected _entityService: PrivilegeService,
    protected validatorService: PrivilegeValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'description', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      Privilege,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.PRIVILEGE.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[privilege-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent || (!this.rightPanelVisible && isNotEmptyArray(this.singleSelectedRow?.currentData?.userIds));
  }

  protected async loadRightArea(row?: AsyncTableElement<Privilege>): Promise<void> {
    if (this.subTable) return; // don't load if sub table
    switch (this.rightMenuItem?.view) {
      case 'userTable': {
        this.detectChanges();
        if (this.userTable()) {
          this.registerSubForm(this.userTable());
          await this.loadUserTable(row);
        }
        break;
      }
      // case 'departmentTable': {
      //   this.cd.detectChanges();
      //   this.registerSubForm(this.departmentTable);
      //   await this.loadDepartmentTable(row);
      //   break;
      // }
    }
    await super.loadRightArea(row);
  }

  async userTableChanged(values: number[]) {
    if (this.canEdit) {
      await this.patchUsers(values);
    }
  }

  rightTableSelectionChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow(this.rightMenuItem.attribute, values);
    }
  }

  protected async loadUserTable(row?: AsyncTableElement<Privilege>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.userTable(), row);
      await this.userTable().setSelectedIds(undefined);
      this.rightTableEnabled = false;
      return;
    }

    // Load selected users
    this.applyRightOptions(this.userTable(), row);
    await this.userTable().setSelectedIds((row.currentData?.userIds || []).slice());
    this.rightTableEnabled = this.canEdit;
  }

  protected async addUser(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'User',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.title,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.userTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.userTable());
      await this.userTable().setSelectedIds(includedIds);
      this.rightTableSelectionChanged(includedIds);
    }
  }

  protected async patchUsers(userIds: number[]) {
    const row = this.singleEditingRow;

    if (!row) {
      console.warn(`${this.logPrefix} no row to update`);
      return;
    }

    // Ask confirmation if current row is 'admin' and remove all users
    // eslint-disable-next-line eqeqeq
    if (row.currentData.id == privilegeEnum.admin && isEmptyArray(userIds)) {
      const confirmed = await Alerts.askConfirmation('REFERENTIAL.PRIVILEGE.ERROR.USERS_EMPTY', this.alertCtrl, this.translate);
      if (!confirmed) {
        await this.loadUserTable(row);
        return;
      }
    }

    this.patchRow('userIds', userIds, row);
  }

  // protected async loadDepartmentTable(row?: AsyncTableElement<Privilege>) {
  //   // get current row
  //   row = row || this.singleEditingRow || this.singleSelectedRow;
  //   if (!row) {
  //     if (this.debug) {
  //       console.debug(`${this.logPrefix} no row to load`);
  //     }
  //     this.applyRightOptions(this.departmentTable, row);
  //     await this.departmentTable.setSelectedIds(undefined);
  //     this.rightTableEnabled = false;
  //     return;
  //   }
  //
  //   // Load selected departments
  //   this.applyRightOptions(this.departmentTable, row);
  //   await this.departmentTable.setSelectedIds((row.currentData?.departmentIds || []).slice());
  //   this.rightTableEnabled = this.canEdit;
  // }

  // protected async addDepartment(event: ISelectModalOptions) {
  //   const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
  //     EntityAddModal,
  //     {
  //       entityName: 'Department',
  //       addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
  //       titleAddPrefixI18n: 'COMMON.ADD',
  //       titleI18n: this.rightMenuItem.title,
  //       displayMode: false,
  //     },
  //     'modal-medium'
  //   );
  //
  //   // Add new values
  //   if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
  //     const includedIds = [...(this.departmentTable.selectCriteria.includedIds || []), ...data.addedIds];
  //     this.applyRightOptions(this.departmentTable);
  //     await this.departmentTable.setSelectedIds(includedIds);
  //     this.rightTableSelectionChanged(includedIds);
  //   }
  // }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(true, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<Privilege, PrivilegeView>[] {
    return [this.userMenuItem /*, this.departmentMenuItem*/];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Privilege, PrivilegeView> {
    return this.userMenuItem;
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.PRIVILEGES' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name');
  }

  protected applyRightOptions(table: IRightSelectTable<any>, row?: AsyncTableElement<Privilege>) {
    row = row || this.singleEditingRow;
    table.rightOptions = {
      entityName: Privilege.entityName,
      privilegeId: row?.currentData.id,
    };
  }
}
