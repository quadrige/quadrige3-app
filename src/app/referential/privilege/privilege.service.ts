import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Privilege } from '@app/referential/privilege/privilege.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { StrReferentialFilter, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { jobFragments } from '@app/social/job/job.service';

const privilegeFragment = gql`
  fragment PrivilegeFragment on PrivilegeVO {
    id
    name
    description
    userIds
    comments
    updateDate
    creationDate
    statusId
    __typename
  }
`;

const privilegeLightFragment = gql`
  fragment PrivilegeLightFragment on PrivilegeVO {
    id
    name
    creationDate
    statusId
    __typename
  }
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Privileges($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: privileges(page: $page, filter: $filter) {
        ...PrivilegeFragment
      }
    }
    ${privilegeFragment}
  `,

  loadAllLight: gql`
    query PrivilegesLight($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: privileges(page: $page, filter: $filter) {
        ...PrivilegeLightFragment
      }
    }
    ${privilegeLightFragment}
  `,

  loadAllWithTotal: gql`
    query PrivilegesWithTotal($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: privileges(page: $page, filter: $filter) {
        ...PrivilegeFragment
      }
      total: privilegesCount(filter: $filter)
    }
    ${privilegeFragment}
  `,

  loadAllLightWithTotal: gql`
    query PrivilegesLightWithTotal($page: PageInput, $filter: StrReferentialFilterVOInput) {
      data: privileges(page: $page, filter: $filter) {
        ...PrivilegeLightFragment
      }
      total: privilegesCount(filter: $filter)
    }
    ${privilegeLightFragment}
  `,

  countAll: gql`
    query PrivilegesCount($filter: StrReferentialFilterVOInput) {
      total: privilegesCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportPrivilegesAsync($filter: StrReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportPrivilegesAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SavePrivileges($data: [PrivilegeVOInput]) {
      data: savePrivileges(privileges: $data) {
        ...PrivilegeFragment
      }
    }
    ${privilegeFragment}
  `,

  deleteAll: gql`
    mutation DeletePrivileges($ids: [String]) {
      deletePrivileges(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class PrivilegeService extends BaseReferentialService<Privilege, StrReferentialFilter, StrReferentialFilterCriteria, string> {
  constructor(protected injector: Injector) {
    super(injector, Privilege, StrReferentialFilter, { queries, mutations });
    this._logPrefix = '[privilege-service]';
  }
}
