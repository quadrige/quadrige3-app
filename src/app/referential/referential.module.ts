import { NgModule } from '@angular/core';
import { QuadrigeCoreModule } from '../core/quadrige.core.module';
import { ReferentialHomePage } from '@app/referential/menu/referential-home.page';
import { ReferentialForm } from '@app/referential/form/referential.form';
import { GenericReferentialModule } from '@app/referential/generic/generic.module';
import { BaseFilterFormComponent } from '@app/shared/component/filter/base-filter-form.component';
import { ManagementHomePage } from '@app/referential/menu/management-home.page';
import { SocialComponent } from '@app/social/social.component';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';

const appModules = [QuadrigeCoreModule, GenericReferentialModule, BaseFilterFormComponent];

@NgModule({
  imports: [appModules, SocialComponent, TranscribingItemRowTable, ReferentialFilterFormField],
  declarations: [ReferentialHomePage, ManagementHomePage, ReferentialForm],
  exports: [...appModules, ReferentialForm, TranscribingItemRowTable, ReferentialFilterFormField],
})
export class ReferentialModule {}
