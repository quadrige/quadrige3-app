import { EntityUtils } from '@app/shared/entity.utils';
import { ITranscribingCell, TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { pmfmuSandreAttributes, TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';

export class TranscribingItemRowUtils {
  public static getParentCell(transcribingByAttribute: Record<string, ITranscribingCell>): ITranscribingCell {
    for (const attribute in transcribingByAttribute) {
      if (!transcribingByAttribute[attribute].parentTypeId) {
        return transcribingByAttribute[attribute];
      }
    }
    return undefined;
  }

  public static getChildrenCells(transcribingByAttribute: Record<string, ITranscribingCell>, parentCell: ITranscribingCell): ITranscribingCell[] {
    return Object.values(transcribingByAttribute).filter((cell) => cell.parentTypeId === parentCell.typeId);
  }

  public static sameType(t1: TranscribingItemRow, t2: TranscribingItemRow): boolean {
    return (
      t1 &&
      t2 &&
      EntityUtils.equals(t1.system, t2.system, 'id') &&
      EntityUtils.equals(t1.function, t2.function, 'id') &&
      EntityUtils.equals(t1.language, t2.language, 'id')
    );
  }

  public static equals(t1: TranscribingItemRow, t2: TranscribingItemRow): boolean {
    return (
      EntityUtils.equals(t1, t2, 'id') ||
      (t1 &&
        t2 &&
        this.sameType(t1, t2) &&
        Object.keys(t1.transcribingByAttribute).length === Object.keys(t2.transcribingByAttribute).length &&
        Object.keys(t1.transcribingByAttribute).every(
          (key) => t1.transcribingByAttribute[key].externalCode === t2.transcribingByAttribute[key]?.externalCode
        ))
    );
  }

  public static toExternalCode(value: TranscribingItemRow, entityName: string, systemId: TranscribingSystemType): string {
    if (entityName === Pmfmu.entityName && systemId === 'SANDRE') {
      // For Pmfmu Sandre, this is a composition of each attribute
      return pmfmuSandreAttributes.map((attribute) => value.transcribingByAttribute[attribute]?.externalCode).join('|');
    }
    // Default case
    return value.transcribingByAttribute[value.mainAttribute].externalCode;
  }
}
