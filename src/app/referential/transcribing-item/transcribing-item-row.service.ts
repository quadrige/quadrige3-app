import { inject, Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { ITranscribingColumn, TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { TranscribingItemRowFilter } from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.model';
import { TranscribingItemRowUtils } from '@app/referential/transcribing-item/transcribing-item-row.utils';
import {
  ILoadTranscribingItemOptions,
  ILoadTranscribingItemRowOptions,
  TranscribingItemTypeService,
} from '@app/referential/transcribing-item-type/transcribing-item-type.service';
import { uniqueValue, Utils } from '@app/shared/utils';
import { isEmptyArray, isNotNil, isNotNilOrBlank, ObjectMap } from '@sumaris-net/ngx-components';
import { ITranscribingItemTableOptions, TranscribingItemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { StrReferential } from '@app/referential/model/referential.model';
import { addToMultiMap, ObjectMultiMap } from '@app/shared/multimap';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';
import { TranscribingItem } from '@app/referential/transcribing-item/transcribing-item.model';

@Injectable()
export class TranscribingItemRowService extends EntitiesMemoryService<TranscribingItemRow, TranscribingItemRowFilter> {
  private readonly transcribingItemTypeService = inject(TranscribingItemTypeService);

  options: ITranscribingItemTableOptions;
  transcribingItemTypes: TranscribingItemType[] = [];
  transcribingItemTypesById: Record<number, TranscribingItemType> = {};
  availableSystems: StrReferential[] = [];
  availableFunctions: StrReferential[] = [];
  availableLanguages: StrReferential[] = [];
  availableCodificationTypes: StrReferential[] = [];
  transcribingColumns: ITranscribingColumn[] = [];

  constructor() {
    super(TranscribingItemRow, TranscribingItemRowFilter);
  }

  asFilter(source: Partial<TranscribingItemRowFilter>): TranscribingItemRowFilter {
    const rowFilter = super.asFilter(source);
    const hiddenTranscribingAttributes = this.hiddenTranscribingAttributes;
    rowFilter.patch({ searchAttributes: this.transcribingColumnAttributes.filter((value) => !hiddenTranscribingAttributes.includes(value)) });
    return rowFilter;
  }

  equals(d1: TranscribingItemRow, d2: TranscribingItemRow): boolean {
    return TranscribingItemRowUtils.equals(d1, d2);
  }

  get transcribingColumnAttributes(): string[] {
    return this.transcribingColumns.map((column) => column.attribute);
  }

  get hiddenTranscribingAttributes() {
    return this.transcribingItemTypes.filter((type) => type.hidden).map((type) => type.targetAttribute);
  }

  async loadTranscribingElements(entityName: string) {
    this.transcribingItemTypes = (await this.loadTranscribingItemTypes(entityName)).filter((type) => this.byTypeFilter(type));
    this.transcribingItemTypesById = Utils.toRecord(this.transcribingItemTypes, 'id');
    // Get available sub entities
    const systems = await this.transcribingItemTypeService.getSystems();
    this.availableSystems = this.transcribingItemTypes
      .map((type) => type.systemId)
      .filter(isNotNil)
      .filter(uniqueValue)
      .map((systemId) => systems.find((ref) => ref.id === systemId));
    const functions = await this.transcribingItemTypeService.getFunctions();
    this.availableFunctions = this.transcribingItemTypes
      .map((type) => type.functionId)
      .filter(isNotNil)
      .filter(uniqueValue)
      .map((functionId) => functions.find((ref) => ref.id === functionId));
    const languages = await this.transcribingItemTypeService.getLanguages();
    this.availableLanguages = this.transcribingItemTypes
      .map((type) => type.languageId)
      .filter(isNotNil)
      .filter(uniqueValue)
      .map((languageId) => languages.find((ref) => ref.id === languageId));
    this.availableCodificationTypes = await this.transcribingItemTypeService.getCodificationTypes();

    const labelsByAttribute: ObjectMultiMap<string> = {};

    // Gather all attributes and labels hierarchical
    this.transcribingItemTypes
      .filter((type) => !type.parentId)
      .forEach((parentType) => {
        addToMultiMap(labelsByAttribute, parentType.targetAttribute, parentType.name);
        const childrenTypeIds = TranscribingItemTypeUtils.getChildrenTranscribingTypeIds(this.transcribingItemTypes, parentType.id);
        childrenTypeIds.forEach((childTypeId) => {
          const childType = this.transcribingItemTypesById[childTypeId];
          addToMultiMap(labelsByAttribute, childType.targetAttribute, childType.name, true);
        });
      });

    // Determine label by column (keep only shared start)
    const labelByAttribute: ObjectMap<string> = {};
    Object.keys(labelsByAttribute).forEach((attribute) => {
      const labels = labelsByAttribute[attribute];
      labelByAttribute[attribute] = Utils.sharedStart(labels);
    });
    // Find shared start over all columns (better visibility)
    const sharedStartLabel = Object.keys(labelByAttribute).length > 1 ? Utils.sharedStart(Object.values(labelByAttribute)) : undefined;
    // Then clean (remove separators also)
    Object.keys(labelByAttribute).forEach((attribute) => {
      if (isNotNilOrBlank(sharedStartLabel)) {
        labelByAttribute[attribute] = labelByAttribute[attribute].replace(sharedStartLabel, '');
      }
      labelByAttribute[attribute] = labelByAttribute[attribute].replace(/[\s-:]+$/g, '').trim();
    });

    // Create columns
    this.transcribingColumns = [];
    Object.keys(labelByAttribute).forEach((attribute) => {
      this.transcribingColumns.push({
        attribute,
        label: labelByAttribute[attribute],
      });
    });

    // Update service
    this.transcribingColumns.forEach((column) => {
      this.addSortByReplacement(column.attribute, `transcribingByAttribute.${column.attribute}.externalCode`);
    });
  }

  byTypeFilter = (type: TranscribingItemType) =>
    type &&
    (!this.options?.systemId || type.systemId === this.options.systemId) &&
    (isEmptyArray(this.options?.functionIds) || this.options.functionIds.includes(type.functionId));

  async loadTranscribingItemTypes(entityName: string): Promise<TranscribingItemType[]> {
    return this.transcribingItemTypeService.loadTranscribingItemTypesByEntityName(entityName, this.options?.forceEntityName);
  }

  async loadTranscribingItemsTree(opts: ILoadTranscribingItemOptions): Promise<TranscribingItem[]> {
    return this.transcribingItemTypeService.loadTranscribingItemsTree(opts);
  }

  async loadTranscribingItemRows(opts: ILoadTranscribingItemRowOptions): Promise<TranscribingItemRow[]> {
    return this.transcribingItemTypeService.loadTranscribingItemRows(opts);
  }
}
