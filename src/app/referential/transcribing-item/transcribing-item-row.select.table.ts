import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { TranscribingItemRowService } from '@app/referential/transcribing-item/transcribing-item-row.service';
import { TranscribingItemRowValidator } from '@app/referential/transcribing-item/transcribing-item-row.validator';
import { ISelectTable } from '@app/shared/table/table.model';
import { GenericService } from '@app/referential/generic/generic.service';
import {
  TranscribingItemRowFilter,
  TranscribingItemRowFilterCriteria,
} from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.model';
import { TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { TranscribingItemRowFilterForm } from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.form';
import { AttributeInputTypePipe } from '@app/referential/transcribing-item/attribute-input-type.pipe';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { isEmptyArray, isNilOrBlank, isNotEmptyArray } from '@sumaris-net/ngx-components';
import { ITranscribingItemTableOptions } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { uniqueValue } from '@app/shared/utils';
import { TranscribingItemRowUtils } from '@app/referential/transcribing-item/transcribing-item-row.utils';

@Component({
  selector: 'app-transcribing-item-row-select-table',
  templateUrl: './transcribing-item-row.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TranscribingItemRowValidator, useExisting: false },
    { provide: TranscribingItemRowService, useExisting: false },
  ],
  standalone: true,
  imports: [QuadrigeCoreModule, TranscribingItemRowFilterForm, AttributeInputTypePipe],
})
export class TranscribingItemRowSelectTable extends TranscribingItemRowTable implements ISelectTable<TranscribingItemRow> {
  @Input() titlePrefixI18n: string;
  @Input() selectCriteria: ISelectCriteria = {};
  @Output() openAddEntities = new EventEmitter<ISelectModalOptions>();
  @Output() selectionChanged = new EventEmitter<any[]>();

  get canAdd(): boolean {
    return true;
  }

  get canMultiEdit(): boolean {
    return false;
  }

  get canDuplicate(): boolean {
    return false;
  }

  constructor(
    protected injector: Injector,
    protected _entityService: TranscribingItemRowService,
    protected genericService: GenericService,
    protected validatorService: TranscribingItemRowValidator
  ) {
    super(injector, _entityService, genericService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[transcribing-item-select-table]';
    this.selectTable = true;
  }

  protected generateTableId(): string {
    if (isNilOrBlank(this.options?.referentialEntityName)) return super.generateTableId();
    return `${super.generateTableId()}_${this.options.referentialEntityName}`;
  }

  protected async applyOptions(options: ITranscribingItemTableOptions): Promise<void> {
    await super.applyOptions({ ...options, forceEntityName: false });
  }

  async resetFilter(filter?: TranscribingItemRowFilter, opts?: ISetFilterOptions<TranscribingItemRowFilterCriteria>) {
    await this.applyOptions({
      systemId: this.selectCriteria.systemId,
      functionIds: this.selectCriteria.functionIds,
      referentialEntityName: this.selectCriteria.referentialEntityName,
    });
    await super.resetFilter(filter, {
      ...opts,
      staticCriteria: {
        ...this.selectCriteria, // Always use default filter which include selected ids
        forceIncludedIds: isEmptyArray(this.selectCriteria.includedIds),
      },
    });

    // Load transcribing items by selected entity ids
    if (isNotEmptyArray(this.selectCriteria.includedIds)) {
      const rows = await this._entityService.loadTranscribingItemRows({
        entityName: this.targetEntityName,
        includedExternalCodes: this.selectCriteria.includedIds,
        systemId: this.options.systemId,
        functionIds: this.options.functionIds,
        fetchPolicy: 'cache-first',
      });
      await this.setValue(rows);
    } else {
      await this.setValue(undefined);
    }
  }

  // Override the default addRow method to filter only unselected referential
  async addRow(event?: any): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    this.openAddEntities.emit();
    return false;
  }

  // Override the default deleteSelection method to remove only from includedIds
  async deleteSelection(event: UIEvent): Promise<number> {
    event.stopPropagation();
    const toDelete = this.selection.selected;
    if (isEmptyArray(toDelete)) {
      return; // nothing to delete
    }

    // Ask delete confirmation
    const canDelete = await this.canDeleteRows(toDelete);
    if (!canDelete) return; // Cannot delete

    // Remove from selection
    const removedIds = toDelete
      .map((value) => value.currentData)
      .map((value) => TranscribingItemRowUtils.toExternalCode(value, this.targetEntityName, this.options.systemId))
      .filter(uniqueValue);
    this.selectCriteria.includedIds = this.selectCriteria.includedIds.filter((id) => !removedIds.includes(id.toString()));
    await this.resetFilter();
  }

  updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
