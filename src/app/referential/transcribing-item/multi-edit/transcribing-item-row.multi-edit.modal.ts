import { ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { CompositeUtils } from '@app/shared/model/composite';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';
import { ITranscribingColumn, ITranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import {
  ITranscribingItemRowComposite,
  TranscribingItemRowComposite,
} from '@app/referential/transcribing-item/multi-edit/transcribing-item-row.multi-edit.model';
import { isEmptyArray, MatAutocompleteFieldConfig, TextForm } from '@sumaris-net/ngx-components';
import { StrReferential } from '@app/referential/model/referential.model';
import { autocompleteWidthMedium } from '@app/shared/constants';
import { TranscribingItemRowValidator } from '@app/referential/transcribing-item/transcribing-item-row.validator';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { AttributeInputTypePipe } from '@app/referential/transcribing-item/attribute-input-type.pipe';

export interface ITranscribingItemRowMultiEditOptions extends IModalOptions {
  i18nPrefix: string;
  selection: ITranscribingItemRow[];
  codificationTypeConfig: MatAutocompleteFieldConfig;
  transcribingColumns: ITranscribingColumn[];
  hiddenTranscribingAttributes: string[];
  system: StrReferential;
}

@Component({
  selector: 'app-transcribing-item-multi-edit-modal',
  templateUrl: './transcribing-item-row.multi-edit.modal.html',
  styleUrl: './transcribing-item-row.multi-edit.modal.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TranscribingItemRowValidator],
  standalone: true,
  imports: [QuadrigeCoreModule, AttributeInputTypePipe],
})
export class TranscribingItemRowMultiEditModal extends ModalComponent<ITranscribingItemRowComposite> implements ITranscribingItemRowMultiEditOptions {
  @Input() i18nPrefix: string;
  @Input() selection: ITranscribingItemRow[];
  @Input() codificationTypeConfig: MatAutocompleteFieldConfig;
  @Input() transcribingColumns: ITranscribingColumn[] = [];
  @Input() hiddenTranscribingAttributes: string[] = [];
  @Input() system: StrReferential;

  @ViewChild(TextForm) commentsTextForm: TextForm;

  panelWidth = autocompleteWidthMedium;
  protected propertyKeysAsMap: string[] = ['transcribingByAttribute'];

  constructor(
    protected injector: Injector,
    protected validator: TranscribingItemRowValidator
  ) {
    super(injector);
    // Get TranscribingItemComposite form config
    this.validator.transcribingColumns = this.transcribingColumns;
    this.validator.hiddenTranscribingAttributes = this.hiddenTranscribingAttributes;
    const formConfig = CompositeUtils.getFormGroupConfig(
      new TranscribingItemRowComposite(),
      this.validator.getFormGroupConfig(undefined, { multipleEnabled: true })
    );
    this.setForm(this.formBuilder.group(formConfig));
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    this.commentsTextForm.enable(opts);
    // Listen for changes
    CompositeUtils.listenCompositeFormChanges(this.form.controls, (subscription) => this.registerSubscription(subscription), this.propertyKeysAsMap);
  }

  protected afterInit() {
    if (isEmptyArray(this.selection)) {
      console.warn('[transcribing-item-multi-edit-modal] No selection');
      return;
    }
    this.value = CompositeUtils.build(
      this.selection,
      (candidate) =>
        TranscribingItemRowComposite.fromTranscribingItem(
          candidate,
          this.transcribingColumns?.map((value) => value.attribute)
        ),
      this.propertyKeysAsMap
    );
  }

  protected dataToValidate(): ITranscribingItemRowComposite {
    return CompositeUtils.value(this.value, this.propertyKeysAsMap);
  }
}
