import { CompositeMap, IComposite } from '@app/shared/model/composite';
import { ITranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { StrReferential } from '@app/referential/model/referential.model';
import { isNotEmptyArray } from '@sumaris-net/ngx-components';

class AttributeComposite implements IComposite {
  externalCode: string = null;
  externalCodeMultiple = false;
}

export interface ITranscribingItemRowComposite extends IComposite {
  codificationType: StrReferential;
  comments: string;
  transcribingByAttribute: CompositeMap<AttributeComposite>;
}

export class TranscribingItemRowComposite implements ITranscribingItemRowComposite {
  codificationType: StrReferential = null;
  codificationTypeMultiple = false;
  comments: string = null;
  commentsMultiple = false;
  transcribingByAttribute: CompositeMap<AttributeComposite> = new CompositeMap<AttributeComposite>();

  static fromTranscribingItem(source: ITranscribingItemRow, attributes: string[]): TranscribingItemRowComposite {
    const target = new TranscribingItemRowComposite();

    target.codificationType = source.codificationType;
    target.comments = source.comments;

    if (isNotEmptyArray(attributes)) {
      attributes.forEach((attribute) => {
        const composite = new AttributeComposite();
        composite.externalCode = source.transcribingByAttribute[attribute].externalCode;
        target.transcribingByAttribute[attribute] = composite;
      });
    }

    return target;
  }
}
