import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import {
  TranscribingItemRowFilter,
  TranscribingItemRowFilterCriteria,
} from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.model';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';
import { StrReferential } from '@app/referential/model/referential.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { BaseFilterFormComponent } from '@app/shared/component/filter/base-filter-form.component';

@Component({
  selector: 'app-transcribing-item-row-filter-form',
  templateUrl: './transcribing-item-row.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule, BaseFilterFormComponent],
})
export class TranscribingItemRowFilterForm extends ReferentialCriteriaFormComponent<TranscribingItemRowFilter, TranscribingItemRowFilterCriteria> {
  constructor() {
    super();
  }

  get transcribingItemRowTable(): TranscribingItemRowTable {
    return this.table() as TranscribingItemRowTable;
  }

  get availableFunctions(): StrReferential[] {
    return this.transcribingItemRowTable.availableFunctions;
  }

  get availableLanguages(): StrReferential[] {
    return this.transcribingItemRowTable.availableLanguages;
  }

  get availableCodificationTypes(): StrReferential[] {
    return this.transcribingItemRowTable.availableCodificationTypes;
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }
}
