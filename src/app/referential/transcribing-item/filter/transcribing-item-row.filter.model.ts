import { EntityAsObjectOptions, EntityClass, FilterFn, isNotNilOrBlank } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { BaseEntityFilter } from '@app/shared/model/filter.model';
import { TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';

@EntityClass({ typename: 'TranscribingItemRowFilterCriteriaVO' })
export class TranscribingItemRowFilterCriteria extends ReferentialFilterCriteria<TranscribingItemRow, number> {
  static fromObject: (source: any, opts?: any) => TranscribingItemRowFilterCriteria;

  functionId: string = null;
  languageId: string = null;
  codificationTypeId: string = null;
  comments: string = null;

  constructor() {
    super(TranscribingItemRowFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.functionId = source.functionId;
    this.languageId = source.languageId;
    this.codificationTypeId = source.codificationTypeId;
    this.comments = source.comments;
  }

  protected buildFilter(): FilterFn<TranscribingItemRow>[] {
    const target: FilterFn<TranscribingItemRow>[] = []; // don't call super.buildFilter();

    // Filter by text search (=externalCode on each attribute)
    const searchTextFilter = EntityUtils.searchTextFilter(
      this.searchAttributes?.map((attribute) => `transcribingByAttribute.${attribute}.externalCode`),
      this.searchText
    );
    if (searchTextFilter) target.push(searchTextFilter);
    // Filter by comments
    const commentsFilter = EntityUtils.searchTextFilter(['comments'], this.comments);
    if (commentsFilter) target.push(commentsFilter);

    // Filter by function
    if (isNotNilOrBlank(this.functionId)) {
      target.push((data) => this.functionId === data.function?.id);
    }
    // Filter by language
    if (isNotNilOrBlank(this.languageId)) {
      target.push((data) => this.languageId === data.language?.id);
    }
    // Filter by codificationType
    if (isNotNilOrBlank(this.codificationTypeId)) {
      target.push((data) => this.codificationTypeId === data.codificationType?.id);
    }

    return target;
  }
}

@EntityClass({ typename: 'TranscribingItemRowFilterVO' })
export class TranscribingItemRowFilter extends BaseEntityFilter<TranscribingItemRowFilter, TranscribingItemRowFilterCriteria, TranscribingItemRow> {
  static fromObject: (source: any, opts?: any) => TranscribingItemRowFilter;

  constructor() {
    super(TranscribingItemRowFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  criteriaFromObject(source: any, opts: any): TranscribingItemRowFilterCriteria {
    return TranscribingItemRowFilterCriteria.fromObject(source, opts);
  }
}
