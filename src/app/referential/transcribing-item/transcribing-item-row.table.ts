import { ChangeDetectionStrategy, Component, Injector, input, OnInit } from '@angular/core';
import {
  arraySize,
  isEmptyArray,
  isNil,
  isNotEmptyArray,
  isNotNil,
  KeyType,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  suggestFromArray,
} from '@sumaris-net/ngx-components';
import {
  ITranscribingItemTableOptions,
  TranscribingItemType,
  TranscribingSystemType,
} from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { TranscribingItemRowService } from '@app/referential/transcribing-item/transcribing-item-row.service';
import { BaseMemoryTable } from '@app/shared/table/base.memory.table';
import { TranscribingItemRowValidator } from '@app/referential/transcribing-item/transcribing-item-row.validator';
import { isNotEmptyMultiMap, ObjectMultiMap, splitToMultiMap } from '@app/shared/multimap';
import { uniqueValue, Utils } from '@app/shared/utils';
import { IBaseTable } from '@app/shared/table/table.model';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { EntityType, IWithTranscribingItems, Referential, StrReferential } from '@app/referential/model/referential.model';
import { GenericService } from '@app/referential/generic/generic.service';
import {
  TranscribingItemRowFilter,
  TranscribingItemRowFilterCriteria,
} from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.model';
import { merge, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Optional } from 'typescript-optional';
import { EntityUtils } from '@app/shared/entity.utils';
import {
  ITranscribingItemRowMultiEditOptions,
  TranscribingItemRowMultiEditModal,
} from '@app/referential/transcribing-item/multi-edit/transcribing-item-row.multi-edit.modal';
import { autocompleteWidthMedium } from '@app/shared/constants';
import { ITranscribingColumn, TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { TranscribingItem } from '@app/referential/transcribing-item/transcribing-item.model';
import { ITranscribingItemRowComposite } from '@app/referential/transcribing-item/multi-edit/transcribing-item-row.multi-edit.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { TranscribingItemRowFilterForm } from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.form';
import { AttributeInputTypePipe } from '@app/referential/transcribing-item/attribute-input-type.pipe';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';
import { TranscribingItemRowUtils } from '@app/referential/transcribing-item/transcribing-item-row.utils';
import { ISetFilterOptions } from '@app/shared/model/options.model';

const START_COLUMNS = RESERVED_START_COLUMNS.concat(['system', 'function', 'language', 'entityId']);
const END_COLUMNS = ['codificationType', 'comments', 'creationDate', 'updateDate'].concat(RESERVED_END_COLUMNS);

export type ReferentialRow = AsyncTableElement<Referential<any, KeyType>>;

@Component({
  selector: 'app-transcribing-item-row-table',
  templateUrl: './transcribing-item-row.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TranscribingItemRowValidator, TranscribingItemRowService],
  standalone: true,
  imports: [QuadrigeCoreModule, TranscribingItemRowFilterForm, AttributeInputTypePipe],
})
export class TranscribingItemRowTable
  extends BaseMemoryTable<TranscribingItemRow, number, TranscribingItemRowFilter, TranscribingItemRowFilterCriteria, TranscribingItemRowValidator>
  implements OnInit
{
  referentialTable = input.required<IBaseTable<IWithTranscribingItems, any>>();

  get targetEntityName(): string {
    return this.options.referentialEntityName ?? this.referentialTable().entityName;
  }

  get options(): ITranscribingItemTableOptions {
    return this._entityService.options;
  }

  protected targetEntityType: EntityType;
  protected referentialRows: ReferentialRow[];
  protected nbInitialSelection: number;
  protected comboPanelWidth = autocompleteWidthMedium;

  private _hiddenItemsByEntityId: ObjectMultiMap<TranscribingItem> = {};
  private _rowDirtySubscription: Subscription;

  constructor(
    protected injector: Injector,
    protected _entityService: TranscribingItemRowService,
    protected genericService: GenericService,
    protected validatorService: TranscribingItemRowValidator
  ) {
    super(
      injector,
      // columns
      START_COLUMNS.concat(END_COLUMNS),
      TranscribingItemRow,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.TRANSCRIBING_ITEM.';
    this.defaultSortBy = 'function';
    this.logPrefix = '[transcribing-item-table]';
  }

  get canAdd(): boolean {
    return arraySize(this.referentialRows) === this.nbInitialSelection && this.nbInitialSelection === 1;
  }

  get canMultiEdit(): boolean {
    const systems: StrReferential[] = this.selection.selected.map((value) => value.currentData.system).filter(uniqueValue) || [];
    return arraySize(systems) === 1;
  }

  get canDuplicate(): boolean {
    return arraySize(this.availableSystems) === 1 && this.availableSystems[0].id === <TranscribingSystemType>'SANDRE';
  }

  get transcribingItemTypes(): TranscribingItemType[] {
    return this._entityService.transcribingItemTypes;
  }

  get transcribingItemTypesById(): Record<number, TranscribingItemType> {
    return this._entityService.transcribingItemTypesById;
  }

  get transcribingColumns(): ITranscribingColumn[] {
    return this._entityService.transcribingColumns;
  }

  get availableSystems(): StrReferential[] {
    return this._entityService.availableSystems;
  }

  get availableFunctions(): StrReferential[] {
    return this._entityService.availableFunctions;
  }

  get availableLanguages(): StrReferential[] {
    return this._entityService.availableLanguages;
  }

  get availableCodificationTypes(): StrReferential[] {
    return this._entityService.availableCodificationTypes;
  }

  get hiddenTranscribingAttributes(): string[] {
    return this._entityService.hiddenTranscribingAttributes;
  }

  get transcribingColumnAttributes(): string[] {
    return this._entityService.transcribingColumnAttributes;
  }

  ngOnInit() {
    this.subTable = true;
    this.showPaginator = true;
    super.ngOnInit();

    this.registerAutocompleteField('system', {
      ...this.referentialOptions.transcribingSystem,
      suggestFn: (value, options) => this.suggestFromArray(this.availableSystems, value, options),
      showAllOnFocus: true,
    });
    this.registerAutocompleteField('function', {
      ...this.referentialOptions.transcribingFunction,
      suggestFn: (value, options) => this.suggestFromArray(this.availableFunctions, value, options),
      showAllOnFocus: true,
    });
    this.registerAutocompleteField('language', {
      ...this.referentialOptions.transcribingLanguage,
      suggestFn: (value, options) => this.suggestFromArray(this.availableLanguages, value, options),
      showAllOnFocus: true,
    });
    this.registerAutocompleteField('codificationType', {
      ...this.referentialOptions.codificationType,
      suggestFn: (value, options) => this.suggestFromArray(this.availableCodificationTypes, value, options),
      showAllOnFocus: true,
    });

    // Add listener on row to setup the cells
    this.registerSubscription(
      merge(
        this.registerCellValueChanges('system').pipe(filter(isNotNil), filter(EntityUtils.isEntity)),
        this.registerCellValueChanges('function').pipe(filter(isNotNil), filter(EntityUtils.isEntity)),
        this.registerCellValueChanges('language').pipe(filter((value) => isNil(value) || EntityUtils.isEntity(value)))
      )
        .pipe(filter(() => !!this.singleEditingRow && this.loaded))
        .subscribe(() => {
          // Affect type ids on each cell on current editing row
          const data = this.singleEditingRow.currentData;
          const types = this.getTranscribingItemTypes(data.system?.id, data.function?.id, data.language?.id);
          Object.keys(data.transcribingByAttribute).forEach((attribute) => {
            const typeFound = types.find((type) => type.targetAttribute === attribute);
            this.singleEditingRow.validator
              .get(['transcribingByAttribute', attribute])
              .patchValue({ typeId: typeFound?.id, parentTypeId: typeFound?.parentId });
          });
        })
    );

    // Add listener on deleted rows
    this.registerSubscription(
      this.onAfterDeletedRows.subscribe((rows) => {
        rows
          .filter((row) => !!row.currentData.entityId && row.id !== -1)
          .map((row) => row.currentData.entityId)
          .filter(uniqueValue)
          .forEach((entityId) => this.markReferentialRowAsDirty(entityId));
      })
    );
  }

  protected async suggestFromArray(array: any[], value: any, options: any): Promise<any[]> {
    if (!value || value === '*') {
      return array;
    } else {
      return suggestFromArray(array, value, { searchAttributes: options.searchAttributes || [options.searchAttribute], anySearch: true }).data;
    }
  }

  async setSelection(referentialRows: ReferentialRow[], options: ITranscribingItemTableOptions) {
    this.markAsLoading();
    await this.referentialTable().ready(); // Wait for entityName to be set (GenericTable)

    // Init columns
    if (!Utils.deepEquals(this.options, options) || this.entityNameChanged()) {
      await this.applyOptions(options);
    }

    if (isEmptyArray(referentialRows)) {
      this.referentialRows = [];
      this.nbInitialSelection = 0;
      await super.setValue(undefined);
      return;
    }
    this.nbInitialSelection = referentialRows.length;

    // Load items
    for (const row of referentialRows) {
      await this.loadTranscribingItems(row);
    }

    // Determine multiple selection
    if (referentialRows.length > 1) {
      // Filter saved referential rows
      this.referentialRows = referentialRows.filter(
        (referentialRow: ReferentialRow) =>
          (!!referentialRow.currentData.id && typeof referentialRow.currentData.id === 'number') || !!referentialRow.currentData.creationDate
      );
    } else {
      // Unique selection (even new referential)
      this.referentialRows = referentialRows;
    }
    this.setShowColumn('entityId', referentialRows.length > 1);

    await this.resetFilter(undefined, { emitEvent: false });
    await super.setValue(this.convertItemsToRows());
  }

  async resetFilter(f?: TranscribingItemRowFilter, opts?: ISetFilterOptions<TranscribingItemRowFilterCriteria>): Promise<void> {
    // Update filter form
    this.detectChanges();
    this.filterFormComponent().setSearchFieldsText(
      this.transcribingColumns
        .filter((value) => !this.hiddenTranscribingAttributes.includes(value.attribute))
        .map((value) => value.label)
        .join(', ')
    );

    // Patch filter static criteria
    if (opts?.staticCriteria) {
      if (isNil(opts.staticCriteria.functionId) && arraySize(opts.staticCriteria.functionIds) === 1) {
        opts.staticCriteria.functionId = opts.staticCriteria.functionIds[0];
      }
    }

    await super.resetFilter(f, opts);
  }

  protected async applyOptions(options: ITranscribingItemTableOptions) {
    this._entityService.options = options;
    this.updateSettingsId();
    await this.initTranscribingColumns();
    this.applySettings({ useSortColumnFromSettings: true });
    if (this.showTitle) {
      this.updateTitle();
    }
  }

  protected async defaultNewRowValue(): Promise<any> {
    const data = await super.defaultNewRowValue();
    if (arraySize(this.referentialRows) === 1) {
      data.entityId = this.referentialRows[0].currentData.id?.toString();
      if (arraySize(this.availableSystems) === 1) {
        data.system = this.availableSystems[0];
      }
      if (arraySize(this.availableFunctions) === 1) {
        data.function = this.availableFunctions[0];
      }
      if (arraySize(this.availableLanguages) === 1) {
        data.language = this.availableLanguages[0];
      }
    }
    return data;
  }

  protected onStartEditRow(row: AsyncTableElement<TranscribingItemRow>) {
    super.onStartEditRow(row);
    this.unregisterSubscription(this._rowDirtySubscription);
    this._rowDirtySubscription = row.validator.valueChanges
      .pipe(filter(() => row.validator.dirty && this.loaded))
      .subscribe(() => this.markReferentialRowAsDirty(row.currentData.entityId));
    this.registerSubscription(this._rowDirtySubscription);
  }

  markRowAsDirty(row?: AsyncTableElement<TranscribingItemRow>, opts?: { onlySelf?: boolean; emitEVent?: boolean }) {
    super.markRowAsDirty(row, opts);
    row = row || this.singleEditingRow;
    if (row?.currentData?.entityId) {
      this.markReferentialRowAsDirty(row.currentData.entityId);
    }
  }

  protected async patchDuplicateAdditionalProperties(source: TranscribingItemRow, target: any, opts?: any): Promise<void> {
    Object.keys(target.transcribingByAttribute).forEach((attribute) => {
      target.transcribingByAttribute[attribute] = {
        ...target.transcribingByAttribute[attribute],
        id: undefined,
        updateDate: undefined,
      };
    });
  }

  protected async initTranscribingColumns() {
    await this._entityService.loadTranscribingElements(this.targetEntityName);

    // Initialize column visibility
    this.setShowColumn('system', arraySize(this.availableSystems) > 1, { emitEvent: false });
    this.setShowColumn('function', isNotEmptyArray(this.availableFunctions) || isNotEmptyArray(this.options.functionIds), { emitEvent: false });
    this.setShowColumn('language', isNotEmptyArray(this.availableLanguages), { emitEvent: false });
    this.setShowColumn('entityId', false, { emitEvent: false });

    // Update validator
    this.validatorService.transcribingColumns = this.transcribingColumns;
    this.validatorService.hiddenTranscribingAttributes = this.hiddenTranscribingAttributes;

    // Get target entity type
    this.targetEntityType = this.genericService.getType(this.targetEntityName);

    this.updateColumns();
  }

  translateFormPath(path: string): string {
    const matcher = path.match(/transcribingByAttribute\.(.*)\.externalCode/);
    if (arraySize(matcher) > 1) {
      path = matcher[1];
    }
    return super.translateFormPath(path);
  }

  private convertItemsToRows(): TranscribingItemRow[] {
    // Convert items to rows
    const rows: TranscribingItemRow[] = [];

    // Filter by system and function, keep rejected items into a hidden list which is added when calling getItems
    const allItems: TranscribingItem[] = this.referentialRows?.flatMap((row) => row.currentData.transcribingItems).filter(isNotNil);
    const [itemsToConvert, itemsToHide] = Utils.splitArray(allItems, (item) => this.byTypeIdFilter(item.transcribingItemTypeId));
    this._hiddenItemsByEntityId = splitToMultiMap(itemsToHide, (item) => item.entityId);

    // Group items by entityId
    const map = splitToMultiMap(itemsToConvert, (item) => item.entityId);
    Object.keys(map).forEach((entityId) => {
      map[entityId].forEach((parentItem) => {
        const row = new TranscribingItemRow();
        const parentType = this.transcribingItemTypesById[parentItem.transcribingItemTypeId];
        row.id = parentItem.id;
        row.entityId = entityId === 'undefined' || entityId === 'null' ? undefined : entityId; // entityId is 'undefined' string due to mapping
        row.system = this.availableSystems.find((systemRef) => systemRef.id === parentType.systemId);
        row.function = this.availableFunctions.find((functionRef) => functionRef.id === parentType.functionId);
        row.language = this.availableLanguages.find((languageRef) => languageRef.id === parentType.languageId);
        row.codificationType = this.availableCodificationTypes.find((codificationType) => codificationType.id === parentItem.codificationTypeId);
        row.comments = parentItem.comments;
        row.creationDate = parentItem.creationDate;
        row.updateDate = parentItem.updateDate;
        row.mainAttribute = parentItem.mainItemType ? parentType.targetAttribute : undefined;
        row.transcribingByAttribute[parentType.targetAttribute] = {
          id: parentItem.id,
          typeId: parentItem.transcribingItemTypeId,
          externalCode: parentItem.externalCode,
          updateDate: parentItem.updateDate,
        };
        TranscribingItemTypeUtils.getChildrenTranscribingTypeIds(this.transcribingItemTypes, parentItem.transcribingItemTypeId).forEach(
          (childTypeId) => {
            const childItem = parentItem.children.find((c) => c.transcribingItemTypeId === childTypeId);
            row.transcribingByAttribute[this.transcribingItemTypesById[childTypeId].targetAttribute] = {
              id: childItem?.id,
              typeId: childTypeId,
              externalCode: childItem?.externalCode || '',
              parentTypeId: parentItem.transcribingItemTypeId,
              updateDate: childItem?.updateDate,
            };
          }
        );
        rows.push(row);
      });
    });

    return rows;
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    const saved = await super.save(opts);
    if (saved) {
      this.updateReferentialRows();
    }
    return saved;
  }

  private updateReferentialRows() {
    this.referentialRows
      .filter((row) => row.dirty)
      .forEach((referentialRow) => {
        const entityId: string | undefined = referentialRow.currentData.id?.toString();
        // Get rows related to this referential
        const rows = this.value.filter((itemRow) => Utils.equals(itemRow.entityId, entityId));

        if (isNotEmptyArray(this.value) && isEmptyArray(rows)) {
          console.warn(`${this.logPrefix} No row found ! Make sure entityId corresponds to selected referential row`);
        }

        // Convert rows to items for this referential
        const items: TranscribingItem[] = [];
        rows.forEach((row: TranscribingItemRow) => {
          const parentItem = new TranscribingItem();
          parentItem.id = row.id;
          parentItem.entityId = row.entityId;
          parentItem.codificationTypeId = row.codificationType?.id;
          parentItem.comments = row.comments;
          parentItem.creationDate = row.creationDate;
          parentItem.updateDate = row.updateDate;
          const parentCell = TranscribingItemRowUtils.getParentCell(row.transcribingByAttribute);
          parentItem.transcribingItemTypeId = parentCell.typeId;
          parentItem.externalCode = parentCell.externalCode;
          items.push(parentItem);
          // Children
          TranscribingItemRowUtils.getChildrenCells(row.transcribingByAttribute, parentCell).forEach((childCell) => {
            const childItem = new TranscribingItem();
            childItem.id = childCell.id;
            childItem.entityId = row.entityId;
            childItem.codificationTypeId = row.codificationType?.id;
            childItem.comments = row.comments;
            childItem.creationDate = row.creationDate;
            childItem.updateDate = childCell.updateDate;
            childItem.transcribingItemTypeId = childCell.typeId;
            childItem.externalCode = childCell.externalCode;
            parentItem.children.push(childItem);
          });
        });

        // Restore hidden items
        if (isNotEmptyMultiMap(this._hiddenItemsByEntityId) && isNotEmptyArray(this._hiddenItemsByEntityId[entityId]))
          items.push(...this._hiddenItemsByEntityId[entityId]);

        // Copy to referential row
        this.referentialTable().patchRow('transcribingItems', items, referentialRow);
      });
  }

  async multiEditRows(event: UIEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }
    const selection = this.selection.selected.map((row) => row.currentData);

    // Assert only one system selected
    const systems = selection.map((value) => value.system).filter(uniqueValue);
    if (systems.length !== 1) {
      console.error(`${this.logPrefix} Impossible to multi-edit on different systems`, systems);
      return;
    }
    const system = systems[0];

    // Add transcribing columns only for Pmfmu
    const orderedTranscribingColumns: ITranscribingColumn[] = [];
    let cssClass: string;
    if (this.targetEntityName === 'Pmfmu') {
      this.getDisplayColumns().forEach((column) => {
        Optional.ofNullable(this.transcribingColumns.find((value) => value.attribute === column)).ifPresent((transcribingColumn) =>
          orderedTranscribingColumns.push(transcribingColumn)
        );
      });
      cssClass = 'modal-full-height';
    }

    const { data, role } = await this.modalService.openModal<ITranscribingItemRowMultiEditOptions, ITranscribingItemRowComposite>(
      TranscribingItemRowMultiEditModal,
      {
        titleI18n: this.i18nColumnPrefix + 'MULTI_EDIT.TITLE',
        i18nPrefix: this.i18nColumnPrefix,
        selection,
        codificationTypeConfig: this.autocompleteFields.codificationType,
        transcribingColumns: orderedTranscribingColumns,
        hiddenTranscribingAttributes: this.hiddenTranscribingAttributes,
        system,
      },
      cssClass
    );

    if (this.debug) {
      console.debug(`${this.logPrefix} multi-edit: returned data`, data);
    }

    if (role === 'validate' && data) {
      const entityIds: string[] = [];
      // Patch selected rows
      for (const row of this.selection.selected) {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
        await this.confirmEditCreate(event, row);
        entityIds.push(row.currentData.entityId);
      }
      entityIds
        .filter(isNotNil)
        .filter(uniqueValue)
        .forEach((entityId) => this.markReferentialRowAsDirty(entityId));
      this.markForCheck();
    }
  }

  // /**@deprecated*/
  // async setValue(value: TranscribingItemRow[], opts?: { emitEvent?: boolean }) {
  //   throw new Error(`${this.logPrefix} Don't call setValue(), use setItems instead`);
  // }

  // Protected methods

  protected markReferentialRowAsDirty(entityId: string) {
    this.referentialTable().markRowAsDirty(this.getReferentialRow(entityId));
  }

  protected generateTableId(): string {
    return (
      super.generateTableId() +
      Optional.ofNullable(this.options?.systemId)
        .map((value) => `_${value}`)
        .orElse('') +
      Optional.ofNullable(this.options?.functionIds)
        .map((values) => '_' + values.map((value) => `+${value}`))
        .orElse('')
    );
  }

  protected getFilterSettingId(): string {
    return super.getFilterSettingId() + (this.targetEntityName ? `_${this.targetEntityName}` : '');
  }

  protected getDisplayColumns(): string[] {
    if (isEmptyArray(this.transcribingColumns)) {
      return super.getDisplayColumns();
    }

    const defaultColumns = START_COLUMNS.concat(this.transcribingColumnAttributes).concat(END_COLUMNS);
    let columns: string[];
    const hiddenColumns: string[] = this.hiddenTranscribingAttributes.slice();

    if (isEmptyArray(this.getUserColumns())) {
      // Default disposition
      columns = defaultColumns;
      hiddenColumns.push(...this.getDefaultHiddenColumns());
    } else {
      // Show mandatory columns
      const mandatoryColumns = [];
      if (this.getShowColumn('system')) {
        mandatoryColumns.push('system');
      }
      if (this.getShowColumn('function')) {
        mandatoryColumns.push('function');
      }
      if (this.getShowColumn('language')) {
        mandatoryColumns.push('language');
      }
      if (this.getShowColumn('entityId')) {
        mandatoryColumns.push('entityId');
      }
      // Add transcribing columns as mandatory
      const userColumns = this.getUserColumns().filter((column) => defaultColumns.includes(column) && !mandatoryColumns.includes(column));
      const missingColumns = this.transcribingColumnAttributes.filter((attribute) => !userColumns.includes(attribute));
      // Adapt with user specific disposition
      columns = RESERVED_START_COLUMNS.concat(...mandatoryColumns)
        .concat(...userColumns)
        .concat(...missingColumns)
        .concat(RESERVED_END_COLUMNS);
    }

    // fix: remove excluded columns
    columns = columns.filter((column) => !this.excludesColumns.includes(column)).filter((column) => !hiddenColumns.includes(column));

    return columns;
  }

  getI18nColumnName(columnName: string): string {
    if (columnName === 'entityId') {
      return `${this.i18nColumnPrefix}${this.targetEntityType.idIsString ? 'ENTITY_CODE' : 'ENTITY_ID'}`;
    }
    const transcribingColumn = this.transcribingColumns.find((column) => column.attribute === columnName);
    if (transcribingColumn) {
      return transcribingColumn.label;
    }
    return super.getI18nColumnName(columnName);
  }

  protected getReferentialRow(entityId: KeyType): ReferentialRow {
    if (!entityId && arraySize(this.referentialRows) === 1) {
      return this.referentialRows[0];
    }
    const referentialRow = this.referentialRows.find((row) => row.currentData.id.toString() === entityId.toString());
    if (!referentialRow) {
      console.error(`${this.logPrefix} Referential row not found, please check 'entityId'`);
    }
    return referentialRow;
  }

  protected getTranscribingItemTypes(systemId: string, functionId: string, languageId: string): TranscribingItemType[] {
    return this.transcribingItemTypes.filter(
      (type) => Utils.equals(systemId, type.systemId) && Utils.equals(functionId, type.functionId) && Utils.equals(languageId, type.languageId)
    );
  }

  protected async loadTranscribingItems(row: ReferentialRow) {
    // If new table element, init transcribing to null
    if (isNil(row.currentData?.id)) {
      row.validator.patchValue({ transcribingItemsLoaded: true });
      return;
    }
    // Call service if transcribing items are not loaded yet
    if (!row.currentData.transcribingItemsLoaded) {
      // Check entity name
      if (row.currentData.entityName !== this.targetEntityName)
        throw new Error(
          `${this.logPrefix} Incoherent entityName ! Should be ${this.targetEntityName} but found ${row.currentData.entityName} from row`
        );

      // Load transcribing items tree
      const transcribingItems = await this._entityService.loadTranscribingItemsTree({
        entityName: this.targetEntityName,
        entityId: row.currentData.id.toString(),
      });

      // Patch the referential row
      row.validator.patchValue({ transcribingItems, transcribingItemsLoaded: true });
    }
  }

  protected byTypeIdFilter = (typeId: number) => this._entityService.byTypeFilter(this.transcribingItemTypesById[typeId]);

  updateTitle() {
    if (!this.showTitle || !this.targetEntityName) return;
    const entityTitle: string = this.translate.instant(Utils.toI18nKey(this.targetEntityName));
    this.title = this.translate.instant(this.i18nColumnPrefix + 'TITLE', { entityName: entityTitle.toLowerCase() });
  }

  protected getRequiredColumns(): string[] {
    const requiredColumns = super.getRequiredColumns();
    if (this.getShowColumn('system')) {
      requiredColumns.push('system');
    }
    if (this.getShowColumn('function')) {
      requiredColumns.push('function');
    }
    if (this.getShowColumn('language')) {
      requiredColumns.push('language');
    }
    if (this.getShowColumn('entityId')) {
      requiredColumns.push('entityId');
    }
    requiredColumns.push(...this.transcribingColumnAttributes);
    return requiredColumns;
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns();
  }

  private entityNameChanged() {
    return !this.transcribingItemTypes?.map((type) => type.targetEntityName).includes(this.targetEntityName);
  }
}
