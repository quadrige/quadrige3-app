import { BaseReferential, EntityClass, IReferentialRef, isNotNilOrBlank, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IWithTranscribingItems } from '@app/referential/model/referential.model';
import { Moment } from 'moment/moment';
import gql from 'graphql-tag';

export const transcribingItemFragment = gql`
  fragment TranscribingItemFragment on TranscribingItemVO {
    id
    transcribingItemTypeId
    mainItemType
    codificationTypeId
    parentId
    entityId
    externalCode
    comments
    creationDate
    updateDate
    statusId
    __typename
  }
`;
export const transcribingItemWithChildrenFragment = gql`
  fragment TranscribingItemWithChildrenFragment on TranscribingItemVO {
    ...TranscribingItemFragment
    children {
      ...TranscribingItemFragment
    }
  }
  ${transcribingItemFragment}
`;

export interface ITranscribingItem extends IReferentialRef {
  transcribingItemTypeId: number;
  mainItemType: boolean;
  codificationTypeId: string;
  entityId: string;
  externalCode: string;
  parentId: number;
  children: ITranscribingItem[];
  creationDate: Date | Moment;
}

/**
 * Transcribing item MUST extend BaseReferential, NOT Referential
 */
@EntityClass({ typename: 'TranscribingItemVO' })
export class TranscribingItem extends BaseReferential<TranscribingItem> implements ITranscribingItem {
  static entityName = 'TranscribingItem';
  static fromObject: (source: any, opts?: any) => TranscribingItem;

  transcribingItemTypeId: number = null;
  mainItemType = false;
  codificationTypeId: string = null;
  entityId: string = null;
  externalCode: string = null;
  children: TranscribingItem[] = [];

  constructor() {
    super(TranscribingItem.TYPENAME);
    this.entityName = TranscribingItem.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = TranscribingItem.entityName;
    this.transcribingItemTypeId = source.transcribingItemTypeId;
    this.mainItemType = source.mainItemType;
    this.codificationTypeId = source.codificationTypeId;
    this.entityId = source.entityId;
    this.externalCode = source.externalCode;
    this.children = source.children?.map((child: any) => TranscribingItem.fromObject(child)) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.children = this.children.map((child) => child.asObject(opts));
    delete target.entityName;
    delete target.transcribingItems;
    delete target.transcribingItemsLoaded;
    return target;
  }
}

export class TranscribingItemUtils {
  static hasValidTranscribingItems(value: IWithTranscribingItems) {
    return value?.transcribingItems?.some((transcribingItem: { externalCode: any }) => isNotNilOrBlank(transcribingItem.externalCode)) || false;
  }
}
