import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { TranscribingItemRowService } from '@app/referential/transcribing-item/transcribing-item-row.service';
import { TranscribingItemRowValidator } from '@app/referential/transcribing-item/transcribing-item-row.validator';
import { IAddTable } from '@app/shared/table/table.model';
import { GenericService } from '@app/referential/generic/generic.service';
import {
  TranscribingItemRowFilter,
  TranscribingItemRowFilterCriteria,
} from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.model';
import { TranscribingItemRow } from '@app/referential/transcribing-item/transcribing-item-row.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { TranscribingItemRowFilterForm } from '@app/referential/transcribing-item/filter/transcribing-item-row.filter.form';
import { AttributeInputTypePipe } from '@app/referential/transcribing-item/attribute-input-type.pipe';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { uniqueValue } from '@app/shared/utils';
import { ITranscribingItemTableOptions } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { isNilOrBlank } from '@sumaris-net/ngx-components';
import { TranscribingItemRowUtils } from '@app/referential/transcribing-item/transcribing-item-row.utils';

@Component({
  selector: 'app-transcribing-item-row-add-table',
  templateUrl: './transcribing-item-row.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TranscribingItemRowValidator, useExisting: false },
    { provide: TranscribingItemRowService, useExisting: false },
  ],
  standalone: true,
  imports: [QuadrigeCoreModule, TranscribingItemRowFilterForm, AttributeInputTypePipe],
})
export class TranscribingItemRowAddTable extends TranscribingItemRowTable implements IAddTable<TranscribingItemRow> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  get selectedEntityIds(): any[] {
    // Get externalCode from the main transcribing
    return super.selectedEntities
      .map((value) => TranscribingItemRowUtils.toExternalCode(value, this.targetEntityName, this.options.systemId))
      .filter(uniqueValue);
  }

  constructor(
    protected injector: Injector,
    protected _entityService: TranscribingItemRowService,
    protected genericService: GenericService,
    protected validatorService: TranscribingItemRowValidator
  ) {
    super(injector, _entityService, genericService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[transcribing-item-add-table]';
    this.addTable = true;
  }

  protected generateTableId(): string {
    if (isNilOrBlank(this.options?.referentialEntityName)) return super.generateTableId();
    return `${super.generateTableId()}_${this.options.referentialEntityName}`;
  }

  protected async applyOptions(options: ITranscribingItemTableOptions): Promise<void> {
    await super.applyOptions({ ...options, forceEntityName: false });
  }

  async resetFilter(filter?: TranscribingItemRowFilter, opts?: ISetFilterOptions<TranscribingItemRowFilterCriteria>) {
    await this.applyOptions({
      systemId: this.addCriteria.systemId,
      functionIds: this.addCriteria.functionIds,
      referentialEntityName: this.addCriteria.referentialEntityName,
    });
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;

    const rows = await this._entityService.loadTranscribingItemRows({
      entityName: this.targetEntityName,
      excludedExternalCodes: this.addCriteria.excludedIds,
      systemId: this.options.systemId,
      functionIds: this.options.functionIds,
      fetchPolicy: 'cache-first',
    });
    await this.setValue(rows);
  }

  updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
