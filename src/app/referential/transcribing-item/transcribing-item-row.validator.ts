import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { lengthComment, ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import {
  ITranscribingCell,
  ITranscribingColumn,
  ITranscribingItemRow,
  TranscribingItemRow,
} from '@app/referential/transcribing-item/transcribing-item-row.model';
import { PartialRecord } from '@app/shared/model/interface';
import { IEntitiesTableDataSource, SharedFormGroupValidators, SharedValidators } from '@sumaris-net/ngx-components';
import { TranscribingItemRowUtils } from '@app/referential/transcribing-item/transcribing-item-row.utils';

export interface TranscribingItemRowValidatorOptions extends ReferentialValidatorOptions {
  multipleEnabled?: boolean;
  hiddenTranscribingAttributes?: string[];
}

@Injectable()
export class TranscribingItemRowValidator extends ReferentialValidatorService<TranscribingItemRow, number, TranscribingItemRowValidatorOptions> {
  transcribingColumns: ITranscribingColumn[];
  hiddenTranscribingAttributes: string[] = [];

  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: TranscribingItemRow, opts?: TranscribingItemRowValidatorOptions): { [p: string]: any } {
    return <Record<keyof ITranscribingItemRow, any>>{
      id: [data?.id || null],
      system: [data?.system || null, Validators.compose([Validators.required, SharedValidators.entity])],
      function: [data?.function || null, Validators.compose([Validators.required, SharedValidators.entity])],
      language: [data?.language || null, SharedValidators.entity],
      entityId: [data?.entityId || null],
      transcribingByAttribute: this.createCellFormGroup(data, opts),
      mainAttribute: [data?.mainAttribute || null],
      codificationType: [data?.codificationType || null, SharedValidators.entity],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
    };
  }

  getFormGroupOptions(data?: TranscribingItemRow, opts?: TranscribingItemRowValidatorOptions): AbstractControlOptions {
    if (opts?.multipleEnabled !== true) {
      return {
        // todo : check same type but only for functions that requires unity
        // validators: this.checkSameType(this.dataSource),
      };
    }
    return super.getFormGroupOptions(data, opts);
  }

  checkSameType(datasource: IEntitiesTableDataSource<any>): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const value = group.value;
      if (datasource?.getRows().filter((row) => TranscribingItemRowUtils.sameType(row.currentData, value)).length > 1) {
        return {
          sameType: 'SAME_TYPE', // todo i18n
        };
      }
      return null;
    };
  }

  protected createCellFormGroup(data?: TranscribingItemRow, opts?: TranscribingItemRowValidatorOptions): UntypedFormGroup {
    const formGroup = this.formBuilder.group({});
    this.transcribingColumns?.forEach((column) => {
      const cellData = data?.transcribingByAttribute?.[column.attribute];
      formGroup.addControl(
        column.attribute,
        opts?.multipleEnabled
          ? this.formBuilder.group(<PartialRecord<keyof ITranscribingCell, any>>{
              externalCode: [cellData?.externalCode || null],
              externalCodeMultiple: [null],
            })
          : this.formBuilder.group(
              <Record<keyof ITranscribingCell, any>>{
                id: [cellData?.id || null],
                typeId: [cellData?.typeId || null],
                parentTypeId: [cellData?.parentTypeId || null],
                externalCode: [cellData?.externalCode || null],
                updateDate: [cellData?.updateDate || null],
              },
              {
                // Make external code required if a type is provided (except for hidden attributes)
                validators: !this.hiddenTranscribingAttributes.includes(column.attribute)
                  ? SharedFormGroupValidators.requiredIf('externalCode', 'typeId', { fieldOnly: true })
                  : undefined,
              }
            )
      );
    });
    return formGroup;
  }
}
