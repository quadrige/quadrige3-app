import { BaseReferential, EntityClass, IEntity } from '@sumaris-net/ngx-components';
import { StrReferential } from '@app/referential/model/referential.model';
import { Moment } from 'moment';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';

export const transcribingItemRowFragment = gql`
  fragment TranscribingItemRowFragment on TranscribingItemRowVO {
    id
    system {
      ...ReferentialFragment
    }
    function {
      ...ReferentialFragment
    }
    language {
      ...ReferentialFragment
    }
    codificationType {
      ...ReferentialFragment
    }
    entityId
    externalCodeByAttribute
    mainAttribute
    comments
    creationDate
    updateDate
    statusId
    __typename
  }
  ${referentialFragments.light}
`;

export interface ITranscribingColumn {
  label: string;
  attribute: string;
}

export interface ITranscribingCell {
  id: number;
  externalCode: string;
  typeId: number;
  parentTypeId?: number;
  updateDate: Moment;
}

export interface ITranscribingItemRow extends IEntity<ITranscribingItemRow> {
  system: StrReferential;
  function: StrReferential;
  language: StrReferential;
  entityId: string;
  transcribingByAttribute: Record<string, ITranscribingCell>;
  mainAttribute: string;
  codificationType: StrReferential;
  comments: string;
  creationDate: Moment | Date;
}

@EntityClass({ typename: 'TranscribingItemRowVO' })
export class TranscribingItemRow extends BaseReferential<TranscribingItemRow> implements ITranscribingItemRow {
  static entityName = 'TranscribingItemRow';
  static fromObject: (source: any, opts?: any) => TranscribingItemRow;

  system: StrReferential = null;
  function: StrReferential = null;
  language: StrReferential = null;
  entityId: string = null;
  transcribingByAttribute: Record<string, ITranscribingCell> = null;
  mainAttribute: string = null;
  codificationType: StrReferential = null;

  constructor() {
    super(TranscribingItemRow.TYPENAME);
    this.transcribingByAttribute = {};
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.system = source.system;
    this.function = source.function;
    this.language = source.language;
    this.entityId = source.entityId;
    if (source.transcribingByAttribute) {
      this.transcribingByAttribute = source.transcribingByAttribute;
    } else if (source.externalCodeByAttribute) {
      Object.keys(source.externalCodeByAttribute).forEach((attribute) => {
        this.transcribingByAttribute[attribute] = {
          id: undefined,
          typeId: undefined,
          updateDate: undefined,
          externalCode: source.externalCodeByAttribute[attribute],
        };
      });
    }
    this.mainAttribute = source.mainAttribute;
    this.codificationType = source.codificationType;
  }
}
