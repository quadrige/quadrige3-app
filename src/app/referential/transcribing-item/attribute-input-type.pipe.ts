import { Pipe, PipeTransform } from '@angular/core';
import { StrReferential } from '@app/referential/model/referential.model';

// Attributes allowed to be alphanumerical (default are numeric)
const alphanumericTranscribingAttributes: Record<string, string[]> = {
  SANDRE: ['PROG_CD', 'UNIT_ID'],
  DALI: ['PAR_CD'],
  CAS: ['PAR_CD'],
};

@Pipe({
  name: 'attributeInputType',
  standalone: true,
})
export class AttributeInputTypePipe implements PipeTransform {
  /**
   * For a code attribute (_ID or _CD), return 'text' for alphanumeric transcribing attributes, 'number' otherwise.
   * Returns 'text' for other attributes
   *
   * @param attribute
   * @param system
   */
  transform(attribute: string, system: StrReferential): 'text' | 'number' {
    return attribute?.endsWith('_ID') || attribute?.endsWith('_CD')
      ? alphanumericTranscribingAttributes[system?.id]?.includes(attribute)
        ? 'text'
        : 'number'
      : 'text';
  }
}
