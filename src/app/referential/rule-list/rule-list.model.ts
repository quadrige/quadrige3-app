import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { ControlRule } from '@app/referential/rule/control-rule.model';

@EntityClass({ typename: 'RuleListVO' })
export class RuleList extends Referential<RuleList, string> {
  static entityName = 'RuleList';
  static fromObject: (source: any, opts?: any) => RuleList;

  active: boolean = null;
  firstMonth: number = null;
  lastMonth: number = null;
  controlRuleCount: number = null;
  responsibleDepartmentIds: number[] = null;
  responsibleUserIds: number[] = null;
  controlledDepartmentIds: number[] = null;
  programIds: string[] = null;
  controlRules: ControlRule[] = null;
  rulesLoaded = false;
  rulesDuplicated = false;

  constructor() {
    super(RuleList.TYPENAME);
    this.entityName = RuleList.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = RuleList.entityName;
    this.active = source.active;
    this.firstMonth = source.firstMonth;
    this.lastMonth = source.lastMonth;
    this.controlRuleCount = source.controlRuleCount;
    this.responsibleDepartmentIds = source.responsibleDepartmentIds || [];
    this.responsibleUserIds = source.responsibleUserIds || [];
    this.controlledDepartmentIds = source.controlledDepartmentIds || [];
    this.programIds = source.programIds || [];
    this.controlRules = source.controlRules?.map(ControlRule.fromObject) || [];
    this.rulesLoaded = source.rulesLoaded;
    this.rulesDuplicated = source.rulesDuplicated;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.controlRules = this.controlRules?.map((value) => value.asObject(opts));
    delete target.entityName;
    delete target.statusId;
    delete target.rulesLoaded;
    delete target.rulesDuplicated;
    return target;
  }
}
