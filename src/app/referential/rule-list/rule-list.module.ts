import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { RuleListTable } from '@app/referential/rule-list/rule-list.table';
import { RuleListDuplicateModal } from '@app/referential/rule-list/rule-list-duplicate.modal';
import { RuleListFilterForm } from '@app/referential/rule-list/filter/rule-list.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';

@NgModule({
  imports: [ReferentialModule, GenericSelectTable, DepartmentSelectTable, UserSelectTable],
  declarations: [RuleListTable, RuleListFilterForm, RuleListDuplicateModal],
  exports: [RuleListTable, RuleListFilterForm],
})
export class RuleListModule {}
