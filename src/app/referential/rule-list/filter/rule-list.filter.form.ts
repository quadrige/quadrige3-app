import { ChangeDetectionStrategy, Component } from '@angular/core';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { RuleListFilter, RuleListFilterCriteria } from '@app/referential/rule-list/filter/rule-list.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-rule-list-filter-form',
  templateUrl: './rule-list.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RuleListFilterForm extends ReferentialCriteriaFormComponent<RuleListFilter, RuleListFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idDescription;
  }

  protected criteriaToQueryParams(criteria: RuleListFilterCriteria): PartialRecord<keyof RuleListFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.onlyActive)) params.onlyActive = criteria.onlyActive;
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'departmentFilter');
    return params;
  }
}
