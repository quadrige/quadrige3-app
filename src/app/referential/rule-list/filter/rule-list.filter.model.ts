import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { RuleList } from '@app/referential/rule-list/rule-list.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'RuleListFilterCriteriaVO' })
export class RuleListFilterCriteria extends ReferentialFilterCriteria<RuleList, string> {
  static fromObject: (source: any, opts?: any) => RuleListFilterCriteria;

  programFilter: StrReferentialFilterCriteria = null;
  departmentFilter: IntReferentialFilterCriteria = null;
  onlyActive: boolean = null;

  constructor() {
    super(RuleListFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.onlyActive = source.onlyActive || false;
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.departmentFilter = IntReferentialFilterCriteria.fromObject(source.departmentFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.onlyActive = this.onlyActive || false;
    target.programFilter = this.programFilter?.asObject(opts);
    target.departmentFilter = this.departmentFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'onlyActive') return this.onlyActive === true;
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'departmentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.departmentFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'RuleListFilterVO' })
export class RuleListFilter extends ReferentialFilter<RuleListFilter, RuleListFilterCriteria, RuleList, string> {
  static fromObject: (source: any, opts?: any) => RuleListFilter;

  constructor() {
    super(RuleListFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): RuleListFilterCriteria {
    return RuleListFilterCriteria.fromObject(source, opts);
  }
}
