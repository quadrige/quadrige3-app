import { Injectable, Injector } from '@angular/core';
import {
  BaseReferentialService,
  defaultReferentialSaveOption,
  ReferentialEntityGraphqlQueries,
  referentialFragments,
  ReferentialSaveOptions,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { RuleList } from '@app/referential/rule-list/rule-list.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { parameterFragments } from '@app/referential/parameter/parameter.service';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { ControlledAttribute, ControlRule, newRuleIdSuffix, RuleFunction } from '@app/referential/rule/control-rule.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { jobFragments } from '@app/social/job/job.service';
import { RuleListFilter, RuleListFilterCriteria } from '@app/referential/rule-list/filter/rule-list.filter.model';

const rulePmfmuFragment = gql`
  fragment RulePmfmuFragment on RulePmfmuVO {
    id
    ruleId
    pmfmuId
    parameter {
      ...FullParameterFragment
    }
    matrix {
      ...ReferentialFragment
    }
    fraction {
      ...ReferentialFragment
    }
    method {
      ...ReferentialFragment
    }
    unit {
      ...ReferentialFragment
    }
    updateDate
    __typename
  }
  ${parameterFragments.fullParameter}
  ${referentialFragments.light}
`;

const controlledAttributeFragment = gql`
  fragment ControlledAttributeFragment on ControlledAttributeVO {
    id
    name
    entity {
      id
      name
      __typename
    }
    __typename
  }
`;

const ruleFunctionFragment = gql`
  fragment RuleFunctionFragment on RuleFunctionVO {
    id
    name
    __typename
  }
`;

/** ControlRuleVO fragment without preconditions and groups */
const lightRuleFragment = gql`
  fragment LightRuleFragment on ControlRuleVO {
    id
    ruleListId
    functionId
    controlledEntityId
    controlledAttributeId
    active
    blocking
    description
    errorMessage
    min
    max
    allowedValues
    pmfmus {
      ...RulePmfmuFragment
    }
    updateDate
    __typename
  }
  ${rulePmfmuFragment}
`;

const rulePreconditionFragment = gql`
  fragment RulePreconditionFragment on PreconditionControlRuleVO {
    id
    active
    bidirectional
    baseRule {
      ...LightRuleFragment
    }
    usedRule {
      ...LightRuleFragment
    }
    updateDate
    __typename
  }
  ${lightRuleFragment}
`;

const ruleGroupFragment = gql`
  fragment RuleGroupFragment on GroupControlRuleVO {
    id
    label
    active
    or
    rule {
      ...LightRuleFragment
    }
    updateDate
    __typename
  }
  ${lightRuleFragment}
`;

const ruleFragment = gql`
  fragment RuleFragment on ControlRuleVO {
    id
    ruleListId
    functionId
    controlledEntityId
    controlledAttributeId
    active
    blocking
    description
    errorMessage
    min
    max
    allowedValues
    pmfmus {
      ...RulePmfmuFragment
    }
    preconditions {
      ...RulePreconditionFragment
    }
    groups {
      ...RuleGroupFragment
    }
    updateDate
    __typename
  }
  ${rulePmfmuFragment}
  ${rulePreconditionFragment}
  ${ruleGroupFragment}
`;

export const ruleListFragments = {
  ruleList: gql`
    fragment RuleListFragment on RuleListVO {
      id
      description
      active
      firstMonth
      lastMonth
      controlRuleCount
      responsibleDepartmentIds
      responsibleUserIds
      controlledDepartmentIds
      programIds
      creationDate
      updateDate
      __typename
    }
  `,
  ruleListWithRules: gql`
    fragment RuleListWithRulesFragment on RuleListVO {
      id
      description
      active
      firstMonth
      lastMonth
      controlRules {
        ...RuleFragment
      }
      responsibleDepartmentIds
      responsibleUserIds
      controlledDepartmentIds
      programIds
      creationDate
      updateDate
      __typename
    }
    ${ruleFragment}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  load: gql`
    query RuleList($id: String) {
      data: ruleList(id: $id) {
        ...RuleListWithRulesFragment
      }
    }
    ${ruleListFragments.ruleListWithRules}
  `,

  loadAll: gql`
    query RuleLists($page: PageInput, $filter: RuleListFilterVOInput) {
      data: ruleLists(page: $page, filter: $filter) {
        ...RuleListFragment
      }
    }
    ${ruleListFragments.ruleList}
  `,

  loadAllWithTotal: gql`
    query RuleListsWithTotal($page: PageInput, $filter: RuleListFilterVOInput) {
      data: ruleLists(page: $page, filter: $filter) {
        ...RuleListFragment
      }
      total: ruleListsCount(filter: $filter)
    }
    ${ruleListFragments.ruleList}
  `,

  countAll: gql`
    query RuleListsCount($filter: RuleListFilterVOInput) {
      total: ruleListsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportRuleListsAsync($filter: RuleListFilterVOInput, $context: ExportContextInput) {
      data: exportRuleListsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

interface RuleListGraphqlMutations extends BaseEntityGraphqlMutations {
  saveAllWithRules: any;
}

const mutations: RuleListGraphqlMutations = {
  save: gql`
    mutation SaveRuleList($data: RuleListVOInput, $options: RuleListSaveOptionsInput) {
      data: saveRuleList(ruleList: $data, options: $options) {
        ...RuleListWithRulesFragment
      }
    }
    ${ruleListFragments.ruleListWithRules}
  `,

  saveAll: gql`
    mutation SaveRuleLists($data: [RuleListVOInput], $options: RuleListSaveOptionsInput) {
      data: saveRuleLists(ruleLists: $data, options: $options) {
        ...RuleListFragment
      }
    }
    ${ruleListFragments.ruleList}
  `,

  saveAllWithRules: gql`
    mutation SaveRuleListsWithRules($data: [RuleListVOInput], $options: RuleListSaveOptionsInput) {
      data: saveRuleLists(ruleLists: $data, options: $options) {
        ...RuleListWithRulesFragment
      }
    }
    ${ruleListFragments.ruleListWithRules}
  `,

  deleteAll: gql`
    mutation DeleteRuleLists($ids: [String]) {
      deleteRuleLists(ids: $ids)
    }
  `,
};

const otherQueries = {
  ruleExists: gql`
    query RuleExists($id: String) {
      data: ruleExists(id: $id)
    }
  `,
  controlledAttributes: gql`
    query {
      data: controlledAttributes {
        ...ControlledAttributeFragment
      }
    }
    ${controlledAttributeFragment}
  `,
  functions: gql`
    query {
      data: functions {
        ...RuleFunctionFragment
      }
    }
    ${ruleFunctionFragment}
  `,
};

interface RuleListSaveOptions extends ReferentialSaveOptions {
  withPrivileges: boolean;
  withDepartments: boolean;
  withPrograms: boolean;
  withRules: boolean;
}

const defaultSaveAllOptions: RuleListSaveOptions = {
  ...defaultReferentialSaveOption,
  withPrivileges: true,
  withDepartments: true,
  withPrograms: true,
  withRules: false,
};

const defaultSaveOptions: RuleListSaveOptions = {
  ...defaultReferentialSaveOption,
  withPrivileges: false,
  withDepartments: false,
  withPrograms: false,
  withRules: true,
};

@Injectable({ providedIn: 'root' })
export class RuleListService extends BaseReferentialService<RuleList, RuleListFilter, RuleListFilterCriteria, string, any, any, RuleListSaveOptions> {
  constructor(protected injector: Injector) {
    super(injector, RuleList, RuleListFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache', loadFetchPolicy: 'no-cache' });
    this._logPrefix = '[ruleList-service]';
  }

  async saveAll(entities: RuleList[], opts?: RuleListSaveOptions): Promise<RuleList[]> {
    // Split save operations depending on rules are loaded or not
    const ruleListsWithRules = entities?.filter((value) => value.rulesLoaded) || [];
    const ruleListsWithoutRules = entities?.filter((value) => !value.rulesLoaded) || [];

    // Affect new rule ids from duplication (Mantis #58320)
    const ruleListWithDuplicatedRules = ruleListsWithRules?.filter((ruleList) => ruleList.rulesDuplicated) || [];
    for (const ruleList of ruleListWithDuplicatedRules) {
      const ruleListId = ruleList.id;
      let index = 1;
      const suffix = newRuleIdSuffix();

      // Convert to control rules to ease the process
      const controlRules: ControlRule[] = EntityUtils.sort(ruleList.controlRules);
      for (const controlRule of controlRules) {
        // Change control rule id with correct rule list id
        controlRule.id = `${ruleListId}_${index++}`;
        // Change preconditioned rule ids
        for (const precondition of controlRule.preconditions || []) {
          precondition.baseRule.id = await this.nextRuleId(controlRule.id, suffix);
          precondition.usedRule.id = await this.nextRuleId(controlRule.id, suffix);
        }
        // Change grouped rule ids
        for (const group of controlRule.groups || []) {
          group.rule.id = await this.nextRuleId(controlRule.id, suffix);
        }
      }
      // Convert back to rules
      ruleList.controlRules = controlRules;
      // Reset remaining properties
      ruleList.controlRules.forEach((rule) => {
        rule.updateDate = undefined;
        // Reset children ids and updateDate
        rule.pmfmus?.forEach((value) => {
          value.id = undefined;
          value.updateDate = undefined;
        });
        rule.preconditions?.forEach((value) => {
          value.id = undefined;
          value.updateDate = undefined;
        });
        rule.groups?.forEach((value) => {
          value.id = undefined;
          value.updateDate = undefined;
        });
      });
    }

    if (this._debug) {
      console.debug(`${this._logPrefix} Saving ${ruleListsWithRules.length} ruleLists with rules`, ruleListsWithRules);
      console.debug(`${this._logPrefix} Saving ${ruleListsWithoutRules.length} ruleLists without rules`, ruleListsWithoutRules);
    }
    const [result1, result2] = await Promise.all([
      super.saveAll(ruleListsWithRules, { ...defaultSaveAllOptions, ...opts, withRules: true, query: mutations.saveAllWithRules }),
      super.saveAll(ruleListsWithoutRules, { ...defaultSaveAllOptions, ...opts }),
    ]);
    return [].concat(...result1).concat(...result2);
  }

  async save(entity: RuleList, opts?: RuleListSaveOptions): Promise<RuleList> {
    // Always save rules with unique rule list
    return super.save(entity, { ...defaultSaveOptions, ...opts });
  }

  async nextRuleId(targetId: string, suffix: { index: number }): Promise<string> {
    let ruleId: string;
    do {
      const suffixString = suffix.index.toString();
      suffix.index++;
      const suffixLength = suffixString.length + 1;
      const totalLength = targetId.length + suffixLength;
      if (totalLength > 40) {
        targetId = targetId.substring(0, 40 - suffixLength);
      }
      ruleId = `${targetId}_${suffixString}`;
    } while (await this.ruleExists(ruleId));
    return ruleId;
  }

  async ruleExists(id: string): Promise<boolean> {
    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Check Rule exists...`, id);
    const res = await this.graphql.query<{ data?: boolean }>({
      query: otherQueries.ruleExists,
      variables: { id },
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.COUNT_REFERENTIAL_ERROR' },
      fetchPolicy: 'no-cache',
    });
    if (this._debug) console.debug(`${this._logPrefix} Check Rule exists in ${Date.now() - now}ms`, res.data);
    return res.data;
  }

  async controlledAttributes(): Promise<ControlledAttribute[]> {
    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Get all controlled attributes...`);
    const res = await this.graphql.query<{ data?: any[] }>({
      query: otherQueries.controlledAttributes,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.COUNT_REFERENTIAL_ERROR' },
      fetchPolicy: 'no-cache',
    });
    if (this._debug) console.debug(`${this._logPrefix} All controlled attributes in ${Date.now() - now}ms`, res.data);
    return (res.data || []).map((value) => ControlledAttribute.fromObject(value));
  }

  async ruleFunctions(): Promise<RuleFunction[]> {
    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Get all rule functions...`);
    const res = await this.graphql.query<{ data?: any[] }>({
      query: otherQueries.functions,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.COUNT_REFERENTIAL_ERROR' },
      fetchPolicy: 'no-cache',
    });
    if (this._debug) console.debug(`${this._logPrefix} All rule functions in ${Date.now() - now}ms`, res.data);
    return (res.data || []).map((value) => RuleFunction.fromObject(value));
  }
}
