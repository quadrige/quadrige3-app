import { ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import { isNilOrBlank, isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { RuleList } from '@app/referential/rule-list/rule-list.model';
import { RuleListService } from '@app/referential/rule-list/rule-list.service';
import { RuleListValidatorService } from '@app/referential/rule-list/rule-list.validator';
import {
  IRuleListDuplicateModalOptions,
  IRuleListDuplicateModalResult,
  RuleListDuplicateModal,
} from '@app/referential/rule-list/rule-list-duplicate.modal';
import { EntityUtils } from '@app/shared/entity.utils';
import { IAddResult, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { RuleListFilter, RuleListFilterCriteria } from '@app/referential/rule-list/filter/rule-list.filter.model';
import { ExportOptions, IRightSelectTable } from '@app/shared/table/table.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { UserSelectTable } from '@app/referential/user/user.select.table';
import { DepartmentSelectTable } from '@app/referential/department/department.select.table';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

type RuleListView = 'genericTable' | 'userTable' | 'departmentTable';

@Component({
  selector: 'app-rule-list-table',
  templateUrl: './rule-list.table.html',
  styleUrls: ['./rule-list.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RuleListTable
  extends ReferentialTable<RuleList, string, RuleListFilter, RuleListFilterCriteria, RuleListValidatorService, RuleListView>
  implements OnInit
{
  genericTable = viewChild<GenericSelectTable>('genericTable');
  userTable = viewChild<UserSelectTable>('userTable');
  departmentTable = viewChild<DepartmentSelectTable>('departmentTable');

  readonly ruleListMenuItems: IEntityMenuItem<RuleList, RuleListView>[] = [
    {
      title: 'REFERENTIAL.RULE_LIST.RESPONSIBLE.TITLE',
      children: [
        {
          title: 'REFERENTIAL.ENTITY.USERS',
          entityName: 'User',
          attribute: 'responsibleUserIds',
          view: 'userTable',
          viewTitle: 'REFERENTIAL.RULE_LIST.RESPONSIBLE.USER',
        },
        {
          title: 'REFERENTIAL.ENTITY.DEPARTMENTS',
          entityName: 'Department',
          attribute: 'responsibleDepartmentIds',
          view: 'departmentTable',
          viewTitle: 'REFERENTIAL.RULE_LIST.RESPONSIBLE.DEPARTMENT',
        },
      ],
    },
    {
      title: 'REFERENTIAL.RULE_LIST.PROGRAMS',
      entityName: 'Program',
      attribute: 'programIds',
      view: 'genericTable',
    },
    {
      title: 'REFERENTIAL.RULE_LIST.DEPARTMENTS',
      entityName: 'Department',
      attribute: 'controlledDepartmentIds',
      view: 'genericTable',
    },
  ];

  rightAreaEnabled = false;

  constructor(
    protected injector: Injector,
    protected _entityService: RuleListService,
    protected validatorService: RuleListValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['description', 'active', 'firstMonth', 'lastMonth', 'rules', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      RuleList,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.RULE_LIST.';
    this.logPrefix = '[rule-list-table]';
    this.setShowColumn('statusId', false);
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: RuleList = this.singleSelectedRow.currentData;
      return (
        isNotEmptyArray(data.responsibleDepartmentIds) ||
        isNotEmptyArray(data.responsibleUserIds) ||
        isNotEmptyArray(data.programIds) ||
        isNotEmptyArray(data.controlledDepartmentIds)
      );
    }
    return false;
  }

  async onAfterSelectionChange(row?: AsyncTableElement<RuleList>) {
    this.refreshButtons();
    await super.onAfterSelectionChange(row);
  }

  updatePermission() {
    // Manage user rights :
    // By default, all user have edition write to allow further finer checks
    this.canEdit = true;
    // Refresh other controls
    this.refreshButtons();
    this.markForCheck();
  }

  async setFilter(f: RuleListFilter, opts?: ISetFilterOptions) {
    await super.setFilter(f, opts);

    if (this.previousUrl) {
      // Try to reselect previous program from url (Mantis #59263)
      const test = /\/referential\/RuleList\/(\w+)\/rule/g.exec(this.previousUrl);
      const ruleListIdToSelect = test?.[1];
      if (!!ruleListIdToSelect) {
        setTimeout(() => this.selectRowByData(<RuleList>{ id: ruleListIdToSelect }), 250);
      }
    }
  }

  async resetFilter(filter?: RuleListFilter, opts?: ISetFilterOptions) {
    filter = this.asFilter(filter);
    filter.patch({
      onlyActive: false,
    });
    await super.resetFilter(filter, opts);
  }

  openRules(event: UIEvent, row: AsyncTableElement<RuleList>) {
    if (event) event.stopPropagation();
    if (!row.validator.invalid) {
      // Go to control rules table
      this.router.navigate([row.currentData.id, 'rule'], {
        relativeTo: this.route,
      });
    }
  }

  rightTableSelectionChanged(values: any[]) {
    if (this.canEdit) {
      this.patchRow(this.rightMenuItem.attribute, values);
    }
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<RuleList>): Promise<boolean> {
    // following lines comes from super.editRow(event, row), don't remove
    if (!this._enabled) return false;
    if (this.singleEditingRow === row) return true; // Already the edited row
    if (event?.defaultPrevented) return false;
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // User without right to edit, don't turn on row edition
    if (!this.hasRightOnRuleList(row.currentData)) {
      return true;
    }

    return super.editRow(event, row);
  }

  async duplicateRuleList(event: MouseEvent) {
    event?.stopPropagation();
    if (this.selection.selected.length !== 1) return;

    const row = this.singleSelectedRow;
    if (!row || !(await this.confirmEditCreate(event, row))) return false;

    // Open select program modal
    const { data } = await this.modalService.openModal<IRuleListDuplicateModalOptions, IRuleListDuplicateModalResult>(
      RuleListDuplicateModal,
      {
        ruleListId: row.currentData.id,
      },
      'modal-200'
    );

    if (data) {
      this.markAsLoading();
      await this.duplicateRow(event, { withRules: data.withRules });
      this.markAsLoaded();
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { forceAllColumns: true, rowCountWarningThreshold: 5, ...opts });
  }

  // protected methods

  protected getRightMenuItems(): IEntityMenuItem<RuleList, RuleListView>[] {
    return this.ruleListMenuItems;
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<RuleList, RuleListView> {
    return this.ruleListMenuItems[0].children[0];
  }

  // Override default referential table equals method because RuleList don't have 'name' attribute
  protected equals(t1: RuleList, t2: RuleList): boolean {
    return EntityUtils.equals(t1, t2, 'id');
  }

  protected async defaultNewRowValue() {
    return {
      ...(await super.defaultNewRowValue()),
      responsibleUserIds: [this.accountService.person.id],
      responsibleDepartmentIds: [this.accountService.department.id],
      controlledDepartmentIds: [this.accountService.department.id],
      programIds: this.permissionService.managedProgramIds,
    };
  }

  protected async patchDuplicateEntity(entity: RuleList, opts?: any): Promise<any> {
    const patch = await super.patchDuplicateEntity(entity, opts);

    // Filter writable programs for non admin
    if (!this.accountService.isAdmin()) {
      const writableProgramIds = this.permissionService.writableProgramIds;
      patch.programIds = patch.programIds.filter((programId: string) => writableProgramIds.includes(programId));
    }

    if (opts?.withRules) {
      // Load rules to duplicate, further duplication process is made in service
      patch.controlRules = (await this._entityService.load(entity.id)).controlRules;
      patch.rulesLoaded = true;
      patch.rulesDuplicated = true;
      patch.controlRuleCount = entity.controlRuleCount;
    } else {
      patch.controlRules = undefined;
      patch.rulesLoaded = false;
      patch.rulesDuplicated = false;
      patch.controlRuleCount = undefined;
    }
    return patch;
  }

  protected async loadRightArea(row?: AsyncTableElement<RuleList>) {
    if (this.subTable) return; // don't load if sub table
    switch (this.rightMenuItem?.view) {
      case 'genericTable': {
        this.detectChanges();
        if (this.genericTable()) {
          this.registerSubForm(this.genericTable());
          await this.loadGenericTable(row);
        }
        break;
      }
      case 'userTable': {
        this.detectChanges();
        if (this.userTable()) {
          this.registerSubForm(this.userTable());
          await this.loadUserTable(row);
        }
        break;
      }
      case 'departmentTable': {
        this.detectChanges();
        if (this.departmentTable()) {
          this.registerSubForm(this.departmentTable());
          await this.loadDepartmentTable(row);
        }
        break;
      }
    }
    await super.loadRightArea(row);
  }

  protected async loadGenericTable(row?: AsyncTableElement<RuleList>) {
    // set entityName
    await this.genericTable().setEntityName(this.rightMenuItem.entityName);

    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.genericTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }
    const attribute = this.rightMenuItem.attribute;
    if (isNilOrBlank(attribute)) {
      throw Error(`${this.logPrefix} no ruleList attribute for type ${this.rightMenuItem}`);
    }

    // Load selected data
    await this.genericTable().setSelectedIds(((row.currentData?.[attribute] || []) as any[]).slice());
    this.rightAreaEnabled = this.hasRightOnRuleList(row.currentData);
  }

  protected async loadUserTable(row?: AsyncTableElement<RuleList>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.userTable(), row);
      await this.userTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected users
    this.applyRightOptions(this.userTable(), row);
    await this.userTable().setSelectedIds((row.currentData?.responsibleUserIds || []).slice());
    this.rightAreaEnabled = this.hasRightOnRuleList(row?.currentData);
  }

  protected async addUser(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'User',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.userTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.userTable());
      await this.userTable().setSelectedIds(includedIds);
      this.rightTableSelectionChanged(includedIds);
    }
  }

  protected async loadDepartmentTable(row?: AsyncTableElement<RuleList>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.applyRightOptions(this.departmentTable(), row);
      await this.departmentTable().setSelectedIds(undefined);
      this.rightAreaEnabled = false;
      return;
    }

    // Load selected departments
    this.applyRightOptions(this.departmentTable(), row);
    await this.departmentTable().setSelectedIds((row.currentData?.responsibleDepartmentIds || []).slice());
    this.rightAreaEnabled = this.hasRightOnRuleList(row?.currentData);
  }

  protected async addDepartment(event: ISelectModalOptions) {
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: 'Department',
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: event.selectCriteria.includedIds || [] },
        titleAddPrefixI18n: 'COMMON.ADD',
        titleI18n: this.rightMenuItem.viewTitle,
        showMode: false,
      },
      'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.addedIds)) {
      const includedIds = [...(this.departmentTable().selectCriteria.includedIds || []), ...data.addedIds];
      this.applyRightOptions(this.departmentTable());
      await this.departmentTable().setSelectedIds(includedIds);
      this.rightTableSelectionChanged(includedIds);
    }
  }

  protected applyRightOptions(table: IRightSelectTable<any>, row?: AsyncTableElement<RuleList>) {
    row = row || this.singleEditingRow;
    table.rightOptions = {
      entityName: RuleList.entityName,
      ruleListId: row?.currentData.id,
    };
  }

  protected hasRightOnRuleList(ruleList: RuleList): boolean {
    if (!ruleList) {
      return false;
    }

    // Admins and users with manager right can edit
    return (
      this.accountService.isAdmin() ||
      (this.accountService.isUser() &&
        (ruleList.responsibleUserIds.includes(this.accountService.person.id) ||
          ruleList.responsibleDepartmentIds.includes(this.accountService.department.id)))
    );
  }

  /**
   * Refresh buttons state according to user profile or rule list rights
   *
   * @protected
   */
  protected refreshButtons() {
    // Only admins can add rule list
    this.tableButtons.canAdd = this.permissionService.hasSomeManagementPermission;
    this.tableButtons.canDuplicate = this.permissionService.hasSomeManagementPermission;
    this.tableButtons.canDelete =
      (this.selection.hasValue() && this.selection.selected.every((row) => this.hasRightOnRuleList(row.currentData))) || false;
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(true, 40);
  }

  protected updateTitle() {
    this.title = this.titleI18n
      ? this.translate.instant(this.titleI18n)
      : ['MENU.REFERENTIAL.MANAGEMENT', 'REFERENTIAL.ENTITY.RULE_LISTS'].map((value) => this.translate.instant(value)).join(' &rArr; ');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('description', 'active', 'rules');
  }
}
