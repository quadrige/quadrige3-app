import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IRuleListDuplicateModalOptions extends IModalOptions {
  ruleListId: string;
}

export interface IRuleListDuplicateModalResult {
  withRules: boolean;
}

@Component({
  selector: 'app-rule-list-duplicate-modal',
  templateUrl: './rule-list-duplicate.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RuleListDuplicateModal extends ModalComponent<IRuleListDuplicateModalResult> implements IRuleListDuplicateModalOptions {
  @Input() ruleListId: string;

  constructor(protected injector: Injector) {
    super(injector);
    this.setForm(this.formBuilder.group({ withRules: [false] }));
  }

  protected afterInit(): Promise<void> | void {
    return undefined;
  }

  protected dataToValidate(): Promise<IRuleListDuplicateModalResult> | IRuleListDuplicateModalResult {
    return this.value;
  }
}
