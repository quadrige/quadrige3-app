import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { RuleListTable } from '@app/referential/rule-list/rule-list.table';
import { RuleListModule } from '@app/referential/rule-list/rule-list.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: RuleListTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
  {
    path: ':ruleListId/rule',
    runGuardsAndResolvers: 'pathParamsChange',
    loadChildren: () => import('../rule/control-rule.routing.module').then((m) => m.ControlRuleRoutingModule),
    data: {
      profile: 'ADMIN',
      pathIdParam: 'ruleListId',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), RuleListModule],
})
export class RuleListRoutingModule {}
