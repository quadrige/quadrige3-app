import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedFormGroupValidators, toBoolean } from '@sumaris-net/ngx-components';
import {
  lengthDescription,
  lengthLabel,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { RuleList } from '@app/referential/rule-list/rule-list.model';
import { RuleListService } from '@app/referential/rule-list/rule-list.service';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class RuleListValidatorService extends ReferentialValidatorService<RuleList, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: RuleListService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: RuleList, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkIdAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.compose([Validators.required, Validators.maxLength(lengthDescription)])],
      active: [toBoolean(data?.active, true)],
      firstMonth: [data?.firstMonth || null],
      lastMonth: [data?.lastMonth || null],
      controlRuleCount: [data?.controlRuleCount || null],
      responsibleDepartmentIds: [data?.responsibleDepartmentIds || null],
      responsibleUserIds: [data?.responsibleUserIds || null],
      controlledDepartmentIds: [data?.controlledDepartmentIds || null],
      programIds: [data?.programIds || null],
      controlRules: [data?.controlRules || null],
      rulesLoaded: [data?.rulesLoaded || null],
      rulesDuplicated: [data?.rulesDuplicated || null],
      // statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: RuleList, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredOneArrayNotEmpty(
          ['responsibleDepartmentIds', 'responsibleUserIds'],
          'REFERENTIAL.RULE_LIST.ERROR.MISSING_MANAGER'
        ),
        BaseGroupValidators.requiredOneArrayNotEmpty(['controlledDepartmentIds'], 'REFERENTIAL.RULE_LIST.ERROR.MISSING_DEPARTMENT'),
        BaseGroupValidators.requiredOneArrayNotEmpty(['programIds'], 'REFERENTIAL.RULE_LIST.ERROR.MISSING_PROGRAM'),
        SharedFormGroupValidators.requiredIf('firstMonth', 'lastMonth'),
        SharedFormGroupValidators.requiredIf('lastMonth', 'firstMonth'),
      ],
    };
  }
}
