import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { UnitTable } from '@app/referential/unit/unit.table';
import { UnitModule } from '@app/referential/unit/unit.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: UnitTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), UnitModule],
})
export class UnitRoutingModule {}
