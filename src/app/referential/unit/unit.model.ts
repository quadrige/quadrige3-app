import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'UnitVO' })
export class Unit extends Referential<Unit> {
  static entityName = 'Unit';
  static fromObject: (source: any, opts?: any) => Unit;

  symbol: string = null;

  constructor() {
    super(Unit.TYPENAME);
    this.entityName = Unit.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Unit.entityName;
    this.symbol = source.symbol;
  }
}
