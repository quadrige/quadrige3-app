import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { UnitTable } from '@app/referential/unit/unit.table';
import { UnitService } from '@app/referential/unit/unit.service';
import { UnitValidatorService } from '@app/referential/unit/unit.validator';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IntReferentialFilter } from '@app/referential/model/referential.filter.model';
import { IAddTable } from '@app/shared/table/table.model';
import { Unit } from '@app/referential/unit/unit.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { UnitFilterForm } from '@app/referential/unit/filter/unit.filter.form';

@Component({
  selector: 'app-unit-add-table',
  templateUrl: './unit.table.html',
  standalone: true,
  imports: [ReferentialModule, UnitFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnitAddTable extends UnitTable implements IAddTable<Unit> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: UnitService,
    protected validatorService: UnitValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(injector, _entityService, validatorService, pmfmuService, strategyService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[unit-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: IntReferentialFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
