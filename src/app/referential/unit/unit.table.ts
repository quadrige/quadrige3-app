import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { isNotEmptyArray, referentialToString, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS, StatusIds } from '@sumaris-net/ngx-components';
import { Unit } from '@app/referential/unit/unit.model';
import { UnitValidatorService } from '@app/referential/unit/unit.validator';
import { UnitService } from '@app/referential/unit/unit.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { StrategyService } from '@app/referential/program-strategy/strategy/strategy.service';
import { Alerts } from '@app/shared/alerts';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { EntityUtils } from '@app/shared/entity.utils';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';
import { StrategyFilter } from '@app/referential/program-strategy/strategy/filter/strategy.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-unit-table',
  templateUrl: './unit.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnitTable
  extends ReferentialTable<Unit, number, IntReferentialFilter, IntReferentialFilterCriteria, UnitValidatorService>
  implements OnInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: UnitService,
    protected validatorService: UnitValidatorService,
    protected pmfmuService: PmfmuService,
    protected strategyService: StrategyService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'symbol', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      Unit,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.UNIT.';
    this.defaultSortBy = 'name';
    this.logPrefix = '[unit-table]';
  }

  protected async canSaveRowsWithDisabledStatus(dirtyRowsWithDisabledStatusId: AsyncTableElement<Unit>[]): Promise<boolean> {
    let confirmed = true;
    const unitIds = EntityUtils.ids(dirtyRowsWithDisabledStatusId);

    // Check enabled pmfmu with this unit
    const exists = await this.pmfmuService.exists(
      PmfmuFilterCriteria.fromObject({
        statusId: StatusIds.ENABLE,
        unitFilter: { includedIds: unitIds },
      })
    );
    if (exists) {
      confirmed = await Alerts.askConfirmation('REFERENTIAL.UNIT.CONFIRM.DISABLE_PMFMU', this.alertCtrl, this.translate);
    }

    if (confirmed) {
      // Check if used in pmfmu applied strategies
      const res = await this.strategyService.loadPage(
        { offset: 0, size: 100 },
        StrategyFilter.fromObject({
          criterias: [
            {
              programFilter: { statusId: StatusIds.ENABLE },
              onlyActive: true,
              pmfmuFilter: {
                unitFilter: {
                  includedIds: unitIds,
                },
              },
            },
          ],
        })
      );

      if (isNotEmptyArray(res.data)) {
        confirmed = await Alerts.askConfirmation('REFERENTIAL.UNIT.CONFIRM.DISABLE_APPLIED_STRATEGIES', this.alertCtrl, this.translate, undefined, {
          list: `<div class="scroll-content"><ul>${res.data
            .map((r) => referentialToString(r, ['programId', 'name']))
            .map((s) => `<li>${s}</li>`)
            .join('')}</ul></div>`,
        });
      }
    }

    return confirmed;
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.UNITS' });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name', 'symbol');
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }
}
