import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-unit-filter-form',
  templateUrl: './unit.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnitFilterForm extends ReferentialCriteriaFormComponent<IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName.concat('symbol');
  }
}
