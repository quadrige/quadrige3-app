import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { Unit } from '@app/referential/unit/unit.model';
import { UnitService } from '@app/referential/unit/unit.service';
import {
  lengthComment,
  lengthName,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';

@Injectable({ providedIn: 'root' })
export class UnitValidatorService extends ReferentialValidatorService<Unit> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: UnitService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Unit, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      name: [
        data?.name || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthName)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data),
      ],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      symbol: [data?.symbol || null, Validators.compose([Validators.required, Validators.maxLength(lengthName)])],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
