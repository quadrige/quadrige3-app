import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { UnitTable } from '@app/referential/unit/unit.table';
import { UnitFilterForm } from '@app/referential/unit/filter/unit.filter.form';

@NgModule({
  imports: [ReferentialModule, UnitFilterForm],
  declarations: [UnitTable],
  exports: [UnitTable],
})
export class UnitModule {}
