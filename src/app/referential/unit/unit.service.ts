import { Injectable, Injector } from '@angular/core';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Unit } from '@app/referential/unit/unit.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { jobFragments } from '@app/social/job/job.service';

const fragments = {
  unit: gql`
    fragment UnitFragment on UnitVO {
      id
      name
      symbol
      comments
      updateDate
      creationDate
      statusId
      __typename
    }
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Units($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: units(page: $page, filter: $filter) {
        ...UnitFragment
      }
    }
    ${fragments.unit}
  `,

  loadAllWithTotal: gql`
    query UnitsWithTotal($page: PageInput, $filter: IntReferentialFilterVOInput) {
      data: units(page: $page, filter: $filter) {
        ...UnitFragment
      }
      total: unitsCount(filter: $filter)
    }
    ${fragments.unit}
  `,

  countAll: gql`
    query UnitsCount($filter: IntReferentialFilterVOInput) {
      total: unitsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportUnitsAsync($filter: IntReferentialFilterVOInput, $context: ExportContextInput) {
      data: exportUnitsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveUnits($data: [UnitVOInput]) {
      data: saveUnits(units: $data) {
        ...UnitFragment
      }
    }
    ${fragments.unit}
  `,

  deleteAll: gql`
    mutation DeleteUnits($ids: [Int]) {
      deleteUnits(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class UnitService extends BaseReferentialService<Unit, IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Unit, IntReferentialFilter, { queries, mutations });
    this._logPrefix = '[unit-service]';
  }
}
