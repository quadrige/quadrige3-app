import { ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { SurveyTable } from '@app/data/survey/survey.table';
import { SurveyFilter } from '@app/data/survey/filter/survey.filter.model';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IMoratoriumSurveyModalOptions extends IModalOptions {
  surveyFilter: SurveyFilter;
}

@Component({
  selector: 'app-moratorium-survey-modal',
  templateUrl: './moratorium-survey.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoratoriumSurveyModal extends ModalComponent<void> implements IMoratoriumSurveyModalOptions {
  @ViewChild('surveyTable') surveyTable: SurveyTable;

  @Input() surveyFilter: SurveyFilter;

  constructor(protected injector: Injector) {
    super(injector);
  }

  protected afterInit(): Promise<void> | void {
    this.surveyTable.tableButtons.canFilter = false;
    this.surveyTable.setFilter(this.surveyFilter, { emitEvent: true });
  }

  protected dataToValidate(): Promise<void> | void {
    return undefined;
  }
}
