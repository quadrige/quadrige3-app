import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { PmfmuModule } from '@app/referential/pmfmu/pmfmu.module';
import { MoratoriumTable } from '@app/referential/moratorium/moratorium.table';
import { MoratoriumPeriodTable } from '@app/referential/moratorium/period/moratorium-period.table';
import { MoratoriumPmfmuTable } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.table';
import { MoratoriumSurveyModal } from '@app/referential/moratorium/survey/moratorium-survey.modal';
import { SurveyModule } from '@app/data/survey/survey.module';
import { ParameterModule } from '@app/referential/parameter/parameter.module';
import { MoratoriumFilterForm } from '@app/referential/moratorium/filter/moratorium.filter.form';
import { MoratoriumPmfmuFilterForm } from '@app/referential/moratorium/pmfmu/filter/moratorium-pmfmu.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { DateRangeFilterFormField } from '@app/shared/component/filter/date-range-filter-form-field.component';

@NgModule({
  imports: [ReferentialModule, PmfmuModule, ParameterModule, SurveyModule, GenericSelectTable, DateRangeFilterFormField],
  declarations: [
    MoratoriumTable,
    MoratoriumFilterForm,
    MoratoriumPeriodTable,
    MoratoriumPmfmuTable,
    MoratoriumPmfmuFilterForm,
    MoratoriumSurveyModal,
  ],
  exports: [MoratoriumTable],
})
export class MoratoriumModule {}
