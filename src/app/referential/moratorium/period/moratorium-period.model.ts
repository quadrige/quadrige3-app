import { EntityClass, fromDateISOString, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { Dates } from '@app/shared/dates';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'MoratoriumPeriodVO' })
export class MoratoriumPeriod extends Referential<MoratoriumPeriod, string> {
  static entityName = 'MoratoriumPeriod';
  static fromObject: (source: any, opts?: any) => MoratoriumPeriod;

  moratoriumId: number = null;
  startDate: Moment = null;
  endDate: Moment = null;

  constructor(moratoriumId?: number, startDate?: Moment, endDate?: Moment) {
    super(MoratoriumPeriod.TYPENAME);
    this.entityName = MoratoriumPeriod.entityName;
    this.moratoriumId = moratoriumId;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = MoratoriumPeriod.entityName;
    this.moratoriumId = source.moratoriumId;
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.startDate = Dates.toLocalDateString(this.startDate);
    target.endDate = Dates.toLocalDateString(this.endDate);
    delete target.entityName;
    delete target.name;
    delete target.label;
    delete target.description;
    delete target.comments;
    delete target.statusId;
    return target;
  }

  dateEquals(other: MoratoriumPeriod): boolean {
    return this.startDate.isSame(other.startDate, 'date') && this.endDate.isSame(other.endDate, 'date');
  }
}
