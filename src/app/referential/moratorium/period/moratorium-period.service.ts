import { Injectable } from '@angular/core';
import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { MoratoriumPeriod } from '@app/referential/moratorium/period/moratorium-period.model';

@Injectable({ providedIn: 'root' })
export class MoratoriumPeriodService extends UnfilteredEntitiesMemoryService<MoratoriumPeriod, string> {
  constructor() {
    super(MoratoriumPeriod);
  }
}
