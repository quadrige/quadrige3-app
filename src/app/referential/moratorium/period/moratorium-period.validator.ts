import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { Injectable } from '@angular/core';
import { AbstractControlOptions, AsyncValidatorFn, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, Validators } from '@angular/forms';
import { IEntitiesTableDataSource, SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { Observable, timer } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { Dates } from '@app/shared/dates';
import { MoratoriumPeriod } from '@app/referential/moratorium/period/moratorium-period.model';

@Injectable({ providedIn: 'root' })
export class MoratoriumPeriodValidatorService extends ReferentialValidatorService<MoratoriumPeriod, string> {
  private overlappingError = { overlappingErrorMsg: 'REFERENTIAL.MORATORIUM_PERIOD.ERROR.OVERLAPPING_PERIODS' };

  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: MoratoriumPeriod, opts?: ReferentialValidatorOptions): { [p: string]: any } {
    return {
      id: [data?.id || null],
      moratoriumId: [data?.moratoriumId || null],
      startDate: [data?.startDate || null], // no validators here because mat-date-field owns them
      endDate: [data?.endDate || null], // no validators here because mat-date-field owns them
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: MoratoriumPeriod, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [SharedFormGroupValidators.dateRange('startDate', 'endDate')],
      asyncValidators: this.checkOverlappingPeriods(this.dataSource),
    };
  }

  checkOverlappingPeriods(dataSource: IEntitiesTableDataSource<any>): AsyncValidatorFn {
    return (group: UntypedFormGroup): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => group),
        mergeMap(async (_group) => {
          const value = _group.value;
          if (_group.dirty && !!value.startDate && !!value.endDate) {
            // first check in current rows
            if (
              dataSource
                .getRows()
                .filter((row) =>
                  Dates.overlaps({ start: row.currentData.startDate, end: row.currentData.endDate }, { start: value.startDate, end: value.endDate })
                ).length > 1
            ) {
              return this.overlappingError;
            }
          }
          return null;
        })
      );
  }
}
