import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { MoratoriumPeriodService } from '@app/referential/moratorium/period/moratorium-period.service';
import { MoratoriumPeriod } from '@app/referential/moratorium/period/moratorium-period.model';
import { MoratoriumPeriodValidatorService } from '@app/referential/moratorium/period/moratorium-period.validator';
import { Moratorium } from '@app/referential/moratorium/moratorium.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';

@Component({
  selector: 'app-moratorium-period-table',
  templateUrl: './moratorium-period.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoratoriumPeriodTable
  extends ReferentialMemoryTable<MoratoriumPeriod, string, any, any, MoratoriumPeriodValidatorService>
  implements OnInit, AfterViewInit
{
  private _multiplePeriods = false;

  constructor(
    protected injector: Injector,
    protected _entityService: MoratoriumPeriodService,
    protected validatorService: MoratoriumPeriodValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['startDate', 'endDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      MoratoriumPeriod,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.MORATORIUM.PERIOD';
    this.i18nColumnPrefix = 'REFERENTIAL.MORATORIUM_PERIOD.';
    this.defaultSortBy = 'startDate';
    this.logPrefix = '[moratorium-period-table]';
  }

  get multiplePeriods(): boolean {
    return this._multiplePeriods;
  }

  set multiplePeriods(value: boolean) {
    this._multiplePeriods = value;
    this.inlineEdition = this.canEdit && !value;
  }

  ngOnInit() {
    this.subTable = true;
    this.showPaginator = false;

    super.ngOnInit();
  }

  async setMoratorium(moratorium: Moratorium) {
    this.multiplePeriods = false;

    await this.setValue(moratorium?.periods?.slice() || []);
  }

  async setMoratoriums(moratoriums: Moratorium[]) {
    this.multiplePeriods = true;

    // compute only common periods
    const distinctPeriods: MoratoriumPeriod[] = [];
    moratoriums?.forEach((moratorium) => {
      moratorium.periods.forEach((moratoriumPeriod) => {
        if (!distinctPeriods.find((period) => period.dateEquals(moratoriumPeriod))) distinctPeriods.push(moratoriumPeriod);
      });
    });

    await this.setValue(distinctPeriods);
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('startDate', 'endDate');
  }
}
