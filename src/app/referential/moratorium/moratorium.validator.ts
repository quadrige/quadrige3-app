import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import {
  lengthComment,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { toBoolean } from '@sumaris-net/ngx-components';
import { Moratorium } from '@app/referential/moratorium/moratorium.model';
import { MoratoriumService } from '@app/referential/moratorium/moratorium.service';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class MoratoriumValidatorService extends ReferentialValidatorService<Moratorium> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: MoratoriumService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Moratorium, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      programId: [data?.programId || null, Validators.required],
      id: [data?.id || null],
      description: [
        data?.description || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthComment)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data, 'description'),
      ],
      global: [toBoolean(data?.global, true)],
      periods: [data?.periods || null],
      pmfmus: [data?.pmfmus || null],
      locationIds: [data?.locationIds || null],
      campaignIds: [data?.campaignIds || null],
      occasionIds: [data?.occasionIds || null],
      detailLoaded: [toBoolean(data?.detailLoaded, false)],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: Moratorium, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [
        BaseGroupValidators.requiredAllArraysNotEmpty(['periods'], 'REFERENTIAL.MORATORIUM.ERROR.MISSING_PERIOD'),
        BaseGroupValidators.requiredIfArrayEmpty('global', true, 'pmfmus', 'REFERENTIAL.MORATORIUM.ERROR.MISSING_PMFMU'),
      ],
    };
  }
}
