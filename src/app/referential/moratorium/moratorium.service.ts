import { Injectable, Injector } from '@angular/core';
import { isNil } from '@sumaris-net/ngx-components';
import { BaseReferentialService, ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { Moratorium } from '@app/referential/moratorium/moratorium.model';
import { MoratoriumPmfmu } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.model';
import { parameterFragments } from '@app/referential/parameter/parameter.service';
import { MoratoriumFilter, MoratoriumFilterCriteria } from '@app/referential/moratorium/filter/moratorium.filter.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { jobFragments } from '@app/social/job/job.service';

const moratoriumPeriodFragment = gql`
  fragment MoratoriumPeriodFragment on MoratoriumPeriodVO {
    id
    moratoriumId
    startDate
    endDate
    updateDate
    __typename
  }
`;

const moratoriumPmfmuFragment = gql`
  fragment MoratoriumPmfmuFragment on MoratoriumPmfmuVO {
    id
    moratoriumId
    pmfmuId
    parameter {
      ...ParameterFragment
    }
    matrix {
      ...ReferentialFragment
    }
    fraction {
      ...ReferentialFragment
    }
    method {
      ...ReferentialFragment
    }
    unit {
      ...ReferentialFragment
    }
    updateDate
    __typename
  }
  ${parameterFragments.parameter}
  ${referentialFragments.light}
`;

// fragment used to load moratorium
const moratoriumFragment = gql`
  fragment MoratoriumFragment on MoratoriumVO {
    id
    programId
    description
    global
    periods {
      ...MoratoriumPeriodFragment
    }
    creationDate
    updateDate
    __typename
  }
  ${moratoriumPeriodFragment}
`;

// fragment used for saving moratorium
const fullMoratoriumFragment = gql`
  fragment FullMoratoriumFragment on MoratoriumVO {
    id
    programId
    description
    global
    periods {
      ...MoratoriumPeriodFragment
    }
    pmfmus {
      ...MoratoriumPmfmuFragment
    }
    locationIds
    campaignIds
    occasionIds
    creationDate
    updateDate
    __typename
  }
  ${moratoriumPeriodFragment}
  ${moratoriumPmfmuFragment}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Moratoriums($page: PageInput, $filter: MoratoriumFilterVOInput) {
      data: moratoriums(page: $page, filter: $filter) {
        ...MoratoriumFragment
      }
    }
    ${moratoriumFragment}
  `,

  loadAllWithTotal: gql`
    query MoratoriumsWithTotal($page: PageInput, $filter: MoratoriumFilterVOInput) {
      data: moratoriums(page: $page, filter: $filter) {
        ...MoratoriumFragment
      }
      total: moratoriumsCount(filter: $filter)
    }
    ${moratoriumFragment}
  `,

  countAll: gql`
    query MoratoriumsCount($filter: MoratoriumFilterVOInput) {
      total: moratoriumsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportMoratoriumsAsync($filter: MoratoriumFilterVOInput, $context: ExportContextInput) {
      data: exportMoratoriumsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveMoratoriums($data: [MoratoriumVOInput], $options: MoratoriumSaveOptionsInput) {
      data: saveMoratoriums(moratoriums: $data, options: $options) {
        ...FullMoratoriumFragment
      }
    }
    ${fullMoratoriumFragment}
  `,

  deleteAll: gql`
    mutation DeleteMoratoriums($ids: [Int]) {
      deleteMoratoriums(ids: $ids)
    }
  `,
};

const otherQueries = {
  moratoriumPmfmus: gql`
    query MoratoriumPmfmus($moratoriumId: Int) {
      data: moratoriumPmfmus(moratoriumId: $moratoriumId) {
        ...MoratoriumPmfmuFragment
      }
    }
    ${moratoriumPmfmuFragment}
  `,

  moratoriumLocationIds: gql`
    query MoratoriumLocationIds($moratoriumId: Int) {
      data: moratoriumLocationIds(moratoriumId: $moratoriumId)
    }
  `,
  moratoriumCampaignIds: gql`
    query MoratoriumCampaignIss($moratoriumId: Int) {
      data: moratoriumCampaignIds(moratoriumId: $moratoriumId)
    }
  `,
  moratoriumOccasionIds: gql`
    query MoratoriumOccasionIds($moratoriumId: Int) {
      data: moratoriumOccasionIds(moratoriumId: $moratoriumId)
    }
  `,
  hasDataOnMultiProgram: gql`
    query HasDataOnMultiProgram(
      $programId: String
      $moratoriumPeriods: [MoratoriumPeriodVOInput]
      $moratoriumPmfmus: [MoratoriumPmfmuVOInput]
      $monitoringLocationIds: [Int]
      $campaignIds: [Int]
      $occasionIds: [Int]
    ) {
      data: hasDataOnMultiProgram(
        programId: $programId
        moratoriumPeriods: $moratoriumPeriods
        moratoriumPmfmus: $moratoriumPmfmus
        monitoringLocationIds: $monitoringLocationIds
        campaignIds: $campaignIds
        occasionIds: $occasionIds
      )
    }
  `,
};

const defaultSaveOptions = {
  withPmfmus: true,
  withLocations: true,
  withCampaigns: true,
  withOccasions: true,
};

@Injectable({ providedIn: 'root' })
export class MoratoriumService extends BaseReferentialService<Moratorium, MoratoriumFilter, MoratoriumFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Moratorium, MoratoriumFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[moratorium-service]';
  }

  equals(e1: Moratorium, e2: Moratorium): boolean {
    return e1 && e2 && (e1.id === e2.id || (e1.programId === e2.programId && e1.description === e2.description));
  }

  async save(entity: Moratorium, opts?: EntitySaveOptions): Promise<Moratorium> {
    return super.save(entity, { ...defaultSaveOptions, ...opts });
  }

  async saveAll(entities: Moratorium[], opts?: EntitySaveOptions): Promise<Moratorium[]> {
    return super.saveAll(entities, { ...defaultSaveOptions, ...opts });
  }

  async hasDataOnMultiProgram(programId: string, moratorium: Moratorium): Promise<boolean> {
    const variables: any = {
      programId,
      moratoriumPeriods: moratorium.periods?.map((value) => value.asObject()),
      moratoriumPmfmus: moratorium.pmfmus?.map((value) => EntityUtils.asMinifiedObject(value)),
      monitoringLocationIds: moratorium.locationIds,
      campaignIds: moratorium.campaignIds,
      occasionIds: moratorium.occasionIds,
    };
    console.info(`${this._logPrefix} Check has data on multi program...`, variables);
    const now = Date.now();
    const query = otherQueries.hasDataOnMultiProgram;
    const res = await this.graphql.query<{ data: boolean }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    console.info(`${this._logPrefix} Check has data on multi program in ${Date.now() - now}ms, result=`, data);
    return data;
  }

  async loadPmfmus(moratoriumId: number): Promise<MoratoriumPmfmu[]> {
    if (isNil(moratoriumId)) {
      return [];
    }
    const variables: any = { moratoriumId };
    if (this._debug) console.debug(`${this._logPrefix} Loading moratorium pmfmus...`, variables);
    const now = Date.now();
    const query = otherQueries.moratoriumPmfmus;
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(MoratoriumPmfmu.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} moratorium pmfmus loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async loadLocationIds(moratoriumId: number): Promise<number[]> {
    return this.loadChildrenReferentialIds(moratoriumId, otherQueries.moratoriumLocationIds);
  }

  async loadCampaignIds(moratoriumId: number): Promise<number[]> {
    return this.loadChildrenReferentialIds(moratoriumId, otherQueries.moratoriumCampaignIds);
  }

  async loadOccasionIds(moratoriumId: number): Promise<number[]> {
    return this.loadChildrenReferentialIds(moratoriumId, otherQueries.moratoriumOccasionIds);
  }

  protected async loadChildrenReferentialIds(moratoriumId: number, query: any): Promise<number[]> {
    if (isNil(moratoriumId)) {
      return [];
    }
    const variables: any = { moratoriumId };
    if (this._debug) console.debug(`${this._logPrefix} Loading moratorium locations...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[]; total?: number }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data || [];
    if (this._debug) console.debug(`${this._logPrefix} moratorium locations loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
