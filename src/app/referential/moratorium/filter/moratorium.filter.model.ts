import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import {
  IntReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { Moratorium } from '@app/referential/moratorium/moratorium.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { DateFilter } from '@app/shared/model/date-filter';

@EntityClass({ typename: 'MoratoriumFilterCriteriaVO' })
export class MoratoriumFilterCriteria extends ReferentialFilterCriteria<Moratorium, number> {
  static fromObject: (source: any, opts?: any) => MoratoriumFilterCriteria;

  programFilter: StrReferentialFilterCriteria = null;
  monitoringLocationFilter: IntReferentialFilterCriteria = null;
  onlyActive: boolean = null;
  global: boolean = null;
  dateFilter: DateFilter = null;

  constructor() {
    super(MoratoriumFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.onlyActive = source.onlyActive;
    this.global = source.global;
    this.dateFilter = DateFilter.fromObject(source.dateFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    target.onlyActive = this.onlyActive || false;
    target.dateFilter = this.dateFilter?.asObject();
    target.parentId = this.parentId; // = programId (mandatory)
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'programFilter') return !BaseFilterUtils.isCriteriaEmpty(this.programFilter, true);
    if (key === 'monitoringLocationFilter') return !BaseFilterUtils.isCriteriaEmpty(this.monitoringLocationFilter, true);
    if (key === 'onlyActive') return this.onlyActive === true;
    if (key === 'parentId') return false;
    if (key === 'dateFilter') return !this.dateFilter.isEmpty();
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'MoratoriumFilterVO' })
export class MoratoriumFilter extends ReferentialFilter<MoratoriumFilter, MoratoriumFilterCriteria, Moratorium> {
  static fromObject: (source: any, opts?: any) => MoratoriumFilter;

  constructor() {
    super(MoratoriumFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): MoratoriumFilterCriteria {
    return MoratoriumFilterCriteria.fromObject(source, opts);
  }
}
