import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { MoratoriumFilter, MoratoriumFilterCriteria } from '@app/referential/moratorium/filter/moratorium.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-moratorium-filter-form',
  templateUrl: './moratorium.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoratoriumFilterForm extends ReferentialCriteriaFormComponent<MoratoriumFilter, MoratoriumFilterCriteria> {
  @Input() withProgram: boolean;

  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idDescription;
  }

  protected criteriaToQueryParams(criteria: MoratoriumFilterCriteria): PartialRecord<keyof MoratoriumFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.onlyActive)) params.onlyActive = criteria.onlyActive;
    if (isNotNil(criteria.global)) params.global = criteria.global;
    this.subCriteriaToQueryParams(params, criteria, 'programFilter');
    this.subCriteriaToQueryParams(params, criteria, 'monitoringLocationFilter');
    this.dateCriteriaToQueryParams(params, criteria, 'dateFilter');
    return params;
  }

  protected readonly prompt = prompt;
}
