import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import { isEmptyArray, isNotEmptyArray, PromiseEvent, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS, toBoolean } from '@sumaris-net/ngx-components';
import { distinctUntilChanged } from 'rxjs/operators';
import { Moratorium, MoratoriumUtils } from '@app/referential/moratorium/moratorium.model';
import { MoratoriumService } from '@app/referential/moratorium/moratorium.service';
import { MoratoriumValidatorService } from '@app/referential/moratorium/moratorium.validator';
import { UntypedFormGroup } from '@angular/forms';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { Program } from '@app/referential/program-strategy/program/program.model';
import { MatTabNav } from '@angular/material/tabs';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { MoratoriumPeriodTable } from '@app/referential/moratorium/period/moratorium-period.table';
import { MoratoriumPmfmuTable } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.table';
import { Alerts } from '@app/shared/alerts';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { IMoratoriumSurveyModalOptions, MoratoriumSurveyModal } from '@app/referential/moratorium/survey/moratorium-survey.modal';
import { MoratoriumPmfmu } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.model';
import { entityId, EntityUtils } from '@app/shared/entity.utils';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { MoratoriumPeriod } from '@app/referential/moratorium/period/moratorium-period.model';
import { lengthComment, ReferentialValidatorOptions } from '@app/referential/service/referential-validator.service';
import { MoratoriumFilter, MoratoriumFilterCriteria } from '@app/referential/moratorium/filter/moratorium.filter.model';
import { SurveyFilter } from '@app/data/survey/filter/survey.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { ISelectCriteria } from '@app/shared/model/options.model';
import { ExportOptions } from '@app/shared/table/table.model';
import { accountConfigOptions } from '@app/core/config/account.config';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

type MoratoriumView = 'locationTable' | 'pmfmuTable' | 'campaignTable' | 'occasionTable';

@Component({
  selector: 'app-moratorium-table',
  templateUrl: './moratorium.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoratoriumTable
  extends ReferentialTable<Moratorium, number, MoratoriumFilter, MoratoriumFilterCriteria, MoratoriumValidatorService>
  implements OnInit, AfterViewInit
{
  periodTable = viewChild<MoratoriumPeriodTable>('periodTable');

  detailAreaTabs = viewChild<MatTabNav>('detailAreaTabs');
  locationTable = viewChild<GenericSelectTable>('locationTable');
  campaignTable = viewChild<GenericSelectTable>('campaignTable');
  occasionTable = viewChild<GenericSelectTable>('occasionTable');
  pmfmuTable = viewChild<MoratoriumPmfmuTable>('pmfmuTable');

  periodTableEnabled = false;

  readonly locationsMenuItem: IEntityMenuItem<Moratorium, MoratoriumView> = {
    title: 'REFERENTIAL.MORATORIUM.MONITORING_LOCATIONS',
    attribute: 'locationIds',
    view: 'locationTable',
  };
  readonly pmfmuMenuItem: IEntityMenuItem<Moratorium, MoratoriumView> = {
    title: 'REFERENTIAL.MORATORIUM.PMFMUS',
    attribute: 'pmfmus',
    view: 'pmfmuTable',
  };
  readonly campaignMenuItem: IEntityMenuItem<Moratorium, MoratoriumView> = {
    title: 'REFERENTIAL.MORATORIUM.CAMPAIGNS',
    attribute: 'campaignIds',
    view: 'campaignTable',
  };
  readonly occasionMenuItem: IEntityMenuItem<Moratorium, MoratoriumView> = {
    title: 'REFERENTIAL.MORATORIUM.OCCASIONS',
    attribute: 'occasionIds',
    view: 'occasionTable',
  };
  readonly detailAreaMenuItems: IEntityMenuItem<Moratorium, MoratoriumView>[] = [
    this.locationsMenuItem,
    this.campaignMenuItem,
    this.occasionMenuItem,
    this.pmfmuMenuItem,
  ];

  selectedDetailAreaMenuItem: IEntityMenuItem<Moratorium, MoratoriumView> = this.detailAreaMenuItems[0];
  detailAreaEnabled = false;
  defaultBackHref = '/management/Program';
  descriptionMaxLength = lengthComment;

  private readonly _programId: string;
  private _program: Program;

  constructor(
    protected injector: Injector,
    protected _entityService: MoratoriumService,
    protected validatorService: MoratoriumValidatorService,
    protected programService: ProgramService,
    private dateAdapter: MomentDateAdapter
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['programId', 'description', 'global', 'creationDate', 'updateDate']).concat(RESERVED_END_COLUMNS),
      Moratorium,
      _entityService,
      validatorService
    );

    // Get program id from route
    this._programId = this.route.snapshot.params.programId;

    this.i18nColumnPrefix = 'REFERENTIAL.MORATORIUM.';
    this.logPrefix = '[moratorium-table]';
    this.setShowColumn('statusId', false);
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: Moratorium = this.singleSelectedRow.currentData;
      return isNotEmptyArray(data.periods);
    }
    return false;
  }

  get programId(): string {
    return this._programId;
  }

  get referentialCriteriaWithProgram(): ISelectCriteria {
    return {
      ...this.defaultReferentialCriteria,
      parentId: this.programId,
    };
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(this.onBeforeDeleteRows.subscribe((event) => this.checkCanDeleteMoratoriums(event)));

    // Load program
    this.loadProgram();
  }

  protected loadProgram() {
    if (this.programId) {
      this.programService.load(this.programId).then((program) => {
        this._program = program;
        this.setShowColumn('programId', false);
        this.updatePermission();
      });
    } else {
      this.readOnly = true;
      this.setShowColumn('programId', true);
      this.updatePermission();
    }
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    // Listen 'global' property change
    this.registerSubscription(
      this.registerCellValueChanges('global')
        .pipe(distinctUntilChanged())
        .subscribe((value) => this.switchGlobal(value))
    );

    this.restoreBottomPanel(true, 50);
  }

  protected async initRightView() {
    await super.initRightView();
    this.rightPanelAllowed = true;
    this.restoreRightPanel(true, 25);
    this.markForCheck();
  }

  async onAfterSelectionChange(row?: AsyncTableElement<Moratorium>) {
    this.refreshButtons();
    await super.onAfterSelectionChange(row);
    await this.loadDetailArea(row);
  }

  updatePermission() {
    // Manage user rights :
    // By default, all user have edition write to allow further finer checks
    this.canEdit = !this.readOnly;
    // Refresh other controls
    this.refreshButtons();
    this.refreshTabs();

    this.markForCheck();
  }

  asFilter(source?: any): MoratoriumFilter {
    const filter = super.asFilter(source);
    // Force parentId
    filter.patch({ parentId: this.programId });
    return filter;
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<Moratorium>): Promise<boolean> {
    row = row || this.singleEditingRow;

    if (row) {
      // save current period table
      if (this.periodTable().dirty) {
        if (await this.periodTable().save()) {
          this.patchPeriods(this.periodTable().value, row);
        } else {
          return false;
        }
      }

      // confirm current detail table row
      if (!(await this.confirmDetail(row))) {
        return false;
      }
    }

    const confirmed = await super.confirmEditCreate(event, row);

    if (row?.validator?.getError('required') === 'REFERENTIAL.MORATORIUM.ERROR.MISSING_PMFMU') {
      this.selectedDetailAreaMenuItem = this.pmfmuMenuItem;
      this.refreshTabs();
    }

    return confirmed;
  }

  async confirmDetail(row?: AsyncTableElement<Moratorium>): Promise<boolean> {
    if (row) {
      if (this.pmfmuTable().dirty) {
        if (await this.pmfmuTable().save()) {
          this.patchPmfmus(this.pmfmuTable().value, row);
        } else {
          return false;
        }
      }
    }
    return true;
  }

  patchPeriods(values: MoratoriumPeriod[], row?: AsyncTableElement<Moratorium>) {
    if (this.canEdit) {
      this.patchRow('periods', values, row);
    }
  }

  patchLocations(values: any[], row?: AsyncTableElement<Moratorium>) {
    if (this.canEdit) {
      this.patchRow(this.locationsMenuItem.attribute, values, row);
    }
  }

  patchCampaigns(values: any[], row?: AsyncTableElement<Moratorium>) {
    if (this.canEdit) {
      this.patchRow(this.campaignMenuItem.attribute, values, row);
      this.updateOccasionTableFilter();
      this.updateOccasionTable();
    }
  }

  patchOccasions(values: any[], row?: AsyncTableElement<Moratorium>) {
    if (this.canEdit) {
      this.patchRow(this.occasionMenuItem.attribute, values, row);
    }
  }

  patchPmfmus(values: MoratoriumPmfmu[], row?: AsyncTableElement<Moratorium>) {
    if (this.canEdit) {
      this.patchRow(this.pmfmuMenuItem.attribute, values, row);
    }
  }

  async detailAreaTabChange(event: UIEvent, detailTab: IEntityMenuItem<Moratorium, MoratoriumView>) {
    if (!detailTab) {
      throw new Error(`Cannot determinate the type of bottom area`);
    }
    if (this.selectedDetailAreaMenuItem !== detailTab) {
      if (!(await this.confirmDetail(this.singleEditingRow))) {
        return;
      }
    }
    this.selectedDetailAreaMenuItem = detailTab;
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    let continueSave = true;

    // Save sub tables only if current row is dirty (Mantis #60707)
    if (this.singleEditingRow?.dirty) {
      {
        continueSave = await this.periodTable().save();
        if (continueSave) {
          this.patchPeriods(this.periodTable().value);
        } else {
          console.warn(`${this.logPrefix} period table not saved`);
        }
      }
      if (continueSave && this.locationTable().dirty) {
        continueSave = await this.locationTable().save();
        if (continueSave) {
          this.patchRow(this.locationsMenuItem.attribute, this.locationTable().selectedIds);
        } else {
          console.warn(`${this.logPrefix} location table not saved`);
          this.selectedDetailAreaMenuItem = this.locationsMenuItem;
          this.refreshTabs();
        }
      }
      if (continueSave && this.campaignTable().dirty) {
        continueSave = await this.campaignTable().save();
        if (continueSave) {
          this.patchRow(this.campaignMenuItem.attribute, this.campaignTable().selectedIds);
        } else {
          console.warn(`${this.logPrefix} campaign table not saved`);
          this.selectedDetailAreaMenuItem = this.campaignMenuItem;
          this.refreshTabs();
        }
      }
      if (continueSave && this.occasionTable().dirty) {
        continueSave = await this.occasionTable().save();
        if (continueSave) {
          this.patchRow(this.occasionMenuItem.attribute, this.occasionTable().selectedIds);
        } else {
          console.warn(`${this.logPrefix} occasion table not saved`);
          this.selectedDetailAreaMenuItem = this.occasionMenuItem;
          this.refreshTabs();
        }
      }
      if (continueSave && this.pmfmuTable().dirty) {
        continueSave = await this.pmfmuTable().save();
        if (continueSave) {
          this.patchRow(this.pmfmuMenuItem.attribute, this.pmfmuTable().value);
        } else {
          console.warn(`${this.logPrefix} pmfmu table not saved`);
          this.selectedDetailAreaMenuItem = this.pmfmuMenuItem;
          this.refreshTabs();
        }
      }
    }

    if (continueSave) {
      continueSave = await this.canSave();
    }

    if (continueSave) {
      return await super.save(opts);
    }
    return false;
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<Moratorium>): Promise<boolean> {
    // following lines comes from super.editRow(event, row), don't remove
    if (!this._enabled) return false;
    if (this.singleEditingRow === row) return true; // Already the edited row
    if (event?.defaultPrevented) return false;
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // User without right to edit, don't turn on row edition
    if (!this.hasRightOnMoratorium(row.currentData)) {
      return true;
    }

    return super.editRow(event, row);
  }

  async openSurveyModal(event: UIEvent) {
    event?.stopPropagation();
    if (!this.singleSelectedRow || !(await this.confirmEditCreate())) {
      return;
    }

    const moratorium = this.singleSelectedRow.currentData;
    // build survey filter
    const surveyFilter = SurveyFilter.fromObject({
      criterias: [
        {
          programFilter: {
            id: moratorium.programId,
          },
          multiProgramOnly: false, // todo will be true on save check
          dateFilters: moratorium.periods.map((period) => ({
            startLowerBound: period.startDate,
            endUpperBound: period.endDate,
          })),
          pmfmuFilters: moratorium.pmfmus?.map((pmfmu) => ({
            parameterFilter: { id: pmfmu.parameter.id },
            matrixFilter: { id: pmfmu.matrix?.id },
            fractionFilter: { id: pmfmu.fraction?.id },
            methodFilter: { id: pmfmu.method?.id },
            unitFilter: { id: pmfmu.unit?.id },
          })),
          monitoringLocationFilter: { includedIds: moratorium.locationIds },
          campaignFilter: { includedIds: moratorium.campaignIds },
          occasionFilter: { includedIds: moratorium.occasionIds },
        },
      ],
    });

    await this.modalService.openModal<IMoratoriumSurveyModalOptions, void>(MoratoriumSurveyModal, { surveyFilter }, 'modal-large');
  }

  detailCount(detailTab: IEntityMenuItem<Moratorium, MoratoriumView>) {
    switch (detailTab?.view) {
      case 'locationTable':
        return this.locationTable()?.totalRowCount || undefined;
      case 'campaignTable':
        return this.campaignTable()?.totalRowCount || undefined;
      case 'occasionTable':
        return this.occasionTable()?.totalRowCount || undefined;
      case 'pmfmuTable':
        return this.pmfmuTable()?.totalRowCount || undefined;
      default:
        return undefined;
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { forceAllColumns: true, ...opts });
  }

  // protected methods

  protected getFilterSettingId(): string {
    return super.getFilterSettingId() + (this.programId ? `_${this.programId}` : '');
  }

  protected equals(t1: Moratorium, t2: Moratorium): boolean {
    return super.equals(t1, t2) || (EntityUtils.equals(t1, t2, 'programId') && EntityUtils.equals(t1, t2, 'description'));
  }

  protected async loadRightArea(row?: AsyncTableElement<Moratorium>) {
    // multiple selection allowed
    if (this.selection.selected.length > 1) {
      if (this.debug) {
        console.debug(`${this.logPrefix} multiple applied strategy selected`);
      }
      await this.periodTable()?.setMoratoriums(this.selection.selected.map((selectedRow) => selectedRow.currentData));
      this.periodTableEnabled = this.canEdit;
      return;
    }

    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      await this.periodTable()?.setMoratorium(undefined);
      this.periodTableEnabled = false;
      return;
    }
    // Load selected periods
    if (this.periodTable()) {
      await this.periodTable().setMoratorium(row.currentData);
    } else {
      throw new Error(`the period table is not present in DOM`);
    }

    this.periodTableEnabled = this.hasRightOnMoratorium(row.currentData);
  }

  protected async loadDetailArea(row?: AsyncTableElement<Moratorium>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.detailAreaEnabled = false;
      return;
    }

    // Load details data
    await this.loadDetailData(row);

    // Register sub tables
    this.registerSubForm(this.periodTable());
    this.registerSubForm(this.locationTable());
    this.registerSubForm(this.campaignTable());
    this.registerSubForm(this.occasionTable());
    this.registerSubForm(this.pmfmuTable());

    // Load selected data
    const global = row.currentData.global;
    await this.locationTable().setSelectedIds(global ? undefined : (row.currentData?.locationIds || []).slice());
    await this.campaignTable().setSelectedIds(global ? undefined : (row.currentData?.campaignIds || []).slice());
    await this.occasionTable().setSelectedIds(global ? undefined : (row.currentData?.occasionIds || []).slice());
    this.updateOccasionTableFilter();
    await this.pmfmuTable().setValue(global ? undefined : (row.currentData?.pmfmus || []).slice());
    this.detailAreaEnabled = this.hasRightOnMoratorium(row.currentData) && !global;
  }

  protected async loadDetailData(row: AsyncTableElement<Moratorium>) {
    // Call service if detail data is not loaded yet
    if (!row.currentData.detailLoaded) {
      this.locationTable().markAsLoading();
      this.campaignTable().markAsLoading();
      this.occasionTable().markAsLoading();
      this.pmfmuTable().markAsLoading();

      // Get applied strategies and pmfmu strategies
      const [locationIds, campaignIds, occasionIds, pmfmus] = await Promise.all([
        this._entityService.loadLocationIds(row.currentData.id),
        this._entityService.loadCampaignIds(row.currentData.id),
        this._entityService.loadOccasionIds(row.currentData.id),
        this._entityService.loadPmfmus(row.currentData.id),
      ]);

      row.validator.patchValue({ locationIds, campaignIds, occasionIds, pmfmus, detailLoaded: true });
    }
  }

  protected getRowValidator(row: AsyncTableElement<Moratorium>): UntypedFormGroup {
    return super.getRowValidator(row, <ReferentialValidatorOptions>{
      criteria: { parentId: this.programId },
    });
  }

  protected async defaultNewRowValue() {
    return {
      ...(await super.defaultNewRowValue()),
      programId: this.programId,
    };
  }

  protected hasRightOnProgram(): boolean {
    if (!this._program) {
      return false;
    }

    // Admins or users with manager right
    return (
      this.accountService.isAdmin() ||
      (this.accountService.isUser() &&
        (this._program.managerUserIds.includes(this.accountService.person.id) ||
          this._program.managerDepartmentIds.includes(this.accountService.department.id)))
    );
  }

  protected hasRightOnMoratorium(moratorium: Moratorium): boolean {
    if (!moratorium || this.readOnly) {
      return false;
    }

    // Admins or program managers
    return this.accountService.isAdmin() || this.hasRightOnProgram();
  }

  protected async canSave(): Promise<boolean> {
    // Get dirty moratoriums to save
    const moratoriums: Moratorium[] = (this.getRows() || []).filter((row) => row.validator.dirty).map((row) => row.currentData);
    if (isEmptyArray(moratoriums)) {
      return true;
    }

    // Check if this moratorium has data on multi-program
    try {
      this.markAsLoading();
      for (const moratorium of moratoriums) {
        const hasData = await this._entityService.hasDataOnMultiProgram(this.programId, moratorium);
        if (hasData) {
          await Alerts.showError('REFERENTIAL.MORATORIUM.ERROR.HAS_DATA_ON_MULTI_PROGRAM', this.alertCtrl, this.translate, undefined, {
            name: moratorium.description,
          });
          return false;
        }
      }
    } finally {
      this.markAsLoaded();
    }
    return true;
  }

  /**
   * Refresh buttons state according to user profile or program rights
   *
   * @protected
   */
  protected refreshButtons() {
    // Only admins or program managers can add moratoriums
    this.tableButtons.canAdd = this.hasRightOnProgram();
    // this.tableButtons.canDuplicate = this.hasRightOnProgram(); todo duplication not implemented yet
    this.tableButtons.canDelete =
      (this.selection.hasValue() && this.selection.selected.every((row) => this.hasRightOnMoratorium(row.currentData))) || false;

    if (!this.programId) {
      this.defaultBackHref = undefined;
      this.canGoBack = false;
    }

    // Update default referential filter (Mantis #65560)
    this.showDisabledReferential =
      this.settings.getPropertyAsBoolean(accountConfigOptions.showDisabledReferential) &&
      (this.accountService.isAdmin() || this.hasRightOnProgram() || this.hasRightOnMoratorium(this.singleEditingRow?.currentData));
  }

  protected refreshTabs() {
    setTimeout(() => {
      this.detailAreaTabs()?.updatePagination();
      this.detailAreaTabs()?._alignInkBarToSelectedTab();
    });
  }

  protected updateTitle() {
    this.title = !!this.programId
      ? ['MENU.REFERENTIAL.MANAGEMENT', 'REFERENTIAL.PROGRAM.TITLE', 'REFERENTIAL.PROGRAM.MORATORIUMS']
          .map((value) => this.translate.instant(value, { programId: this.programId }))
          .join(' &rArr; ')
      : ['MENU.REFERENTIAL.MANAGEMENT', 'REFERENTIAL.PROGRAM.MORATORIUMS'].map((value) => this.translate.instant(value)).join(' &rArr; ');
  }

  protected getRequiredColumns(): string[] {
    const requiredColumns = super.getRequiredColumns().slice();
    if (!this.programId) {
      requiredColumns.push('programId');
    }
    return requiredColumns.concat('description', 'global');
  }

  /**
   * Ask confirmation on 'global' or 'partial'
   *
   * @param global
   * @private
   */
  private async switchGlobal(global: boolean) {
    const editingRow = this.singleEditingRow;
    if (!editingRow || !editingRow.currentData) return;
    const moratorium = editingRow.currentData;
    if (
      global &&
      (isNotEmptyArray(moratorium.locationIds) ||
        isNotEmptyArray(moratorium.campaignIds) ||
        isNotEmptyArray(moratorium.occasionIds) ||
        isNotEmptyArray(moratorium.pmfmus))
    ) {
      // Ask confirmation to remove related data
      const confirmed = await Alerts.askConfirmation('REFERENTIAL.MORATORIUM.CONFIRM.RESET_CHILDREN', this.alertCtrl, this.translate);
      if (confirmed) {
        editingRow.validator.patchValue({
          locationIds: [],
          campaignIds: [],
          occasionIds: [],
          pmfmus: [],
        });
      } else {
        // reset global
        editingRow.validator.patchValue({ global: false });
        return; // prevent reloading twice
      }
    }

    // refresh (active sub tables if partial)
    await this.loadDetailArea(editingRow);
  }

  private updateOccasionTableFilter() {
    const campaignIds = this.campaignTable().selectedIds;
    this.occasionTable().addCriteria = { parentFilter: { includedIds: isNotEmptyArray(campaignIds) ? campaignIds : [-1] } };
  }

  /**
   * Update occasion table content: add missing occasions of added campaigns, remove occasions of removed campaigns
   *
   * @private
   */
  private async updateOccasionTable() {
    const campaignIds = this.campaignTable().selectedIds;
    const targetOccasionIds = [];
    const remainingOccasionIds = this.occasionTable().selectedIds?.slice() || [];
    // get all occasions
    for (const campaignId of campaignIds) {
      const occasions = await this.referentialGenericService.loadPage(
        {}, // no page
        GenericReferentialFilter.fromObject({
          entityName: 'Occasion',
          criterias: [{ parentId: campaignId }],
        })
      );
      const occasionIds = occasions?.data?.map(entityId) || [];
      // find any
      if (remainingOccasionIds.some((occasionId) => occasionIds.includes(occasionId.toString()))) {
        // if some found, copy them into target
        targetOccasionIds.push(...remainingOccasionIds.filter((occasionId) => occasionIds.includes(occasionId.toString())));
      } else {
        // if any present, add all
        targetOccasionIds.push(...occasionIds);
      }
    }
    // replace occasion ids
    await this.occasionTable().setSelectedIds(targetOccasionIds);
  }

  private async checkCanDeleteMoratoriums(event: PromiseEvent<boolean, { rows: AsyncTableElement<Moratorium>[] }>) {
    let canDelete = true;
    // Get active moratorium
    const activeMoratoriums =
      event.detail.rows?.map((row) => row.currentData).filter((moratorium) => MoratoriumUtils.isActive(moratorium.periods)) || [];
    if (isNotEmptyArray(activeMoratoriums)) {
      const lastPeriod = activeMoratoriums[0].periods.reduce((last, period) => (period.endDate.isAfter(last?.endDate) ? period : last));
      const format = this.translate.instant('COMMON.DATE_PATTERN');
      // Ask confirmation
      const confirmed = await Alerts.askConfirmation('REFERENTIAL.MORATORIUM.CONFIRM.DELETE_ACTIVE', this.alertCtrl, this.translate, undefined, {
        date: this.dateAdapter.format(lastPeriod.endDate, format),
      });
      canDelete = toBoolean(confirmed, false);
    }
    event.detail.success(canDelete);
  }
}
