import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { MoratoriumTable } from '@app/referential/moratorium/moratorium.table';
import { MoratoriumModule } from '@app/referential/moratorium/moratorium.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MoratoriumTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
      pathIdParam: 'programId',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), MoratoriumModule],
})
export class MoratoriumRoutingModule {}
