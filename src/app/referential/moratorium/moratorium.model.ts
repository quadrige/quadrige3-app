import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { MoratoriumPeriod } from '@app/referential/moratorium/period/moratorium-period.model';
import { MoratoriumPmfmu } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.model';
import moment from 'moment';

@EntityClass({ typename: 'MoratoriumVO' })
export class Moratorium extends Referential<Moratorium> {
  static entityName = 'Moratorium';
  static fromObject: (source: any, opts?: any) => Moratorium;

  programId: string = null;
  global: boolean = null;
  periods: MoratoriumPeriod[] = null;
  pmfmus: MoratoriumPmfmu[] = null;
  locationIds: number[] = null;
  campaignIds: number[] = null;
  occasionIds: number[] = null;
  detailLoaded = false;

  constructor() {
    super(Moratorium.TYPENAME);
    this.entityName = Moratorium.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Moratorium.entityName;
    this.programId = source.programId;
    this.global = source.global;
    this.periods = source.periods?.map(MoratoriumPeriod.fromObject) || [];
    this.pmfmus = source.pmfmus?.map(MoratoriumPmfmu.fromObject) || [];
    this.locationIds = source.locationIds || [];
    this.campaignIds = source.campaignIds || [];
    this.occasionIds = source.occasionIds || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    target.periods = this.periods?.map((value) => value.asObject(opts));
    target.pmfmus = this.pmfmus?.map((value) => value.asObject(opts));
    target.locationIds = this.locationIds || undefined;
    target.campaignIds = this.campaignIds || undefined;
    target.occasionIds = this.occasionIds || undefined;
    delete target.detailLoaded;
    delete target.label;
    delete target.statusId;
    return target;
  }
}

export class MoratoriumUtils {
  static isActive(periods: MoratoriumPeriod[]): boolean {
    return periods?.some((period) => moment().isBetween(period.startDate, period.endDate, 'day', '[]')) || false;
  }
}
