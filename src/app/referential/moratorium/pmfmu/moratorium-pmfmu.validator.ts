import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import {
  ReferentialGroupAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { SharedValidators } from '@sumaris-net/ngx-components';
import { MoratoriumPmfmu } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.model';

@Injectable({ providedIn: 'root' })
export class MoratoriumPmfmuValidatorService extends ReferentialValidatorService<MoratoriumPmfmu> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: MoratoriumPmfmu, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      moratoriumId: [data?.moratoriumId || null],
      pmfmuId: [data?.pmfmuId || null],
      parameter: [data?.parameter || null, Validators.compose([Validators.required, SharedValidators.entity])],
      matrix: [data?.matrix || null, SharedValidators.entity],
      fraction: [data?.fraction || null, SharedValidators.entity],
      method: [data?.method || null, SharedValidators.entity],
      unit: [data?.unit || null, SharedValidators.entity],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: MoratoriumPmfmu, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      asyncValidators: ReferentialGroupAsyncValidators.checkAttributesAlreadyExists(
        undefined,
        this.dataSource,
        opts,
        data,
        ['parameter?.id', 'matrix?.id', 'fraction?.id', 'method?.id', 'unit?.id'],
        'REFERENTIAL.MORATORIUM_PMFMU.ERROR.ALREADY_EXISTS'
      ),
    };
  }
}
