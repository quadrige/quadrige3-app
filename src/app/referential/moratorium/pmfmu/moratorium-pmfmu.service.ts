import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { MoratoriumPmfmu } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.model';
import { MoratoriumPmfmuFilter } from '@app/referential/moratorium/pmfmu/filter/moratorium-pmfmu.filter.model';

@Injectable({ providedIn: 'root' })
export class MoratoriumPmfmuService extends EntitiesMemoryService<MoratoriumPmfmu, MoratoriumPmfmuFilter> {
  constructor() {
    super(MoratoriumPmfmu, MoratoriumPmfmuFilter);
  }
}
