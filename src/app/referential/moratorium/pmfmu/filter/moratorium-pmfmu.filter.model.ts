import { EntityAsObjectOptions, FilterFn } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { MoratoriumPmfmu } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.model';
import { ParameterFilterCriteria } from '@app/referential/parameter/filter/parameter.filter.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

export class MoratoriumPmfmuFilterCriteria extends ReferentialFilterCriteria<Referential<any>, number> {
  parameterFilter: ParameterFilterCriteria = null;
  matrixFilter: IntReferentialFilterCriteria = null;
  fractionFilter: IntReferentialFilterCriteria = null;
  methodFilter: IntReferentialFilterCriteria = null;
  unitFilter: IntReferentialFilterCriteria = null;

  static fromObject(source: any, opts?: any): MoratoriumPmfmuFilterCriteria {
    const target = new MoratoriumPmfmuFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.parameterFilter = ParameterFilterCriteria.fromObject(source.parameterFilter || {});
    this.matrixFilter = IntReferentialFilterCriteria.fromObject(source.matrixFilter || {});
    this.fractionFilter = IntReferentialFilterCriteria.fromObject(source.fractionFilter || {});
    this.methodFilter = IntReferentialFilterCriteria.fromObject(source.methodFilter || {});
    this.unitFilter = IntReferentialFilterCriteria.fromObject(source.unitFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parameterFilter = this.parameterFilter?.asObject(opts);
    target.matrixFilter = this.matrixFilter?.asObject(opts);
    target.fractionFilter = this.fractionFilter?.asObject(opts);
    target.methodFilter = this.methodFilter?.asObject(opts);
    target.unitFilter = this.unitFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'parameterFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parameterFilter, true);
    if (key === 'matrixFilter') return !BaseFilterUtils.isCriteriaEmpty(this.matrixFilter, true);
    if (key === 'fractionFilter') return !BaseFilterUtils.isCriteriaEmpty(this.fractionFilter, true);
    if (key === 'methodFilter') return !BaseFilterUtils.isCriteriaEmpty(this.methodFilter, true);
    if (key === 'unitFilter') return !BaseFilterUtils.isCriteriaEmpty(this.unitFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  // todo use buildCriteria
  protected buildFilter(): FilterFn<MoratoriumPmfmu>[] {
    const target: FilterFn<MoratoriumPmfmu>[] = super.buildFilter();

    if (this.parameterFilter) {
      const filter = this.parameterFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.parameter));
    }
    if (this.matrixFilter) {
      const filter = this.matrixFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.matrix));
    }
    if (this.fractionFilter) {
      const filter = this.fractionFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.fraction));
    }
    if (this.methodFilter) {
      this.methodFilter.searchAttributes = ['id', 'name', 'description'];
      const filter = this.methodFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.method));
    }
    if (this.unitFilter) {
      const filter = this.unitFilter.asFilterFn();
      if (filter) target.push((data) => filter(data.unit));
    }

    return target;
  }
}

export class MoratoriumPmfmuFilter extends ReferentialFilter<MoratoriumPmfmuFilter, MoratoriumPmfmuFilterCriteria, MoratoriumPmfmu> {
  static fromObject(source: any, opts?: any): MoratoriumPmfmuFilter {
    const target = new MoratoriumPmfmuFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): MoratoriumPmfmuFilterCriteria {
    return MoratoriumPmfmuFilterCriteria.fromObject(source, opts);
  }
}
