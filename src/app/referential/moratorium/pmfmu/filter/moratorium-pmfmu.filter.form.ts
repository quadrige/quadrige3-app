import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { MoratoriumPmfmuFilter, MoratoriumPmfmuFilterCriteria } from '@app/referential/moratorium/pmfmu/filter/moratorium-pmfmu.filter.model';

@Component({
  selector: 'app-moratorium-pmfmu-filter-form',
  templateUrl: './moratorium-pmfmu.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoratoriumPmfmuFilterForm extends ReferentialCriteriaFormComponent<MoratoriumPmfmuFilter, MoratoriumPmfmuFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected criteriaToQueryParams(criteria: MoratoriumPmfmuFilterCriteria): PartialRecord<keyof MoratoriumPmfmuFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'parameterFilter');
    this.subCriteriaToQueryParams(params, criteria, 'matrixFilter');
    this.subCriteriaToQueryParams(params, criteria, 'fractionFilter');
    this.subCriteriaToQueryParams(params, criteria, 'methodFilter');
    this.subCriteriaToQueryParams(params, criteria, 'unitFilter');
    return params;
  }
}
