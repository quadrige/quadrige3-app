import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { MoratoriumPmfmu, MoratoriumPmfmuUtils } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.model';
import { MoratoriumPmfmuValidatorService } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.validator';
import { MoratoriumPmfmuService } from '@app/referential/moratorium/pmfmu/moratorium-pmfmu.service';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { IParameterAddOptions } from '@app/referential/parameter/parameter.add.table';
import { IAddResult } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { IPmfmuAddOptions } from '@app/referential/pmfmu/pmfmu.add.table';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { uniqueValue } from '@app/shared/utils';
import { MoratoriumPmfmuFilter, MoratoriumPmfmuFilterCriteria } from '@app/referential/moratorium/pmfmu/filter/moratorium-pmfmu.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';

@Component({
  selector: 'app-moratorium-pmfmu-table',
  templateUrl: './moratorium-pmfmu.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoratoriumPmfmuTable
  extends ReferentialMemoryTable<MoratoriumPmfmu, number, MoratoriumPmfmuFilter, MoratoriumPmfmuFilterCriteria, MoratoriumPmfmuValidatorService>
  implements OnInit
{
  @Input() programId: string;

  constructor(
    protected injector: Injector,
    protected _entityService: MoratoriumPmfmuService,
    protected validatorService: MoratoriumPmfmuValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['pmfmuId', 'parameter', 'matrix', 'fraction', 'method', 'unit', 'updateDate']).concat(RESERVED_END_COLUMNS),
      MoratoriumPmfmu,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.MORATORIUM.PMFMU';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.MORATORIUM_PMFMU.';
    this.logPrefix = '[moratorium-pmfmu-table]';
  }

  ngOnInit() {
    this.subTable = true;

    super.ngOnInit();

    // Parameter combo
    this.registerAutocompleteField('parameter', {
      ...this.referentialOptions.parameter,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Parameter',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Matrix combo
    this.registerAutocompleteField('matrix', {
      ...this.referentialOptions.matrix,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Matrix',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
    });

    // Fraction combo
    this.registerAutocompleteField('fraction', {
      ...this.referentialOptions.fraction,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Fraction',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
    });

    // Method combo
    this.registerAutocompleteField('method', {
      ...this.referentialOptions.method,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Method',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Unit combo
    this.registerAutocompleteField('unit', {
      ...this.referentialOptions.unit,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Unit',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    this.registerSubscription(
      this.registerCellValueChanges('matrix').subscribe((matrix) => (this.autocompleteFields.fraction.filter.criterias[0].parentId = matrix?.id))
    );

    this.registerSubscription(
      this.registerCellValueChanges('fraction').subscribe((fraction) => (this.autocompleteFields.matrix.filter.criterias[0].parentId = fraction?.id))
    );
  }

  async openAddPmfmuModal(event: MouseEvent) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IPmfmuAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: Pmfmu.entityName,
        titleI18n: 'REFERENTIAL.MORATORIUM_PMFMU.ADD_PMFMU',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          programFilter: { includedIds: [this.programId] },
          // parameterFilter: { excludedIds: this.existingParameterIds() }, // Filter removed because not consistent (Mantis #61215)
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      await this.save();
      const newValue = this.value?.slice() || [];
      const toAdd = data.added
        .map(Pmfmu.fromObject)
        .map((pmfmu: Pmfmu) => new MoratoriumPmfmu(pmfmu.parameter, pmfmu.matrix, pmfmu.fraction, pmfmu.method, pmfmu.unit, pmfmu.id));
      // Set new values (excluding already present PMFMU)
      newValue.push(...toAdd.filter((valueToAdd) => !newValue.find((value) => MoratoriumPmfmuUtils.samePmfmu(value, valueToAdd))));
      await this.setValue(newValue);
      this.markAsDirty();
    }
  }

  async openAddParameterModal(event: MouseEvent) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IParameterAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: Parameter.entityName,
        titleI18n: 'REFERENTIAL.MORATORIUM_PMFMU.ADD_PARAMETER',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          programFilter: { includedIds: [this.programId] },
          excludedIds: this.existingParameterIds(),
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      await this.save();
      const newValue = this.value?.slice() || [];
      const toAdd = data.added.map(Parameter.fromObject).map((parameter) => new MoratoriumPmfmu(parameter));
      // Set new values (excluding already present (alone) parameter
      newValue.push(...toAdd.filter((valueToAdd) => !newValue.find((value) => MoratoriumPmfmuUtils.samePmfmu(value, valueToAdd))));
      await this.setValue(newValue);
      this.markAsDirty();
    }
  }

  // protected methods

  protected existingParameterIds(): string[] {
    return this.value.map((value) => value.parameter.id.toString()).filter(uniqueValue);
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('pmfmuId', 'parameter');
  }
}
