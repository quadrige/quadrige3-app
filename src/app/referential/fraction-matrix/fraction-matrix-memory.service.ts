import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { FractionMatrix, FractionMatrixUtils } from '@app/referential/fraction-matrix/fraction-matrix.model';
import { FractionMatrixFilter } from '@app/referential/fraction-matrix/filter/fraction-matrix.filter.model';

@Injectable({ providedIn: 'root' })
export class FractionMatrixMemoryService extends EntitiesMemoryService<FractionMatrix, FractionMatrixFilter> {
  constructor() {
    super(FractionMatrix, FractionMatrixFilter, { equals: FractionMatrixUtils.equals });
  }
}
