import { EntityClass, FilterFn } from '@sumaris-net/ngx-components';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { FractionMatrix } from '@app/referential/fraction-matrix/fraction-matrix.model';

@EntityClass({ typename: 'FractionMatrixFilterCriteriaVO' })
export class FractionMatrixFilterCriteria extends ReferentialFilterCriteria<FractionMatrix, number> {
  static fromObject: (source: any, opts?: any) => FractionMatrixFilterCriteria;

  constructor() {
    super(FractionMatrixFilterCriteria.TYPENAME);
  }

  protected buildFilter(): FilterFn<FractionMatrix>[] {
    const filterFns = [];

    const searchTextFilter = EntityUtils.searchTextFilter(['fraction.id', 'fraction.name', 'matrix.id', 'matrix.name'], this.searchText);
    if (searchTextFilter) filterFns.push(searchTextFilter);

    return filterFns;
  }
}

@EntityClass({ typename: 'FractionMatrixFilterVO' })
export class FractionMatrixFilter extends ReferentialFilter<FractionMatrixFilter, FractionMatrixFilterCriteria, FractionMatrix> {
  static fromObject: (source: any, opts?: any) => FractionMatrixFilter;

  constructor() {
    super(FractionMatrixFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts?: any): FractionMatrixFilterCriteria {
    return FractionMatrixFilterCriteria.fromObject(source, opts);
  }
}
