import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { ReferentialModule } from '@app/referential/referential.module';
import { FractionMatrixFilter, FractionMatrixFilterCriteria } from '@app/referential/fraction-matrix/filter/fraction-matrix.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-fraction-matrix-filter-form',
  templateUrl: './fraction-matrix.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FractionMatrixFilterForm extends ReferentialCriteriaFormComponent<FractionMatrixFilter, FractionMatrixFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }
}
