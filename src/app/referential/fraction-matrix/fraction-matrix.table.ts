import { AfterViewInit, ChangeDetectionStrategy, Component, inject, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS, uncapitalizeFirstLetter } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { UntypedFormGroup } from '@angular/forms';
import { IAddResult, ISelectCriteria, ISelectModalOptions } from '@app/shared/model/options.model';
import { ReferentialValidatorOptions } from '@app/referential/service/referential-validator.service';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { TranscribingItemRowTable } from '@app/referential/transcribing-item/transcribing-item-row.table';
import { TranscribingItemTypeService } from '@app/referential/transcribing-item-type/transcribing-item-type.service';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { ReferentialModule } from '@app/referential/referential.module';
import { FractionMatrixFilterForm } from '@app/referential/fraction-matrix/filter/fraction-matrix.filter.form';
import { FractionMatrix, FractionMatrixUtils } from '@app/referential/fraction-matrix/fraction-matrix.model';
import { FractionMatrixValidatorService } from '@app/referential/fraction-matrix/fraction-matrix.validator';
import { FractionMatrixMemoryService } from '@app/referential/fraction-matrix/fraction-matrix-memory.service';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { entitiesTableNeedingMoreWidth } from '@app/referential/model/referential.constants';
import { FractionMatrixFilter, FractionMatrixFilterCriteria } from '@app/referential/fraction-matrix/filter/fraction-matrix.filter.model';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

export type ChildEntityName = 'Fraction' | 'Matrix';

@Component({
  selector: 'app-fraction-matrix-table',
  templateUrl: './fraction-matrix.table.html',
  standalone: true,
  imports: [ReferentialModule, FractionMatrixFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FractionMatrixTable
  extends ReferentialMemoryTable<FractionMatrix, number, FractionMatrixFilter, FractionMatrixFilterCriteria, FractionMatrixValidatorService>
  implements OnInit, AfterViewInit
{
  @Input() transcribingItemEnabled = true;
  @ViewChild('transcribingItemTable') transcribingItemTable: TranscribingItemRowTable;
  protected transcribingItemTypeService = inject(TranscribingItemTypeService);

  @Input() parentId: number;
  @Input() childEntityName: ChildEntityName;
  @Input() selectCriteria: ISelectCriteria;

  transcribingMenuItems: IEntityMenuItem<FractionMatrix, TranscribingItemView>[] = [];

  constructor(
    protected injector: Injector,
    protected _entityService: FractionMatrixMemoryService,
    protected validatorService: FractionMatrixValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'fraction.id',
        'fraction.name',
        'fraction.description',
        'fraction.comments',
        'fraction.statusId',
        'fraction.creationDate',
        'fraction.updateDate',
        'matrix.id',
        'matrix.name',
        'matrix.description',
        'matrix.comments',
        'matrix.statusId',
        'matrix.creationDate',
        'matrix.updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      FractionMatrix,
      _entityService,
      validatorService
    );

    this.logPrefix = '[fraction-matrix-table]';
  }

  get childProperty(): string {
    return uncapitalizeFirstLetter(this.childEntityName);
  }

  ngOnInit() {
    if (!this.childEntityName) throw new Error('No child entity name');

    this.subTable = true;
    this.inlineEdition = false;
    this.forceNoInlineEdition = true;
    super.ngOnInit();

    // Initialize right menu
    if (this.transcribingItemEnabled) {
      this.transcribingItemTypeService.initMenus(this.entityName).then((transcribingMenuItems) => {
        this.transcribingMenuItems = transcribingMenuItems;
        this.rightMenuItems = this.transcribingMenuItems;
        this.rightMenuItem = this.transcribingMenuItems?.[0]?.children?.[0];
        this.restoreRightPanel(true, 50);
        this.markForCheck();
      });
    }

    // Show/hide columns
    this.setShowColumn('id', this.childEntityName === 'Fraction');
    this.setShowColumn('fraction.id', this.childEntityName === 'Fraction');
    this.setShowColumn('fraction.name', this.childEntityName === 'Fraction');
    this.setShowColumn('fraction.description', this.childEntityName === 'Fraction');
    this.setShowColumn('fraction.comments', this.childEntityName === 'Fraction');
    this.setShowColumn('fraction.statusId', this.childEntityName === 'Fraction');
    this.setShowColumn('fraction.creationDate', this.childEntityName === 'Fraction');
    this.setShowColumn('fraction.updateDate', this.childEntityName === 'Fraction');
    this.setShowColumn('matrix.id', this.childEntityName === 'Matrix');
    this.setShowColumn('matrix.name', this.childEntityName === 'Matrix');
    this.setShowColumn('matrix.description', this.childEntityName === 'Matrix');
    this.setShowColumn('matrix.comments', this.childEntityName === 'Matrix');
    this.setShowColumn('matrix.statusId', this.childEntityName === 'Matrix');
    this.setShowColumn('matrix.creationDate', this.childEntityName === 'Matrix');
    this.setShowColumn('matrix.updateDate', this.childEntityName === 'Matrix');
    if (this.childEntityName === 'Fraction') this.defaultSortBy = 'fraction.name';
    if (this.childEntityName === 'Matrix') this.defaultSortBy = 'matrix.name';
  }

  async addRow(event?: Event, insertAt?: number, opts?: { focusColumn?: string; editing?: boolean }): Promise<boolean> {
    event?.preventDefault();
    this.selection.clear();
    const { role, data } = await this.modalService.openModal<ISelectModalOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: this.childEntityName,
        addCriteria: { ...this.defaultReferentialCriteria, excludedIds: this.value.map((value) => value[this.childProperty].id) || [] },
        titleI18n: this.titleI18n,
        showMode: false,
      },
      entitiesTableNeedingMoreWidth.includes(this.childEntityName) ? 'modal-large' : 'modal-medium'
    );

    // Add new values
    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      const newData = data.added.map((value) => {
        return FractionMatrix.fromObject(
          this.childEntityName === 'Fraction'
            ? {
                matrixId: this.parentId,
                fractionId: value.id,
                fraction: value,
              }
            : {
                fractionId: this.parentId,
                matrixId: value.id,
                matrix: value,
              }
        );
      });
      // Update datasource
      await this.setValue(this.value.slice().concat(newData));
      this.markAsDirty();
    }
    return false;
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<FractionMatrix>): Promise<boolean> {
    row = row || this.singleEditingRow || this.singleSelectedRow;

    // Save transcribing items table first
    if (this.transcribingItemEnabled && !(await this.transcribingItemTable.save())) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    // Save transcribing items table first
    let continueSave = true;
    if (this.transcribingItemEnabled) {
      continueSave = await this.transcribingItemTable.save();
    }

    if (continueSave) {
      return super.save(opts);
    }
    return false;
  }

  protected async canRightMenuItemChange(
    previousMenuItem: IEntityMenuItem<FractionMatrix, TranscribingItemView>,
    nextMenuItem: IEntityMenuItem<FractionMatrix, TranscribingItemView>
  ): Promise<boolean> {
    if (this.transcribingItemEnabled && previousMenuItem.view === 'transcribingItemTable' && this.transcribingItemTable?.dirty) {
      if (!(await this.transcribingItemTable.save())) {
        return false;
      }
    }
    return true;
  }

  protected async loadRightArea() {
    if (!this.transcribingItemEnabled) return;
    await this.transcribingItemTypeService.ready();
    if (this.rightMenuItem?.view === 'transcribingItemTable') {
      if (!this.transcribingItemTable) {
        this.cd.detectChanges();
      }
      if (this.transcribingItemTable) {
        this.registerSubForm(this.transcribingItemTable);
      } else {
        console.error(`${this.logPrefix} Transcribing item table not detected. Please check the template.`);
        return;
      }
      console.debug(`${this.logPrefix} load transcribing item table`);
      await this.transcribingItemTable.setSelection(this.selection.selected, this.rightMenuItem?.params);
      this.transcribingItemTable.updateTitle();
      this.transcribingItemTable.canEdit = this.canEdit;
    }
  }

  protected getRowValidator(row?: AsyncTableElement<FractionMatrix>): UntypedFormGroup {
    return super.getRowValidator(row, <ReferentialValidatorOptions>{
      criteria: this.selectCriteria,
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('fraction.id', 'fraction.name', 'matrix.id', 'matrix.name');
  }

  protected equals(t1: FractionMatrix, t2: FractionMatrix): boolean {
    return FractionMatrixUtils.equals(t1, t2);
  }
}
