import { EntityClass, ReferentialAsObjectOptions } from '@sumaris-net/ngx-components';
import { IntReferential, Referential } from '@app/referential/model/referential.model';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';
import { EntityUtils } from '@app/shared/entity.utils';

export const fractionMatrixFragment = gql`
  fragment FractionMatrixFragment on FractionMatrixVO {
    id
    fractionId
    fraction {
      ...FullReferentialFragment
    }
    matrixId
    matrix {
      ...FullReferentialFragment
    }
  }
  ${referentialFragments.full}
`;

@EntityClass({ typename: 'FractionMatrixVO' })
export class FractionMatrix extends Referential<FractionMatrix> {
  static entityName = 'FractionMatrix';
  static fromObject: (source: any, opts?: any) => FractionMatrix;

  fractionId: number = null;
  fraction: IntReferential = null;
  matrixId: number = null;
  matrix: IntReferential = null;

  constructor() {
    super(FractionMatrix.TYPENAME);
    this.entityName = FractionMatrix.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = FractionMatrix.entityName;
    this.fraction = IntReferential.fromObject(source.fraction);
    this.fractionId = this.fraction?.id || source.fractionId;
    this.matrix = IntReferential.fromObject(source.matrix);
    this.matrixId = this.matrix?.id || source.matrixId;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    if (opts?.minify) {
      target.fractionId = this.fractionId || this.fraction?.id;
      delete target.fraction;
      target.matrixId = this.matrixId || this.matrix?.id;
      delete target.matrix;
    }
    target.transcribingItems = this.transcribingItems?.map((value) => value.asObject());
    target.transcribingItemsLoaded = this.transcribingItemsLoaded;
    return target;
  }
}

export class FractionMatrixUtils {
  static equals(fm1: FractionMatrix, fm2: FractionMatrix): boolean {
    return EntityUtils.equals(fm1, fm2, 'id') || (EntityUtils.equals(fm1, fm2, 'fractionId') && EntityUtils.equals(fm1, fm2, 'matrixId'));
  }
}
