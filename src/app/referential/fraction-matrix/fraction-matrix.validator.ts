import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { toBoolean } from '@sumaris-net/ngx-components';
import { ReferentialValidatorOptions, ReferentialValidatorService } from '@app/referential/service/referential-validator.service';
import { FractionMatrix } from '@app/referential/fraction-matrix/fraction-matrix.model';

@Injectable({ providedIn: 'root' })
export class FractionMatrixValidatorService extends ReferentialValidatorService<FractionMatrix> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: FractionMatrix, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      fractionId: [data?.fractionId || null],
      fraction: [data?.fraction || null],
      matrixId: [data?.matrixId || null],
      matrix: [data?.matrix || null],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
