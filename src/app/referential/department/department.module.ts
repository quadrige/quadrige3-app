import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { DepartmentTable } from '@app/referential/department/department.table';
import { DepartmentLdapTable } from '@app/referential/department/ldap/department-ldap.table';
import { DepartmentLdapAddModal } from '@app/referential/department/ldap/department-ldap.add.modal';
import { DepartmentLdapFilterForm } from '@app/referential/department/ldap/filter/department-ldap.filter.form';
import { DepartmentFilterForm } from '@app/referential/department/filter/department.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { ProgramRightTable } from '@app/referential/right/program/program-right.table';
import { MetaProgramRightTable } from '@app/referential/right/meta-program/meta-program-right.table';
import { RuleListRightTable } from '@app/referential/right/rule-list/rule-list-right.table';

@NgModule({
  imports: [ReferentialModule, DepartmentFilterForm, GenericSelectTable, ProgramRightTable, MetaProgramRightTable, RuleListRightTable],
  declarations: [DepartmentTable, DepartmentLdapTable, DepartmentLdapFilterForm, DepartmentLdapAddModal],
  exports: [DepartmentTable],
})
export class DepartmentModule {}
