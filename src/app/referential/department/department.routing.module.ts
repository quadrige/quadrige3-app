import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { DepartmentTable } from '@app/referential/department/department.table';
import { DepartmentModule } from '@app/referential/department/department.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DepartmentTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), DepartmentModule],
})
export class DepartmentRoutingModule {}
