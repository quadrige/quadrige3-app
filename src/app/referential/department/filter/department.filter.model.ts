import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { IntReferentialFilterCriteria, ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Department } from '@app/referential/department/department.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'DepartmentFilterCriteriaVO' })
export class DepartmentFilterCriteria extends ReferentialFilterCriteria<Department, number> {
  static fromObject: (source: any, opts?: any) => DepartmentFilterCriteria;

  parentFilter: IntReferentialFilterCriteria = null;
  ldapPresent: boolean = null;

  constructor() {
    super(DepartmentFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.parentFilter = IntReferentialFilterCriteria.fromObject(source.parentFilter || {});
    this.ldapPresent = source.ldapPresent;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.parentFilter = this.parentFilter?.asObject(opts);
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'parentFilter') return !BaseFilterUtils.isCriteriaEmpty(this.parentFilter, true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'DepartmentFilterVO' })
export class DepartmentFilter extends ReferentialFilter<DepartmentFilter, DepartmentFilterCriteria, Department> {
  static fromObject: (source: any, opts?: any) => DepartmentFilter;

  constructor() {
    super(DepartmentFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): DepartmentFilterCriteria {
    return DepartmentFilterCriteria.fromObject(source, opts);
  }
}
