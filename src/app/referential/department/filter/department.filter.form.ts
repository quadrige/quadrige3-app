import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { DepartmentFilter, DepartmentFilterCriteria } from '@app/referential/department/filter/department.filter.model';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@Component({
  selector: 'app-department-filter-form',
  templateUrl: './department.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepartmentFilterForm extends ReferentialCriteriaFormComponent<DepartmentFilter, DepartmentFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabelName;
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof DepartmentFilterCriteria)[] {
    const preservedCriterias = super.getPreservedCriteriaBySystem(systemId);
    switch (systemId) {
      case 'SANDRE':
        return preservedCriterias.concat('parentFilter');
    }
    return preservedCriterias;
  }

  protected criteriaToQueryParams(criteria: DepartmentFilterCriteria): PartialRecord<keyof DepartmentFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.ldapPresent)) params.ldapPresent = criteria.ldapPresent;
    this.subCriteriaToQueryParams(params, criteria, 'parentFilter');
    return params;
  }
}
