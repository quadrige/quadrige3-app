import { Department as BaseDepartment, EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { TranscribingItem } from '@app/referential/transcribing-item/transcribing-item.model';
import { IntReferential, IWithTranscribingItems } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';

@EntityClass({ typename: 'DepartmentVO' })
export class Department extends BaseDepartment implements IWithTranscribingItems {
  static entityName = 'Department';
  static fromObject: (source: any, opts?: any) => Department;

  email: string = null;
  address: string = null;
  phone: string = null;
  siret: string = null;
  siteUrl: string = null;
  ldapPresent: boolean = null;
  parent: IntReferential = null;

  transcribingItems: TranscribingItem[] = null;
  transcribingItemsLoaded = false;

  constructor() {
    super(); // __typename is managed in BaseDepartment
    this.entityName = Department.entityName;
    this.label = null;
    this.name = null;
    this.description = null;
    this.comments = null;
    this.statusId = null;
    this.creationDate = null;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = Department.entityName;
    this.email = source.email;
    this.address = source.address;
    this.phone = source.phone;
    this.siret = source.siret;
    this.siteUrl = source.siteUrl;
    this.ldapPresent = source.ldapPresent;
    this.parent = IntReferential.fromObject(source.parent);
    this.transcribingItems = source.transcribingItems?.map(TranscribingItem.fromObject);
    this.transcribingItemsLoaded = source.transcribingItemsLoaded;
  }

  asObject(options?: EntityAsObjectOptions): any {
    const target = super.asObject(options);
    target.parent = EntityUtils.asMinifiedObject(this.parent, options);
    target.transcribingItems = this.transcribingItems?.map((value) => value.asObject(options));
    delete target.entityName;
    delete target.validityStatusId;
    delete target.parentId;
    delete target.levelId;
    return target;
  }

  clone(): Department {
    const target = new Department();
    target.fromObject(this);
    return target;
  }
}
