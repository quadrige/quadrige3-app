import { DepartmentTable } from '@app/referential/department/department.table';
import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { IAddTable } from '@app/shared/table/table.model';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { DepartmentService } from '@app/referential/department/department.service';
import { DepartmentValidatorService } from '@app/referential/department/department.validator';
import { DepartmentFilter } from '@app/referential/department/filter/department.filter.model';
import { Department } from '@app/referential/department/department.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { DepartmentFilterForm } from '@app/referential/department/filter/department.filter.form';
import { ProgramRightTable } from '@app/referential/right/program/program-right.table';
import { MetaProgramRightTable } from '@app/referential/right/meta-program/meta-program-right.table';
import { RuleListRightTable } from '@app/referential/right/rule-list/rule-list-right.table';

@Component({
  selector: 'app-department-add-table',
  templateUrl: './department.table.html',
  standalone: true,
  imports: [ReferentialModule, DepartmentFilterForm, ProgramRightTable, MetaProgramRightTable, RuleListRightTable],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepartmentAddTable extends DepartmentTable implements IAddTable<Department> {
  @Input() addCriteria: ISelectCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: DepartmentService,
    protected validatorService: DepartmentValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[department-add-table]';
    this.addTable = true;
  }

  async resetFilter(filter?: DepartmentFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: this.addCriteria,
      defaultCriteria: this.addFirstCriteria,
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('ldapPresent', 'parent');
  }
}
