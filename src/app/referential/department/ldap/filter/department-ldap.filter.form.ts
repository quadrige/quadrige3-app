import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { DepartmentFilter, DepartmentFilterCriteria } from '@app/referential/department/filter/department.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-department-ldap-filter-form',
  templateUrl: './department-ldap.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepartmentLdapFilterForm extends ReferentialCriteriaFormComponent<DepartmentFilter, DepartmentFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.labelName;
  }
}
