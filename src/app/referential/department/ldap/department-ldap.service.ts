import { Injectable, Injector } from '@angular/core';
import { BaseEntityGraphqlQueries, LoadResult, Page } from '@sumaris-net/ngx-components';
import { BaseReferentialService, ReferentialServiceWatchOptions } from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { Department } from '@app/referential/department/department.model';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { DepartmentFilter, DepartmentFilterCriteria } from '@app/referential/department/filter/department.filter.model';

const fragments = {
  ldapDepartment: gql`
    fragment LdapDepartmentFragment on DepartmentVO {
      id
      label
      name
      address
      email
      phone
      parent {
        id
        label
        name
      }
      __typename
    }
  `,
};

const queries: BaseEntityGraphqlQueries = {
  load: gql`
    query LdapDepartment($id: String) {
      data: ldapDepartment(id: $id) {
        ...LdapDepartmentFragment
      }
    }
    ${fragments.ldapDepartment}
  `,

  loadAll: gql`
    query LdapDepartments($sortBy: String, $sortDirection: String, $filter: DepartmentFilterVOInput) {
      data: ldapDepartments(sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter) {
        ...LdapDepartmentFragment
      }
    }
    ${fragments.ldapDepartment}
  `,

  loadAllWithTotal: undefined,
  countAll: undefined,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: undefined,
  deleteAll: undefined,
};

@Injectable({ providedIn: 'root' })
export class DepartmentLdapService extends BaseReferentialService<Department, DepartmentFilter, DepartmentFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Department, DepartmentFilter, { queries, mutations, watchQueryFetchPolicy: 'no-cache' });
    this._logPrefix = '[department-ldap-service]';
  }

  async loadByLabel(label: string): Promise<Department> {
    const now = Date.now();
    if (this._debug) console.debug(`[referential-service] Loading ${this._logTypeName} {${label}}...`);
    const query = this.queries.load;

    const { data } = await this.graphql.query<{ data: any }>({
      query,
      variables: {
        id: label,
      },
      fetchPolicy: 'no-cache',
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
    });
    if (this._debug) console.debug(`[referential-service] ${this._logTypeName} item loaded in ${Date.now() - now}ms`, data);

    // Convert to entity
    return data && this.fromObject(data);
  }

  watchPage(page: Partial<Page>, filter?: DepartmentFilter, opts?: ReferentialServiceWatchOptions): Observable<LoadResult<Department>> {
    return super.watchPage(page, filter, { ...opts, withTotal: false });
  }

  async save(entity: Department, opts?: EntitySaveOptions): Promise<Department> {
    throw new Error(`forbidden`);
  }

  async saveAll(entities: Department[], opts?: EntitySaveOptions): Promise<Department[]> {
    throw new Error(`forbidden`);
  }

  delete(data: Department, opts?: any): Promise<any> {
    throw new Error(`forbidden`);
  }

  async deleteAll(entities: Department[], opts?: any): Promise<any> {
    throw new Error(`forbidden`);
  }
}
