import { ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { Department } from '@app/referential/department/department.model';
import { DepartmentLdapService } from '@app/referential/department/ldap/department-ldap.service';
import { DepartmentFilter, DepartmentFilterCriteria } from '@app/referential/department/filter/department.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-department-ldap-table',
  templateUrl: './department-ldap.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepartmentLdapTable extends ReferentialTable<Department, number, DepartmentFilter, DepartmentFilterCriteria> implements OnInit {
  constructor(
    protected injector: Injector,
    protected _entityService: DepartmentLdapService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['label', 'name', 'address', 'email', 'phone']).concat(RESERVED_END_COLUMNS),
      Department,
      _entityService,
      undefined
    );

    this.i18nColumnPrefix = 'REFERENTIAL.DEPARTMENT.';
    this.logPrefix = '[department-ldap-table]';
    this.defaultSortBy = 'label';
  }

  ngOnInit() {
    // default properties
    this.subTable = true;
    // change toolbar color
    this.toolbarColor = 'secondary900';

    this.inlineEdition = false;
    this.checkBoxSelection = true;
    this.toggleOnlySelection = true;
    this.permanentSelectionAllowed = true;

    super.ngOnInit();

    setTimeout(() => {
      this.filterFormComponent()?.open();
    }, 300); // not less than 300ms !
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('email', 'address', 'phone');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name');
  }
}
