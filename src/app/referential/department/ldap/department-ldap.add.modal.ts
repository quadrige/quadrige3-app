import { ChangeDetectionStrategy, Component, Injector, ViewChild } from '@angular/core';
import { DepartmentLdapTable } from '@app/referential/department/ldap/department-ldap.table';
import { isEmptyArray } from '@sumaris-net/ngx-components';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IAddResult } from '@app/shared/model/options.model';

@Component({
  selector: 'app-department-ldap-add-modal',
  templateUrl: './department-ldap.add.modal.html',
  styleUrls: ['./department-ldap.add.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepartmentLdapAddModal extends ModalComponent<IAddResult> {
  @ViewChild('addTable', { static: true }) addTable: DepartmentLdapTable;

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled() {
    return isEmptyArray(this.addTable.selectedEntities);
  }

  protected afterInit() {}

  protected dataToValidate(): Promise<IAddResult> | IAddResult {
    return { added: this.addTable.selectedEntities };
  }

  protected onError(e: any) {
    super.onError(e);
    this.addTable.markAsError(e?.message || e);
  }
}
