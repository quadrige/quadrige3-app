import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { Department } from '@app/referential/department/department.model';
import { DepartmentService } from '@app/referential/department/department.service';
import {
  lengthComment,
  lengthDescription,
  lengthLabel,
  ReferentialAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';

@Injectable({ providedIn: 'root' })
export class DepartmentValidatorService extends ReferentialValidatorService<Department> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected service: DepartmentService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Department, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      label: [
        data?.label || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        ReferentialAsyncValidators.checkAttributeAlreadyExists(this.service, this.dataSource, opts, data, 'label'),
      ],
      name: [data?.name || null, Validators.compose([Validators.required, Validators.maxLength(lengthDescription)])],
      description: [data?.description || null, Validators.maxLength(lengthDescription)],
      email: [data?.email || null, Validators.compose([Validators.email, Validators.maxLength(lengthDescription)])],
      address: [data?.address || null, Validators.maxLength(lengthDescription)],
      phone: [data?.phone || null, Validators.maxLength(lengthDescription)],
      siret: [data?.siret || null, Validators.maxLength(14)],
      siteUrl: [data?.siteUrl || null, Validators.maxLength(255)],
      ldapPresent: [data?.ldapPresent || false],
      parent: [data?.parent || null, SharedValidators.entity],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      transcribingItems: [data?.transcribingItems || null],
      transcribingItemsLoaded: [toBoolean(data?.transcribingItemsLoaded, false)],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }

  getFormGroupOptions(data?: Department, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [BaseGroupValidators.invalidIfSameEntity('parent')],
    };
  }
}
