import { Injectable, Injector } from '@angular/core';
import {
  BaseReferentialService,
  ReferentialEntityGraphqlQueries,
  ReferentialServiceLoadOptions,
  ReferentialServiceWatchOptions,
} from '@app/referential/service/base-referential.service';
import gql from 'graphql-tag';
import { Department } from '@app/referential/department/department.model';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { jobFragments } from '@app/social/job/job.service';
import { DepartmentFilter, DepartmentFilterCriteria } from '@app/referential/department/filter/department.filter.model';
import { programRightFragment } from '@app/referential/right/program/program-right.service';
import { metaProgramRightFragment } from '@app/referential/right/meta-program/meta-program-right.service';
import { ruleListRightFragment } from '@app/referential/right/rule-list/rule-list-right.service';
import { ProgramRight } from '@app/referential/right/program/program-right.model';
import { isEmptyArray, Page } from '@sumaris-net/ngx-components';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { MetaProgramRight } from '@app/referential/right/meta-program/meta-program-right.model';
import { RuleListRight } from '@app/referential/right/rule-list/rule-list-right.model';
import { RightFetchOptions } from '@app/referential/right/right.model';

export const departmentFragment = gql`
  fragment DepartmentFragment on DepartmentVO {
    id
    label
    name
    description
    comments
    parent {
      id
      label
      name
      description
      comments
      ldapPresent
      updateDate
      creationDate
      statusId
      __typename
    }
    ldapPresent
    email
    address
    phone
    siret
    siteUrl
    updateDate
    creationDate
    statusId
    __typename
  }
`;

export const departmentLightFragment = gql`
  fragment DepartmentLightFragment on DepartmentVO {
    id
    label
    name
    creationDate
    statusId
    __typename
  }
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Departments($page: PageInput, $filter: DepartmentFilterVOInput, $options: RightFetchOptionsInput) {
      data: departments(page: $page, filter: $filter, options: $options) {
        ...DepartmentFragment
      }
    }
    ${departmentFragment}
  `,

  loadAllLight: gql`
    query DepartmentsLight($page: PageInput, $filter: DepartmentFilterVOInput, $options: RightFetchOptionsInput) {
      data: departments(page: $page, filter: $filter, options: $options) {
        ...DepartmentLightFragment
      }
    }
    ${departmentLightFragment}
  `,

  loadAllWithTotal: gql`
    query DepartmentsWithTotal($page: PageInput, $filter: DepartmentFilterVOInput, $options: RightFetchOptionsInput) {
      data: departments(page: $page, filter: $filter, options: $options) {
        ...DepartmentFragment
      }
      total: departmentsCount(filter: $filter)
    }
    ${departmentFragment}
  `,

  loadAllLightWithTotal: gql`
    query DepartmentsLightWithTotal($page: PageInput, $filter: DepartmentFilterVOInput, $options: RightFetchOptionsInput) {
      data: departments(page: $page, filter: $filter, options: $options) {
        ...DepartmentLightFragment
      }
      total: departmentsCount(filter: $filter)
    }
    ${departmentLightFragment}
  `,

  countAll: gql`
    query DepartmentsCount($filter: DepartmentFilterVOInput) {
      total: departmentsCount(filter: $filter)
    }
  `,

  exportAllAsync: gql`
    query ExportDepartmentsAsync($filter: DepartmentFilterVOInput, $context: ExportContextInput) {
      data: exportDepartmentsAsync(filter: $filter, context: $context) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation SaveDepartments($data: [DepartmentVOInput]) {
      data: saveDepartments(departments: $data) {
        ...DepartmentFragment
      }
    }
    ${departmentFragment}
  `,

  deleteAll: gql`
    mutation DeleteDepartments($ids: [Int]) {
      deleteDepartments(ids: $ids)
    }
  `,
};

const otherQueries = {
  programRights: gql`
    query DepartmentsProgramRights($ids: [Int]) {
      data: departmentsProgramRights(ids: $ids) {
        ...ProgramRightFragment
      }
    }
    ${programRightFragment}
  `,
  metaProgramRights: gql`
    query DepartmentsMetaProgramRights($ids: [Int]) {
      data: departmentsMetaProgramRights(ids: $ids) {
        ...MetaProgramRightFragment
      }
    }
    ${metaProgramRightFragment}
  `,
  ruleListRights: gql`
    query DepartmentsRuleListRights($ids: [Int]) {
      data: departmentsRuleListRights(ids: $ids) {
        ...RuleListRightFragment
      }
    }
    ${ruleListRightFragment}
  `,
};

export interface DepartmentServiceWatchOptions extends ReferentialServiceWatchOptions {
  options: RightFetchOptions;
}

interface DepartmentServiceLoadOptions extends ReferentialServiceLoadOptions {
  options: RightFetchOptions;
}

@Injectable({ providedIn: 'root' })
export class DepartmentService extends BaseReferentialService<
  Department,
  DepartmentFilter,
  DepartmentFilterCriteria,
  number,
  DepartmentServiceWatchOptions,
  DepartmentServiceLoadOptions
> {
  constructor(protected injector: Injector) {
    super(injector, Department, DepartmentFilter, { queries, mutations });
    this._logPrefix = '[department-service]';
  }

  protected buildWatchVariables(page: Partial<Page>, filter?: Partial<DepartmentFilter>, opts?: DepartmentServiceWatchOptions): any {
    return {
      ...super.buildWatchVariables(page, filter, opts),
      options: opts?.options,
    };
  }

  protected buildLoadVariables(page: Partial<Page>, filter?: Partial<DepartmentFilter>, opts?: DepartmentServiceLoadOptions): any {
    return {
      ...super.buildLoadVariables(page, filter, opts),
      options: opts?.options,
    };
  }

  async getProgramRights(departmentIds: number[]): Promise<ProgramRight[]> {
    if (isEmptyArray(departmentIds)) return [];
    const variables: any = { ids: departmentIds };
    if (this._debug) console.debug(`${this._logPrefix} Loading program rights...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[] }>({
      query: otherQueries.programRights,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(ProgramRight.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} Program rights loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async getMetaProgramRights(departmentIds: number[]): Promise<MetaProgramRight[]> {
    if (isEmptyArray(departmentIds)) return [];
    const variables: any = { ids: departmentIds };
    if (this._debug) console.debug(`${this._logPrefix} Loading meta-program rights...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[] }>({
      query: otherQueries.metaProgramRights,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(MetaProgramRight.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} Meta-program rights loaded in ${Date.now() - now}ms`, data);
    return data;
  }

  async getRuleListRights(departmentIds: number[]): Promise<RuleListRight[]> {
    if (isEmptyArray(departmentIds)) return [];
    const variables: any = { ids: departmentIds };
    if (this._debug) console.debug(`${this._logPrefix} Loading rule list rights...`, variables);
    const now = Date.now();
    const res = await this.graphql.query<{ data: any[] }>({
      query: otherQueries.ruleListRights,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = (res?.data || []).map(RuleListRight.fromObject);
    if (this._debug) console.debug(`${this._logPrefix} Rule list rights loaded in ${Date.now() - now}ms`, data);
    return data;
  }
}
