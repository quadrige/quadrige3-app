import { ChangeDetectionStrategy, Component, Injector, OnInit, Optional, ViewChild } from '@angular/core';
import { AppFormUtils, isNotEmptyArray, isNotNil, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { Department } from '@app/referential/department/department.model';
import { DepartmentValidatorService } from '@app/referential/department/department.validator';
import { DepartmentService } from '@app/referential/department/department.service';
import { DepartmentLdapAddModal } from '@app/referential/department/ldap/department-ldap.add.modal';
import { Alerts } from '@app/shared/alerts';
import { DepartmentLdapService } from '@app/referential/department/ldap/department-ldap.service';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { IStatusMultiEditModalOptions, StatusComposite, StatusMultiEditModal } from '@app/shared/component/multi-edit/status.multi-edit.modal';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { DepartmentFilter, DepartmentFilterCriteria } from '@app/referential/department/filter/department.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import { ProgramRightTable } from '@app/referential/right/program/program-right.table';
import { MetaProgramRightTable } from '@app/referential/right/meta-program/meta-program-right.table';
import { RuleListRightTable } from '@app/referential/right/rule-list/rule-list-right.table';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { BaseRightTable } from '@app/referential/right/base-right.table';
import { IRightModel, RightFetchOptions } from '@app/referential/right/right.model';
import { TranscribingItemView } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { IAddResult, IModalOptions } from '@app/shared/model/options.model';
import { ExportOptions } from '@app/shared/table/table.model';

type DepartmentView = 'programRightTable' | 'metaProgramRightTable' | 'ruleListRightTable' | TranscribingItemView;

@Component({
  selector: 'app-department-table',
  templateUrl: './department.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepartmentTable
  extends ReferentialTable<Department, number, DepartmentFilter, DepartmentFilterCriteria, DepartmentValidatorService, DepartmentView>
  implements OnInit
{
  @ViewChild('programRightTable') programRightTable: ProgramRightTable;
  @ViewChild('metaProgramRightTable') metaProgramRightTable: MetaProgramRightTable;
  @ViewChild('ruleListRightTable') ruleListRightTable: RuleListRightTable;

  rightFetchOptions: RightFetchOptions;

  departmentRightMenuItem: IEntityMenuItem<Department, DepartmentView> = {
    title: 'REFERENTIAL.RIGHT.TITLE',
    children: [
      {
        title: 'REFERENTIAL.ENTITY.PROGRAM_STRATEGIES',
        viewTitle: 'REFERENTIAL.ENTITY.PROGRAMS',
        view: 'programRightTable',
      },
      {
        title: 'REFERENTIAL.ENTITY.META_PROGRAMS',
        view: 'metaProgramRightTable',
      },
      {
        title: 'REFERENTIAL.ENTITY.RULE_LISTS',
        view: 'ruleListRightTable',
      },
    ],
  };

  constructor(
    protected injector: Injector,
    protected _entityService: DepartmentService,
    protected validatorService: DepartmentValidatorService,
    @Optional() protected ldapService?: DepartmentLdapService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'label',
        'name',
        'description',
        'parent',
        'ldapPresent',
        'email',
        'address',
        'phone',
        'siret',
        'siteUrl',
        'comments',
        'statusId',
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      Department,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.DEPARTMENT.';
    this.logPrefix = '[department-table]';
    this.defaultSortBy = 'label';
  }

  ngOnInit() {
    super.ngOnInit();

    // parent combo
    this.registerAutocompleteField('parent', {
      ...this.referentialOptions.department,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Department',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected async initRightView() {
    await super.initRightView();
    this.restoreRightPanel(false, 40);
  }

  protected getRightMenuItems(): IEntityMenuItem<Department, TranscribingItemView | DepartmentView>[] {
    return [this.departmentRightMenuItem, ...super.getRightMenuItems()];
  }

  protected getDefaultRightMenuItem(): IEntityMenuItem<Department, TranscribingItemView | DepartmentView> {
    return this.departmentRightMenuItem.children?.[0];
  }

  protected async loadRightArea(row?: AsyncTableElement<Department>): Promise<void> {
    if (this.subTable) return;
    switch (this.rightMenuItem?.view) {
      case 'programRightTable': {
        this.detectChanges();
        this.registerSubForm(this.programRightTable);
        await this.loadRightTable(this.programRightTable, (userIds) => this._entityService.getProgramRights(userIds));
        break;
      }
      case 'metaProgramRightTable': {
        this.detectChanges();
        this.registerSubForm(this.metaProgramRightTable);
        await this.loadRightTable(this.metaProgramRightTable, (userIds) => this._entityService.getMetaProgramRights(userIds));
        break;
      }
      case 'ruleListRightTable': {
        this.detectChanges();
        this.registerSubForm(this.ruleListRightTable);
        await this.loadRightTable(this.ruleListRightTable, (userIds) => this._entityService.getRuleListRights(userIds));
        break;
      }
    }
    return super.loadRightArea(row);
  }

  protected async loadRightTable(rightTable: BaseRightTable<any>, load: (userIds: number[]) => Promise<IRightModel[]>) {
    await rightTable.setValue(undefined);
    if (this.selection.isEmpty()) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      return;
    }

    await rightTable.waitIdle();
    rightTable.markAsLoading();
    const userIds = this.selection.selected.map((value) => value.currentData.id).filter(isNotNil);
    const rights = await load(userIds);
    await rightTable.setValue(rights);
  }

  async openAddFromLdapModal(event: any) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IModalOptions, IAddResult>(
      DepartmentLdapAddModal,
      {
        titleI18n: 'REFERENTIAL.DEPARTMENT.LDAP.ADD',
      },
      'modal-medium'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      for (const ldapDepartment of data.added) {
        const newRow = await this.addRowToTable();
        newRow.validator.patchValue({ ...ldapDepartment, id: null, ldapPresent: true, statusId: 1 });
        newRow.validator.markAsDirty();

        let rowValid = await this.confirmEditCreate(event, newRow);
        if (!rowValid) {
          if (newRow.validator.pending) {
            await AppFormUtils.waitWhilePending(newRow.validator);
            rowValid = await this.confirmEditCreate(event, newRow);
          }
        }
        if (!rowValid) {
          console.warn(`${this.logPrefix} This department cannot be imported due to validation error`, newRow);
          break;
        }
      }
      this.markAsDirty();
    }
  }

  isReadOnly(): ReadOnlyIfFn {
    return (row) => row.currentData?.ldapPresent;
  }

  canUpdateFromLdap(): boolean {
    return this.singleSelectedRow?.currentData.ldapPresent || false;
  }

  async updateFromLdap(event: any) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate()) || !this.canUpdateFromLdap()) {
      return;
    }

    const row = this.singleSelectedRow;
    const ldapDepartment = await this.ldapService?.loadByLabel(row.currentData.label);
    if (!ldapDepartment) {
      await Alerts.showError('REFERENTIAL.DEPARTMENT.LDAP.NOT_FOUND', this.alertCtrl, this.translate);
      return;
    }

    const partialLdapDepartment = {
      label: ldapDepartment.label,
      name: ldapDepartment.name,
      address: ldapDepartment.address,
      email: ldapDepartment.email,
      phone: ldapDepartment.phone,
    };

    // check difference
    if (!EntityUtils.deepEquals(row.currentData, partialLdapDepartment, Object.keys(partialLdapDepartment))) {
      // difference found: patch row
      row.validator.patchValue({ ...partialLdapDepartment });

      await Alerts.showError('REFERENTIAL.DEPARTMENT.LDAP.UPDATED', this.alertCtrl, this.translate, { titleKey: 'INFO.ALERT_HEADER' });
      this.markRowAsDirty(row);
    } else {
      await Alerts.showError('REFERENTIAL.DEPARTMENT.LDAP.NOT_UPDATED', this.alertCtrl, this.translate, { titleKey: 'INFO.ALERT_HEADER' });
    }
  }

  async multiEditRows(event: MouseEvent) {
    if (this.selection.selected.length < 2) return;
    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IStatusMultiEditModalOptions, StatusComposite>(
      StatusMultiEditModal,
      {
        titleI18n: 'REFERENTIAL.DEPARTMENT.MULTI_EDIT.TITLE',
        selection: this.selection.selected.map((row) => row.currentData),
      },
      'modal-200'
    );
    if (this.debug) {
      console.debug(`${this.logPrefix} returned data`, data);
    }

    if (role === 'validate' && data) {
      // Patch selected rows
      this.selection.selected.forEach((row) => {
        row.validator.patchValue(data);
        this.markRowAsDirty(row);
      });
      this.markForCheck();
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions): Promise<void> {
    return super.export(event, { ...opts, rowCountWarningThreshold: 10, rightsEnabled: true });
  }

  protected updateTitle() {
    this.title = ReferentialMenuModel.title(this.translate, { i18nKey: 'REFERENTIAL.ENTITY.DEPARTMENTS' });
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('email', 'address', 'phone', 'siret', 'siteUrl', 'comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name');
  }
}
