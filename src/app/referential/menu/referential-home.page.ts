import { Component } from '@angular/core';
import { ReferentialMenuModel } from '@app/referential/menu/referential-menu.model';
import { MenuPage } from '@app/shared/component/menu/menu.page';

@Component({
  selector: 'app-referential-home-page',
  templateUrl: 'referential-home.page.html',
})
export class ReferentialHomePage extends MenuPage {
  constructor() {
    super(ReferentialMenuModel.referentialMenuItems());
  }
}
