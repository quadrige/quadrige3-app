import { IMenuItem, isNil } from '@sumaris-net/ngx-components';
import { TranslateService } from '@ngx-translate/core';
import { Utils } from '@app/shared/utils';

const toI18nKey = Utils.toI18nKey;

const referentialPmfmuMenu: IMenuItem = {
  title: 'MENU.REFERENTIAL.PMFMU',
  children: [
    { title: toI18nKey('Pmfmu'), path: '/referential/Pmfmu' },
    { title: toI18nKey('Parameter'), path: '/referential/Parameter' },
    { title: toI18nKey('Matrix'), path: '/referential/Matrix' },
    { title: toI18nKey('Fraction'), path: '/referential/Fraction' },
    { title: toI18nKey('Method'), path: '/referential/Method' },
    { title: toI18nKey('Unit'), path: '/referential/Unit' },
    { title: toI18nKey('ParameterGroup'), path: '/referential/ParameterGroup' },
  ],
};
const referentialAdministrationMenu: IMenuItem = {
  title: 'MENU.REFERENTIAL.ADMINISTRATION',
  children: [
    { title: toI18nKey('User'), path: '/referential/User' },
    { title: toI18nKey('Department'), path: '/referential/Department' },
    { title: toI18nKey('Privilege'), path: '/referential/Privilege' },
    { title: toI18nKey('Training'), path: '/referential/Training' },
  ],
};
const referentialGeographicMenu: IMenuItem = {
  title: 'MENU.REFERENTIAL.GEOGRAPHIC',
  children: [
    { title: toI18nKey('MonitoringLocation'), path: '/referential/MonitoringLocation' },
    { title: toI18nKey('Harbour'), path: '/referential/Harbour' },
    { title: toI18nKey('OrderItemType'), path: '/referential/OrderItemType' },
    { title: toI18nKey('PositioningSystem'), path: '/referential/PositioningSystem' },
    { title: toI18nKey('PositioningType'), path: '/referential/PositioningType' },
    { title: toI18nKey('DredgingTargetArea'), path: '/referential/DredgingTargetArea' },
    { title: toI18nKey('DredgingAreaType'), path: '/referential/DredgingAreaType' },
  ],
};
// const referentialTaxonomicMenu: IMenuItem = {
//   title: 'MENU.REFERENTIAL.TAXONOMIC',
//   children: [
//     { title: toI18nKey('TaxonName') },
//     { title: toI18nKey('TaxonGroup') },
//     { title: toI18nKey('Citation') },
//     { title: toI18nKey('TaxonomicLevel') },
//   ],
// };
const referentialAquacultureMenu: IMenuItem = {
  title: 'MENU.REFERENTIAL.AQUACULTURE',
  children: [
    { title: toI18nKey('AgeGroup'), path: '/referential/AgeGroup' },
    { title: toI18nKey('Ploidy'), path: '/referential/Ploidy' },
    { title: toI18nKey('BreedingStructure'), path: '/referential/BreedingStructure' },
    { title: toI18nKey('BreedingSystem'), path: '/referential/BreedingSystem' },
    { title: toI18nKey('BreedingPhaseType'), path: '/referential/BreedingPhaseType' },
  ],
};
const referentialOtherMenu: IMenuItem = {
  title: 'MENU.REFERENTIAL.OTHER',
  children: [
    { title: toI18nKey('AnalysisInstrument'), path: '/referential/AnalysisInstrument' },
    { title: toI18nKey('SamplingEquipment'), path: '/referential/SamplingEquipment' },
    { title: toI18nKey('Frequency'), path: '/referential/Frequency' },
    { title: toI18nKey('Ship'), path: '/referential/Ship' },
    { title: toI18nKey('DepthLevel'), path: '/referential/DepthLevel' },
    { title: toI18nKey('QualityFlag'), path: '/referential/QualityFlag' },
    { title: toI18nKey('NumericalPrecision'), path: '/referential/NumericalPrecision' },
    { title: toI18nKey('EventType'), path: '/referential/EventType' },
    { title: toI18nKey('PrecisionType'), path: '/referential/PrecisionType' },
    { title: toI18nKey('PhotoType'), path: '/referential/PhotoType' },
    { title: toI18nKey('ResourceType'), path: '/referential/ResourceType' },
    { title: toI18nKey('ObservationTypology'), path: '/referential/ObservationTypology' },
  ],
};

export class ReferentialMenuModel {
  static managementMenuItems(): IMenuItem[] {
    return [
      { title: 'REFERENTIAL.ENTITY.PROGRAM_STRATEGIES', path: '/management/Program' },
      { title: toI18nKey('MetaProgram'), path: '/management/MetaProgram' },
      { title: toI18nKey('RuleList'), path: '/management/RuleList' },
      { title: toI18nKey('Moratorium'), path: '/management/Moratorium' },
    ];
  }

  static referentialMenuItems(): IMenuItem[] {
    return [
      referentialPmfmuMenu,
      referentialAdministrationMenu,
      referentialGeographicMenu,
      // referentialTaxonomicMenu,
      referentialAquacultureMenu,
      referentialOtherMenu,
    ];
  }

  static root(childTitle: string): IMenuItem {
    return ReferentialMenuModel.referentialMenuItems().find(
      (menu) => menu.children && menu.children?.find((child) => child.title === childTitle) !== undefined
    );
  }

  static title(translate: TranslateService, opts: { i18nKey?: string; entityName?: string; withRoot?: boolean }): string {
    if (isNil(opts.entityName) && isNil(opts.i18nKey)) return undefined;

    const keys = [];
    let key = opts.i18nKey;
    let rootKey: string;

    if (!key && opts.entityName) {
      key = toI18nKey(opts.entityName);
    }
    if (key) {
      keys.push(key);
    }
    if (opts.withRoot !== false) {
      rootKey = ReferentialMenuModel.root(key)?.title;
      if (rootKey) keys.push(rootKey);
    }

    // Translate
    const message = translate.instant(keys);
    if (rootKey) return message[rootKey] + ' &rArr; ' + message[key];
    return message[key];
  }
}
