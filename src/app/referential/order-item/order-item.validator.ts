import { Injectable } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { SharedValidators, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import { GenericService } from '@app/referential/generic/generic.service';
import {
  lengthComment,
  lengthLabel,
  lengthName,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { OrderItem } from '@app/referential/order-item/order-item.model';

@Injectable({ providedIn: 'root' })
export class OrderItemValidatorService extends ReferentialValidatorService<OrderItem> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected referentialService: GenericService
  ) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: OrderItem, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      label: [data?.label || null, Validators.compose([Validators.required, Validators.maxLength(lengthLabel)])],
      name: [data?.name || null, Validators.compose([Validators.required, Validators.maxLength(lengthName)])],
      orderItemType: [data?.orderItemType || null],
      rankOrder: [toNumber(data?.rankOrder, null), Validators.compose([Validators.required, SharedValidators.integer])],
      geometry: [data?.geometry || null],
      geometryLoaded: [toBoolean(data?.geometryLoaded, false)],
      comments: [data?.comments || null, Validators.maxLength(lengthComment)],
      statusId: [toNumber(data?.statusId, null), Validators.required],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
    };
  }
}
