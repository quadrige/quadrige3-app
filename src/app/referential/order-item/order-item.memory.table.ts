import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import { OrderItemValidatorService } from '@app/referential/order-item/order-item.validator';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { IOrderItemReportModalOptions, OrderItemReportModal } from '@app/referential/mon-loc-order-item/order-item-report.modal';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { OrderItemMemoryService } from '@app/referential/order-item/order-item.memory.service';
import { OrderItemService } from '@app/referential/order-item/order-item.service';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';

@Component({
  selector: 'app-order-item-memory-table',
  templateUrl: './order-item.memory.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemMemoryTable
  extends ReferentialMemoryTable<OrderItem, number, IntReferentialFilter, IntReferentialFilterCriteria, OrderItemValidatorService>
  implements OnInit, AfterViewInit
{
  constructor(
    protected injector: Injector,
    protected _entityService: OrderItemMemoryService,
    protected validatorService: OrderItemValidatorService,
    protected orderItemService: OrderItemService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['label', 'name', 'rankOrder', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      OrderItem,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.ENTITY.ORDER_ITEMS';
    this.i18nColumnPrefix = 'REFERENTIAL.ORDER_ITEM.';
    this.defaultSortBy = 'label';
    this.logPrefix = '[order-item-table]';
  }

  ngOnInit() {
    this.subTable = true;
    super.ngOnInit();
  }

  async report(event: UIEvent, row?: AsyncTableElement<OrderItem>) {
    row = row || this.singleEditingRow;
    if (!row || !row.editing || !row.validator.valid || !row.currentData.id) {
      console.warn(`${this.logPrefix} Can't execute report: Invalid OrderItem selected`, row);
      return;
    }
    await this.modalService.openModal<IOrderItemReportModalOptions, void>(
      OrderItemReportModal,
      {
        titleI18n: 'REFERENTIAL.ORDER_ITEM.REPORT_TITLE',
        orderItemId: row.currentData.id,
        service: this.orderItemService,
      },
      'modal-medium'
    );
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name');
  }
}
