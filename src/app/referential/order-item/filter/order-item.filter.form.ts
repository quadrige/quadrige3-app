import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { isNotNil, isNotNilOrBlank } from '@sumaris-net/ngx-components';
import { OrderItemFilter, OrderItemFilterCriteria } from '@app/referential/order-item/filter/order-item.filter.model';
import { StrReferential } from '@app/referential/model/referential.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-order-item-filter-form',
  templateUrl: './order-item.filter.form.html',
  standalone: true,
  imports: [ReferentialModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemFilterForm extends ReferentialCriteriaFormComponent<OrderItemFilter, OrderItemFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.labelName;
  }

  protected criteriaToQueryParams(criteria: OrderItemFilterCriteria): PartialRecord<keyof OrderItemFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.orderItemType)) params.orderItemType = criteria.orderItemType.id; // Id only
    this.subCriteriaToQueryParams(params, criteria, 'orderItemTypeFilter');
    return params;
  }

  protected queryParamsToCriteria(params: string): OrderItemFilterCriteria {
    const criteria = super.queryParamsToCriteria(params);
    // orderItemType param is the id only
    if (isNotNilOrBlank(criteria.orderItemType)) {
      criteria.orderItemType = StrReferential.fromObject({ id: criteria.orderItemType });
    }
    return criteria;
  }
}
