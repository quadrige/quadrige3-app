import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { IntReferentialFilter, IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-order-item-memory-filter-form',
  templateUrl: './order-item.memory.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemMemoryFilterForm extends ReferentialCriteriaFormComponent<IntReferentialFilter, IntReferentialFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.labelName;
  }
}
