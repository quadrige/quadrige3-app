import { EntityAsObjectOptions, EntityClass, isNotNilOrBlank } from '@sumaris-net/ngx-components';
import {
  IReferentialFilterCriteria,
  ReferentialFilter,
  ReferentialFilterCriteria,
  StrReferentialFilterCriteria,
} from '@app/referential/model/referential.filter.model';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { StrReferential } from '@app/referential/model/referential.model';

export interface IOrderItemFilterCriteria extends IReferentialFilterCriteria<number> {
  orderItemType?: StrReferential;
  orderItemTypeFilter?: IReferentialFilterCriteria<string>;
}

@EntityClass({ typename: 'OrderItemFilterCriteriaVO' })
export class OrderItemFilterCriteria extends ReferentialFilterCriteria<OrderItem, number> implements IOrderItemFilterCriteria {
  static fromObject: (source: any, opts?: any) => OrderItemFilterCriteria;

  orderItemType: StrReferential = null;
  orderItemTypeFilter: StrReferentialFilterCriteria = null;

  constructor() {
    super(OrderItemFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.orderItemType = StrReferential.fromObject(source.orderItemType);
    this.orderItemTypeFilter = StrReferentialFilterCriteria.fromObject(source.orderItemTypeFilter || {});
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    if (opts?.minify) {
      // If parentId not already defined
      if (!target.parentId) {
        // Compute a valid OrderItemFilterVO
        if (!BaseFilterUtils.isCriteriaEmpty(this.orderItemTypeFilter, true)) {
          target.orderItemTypeFilter = this.orderItemTypeFilter?.asObject(opts);
        } else if (!!this.orderItemType) {
          target.parentId = this.orderItemType.id;
          delete target.orderItemTypeFilter;
        } else {
          delete target.orderItemTypeFilter;
        }
      } else {
        delete target.orderItemTypeFilter;
      }
      delete target.orderItemType;
    } else {
      target.orderItemType = this.orderItemType?.asObject(opts);
      target.orderItemTypeFilter = this.orderItemTypeFilter?.asObject(opts);
    }
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'orderItemTypeFilter') return !BaseFilterUtils.isCriteriaEmpty(this.orderItemTypeFilter, true);
    if (key === 'parentId') return isNotNilOrBlank(value);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'OrderItemFilterVO' })
export class OrderItemFilter extends ReferentialFilter<OrderItemFilter, OrderItemFilterCriteria, OrderItem> {
  static fromObject: (source: any, opts?: any) => OrderItemFilter;

  constructor() {
    super(OrderItemFilter.TYPENAME);
  }

  criteriaFromObject(source: any, opts: any): OrderItemFilterCriteria {
    return OrderItemFilterCriteria.fromObject(source, opts);
  }
}
