import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import { OrderItemService } from '@app/referential/order-item/order-item.service';
import { OrderItemValidatorService } from '@app/referential/order-item/order-item.validator';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { OrderItemFilter, OrderItemFilterCriteria } from '@app/referential/order-item/filter/order-item.filter.model';
import { ReferentialTable } from '@app/referential/table/referential.table';

@Component({
  selector: 'app-order-item-table',
  templateUrl: './order-item.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemTable
  extends ReferentialTable<OrderItem, number, OrderItemFilter, OrderItemFilterCriteria, OrderItemValidatorService>
  implements OnInit, AfterViewInit
{
  autocompleteWidthExtraLarge = autocompleteWidthExtraLarge;

  constructor(
    protected injector: Injector,
    protected _entityService: OrderItemService,
    protected validatorService: OrderItemValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['label', 'name', 'orderItemType', 'rankOrder', 'comments', 'statusId', 'creationDate', 'updateDate']).concat(
        RESERVED_END_COLUMNS
      ),
      OrderItem,
      _entityService,
      validatorService
    );

    this.i18nColumnPrefix = 'REFERENTIAL.ORDER_ITEM.';
    this.defaultSortBy = 'label';
    this.logPrefix = '[order-item-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    this.titleI18n = this.titleI18n || 'REFERENTIAL.ENTITY.ORDER_ITEMS';

    // OrderItemType combo
    this.registerAutocompleteField('orderItemType', {
      ...this.referentialOptions.orderItemType,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'OrderItemType',
        criterias: [this.defaultReferentialCriteria],
      }),
    });
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'name');
  }
}
