import { EntityClass, isEmptyArray, ReferentialAsObjectOptions, referentialToString } from '@sumaris-net/ngx-components';
import { Referential, StrReferential } from '@app/referential/model/referential.model';
import { Feature } from 'geojson';
import { FeatureRecord } from '@app/shared/geometries';
import { referentialOptions } from '@app/referential/model/referential.constants';

@EntityClass({ typename: 'OrderItemVO' })
export class OrderItem extends Referential<OrderItem> {
  static entityName = 'OrderItem';
  static fromObject: (source: any, opts?: any) => OrderItem;

  rankOrder: number = null;
  orderItemTypeId: string = null;
  orderItemType?: StrReferential = null;
  geometry: Feature = null;
  geometryLoaded = false;

  constructor() {
    super(OrderItem.TYPENAME);
    this.entityName = OrderItem.entityName;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.entityName = OrderItem.entityName;
    this.rankOrder = source.rankOrder;
    this.orderItemTypeId = source.orderItemTypeId;
    this.orderItemType = StrReferential.fromObject(source.orderItemType);
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.orderItemType;
    delete target.geometry;
    delete target.geometryLoaded;
    delete target.entityName;
    return target;
  }
}

export class OrderItemUtils {
  static updateOrderItemGeometries(orderItems: OrderItem[], geometries: FeatureRecord) {
    if (isEmptyArray(orderItems) || !geometries) return;
    // update order items
    Object.keys(geometries).forEach((orderItemId) => {
      const orderItem = orderItems.find((value) => value.id.toString() === orderItemId);
      const geometry: Feature = geometries[orderItemId];
      if (!!orderItem && !!geometry) {
        // Add properties
        geometry.properties = {
          ...geometry.properties,
          name: referentialToString(orderItem, referentialOptions.orderItem.attributes),
        };
        orderItem.geometry = geometry;
        orderItem.geometryLoaded = true;
      } else {
        console.error(`[order-item] Unable to map order item geometry`, orderItem, geometry);
      }
    });
  }
}
