import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import { IntReferentialFilter } from '@app/referential/model/referential.filter.model';

@Injectable({ providedIn: 'root' })
export class OrderItemMemoryService extends EntitiesMemoryService<OrderItem, IntReferentialFilter> {
  constructor() {
    super(OrderItem, IntReferentialFilter);
  }
}
