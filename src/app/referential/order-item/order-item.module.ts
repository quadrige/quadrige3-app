import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { OrderItemTable } from '@app/referential/order-item/order-item.table';
import { OrderItemMemoryTable } from '@app/referential/order-item/order-item.memory.table';
import { OrderItemFilterForm } from '@app/referential/order-item/filter/order-item.filter.form';
import { OrderItemMemoryFilterForm } from '@app/referential/order-item/filter/order-item.memory.filter.form';

@NgModule({
  imports: [ReferentialModule, OrderItemFilterForm],
  declarations: [OrderItemTable, OrderItemMemoryTable, OrderItemMemoryFilterForm],
  exports: [OrderItemTable, OrderItemMemoryTable, OrderItemMemoryFilterForm],
})
export class OrderItemModule {}
