import { Injectable, Injector } from '@angular/core';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import gql from 'graphql-tag';
import { BaseReferentialService, ReferentialEntityGraphqlQueries } from '@app/referential/service/base-referential.service';
import { isEmptyArray, isNil } from '@sumaris-net/ngx-components';
import { FeatureRecord, Geometries } from '@app/shared/geometries';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { monLocOrderItemFragment, OrderItemReport } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { orderItemFragments } from '@app/referential/order-item/order-item.fragment';
import { OrderItemFilter, OrderItemFilterCriteria } from '@app/referential/order-item/filter/order-item.filter.model';

const orderItemReportFragment = gql`
  fragment OrderItemReportFragment on OrderItemReportVO {
    monLocOrderItems {
      ...MonLocOrderItemFragment
    }
    expectedMonLocOrderItems {
      ...MonLocOrderItemFragment
    }
  }
  ${monLocOrderItemFragment}
`;

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query OrderItems($page: PageInput, $filter: OrderItemFilterVOInput) {
      data: orderItems(page: $page, filter: $filter) {
        ...FullOrderItemFragment
      }
    }
    ${orderItemFragments.fullOrderItem}
  `,
  loadAllWithTotal: gql`
    query OrderItemsWithTotal($page: PageInput, $filter: OrderItemFilterVOInput) {
      data: orderItems(page: $page, filter: $filter) {
        ...FullOrderItemFragment
      }
      total: orderItemsCount(filter: $filter)
    }
    ${orderItemFragments.fullOrderItem}
  `,

  countAll: gql`
    query OrderItemsCount($filter: OrderItemFilterVOInput) {
      total: orderItemsCount(filter: $filter)
    }
  `,
};

const otherQueries = {
  loadAllLight: gql`
    query OrderItemsLight($page: PageInput, $filter: OrderItemFilterVOInput) {
      data: orderItems(page: $page, filter: $filter) {
        ...OrderItemFragment
      }
    }
    ${orderItemFragments.orderItem}
  `,
  geometry: gql`
    query OrderItemGeometry($id: Int!) {
      data: orderItemGeometry(id: $id)
    }
  `,
  geometries: gql`
    query OrderItemGeometries($ids: [Int]) {
      data: orderItemGeometries(ids: $ids)
    }
  `,
  report: gql`
    query ReportOrderItem($id: Int) {
      data: reportOrderItem(orderItemId: $id) {
        ...OrderItemReportFragment
      }
    }
    ${orderItemReportFragment}
  `,
};

@Injectable({ providedIn: 'root' })
export class OrderItemService extends BaseReferentialService<OrderItem, OrderItemFilter, OrderItemFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, OrderItem, OrderItemFilter, { queries });
    this._logPrefix = '[order-item-service]';
  }

  async loadAllByCriteria(criteria: Partial<OrderItemFilterCriteria>): Promise<OrderItem[]> {
    const result = await this.loadPage(undefined, OrderItemFilter.fromObject({ criterias: [criteria] }), {
      query: otherQueries.loadAllLight,
      withTotal: false,
      fetchPolicy: 'network-only',
    });
    return result.data;
  }

  async loadGeometryFeatures(ids: number[]): Promise<FeatureRecord> {
    if (isEmptyArray(ids)) {
      return undefined;
    }

    const variables: any = { ids };
    if (this._debug) console.debug(`${this._logPrefix} Loading geometries...`, variables);
    const now = Date.now();
    const query = otherQueries.geometries;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} geometry loaded in ${Date.now() - now}ms`, data);
    return Geometries.toFeatureRecord(data);
  }

  async getReport(id: number): Promise<OrderItemReport> {
    if (isNil(id)) {
      return undefined;
    }
    const variables: any = { id };
    if (this._debug) console.debug(`${this._logPrefix} Generating report...`, variables);
    const now = Date.now();
    const query = otherQueries.report;
    const res = await this.graphql.query<{ data: any }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const data = res?.data;
    if (this._debug) console.debug(`${this._logPrefix} Report generated in ${Date.now() - now}ms`, data);
    return OrderItemReport.fromObject(data);
  }
}
