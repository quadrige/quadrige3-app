import { ChangeDetectionStrategy, Component, Injector, Input, OnInit } from '@angular/core';
import { OrderItemTable } from '@app/referential/order-item/order-item.table';
import { OrderItemService } from '@app/referential/order-item/order-item.service';
import { OrderItemValidatorService } from '@app/referential/order-item/order-item.validator';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { IAddTable } from '@app/shared/table/table.model';
import { IOrderItemFilterCriteria, OrderItemFilter, OrderItemFilterCriteria } from '@app/referential/order-item/filter/order-item.filter.model';
import { OrderItem } from '@app/referential/order-item/order-item.model';
import { ReferentialModule } from '@app/referential/referential.module';
import { OrderItemFilterForm } from '@app/referential/order-item/filter/order-item.filter.form';

@Component({
  selector: 'app-order-item-add-table',
  templateUrl: './order-item.table.html',
  standalone: true,
  imports: [ReferentialModule, OrderItemFilterForm],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderItemAddTable extends OrderItemTable implements OnInit, IAddTable<OrderItem> {
  @Input() addCriteria: IOrderItemFilterCriteria;
  @Input() addFirstCriteria: ISelectCriteria;
  @Input() titleAddPrefixI18n: string;
  @Input() titlePrefixI18n: string;

  constructor(
    protected injector: Injector,
    protected _entityService: OrderItemService,
    protected validatorService: OrderItemValidatorService
  ) {
    super(injector, _entityService, validatorService);
    this.dataSource.watchAllOptions = { ...this.dataSource.watchAllOptions, light: true };
    this.logPrefix = '[order-item-add-table]';
    this.addTable = true;
  }

  ngOnInit() {
    super.ngOnInit();

    if (this.addCriteria?.orderItemTypeFilter) {
      this.autocompleteFields.orderItemType.filter = { ...this.addCriteria?.orderItemTypeFilter, entityName: 'OrderItemType' };
    }
  }

  async resetFilter(filter?: OrderItemFilter, opts?: ISetFilterOptions) {
    if (opts?.firstTime) filter = this.loadLastFilter();
    await super.resetFilter(filter, {
      ...opts,
      resetNamedFilter: !filter?.name,
      findByName: !!filter?.name,
      staticCriteria: OrderItemFilterCriteria.fromObject(this.addCriteria),
      defaultCriteria: OrderItemFilterCriteria.fromObject(this.addFirstCriteria),
    });
    // Reset add filter
    this.addFirstCriteria = undefined;
  }

  protected updateTitle() {
    // change title
    if (!this.title && this.showTitle) {
      const keys = [];
      if (this.titleAddPrefixI18n) keys.push(this.titleAddPrefixI18n);
      if (this.titlePrefixI18n) keys.push(this.titlePrefixI18n);
      if (this.titleI18n) keys.push(this.titleI18n);
      this.title = keys.map((value) => this.translate.instant(value)).join(' ');
    }
  }
}
