import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';

export const orderItemFragments = {
  orderItem: gql`
    fragment OrderItemFragment on OrderItemVO {
      id
      label
      name
      rankOrder
      orderItemTypeId
      comments
      creationDate
      updateDate
      statusId
      __typename
    }
  `,
  fullOrderItem: gql`
    fragment FullOrderItemFragment on OrderItemVO {
      id
      label
      name
      rankOrder
      orderItemType {
        ...ReferentialFragment
      }
      orderItemTypeId
      comments
      creationDate
      updateDate
      statusId
      __typename
    }
    ${referentialFragments.light}
  `,
};
