import { ChangeDetectionStrategy, Component, Injector, Input, ViewChild } from '@angular/core';
import { RulePmfmu } from '@app/referential/rule/pmfmu/rule-pmfmu.model';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { ObjectMultiMap } from '@app/shared/multimap';
import { attributes } from '@app/referential/model/referential.constants';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IPreconditionQualitativeModalOptions extends IModalOptions {
  basePmfmu: RulePmfmu;
  usedPmfmu: RulePmfmu;
  qvMap: ObjectMultiMap<number>;
}

@Component({
  selector: 'app-precondition-qualitative-modal',
  templateUrl: './precondition-qualitative.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreconditionQualitativeModal extends ModalComponent<ObjectMultiMap<number>> implements IPreconditionQualitativeModalOptions {
  @ViewChild('baseTable') baseTable: GenericSelectTable;
  @ViewChild('usedTable') usedTable: GenericSelectTable;

  @Input() basePmfmu: RulePmfmu;
  @Input() usedPmfmu: RulePmfmu;
  @Input() qvMap: ObjectMultiMap<number>;

  private _selectedBaseQVId: number;

  constructor(protected injector: Injector) {
    super(injector);
  }

  protected async afterInit() {
    // Initialize all associations
    await this.baseTable.ready();
    this.baseTable.showColumnsOnly(attributes.name);
    this.usedTable.showColumnsOnly(attributes.name);
    await this.baseTable.resetFilter();
  }

  protected dataToValidate(): Promise<ObjectMultiMap<number>> | ObjectMultiMap<number> {
    return this.qvMap;
  }

  async baseQualitativeValueSelected(event: AsyncTableElement<GenericReferential>) {
    if (event) {
      this._selectedBaseQVId = +event.currentData.id;
      await this.usedTable.setSelectedIds(this.qvMap[this._selectedBaseQVId]);
    }
  }

  usedQualitativeValueChanged(values: number[]) {
    if (!this._selectedBaseQVId) {
      throw new Error('precondition-qualitative.modal] no base qualitative value selected');
    }
    this.qvMap[this._selectedBaseQVId] = values;
  }
}
