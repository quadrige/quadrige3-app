import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { RulePmfmu } from '@app/referential/rule/pmfmu/rule-pmfmu.model';
import { arraySize, FormArrayHelper } from '@sumaris-net/ngx-components';
import { UntypedFormArray, UntypedFormGroup } from '@angular/forms';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { BaseGroupValidators } from '@app/shared/service/base-validator.service';
import { IModalOptions } from '@app/shared/model/options.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';

export interface IPreconditionNumericalModal extends IModalOptions {
  basePmfmu: RulePmfmu;
  usedPmfmu: RulePmfmu;
  values: IPreconditionNumerical[];
}

export interface IPreconditionNumerical {
  qualitativeValue: GenericReferential;
  min?: number;
  max?: number;
}

@Component({
  selector: 'app-precondition-numerical-modal',
  templateUrl: './precondition-numerical.modal.html',
  styleUrls: ['./precondition-numerical.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreconditionNumericalModal extends ModalComponent<IPreconditionNumerical[]> implements IPreconditionNumericalModal {
  @Input() basePmfmu: RulePmfmu;
  @Input() usedPmfmu: RulePmfmu;
  @Input() values: IPreconditionNumerical[];

  helper: FormArrayHelper<IPreconditionNumerical>;
  formArray: UntypedFormArray;

  constructor(protected injector: Injector) {
    super(injector);
    this.setForm(this.formBuilder.group({}));

    this.formArray = this.formBuilder.array([]);
    this.form.addControl('values', this.formArray);
    this.helper = new FormArrayHelper<IPreconditionNumerical>(
      this.formArray,
      (value) =>
        this.formBuilder.group(
          {
            qualitativeValue: [value?.qualitativeValue || null],
            min: [value?.min || null],
            max: [value?.max || null],
          },
          {
            validators: [BaseGroupValidators.minMax('min', 'max')],
          }
        ),
      (v1, v2) => v1.qualitativeValue.id === v2.qualitativeValue.id,
      (value) => !value.min && !value.max
    );
  }

  get itemControls(): UntypedFormGroup[] {
    return this.formArray && (this.formArray.controls as UntypedFormGroup[]);
  }

  protected afterInit() {
    // Affect all associations
    this.helper.resize(arraySize(this.values));
    this.value = { values: this.values };
  }

  protected dataToValidate(): Promise<IPreconditionNumerical[]> | IPreconditionNumerical[] {
    if (!this.valid) {
      throw new Error('[precondition-numerical-modal] invalid values');
    }
    return this.formArray.value;
  }
}
