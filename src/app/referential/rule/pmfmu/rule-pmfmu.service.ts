import { Injectable } from '@angular/core';
import { UnfilteredEntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { RulePmfmu, RulePmfmuUtils } from '@app/referential/rule/pmfmu/rule-pmfmu.model';
import { SortDirection } from '@angular/material/sort';

@Injectable({ providedIn: 'root' })
export class RulePmfmuService extends UnfilteredEntitiesMemoryService<RulePmfmu> {
  constructor() {
    super(RulePmfmu, {
      equals: (d1, d2) => RulePmfmuUtils.samePmfmu(d1, d2),
    });
  }

  sort(data: RulePmfmu[], sortBy?: string, sortDirection?: SortDirection): RulePmfmu[] {
    return data; // No sort
  }
}
