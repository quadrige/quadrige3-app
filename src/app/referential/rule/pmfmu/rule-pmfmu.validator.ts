import { Injectable } from '@angular/core';
import { AbstractControlOptions, UntypedFormBuilder, Validators } from '@angular/forms';
import {
  ReferentialGroupAsyncValidators,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import { SharedValidators } from '@sumaris-net/ngx-components';
import { RulePmfmu } from '@app/referential/rule/pmfmu/rule-pmfmu.model';

@Injectable({ providedIn: 'root' })
export class RulePmfmuValidatorService extends ReferentialValidatorService<RulePmfmu> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: RulePmfmu, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [data?.id || null],
      ruleId: [data?.ruleId || null],
      pmfmuId: [data?.pmfmuId || null],
      parameter: [data?.parameter || null, Validators.compose([Validators.required, SharedValidators.entity])],
      matrix: [data?.matrix || null, SharedValidators.entity],
      fraction: [data?.fraction || null, SharedValidators.entity],
      method: [data?.method || null, SharedValidators.entity],
      unit: [data?.unit || null, SharedValidators.entity],
      updateDate: [data?.updateDate || null],
      entityName: [data?.entityName || null, Validators.required],
      statusId: [null],
    };
  }

  getFormGroupOptions(data?: RulePmfmu, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      asyncValidators: ReferentialGroupAsyncValidators.checkAttributesAlreadyExists(
        undefined,
        this.dataSource,
        opts,
        data,
        ['parameter?.id', 'matrix?.id', 'fraction?.id', 'method?.id', 'unit?.id'],
        'REFERENTIAL.RULE_PMFMU.ERROR.ALREADY_EXISTS'
      ),
    };
  }
}
