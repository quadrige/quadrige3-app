import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { isNotEmptyArray, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { Pmfmu } from '@app/referential/pmfmu/pmfmu.model';
import { Parameter } from '@app/referential/parameter/parameter.model';
import { RulePmfmuValidatorService } from '@app/referential/rule/pmfmu/rule-pmfmu.validator';
import { RulePmfmuService } from '@app/referential/rule/pmfmu/rule-pmfmu.service';
import { RulePmfmu, RulePmfmuUtils } from '@app/referential/rule/pmfmu/rule-pmfmu.model';
import { IParameterAddOptions } from '@app/referential/parameter/parameter.add.table';
import { IAddResult } from '@app/shared/model/options.model';
import { EntityAddModal } from '@app/selection/entity.add.modal';
import { IPmfmuAddOptions } from '@app/referential/pmfmu/pmfmu.add.table';
import { uniqueValue } from '@app/shared/utils';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { ComboColumnComponent } from '@app/shared/component/column/combo-column.component';

@Component({
  selector: 'app-rule-pmfmu-table',
  templateUrl: './rule-pmfmu.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RulePmfmuTable extends ReferentialMemoryTable<RulePmfmu, number, any, any, RulePmfmuValidatorService> implements OnInit, AfterViewInit {
  @Input() programIds: string[];
  @Output() afterAddedRulePmfmu = new EventEmitter<RulePmfmu[]>();

  @ViewChild('matrixColumn') matrixColumn: ComboColumnComponent;
  @ViewChild('fractionColumn') fractionColumn: ComboColumnComponent;

  constructor(
    protected injector: Injector,
    protected _entityService: RulePmfmuService,
    protected validatorService: RulePmfmuValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['parameter', 'matrix', 'fraction', 'method', 'unit', 'updateDate']).concat(RESERVED_END_COLUMNS),
      RulePmfmu,
      _entityService,
      validatorService
    );

    this.titleI18n = 'REFERENTIAL.RULE.PMFMU';
    this.showTitle = false;
    this.i18nColumnPrefix = 'REFERENTIAL.RULE_PMFMU.';
    this.logPrefix = '[rule-pmfmu-table]';
  }

  ngOnInit() {
    this.subTable = true;
    this.showPaginator = false; // important !

    super.ngOnInit();

    // Parameter combo
    this.registerAutocompleteField('parameter', {
      ...this.referentialOptions.parameter,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Parameter',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Matrix combo
    this.registerAutocompleteField('matrix', {
      ...this.referentialOptions.matrix,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Matrix',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
      showAllOnFocus: true,
    });

    // Fraction combo
    this.registerAutocompleteField('fraction', {
      ...this.referentialOptions.fraction,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Fraction',
        criterias: [
          {
            ...this.defaultReferentialCriteria,
            parentId: undefined,
          },
        ],
      }),
      showAllOnFocus: true,
    });

    // Method combo
    this.registerAutocompleteField('method', {
      ...this.referentialOptions.method,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Method',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // Unit combo
    this.registerAutocompleteField('unit', {
      ...this.referentialOptions.unit,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'Unit',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    this.registerSubscription(
      this.registerCellValueChanges('matrix').subscribe((matrix) => {
        this.autocompleteFields.fraction.filter.criterias[0].parentId = matrix?.id;
        this.fractionColumn?.autocompleteField?.reloadItems();
      })
    );

    this.registerSubscription(
      this.registerCellValueChanges('fraction').subscribe((fraction) => {
        this.autocompleteFields.matrix.filter.criterias[0].parentId = fraction?.id;
        this.matrixColumn?.autocompleteField?.reloadItems();
      })
    );
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  canGoUp(): boolean {
    return this.filterIsEmpty && (this.singleSelectedRow?.id > 0 || false);
  }

  async goUp() {
    const selectedRow = this.singleSelectedRow;
    if (selectedRow && (await this.confirmEditCreate(undefined, selectedRow))) {
      await this.moveRow(selectedRow.id, -1);
      this.markAsDirty();
      await this.updateValue();
    }
  }

  canGoDown(): boolean {
    return this.filterIsEmpty && (this.singleSelectedRow?.id < this.visibleRowCount - 1 || false);
  }

  async goDown() {
    const selectedRow = this.singleSelectedRow;
    if (selectedRow && (await this.confirmEditCreate(undefined, selectedRow))) {
      await this.moveRow(selectedRow.id, 1);
      this.markAsDirty();
      await this.updateValue();
    }
  }

  async updateValue() {
    const rows = this.getRows();
    await this.setValue(rows.sort((row) => row.id).map((row) => row.currentData));
    this.afterAddedRulePmfmu.emit(this.value);
  }

  async openAddPmfmuModal(event: MouseEvent) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IPmfmuAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: Pmfmu.entityName,
        titleI18n: 'REFERENTIAL.RULE_PMFMU.ADD_PMFMU',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          programFilter: { includedIds: this.programIds },
          // parameterFilter: { excludedIds: this.existingParameterIds() }, // Filter removed because not consistent (Mantis #61215)
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      await this.save();
      const newValue = this.value?.slice() || [];
      const toAdd = data.added
        .map(Pmfmu.fromObject)
        .map((pmfmu: Pmfmu) => new RulePmfmu(pmfmu.parameter, pmfmu.matrix, pmfmu.fraction, pmfmu.method, pmfmu.unit, pmfmu.id));
      // Set new values (excluding already present PMFMU)
      newValue.push(...toAdd.filter((valueToAdd) => !newValue.find((value) => RulePmfmuUtils.samePmfmu(value, valueToAdd))));
      await this.setValue(newValue);
      this.markAsDirty();
      this.afterAddedRulePmfmu.emit(newValue);
    }
  }

  async openAddParameterModal(event: MouseEvent) {
    event?.preventDefault();

    if (!this.canEdit || !(await this.confirmEditCreate())) {
      return;
    }

    const { role, data } = await this.modalService.openModal<IParameterAddOptions, IAddResult>(
      EntityAddModal,
      {
        entityName: Parameter.entityName,
        titleI18n: 'REFERENTIAL.RULE_PMFMU.ADD_PARAMETER',
        addCriteria: {
          ...this.defaultReferentialCriteria,
          programFilter: { includedIds: this.programIds },
          excludedIds: this.existingParameterIds(),
        },
      },
      'modal-large'
    );

    if (role === 'validate' && isNotEmptyArray(data?.added)) {
      await this.save();
      const newValue = this.value?.slice() || [];
      const toAdd = data.added.map(Parameter.fromObject).map((parameter) => new RulePmfmu(parameter));
      // Set new values (excluding already present (alone) parameter
      newValue.push(...toAdd.filter((valueToAdd) => !newValue.find((value) => RulePmfmuUtils.samePmfmu(value, valueToAdd))));
      await this.setValue(newValue);
      this.markAsDirty();
      this.afterAddedRulePmfmu.emit(newValue);
    }
  }

  // protected methods

  protected existingParameterIds(): string[] {
    return this.value.map((value) => value.parameter.id.toString()).filter(uniqueValue);
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('parameter');
  }
}
