import { isMoment, Moment, now } from 'moment';
import { RulePmfmu } from '@app/referential/rule/pmfmu/rule-pmfmu.model';
import { Entity, EntityAsObjectOptions, EntityClass, fromDateISOString, ReferentialAsObjectOptions, toFloat } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';
import { Dates } from '@app/shared/dates';

@EntityClass({ typename: 'RuleFunctionVO' })
export class RuleFunction extends Entity<RuleFunction, string> {
  static entityName = 'RuleFunction';
  static fromObject: (source: any, opts?: any) => RuleFunction;

  name: string = null;

  constructor() {
    super(RuleFunction.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.name = source.name;
  }
}

@EntityClass({ typename: 'ControlledEntityVO' })
export class ControlledEntity extends Entity<ControlledEntity, string> {
  static entityName = 'ControlledEntity';
  static fromObject: (source: any, opts?: any) => ControlledEntity;

  name: string = null;

  constructor() {
    super(ControlledEntity.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.name = source.name;
  }
}

@EntityClass({ typename: 'ControlledAttributeVO' })
export class ControlledAttribute extends Entity<ControlledAttribute, string> {
  static entityName = 'ControlledAttribute';
  static fromObject: (source: any, opts?: any) => ControlledAttribute;

  name: string = null;
  entity: ControlledEntity = null;

  constructor() {
    super(ControlledAttribute.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.name = source.name;
    this.entity = ControlledEntity.fromObject(source.entity);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.entity = this.entity?.asObject(opts);
    return target;
  }
}

@EntityClass({ typename: 'ControlRuleVO' })
export class ControlRule extends Referential<ControlRule, string> {
  static entityName = 'ControlRule';
  static fromObject: (source: any, opts?: any) => ControlRule;

  active: boolean = null;
  blocking: boolean = null;
  controlledEntityId: string = null;
  controlledAttributeId: string = null;
  functionId: string = null;
  min?: number | Moment = null;
  max?: number | Moment = null;
  allowedValues?: string = null;
  errorMessage: string = null;
  pmfmus: RulePmfmu[] = null;
  preconditions: PreconditionControlRule[] = null;
  groups: GroupControlRule[] = null;

  constructor() {
    super(ControlRule.TYPENAME);
    this.entityName = ControlRule.entityName;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.entityName = ControlRule.entityName;
    this.active = source.active;
    this.blocking = source.blocking;
    this.controlledEntityId = source.controlledEntityId;
    this.controlledAttributeId = source.controlledAttributeId;
    this.functionId = source.functionId;
    switch (this.functionId) {
      case 'MIN_MAX': {
        this.min = toFloat(source.min);
        this.max = toFloat(source.max);
        break;
      }
      case 'MIN_MAX_DATE': {
        this.min = fromDateISOString(source.min);
        this.max = fromDateISOString(source.max);
        break;
      }
      default: {
        this.min = undefined;
        this.max = undefined;
      }
    }
    this.allowedValues = source.allowedValues;
    this.errorMessage = source.errorMessage;
    this.pmfmus = source.pmfmus?.map(RulePmfmu.fromObject) || [];
    this.preconditions = source.preconditions?.map(PreconditionControlRule.fromObject) || [];
    this.groups = source.groups?.map(GroupControlRule.fromObject) || [];
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    if (this.functionId === 'MIN_MAX') {
      target.min = this.min?.toString();
      target.max = this.max?.toString();
    } else if (this.functionId === 'MIN_MAX_DATE') {
      if (!!this.min) {
        const min = fromDateISOString(this.min);
        if (isMoment(min)) {
          target.min = Dates.toLocalDateString(min);
        } else {
          console.warn('[control-rule-model] min value should be a moment like', this.min);
        }
      }
      if (!!this.max) {
        const max = fromDateISOString(this.max);
        if (isMoment(max)) {
          target.max = Dates.toLocalDateString(max);
        } else {
          console.warn('[control-rule-model] max value should be a moment like', this.max);
        }
      }
    }
    target.pmfmus = this.pmfmus?.map((value) => value.asObject(opts));
    target.preconditions = this.preconditions?.map((value) => value.asObject(opts));
    target.groups = this.groups?.map((value) => value.asObject(opts));
    delete target.entityName;
    return target;
  }
}

@EntityClass({ typename: 'PreconditionControlRuleVO' })
export class PreconditionControlRule extends Entity<PreconditionControlRule> {
  static entityName = 'PreconditionControlRule';
  static fromObject: (source: any, opts?: any) => PreconditionControlRule;

  active: boolean = null;
  bidirectional: boolean = null;
  baseRule: ControlRule = null;
  usedRule: ControlRule = null;

  constructor() {
    super(PreconditionControlRule.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.active = source.active;
    this.bidirectional = source.bidirectional;
    this.baseRule = ControlRule.fromObject(source.baseRule);
    this.usedRule = ControlRule.fromObject(source.usedRule);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.baseRule = this.baseRule?.asObject(opts);
    target.usedRule = this.usedRule?.asObject(opts);
    return target;
  }
}

@EntityClass({ typename: 'GroupControlRuleVO' })
export class GroupControlRule extends Entity<GroupControlRule> {
  static entityName = 'GroupControlRule';
  static fromObject: (source: any, opts?: any) => GroupControlRule;

  label: string = null;
  active: boolean = null;
  or: boolean = null;
  rule: ControlRule = null;

  constructor() {
    super(GroupControlRule.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.label = source.label;
    this.active = source.active;
    this.or = source.or;
    this.rule = ControlRule.fromObject(source.rule);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.rule = this.rule?.asObject(opts);
    return target;
  }
}

export const isPmfmuMandatory = (controlRule: ControlRule): boolean =>
  [
    'MEASUREMENT_PMFMU',
    'MEASUREMENT_NUMERICAL_VALUE',
    'MEASUREMENT_QUALITATIVE_VALUE',
    'TAXON_MEASUREMENT_PMFMU',
    'TAXON_MEASUREMENT_NUMERICAL_VALUE',
    'TAXON_MEASUREMENT_QUALITATIVE_VALUE',
  ].includes(controlRule.controlledAttributeId);

export const isFunctionValidForAttribute = (functionId: string, attribute: string) => {
  if (!functionId || !attribute) return false;
  if (functionId === 'MIN_MAX') {
    return [
      'SURVEY_BOTTOM_DEPTH',
      'SURVEY_LATITUDE_MIN_LOCATION',
      'SURVEY_LATITUDE_MAX_LOCATION',
      'SURVEY_LONGITUDE_MAX_LOCATION',
      'SURVEY_LONGITUDE_MIN_LOCATION',
      'SURVEY_LATITUDE_MIN',
      'SURVEY_LATITUDE_MAX',
      'SURVEY_LONGITUDE_MIN',
      'SURVEY_LONGITUDE_MAX',
      'SURVEY_TIME',
      'SURVEY_INDIVIDUAL_COUNT',
      'SAMPLING_OPERATION_DEPTH',
      'SAMPLING_OPERATION_MIN_DEPTH',
      'SAMPLING_OPERATION_MAX_DEPTH',
      'SAMPLING_OPERATION_DEPTH_LEVEL',
      'SAMPLING_OPERATION_INDIVIDUAL_COUNT',
      'SAMPLING_OPERATION_LATITUDE_MIN',
      'SAMPLING_OPERATION_LATITUDE_MAX',
      'SAMPLING_OPERATION_LONGITUDE_MIN',
      'SAMPLING_OPERATION_LONGITUDE_MAX',
      'SAMPLING_OPERATION_LATITUDE_MIN_SURVEY',
      'SAMPLING_OPERATION_LATITUDE_MAX_SURVEY',
      'SAMPLING_OPERATION_LONGITUDE_MIN_SURVEY',
      'SAMPLING_OPERATION_LONGITUDE_MAX_SURVEY',
      'SAMPLING_OPERATION_SIZE',
      'SAMPLING_OPERATION_TIME',
      'SAMPLE_INDIVIDUAL_COUNT',
      'SAMPLE_SIZE',
      'MEASUREMENT_NUMERICAL_VALUE',
      'TAXON_MEASUREMENT_NUMERICAL_VALUE',
    ].includes(attribute);
  } else if (functionId === 'MIN_MAX_DATE') {
    return ['SURVEY_DATE', 'SURVEY_CONTROL_DATE', 'SURVEY_QUALIFICATION_DATE', 'SURVEY_UPDATE_DATE', 'SURVEY_VALIDATION_DATE'].includes(attribute);
  }
  // Other cases are valid
  return true;
};

export const newRuleIdSuffix = (): {
  index: number;
} => ({ index: Math.trunc(now() / 1000) });

export const isMinMaxRuleFunction = (functionId: string): boolean => {
  return ['MIN_MAX', 'MIN_MAX_DATE'].includes(functionId);
};
export const isComplexRuleFunction = (functionId: string): boolean => {
  return isPreconditionRuleFunction(functionId) || isGroupedRuleFunction(functionId);
};
export const isPreconditionRuleFunction = (functionId: string): boolean => {
  return ['PRECONDITION_QUALITATIVE', 'PRECONDITION_NUMERICAL'].includes(functionId);
};
export const isGroupedRuleFunction = (functionId: string): boolean => {
  return ['NOT_EMPTY_CONDITIONAL'].includes(functionId);
};
