import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { ControlRuleTable } from '@app/referential/rule/control-rule.table';
import { ControlRuleModule } from '@app/referential/rule/control-rule.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ControlRuleTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      profile: 'ADMIN',
      pathIdParam: 'ruleListId',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ControlRuleModule],
})
export class ControlRuleRoutingModule {}
