import { Injectable } from '@angular/core';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { ControlRule } from '@app/referential/rule/control-rule.model';
import { ControlRuleFilter } from '@app/referential/rule/filter/control-rule.filter.model';

@Injectable({ providedIn: 'root' })
export class ControlRuleService extends EntitiesMemoryService<ControlRule, ControlRuleFilter, string> {
  constructor() {
    super(ControlRule, ControlRuleFilter);
  }
}
