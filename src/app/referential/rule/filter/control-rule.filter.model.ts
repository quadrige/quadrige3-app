import { EntityAsObjectOptions, FilterFn } from '@sumaris-net/ngx-components';
import { ReferentialFilter, ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ControlRule } from '@app/referential/rule/control-rule.model';

export class ControlRuleFilterCriteria extends ReferentialFilterCriteria<ControlRule, string> {
  onlyActive: boolean = null;

  static fromObject(source: any, opts?: any): ControlRuleFilterCriteria {
    const target = new ControlRuleFilterCriteria();
    target.fromObject(source, opts);
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.onlyActive = source.onlyActive || false;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.onlyActive = this.onlyActive || false;
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (key === 'onlyActive') return this.onlyActive === true;
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }

  protected buildFilter(): FilterFn<ControlRule>[] {
    const target: FilterFn<ControlRule>[] = super.buildFilter();
    if (this.onlyActive) {
      target.push((data) => data.active === true);
    }
    return target;
  }
}

export class ControlRuleFilter extends ReferentialFilter<ControlRuleFilter, ControlRuleFilterCriteria, ControlRule, string> {
  static fromObject(source: any, opts?: any): ControlRuleFilter {
    const target = new ControlRuleFilter();
    target.fromObject(source, opts);
    return target;
  }

  criteriaFromObject(source: any, opts: any): ControlRuleFilterCriteria {
    return ControlRuleFilterCriteria.fromObject(source, opts);
  }
}
