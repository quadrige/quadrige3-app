import { ChangeDetectionStrategy, Component } from '@angular/core';
import { isNotNil } from '@sumaris-net/ngx-components';
import { ReferentialCriteriaFormComponent } from '@app/referential/component/referential-criteria-form.component';
import { PartialRecord } from '@app/shared/model/interface';
import { ControlRuleFilter, ControlRuleFilterCriteria } from '@app/referential/rule/filter/control-rule.filter.model';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-control-rule-filter-form',
  templateUrl: './control-rule.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ControlRuleFilterForm extends ReferentialCriteriaFormComponent<ControlRuleFilter, ControlRuleFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.id;
  }

  protected criteriaToQueryParams(criteria: ControlRuleFilterCriteria): PartialRecord<keyof ControlRuleFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    if (isNotNil(criteria.onlyActive)) params.onlyActive = criteria.onlyActive;
    return params;
  }
}
