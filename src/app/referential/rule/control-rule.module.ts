import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { ControlRuleTable } from '@app/referential/rule/control-rule.table';
import { RulePmfmuTable } from '@app/referential/rule/pmfmu/rule-pmfmu.table';
import { PmfmuModule } from '@app/referential/pmfmu/pmfmu.module';
import { ParameterModule } from '@app/referential/parameter/parameter.module';
import { PreconditionQualitativeModal } from '@app/referential/rule/precondition/precondition-qualitative.modal';
import { PreconditionNumericalModal } from '@app/referential/rule/precondition/precondition-numerical.modal';
import { ControlRuleFilterForm } from '@app/referential/rule/filter/control-rule.filter.form';
import { GenericSelectTable } from '@app/referential/generic/generic.select.table';
import { FunctionValidForAttributePipe, SimpleFunctionPipe } from '@app/referential/rule/control-rule.pipes';

@NgModule({
  imports: [ReferentialModule, PmfmuModule, ParameterModule, GenericSelectTable],
  declarations: [
    ControlRuleTable,
    ControlRuleFilterForm,
    RulePmfmuTable,
    PreconditionQualitativeModal,
    PreconditionNumericalModal,
    SimpleFunctionPipe,
    FunctionValidForAttributePipe,
  ],
  exports: [ControlRuleTable, ControlRuleFilterForm],
})
export class ControlRuleModule {}
