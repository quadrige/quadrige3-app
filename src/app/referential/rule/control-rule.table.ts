import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit, viewChild } from '@angular/core';
import {
  arraySize,
  isEmptyArray,
  isNil,
  isNotEmptyArray,
  isNotNil,
  LoadResult,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { UntypedFormGroup } from '@angular/forms';
import { lengthComment, ReferentialValidatorOptions } from '@app/referential/service/referential-validator.service';
import { MatTabNav } from '@angular/material/tabs';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';
import {
  ControlledAttribute,
  ControlledEntity,
  ControlRule,
  isComplexRuleFunction,
  isGroupedRuleFunction,
  isPmfmuMandatory,
  isPreconditionRuleFunction,
  newRuleIdSuffix,
  PreconditionControlRule,
  RuleFunction,
} from '@app/referential/rule/control-rule.model';
import { ControlRuleValidatorService } from '@app/referential/rule/control-rule.validator';
import { ControlRuleService } from '@app/referential/rule/control-rule.service';
import { RuleList } from '@app/referential/rule-list/rule-list.model';
import { RuleListService } from '@app/referential/rule-list/rule-list.service';
import { RulePmfmuTable } from '@app/referential/rule/pmfmu/rule-pmfmu.table';
import { RulePmfmu } from '@app/referential/rule/pmfmu/rule-pmfmu.model';
import {
  IPreconditionQualitativeModalOptions,
  PreconditionQualitativeModal,
} from '@app/referential/rule/precondition/precondition-qualitative.modal';
import { ObjectMultiMap } from '@app/shared/multimap';
import {
  IPreconditionNumerical,
  IPreconditionNumericalModal,
  PreconditionNumericalModal,
} from '@app/referential/rule/precondition/precondition-numerical.modal';
import { PmfmuService } from '@app/referential/pmfmu/pmfmu.service';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { TextFrameComponent } from '@app/shared/component/text/text-frame.component';
import { GenericReferential } from '@app/referential/generic/generic.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { ControlRuleFilter, ControlRuleFilterCriteria } from '@app/referential/rule/filter/control-rule.filter.model';
import { ReferentialMemoryTable } from '@app/referential/table/referential.memory.table';
import { Utils } from '@app/shared/utils';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

type RuleView = 'rulePmfmuTable';

@Component({
  selector: 'app-control-rule-table',
  templateUrl: './control-rule.table.html',
  styleUrls: ['./control-rule.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ControlRuleTable
  extends ReferentialMemoryTable<ControlRule, string, ControlRuleFilter, ControlRuleFilterCriteria, ControlRuleValidatorService>
  implements OnInit, AfterViewInit
{
  detailAreaTabs = viewChild<MatTabNav>('detailAreaTabs');
  ruleMessageComponent = viewChild<TextFrameComponent>('ruleMessageComponent');
  rulePmfmuTable = viewChild<RulePmfmuTable>('rulePmfmuTable');

  readonly rulePmfmusMenuItem: IEntityMenuItem<ControlRule, RuleView> = {
    title: 'REFERENTIAL.RULE.PMFMU',
    attribute: 'pmfmus',
    view: 'rulePmfmuTable',
  };
  readonly detailAreaMenuItems: IEntityMenuItem<ControlRule, RuleView>[] = [this.rulePmfmusMenuItem];
  selectedDetailAreaMenuItem: IEntityMenuItem<ControlRule, RuleView> = this.detailAreaMenuItems[0];
  detailAreaEnabled = false;
  messageAreaEnabled = false;
  defaultBackHref = '/management/RuleList';
  descriptionMaxLength = lengthComment;
  functions: RuleFunction[] = [];
  functionsById: Record<string, RuleFunction> = {};
  controlledEntities: ControlledEntity[] = [];
  controlledEntitiesById: Record<string, ControlledEntity> = {};
  controlledAttributesById: Record<string, ControlledAttribute> = {};
  controlledAttributesByEntityType: Record<string, ControlledAttribute[]> = {};

  private readonly _ruleListId: string;
  private _ruleList: RuleList;

  constructor(
    protected injector: Injector,
    protected _entityService: ControlRuleService,
    protected validatorService: ControlRuleValidatorService,
    protected ruleListService: RuleListService,
    protected pmfmuService: PmfmuService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'functionId',
        'controlledEntityId',
        'controlledAttributeId',
        'active',
        'blocking',
        'min',
        'max',
        'allowedValues',
        'description',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      ControlRule,
      _entityService,
      validatorService
    );

    // Get program id from route
    this._ruleListId = this.route.snapshot.params.ruleListId;

    // register custom error message
    // const customErrors: PropertiesMap = {};
    // customErrors[REFERENTIAL_ERROR_CODES.DELETE_FORBIDDEN] = 'REFERENTIAL.STRATEGY.ERROR.ATTACHED_DATA';
    // customErrors[REFERENTIAL_ERROR_CODES.ATTACHED_DATA] = 'REFERENTIAL.STRATEGY.ERROR.ATTACHED_DATA';
    // this.registerCustomErrors(customErrors);

    this.i18nColumnPrefix = 'REFERENTIAL.RULE.';
    this.defaultSortBy = 'id';
    this.logPrefix = '[control-rule-table]';
  }

  get rightPanelButtonAccent(): boolean {
    if (!this.rightPanelVisible && this.singleSelectedRow?.currentData) {
      const data: ControlRule = this.singleSelectedRow.currentData;
      return !!data.errorMessage;
    }
    return false;
  }

  get ruleListId(): string {
    return this._ruleListId;
  }

  get programIds(): string[] {
    return this._ruleList?.programIds;
  }

  ngOnInit() {
    super.ngOnInit();

    this.saveBeforeDelete = false;

    // Listen function change -> update controlled entity and attribute
    this.registerSubscription(
      this.registerCellValueChanges('functionId', 'functionId', false).subscribe((functionType) => {
        if (!this.singleEditingRow) return;
        if (this.debug) {
          console.debug(`${this.logPrefix} functionType has changed:`, functionType);
        }
        this.resetError();
        this.validatorService.updateFormGroup(this.singleEditingRow.validator, functionType);
      })
    );

    this.registerSubscription(
      this.registerCellValueChanges('controlledEntityId', 'controlledEntityId', false).subscribe((controlledEntityId) => {
        if (!this.singleEditingRow || !this.singleEditingRow.dirty) return;
        if (this.debug) {
          console.debug(`${this.logPrefix} controlledEntityId has changed:`, controlledEntityId);
        }
        this.singleEditingRow.validator.patchValue({ controlledAttributeId: undefined });
      })
    );

    this.registerSubscription(
      this.registerCellValueChanges('controlledAttributeId', 'controlledAttributeId', false).subscribe((controlledAttributeId) => {
        if (!this.singleEditingRow || !this.singleEditingRow.dirty) return;
        if (this.debug) {
          console.debug(`${this.logPrefix} controlledAttributeId has changed:`, controlledAttributeId);
        }
        const controlRule = this.singleEditingRow.currentData;
        // reset pmfmus if not mandatory
        if (!isPmfmuMandatory(controlRule) && !isPreconditionRuleFunction(controlRule.functionId) && !isGroupedRuleFunction(controlRule.functionId)) {
          this.singleEditingRow.validator.patchValue({ pmfmus: undefined });
        }
        this.loadDetailArea(this.singleEditingRow);
      })
    );

    // Load rule list
    this.loadControlRules();
  }

  updateView(res: LoadResult<ControlRule> | undefined, opts?: { emitEvent?: boolean }): Promise<void> {
    return super.updateView(res, { emitEvent: false, ...opts });
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.restoreRightPanel(true, 40);
    this.restoreBottomPanel(true, 30);
  }

  async onAfterSelectionChange(row?: AsyncTableElement<ControlRule>) {
    await super.onAfterSelectionChange(row);
    await this.loadDetailArea(row);
  }

  ifComplexFunction(): ReadOnlyIfFn {
    return (row) => isComplexRuleFunction(row.currentData?.functionId);
  }

  updatePermission() {
    // Manage user rights :
    // By default, all user have edition write to allow further finer checks
    this.canEdit = this.hasRightOnRuleList();
    // Refresh other controls
    this.refreshTabs();

    this.markForCheck();
  }

  async resetFilter(filter?: ControlRuleFilter, opts?: ISetFilterOptions) {
    filter = this.asFilter(filter);
    filter.patch({ onlyActive: false });
    await super.resetFilter(filter, opts);
  }

  asFilter(source?: any): ControlRuleFilter {
    const filter = super.asFilter(source);
    // Force parentId
    filter.patch({ parentId: this.ruleListId });
    return filter;
  }

  async confirmEditCreate(event?: Event, row?: AsyncTableElement<ControlRule>): Promise<boolean> {
    row = row || this.singleEditingRow;

    // confirm current detail table row
    if (!(await this.confirmDetail(row))) {
      return false;
    }

    return super.confirmEditCreate(event, row);
  }

  ruleMessageTextChanged(event: string) {
    if (isNil(event) || this.loading) return;
    const row = this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      console.warn(`${this.logPrefix} no row to update`);
      return;
    }
    if (this.debug) {
      console.debug(`${this.logPrefix} current error message:`, row.currentData.errorMessage);
    }
    row.validator.patchValue({ errorMessage: event });
    this.markRowAsDirty(row);
    if (this.debug) {
      console.debug(`${this.logPrefix} new error message:`, row.currentData.errorMessage);
    }
  }

  async confirmDetail(row?: AsyncTableElement<ControlRule>): Promise<boolean> {
    if (row) {
      if (this.selectedDetailAreaMenuItem.view === 'rulePmfmuTable' && this.rulePmfmuTable().dirty) {
        if (await this.rulePmfmuTable().save()) {
          await this.patchRulePmfmus(this.rulePmfmuTable().value, row);
        } else {
          return false;
        }
      }
    }
    return true;
  }

  async patchRulePmfmus(values: RulePmfmu[], row?: AsyncTableElement<ControlRule>) {
    if (this.canEdit) {
      this.patchRow(this.rulePmfmusMenuItem.attribute, values);

      row = row || this.singleEditingRow || this.singleSelectedRow;
      if (row) {
        // Build grouped rules
        if (isGroupedRuleFunction(row.currentData.functionId)) {
          await this.validatorService.buildNotEmptyConditionalGroupsFromGroupedRule(row.validator);
        }

        // Clear preconditions when the pmfmus are not valid (Mantis #46663)
        if (isPreconditionRuleFunction(row.currentData.functionId) && arraySize(values) < 2) {
          row.validator.patchValue({ preconditions: undefined });
        }
      }
    }
  }

  async openPreconditionQualitativeModal(event: UIEvent, row: AsyncTableElement<ControlRule>) {
    event?.stopPropagation();
    event?.preventDefault();
    if (!(await this.clickRow(undefined, row))) return;
    if (this.canEdit) {
      if (this.rulePmfmuTable().dirty) {
        await this.rulePmfmuTable().save();
        await this.patchRulePmfmus(this.rulePmfmuTable().value, row);
      }
      if (!(await this.confirmEditCreate())) {
        if (row.validator.controls.preconditions.hasError('emptyPreconditions') && !row.validator.hasError('invalidPmfmus')) {
          console.debug(`${this.logPrefix} Force row confirmation`);
          this.resetError();
        } else {
          return;
        }
      }
    }
    const controlRule: ControlRule = row.currentData;
    if (arraySize(controlRule.pmfmus) !== 2) {
      console.error(`${this.logPrefix} The control rule must have 2 pmfmus, should not happened`);
      return;
    }

    // build qualitative value map
    const qvMap: ObjectMultiMap<number> = {};
    controlRule.preconditions?.forEach((precondition) => {
      precondition.baseRule.allowedValues.split(',').forEach((baseQV) => {
        qvMap[+baseQV] = precondition.usedRule.allowedValues.split(',').map((value) => +value);
      });
    });

    // Open modal
    const { data, role } = await this.modalService.openModal<IPreconditionQualitativeModalOptions, ObjectMultiMap<number>>(
      PreconditionQualitativeModal,
      {
        basePmfmu: controlRule.pmfmus[0],
        usedPmfmu: controlRule.pmfmus[1],
        qvMap,
        canEdit: this.canEdit,
      },
      'modal-large'
    );

    if (role === 'validate' && data) {
      const result: PreconditionControlRule[] = [];
      const suffix = newRuleIdSuffix();
      // build preconditions
      for (const baseValue of Object.keys(data)) {
        const usedValues: any[] = data[baseValue];
        if (isNotEmptyArray(usedValues)) {
          let precondition: PreconditionControlRule = (controlRule.preconditions || []).find((value) => value.baseRule.allowedValues === baseValue);
          if (!precondition) {
            // create
            precondition = PreconditionControlRule.fromObject({
              bidirectional: true,
            });
          }
          precondition.active = controlRule.active;

          let baseRule = precondition.baseRule;
          if (!baseRule) {
            baseRule = ControlRule.fromObject({
              id: await this.ruleListService.nextRuleId(controlRule.id, suffix),
              active: false,
              blocking: false,
              functionId: 'IN',
              controlledEntityId: 'MEASUREMENT',
              controlledAttributeId: 'MEASUREMENT_QUALITATIVE_VALUE',
              pmfmus: [controlRule.pmfmus[0]],
              allowedValues: baseValue,
            });
            precondition.baseRule = baseRule;
          }
          let usedRule = precondition.usedRule;
          if (!usedRule) {
            usedRule = ControlRule.fromObject({
              id: await this.ruleListService.nextRuleId(controlRule.id, suffix),
              active: false,
              blocking: false,
              functionId: 'IN',
              controlledEntityId: 'MEASUREMENT',
              controlledAttributeId: 'MEASUREMENT_QUALITATIVE_VALUE',
              pmfmus: [controlRule.pmfmus[1]],
            });
            precondition.usedRule = usedRule;
          }
          usedRule.allowedValues = usedValues.join(',');

          result.push(precondition);
        }
      }

      this.patchRow('preconditions', result, row);
      this.refreshPmfmuTableEnabled(row);
    }
  }

  async openPreconditionNumericalModal(event: UIEvent, row: AsyncTableElement<ControlRule>) {
    event?.stopPropagation();
    event?.preventDefault();
    if (!(await this.clickRow(undefined, row))) return;
    if (this.canEdit) {
      if (this.rulePmfmuTable().dirty) {
        await this.rulePmfmuTable().save();
        await this.patchRulePmfmus(this.rulePmfmuTable().value, row);
      }
      if (!(await this.confirmEditCreate())) {
        if (row.validator.controls.preconditions.hasError('emptyPreconditions') && !row.validator.hasError('invalidPmfmus')) {
          console.debug(`${this.logPrefix} Force row confirmation`);
          this.resetError();
        } else {
          return;
        }
      }
    }
    const controlRule: ControlRule = row.currentData;
    if (arraySize(controlRule.pmfmus) !== 2) {
      console.error(`${this.logPrefix} The control rule must have 2 pmfmus, should not happened`);
      return;
    }

    // Get qualitative values for base pmfmu
    const basePmfmuId = controlRule.pmfmus[0].pmfmuId;
    const qualitativeValues: GenericReferential[] =
      (
        await this.referentialGenericService.loadPage(
          {}, // no page
          GenericReferentialFilter.fromObject({
            entityName: 'PmfmuQualitativeValue',
            criterias: [{ parentId: basePmfmuId.toString() }],
          })
        )
      ).data || [];

    const values: IPreconditionNumerical[] = EntityUtils.sort(qualitativeValues, 'name').map((qualitativeValue) => {
      const existingPrecondition = controlRule.preconditions?.find((precondition) => precondition.baseRule.allowedValues === qualitativeValue.id);
      return {
        qualitativeValue,
        min: existingPrecondition?.usedRule.min as number,
        max: existingPrecondition?.usedRule.max as number,
      };
    });

    // Open modal
    const { data, role } = await this.modalService.openModal<IPreconditionNumericalModal, IPreconditionNumerical[]>(
      PreconditionNumericalModal,
      {
        basePmfmu: controlRule.pmfmus[0],
        usedPmfmu: controlRule.pmfmus[1],
        values,
        canEdit: this.canEdit,
      },
      ['modal-full-height', 'modal-medium']
    );

    if (role === 'validate' && data) {
      const result: PreconditionControlRule[] = [];
      const suffix = newRuleIdSuffix();
      // build preconditions
      for (const preconditionNumerical of data) {
        // only if there is a min or/and max values
        if (isNotNil(preconditionNumerical.min) || isNotNil(preconditionNumerical.max)) {
          let precondition: PreconditionControlRule = (controlRule.preconditions || []).find(
            (value) => value.baseRule.allowedValues === preconditionNumerical.qualitativeValue.id.toString()
          );
          if (!precondition) {
            // create
            precondition = PreconditionControlRule.fromObject({
              bidirectional: true,
            });
          }
          precondition.active = controlRule.active;

          let baseRule = precondition.baseRule;
          if (!baseRule) {
            baseRule = ControlRule.fromObject({
              id: await this.ruleListService.nextRuleId(controlRule.id, suffix),
              active: false,
              blocking: false,
              functionId: 'IN',
              controlledEntityId: 'MEASUREMENT',
              controlledAttributeId: 'MEASUREMENT_QUALITATIVE_VALUE',
              pmfmus: [controlRule.pmfmus[0]],
              allowedValues: preconditionNumerical.qualitativeValue.id,
            });
            precondition.baseRule = baseRule;
          }
          let usedRule = precondition.usedRule;
          if (!usedRule) {
            usedRule = ControlRule.fromObject({
              id: await this.ruleListService.nextRuleId(controlRule.id, suffix),
              active: false,
              blocking: false,
              functionId: 'MIN_MAX',
              controlledEntityId: 'MEASUREMENT',
              controlledAttributeId: 'MEASUREMENT_NUMERICAL_VALUE',
              pmfmus: [controlRule.pmfmus[1]],
            });
            precondition.usedRule = usedRule;
          }
          usedRule.min = preconditionNumerical.min;
          usedRule.max = preconditionNumerical.max;

          result.push(precondition);
        }
      }

      this.patchRow('preconditions', result, row);
      this.refreshPmfmuTableEnabled(row);
    }
  }

  async detailAreaTabChange(event: UIEvent, detailTab: IEntityMenuItem<ControlRule, RuleView>) {
    if (!detailTab) {
      throw new Error(`Cannot determinate the type of bottom area`);
    }
  }

  async deleteRows(event: UIEvent | null, rows: AsyncTableElement<ControlRule>[], opts?: { interactive?: boolean }): Promise<number> {
    const deleted = await super.deleteRows(event, rows, opts);
    if (deleted) {
      await this.save();
    }
    return deleted;
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    let continueSave = true;
    let previousRowData: ControlRule;
    if (this.rulePmfmuTable().dirty) {
      continueSave = await this.rulePmfmuTable().save();
      if (continueSave) {
        await this.patchRulePmfmus(this.rulePmfmuTable().value);
      } else {
        this.selectedDetailAreaMenuItem = this.rulePmfmusMenuItem;
        this.refreshTabs();
      }
    }
    if (continueSave) {
      // Save the control rules table
      previousRowData = (this.singleEditingRow || this.singleSelectedRow)?.currentData;
      continueSave = await super.save({ keepEditing: false });
    }
    if (continueSave) {
      // Perform save on rule list
      try {
        this.markAsSaving();
        // Affect control rules to rule list
        this._ruleList.controlRules = this.value;
        // Save the rule list
        this._ruleList = await this.ruleListService.save(this._ruleList);
        return true;
      } catch (e) {
        this.markAsError(e.message || e);
        return false;
      } finally {
        this.markAsSaved();
        await this.loadControlRules();
        await this.selectRowByData(previousRowData);
      }
    }
    return false;
  }

  // protected methods

  protected async loadControlledAttributes() {
    if (isNotEmptyArray(this.controlledEntities)) return;

    // Load all rule functions
    this.functions = await this.ruleListService.ruleFunctions();
    this.functionsById = Utils.toRecord(this.functions, 'id');

    // Load all controlled attributes
    const controlledAttributes = await this.ruleListService.controlledAttributes();

    for (const controlledAttribute of controlledAttributes) {
      const controlledEntityId = controlledAttribute.entity.id;
      if (!Object.keys(this.controlledAttributesByEntityType).includes(controlledEntityId)) {
        this.controlledAttributesByEntityType[controlledEntityId] = [];
        this.controlledEntities.push(controlledAttribute.entity);
      }
      this.controlledAttributesByEntityType[controlledEntityId].push(controlledAttribute);
    }

    // Build records
    this.controlledEntitiesById = Utils.toRecord(this.controlledEntities, 'id');
    this.controlledAttributesById = Utils.toRecord(controlledAttributes, 'id');
  }

  protected async loadControlRules() {
    await this.loadControlledAttributes();
    // Clear first
    await this.setValue(undefined);

    this.markAsLoading();
    this._ruleList = await this.ruleListService.load(this.ruleListId);
    if (!this._ruleList) {
      throw new Error(`${this.logPrefix} The rule list ${this.ruleListId} not found, can't continue !`);
    }
    // Affect table
    await this.setValue(this._ruleList.controlRules);
    this.updatePermission();
    this.markAsLoaded();
  }

  protected async loadDetailArea(row?: AsyncTableElement<ControlRule>) {
    // get current row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      if (this.debug) {
        console.debug(`${this.logPrefix} no row to load`);
      }
      this.detailAreaEnabled = false;
      this.messageAreaEnabled = false;
      return;
    }

    // Register sub tables
    this.registerSubForm(this.rulePmfmuTable());

    // Load selected data
    this.ruleMessageComponent().value = row.currentData.errorMessage;
    await this.rulePmfmuTable().setValue((row.currentData?.pmfmus || []).slice());
    this.refreshPmfmuTableEnabled(row);
    this.messageAreaEnabled = this.canEdit;
  }

  protected refreshPmfmuTableEnabled(row?: AsyncTableElement<ControlRule>) {
    this.detailAreaEnabled = this.canEdit && isPmfmuMandatory(row.currentData) && isEmptyArray(row.currentData.preconditions);
  }

  protected getRowValidator(row?: AsyncTableElement<ControlRule>): UntypedFormGroup {
    return super.getRowValidator(row, <ReferentialValidatorOptions>{
      criteria: { parentId: this.ruleListId },
    });
  }

  protected async defaultNewRowValue() {
    const rows = this.getRows();
    let maxSuffix = 0;
    // Find existing rule ids
    if (isNotEmptyArray(rows)) {
      maxSuffix = rows
        .map((value) => value.currentData)
        .filter((value) => value.id?.startsWith(this.ruleListId) && value.id?.search(`^${this.ruleListId}_\\d+$`) !== -1)
        .map((value) => value.id.substring(value.id.lastIndexOf('_') + 1))
        .map((value) => +value)
        .reduce((previousValue, currentValue) => Math.max(previousValue || 0, currentValue || 0), 0);
    }

    const nextId = await this.ruleListService.nextRuleId(this.ruleListId, { index: maxSuffix + 1 });
    return {
      ...(await super.defaultNewRowValue()),
      id: nextId,
    };
  }

  protected hasRightOnRuleList(): boolean {
    if (!this._ruleList) {
      return false;
    }

    // Admins or users with manager right
    return (
      this.permissionService.hasSomeManagementPermission ||
      (this.accountService.isUser() &&
        (this._ruleList.responsibleUserIds.includes(this.accountService.person.id) ||
          this._ruleList.responsibleDepartmentIds.includes(this.accountService.department.id)))
    );
  }

  protected refreshTabs() {
    setTimeout(() => {
      this.detailAreaTabs()?.updatePagination();
      this.detailAreaTabs()?._alignInkBarToSelectedTab();
    });
  }

  protected updateTitle() {
    this.title = ['MENU.REFERENTIAL.MANAGEMENT', 'REFERENTIAL.RULE_LIST.TITLE', 'REFERENTIAL.RULE_LIST.RULES']
      .map((value) => this.translate.instant(value, { ruleListId: this.ruleListId }))
      .join(' &rArr; ');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('functionId', 'controlledEntityId', 'controlledAttributeId');
  }
}
