import { Injectable } from '@angular/core';
import {
  AbstractControlOptions,
  AsyncValidatorFn,
  UntypedFormBuilder,
  UntypedFormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { arraySize, IEntitiesTableDataSource, IEntity, isEmptyArray, toBoolean, toNumber } from '@sumaris-net/ngx-components';
import {
  lengthComment,
  lengthLabel,
  ReferentialValidatorOptions,
  ReferentialValidatorService,
} from '@app/referential/service/referential-validator.service';
import {
  ControlRule,
  isFunctionValidForAttribute,
  isGroupedRuleFunction,
  isMinMaxRuleFunction,
  isPmfmuMandatory,
  isPreconditionRuleFunction,
  newRuleIdSuffix,
} from '@app/referential/rule/control-rule.model';
import { isMoment } from 'moment';
import { RuleListService } from '@app/referential/rule-list/rule-list.service';
import { Observable, timer } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ControlRuleValidatorService extends ReferentialValidatorService<ControlRule, string> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected ruleListService: RuleListService
  ) {
    super(formBuilder);
  }

  getRowValidator(data?: ControlRule, opts?: ReferentialValidatorOptions): UntypedFormGroup {
    const validator = super.getRowValidator(data, opts);
    if (data?.functionId) {
      this.updateFormGroup(validator, data.functionId);
    }
    return validator;
  }

  getFormGroupConfig(data?: ControlRule, opts?: ReferentialValidatorOptions): { [key: string]: any } {
    return {
      id: [
        data?.id || null,
        Validators.compose([Validators.required, Validators.maxLength(lengthLabel)]),
        this.checkIdAlreadyExists(this.ruleListService, this.dataSource, opts, data),
      ],
      description: [data?.description || null, Validators.maxLength(lengthComment)],
      errorMessage: [data?.errorMessage || null, Validators.maxLength(lengthComment)],
      active: [toBoolean(data?.active, true)],
      blocking: [toBoolean(data?.blocking, false)],
      functionId: [data?.functionId || null, Validators.required],
      controlledEntityId: [data?.controlledEntityId || null, Validators.required],
      controlledAttributeId: [data?.controlledAttributeId || null, Validators.required],
      min: [isMoment(data?.min) || typeof data?.min === 'string' ? data.min : toNumber(data?.min)],
      max: [isMoment(data?.max) || typeof data?.max === 'string' ? data.max : toNumber(data?.max)],
      allowedValues: [data?.allowedValues || null],
      pmfmus: [data?.pmfmus || null],
      preconditions: [data?.preconditions || null],
      groups: [data?.groups || null],
      updateDate: [data?.updateDate || null],
    };
  }

  getFormGroupOptions(data?: ControlRule, opts?: ReferentialValidatorOptions): AbstractControlOptions {
    return {
      validators: [this.checkPmfmus(), this.checkMinMax(), this.checkFunctionAcceptAttribute()],
    };
  }

  updateFormGroup(validator: UntypedFormGroup, functionType: any) {
    if (isPreconditionRuleFunction(functionType)) {
      validator.patchValue({ controlledEntityId: 'MEASUREMENT', controlledAttributeId: 'MEASUREMENT_QUALITATIVE_VALUE' }, { emitEvent: true });
    } else if (isGroupedRuleFunction(functionType)) {
      validator.patchValue({ controlledEntityId: 'MEASUREMENT', controlledAttributeId: 'MEASUREMENT_PMFMU' }, { emitEvent: true });
    }

    if (isMinMaxRuleFunction(functionType)) {
      // add required validators
      validator.controls.min.setValidators(Validators.required);
      validator.controls.max.setValidators(Validators.required);
    } else {
      // remove required validators
      validator.patchValue({ min: undefined, max: undefined }, { emitEvent: false });
      validator.controls.min.setErrors(null);
      validator.controls.min.clearValidators();
      validator.controls.max.setErrors(null);
      validator.controls.max.clearValidators();
    }

    if (functionType === 'IN') {
      // add required validator
      validator.controls.allowedValues.setValidators([Validators.required, Validators.maxLength(1000), this.checkAllowedValues()]);
    } else {
      // remove required validator
      validator.patchValue({ allowedValues: undefined }, { emitEvent: false });
      validator.controls.allowedValues.setErrors(null);
      validator.controls.allowedValues.clearValidators();
    }

    if (isPreconditionRuleFunction(functionType)) {
      validator.controls.preconditions.setValidators(this.checkPreconditions());
    } else {
      validator.patchValue({ preconditions: undefined }, { emitEvent: false });
      validator.controls.preconditions.setErrors(null);
      validator.controls.preconditions.clearValidators();
    }

    if (isGroupedRuleFunction(functionType)) {
      // await this.buildNotEmptyConditionalGroupsFromGroupedRule(validator);
    } else {
      validator.patchValue({ groups: undefined }, { emitEvent: false });
    }

    validator.updateValueAndValidity();
  }

  async buildNotEmptyConditionalGroupsFromGroupedRule(validator: UntypedFormGroup) {
    const rule = validator.value;
    // if incorrect pmfmus or function, just reset
    if (isEmptyArray(rule.pmfmus)) {
      validator.patchValue({ groups: [] });
      return;
    }
    const suffix = newRuleIdSuffix();
    const groups = [];
    let taxonGroupRuleGroup = validator.value.groups.find(
      (group) =>
        !!group.rule &&
        group.rule.functionId === 'NOT_EMPTY' &&
        group.rule.controlledEntityId === 'MEASUREMENT' &&
        group.rule.controlledAttributeId === 'MEASUREMENT_TAXON_GROUP'
    );
    if (!taxonGroupRuleGroup) {
      taxonGroupRuleGroup = {
        or: true,
        rule: {
          id: await this.ruleListService.nextRuleId(rule.id, suffix),
          active: false,
          blocking: false,
          functionId: 'NOT_EMPTY',
          controlledEntityId: 'MEASUREMENT',
          controlledAttributeId: 'MEASUREMENT_TAXON_GROUP',
        },
      };
    }
    groups.push(taxonGroupRuleGroup);
    taxonGroupRuleGroup.label = rule.id;
    taxonGroupRuleGroup.active = rule.active;
    taxonGroupRuleGroup.rule.description = rule.description;
    taxonGroupRuleGroup.rule.errorMessage = rule.errorMessage;

    let taxonRuleGroup = rule.groups.find(
      (group) =>
        !!group.rule &&
        group.rule.functionId === 'NOT_EMPTY' &&
        group.rule.controlledEntityId === 'MEASUREMENT' &&
        group.rule.controlledAttributeId === 'MEASUREMENT_TAXON'
    );
    if (!taxonRuleGroup) {
      taxonRuleGroup = {
        or: true,
        rule: {
          id: await this.ruleListService.nextRuleId(rule.id, suffix),
          active: false,
          blocking: false,
          functionId: 'NOT_EMPTY',
          controlledEntityId: 'MEASUREMENT',
          controlledAttributeId: 'MEASUREMENT_TAXON',
        },
      };
    }
    groups.push(taxonRuleGroup);
    taxonRuleGroup.label = rule.id;
    taxonRuleGroup.active = rule.active;
    taxonRuleGroup.rule.description = rule.description;
    taxonRuleGroup.rule.errorMessage = rule.errorMessage;

    let pmfmuRuleGroup = rule.groups.find(
      (group) =>
        !!group.rule &&
        group.rule.functionId === 'NOT_EMPTY' &&
        group.rule.controlledEntityId === 'MEASUREMENT' &&
        group.rule.controlledAttributeId === 'MEASUREMENT_PMFMU'
    );
    if (!pmfmuRuleGroup) {
      pmfmuRuleGroup = {
        or: true,
        rule: {
          id: await this.ruleListService.nextRuleId(rule.id, suffix),
          active: false,
          blocking: false,
          functionId: 'NOT_EMPTY',
          controlledEntityId: 'MEASUREMENT',
          controlledAttributeId: 'MEASUREMENT_PMFMU',
        },
      };
    }
    groups.push(pmfmuRuleGroup);
    pmfmuRuleGroup.label = rule.id;
    pmfmuRuleGroup.active = rule.active;
    pmfmuRuleGroup.rule.description = rule.description;
    pmfmuRuleGroup.rule.errorMessage = rule.errorMessage;
    pmfmuRuleGroup.rule.pmfmus = rule.pmfmus?.slice() || [];

    validator.patchValue({ groups });
  }

  // Specific Validators

  checkIdAlreadyExists(
    ruleListService: RuleListService,
    dataSource: IEntitiesTableDataSource<any>,
    opts: ReferentialValidatorOptions,
    currentData?: IEntity<any, any>,
    errorMessage?: string
  ): AsyncValidatorFn {
    if (!opts || !opts.newData) return null;
    if (!currentData && opts.newData !== true) {
      console.warn(`[control-rule-validator] can't check id already exists of this existing row : no current data`);
      return null;
    }

    return (control): Observable<ValidationErrors | null> =>
      timer(500).pipe(
        map(() => control.value),
        mergeMap(async (value) => {
          if (
            // no value
            !value ||
            // or id is same as the edited data
            (!opts.newData && currentData?.id === value)
          ) {
            // don't check
            return null;
          }

          const error = errorMessage ? { checkIdAlreadyExistsMsg: errorMessage } : { alreadyExists: true };

          // First check in current data source
          if (dataSource) {
            if (dataSource.getRows().filter((row) => row.currentData?.id?.toString().toUpperCase() === value?.toString().toUpperCase()).length > 1) {
              return error;
            }
          }

          // Next, check via service
          return ruleListService.ruleExists(value).then((exists) => (exists ? error : null));
        })
      );
  }

  checkPmfmus(): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const controlRule = ControlRule.fromObject(group.value);
      if (isPmfmuMandatory(controlRule)) {
        if (isPreconditionRuleFunction(controlRule.functionId)) {
          if (controlRule.functionId === 'PRECONDITION_QUALITATIVE') {
            if (
              arraySize(controlRule.pmfmus?.filter((pmfmu) => !!pmfmu.pmfmuId)) !== 2 ||
              !controlRule.pmfmus[0].parameter.qualitative ||
              !controlRule.pmfmus[1].parameter.qualitative
            ) {
              return { invalidPmfmus: 'REFERENTIAL.RULE.ERROR.INVALID_QUALITATIVE_PMFMU' };
            }
          } else if (controlRule.functionId === 'PRECONDITION_NUMERICAL') {
            if (
              arraySize(controlRule.pmfmus?.filter((pmfmu) => !!pmfmu.pmfmuId)) !== 2 ||
              !controlRule.pmfmus[0].parameter.qualitative ||
              controlRule.pmfmus[1].parameter.qualitative
            ) {
              return { invalidPmfmus: 'REFERENTIAL.RULE.ERROR.INVALID_NUMERICAL_PMFMU' };
            }
          }
        } else {
          if (isEmptyArray(controlRule.pmfmus)) {
            return { invalidPmfmus: 'REFERENTIAL.RULE.ERROR.PMFMU_MANDATORY' };
          }
        }
      }
      return null;
    };
  }

  checkMinMax(): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const controlRule = ControlRule.fromObject(group.value);
      if (isMinMaxRuleFunction(controlRule.functionId)) {
        if (!!controlRule.min && !!controlRule.max) {
          if (isMoment(controlRule.min) && isMoment(controlRule.max)) {
            if (controlRule.min.isAfter(controlRule.max)) {
              return { invalidMinMax: 'REFERENTIAL.RULE.ERROR.INVALID_MIN_MAX_DATE' };
            }
          } else {
            if (controlRule.min > controlRule.max) {
              return { invalidMinMax: 'REFERENTIAL.RULE.ERROR.INVALID_MIN_MAX' };
            }
          }
        }
      }
      return null;
    };
  }

  checkFunctionAcceptAttribute(): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const controlRule = ControlRule.fromObject(group.value);
      if (
        controlRule.functionId &&
        controlRule.controlledAttributeId &&
        !isFunctionValidForAttribute(controlRule.functionId, controlRule.controlledAttributeId)
      ) {
        return { invalidAttribute: 'REFERENTIAL.RULE.ERROR.INVALID_ATTRIBUTE' };
      }
      return null;
    };
  }

  checkAllowedValues(): ValidatorFn {
    return (control): ValidationErrors | null => {
      const allowedValues: string = control.value;
      if (!!allowedValues && allowedValues.includes(' ') && !allowedValues.includes(',')) {
        return { invalidAllowedValues: 'REFERENTIAL.RULE.ERROR.INVALID_ALLOWED_VALUES' };
      }
      return null;
    };
  }

  checkPreconditions(): ValidatorFn {
    return (control): ValidationErrors | null => {
      if (isEmptyArray(control.value)) {
        return { emptyPreconditions: 'REFERENTIAL.RULE.ERROR.EMPTY_PRECONDITIONS' };
      }
      return null;
    };
  }
}
