import { Pipe, PipeTransform } from '@angular/core';
import { isComplexRuleFunction, isFunctionValidForAttribute } from '@app/referential/rule/control-rule.model';

@Pipe({
  name: 'simpleFunction',
})
export class SimpleFunctionPipe implements PipeTransform {
  transform(functionId: string): boolean {
    return !isComplexRuleFunction(functionId);
  }
}

@Pipe({
  name: 'functionValid',
})
export class FunctionValidForAttributePipe implements PipeTransform {
  transform(functionId: string, args: { attribute: string }): boolean {
    return isFunctionValidForAttribute(functionId, args.attribute);
  }
}
