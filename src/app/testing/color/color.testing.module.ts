import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ColorTestingPage } from '@app/testing/color/color.testing.page';
import { QuadrigeSharedModule } from '@app/shared/quadrige.shared.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ColorTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, QuadrigeSharedModule, TranslateModule.forChild(), RouterModule.forChild(routes)],
  declarations: [ColorTestingPage],
  exports: [ColorTestingPage],
})
export class ColorTestingModule {}
