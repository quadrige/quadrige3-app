import { Component } from '@angular/core';
import {
  ConfigService,
  Configuration,
  FormArrayHelper,
  getColorContrast,
  isNil,
  isNotNilOrBlank,
  Property,
  PropertyMap,
} from '@sumaris-net/ngx-components';
import { AbstractControl, UntypedFormArray, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-color-testing-page',
  templateUrl: './color.testing.page.html',
  styleUrls: ['./color.testing.page.scss'],
})
export class ColorTestingPage {
  colorArray: UntypedFormArray;
  arrayHelper: FormArrayHelper<Property>;
  form: UntypedFormGroup;

  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected configService: ConfigService
  ) {
    this.colorArray = this.formBuilder.array([]);
    this.arrayHelper = new FormArrayHelper<Property>(
      this.colorArray,
      (value) => this.getPropertyFormGroup(value),
      (v1, v2) => (!v1 && !v2) || v1.key === v2.key,
      (value) => isNil(value) || (isNil(value.key) && isNil(value.value))
    );

    this.form = this.formBuilder.group({ colors: this.colorArray });

    configService.config.subscribe((value) => this.loadConfig(value));
  }

  get colorForms(): UntypedFormGroup[] {
    return this.colorArray.controls as UntypedFormGroup[];
  }

  loadConfig(config: Configuration) {
    const colors: Property[] = [
      { key: 'primary', value: config.properties['sumaris.color.primary'] },
      { key: 'secondary', value: config.properties['sumaris.color.secondary'] },
      { key: 'tertiary', value: config.properties['sumaris.color.tertiary'] },
      { key: 'success', value: config.properties['sumaris.color.success'] },
      { key: 'warning', value: config.properties['sumaris.color.warning'] },
      { key: 'accent', value: config.properties['sumaris.color.accent'] },
      { key: 'danger', value: config.properties['sumaris.color.danger'] },
    ];

    this.arrayHelper.resize(colors.length);
    this.arrayHelper.formArray.patchValue(colors, { emitEvent: false });
  }

  doSubmit($event: any) {
    const properties: PropertyMap = {};
    properties['sumaris.color.primary'] = this.form.controls.colors.value[0].value;
    properties['sumaris.color.secondary'] = this.form.controls.colors.value[1].value;
    properties['sumaris.color.tertiary'] = this.form.controls.colors.value[2].value;
    properties['sumaris.color.success'] = this.form.controls.colors.value[3].value;
    properties['sumaris.color.warning'] = this.form.controls.colors.value[4].value;
    properties['sumaris.color.accent'] = this.form.controls.colors.value[5].value;
    properties['sumaris.color.danger'] = this.form.controls.colors.value[6].value;
    const configuration = new Configuration();
    configuration.properties = properties;
    this.configService.save(configuration);
  }

  doClear($event: any) {
    this.form.controls.colors.value[0].value = null;
    this.form.controls.colors.value[1].value = null;
    this.form.controls.colors.value[2].value = null;
    this.form.controls.colors.value[3].value = null;
    this.form.controls.colors.value[4].value = null;
    this.form.controls.colors.value[5].value = null;
    this.form.controls.colors.value[6].value = null;
    this.doSubmit($event);
  }

  getColorContrast(color: string): string | undefined {
    return isNotNilOrBlank(color) ? getColorContrast(color, true) : undefined;
  }

  writeValue(obj: any, formControl: AbstractControl) {
    if (obj !== formControl.value) {
      //console.debug("Set config value ", this.formControl.value, obj);
      formControl.patchValue(obj, { emitEvent: false });
    }

    this.doSubmit(null);
    // this.cd.markForCheck();
  }

  protected getPropertyFormGroup(data?: { key: string; value?: string }): UntypedFormGroup {
    return this.formBuilder.group({
      key: [(data && data.key) || null],
      value: [(data && data.value) || null],
    });
  }
}
