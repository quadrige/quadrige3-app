import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedTestingModule } from '@sumaris-net/ngx-components';

/**
 * Just a service wrapper to expose sumaris shared testing module
 */
@NgModule({
  imports: [CommonModule, SharedTestingModule],
})
export class QuadrigeSharedTestingModule {}
