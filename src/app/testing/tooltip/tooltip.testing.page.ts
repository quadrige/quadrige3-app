import { Component } from '@angular/core';

@Component({
  selector: 'app-tooltip-testing-page',
  templateUrl: './tooltip.testing.page.html',
  styleUrls: ['./tooltip.testing.page.scss'],
})
export class TooltipTestingPage {
  longText = 'tooltip with long text\nthis line is below\nthis one also\n\nempty line before this one';

  constructor() {}
}
