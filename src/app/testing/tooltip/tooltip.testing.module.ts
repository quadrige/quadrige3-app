import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { QuadrigeSharedModule } from '@app/shared/quadrige.shared.module';
import { TooltipTestingPage } from '@app/testing/tooltip/tooltip.testing.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: TooltipTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, QuadrigeSharedModule, TranslateModule.forChild(), RouterModule.forChild(routes)],
  declarations: [TooltipTestingPage],
  exports: [TooltipTestingPage],
})
export class TooltipTestingModule {}
