import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, provideExperimentalZonelessChangeDetection, SecurityContext } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicStorageModule } from '@ionic/storage-angular';
import { IonicModule, IonRouterOutlet } from '@ionic/angular';
import { CacheModule } from 'ionic-cache';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { MarkdownModule, MARKED_OPTIONS, MarkedOptions } from 'ngx-markdown';
import { QuadrigeCoreModule } from './core/quadrige.core.module';
import {
  APP_ABOUT_DEVELOPERS,
  APP_ABOUT_PARTNERS,
  APP_CONFIG_OPTIONS,
  APP_GRAPHQL_TYPE_POLICIES,
  APP_LOCAL_SETTINGS,
  APP_LOCAL_SETTINGS_OPTIONS,
  APP_LOCALES,
  APP_MENU_ITEMS,
  APP_NAMED_FILTER_SERVICE,
  APP_STORAGE,
  APP_TESTING_PAGES,
  APP_USER_SETTINGS_OPTIONS,
  APP_USER_TOKEN_SCOPES,
  CORE_CONFIG_OPTIONS,
  DATE_ISO_PATTERN,
  Department,
  EnvironmentHttpLoader,
  EnvironmentLoader,
  isAndroid,
  isCapacitor,
  isIOS,
  isMobile,
  LocalSettings,
  LocalSettingsOptions,
  SharedModule,
  StorageService,
  TestingPage,
  TokenScope,
  UserSettingsOptions,
} from '@sumaris-net/ngx-components';
import { TypePolicies } from '@apollo/client';
import { environment } from '@environments/environment';
import { MatStepperIntl } from '@angular/material/stepper';
import { StepperI18n } from '@app/shared/stepper-i18n';
import { JDENTICON_CONFIG } from 'ngx-jdenticon';
import { coreConfigOptions } from '@app/core/config/core.config';
import { attributes } from '@app/referential/model/referential.constants';
import { selectionServiceToken } from '@app/selection/selection.model';
import { SelectionService } from '@app/selection/selection.service';
import { ApolloModule } from 'apollo-angular';
import { accountConfigOptions } from '@app/core/config/account.config';
import { QuadrigeSocialModule } from '@app/social/social.module';
import { NamedFilterService } from '@app/shared/component/named-filter/named-filter.service';
import { loadDevMessages, loadErrorMessages } from '@apollo/client/dev';
import { AUTH_GUARD, AuthGuardService } from '@app/core/service/auth-guard.service';
import { COMPONENT_GUARD, ComponentGuardService } from '@app/core/service/component-guard.service';
import { TranslateHttpLoader } from '@app/shared/translate-http-loader';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { MAT_TABS_CONFIG, MatTabsConfig } from '@angular/material/tabs';

@NgModule({
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ApolloModule,
    IonicModule.forRoot({
      innerHTMLTemplatesEnabled: true,
      platform: {
        mobile: isMobile,
        capacitor: isCapacitor,
        android: isAndroid,
        ios: isIOS,
      },
    }),
    CacheModule.forRoot({
      keyPrefix: '', // For compatibility
      ...environment.cache,
    }),
    IonicStorageModule.forRoot({
      name: 'quadmire', // =old name (should be quadrige) to keep previous settings
      ...environment.storage,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (httpClient: HttpClient) => {
          if (environment.production) {
            // This is need to force a reload, after an app update
            return new TranslateHttpLoader(httpClient, './assets/i18n/', `-${environment.version}.json`);
          }
          return new TranslateHttpLoader(httpClient, './assets/i18n/', `.json`);
        },
        deps: [HttpClient],
      },
    }),
    MarkdownModule.forRoot({
      loader: HttpClient, // Allow to load using [src]
      sanitize: SecurityContext.NONE,
      markedOptions: {
        provide: MARKED_OPTIONS,
        useValue: <MarkedOptions>{
          gfm: true,
          breaks: false,
          pedantic: false,
          smartLists: true,
          smartypants: false,
        },
      },
    }),
    // functional modules
    SharedModule.forRoot({
      loader: {
        provide: EnvironmentLoader,
        deps: [HttpClient],
        useFactory: (httpClient: HttpClient) => new EnvironmentHttpLoader(httpClient, environment),
      },
    }),
    QuadrigeCoreModule.forRoot(),
    QuadrigeSocialModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: AUTH_GUARD, useClass: AuthGuardService },
    { provide: COMPONENT_GUARD, useClass: ComponentGuardService },
    {
      provide: APP_BASE_HREF,
      useFactory: () => document.getElementsByTagName('base')[0]?.href || '/',
    },
    { provide: APP_STORAGE, useExisting: StorageService },
    {
      provide: APP_LOCALES,
      useValue: [
        {
          key: 'fr',
          value: 'Français',
          country: 'fr',
        },
        {
          key: 'en',
          value: 'English',
          country: 'gb',
        },
        {
          key: 'es',
          value: 'Spanish',
          country: 'es',
        },
      ],
    },
    {
      provide: MatStepperIntl,
      useClass: StepperI18n,
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: <MatFormFieldDefaultOptions>{
        // TODO : Test if it suitable
        // appearance: 'outline',
        // subscriptSizing: 'dynamic'
      },
    },
    {
      provide: MAT_TABS_CONFIG,
      useValue: <MatTabsConfig>{
        stretchTabs: false,
      },
    },
    { provide: MAT_DATE_LOCALE, useValue: environment.defaultLocale || 'fr' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: DATE_ISO_PATTERN,
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
    {
      provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
      useValue: { strict: false },
    },
    { provide: MomentDateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
    { provide: DateAdapter, useExisting: MomentDateAdapter },
    // Settings default values
    {
      provide: APP_LOCAL_SETTINGS,
      useValue: <Partial<LocalSettings>>{
        accountInheritance: true,
        locale: environment.defaultLocale || 'fr',
        latLongFormat: environment.defaultLatLongFormat || 'DDMM',
        pages: [],
        pageHistory: [],
        pageHistoryMaxSize: 3,
        mobile: false,
      },
    },
    // Config options (Core + Q3Core)
    {
      provide: APP_CONFIG_OPTIONS,
      useValue: { ...CORE_CONFIG_OPTIONS, ...coreConfigOptions },
    },
    // Setting options definition
    {
      provide: APP_LOCAL_SETTINGS_OPTIONS,
      useValue: <LocalSettingsOptions>{
        serializeAsString: false,
        options: {},
      },
    },
    // Account options definition
    {
      provide: APP_USER_SETTINGS_OPTIONS,
      useValue: <UserSettingsOptions>{
        options: { ...accountConfigOptions },
        remoteLocalSettingsKeys: ['pages', 'pageHistory', 'properties'],
      },
    },
    // Token Scopes
    {
      provide: APP_USER_TOKEN_SCOPES,
      useValue: <TokenScope[]>[
        {
          id: 'read_api',
          // eslint-disable-next-line no-bitwise
          flag: 1 << 0,
          name: 'ACCOUNT.TOKENS.SCOPES.READ',
        },
        {
          id: 'write_api',
          // eslint-disable-next-line no-bitwise
          flag: 1 << 1,
          name: 'ACCOUNT.TOKENS.SCOPES.WRITE',
        },
      ],
    },
    // Menu items
    {
      provide: APP_MENU_ITEMS,
      useValue: [
        { title: 'MENU.HOME', path: '/', icon: 'home' },
        // Referential
        { title: 'MENU.REFERENTIAL.DIVIDER', icon: 'list', profile: 'USER' },
        { title: 'MENU.REFERENTIAL.MANAGEMENT', path: '/management', profile: 'USER', cssClass: 'left-menu-item' },
        { title: 'MENU.REFERENTIAL.REFERENTIAL', path: '/referential', profile: 'USER', cssClass: 'left-menu-item' },
        // Extractions
        { title: 'MENU.EXTRACTION.DIVIDER', icon: 'cloud-download', profile: 'USER', ifProperty: 'quadrige3.extraction.enabled' },
        {
          title: 'MENU.EXTRACTION.RESULT',
          path: '/extraction/result/Result',
          profile: 'USER',
          ifProperty: 'quadrige3.extraction.enabled',
          cssClass: 'left-menu-item',
        },
        {
          title: 'MENU.EXTRACTION.DATA',
          path: '/extraction/data',
          profile: 'USER',
          ifProperty: 'quadrige3.extraction.enabled',
          cssClass: 'left-menu-item',
        },
        {
          title: 'MENU.EXTRACTION.OTHER',
          path: '/extraction/other',
          profile: 'USER',
          ifProperty: 'quadrige3.extraction.enabled',
          cssClass: 'left-menu-item',
        },
        // Other
        { title: '' /*empty divider*/, cssClass: 'flex-spacer' },
        { title: 'MENU.SOCIAL.JOB', path: '/social/Job', icon: 'settings', profile: 'USER' },
        { title: 'MENU.SERVER', path: '/admin/config', icon: 'server', profile: 'ADMIN' },
        { title: 'MENU.TESTING', path: '/testing', icon: 'code', color: 'danger', /*ifProperty: 'sumaris.testing.enable',*/ profile: 'ADMIN' },
        { title: 'MENU.ABOUT', action: 'about', matIcon: 'help_outline', color: 'medium', cssClass: 'visible-mobile' },
        // Logout
        { title: 'MENU.LOGOUT', action: 'logout', icon: 'log-out', profile: 'GUEST', color: 'medium' },
      ],
    },
    // Home buttons
    // {
    //   provide: APP_HOME_BUTTONS,
    //   useValue: [
    //     { title: 'MENU.REFERENTIAL.TITLE', path: '/referential', icon: 'list', profile: 'USER' },
    //     { title: 'MENU.EXTRACTION.TITLE', path: '/extraction', icon: 'cloud-download', profile: 'USER', ifProperty: 'quadrige3.extraction.enabled' },
    //   ],
    // },
    // Entities Apollo cache options
    {
      provide: APP_GRAPHQL_TYPE_POLICIES,
      useValue: <TypePolicies>{
        /* eslint-disable @typescript-eslint/naming-convention */
        // Technical
        SoftwareVO: { keyFields: attributes.id },
        ConfigurationVO: { keyFields: attributes.id },
        // User
        AccountVO: { keyFields: attributes.id },
        UserVO: { keyFields: attributes.id },
        LightUserVO: { keyFields: attributes.id },
        DepartmentVO: { keyFields: attributes.id },
        LightDepartmentVO: { keyFields: attributes.id },
        PrivilegeVO: { keyFields: attributes.id },
        UserSettingsVO: { keyFields: attributes.id },
        // Referential
        ReferentialVO: {
          keyFields: ['entityName', 'id'],
          merge: (existing, incoming) => (incoming?.__ref?.includes('"entityName":null') ? existing : incoming),
        },
        EntityTypeVO: { keyFields: attributes.name },
        TranscribingItemVO: { keyFields: attributes.id },
        SamplingEquipmentVO: { keyFields: attributes.id },
        UnitVO: { keyFields: attributes.id },
        DredgingAreaTypeVO: { keyFields: attributes.id },
        DredgingTargetAreaVO: { keyFields: attributes.id },
        // Monitoring location
        MonitoringLocationVO: { keyFields: attributes.id },
        PositioningSystemVO: { keyFields: attributes.id },
        // PMFMU
        ParameterVO: { keyFields: attributes.id },
        ParameterGroupVO: { keyFields: attributes.id },
        QualitativeValueVO: { keyFields: attributes.id },
        MatrixVO: { keyFields: attributes.id },
        FractionVO: { keyFields: attributes.id },
        MethodVO: { keyFields: attributes.id },
        PmfmuVO: { keyFields: attributes.id },
        // Taxon
        TaxonNameVO: { keyFields: attributes.id },
        // Program&Strategy
        ProgramVO: { keyFields: attributes.id },
        ProgramLocationVO: { keyFields: ['programId', 'monitoringLocationId'] },
        StrategyVO: { keyFields: ['id', 'programId'] },
        AppliedStrategyVO: { keyFields: ['id', 'monitoringLocation', ['id']] },
        AppliedPeriodVO: { keyFields: attributes.id },
        PmfmuStrategyVO: { keyFields: ['id', 'pmfmu', ['id']] },
        PmfmuAppliedStrategyVO: { keyFields: ['id', 'monitoringLocationId', 'pmfmuId'] },
        // Rules
        RuleListVO: { keyFields: attributes.id },
        ControlRuleVO: { keyFields: attributes.id },
        // Social
        UserEventVO: { keyFields: attributes.id },
        // JobVO: { keyFields: attributes.id },
        Query: {
          jobs: {
            merge: true,
          },
        },
        /* eslint-enable @typescript-eslint/naming-convention */
      },
    },
    // Custom identicon style
    // https://jdenticon.com/icon-designer.html?config=4451860010ff320028501e5a
    {
      provide: JDENTICON_CONFIG,
      useValue: {
        lightness: {
          color: [0.26, 0.8],
          grayscale: [0.3, 0.9],
        },
        saturation: {
          color: 0.5,
          grayscale: 0.46,
        },
        backColor: '#0000',
      },
    },
    // About developers
    {
      provide: APP_ABOUT_DEVELOPERS,
      useValue: <Partial<Department>[]>[
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png', label: 'Environmental Information Systems' },
      ],
    },
    // About partners
    {
      provide: APP_ABOUT_PARTNERS,
      useValue: <Partial<Department>[]>[
        { siteUrl: 'https://www.ifremer.fr', logo: 'assets/img/logo/logo-ifremer.png' },
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png' },
      ],
    },
    // Test pages link
    {
      provide: APP_TESTING_PAGES,
      useValue: <TestingPage[]>[
        { label: 'Table', page: '/testing/table' },
        { label: 'Color', page: '/testing/color' },
        { label: 'Tooltip', page: '/testing/tooltip' },
        { label: 'Filter', page: '/testing/filter' },
        { label: 'Boolean', page: '/testing/boolean' },
      ],
    },
    // Tweak the IonRouterOutlet if this component shown in a modal
    {
      provide: IonRouterOutlet,
      useValue: {
        canGoBack: () => false,
        nativeEl: '',
      },
    },
    // Provide the entity selection service
    {
      provide: selectionServiceToken,
      useClass: SelectionService,
    },
    {
      provide: APP_NAMED_FILTER_SERVICE,
      useClass: NamedFilterService,
    },
    provideHttpClient(withInterceptorsFromDi()),
    provideExperimentalZonelessChangeDetection(),
  ],
})
export class AppModule {
  constructor() {
    console.info('[app] Creating module');

    if (!environment?.production) {
      loadDevMessages();
      loadErrorMessages();
    }
  }
}
