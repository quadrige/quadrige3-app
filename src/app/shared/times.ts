export class Times {
  static secondsToString(seconds: number | string): string {
    if (!seconds) {
      return undefined;
    }
    if (typeof seconds === 'string') {
      seconds = parseInt(seconds, 10);
    }
    if (isNaN(seconds) || seconds < 0 || seconds > 3600 * 24) {
      return undefined;
    }

    return new Date(seconds * 1000).toISOString().substring(11, 19);
  }

  static stringToSeconds(timeStr: string): number {
    if (!timeStr || typeof timeStr !== 'string' || !timeStr.match(/\d\d:\d\d:\d\d/)) {
      return undefined;
    }
    return timeStr
      .split(':')
      .map((value) => parseInt(value, 10))
      .reduce((acc, time) => 60 * acc + time);
  }
}
