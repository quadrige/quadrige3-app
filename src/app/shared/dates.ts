import { fromDateISOString, fromUnixMsTimestamp, isNotEmptyArray, isNotNilString, toDuration, toFloat } from '@sumaris-net/ngx-components';
import { isMoment, max, Moment } from 'moment';

export class Dates {
  static toLocalDateString(value: any, format?: string): string | undefined {
    if (!value) return undefined;

    // Make sure to have a Moment object
    // console.debug('DATES value', value);
    const moment = fromDateISOString(value);

    if (format) {
      return moment.format(format);
    } else {
      // Hack the time gap occurred the 1911-03-11 at night in France
      if (moment.year() <= 1911 && !moment.toISOString(true).includes('T00:00:00')) {
        moment.add(10, 'minutes');
      }

      // Return only date part without utc conversion
      return moment.toISOString(true /* important ! */).substring(0, 10);
    }
  }

  static fromUnix(time: number | string): Moment {
    if (!time) return undefined;
    if (isNotNilString(time)) {
      time = toFloat(time);
    }
    return fromUnixMsTimestamp(time);
  }

  /**
   * Return true if the date ranges overlaps each other
   *
   * @param range1
   * @param range2
   */
  static overlaps(range1: { start: Moment | string; end: Moment | string }, range2: { start: Moment | string; end: Moment | string }): boolean {
    return (
      fromDateISOString(range1.end)?.isSameOrAfter(fromDateISOString(range2.start)) &&
      fromDateISOString(range1.start)?.isSameOrBefore(fromDateISOString(range2.end))
    );
  }

  /**
   * Test if a moment is between a range of moment
   * Can handle undefined range.start and end, moment().isBetween cannot.
   *
   * @param moment
   * @param range
   */
  static isBetween(moment: Moment | string, range: { start: Moment | string; end: Moment | string }): boolean {
    if (!moment) return undefined;
    if (!range) return true;
    const instant = fromDateISOString(moment);
    return (
      (!range.start || instant.isSameOrAfter(fromDateISOString(range.start))) && (!range.end || instant.isSameOrBefore(fromDateISOString(range.end)))
    );
  }

  static max(moments: Moment[]): Moment {
    const array = moments?.filter((value) => !!value && isMoment(value));
    return isNotEmptyArray(array) ? max(array) : undefined;
  }

  static duration(start: Moment, end?: Moment): string {
    if (!start) return undefined;
    if (!!end) {
      return toDuration(end.diff(start))?.humanize();
    } else {
      return start.fromNow();
    }
  }
}
