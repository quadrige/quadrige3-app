/*
 * Simple MultiMap
 * Ludovic Pecquot
 */

import { isNotEmptyArray, KeyType } from '@sumaris-net/ngx-components';

export declare type ObjectMultiMap<V = any> = {
  [key in KeyType]: V[];
};

export const splitToMultiMap = <V>(array: V[], keyMapper: (value: V) => KeyType): ObjectMultiMap<V> => {
  const multiMap: ObjectMultiMap<V> = {};
  if (isNotEmptyArray(array)) {
    array.forEach((value) => addToMultiMap(multiMap, keyMapper(value), value));
  }
  return multiMap;
};

export const isNotEmptyMultiMap = (map: ObjectMultiMap): boolean => {
  return isNotEmptyArray(Object.keys(map));
};

export const containsKey = (map: ObjectMultiMap, key: KeyType): boolean => {
  return Object.keys(map).includes(key.toString());
};

export const addToMultiMap = <V>(map: ObjectMultiMap<V>, key: KeyType, value: V, ifNotPresent?: boolean): void => {
  const values: V[] = map[key] || [];
  if (!ifNotPresent || !values.includes(value)) {
    values.push(value);
  }
  map[key] = values;
};

export const removeFromMultiMap = <V>(map: ObjectMultiMap<V>, key: KeyType, value: V): boolean => {
  const values: V[] = map[key] || [];
  const index = values.indexOf(value);
  if (index !== -1) {
    values.splice(index, 1);
    map[key] = values;
    return true;
  }
  return false;
};
