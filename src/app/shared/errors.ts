export const errorCodes = {
  load: 10,
  save: 11,
  delete: 12,
  subscribe: 13,
  loadEntities: 14,

  /* @deprecated */
  deleteForbidden: 522,
  attachedData: 523,
  attachedAdministration: 524,
  attachedControlRule: 525,
  attachedFilter: 526,
  attachedTranscribing: 527,
  attachedReferential: 528,
};

export class DetailedError {
  code?: number;
  message: string;
  detail?: DetailedError;
}
