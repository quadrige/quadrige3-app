import {
  Entity,
  EntityUtils as BaseEntityUtils,
  FilterFn,
  IEntity,
  isEmptyArray,
  isNil,
  isNilOrBlank,
  isNotNil,
  isNotNilOrNaN,
  MINIFY_ENTITY_FOR_POD,
  ReferentialAsObjectOptions,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { SortDirection } from '@angular/material/sort';
import { isMoment, Moment } from 'moment';
import { referentialOptions } from '@app/referential/model/referential.constants';

export const entityId = (entity: Entity<any, any>) => entity.id;
export const uniqueEntity = <E extends IEntity<any, any>>(value: E, index: number, array: E[]) => array.map(entityId).indexOf(value.id) === index;

export class EntityUtils extends BaseEntityUtils {
  static asMinifiedObject<O extends ReferentialAsObjectOptions>(obj: any, opts?: O): any {
    return isNotNil(obj) && typeof obj === 'object' && typeof obj.asObject === 'function'
      ? obj.asObject({ ...MINIFY_ENTITY_FOR_POD, ...opts })
      : undefined;
  }

  static ids(rows: AsyncTableElement<Entity<any, any>>[]): any[] {
    return (
      rows
        ?.map((row) => row.currentData)
        .filter(isNotNil)
        .map(entityId)
        .filter(isNotNilOrNaN) || []
    );
  }

  static sort<E extends IEntity<E> | any>(data: E[], sortBy?: string, sortDirection?: SortDirection): E[] {
    return data.sort(EntityUtils.entitySortComparator(sortBy, sortDirection));
  }

  static entitySortComparator<E extends IEntity<E> | any>(sortBy?: string, sortDirection?: SortDirection): (r1: E, r2: E) => number {
    sortBy = sortBy || 'id';

    // use natural sort comparator
    return EntityUtils.naturalComparator(EntityUtils.entitySortProperty(sortBy), sortDirection);
  }

  static naturalComparator<E extends IEntity<E> | any>(property: string, sortDirection?: SortDirection): (r1: E, r2: E) => number {
    const collator = new Intl.Collator(undefined, { numeric: true });
    const direction = !sortDirection || sortDirection === 'asc' ? 1 : -1;
    return (r1, r2) => {
      let v1 = EntityUtils.getPropertyByPath(r1, property);
      let v2 = EntityUtils.getPropertyByPath(r2, property);
      if (isNil(v1)) return -direction;
      if (isNil(v2)) return direction;
      // Sort moments
      if (isMoment(v1) && isMoment(v2)) {
        return (v1 as Moment).diff(v2) * direction;
      }
      // If values are entities, sort by id
      if (EntityUtils.isNotEmpty(v1, 'id') && EntityUtils.isNotEmpty(v2, 'id')) {
        v1 = v1.id;
        v2 = v2.id;
      }
      // Compare with collator
      return collator.compare(String(v1), String(v2)) * direction;
    };
  }

  static entitySortProperty(path: string): string {
    const dotIndex = path.lastIndexOf('.');
    const root = path.substring(0, dotIndex !== -1 ? dotIndex + 1 : 0);
    const property = path.substring(dotIndex !== -1 ? dotIndex + 1 : 0);
    const sortAttribute = referentialOptions[property]?.attributes[0];
    return `${root}${property}${!!sortAttribute ? '.' + sortAttribute : ''}`;
  }

  static deepEquals<R>(r1: R, r2: R, keys?: string[]): boolean {
    if (r1 === r2 || (!r1 && !r2)) return true;
    if ((r1 && !r2) || (!r1 && r2)) return false;
    return (keys || Object.keys(r1)).every((key) => {
      if (EntityUtils.isNotEmpty(r1[key], 'id')) {
        return EntityUtils.equals(r1[key], r2[key], 'id');
      }
      return (isNilOrBlank(r1[key]) && isNilOrBlank(r2[key])) || r1[key] === r2[key];
    });
  }

  // todo: implement inverse parameter if needed
  static searchTextFilter(searchAttributes: string[], searchText: string, inverse?: boolean): FilterFn<any> {
    if (isNilOrBlank(searchText)) return undefined;
    if (isEmptyArray(searchAttributes)) {
      searchAttributes = ['id', 'label', 'name']; // default attributes
    }

    const searchRegexp = EntityUtils.removeDiacritics(searchText)
      // Escape special characters
      .replace(/[-\/\\^$+.()|[\]{}]/g, '\\$&')
      // Interpret * as any text
      .replace(/[*]+/g, '.*')
      // Interpret ? as one character
      .replace(/[?]/g, '.');
    if (searchRegexp === '.*') return undefined; // filter not need

    const flags = 'i'; // don't use global regex search
    const anyMatchPattern = '^.*' + searchRegexp;

    // many search attributes
    const regexps = searchAttributes.map(() => new RegExp(anyMatchPattern, flags));
    return (entity) => {
      const index = searchAttributes.findIndex((path, i) =>
        regexps[i].test(EntityUtils.removeDiacritics(EntityUtils.getPropertyByPath(entity, path)))
      );
      // if inverse search, return item not corresponding to the search text, otherwise normal search
      return inverse !== true ? index !== -1 : index === -1;
    };
  }

  static removeDiacritics(text: any): string {
    return text
      ?.toString()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '');
  }

  static normalizedContains(text: string, searchText: string): boolean {
    const cleanedSearchText = EntityUtils.removeDiacritics(searchText).replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    const regExp = new RegExp(cleanedSearchText, 'gi');
    return regExp.test(EntityUtils.removeDiacritics(text));
  }

  static isEmptyObject(obj: any): boolean {
    return isNil(obj) || (typeof obj === 'object' && isEmptyArray(Object.keys(obj)));
  }
}
