export const autocompleteWidthMedium = '300px';
export const autocompleteWidthLarge = '500px';
export const autocompleteWidthExtraLarge = '750px';
export const autocompleteWidthFull = '90vw';

export const defaultAreaMinSize = 30;

export const entitiesWithoutName = ['Referential', 'ExtractCriteriaAddModel', 'ExtractFieldDefinition', 'ExtractField', 'TranscribingItemRow'];
