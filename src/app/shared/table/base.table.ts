import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  output,
  Output,
  QueryList,
  signal,
  viewChild,
  ViewChild,
  viewChildren,
  ViewChildren,
  ViewContainerRef,
  WritableSignal,
} from '@angular/core';
import {
  AccountService,
  AppAsyncTable,
  AppFormProvider,
  arraySize,
  changeCaseToUnderscore,
  ConfigService,
  createPromiseEventEmitter,
  emitPromiseEvent,
  EntitiesServiceWatchOptions,
  Entity,
  EntityClasses,
  ENVIRONMENT,
  ErrorCodes,
  Hotkeys,
  IAppForm,
  isEmptyArray,
  isNil,
  isNilOrBlank,
  isNotEmptyArray,
  isNotNilOrBlank,
  KeyType,
  LoadResult,
  ObjectMap,
  PromiseEvent,
  PropertyMap,
  removeEnd,
  RESERVED_START_COLUMNS,
  ResizableComponent,
  SETTINGS_DISPLAY_COLUMNS,
  SETTINGS_SORTED_COLUMN,
  StatusIds,
  StatusList,
  toBoolean,
  toInt,
  toNumber,
  waitForTrue,
} from '@sumaris-net/ngx-components';
import { NavigationStart, Params } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { BehaviorSubject, debounce, defer, from, iif, merge, Observable, of, Subscription, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeMap, tap } from 'rxjs/operators';
import { CanLeave } from '@app/core/service/component-guard.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatRow } from '@angular/material/table';
import { GenericService } from '@app/referential/generic/generic.service';
import { PredefinedColors } from '@ionic/core';
import { Alerts } from '@app/shared/alerts';
import { uniqueValue, Utils } from '@app/shared/utils';
import { UpdatableView } from '@app/shared/model/updatable-view';
import { PermissionService } from '@app/core/service/permission.service';
import { IOutputAreaSizes, SplitComponent } from 'angular-split';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { TableButtonsComponent } from '@app/shared/component/table/table-buttons.component';
import { referentialOptions } from '@app/referential/model/referential.constants';
import { entityId } from '@app/shared/entity.utils';
import { ModalService } from '@app/shared/component/modal/modal.service';
import { ISelectCriteria, ISetFilterOptions } from '@app/shared/model/options.model';
import { defaultAreaMinSize, entitiesWithoutName } from '@app/shared/constants';
import { BaseTableDatasource, BaseTableDataSourceConfig } from '@app/shared/table/base.datasource';
import {
  alwaysExportedColumns,
  BaseColumnItem,
  ColumnWidthSetting,
  defaultHiddenColumns,
  ExportOptions,
  filterSettingsId,
  IBaseTable,
  notExportableColumns,
  SplitPanelSetting,
  tableSettings,
} from './table.model';
import { ExportModal, IExportModalOptions } from '@app/shared/component/export/export.modal';
import { accountConfigOptions } from '@app/core/config/account.config';
import { WaitForOptions } from '@sumaris-net/ngx-components/src/app/shared/observables';
import { IBaseEntitiesService } from '@app/shared/service/base-entity.service';
import { BaseEntityFilter, BaseEntityFilterCriteria, BaseFilterUtils } from '@app/shared/model/filter.model';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import { BaseColumn } from '@app/shared/component/column/base-column.class';
import { SelectionModel } from '@angular/cdk/collections';
import { DetailedError } from '@app/shared/errors';
import { HttpParams } from '@angular/common/http';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { MatPaginator } from '@angular/material/paginator';
import { IExportModel } from '@app/shared/component/export/export.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';
import { MatSortable } from '@angular/material/sort';
import { IEntityMenuItem } from '@app/shared/component/menu/entity-menu.component';

@Directive()
export abstract class BaseTable<
    E extends Entity<E, ID>,
    ID extends KeyType,
    F extends BaseEntityFilter<F, C, E, ID>,
    C extends BaseEntityFilterCriteria<E, ID>,
    V extends BaseValidatorService<E> = any,
    RV = any,
    WO extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions,
  >
  extends AppAsyncTable<E, F, ID>
  implements OnInit, AfterViewInit, CanLeave, UpdatableView, OnDestroy, IBaseTable<E, F>
{
  rightSplit = viewChild<SplitComponent>('rightSplit');
  bottomSplit = viewChild<SplitComponent>('bottomSplit');
  bottomSplitRef = viewChild('bottomSplit', { read: ElementRef });
  filterFormComponent = viewChild<BaseCriteriaFormComponent<F, C>>('filterFormComponent');
  @ViewChild('tableButtons', { static: true }) tableButtons: TableButtonsComponent;
  tableContainerRef = viewChild('tableContainer', { read: ElementRef });
  paginatorRef = viewChild(MatPaginator, { read: ElementRef });
  rowsView = viewChildren(MatRow, { read: ViewContainerRef });
  @ViewChildren(ResizableComponent) resizableColumns: QueryList<ResizableComponent>;
  @ViewChildren(BaseColumn) baseColumns: QueryList<BaseColumn>;

  @Input() canGoBack = false;
  @Input() showTitle = true;
  @Input() showPaginator = true;
  @Input() showFooter = true;

  @Input() titleI18n: string;
  @Input() title: string;
  @Input() titleWithRoot = true;
  @Input() toolbarColor: PredefinedColors | string = 'primary';

  @Input() checkBoxSelection = false;
  @Input() multipleSelection = true;
  @Input() toggleOnlySelection = false;
  @Input() selectRowOnEdit = true;

  @Output() canAddRowEvent = createPromiseEventEmitter<boolean>();
  afterSelectionChange = output<AsyncTableElement<E>>();
  @Output() afterFilterSet = new EventEmitter<F>();

  statusList = StatusList;
  statusIds = StatusIds;

  listenToQueryParams = true;
  updateQueryParamsOnFilter = true;
  updateFilterOnQueryParams = true;
  columnWidthSubscriptions: { name: string; subscription: Subscription }[] = [];
  referentialOptions = referentialOptions;
  confirmBeforeSave = true; // when true, ask confirmation before saving on filter/sort/delete action
  forceNoInlineEdition = false;

  parentEnabledSubject = new BehaviorSubject<boolean>(true);
  entityName$ = new BehaviorSubject<string>(undefined);
  protected init$ = new BehaviorSubject<boolean>(false);
  private emitSaveSettings = new EventEmitter<any>();

  protected readonly configService = inject(ConfigService);
  protected readonly permissionService = inject(PermissionService);
  protected readonly referentialGenericService = inject(GenericService);
  protected readonly popoverController = inject(PopoverController);
  protected readonly modalService = inject(ModalService);
  protected readonly accountService = inject(AccountService);
  protected readonly formBuilder = inject(UntypedFormBuilder);
  protected readonly hotkeys = inject(Hotkeys);
  protected rowDirtySubscription: Subscription;
  protected rowValidSubscription: Subscription;
  protected logPrefix: string;
  protected previousUrl: string;
  protected currentUrl: string;
  protected usePageSettings = true;
  protected autoSaveFilter = true;
  protected progressBarMode$: WritableSignal<ProgressBarMode> = signal('determinate');
  protected paginatorHeight: string;
  protected defaultAreaMinSize = defaultAreaMinSize;
  protected showDisabledReferential: boolean;

  rightMenuItems: IEntityMenuItem<E, RV>[];
  rightMenuItem: IEntityMenuItem<E, RV>;

  private _canEdit = false;
  private _subTable = false;
  private _selectTable = false;
  private _addTable = false;
  private _rightPanelVisible = false;
  private _rightPanelAllowed = true;
  private _rightPanelWidth: number;
  private _bottomPanelVisible: boolean;
  private _bottomPanelAllowed = true;
  private _bottomPanelHeight: number;
  private _children: IAppForm[] = [];
  private _customErrors: PropertyMap = {};

  protected constructor(
    protected injector: Injector,
    protected columns: string[],
    public readonly dataType: new () => E,
    protected _entityService: IBaseEntitiesService<E, F, WO>,
    protected validatorService: V,
    options?: BaseTableDataSourceConfig<E, ID, WO>
  ) {
    super(
      injector,
      columns,
      _entityService
        ? new BaseTableDatasource<E, ID, F, WO>(dataType, _entityService, validatorService, {
            prependNewElements: false,
            saveOnlyDirtyRows: true,
            restoreOriginalDataOnCancel: true,
            suppressErrors: toBoolean(injector.get(ENVIRONMENT)?.production, false),
            onRowCreated: (row) => this.onDefaultRowCreated(row),
            onError: (error) => this.handleError(error),
            ...options,
          })
        : null,
      null
    );

    this.logPrefix = '[base-table]';

    // set entityName if provided
    const type = dataType && new dataType();
    if (isNotNilOrBlank(type?.['entityName'])) {
      this.entityName = type['entityName'];
    } else if (isNotNilOrBlank(type?.__typename)) {
      const entityName = removeEnd(type.__typename, 'VO');
      if (!entitiesWithoutName.includes(entityName)) {
        // Don't warn if entity has no name
        console.warn(
          `${this.logPrefix} Using __typename attribute to determine entityName, Please use a BaseReferential object declaring '${entityName}'`,
          type
        );
      }
      this.entityName = entityName;
    } else {
      console.error(`${this.logPrefix} No type provided`);
    }

    // Affect dataSource to validator
    if (this.validatorService) {
      this.validatorService.dataSource = this.dataSource;
      this.propagateRowError = true;
    }

    // FOR DEV ONLY
    this.debug = toBoolean(!injector.get(ENVIRONMENT)?.production, true);
  }

  get subTable(): boolean {
    return this._subTable;
  }

  @Input()
  set subTable(subTable: boolean) {
    if (!this._subTable && subTable) {
      this._subTable = true;
      this.canGoBack = false;
      this.showFooter = false;
      this.titleWithRoot = false;
      this.toolbarColor = 'secondary';
      this.listenToQueryParams = false;
      this.updateQueryParamsOnFilter = false;
      this.updateFilterOnQueryParams = false;
      this.keepEditedRowOnSave = false;
      this.undoableDeletion = true;
      this.updateSettingsId();
    }
  }

  get selectTable(): boolean {
    return this._selectTable;
  }

  @Input()
  set selectTable(selectTable: boolean) {
    if (!this._selectTable && selectTable) {
      this._selectTable = true;
      this._addTable = false;
      this.subTable = true;
      this.rightPanelAllowed = false;
      this.bottomPanelAllowed = false;
      this._canEdit = true;
      this.inlineEdition = false;
      this.forceNoInlineEdition = true;
      this.checkBoxSelection = this.settings.getPropertyAsBoolean(accountConfigOptions.tableCheckBoxSelection);
      this.toggleOnlySelection = true;
      this.permanentSelectionAllowed = false;
      this.updateSettingsId();
    }
  }

  get addTable(): boolean {
    return this._addTable;
  }

  @Input()
  set addTable(addTable: boolean) {
    if (!this._addTable && addTable) {
      this._addTable = true;
      this._selectTable = false;
      this.subTable = true;
      this.rightPanelAllowed = false;
      this.bottomPanelAllowed = false;
      this._canEdit = false;
      this.inlineEdition = false;
      this.forceNoInlineEdition = true;
      this.checkBoxSelection = this.settings.getPropertyAsBoolean(accountConfigOptions.tableCheckBoxSelection);
      this.toggleOnlySelection = true;
      this.permanentSelectionAllowed = true;
      if (this.tableButtons) {
        this.tableButtons.canAdd = false;
        this.tableButtons.canDelete = false;
      }
      this.toolbarColor = 'secondary900';
      this.updateSettingsId();
    }
  }

  get canEdit(): boolean {
    return this._canEdit && !this.readOnly;
  }

  @Input() set canEdit(value: boolean) {
    this._canEdit = value;
    this.inlineEdition = !this.forceNoInlineEdition && value;
    this.keepEditedRowOnSave = value;
    if (this.selectTable || this.addTable) {
      this.checkBoxSelection = value ? this.settings.getPropertyAsBoolean(accountConfigOptions.tableCheckBoxSelection) : false;
    }
  }

  // Right panel management

  get rightPanelVisible(): boolean {
    return this._rightPanelVisible;
  }

  get rightPanelWidth(): number {
    return this._rightPanelWidth;
  }

  // todo: use rightPanelAllowed instead
  get showRightPanelButton(): boolean {
    return this._rightPanelAllowed;
  }

  get rightPanelButtonAccent(): boolean {
    return !this.rightPanelVisible;
  }

  @Input() set rightPanelAllowed(allowed: boolean) {
    this._rightPanelAllowed = allowed;
    if (!allowed) {
      this._rightPanelVisible = false;
    }
  }

  get rightPanelAllowed(): boolean {
    return this._rightPanelAllowed;
  }

  // Bottom panel management

  get bottomPanelVisible(): boolean {
    return this._bottomPanelVisible;
  }

  get bottomPanelHeight(): number {
    return this._bottomPanelHeight;
  }

  get showBottomPanelButton(): boolean {
    return this._bottomPanelAllowed;
  }

  get bottomPanelButtonAccent(): boolean {
    return !this.bottomPanelVisible;
  }

  @Input() set bottomPanelAllowed(allowed: boolean) {
    this._bottomPanelAllowed = allowed;
    if (!allowed) {
      this._bottomPanelVisible = false;
    }
  }

  get parentEnabled$(): Observable<boolean> {
    return this.parentEnabledSubject.asObservable();
  }

  set parentEnabled(value: boolean) {
    this.parentEnabledSubject.next(value);
  }

  get filterCriteriaCount(): number {
    return this.filterFormComponent()?.filterCriteriaCount || 0;
  }

  get filterIsEmpty(): boolean {
    return this.filterCriteriaCount === 0;
  }

  get selectedEntityIds(): ID[] {
    return this.selectedEntities.map(entityId);
  }

  get entityName(): string {
    return this.entityName$.value;
  }

  set entityName(entityName: string) {
    this.entityName$.next(entityName);
    this.updateSettingsId();
    this.markAsReady();
  }

  get entityService() {
    return this._entityService || (this.dataSource.dataService as IBaseEntitiesService<E, F, WO>);
  }

  get disabled(): boolean {
    return super.disabled || this.loading || this.saving || !this.parentEnabledSubject.value;
  }

  get saving(): boolean {
    return this.savingSubject.value;
  }

  get singleEditingRow(): AsyncTableElement<E> {
    return this.dataSource.getSingleEditingRow();
  }

  get singleSelectedRow(): AsyncTableElement<E> {
    return super.singleSelectedRow;
  }

  ngOnInit() {
    this.autoLoad = false;
    this.allowRowDetail = false;
    this.confirmBeforeDelete = true;
    this.confirmBeforeCancel = true;

    // Do this before super.ngOnInit()
    if (!this.multipleSelection) {
      this.selection = new SelectionModel<AsyncTableElement<E>>(false, []);
    }

    // Call super here
    super.ngOnInit();

    // Force no save before delete due to cache update problem
    // TODO test more !!
    this.saveBeforeDelete = false;

    // Listen save event
    if (this.confirmBeforeSave) {
      this.registerSubscription(
        this.onBeforeSave.subscribe(async (event) => {
          // Ask confirmation before action
          const { confirmed, save } = await Alerts.askSaveBeforeAction(this.alertCtrl, this.translate, {
            valid: event.detail.valid,
          });
          // confirmed: true if user confirmed with or without saving, false if cancelled
          // saved: true is user wants to save before action
          if (confirmed && !save) {
            // Cancel dirty rows if user don't want to save them
            this.getEditingRows().forEach((row) => this.cancelOrDelete(undefined, row));
          }
          event.detail.success({ confirmed, save });
        })
      );
    }

    // Listen refresh event
    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this.unregisterRowDirtySubscription();
        this.unregisterRowValidSubscription();
        this.resetError();
        this.filterFormComponent()?.updateFilterCriteriaCount();
        this.scrollToTop();
        this.markForCheck();
      })
    );

    // Listen start editing row event
    this.registerSubscription(this.onStartEditingRow.subscribe((row) => this.onStartEditRow(row)));

    // Listen cancel or delete row event
    this.registerSubscription(
      this.onCancelOrDeleteRow.subscribe((row) => {
        // remove error
        this.resetError();
        // mark this row as pristine
        row.validator?.markAsPristine();
        // then try to update dirty state (because updateView() is not called here)
        this.updateTableDirtyState();
      })
    );

    // Set a filter on active referential only from configuration option
    this.showDisabledReferential =
      // Only admins can have this option as true
      this.accountService.isAdmin() && this.settings.getPropertyAsBoolean(accountConfigOptions.showDisabledReferential);

    // Update title
    this.updateTitle();

    // Don't auto save filter on extraction context
    this.autoSaveFilter =
      this.autoSaveFilter &&
      (this.isManagementContext() || this.isReferentialContext() || this.isSocialContext()) &&
      (!this.subTable || this.addTable);

    // Save account when signal emitted
    this.registerSubscription(this.emitSaveSettings.pipe(debounceTime(500)).subscribe(() => this.accountService.save(this.accountService.account)));

    this.init$.next(true);
  }

  isManagementContext(): boolean {
    return this.router.url.startsWith('/management');
  }

  isReferentialContext(): boolean {
    return this.router.url.startsWith('/referential');
  }

  isSocialContext(): boolean {
    return this.router.url.startsWith('/social');
  }

  waitInit(opts?: WaitForOptions): Promise<void> {
    return waitForTrue(this.init$, opts);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.ready().then(() => this.afterReady());

    // Update page size
    if (!this.showPaginator || !this.paginator) {
      this.pageSize = 1000;
    }

    if (this.listenToQueryParams) {
      this.registerSubscription(
        this.route.queryParams
          .pipe(
            filter(() => this.updateFilterOnQueryParams),
            // Restore filter
            mergeMap((queryParams) => this.restoreLastFilter(queryParams))
          )
          .subscribe((restored) => {
            // Apply initial filter
            this.setFilter(this.filterFormComponent().value, { updateQueryParams: restored, firstTime: !restored, emitEvent: true });
          })
      );
    }

    // Only for main table
    if (!this.subTable) {
      // Listen navigation start to keep previous url/page
      this.registerSubscription(
        this.router.events.pipe(filter((event) => event instanceof NavigationStart)).subscribe((event: NavigationStart) => {
          this.previousUrl = this.currentUrl;
          this.currentUrl = event.url;
        })
      );
    }

    if (!this.subTable) {
      // Register main table
      this.permissionService.registerMainView(this);
    }

    if (!this.subTable || this.addTable || this.selectTable) {
      // Register the table in permission service to allow refresh
      this.permissionService.registerView(this);
    }

    // Apply column widths from settings
    this.applyColumnWidths();

    // Create resizable column subscriptions
    this.registerSubscription(
      merge(this.resizableColumns.changes, this.baseColumns.changes).subscribe(() => this.registerColumnWidthSubscriptions())
    );
    this.registerColumnWidthSubscriptions();

    // Listen table events
    this.registerSubscription(
      merge(
        this.selection.changed.pipe(
          debounceTime(100),
          distinctUntilChanged(),
          map(() => this.singleSelectedRow)
        ),
        this.onCancelOrDeleteRow.pipe(map(() => this.singleSelectedRow))
      )
        .pipe(
          tap((row) => {
            // Mark as loading to prevent selection on loading phase
            if (!!row) this.markAsLoading();
          })
        )
        .subscribe(async (row) => {
          if (!!row) this.markAsLoading();
          await this.onAfterSelectionChange(row);
          if (!!row) this.markAsLoaded();
        })
    );

    // Reset selection on some events todo: merge with same subscription in ngOnInit
    this.registerSubscription(
      this.onRefresh.pipe(tap(() => this.markAsUntouched())).subscribe(() => {
        this.selection.clear();
        this.onAfterSelectionChange(undefined); // Force deselection
      })
    );

    // Listen loading subject
    this.registerSubscription(
      this.loadingSubject
        .pipe(
          filter(() => !!this.entityName),
          distinctUntilChanged(),
          debounce((loading) => (loading ? timer(70) : of(undefined)))
        )
        .subscribe((loading) => {
          this.progressBarMode$.set(loading ? 'indeterminate' : 'determinate');
        })
    );

    if (this.rightSplit()) {
      this.rightSplit().useTransition = true;
      this.registerSubscription(
        merge(this.rightSplit().dragEnd.pipe(map((value) => value.sizes)), this.rightSplit().transitionEnd)
          .pipe(
            tap((value) => {
              if (this.debug) console.debug(`${this.logPrefix} rightSplit moved`, value);
            })
          )
          .subscribe((value) => {
            this.rightSplitResized();
            this.saveRightPanelState(value);
          })
      );
    }
    if (this.bottomSplit()) {
      this.bottomSplit().useTransition = true;
      this.registerSubscription(
        merge(this.bottomSplit().dragEnd.pipe(map((value) => value.sizes)), this.bottomSplit().transitionEnd)
          .pipe(filter(() => this.bottomPanelVisible))
          .subscribe((value) => {
            this.bottomSplitResized();
            this.saveBottomPanelState(value);
          })
      );

      // Add listener on filter panel open event to adjust bottom split size (Mantis #65498)
      if (this.filterFormComponent()) {
        this.filterFormComponent()
          .base()
          .panelOpened.subscribe((opened) => {
            if (opened) {
              // Current filter panel height
              const filterHeight = this.filterFormComponent().base().filterPanelRef().nativeElement.children[0].offsetHeight;
              // Total visible height
              const totalHeight = this.bottomSplitRef().nativeElement.offsetHeight;
              // Calculate needed height for top panel
              const neededHeight =
                filterHeight +
                8 + // small gap
                (this.filterFormComponent().allowNamedFilter() ? 52 : 0) + // named filter
                (this.filterFormComponent().allowTabs() ? 32 : 0); // tabs
              // To percent
              const neededHeightPercent = (neededHeight * 100) / totalHeight;
              if (this._bottomPanelHeight > 100 - neededHeightPercent) {
                // Change bottom height value
                this._bottomPanelHeight = 100 - neededHeightPercent;
              }
            }
          });
      }
    }
  }

  protected async afterReady() {
    if (this.debug) console.debug(`${this.logPrefix} Table is ready`, this.getFilterEntityName());

    // Update filter component
    await this.filterFormComponent()?.updateEntityName(this.getFilterEntityName());

    // Configure hotkeys
    if (this.tableContainerRef()) {
      if (this.debug) console.debug(`${this.logPrefix} Register hotkeys on`, this.tableContainerRef().nativeElement);
      // Get modal component
      const top = await this.modalService.getModal();
      this.registerSubscription(
        this.hotkeys
          // Don't prevent default to allow 'select all' in an input element
          .addShortcut({
            keys: 'control.a',
            element: this.tableContainerRef().nativeElement,
            elementName: top?.component['name'],
            preventDefault: false,
          })
          .subscribe((event: KeyboardEvent) => {
            // Check if the table container is effectively the target
            if (event.target === this.tableContainerRef().nativeElement) {
              event.preventDefault();
              this.confirmEditCreate().then((confirmed) => {
                if (confirmed) {
                  this.selection.setSelection(...this.getRows());
                  this.markForCheck();
                }
              });
            }
          })
      );
      this.registerSubscription(
        this.hotkeys
          .addShortcut({ keys: 'control.shift.+', element: this.tableContainerRef().nativeElement })
          .pipe(filter(() => this.canEdit && this.inlineEdition))
          .subscribe(() => this.addRow())
      );
    }
  }

  preventGoBackIfLoading(event: Event) {
    if (this.subTable || this.loading || this.saving) event.preventDefault();
  }

  canDeactivate(): Observable<boolean> {
    if (this.debug) console.debug(`${this.logPrefix} canDeactivate component dirty: ${this.dirty}, valid: ${this.valid}`);

    const cancelEditing = () => {
      this.getEditingRows().forEach((row) => (row.editing = false));
    };

    const default$ = of({
      confirmed: true,
      save: false,
    });

    const ask$ = defer(() => from(Alerts.askSaveBeforeAction(this.alertCtrl, this.translate, { valid: this.valid })));

    const save$ = of(true).pipe(
      tap(() => {
        if (this.debug) console.debug(`${this.logPrefix} canDeactivate do save`);
      }),
      mergeMap(() => from(this.save())),
      tap((saved) => {
        if (saved) {
          cancelEditing();
        } else {
          if (this.debug) console.debug(`${this.logPrefix} canDeactivate save failed, cancel deactivate`);
        }
      })
    );

    const cancel$ = of(true).pipe(
      tap(() => {
        if (this.debug) console.debug(`${this.logPrefix} canDeactivate do cancel`);
      }),
      tap(() => {
        cancelEditing();
        if (this.dirty) {
          this.cancel(undefined, { interactive: false, selectPreviousSelectedRow: false }); // Cancel without confirmation
        }
      })
    );

    return of(this.dirty).pipe(
      mergeMap((dirty) => iif(() => dirty, ask$, default$)),
      tap((can) => {
        if (this.debug) console.debug(`${this.logPrefix} canDeactivate confirm: ${can.confirmed}`);
      }),
      mergeMap((can) =>
        iif(
          () => can.confirmed,
          // save if needed
          iif(() => can.save, save$, cancel$),
          // Cancel deactivating
          of(false)
        )
      )
    );
  }

  deactivate() {
    this.dataSource.disconnect();
  }

  async onAfterSelectionChange(row?: AsyncTableElement<E>) {
    await this.loadRightArea(row);
    this.afterSelectionChange.emit(row || this.singleSelectedRow);
  }

  minSelectedRowId(): number {
    if (this.selection.isEmpty()) return undefined;
    return this.selection.selected.map((row) => row.id).reduce((previousId, currentId) => Math.min(previousId, currentId), this.visibleRowCount - 1);
  }

  maxSelectedRowId(): number {
    if (this.selection.isEmpty()) return undefined;
    return this.selection.selected.map((row) => row.id).reduce((previousId, currentId) => Math.max(+previousId, +currentId), 0);
  }

  registerSubForm(subForm: IAppForm) {
    if (!subForm) {
      console.warn(`${this.logPrefix} Can't register an undefined sub form !`);
      return;
    }
    let toRegister: IAppForm;
    if (typeof subForm === 'function') {
      toRegister = new AppFormProvider(subForm);
    } else {
      toRegister = subForm;
    }
    if (!this._children.includes(toRegister)) {
      this._children.push(toRegister);
    }
    return this;
  }

  subFormDirty(dirty: boolean) {
    if (dirty && this.canEdit) {
      this.markRowAsDirty();
    }
  }

  updatePermission() {
    if (!this.subTable) {
      // Allow edition only if admin (by default)
      this.canEdit = this.accountService.isAdmin();
    }
    this.markForCheck();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    // Unregister other subscriptions
    this.columnWidthSubscriptions.forEach((sub) => sub.subscription.unsubscribe());
  }

  submitFilter(opts?: ISetFilterOptions) {
    if (!this.multipleSelection) {
      // Reset selection
      this.selection.clear();
    }

    // Save filter in settings (not first time)
    if (opts?.firstTime !== true) {
      this.saveLastFilter();
    }

    // Set full filter form value
    this.setFilter(this.filterFormComponent().value, {
      emitEvent: true,
      permanentSelectionChangedPrevented: this.addTable /* Prevent permanent selection changes if this table is in add mode */,
      ...opts,
    });
  }

  asFilter(source: Partial<F>): F {
    return (this._entityService || this._dataSource?.dataService)?.asFilter(source);
  }

  async setFilter(f: F, opts?: ISetFilterOptions) {
    f = this.asFilter(f) || f; // this.asFilter(f) should return a filter class, but for UnfilteredEntityMemoryService, it returns undefined
    await super.setFilter(f, opts);

    // Emit filter change
    this.afterFilterSet.emit(f);

    // if new query parameters, update current url in browser without reloading
    if (!this.subTable && toBoolean(opts?.updateQueryParams, this.updateQueryParamsOnFilter)) {
      const params = this.filterFormComponent().filterToQueryParam();
      if (params) {
        this.location.replaceState(this.location.path().split('?')[0], new HttpParams({ fromObject: params }).toString());
      }
    }
  }

  async resetFilter(f?: F, opts?: ISetFilterOptions<C>) {
    await this.filterFormComponent().setValue(f, { resetNamedFilter: true, ...opts });
    await this.filterFormComponent().submit(undefined, opts);
  }

  async saveLastFilter() {
    if (!this.usePageSettings || !this.autoSaveFilter) return;

    // Get current filter
    const currentFilter = BaseFilterUtils.clean(this.filter);
    const filterToSave = BaseFilterUtils.clean(this.filterFormComponent().value);
    if (Utils.deepEquals(currentFilter, filterToSave)) {
      return; // no need to save
    }

    if (this.debug) console.debug(`${this.logPrefix} Saving filter`, filterToSave);
    // If filter name is set, save it only
    if (isNotNilOrBlank(filterToSave?.name)) {
      delete filterToSave.criterias;
    } else if (isNotEmptyArray(filterToSave?.criterias)) {
      delete filterToSave.name;
    }
    // Save last filter in settings
    await this.settings.savePageSetting(filterSettingsId, filterToSave, this.getFilterSettingId());
    // Save also in user settings
    await this.accountService.save(this.accountService.account);
  }

  loadLastFilter(): F {
    if (!this.usePageSettings || !this.autoSaveFilter) return undefined;
    // Load filter from settings
    const savedFilter = BaseFilterUtils.clean(this.settings.getPageSettings(filterSettingsId, this.getFilterSettingId()));
    if (this.debug) console.debug(`${this.logPrefix} Restoring filter`, savedFilter);
    return this.asFilter(savedFilter);
  }

  protected getFilterSettingId(): string {
    return this.entityName;
  }

  protected getFilterEntityName(): string {
    return this.entityName;
  }

  async restoreLastFilter(queryParams?: Params): Promise<boolean> {
    if (queryParams && Object.keys(queryParams).length) {
      // Parse query params
      await this.filterFormComponent().queryParamToFilter(queryParams);
      return false;
    } else {
      // Or load from settings
      const value = this.loadLastFilter();
      if (this.debug) console.debug(`${this.logPrefix} load last filter`, value);
      await this.filterFormComponent().setValue(value, { findByName: true });
      return !value.isEmpty();
    }
  }

  get defaultReferentialCriteria(): ISelectCriteria {
    return { statusId: this.showDisabledReferential ? undefined : this.statusIds.ENABLE };
  }

  async dropColumn(event: CdkDragDrop<string[]>) {
    const offset = RESERVED_START_COLUMNS.length;
    moveItemInArray(this.displayedColumns, event.previousIndex + offset, event.currentIndex + offset);
    this.applyColumnWidths();
    await this.saveColumnSettings();
  }

  async openSelectColumnsModal(event?: UIEvent): Promise<any> {
    await super.openSelectColumnsModal(event);
    this.updateColumns();
    await this.saveColumnSettings();
    return undefined;
  }

  showColumnsOnly(columns: string[]) {
    if (isEmptyArray(columns)) return;

    // Hide others
    this.getDisplayColumns()
      .filter((column) => !RESERVED_START_COLUMNS.concat(...columns).includes(column))
      .forEach((column) => this.setShowColumn(column, false, { emitEvent: false }));

    // Show only
    columns.forEach((column) => this.setShowColumn(column, true));

    this.updateColumns();
  }

  async clickRow(event: MouseEvent | undefined, row: AsyncTableElement<E>, opts?: { force: boolean }): Promise<boolean> {
    if (!row || this.disabled) return false;

    if (!this.checkBoxSelection) {
      const toggle = this.multipleSelection && event?.ctrlKey;
      const range = this.multipleSelection && event?.shiftKey;

      if (toggle || range) {
        if (!(await this.confirmEditCreate())) return false;
      }

      if (toggle) {
        this.selection.toggle(row);
        event.preventDefault();
        return true;
      } else if (range) {
        if (this.selection.hasValue()) {
          const firstId = this.selection.selected[0].id;
          this.selection.select(...this.getRows().slice(firstId < row.id ? firstId : row.id, firstId > row.id ? firstId : row.id + 1));
        }
        event.preventDefault();
        return true;
      }
    } else {
      // If toggle only selection mode active, don't try to open row (Mantis #56265)
      if (this.toggleOnlySelection) {
        this.selection.toggle(row);
        return true;
      }
    }

    // Confirm editing row as fast as possible to avoid multiple editing rows
    const editingRows = this.getEditingRows().filter((editingRow) => editingRow.id !== row.id);
    if (this.canEdit && editingRows.length > 0) {
      for (const currentEditingRow of editingRows) {
        const confirmed = await this.confirmEditCreate(event, currentEditingRow);
        if (!confirmed) return false;
      }
    }

    // Call super
    if (await super.clickRow(event, row)) {
      if (opts?.force || this.selection.selected.length !== 1 || !this.selection.isSelected(row)) {
        // Note: both following actions are needed but will fire 2 change events
        this.selection.setSelection(row);
      }
      return true;
    }
    return false;
  }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<E>): Promise<boolean> {
    await this.waitIdle();
    // Call super
    const ok = await super.editRow(event, row);
    this.markAsLoading();
    if (ok && this.selectRowOnEdit && !this.selection.isSelected(row)) {
      // add row to selection if not
      this.selection.setSelection(row);
    } else {
      this.markAsLoaded();
    }
    return ok;
  }

  async addRow(event?: Event, insertAt?: number, opts?: { focusColumn?: string; editing?: boolean }): Promise<boolean> {
    return await super.addRow(event, insertAt || toNumber(this.singleSelectedRow?.id + 1));
  }

  getRows(): AsyncTableElement<E>[] {
    return this.dataSource.getRows() || [];
  }

  getDirtyRows(): AsyncTableElement<E>[] {
    return this.getRows().filter((row) => row.dirty);
  }

  getEditingRows(): AsyncTableElement<E>[] {
    return this.dataSource.getEditingRows() || [];
  }

  hasEditingRow(): boolean {
    return this.dataSource.hasSomeEditingRow();
  }

  // hasTouchedEditingRow(): boolean {
  //   return this.getEditingRows().some(value => value.validator?.controls)
  // }

  async updateView(res: LoadResult<E> | undefined, opts?: { emitEvent?: boolean }): Promise<void> {
    await super.updateView(res, opts);
    this.updateTableDirtyState();
  }

  updateTableDirtyState() {
    if (this.getRows().some((row) => row.validator?.dirty)) {
      this.markAsDirty();
    } else {
      this.markAsPristine();
    }
  }

  async duplicateRow(event: UIEvent, opts?: any) {
    event?.stopPropagation();
    if (this.selection.selected.length !== 1) return;

    const row = this.singleSelectedRow;
    if (!row || !(await this.confirmEditCreate(event, row))) return false;

    const newRow = await this.addRowToTable(row.id + 1);

    newRow.validator.patchValue(await this.patchDuplicateEntity(row.currentData, opts));
    newRow.validator.markAsDirty();

    // select
    await this.clickRow(undefined, newRow, { force: true });
  }

  multiEditRows(event: UIEvent) {}

  scrollToTop() {
    if (this.tableContainerRef()) {
      // scroll to top
      this.tableContainerRef().nativeElement.scrollTop = 0;
    }
  }

  scrollToBottom() {
    if (this.tableContainerRef()) {
      // scroll to bottom
      this.tableContainerRef().nativeElement.scroll({
        top: this.tableContainerRef().nativeElement.scrollHeight,
      });
    }
  }

  focusRow(row: AsyncTableElement<any>) {
    if (!row) return;

    if (!this.tableContainerRef()) {
      console.warn(`${this.logPrefix} This table has no scrollable container, abort focus`);
    }

    // get row element
    const rowElement = this.rowsView().find((item) => item.element.nativeElement.rowIndex === row.id + 1);
    if (rowElement) {
      setTimeout(() => {
        const tableOffset = 33; // table header height
        const tableScrollTop = this.tableContainerRef().nativeElement.scrollTop + tableOffset;
        const tableHeight = this.tableContainerRef().nativeElement.offsetHeight - tableOffset;
        const rowTop = rowElement.element.nativeElement.offsetTop;
        const rowHeight = rowElement.element.nativeElement.offsetHeight;
        let top: number = null;
        if (rowTop < tableScrollTop) {
          top = rowTop - tableOffset;
        } else if (rowTop + rowHeight > tableScrollTop + tableHeight) {
          top = rowTop - tableHeight;
        }
        if (top != null) {
          this.tableContainerRef().nativeElement.scrollTo({ top });
        }
      });
    }
  }

  async deleteRows(event: UIEvent | null, rows: AsyncTableElement<E>[], opts?: { interactive?: boolean }): Promise<number> {
    const shouldAlertOnException = !this.subTable && (!this.hasEditingRow() || this.singleEditingRow?.validator?.valid);
    try {
      const deletes = await super.deleteRows(event, rows, opts);
      if (deletes && !this.subTable) {
        // Update dirty state on main table (Mantis #59533)
        this.updateTableDirtyState();
      }
      return deletes;
    } catch (e) {
      // Try to customize the error
      const error = this.exceptionToError(e);
      // Show error in an alert dialog
      if (error && shouldAlertOnException) {
        this.resetError();
        await Alerts.showDetailedError(error, this.alertCtrl, this.translate);
      } else if (!this.error) {
        this.markAsError(error.message);
      }
    }
  }

  async cancel(event?: Event, opts?: { interactive?: boolean; selectPreviousSelectedRow?: boolean }): Promise<void> {
    let previousSelectedId = undefined;
    if (opts?.selectPreviousSelectedRow !== false) {
      previousSelectedId = this.singleSelectedRow?.currentData?.id;
    }
    await super.cancel(event, opts);
    if (previousSelectedId) {
      setTimeout(() => this.selectRowByData(<E>{ id: previousSelectedId }));
    }
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    let dataToKeep: E;
    try {
      // Try first to confirm row
      if (!(await this.confirmEditCreate())) {
        // Throw an error with current error if any
        throw { code: ErrorCodes.TABLE_INVALID_ROW_ERROR, message: this.error || 'ERROR.TABLE_INVALID_ROW_ERROR' };
      }

      // Determine if row keeps editing after save
      const keepEditing = !this.subTable && this.keepEditedRowOnSave && opts?.keepEditing !== false;
      if (keepEditing) {
        dataToKeep = this.singleEditingRow?.currentData || this.singleSelectedRow?.currentData;
      }

      // Call save without trying to reselect
      const saved = await super.save({ ...opts, keepEditing: false });

      if (saved) {
        if (!this.singleSelectedRow) {
          // Force deselection on this table and reset right view if no single row selected
          this.selection.clear(true);
          await this.onAfterSelectionChange();
        }

        this.markAsPristine({ emitEvent: false });
        this.markAsLoaded({ emitEvent: false });
      }

      return saved;
    } catch (e) {
      // Try to customize the error
      const error = this.exceptionToError(e);
      // Show error in an alert dialog
      if (error && !this.subTable) {
        this.resetError();
        await Alerts.showDetailedError(error, this.alertCtrl, this.translate);
      } else if (!this.error) {
        this.markAsError(error.message);
      }
    } finally {
      this.markForCheck();
      if (!!dataToKeep) {
        await this.selectRowByData(dataToKeep);
      }
    }
  }

  async export(event: MouseEvent, opts?: ExportOptions) {
    event?.preventDefault();

    await this.modalService.openModal<IExportModalOptions, IExportModel>(ExportModal, {
      table: this,
      columns: await this.buildExportColumns(),
      additionalColumns: await this.buildExportAdditionalColumns(),
      locale: this.settings.locale,
      sortBy: this.sortActive,
      sortDirection: this.sortDirection,
      ...opts,
    });
  }

  async buildExportColumns(): Promise<BaseColumnItem[]> {
    // Get displayed columns and add always exported columns
    const displayColumns = this.getDisplayColumns();
    alwaysExportedColumns.filter((column) => !displayColumns.includes(column)).forEach((column) => displayColumns.push(column));
    // Compute all fields mapped with translated names
    return this.columns
      .filter((column) => !notExportableColumns.includes(column) && !this.excludesColumns.includes(column))
      .map((column) => ({
        label: column,
        visible: displayColumns.includes(column),
        rankOrder: displayColumns.indexOf(column),
      }));
  }

  async buildExportAdditionalColumns(): Promise<ObjectMap<BaseColumnItem[]>> {
    return {};
  }

  /* Make it public */
  markRowAsDirty(row?: AsyncTableElement<E>, opts?: { onlySelf?: boolean; emitEVent?: boolean }) {
    super.markRowAsDirty(row, opts);
  }

  markAsError(message: string, opts?: { emitEvent?: boolean }) {
    this.setError(message, opts);
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    if (opts?.onlySelf !== true) {
      this.enableSubForms(true);
    }
    if (opts?.emitEvent !== false) {
      this.markForCheck();
    }
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.disable(opts);
    if (opts?.onlySelf !== true) {
      this.enableSubForms(false);
    }
    if (opts?.emitEvent !== false) {
      this.markForCheck();
    }
  }

  enableSubForms(enable: boolean) {
    this._children?.forEach((subForm) => {
      subForm['parentEnabled'] = enable;
      if (enable) {
        subForm.enable();
      } else {
        subForm.disable();
      }
    });
  }

  registerCustomErrors(customErrors: PropertyMap) {
    this._customErrors = { ...this._customErrors, ...customErrors };
  }

  entityI18nName(entityName: string, i18nRoot?: string): string {
    return `${i18nRoot}ENTITY.${changeCaseToUnderscore(entityName).toUpperCase()}`;
  }

  openRightPanel() {
    this._rightPanelVisible = true;
    this.saveRightPanelState();
  }

  toggleRightPanel(event: UIEvent) {
    this._rightPanelVisible = !this._rightPanelVisible;
    this.saveRightPanelState();
  }

  protected async rightMenuItemWillChange(event: PromiseEvent<boolean, IEntityMenuItem<E, RV>>) {
    const canSet = await this.setRightMenuItem(event.detail);
    event.detail.success(canSet);
  }

  protected async setRightMenuItem(menuItem: IEntityMenuItem<E, RV>): Promise<boolean> {
    if (!menuItem) {
      throw new Error(`Cannot determinate the type of right area`);
    }

    if (this.rightMenuItem && this.rightMenuItem !== menuItem) {
      const canChange = await this.canRightMenuItemChange(this.rightMenuItem, menuItem);
      if (!canChange) {
        return false;
      }
    }

    this.rightMenuItem = menuItem;
    await this.loadRightArea(); // TODO: pass current row
    return true;
  }

  protected async canRightMenuItemChange(previousMenuItem: IEntityMenuItem<E, RV>, nextMenuItem: IEntityMenuItem<E, RV>): Promise<boolean> {
    return true;
  }

  protected async loadRightArea(row?: AsyncTableElement<E>) {}

  toggleBottomPanel(event: UIEvent) {
    this._bottomPanelVisible = !this._bottomPanelVisible;
  }

  // protected methods

  // can be overridden to specify title
  protected updateTitle() {
    this.title = this.titleI18n ? this.translate.instant(this.titleI18n) : '';
  }

  protected exceptionToError(ex: any): DetailedError {
    console.error(ex);
    if (!ex) {
      // Should not happen
      return { message: 'no error message' };
    }
    if (typeof ex === 'string') return { message: ex };
    if (ex instanceof DetailedError) return ex;
    if (ex.code || ex.message) {
      // if a custom message is declared for this exception code, override the current error
      const message = this._customErrors[ex.code] || ex.message;
      const error: DetailedError = { code: ex.code, message };
      // Find detailed error message
      if (ex.details?.message && typeof ex.details.message === 'string') {
        const detailMessage: string = ex.details.message;
        if (detailMessage.startsWith('{') && detailMessage.endsWith('}')) {
          try {
            error.detail = JSON.parse(detailMessage);
          } catch (e) {
            // Try to decompose by regexp
            error.detail = {
              code: toInt(detailMessage.match(/"code":.(\d+),/)?.[1]),
              message: detailMessage.match(/"message":."(.+?(?=(\n|"[},])))/)?.[1] || detailMessage,
            };
          }
        } else {
          // Affect detail directly
          error.detail = ex.details;
        }
      }
      return error;
    }
    return ex; // todo: or return a default generic message ?
  }

  protected selectRowByIdOrData(id: number, data?: E): Promise<boolean> {
    // Patch this to allow duplicated row to be select by data (Mantis #62343)
    if (id > -1 && isNil(data?.id)) {
      id = undefined;
    }
    return super.selectRowByIdOrData(id, data);
  }

  protected async addRowToTable(insertAt?: number): Promise<AsyncTableElement<E>> {
    if (!(await this.canAddRow())) {
      return undefined;
    }

    // Reset selection
    this.selection.clear();

    return super.addRowToTable(insertAt);
  }

  protected onStartEditRow(row: AsyncTableElement<E>) {
    const previousDirty = row.validator?.dirty;

    row.validator = this.getRowValidator(row); // can be undefined

    if (previousDirty === true || row.id === -1) {
      row.validator?.markAsDirty();
    }

    this.registerRowDirtySubscription(row);
    this.registerRowValidSubscription(row);

    this.markForCheck();
  }

  patchRow(attribute: keyof E, values: any[], row?: AsyncTableElement<E>, opts?: { emitEvent: boolean }): boolean {
    if (!this.canEdit) {
      console.warn(`${this.logPrefix} patch called but the table cannot be edited`);
      return false;
    }
    if (isNilOrBlank(attribute)) {
      throw new Error(`${this.logPrefix} no attribute provided! Patch aborted.`);
    }

    // Get provided row or currently edited row or currently selected row
    row = row || this.singleEditingRow || this.singleSelectedRow;
    if (!row) {
      console.warn(`${this.logPrefix} no row to update`);
      return false;
    }

    if (this.debug) console.debug(`${this.logPrefix} row content before patch: ${attribute.toString()}=`, row.validator.value[attribute]);

    // Patch row validator
    row.validator.patchValue({ [attribute]: (values || []).slice() });
    if (opts?.emitEvent !== false) {
      this.markRowAsDirty(row);
      this.markForCheck();
    }

    if (this.debug) console.debug(`${this.logPrefix} row content after patch: ${attribute.toString()}=`, row.validator.value[attribute]);
    return true;
  }

  protected async patchDuplicateEntity(entity: E, opts?: any): Promise<any> {
    // Be sure the entity is an entity object
    const entityObject = this.fromObject(entity);
    const clonedEntity = {
      ...entityObject.asObject({ minify: false, keepTypename: true, ...opts }),
      id: null,
      creationDate: undefined,
      updateDate: undefined,
      comments: undefined,
    };
    // Patch additional properties
    await this.patchDuplicateAdditionalProperties(entity, clonedEntity, opts);
    return clonedEntity;
  }

  protected fromObject(entity: any): E {
    return <E>EntityClasses.fromObject(entity, { entityName: this.entityName });
  }

  protected async patchDuplicateAdditionalProperties(source: E, target: any, opts?: any) {}

  protected registerRowDirtySubscription(row: AsyncTableElement<E>) {
    this.unregisterRowDirtySubscription();
    if (row?.validator) {
      this.rowDirtySubscription = row.validator.valueChanges
        .pipe(
          tap((value) => {
            if (this.debug) console.debug(`${this.logPrefix} row (id=${row.id},dirty=${row.validator?.dirty}) value changed:`, value);
          }),
          map(() => row.validator?.dirty),
          filter((dirty) => dirty === true),
          distinctUntilChanged()
        )
        .subscribe(() => this.markAsDirty());
    }
  }

  protected unregisterRowDirtySubscription() {
    if (this.rowDirtySubscription) {
      this.unregisterSubscription(this.rowDirtySubscription);
      this.rowDirtySubscription.unsubscribe();
      this.rowDirtySubscription = undefined;
    }
  }

  protected registerRowValidSubscription(row: AsyncTableElement<E>) {
    this.unregisterRowValidSubscription();
    if (row?.validator) {
      this.rowValidSubscription = row.validator.statusChanges
        .pipe(
          tap((value) => {
            if (this.debug) console.debug(`${this.logPrefix} row (id=${row.id}) valid status changed:`, value);
          }),
          map(() => row.validator?.valid),
          distinctUntilChanged()
        )
        .subscribe(() => this.markForCheck());
    }
  }

  protected unregisterRowValidSubscription() {
    if (this.rowValidSubscription) {
      this.unregisterSubscription(this.rowValidSubscription);
      this.rowValidSubscription.unsubscribe();
      this.rowValidSubscription = undefined;
    }
  }

  protected async canAddRow(): Promise<boolean> {
    // Check using emitter
    if (this.canAddRowEvent.observed) {
      try {
        const canAdd = await emitPromiseEvent(this.canAddRowEvent, 'canAdd');
        if (!canAdd) return false;
      } catch (err) {
        if (err === 'CANCELLED') return false; // User cancel
        console.error(`${this.logPrefix} Error while checking if can add row`, err);
        throw err;
      }
    }

    return true;
  }

  protected async askCancelConfirmation(event?: UIEvent, rows?: AsyncTableElement<E>[]): Promise<boolean> {
    const confirmed = await Alerts.askCancelConfirmation(
      arraySize(rows) === 1 ? 'CONFIRM.CANCEL_ROW' : 'CONFIRM.CANCEL_ROWS',
      this.alertCtrl,
      this.translate,
      event
    );
    if (confirmed && this.singleEditingRow) {
      // Cancel or delete current edited row, if let super.askCancelConfirmation, many rows or removed (fixme in ngx-components)
      await this.cancelOrDelete(event, this.singleEditingRow, { keepEditing: false });
    }
    return confirmed;
  }

  protected getRowValidator(row: AsyncTableElement<E>, opts?: any): UntypedFormGroup {
    return this.validatorService?.getRowValidator(row.currentData, {
      entityName: this.entityName,
      newData: row.id === -1 || !row.currentData?.id,
      ...opts,
    });
  }

  protected async onDefaultRowCreated(row: AsyncTableElement<E>) {
    const value = await this.defaultNewRowValue();
    if (row.validator) {
      row.validator.patchValue(value);
    } else {
      Object.assign(row.currentData, value);
    }

    await this.clickRow(undefined, row);
    this.focusRow(row);
  }

  protected async defaultNewRowValue(): Promise<any> {
    return {
      entityName: this.entityName,
      statusId: StatusIds.ENABLE,
    };
  }

  protected updateColumns() {
    super.updateColumns();
    // Apply column widths on every column change
    this.applyColumnWidths();
  }

  protected getDisplayColumns(): string[] {
    let columns = super.getDisplayColumns().slice();

    // Remove duplicates (Mantis #65861)
    columns = columns.filter(uniqueValue);

    // fix: remove excluded columns
    columns = columns.filter((column) => !this.excludesColumns.includes(column));

    // set creationDate and updateDate hidden by default if no user customization
    const hiddenColumns = this.getDefaultHiddenColumns();
    if (!this.getUserColumns() && isNotEmptyArray(hiddenColumns)) {
      columns = columns.filter((column) => !hiddenColumns.includes(column));
    }

    return columns;
  }

  // Make it public
  getI18nColumnName(columnName: string): string {
    return super.getI18nColumnName(columnName);
  }

  // can be overridden to add more default hidden columns
  protected getDefaultHiddenColumns() {
    return defaultHiddenColumns;
  }

  protected updateSettingsId() {
    this.settingsId = this.generateTableId();
  }

  protected applySettings(opts?: { useSortColumnFromSettings?: boolean }) {
    // Update sort
    this.applySort(this.getSortedColumn(opts?.useSortColumnFromSettings));
    // Update page size
    this.pageSize = this.getPageSize();
  }

  protected getSortedColumn(forceSettings?: boolean): MatSortable {
    if (forceSettings) {
      const data = this.getPageSettings(SETTINGS_SORTED_COLUMN);
      const parts = data && data.split(':');
      if (parts && parts.length === 2) {
        return { id: parts[0], start: parts[1] === 'desc' ? 'desc' : 'asc', disableClear: false };
      }
    }
    return super.getSortedColumn();
  }

  protected applySort(sortable: MatSortable) {
    if (this.sort && (this.sort.active !== sortable.id || this.sort.direction !== sortable.start)) {
      this.disableSort();
      this.sort.sort({ ...sortable, disableClear: true });
      this.enableSort();
    }
    this.markForCheck();
  }

  protected generateTableId(): string {
    const path = this.location.path().replace(/[?].*$/g, '');
    const lastPath = path.substring(path.lastIndexOf('/') + 1).toLowerCase();
    const selector = this.constructor['ɵcmp']?.selectors?.[0];
    this.usePageSettings = !!selector || isNotNilOrBlank(this.entityName);
    // Fix Mantis #59965: Use better identification: last part of the current url + component selector
    return `${lastPath}_${selector || 'table'}`;
  }

  protected async saveColumnSettings() {
    if (!this.usePageSettings) return;
    const columns =
      this.getCurrentColumns()
        .filter((c) => c.visible)
        .map((c) => c.name) || [];

    // Save columns position in setting
    await this.savePageSettings(columns, SETTINGS_DISPLAY_COLUMNS);
  }

  protected getColumnWidthSettings(): ColumnWidthSetting[] {
    if (!this.usePageSettings) return [];
    return this.getPageSettings(tableSettings.columnSizes) || [];
  }

  protected applyColumnWidths() {
    setTimeout(() => {
      this.cd.detectChanges(); // Needed to detect added columns
      this.getColumnWidthSettings().forEach((setting) => {
        if (this.debug) console.debug(`${this.logPrefix} Apply saved width on column '${setting.column}' width: ${setting.width}`);
        this.resizableColumns
          .find((column) => column.resizable && column.columnDef?.name === setting.column)
          ?.onResize(setting.width, { emitEvent: false });
        this.baseColumns
          .filter((column) => !!column.resizableColumn)
          .map((column) => column.resizableColumn)
          .find((column) => column.resizable && column.columnDef?.name === setting.column)
          ?.onResize(setting.width, { emitEvent: false });
      });
      this.markForCheck();
    }, 10);
  }

  protected async saveColumnWidth(column: string, width: number) {
    if (!this.usePageSettings) return;
    const columns = this.getColumnWidthSettings();
    const currentSetting = columns.find((c) => c.column === column);
    if (currentSetting) {
      currentSetting.width = width;
    } else {
      columns.push({ column, width });
    }

    await this.savePageSettings(columns, tableSettings.columnSizes);
  }

  protected registerColumnWidthSubscriptions() {
    this.resizableColumns
      .filter((column) => column.resizable && !!column.columnDef?.name)
      .forEach((column) => this.registerColumnWidthSubscription(column));
    this.baseColumns
      .filter((column) => !!column.resizableColumn)
      .map((column) => column.resizableColumn)
      .filter((column) => column.resizable && !!column.columnDef?.name)
      .forEach((column) => this.registerColumnWidthSubscription(column));
  }

  protected registerColumnWidthSubscription(column: ResizableComponent) {
    // If no observer on size changed event
    if (!column.sizeChanged.observed) {
      const name = column.columnDef.name;
      // Create subscription
      const subscription = column.sizeChanged.pipe(debounceTime(500)).subscribe((width) => this.saveColumnWidth(name, width));
      const existing = this.columnWidthSubscriptions.find((sub) => sub.name === name);
      if (existing) {
        // Clear previous subscription and replace it
        existing.subscription.unsubscribe();
        existing.subscription = subscription;
      } else {
        // Register new one
        this.columnWidthSubscriptions.push({ name, subscription });
      }
    }
  }

  protected restoreRightPanel(defaultVisible: boolean, defaultWidth: number) {
    if (this.addTable || this.selectTable || !this.usePageSettings || !this.rightPanelAllowed) return;
    const setting: SplitPanelSetting = this.getPageSettings(tableSettings.rightPanel);
    this._rightPanelVisible = toBoolean(setting?.visible, defaultVisible);
    this._rightPanelWidth = Math.min(setting?.width || defaultWidth, 100 - defaultAreaMinSize);
  }

  protected restoreBottomPanel(defaultVisible: boolean, defaultHeight: number) {
    if (this.addTable || this.selectTable || !this.usePageSettings) return;
    const setting: SplitPanelSetting = this.getPageSettings(tableSettings.bottomPanel);
    this._bottomPanelVisible = toBoolean(setting?.visible, defaultVisible);
    this._bottomPanelHeight = Math.min(setting?.height || defaultHeight, 100 - defaultAreaMinSize);
  }

  protected async saveRightPanelState(event?: IOutputAreaSizes) {
    if (this.addTable || this.selectTable || !this.usePageSettings || !this.rightPanelAllowed) return;
    if (!event && this.rightSplit()) {
      event = this.rightSplit().getVisibleAreaSizes();
    }
    const setting: SplitPanelSetting = {
      visible: this.rightPanelVisible,
      // save new width if visible and valid number, or preserve previous width
      width: this.rightPanelVisible && typeof event?.[1] === 'number' ? event[1] : this.rightPanelWidth,
    };
    const previous = this.getPageSettings(tableSettings.rightPanel);
    if (Utils.equals(setting, previous)) return;
    await this.savePageSettings(setting, tableSettings.rightPanel);
  }

  protected async saveBottomPanelState(event: IOutputAreaSizes) {
    if (this.addTable || this.selectTable || !this.usePageSettings) return;
    const setting: SplitPanelSetting = {
      visible: this.bottomPanelVisible,
      // save new height not greater than the limit, or preserve previous height
      height: this.bottomPanelVisible && typeof event?.[1] === 'number' && event[1] < 90 ? event[1] : this.bottomPanelHeight,
    };
    await this.savePageSettings(setting, tableSettings.bottomPanel);
  }

  protected async savePageSettings(value: any, property: string) {
    await super.savePageSettings(value, property);
    if (this.debug) console.debug(`${this.logPrefix} Page settings saved:`, value);
    this.emitSaveSettings.emit();
  }

  protected rightSplitResized() {
    // Update filter view
    this.filterFormComponent()?.updateView();
    // Try to get the paginator height
    if (!!this.paginatorRef()) {
      this.paginatorHeight = `${this.paginatorRef().nativeElement.offsetHeight}px`;
      this.markForCheck();
    }
    // Follow to child tables
    this._children
      .filter((child) => child instanceof BaseTable)
      .map((child) => child as BaseTable<any, any, any, any>)
      .forEach((childTable) => childTable.rightSplitResized());
  }

  protected bottomSplitResized() {
    this._bottomPanelHeight = +this.bottomSplit().displayedAreas[1].size;
  }

  protected async handleError(error: any) {
    // if 401 : unauthorized or authentication timed out -> try to log in
    if (error?.code === 401) {
      await this.permissionService.accountExpired();
    }
  }
}
