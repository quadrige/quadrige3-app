import { WaitForOptions } from '@sumaris-net/ngx-components/src/app/shared/observables';
import { ISelectCriteria, ISelectModalOptions, ISetFilterOptions } from '@app/shared/model/options.model';
import { EventEmitter } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';

import { IBaseEntitiesService } from '@app/shared/service/base-entity.service';
import { MatAutocompleteFieldConfig } from '@sumaris-net/ngx-components/src/app/shared/material/autocomplete/material.autocomplete.config';
import { ExportType } from '@app/shared/component/export/export.model';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { IBaseEntityFilter, IBaseEntityFilterCriteria } from '@app/shared/model/filter.model';
import { RightFetchOptions } from '@app/referential/right/right.model';
import { ColumnItem } from '@sumaris-net/ngx-components';

export const defaultHiddenColumns = ['creationDate', 'updateDate'];
export const notExportableColumns = ['select', 'actions'];
export const alwaysExportedColumns = ['statusId'];
export const tableSettings = {
  filter: 'filter',
  columnSizes: 'sizeColumns',
  rightPanel: 'rightPanel',
  bottomPanel: 'bottomPanel',
};
export const filterSettingsId = 'filters';

export declare interface BaseColumnItem extends ColumnItem {
  rankOrder?: number;
}

export declare interface ColumnWidthSetting {
  column: string;
  width: number;
}

export declare interface SplitPanelSetting {
  visible: boolean;
  width?: number;
  height?: number;
}

export interface IBaseTable<E, F> {
  entityName: string;
  entityService: IBaseEntitiesService<E, F, any>;
  titleI18n: string;
  title: string;
  i18nColumnPrefix: string;
  autocompleteFields: { [key: string]: MatAutocompleteFieldConfig };
  filter: F;
  loading: boolean;
  canEdit: boolean;
  totalRowCount: number;
  checkBoxSelection: boolean;
  selection: SelectionModel<AsyncTableElement<E>>;
  subTable: boolean;

  isAllSelected(): boolean;

  masterToggle(event: UIEvent): Promise<void>;

  toggleSelectRow(event: UIEvent, row: AsyncTableElement<E>): void;

  clickRow(event: MouseEvent | undefined, row: AsyncTableElement<E>, opts?: { force: boolean }): Promise<boolean>;

  ready(opts?: WaitForOptions): Promise<void>;

  asFilter(source?: Partial<F>): F;

  submitFilter(opts?: ISetFilterOptions): void;

  resetFilter(value?: F, opts?: ISetFilterOptions): Promise<void>;

  entityI18nName(entityName: string, i18nRoot?: string): string;

  getI18nColumnName(columnName: string): string;

  patchRow(attribute: keyof E, values: any[], row?: AsyncTableElement<E>, opts?: { emitEvent: boolean }): boolean;

  markRowAsDirty(
    row?: AsyncTableElement<E>,
    opts?: {
      onlySelf?: boolean;
      emitEVent?: boolean;
    }
  ): void;

  markAsError(e: string): void;
}

export interface ISelectTable<E> extends IBaseTable<E, any> {
  selectTable: boolean;
  titlePrefixI18n?: string;

  selectCriteria: ISelectCriteria;

  openAddEntities?: EventEmitter<ISelectModalOptions>;
}

export interface IRightSelectTable<E> extends ISelectTable<E> {
  rightOptions: RightFetchOptions;
}

export interface IAddTable<E> extends IBaseTable<E, any> {
  addTable: boolean;
  titlePrefixI18n?: string;
  titleAddPrefixI18n?: string;

  addCriteria: ISelectCriteria;
  addFirstCriteria: ISelectCriteria;

  permanentSelection: SelectionModel<E>;
  selectedEntities: any[];
  selectedEntityIds: any[];
}

export interface ExportOptions {
  forceAllColumns?: boolean;
  exportType?: ExportType;
  showSelectType?: boolean;
  defaultFileName?: string;
  defaultFilter?: IBaseEntityFilter<any, IBaseEntityFilterCriteria<any>>;
  rowCountWarningThreshold?: number;
  transcribingItemEnabled?: boolean;
  rightsEnabled?: boolean;
}
