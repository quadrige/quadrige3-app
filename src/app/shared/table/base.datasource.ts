import {
  EntitiesAsyncTableDataSource,
  EntitiesAsyncTableDataSourceConfig,
  EntitiesServiceWatchOptions,
  Entity,
  EntityFilter,
  IEntitiesService,
  LoadResult,
} from '@sumaris-net/ngx-components';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { Observable } from 'rxjs';

export interface BaseTableDataSourceConfig<E extends Entity<E, ID>, ID, WO extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions>
  extends EntitiesAsyncTableDataSourceConfig<E, ID, WO> {
  onError?: (error: any) => void;
}

export class BaseTableDatasource<
  E extends Entity<E, ID>,
  ID,
  F extends EntityFilter<F, E, ID>,
  WO extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions,
> extends EntitiesAsyncTableDataSource<E, F, ID, WO> {
  constructor(
    dataType: new () => E,
    dataService: IEntitiesService<E, F>,
    validatorService: BaseValidatorService<E>,
    config?: BaseTableDataSourceConfig<E, ID, WO>
  ) {
    super(dataType, dataService, validatorService, config);
  }

  get config(): BaseTableDataSourceConfig<E, ID, WO> {
    return super.config;
  }

  handleError(error: any, message: string): Observable<LoadResult<E>> {
    if (this.config?.onError) {
      this.config.onError(error);
    }
    return super.handleError(error, message);
  }

  handleServiceError(error: any) {
    if (this.config?.onError) {
      this.config.onError(error);
    }
    super.handleServiceError(error);
  }
}
