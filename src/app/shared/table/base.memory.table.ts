import { Directive, Injector, OnInit } from '@angular/core';
import { firstFalsePromise, IEntity, KeyType } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { filter } from 'rxjs/operators';
import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { EntitiesMemoryService } from '@app/shared/service/entities-memory.service';
import { BaseTable } from '@app/shared/table/base.table';
import { BaseTableDataSourceConfig } from '@app/shared/table/base.datasource';
import { BaseEntityFilter, BaseEntityFilterCriteria } from '@app/shared/model/filter.model';

@Directive()
export abstract class BaseMemoryTable<
    E extends IEntity<E, ID>,
    ID extends KeyType,
    F extends BaseEntityFilter<F, C, E, ID>,
    C extends BaseEntityFilterCriteria<E, ID>,
    V extends BaseValidatorService<E> = any,
  >
  extends BaseTable<E, ID, F, C, V>
  implements OnInit
{
  protected constructor(
    protected injector: Injector,
    protected columns: string[],
    public readonly dataType: new () => E,
    protected entityMemoryService: EntitiesMemoryService<E, F, ID>,
    protected validatorService: V,
    options?: BaseTableDataSourceConfig<E, ID, any>
  ) {
    super(injector, columns, dataType, entityMemoryService, validatorService, {
      ...options,
      saveOnlyDirtyRows: false, // Force save all rows
    });
  }

  get value(): E[] {
    return this.entityMemoryService.value;
  }

  ngOnInit() {
    // Disable confirmation before save (must be done before super.ngOnInit())
    this.confirmBeforeSave = false;

    super.ngOnInit();

    // Override default options
    this.saveBeforeDelete = true; // Mantis #61195

    // Listen end of loading to refresh total row count
    this.registerSubscription(
      this.loadingSubject.pipe(filter((value) => !value)).subscribe(() => {
        if (this.value === undefined) {
          // this will remove the 'no result' template if the selection is voluntarily empty (=== undefined)
          this.totalRowCount = null;
        }
      })
    );
  }

  async setValue(value: E[], opts?: { emitEvent?: boolean }) {
    // Wait saving
    await firstFalsePromise(this.savingSubject);
    for (const row of this.getEditingRows()) {
      await this.cancelOrDelete(undefined, row, { keepEditing: false });
    }

    // Reset previous error
    if (this.error) {
      this.markAsError(null);
    }

    const emitRefresh = !opts || opts.emitEvent !== false;
    if (emitRefresh) {
      this.markAsLoading();
    }
    this.entityMemoryService.value = value || [];
    if (emitRefresh) {
      this.emitRefresh();
    }

    // Wait loading
    await this.waitIdle();
  }

  async deleteRows(event: UIEvent | null, rows: AsyncTableElement<E>[], opts?: { interactive?: boolean }): Promise<number> {
    // Cancel current row before delete (fixme: why was it necessary ? seems to be useless because saveBeforeDelete is enabled)
    // await this.cancelAllEditingRows(event);
    return super.deleteRows(event, rows, opts);
  }
}
