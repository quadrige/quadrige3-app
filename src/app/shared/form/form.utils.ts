import { AbstractControlOptions, ValidatorFn } from '@angular/forms';

export type FormGroupConfig<E> = {
  [P in keyof E]: [E[P] | { value: E[P]; disabled: boolean }, (AbstractControlOptions | ValidatorFn | ValidatorFn[])?];
};
