import { AfterViewInit, ChangeDetectorRef, Directive, EventEmitter, inject, Injector, Input, Output, signal, WritableSignal } from '@angular/core';
import { AppForm, ENVIRONMENT, toBoolean } from '@sumaris-net/ngx-components';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { ModalService } from '@app/shared/component/modal/modal.service';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { debounce, of, timer } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

@Directive()
export abstract class BaseForm<E> extends AppForm<E> implements AfterViewInit {
  @Input() defaultReferentialCriteria: any; // todo: maybe move it to ModalComponent ?
  @Input() set canEdit(canEdit: boolean) {
    this._canEdit = canEdit;
  }

  get canEdit(): boolean {
    return this._canEdit;
  }

  @Output() dirtyEvent = new EventEmitter<boolean>();
  @Output() errorEvent = new EventEmitter<string>();

  protected logPrefix = '[base-form]';
  protected progressBarMode$: WritableSignal<ProgressBarMode> = signal('determinate');

  protected readonly modalService = inject(ModalService);
  protected readonly formBuilder = inject(UntypedFormBuilder);
  protected readonly environment = inject(ENVIRONMENT);
  private readonly cd = inject(ChangeDetectorRef);
  private _canEdit: boolean;

  protected constructor(
    protected injector: Injector,
    protected formGroup?: UntypedFormGroup
  ) {
    super(injector, formGroup);
    this.debug = toBoolean(!this.environment?.production, true);
  }

  ngAfterViewInit() {
    // Listen loading subject
    this.registerSubscription(
      this.loadingSubject
        .pipe(
          distinctUntilChanged(),
          debounce((loading) => (loading ? timer(70) : of(undefined)))
        )
        .subscribe((loading) => {
          this.progressBarMode$.set(loading ? 'indeterminate' : 'determinate');
        })
    );
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.canEdit === false) {
      this.disable(opts);
      return;
    }
    super.enable(opts);
  }

  markAsDirty(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (opts?.emitEvent !== false) {
      this.dirtyEvent.emit(true);
    }
    super.markAsDirty(opts);
  }

  markAsError(message: string, opts?: { emitEvent?: boolean }) {
    if (opts?.emitEvent !== false) {
      this.errorEvent.emit(message);
    }
    this.setError(message, opts);
  }

  protected detectChanges() {
    this.cd.detectChanges();
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
