import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { BaseEntityFilterCriteria } from '@app/shared/model/filter.model';

@Pipe({
  name: 'criteriaToObject',
  pure: false, // Important to update transformed value on change notification
})
@Injectable({ providedIn: 'root' })
export class CriteriaToObjectPipe implements PipeTransform {
  constructor() {}

  transform(obj: any, args?: any): any {
    return obj instanceof BaseEntityFilterCriteria ? obj.asObject() : obj;
  }
}
