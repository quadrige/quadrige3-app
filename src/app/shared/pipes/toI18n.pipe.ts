import { Pipe, PipeTransform } from '@angular/core';
import { toBoolean } from '@sumaris-net/ngx-components';
import { Utils } from '@app/shared/utils';

@Pipe({
  name: 'toI18nKey',
  standalone: true,
})
export class ToI18nKeyPipe implements PipeTransform {
  transform(entityName: string, multiple?: boolean) {
    return Utils.toI18nKey(entityName, toBoolean(multiple, false));
  }
}
