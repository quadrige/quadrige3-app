import { Pipe, PipeTransform } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';

@Pipe({ name: 'trimLineBreaks' })
export class TrimLineBreaksPipe implements PipeTransform {
  constructor() {}

  transform(v: string): SafeHtml {
    return v?.replace(/(<br\/>)+/g, '<br/>');
  }
}
