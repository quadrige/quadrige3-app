import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { AppFormModule, CorePipesModule, ResizableModule, SharedModule } from '@sumaris-net/ngx-components';
import { TranslateModule } from '@ngx-translate/core';
import { TableButtonsComponent } from '@app/shared/component/table/table-buttons.component';
import { IonicModule } from '@ionic/angular';
import { StatusColumnComponent } from '@app/shared/component/column/status-column.component';
import { AngularSplitModule } from 'angular-split';
import { MaterialSharedModule } from '@app/shared/material/material.shared.module';
import { ComboColumnComponent } from '@app/shared/component/column/combo-column.component';
import { CreationDateColumnComponent } from '@app/shared/component/column/creation-date-column.component';
import { UpdateDateColumnComponent } from '@app/shared/component/column/update-date-column.component';
import { ActionsColumnComponent } from '@app/shared/component/column/actions-column.component';
import { ExportModal } from '@app/shared/component/export/export.modal';
import { MapModule } from '@app/shared/component/map/map.module';
import { FormsModule } from '@angular/forms';
import { BooleanColumnComponent } from '@app/shared/component/column/boolean-column.component';
import { StatusMultiEditModal } from '@app/shared/component/multi-edit/status.multi-edit.modal';
import { MonthColumnComponent } from '@app/shared/component/column/month-column.component';
import { EntityMenuComponent } from '@app/shared/component/menu/entity-menu.component';
import { TopMenuComponent } from '@app/shared/component/menu/top-menu.component';
import { RouterModule } from '@angular/router';
import { SelectColumnComponent } from '@app/shared/component/column/select-column.component';
import { TextFrameComponent } from '@app/shared/component/text/text-frame.component';
import { SanitizeHtmlPipe } from '@app/shared/pipes/sanitize-html.pipe';
import { CriteriaToObjectPipe } from '@app/shared/pipes/referential-filter-to-object.pipe';
import { IdColumnComponent } from '@app/shared/component/column/id-column.component';
import { TextColumnComponent } from '@app/shared/component/column/text-column.component';
import { NumberColumnComponent } from '@app/shared/component/column/number-column.component';
import { DateColumnComponent } from '@app/shared/component/column/date-column.component';
import { ExpandButtonComponent } from '@app/shared/component/expand-button/expand-button.component';
import { TrimLineBreaksPipe } from '@app/shared/pipes/trim-line-breaks.pipe';
import { TimeColumnComponent } from '@app/shared/component/column/time-column.component';
import { TableFooterComponent } from '@app/shared/component/table/table-footer.component';
import { EmptySelectionPanelComponent } from '@app/shared/component/empty-panel/empty-selection-panel.component';
import { ToI18nKeyPipe } from '@app/shared/pipes/toI18n.pipe';

const appModules = [
  FormsModule,
  ResizableModule,
  MapModule,
  MaterialSharedModule,
  AngularSplitModule,
  CorePipesModule,
  EmptySelectionPanelComponent,
  ToI18nKeyPipe,
];
const appComponents = [
  TableButtonsComponent,
  TableFooterComponent,
  ExpandButtonComponent,
  IdColumnComponent,
  TextColumnComponent,
  NumberColumnComponent,
  DateColumnComponent,
  TimeColumnComponent,
  SelectColumnComponent,
  StatusColumnComponent,
  MonthColumnComponent,
  CreationDateColumnComponent,
  UpdateDateColumnComponent,
  ComboColumnComponent,
  BooleanColumnComponent,
  ActionsColumnComponent,
  CriteriaToObjectPipe,
  SanitizeHtmlPipe,
  TrimLineBreaksPipe,
  ExportModal,
  StatusMultiEditModal,
  TopMenuComponent,
  EntityMenuComponent,
  TextFrameComponent,
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    IonicModule,
    TranslateModule.forChild(),
    SharedModule,
    AppFormModule,

    // Application modules
    ...appModules,
    NgOptimizedImage,
  ],
  declarations: appComponents,
  exports: [
    CommonModule,
    SharedModule,
    IonicModule,

    // Application modules and components
    ...appModules,
    ...appComponents,
  ],
})
export class QuadrigeSharedModule {}
