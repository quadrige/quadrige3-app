import { TranslateHttpLoader as OriginTranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable } from 'rxjs';

export class TranslateHttpLoader extends OriginTranslateHttpLoader {
  getTranslation(lang: string): Observable<object> {
    // Remove country code from language
    if (lang?.includes('_')) lang = lang.substring(0, lang.indexOf('_'));
    return super.getTranslation(lang);
  }
}
