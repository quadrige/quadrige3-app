import { defer, from, MonoTypeOperatorFunction, Observable, pipe } from 'rxjs';
import { concatMap, filter, map, tap } from 'rxjs/operators';

/**
 * An operator which filters the data based on an async predicate function returning promise<boolean>
 *
 * NOTE:
 * By using **concatMap**, the order of original data events is preserved and the filter runs sequentially for each data event
 * If you want to run the filter in parallel without preserving order, use **flatMap**
 * See the accepted answer at: https://stackoverflow.com/questions/28490700/is-there-an-async-version-of-filter-operator-in-rxjs
 *
 * @param predicate
 */
export const filterAsync = <T>(predicate: (value: T, index: number) => Promise<boolean>): MonoTypeOperatorFunction<T> => {
  let count = 0;
  return pipe(
    // Convert the predicate Promise<boolean> to an observable (which resolves the promise,
    // Then combine the boolean result of the promise with the input data to a container object
    concatMap((data: T) => {
      return from(predicate(data, count++)).pipe(map((isValid) => ({ filterResult: isValid, entry: data })));
    }),
    // Filter the container object synchronously for the value in each data container object
    filter((data) => data.filterResult === true),
    // remove the data container object from the observable chain
    map((data) => data.entry)
  );
};

/**
 * see https://indepth.dev/posts/1222/create-a-taponce-custom-rxjs-operator
 *
 * @param fn
 */
export const tapOnce =
  <T>(fn: (value: any) => void): MonoTypeOperatorFunction<T> =>
  (source: Observable<any>) =>
    defer(() => {
      let first = true;
      return source.pipe(
        tap<T>((payload) => {
          if (first) {
            fn(payload);
          }
          first = false;
        })
      );
    });
