import { MatStepperIntl } from '@angular/material/stepper';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StepperI18n extends MatStepperIntl {
  constructor(translate: TranslateService) {
    super();
    this.optionalLabel = translate.instant('COMMON.OPTIONAL');
  }
}
