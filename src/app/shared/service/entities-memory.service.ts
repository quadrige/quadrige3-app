import { Entity, InMemoryEntitiesService, InMemoryEntitiesServiceOptions } from '@sumaris-net/ngx-components';
import { SortDirection } from '@angular/material/sort';
import { EntityUtils } from '@app/shared/entity.utils';
import { Job } from '@app/social/job/job.model';
import { IBaseEntitiesService, IExportContext } from '@app/shared/service/base-entity.service';
import { BaseEntityFilter } from '@app/shared/model/filter.model';

/**
 * In memory entities service for base referential
 */
export abstract class EntitiesMemoryService<E extends Entity<E, ID>, F extends BaseEntityFilter<F, any, E, ID>, ID = number>
  extends InMemoryEntitiesService<E, F, ID>
  implements IBaseEntitiesService<E, F, any>
{
  protected constructor(
    protected dataType: new () => E,
    protected filterType: new () => F,
    options?: InMemoryEntitiesServiceOptions<E, F>
  ) {
    super(dataType, filterType, {
      // Implementation options
      ...options,
      sortByReplacement: { id: 'id', ...options?.sortByReplacement },
    });
  }

  asFilter(source: Partial<F>): F {
    return super.asFilter(source || {});
  }

  sort(data: E[], sortBy?: string, sortDirection?: SortDirection): E[] {
    // Replace sortBy, using the replacement map
    sortBy = (sortBy && this.sortByReplacement[sortBy]) || sortBy || 'id';

    // Use natural ordering
    return EntityUtils.sort(data, sortBy, sortDirection);
  }

  deleteAll(dataToRemove: E[], options?: any): Promise<any> {
    if (this.debug) {
      console.debug('[entities-memory-service] deleteAll', dataToRemove);
    }
    return super.deleteAll(dataToRemove, options);
  }

  exportAllAsync(context: IExportContext, filter: F): Promise<Job> {
    throw new Error('Not implemented');
  }
}

/**
 * In memory entities service for base referential (no filter)
 */
export abstract class UnfilteredEntitiesMemoryService<E extends Entity<E, ID>, ID = number> extends EntitiesMemoryService<E, any, ID> {
  protected constructor(
    protected dataType: new () => E,
    options?: InMemoryEntitiesServiceOptions<E, void>
  ) {
    super(dataType, undefined, {
      filterFnFactory: () => () => true, // empty filter factory
      ...options,
    });
  }

  asFilter(source: Partial<any>): any {
    return undefined;
  }
}
