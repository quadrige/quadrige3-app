import { IEntitiesService, ObjectMap } from '@sumaris-net/ngx-components';
import { Job } from '@app/social/job/job.model';
import { ExportType } from '@app/shared/component/export/export.model';

export interface IExportContext {
  exportType: ExportType;
  fileName: string;
  entityName: string;
  locale?: string;
  headers?: string[];
  additionalHeaders?: ObjectMap<string[]>;
  sortBy?: string;
  sortDirection?: string;
  withTranscribingItems?: boolean;
  withRights?: boolean;
}

export interface IBaseEntitiesService<E, F, WO> extends IEntitiesService<E, F, WO> {
  exportAllAsync(context: IExportContext, filter: F): Promise<Job>;
}
