import { Injectable } from '@angular/core';
import {
  FileEvent,
  FileProgressEvent,
  FileResponse,
  filterNotNil,
  isNilOrBlank,
  isNotNil,
  isNotNilOrBlank,
  NetworkService,
} from '@sumaris-net/ngx-components';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpEventType, HttpRequest } from '@angular/common/http';
import { map } from 'rxjs/operators';

export interface UploadOptions {
  replace?: boolean;
  resourceType?: string;
  resourceId?: string;
}

@Injectable({ providedIn: 'root' })
export class FileTransferService {
  constructor(
    private network: NetworkService,
    private http: HttpClient
  ) {}

  downloadFile(file: string): string {
    return `${this.network.peer.url}/download/${file}`;
  }

  downloadResource(type: string, file: string): string {
    return `${this.network.peer.url}/download/resource/${type}/${file}`;
  }

  uploadResource(file: File, opts?: UploadOptions): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    if (opts) {
      if (isNotNilOrBlank(opts.resourceType)) formData.append('resourceType', opts.resourceType);
      if (isNotNilOrBlank(opts.resourceId)) formData.append('resourceId', opts.resourceId);
      if (isNotNil(opts.replace)) formData.append('replace', opts.replace ? 'true' : 'false');
    }

    const req = new HttpRequest('POST', `${this.network.peer.url}/upload/resource`, formData, {
      reportProgress: true,
      responseType: 'json',
    });

    return this.http.request(req);
  }

  uploadShapefile(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.network.peer.url}/upload/shapefile`, formData, {
      reportProgress: true,
      responseType: 'json',
    });

    return this.http.request(req);
  }

  async deleteResource(resourceType: string, filename: string): Promise<boolean> {
    if (isNilOrBlank(resourceType) || isNilOrBlank(filename)) {
      console.error(`[file-transfer-service] resourceType and filename must not be blank`);
      return;
    }
    const formData: FormData = new FormData();
    formData.append('resourceType', resourceType);
    formData.append('filename', filename);

    const req = new HttpRequest('POST', `${this.network.peer.url}/delete`, formData, {
      responseType: 'json',
    });

    try {
      const res = await this.http.request(req).toPromise();
      if (res instanceof HttpErrorResponse) {
        console.error(`[file-transfer-service] Error while deleting resource`, res.message);
        return false;
      }
      return true;
    } catch (err) {
      console.error(`[file-transfer-service] Error while deleting resource`, err);
      return false;
    }
  }

  protected castHttpEvent(observable: Observable<HttpEvent<any>>): Observable<FileEvent<any>> {
    return filterNotNil(
      observable.pipe(
        map((event) => {
          if (event?.type === HttpEventType.UploadProgress) {
            return event as FileProgressEvent;
          }
          if (event?.type === HttpEventType.Response) {
            return event as FileResponse<any>;
          }
          return undefined;
        })
      )
    );
  }
}
