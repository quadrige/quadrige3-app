import {
  AppValidatorService,
  EntityUtils,
  IEntitiesTableDataSource,
  isEmptyArray,
  isNotEmptyArray,
  isNotNil,
  SharedValidators,
} from '@sumaris-net/ngx-components';
import { AbstractControlOptions, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { BaseFilterUtils } from '@app/shared/model/filter.model';
import { hasDuplicates } from '@app/shared/utils';

export abstract class BaseValidatorService<E, O = any> extends AppValidatorService<E> {
  dataSource: IEntitiesTableDataSource<any>;

  getRowValidator(data?: E, opts?: O): UntypedFormGroup {
    return this.getFormGroup(data, opts);
  }

  getFormGroup(data?: E, opts?: O): UntypedFormGroup {
    return this.formBuilder.group(this.getFormGroupConfig(data, opts), this.getFormGroupOptions(data, opts));
  }

  getFormGroupConfig(data?: E, opts?: O): { [p: string]: any } {
    return {};
  }

  getFormGroupOptions(data?: E, opts?: O): AbstractControlOptions {
    return {};
  }
}

export class BaseValidators {
  static criteriaRequired(control: UntypedFormControl): ValidationErrors | null {
    const value = control.value;
    if (BaseFilterUtils.isCriteriaEmpty(value, true)) {
      return { required: true };
    }
    return null;
  }
}

export class BaseGroupValidators {
  static requiredAllArraysNotEmpty(fieldNames: string[], errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const controls = fieldNames.map((fieldName) => group.get(fieldName));
      if (controls.some((control) => control === undefined)) {
        throw new Error(`Unable to find some fields '${fieldNames}' to check!`);
      }
      if (controls.some((control) => isEmptyArray(control.value))) {
        // If one of the value is empty
        return errorMessage ? { requiredAllNotEmptyMsg: errorMessage } : { required: true };
      }
      return null;
    };
  }

  static requiredOneArrayNotEmpty(fieldNames: string[], errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const controls = fieldNames.map((fieldName) => group.get(fieldName));
      if (controls.some((control) => control === undefined)) {
        throw new Error(`Unable to find some fields '${fieldNames}' to check!`);
      }
      if (controls.some((control) => isNotEmptyArray(control.value))) {
        // If one of the value is not empty
        return null;
      }
      return errorMessage ? { requiredOneNotEmptyMsg: errorMessage } : { required: true };
    };
  }

  static requiredIfOneArrayNotEmpty(fieldName: string, expectedValue: any, fieldNames: string[], errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const conditionControl = group.get(fieldName);
      if (!conditionControl) {
        throw new Error(`Unable to find field '${fieldName}' to check!`);
      }
      const controls = fieldNames.map((name) => group.get(name));
      if (controls.some((control) => control === undefined)) {
        throw new Error(`Unable to find some fields '${fieldNames}' to check!`);
      }
      if (conditionControl.value !== expectedValue || controls.some((control) => isNotEmptyArray(control.value))) {
        // If condition is not valid or one of the value is not empty
        return null;
      }
      return errorMessage ? { requiredOneNotEmptyMsg: errorMessage } : { required: true };
    };
  }

  static requiredAnyOf(fieldNames: string[], expectedValue?: any, errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const controls = fieldNames.map((fieldName) => group.get(fieldName));
      if (controls.some((control) => control === undefined)) {
        throw new Error(`Unable to find some fields '${fieldNames}' to check!`);
      }
      if (controls.some((control) => (expectedValue ? control.value === expectedValue : isNotNil(control.value)))) {
        // At least one of the controls has exact expected value, or is just defined
        return null;
      }
      return errorMessage ? { requiredAnyOfMsg: errorMessage } : { required: true };
    };
  }

  static requiredOneOf(fieldNames: string[], expectedValue?: any, errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const controls = fieldNames.map((fieldName) => group.get(fieldName));
      if (controls.some((control) => control === undefined)) {
        throw new Error(`Unable to find some fields '${fieldNames}' to check!`);
      }
      if (controls.filter((control) => (expectedValue ? control.value === expectedValue : isNotNil(control.value))).length === 1) {
        // Only one of the controls has exact expected value, or is just defined
        return null;
      }
      return errorMessage ? { requiredOneOfMsg: errorMessage } : { required: true };
    };
  }

  static requiredIfArrayNotEmpty(fieldName: string, expectedValue: any, arrayFieldName: string, errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const control = group.get(fieldName);
      const arrayControl = group.get(arrayFieldName);
      if (!control) {
        throw new Error(`Unable to find field '${fieldName}' to check!`);
      }
      if (!arrayControl) {
        throw new Error(`Unable to find field '${arrayFieldName}' to check!`);
      }
      if (control.value !== expectedValue && isNotEmptyArray(arrayControl.value)) {
        return errorMessage ? { requiredIfArrayNotEmptyMsg: errorMessage } : { required: true };
      }
      return null;
    };
  }

  static requiredIfArrayEmpty(fieldName: string, expectedValue: any, arrayFieldName: string, errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const control = group.get(fieldName);
      const arrayControl = group.get(arrayFieldName);
      if (!control) {
        throw new Error(`Unable to find field '${fieldName}' to check!`);
      }
      if (!arrayControl) {
        throw new Error(`Unable to find field '${arrayFieldName}' to check!`);
      }
      if (control.value !== expectedValue && isEmptyArray(arrayControl.value)) {
        return errorMessage ? { requiredIfArrayEmptyMsg: errorMessage } : { required: true };
      }
      return null;
    };
  }

  static invalidIfSameEntity(fieldName: string, idField: string = 'id'): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const control = group.get(fieldName);
      const idControl = group.get(idField);
      if (!control) {
        throw new Error(`Unable to find field '${fieldName}' to check!`);
      }
      if (!idControl) {
        throw new Error(`Unable to find field '${idField}' to check!`);
      }
      // eslint-disable-next-line eqeqeq
      if (EntityUtils.isNotEmpty(control.value, 'id') && isNotNil(idControl.value) && control.value.id == idControl.value) {
        const error = { invalid: true };
        control.setErrors(error);
        control.markAsTouched({ onlySelf: true });
        return error;
      }
      SharedValidators.clearError(control, 'invalid');
      return null;
    };
  }

  static invalidIfDuplicates(arrayFieldName: string, compareFn: (value1, value2) => boolean, errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const control = group.get(arrayFieldName);
      if (!control) {
        throw new Error(`Unable to find field '${arrayFieldName}' to check!`);
      }
      const value: any[] = Array.isArray(control.value) ? control.value : [];
      if (value.length) {
        if (hasDuplicates(value, compareFn)) {
          return errorMessage ? { invalidIfDuplicatesMsg: errorMessage } : { invalid: true };
        }
      }
      return null;
    };
  }

  static minMax(minFieldName: string, maxFieldName: string, errorMessage?: string): ValidatorFn {
    return (group: UntypedFormGroup): ValidationErrors | null => {
      const minControl = group.get(minFieldName);
      const maxControl = group.get(maxFieldName);
      if (!minControl) {
        throw new Error(`Unable to find field '${minFieldName}' to check!`);
      }
      if (!maxControl) {
        throw new Error(`Unable to find field '${maxFieldName}' to check!`);
      }
      if (!!minControl.value && !!maxControl.value) {
        if (minControl.value > maxControl.value) {
          const error = { minMax: errorMessage || true };
          minControl.markAsTouched({ onlySelf: true });
          minControl.markAsPending();
          minControl.setErrors({
            ...minControl.errors,
            ...error,
          });
          maxControl.markAsTouched({ onlySelf: true });
          maxControl.markAsPending();
          maxControl.setErrors({
            ...minControl.errors,
            ...error,
          });
          return error;
        }
      }
      SharedValidators.clearError(minControl, 'minMax');
      SharedValidators.clearError(maxControl, 'minMax');
      return null;
    };
  }
}
