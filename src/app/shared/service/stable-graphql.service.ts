import { ErrorPolicy, FetchPolicy } from '@apollo/client/core';
import {
  APP_GRAPHQL_FRAGMENTS,
  APP_GRAPHQL_TYPE_POLICIES,
  ENVIRONMENT,
  GraphqlService,
  isNil,
  MutateQueryOptions,
  NetworkService,
  ServiceError,
  StorageService,
} from '@sumaris-net/ngx-components';
import { HttpLink } from 'apollo-angular/http';
import { Apollo, ExtraSubscriptionOptions } from 'apollo-angular';
import { Platform } from '@ionic/angular';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { PermissionService } from '@app/core/service/permission.service';
import { EmptyObject } from 'apollo-angular/types';

@Injectable({ providedIn: 'root' })
export class StableGraphqlService extends GraphqlService {
  private _permissionService: PermissionService;

  constructor(protected injector: Injector) {
    super(
      injector.get(Platform),
      injector.get(Apollo),
      injector.get(HttpLink),
      injector.get(NetworkService),
      injector.get(StorageService),
      injector.get(ENVIRONMENT),
      injector.get(APP_GRAPHQL_TYPE_POLICIES),
      injector.get(APP_GRAPHQL_FRAGMENTS, [])
    );
  }

  async query<T, V = EmptyObject>(opts: { query: any; variables?: V; error?: ServiceError; fetchPolicy?: FetchPolicy }): Promise<T> {
    try {
      return await super.query(opts);
    } catch (error) {
      await this.handleError(error);
    }
  }

  async mutate<T, V = EmptyObject>(opts: MutateQueryOptions<T, V>): Promise<T> {
    try {
      return await super.mutate(opts);
    } catch (error) {
      await this.handleError(error);
    }
  }

  subscribe<T, V = EmptyObject>(
    opts: { query: any; variables: V; fetchPolicy?: FetchPolicy; errorPolicy?: ErrorPolicy; error?: ServiceError },
    extra?: ExtraSubscriptionOptions
  ): Observable<T> {
    try {
      return super.subscribe(opts, extra);
    } catch (error) {
      this.handleError(error);
    }
  }

  protected async handleError(error: any) {
    // if 401 : unauthorized or authentication timed out -> try to log in
    if (error?.code === 401) {
      await this.permissionService.accountExpired();
    } else {
      // Throws otherwise
      throw error;
    }
  }

  get permissionService(): PermissionService {
    if (isNil(this._permissionService)) {
      this._permissionService = this.injector.get(PermissionService);
    }
    return this._permissionService;
  }
}
