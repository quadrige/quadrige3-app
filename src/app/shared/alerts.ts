import { Alerts as BaseAlerts } from '@sumaris-net/ngx-components';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { DetailedError } from '@app/shared/errors';

export class Alerts {
  /* @deprecated prefer ComponentGuardService */
  // static askSaveBeforeLeave = BaseAlerts.askSaveBeforeLeave;
  static showError = BaseAlerts.showError;
  static askDeleteConfirmation = BaseAlerts.askDeleteConfirmation;
  static askActionConfirmation = BaseAlerts.askActionConfirmation;
  static askConfirmation = BaseAlerts.askConfirmation;
  static askSaveBeforeAction = BaseAlerts.askSaveBeforeAction;

  /**
   * Ask the user to confirm a cancellation. If return undefined: user has cancelled
   *
   * @pram messageKey i18n message key
   * @param messageKey
   * @param alertCtrl
   * @param translate
   * @param event
   * @param interpolateParams
   */
  static async askCancelConfirmation(
    messageKey: string,
    alertCtrl: AlertController,
    translate: TranslateService,
    event?: UIEvent,
    interpolateParams?: any
  ): Promise<boolean | undefined> {
    if (!alertCtrl || !translate) throw new Error(`Missing required argument 'alertCtrl' or 'translate'`);
    let confirm = false;
    let cancel = false;
    const translations = translate.instant(['COMMON.BTN_YES_CONTINUE', 'COMMON.BTN_NO', messageKey, 'CONFIRM.ALERT_HEADER'], interpolateParams);
    const alert = await alertCtrl.create({
      header: translations['CONFIRM.ALERT_HEADER'],
      message: translations[messageKey],
      buttons: [
        {
          text: translations['COMMON.BTN_NO'],
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            cancel = true;
          },
        },
        {
          text: translations['COMMON.BTN_YES_CONTINUE'],
          handler: () => {
            confirm = true; // update upper value
          },
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();

    if (confirm) return true; // = Do save and leave

    if (cancel) {
      // Stop the event
      if (event) event.preventDefault();

      return undefined; // User cancelled
    }

    return false; // Leave without saving
  }

  /**
   * Show a message (an error by default) with optional detail (html)
   */
  static async showDetailedMessage(
    messageKey: string,
    alertCtrl: AlertController,
    translate: TranslateService,
    opts?: {
      titleKey?: string;
      detailedMessage?: string;
    },
    interpolateParams?: any
  ) {
    if (!messageKey || !alertCtrl || !translate) throw new Error(`Missing a required argument ('messageKey', 'alertCtrl' or 'translate')`);
    const titleKey = (opts && opts.titleKey) || 'ERROR.ALERT_HEADER';
    const translations = translate.instant(['COMMON.BTN_OK', messageKey, titleKey], interpolateParams);
    const alert = await alertCtrl.create({
      header: translations[titleKey],
      message: translations[messageKey] + (opts?.detailedMessage ? `<br/><br/>${opts.detailedMessage}` : ''),
      buttons: [
        {
          text: translations['COMMON.BTN_OK'],
          role: 'cancel',
        },
      ],
      backdropDismiss: false,
      keyboardClose: false,
    });
    await alert.present();
    await alert.onDidDismiss();
  }

  static async showMessage(messageKey: string, alertCtrl: AlertController, translate: TranslateService) {
    await this.showDetailedMessage(messageKey, alertCtrl, translate, { titleKey: 'INFO.ALERT_HEADER' });
  }

  static async showExpiredSessionMessage(alertCtrl: AlertController, translate: TranslateService) {
    await this.showMessage('ERROR.AUTH_TIMEOUT', alertCtrl, translate);
  }

  /**
   * Ask the user to confirm (with detailed message). If return undefined: user has cancelled
   *
   * @pram messageKey i18n message key
   * @param messageKey
   * @param alertCtrl
   * @param translate
   * @param event
   * @param opts
   */
  static async askDetailedConfirmation(
    messageKey: string,
    alertCtrl: AlertController,
    translate: TranslateService,
    event?: UIEvent,
    opts?: {
      titleKey?: string;
      detailedMessage?: string;
      interpolateParams?: any;
    }
  ): Promise<boolean | undefined> {
    if (!alertCtrl || !translate) throw new Error("Missing required argument 'alertCtrl' or 'translate'");
    let confirm = false;
    let cancel = false;
    const titleKey = opts?.titleKey || 'CONFIRM.ALERT_HEADER';
    const translations = translate.instant(['COMMON.BTN_YES_CONTINUE', 'COMMON.BTN_CANCEL', messageKey, titleKey], opts?.interpolateParams);
    const alert = await alertCtrl.create({
      header: translations[titleKey],
      message: translations[messageKey] + (opts?.detailedMessage ? `<br/><br/>${opts.detailedMessage}` : ''),
      buttons: [
        {
          text: translations['COMMON.BTN_CANCEL'],
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            cancel = true;
          },
        },
        {
          text: translations['COMMON.BTN_YES_CONTINUE'],
          handler: () => {
            confirm = true; // update upper value
          },
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();

    if (confirm) return true; // = Do save and leave

    if (cancel) {
      // Stop the event
      if (event) event.preventDefault();

      return undefined; // User cancelled
    }

    return false; // Leave without saving
  }

  /**
   * Ask the user to confirm (with detailed message). If return undefined: user has cancelled
   *
   * @pram messageKey i18n message key
   * @param error
   * @param alertCtrl
   * @param translate
   * @param event
   * @param opts
   */
  static async showDetailedError(
    error: DetailedError,
    alertCtrl: AlertController,
    translate: TranslateService,
    event?: UIEvent,
    opts?: {
      titleKey?: string;
      interpolateParams?: any;
    }
  ) {
    if (!alertCtrl || !translate) throw new Error("Missing required argument 'alertCtrl' or 'translate'");
    const titleKey = opts?.titleKey || 'ERROR.ALERT_HEADER';
    const translations = translate.instant(['COMMON.BTN_OK', error.message, titleKey, 'ERROR.ALERT_DETAILS'], opts?.interpolateParams);
    const alert = await alertCtrl.create({
      header: translations[titleKey],
      message:
        translations[error.message] + (error.detail ? `<br/><br/>${translations['ERROR.ALERT_DETAILS']}<br/><b>${error.detail.message}</b>` : ''),
      buttons: [
        {
          text: translations['COMMON.BTN_OK'],
          role: 'cancel',
        },
      ],
      cssClass: 'alert-larger',
    });
    await alert.present();
    await alert.onDidDismiss();
  }

  /**
   * Ask the user to confirm (with detailed message).
   *
   * @pram messageKey i18n message key
   * @param messageKey
   * @param alertCtrl
   * @param translate
   * @param event
   * @param opts
   */
  static async askYesNoConfirmation(
    messageKey: string,
    alertCtrl: AlertController,
    translate: TranslateService,
    event?: UIEvent,
    opts?: {
      titleKey?: string;
      interpolateParams?: any;
    }
  ): Promise<boolean | undefined> {
    if (!alertCtrl || !translate) throw new Error("Missing required argument 'alertCtrl' or 'translate'");
    let answer = false;
    const titleKey = opts?.titleKey || 'CONFIRM.ALERT_HEADER';
    const translations = translate.instant(['COMMON.BTN_YES', 'COMMON.BTN_NO', messageKey, titleKey], opts?.interpolateParams);
    const alert = await alertCtrl.create({
      header: translations[titleKey],
      message: translations[messageKey],
      buttons: [
        {
          text: translations['COMMON.BTN_NO'],
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            answer = false;
          },
        },
        {
          text: translations['COMMON.BTN_YES'],
          handler: () => {
            answer = true; // update upper value
          },
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();

    return answer;
  }

  /**
   * Ask the user to confirm (with detailed message). If return undefined: user has cancelled
   *
   * @pram messageKey i18n message key
   * @param messageKey
   * @param alertCtrl
   * @param translate
   * @param event
   * @param opts
   */
  static async askYesNoCancelConfirmation(
    messageKey: string,
    alertCtrl: AlertController,
    translate: TranslateService,
    event?: UIEvent,
    opts?: {
      titleKey?: string;
      interpolateParams?: any;
    }
  ): Promise<boolean | undefined> {
    if (!alertCtrl || !translate) throw new Error("Missing required argument 'alertCtrl' or 'translate'");
    let answer: boolean | undefined = false;
    const titleKey = opts?.titleKey || 'CONFIRM.ALERT_HEADER';
    const translations = translate.instant(['COMMON.BTN_YES', 'COMMON.BTN_NO', 'COMMON.BTN_CANCEL', messageKey, titleKey], opts?.interpolateParams);
    const alert = await alertCtrl.create({
      header: translations[titleKey],
      message: translations[messageKey],
      buttons: [
        {
          text: translations['COMMON.BTN_CANCEL'],
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            answer = undefined;
          },
        },
        {
          text: translations['COMMON.BTN_NO'],
          cssClass: 'secondary',
          handler: () => {
            answer = false;
          },
        },
        {
          text: translations['COMMON.BTN_YES'],
          handler: () => {
            answer = true; // update upper value
          },
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();

    return answer;
  }
}
