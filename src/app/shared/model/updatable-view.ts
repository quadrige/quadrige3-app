import { CompletableEvent } from '@sumaris-net/ngx-components';

export interface UpdatableView {
  updatePermission(): void;
  doRefresh(event?: CompletableEvent): void;
  deactivate(): void;
}
