import { FormGroupConfig } from '@app/shared/form/form.utils';
import { arraySize, EntityUtils, filterNotNil, isNotEmptyArray, ObjectMap } from '@sumaris-net/ngx-components';
import { AbstractControl, UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

export const multipleAttributeSuffix = 'Multiple';

export type IComposite = any;

export class CompositeMap<C extends IComposite = IComposite> implements Record<string, C> {
  [key: string]: C;
}

export class CompositeUtils {
  static readonly logPrefix = '[composite]';

  /**
   * Build IComposite model from array
   */
  static build<E>(candidates: E[], candidateToComposite: (candidate: E) => IComposite, propertyKeysAsMap?: string[]): IComposite {
    if (arraySize(candidates) > 1) {
      // Build composite from first candidate
      const composite = candidateToComposite(candidates[0]);
      // Iterate other candidates
      for (let i = 1; i < candidates.length; i++) {
        const candidateComposite = candidateToComposite(candidates[i]);
        CompositeUtils.compose(composite, candidateComposite, propertyKeysAsMap);
      }
      return composite;
    }
    return undefined;
  }

  private static compose(composite: IComposite, candidateComposite: IComposite, propertyKeysAsMap?: string[]) {
    // Compare each property
    Object.keys(composite)
      .filter((key) => !key.endsWith(multipleAttributeSuffix))
      .forEach((key) => {
        // Test if composite map (additional properties)
        if (isNotEmptyArray(propertyKeysAsMap) && propertyKeysAsMap.includes(key)) {
          // Get properties and check multiplicity
          const compositeMap: CompositeMap = composite[key];
          const candidateCompositeMap: CompositeMap = candidateComposite[key];
          // Compose with additional map properties
          Object.keys(compositeMap).forEach((mapKey) => CompositeUtils.compose(compositeMap[mapKey], candidateCompositeMap[mapKey]));
        } else if (composite[key + multipleAttributeSuffix] !== true) {
          // If not already multiple, test equality
          if (!CompositeUtils.valueEquals(composite[key], candidateComposite[key])) {
            // If not, reset and set as multiple
            composite[key] = undefined;
            composite[key + multipleAttributeSuffix] = true;
          }
        }
      });
  }

  /**
   * Test equality of 2 attributes, with == or entity.id
   *
   * @param x
   * @param y
   */
  static valueEquals(x: any, y: any): boolean {
    // eslint-disable-next-line eqeqeq
    return x == y || (EntityUtils.isNotEmpty(x, 'id') && EntityUtils.isNotEmpty(y, 'id') && EntityUtils.equals(x, y, 'id'));
  }

  /**
   * Return object with non-multiple properties only
   */
  static value(composite: IComposite, propertyKeysAsMap?: string[]): any {
    const result = {};
    // Decompose
    Object.keys(composite).forEach((key) => {
      if (key.endsWith(multipleAttributeSuffix) && composite[key] === false) {
        // Add non-multiple property
        const propertyKey = key.substring(0, key.length - multipleAttributeSuffix.length);
        result[propertyKey] = composite[propertyKey];
      }
      // Test if composite map (additional properties)
      else if (isNotEmptyArray(propertyKeysAsMap) && propertyKeysAsMap.includes(key)) {
        const compositeMap: CompositeMap = composite[key];
        const resultMap = {};
        Object.keys(compositeMap).forEach((mapKey) => {
          resultMap[mapKey] = CompositeUtils.value(compositeMap[mapKey]);
        });
        result[key] = resultMap;
      }
    });
    return result;
  }

  /**
   * Build FormGroupConfig for IComposite, based on baseFormGroupConfig
   */
  static getFormGroupConfig(compositeType: any, baseFormGroupConfig: FormGroupConfig<any>): FormGroupConfig<any> {
    const formConfig = {};
    Object.keys(compositeType).forEach((key) => {
      // Get config from base config
      const config = baseFormGroupConfig[key];
      if (config) {
        formConfig[key] = config;
      }
      // Add 'multiple' control
      if (key.endsWith(multipleAttributeSuffix)) {
        formConfig[key] = [null];
      }
    });
    return formConfig;
  }

  /**
   * Listen each control and update composite
   */
  static listenCompositeFormChanges(
    controls: ObjectMap<AbstractControl>,
    registerSubscriptionFn: (subscription: Subscription) => void,
    propertyKeysAsMap?: string[]
  ) {
    Object.keys(controls).forEach((key) => {
      if (key.endsWith(multipleAttributeSuffix)) {
        // get corresponding control
        const propertyKey = key.substring(0, key.length - multipleAttributeSuffix.length);
        const control: AbstractControl = controls[propertyKey];
        if (control) {
          console.debug(`${this.logPrefix} control '${propertyKey}' initial value:`, control.value);
          // Listen value change
          registerSubscriptionFn(
            control.valueChanges.pipe(distinctUntilChanged(), filterNotNil).subscribe((value) => {
              console.debug(`${this.logPrefix} control '${propertyKey}' value has changed:`, value);
              // reset multiple attribute
              controls[key].patchValue(false);
            })
          );
        } else {
          console.warn(`${this.logPrefix} control with key '${propertyKey} not found`);
        }
      } else if (isNotEmptyArray(propertyKeysAsMap) && propertyKeysAsMap.includes(key)) {
        // get corresponding form group
        const control = controls[key];
        if (control instanceof UntypedFormGroup) {
          Object.keys(control.controls).forEach((innerKey) => {
            const innerControl = control.controls[innerKey];
            if (innerControl instanceof UntypedFormGroup) {
              CompositeUtils.listenCompositeFormChanges(innerControl.controls, registerSubscriptionFn);
            } else {
              console.warn(`${this.logPrefix} control ${key}.${innerKey} is not a form group`);
            }
          });
        } else {
          console.warn(`${this.logPrefix} control ${key} is not a form group`);
        }
      }
    });
  }
}
