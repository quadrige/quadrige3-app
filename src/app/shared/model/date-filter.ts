import { Moment } from 'moment';
import { Beans, EntityAsObjectOptions, EntityClass, EntityFilter, fromDateISOString, SharedFormGroupValidators } from '@sumaris-net/ngx-components';
import { Dates } from '@app/shared/dates';
import { FormGroupConfig } from '@app/shared/form/form.utils';
import { AbstractControlOptions, UntypedFormBuilder } from '@angular/forms';

export interface IDateFilter {
  startLowerBound?: Moment;
  startUpperBound?: Moment;
  endLowerBound?: Moment;
  endUpperBound?: Moment;
}

@EntityClass({ typename: 'DateFilterVO' })
export class DateFilter extends EntityFilter<any, any> implements IDateFilter {
  static fromObject: (source: any, opts?: any) => DateFilter;

  startLowerBound?: Moment;
  startUpperBound?: Moment;
  endLowerBound?: Moment;
  endUpperBound?: Moment;

  constructor() {
    super(DateFilter.TYPENAME);
  }

  fromObject(source: any) {
    this.startLowerBound = fromDateISOString(source.startLowerBound);
    this.startUpperBound = fromDateISOString(source.startUpperBound);
    this.endLowerBound = fromDateISOString(source.endLowerBound);
    this.endUpperBound = fromDateISOString(source.endUpperBound);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    return {
      startLowerBound: Dates.toLocalDateString(this.startLowerBound),
      startUpperBound: Dates.toLocalDateString(this.startUpperBound),
      endLowerBound: Dates.toLocalDateString(this.endLowerBound),
      endUpperBound: Dates.toLocalDateString(this.endUpperBound),
    };
  }

  isEmpty(): boolean {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    return Beans.isEmpty<DateFilter>({ ...this, __typename: undefined });
  }
}

export class DateFilterUtils {
  static createFormGroup(formBuilder: UntypedFormBuilder) {
    const config: FormGroupConfig<IDateFilter> = {
      startLowerBound: [null],
      startUpperBound: [null],
      endLowerBound: [null],
      endUpperBound: [null],
    };

    const options: AbstractControlOptions = {
      validators: [
        SharedFormGroupValidators.dateRange('startLowerBound', 'startUpperBound'),
        SharedFormGroupValidators.dateRange('endLowerBound', 'endUpperBound'),
      ],
    };

    return formBuilder.group(config, options);
  }
}
