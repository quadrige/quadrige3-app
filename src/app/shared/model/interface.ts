export interface ISimpleType<T> {
  id: T;
  i18nLabel: string;
}

export type PartialRecord<K extends keyof any, T> = {
  [P in K]?: T;
};
