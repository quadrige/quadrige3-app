import {
  Entity,
  EntityAsObjectOptions,
  EntityFilter,
  FilterFn,
  IEntity,
  isEmptyArray,
  isNil,
  isNilOrBlank,
  isNotEmptyArray,
  isNotNil,
} from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';

export interface IBaseEntityFilterCriteria<ID> {
  searchAttributes?: string[];
  searchText?: string;
  exactText?: string;
  includedIds?: ID[];
  excludedIds?: ID[];
  forceIncludedIds?: boolean;

  isEmpty?: (isSubFilter?: boolean) => boolean;
}

export abstract class BaseEntityFilterCriteria<E extends IEntity<E, ID>, ID, AO extends BaseFilterAsObjectOptions = BaseFilterAsObjectOptions>
  extends EntityFilter<BaseEntityFilterCriteria<E, ID>, E, ID, AO>
  implements IBaseEntityFilterCriteria<ID>
{
  searchAttributes?: string[] = null;
  searchText?: string = null;
  exactText?: string = null;
  includedIds?: ID[] = null;
  excludedIds?: ID[] = null;
  forceIncludedIds?: boolean = null;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.searchAttributes = source.searchAttributes;
    this.searchText = source.searchText;
    this.exactText = source.exactText;
    this.includedIds = source.includedIds;
    this.excludedIds = source.excludedIds;
    this.forceIncludedIds = source.forceIncludedIds;
  }

  asObject(opts?: AO): any {
    const target = super.asObject(opts);
    if (opts?.minify) {
      delete target.forceIncludedIds;
    }
    return target;
  }

  /* Overridden from ngx-components to add parameter */
  isEmpty(isSubFilter?: boolean): boolean {
    return this.countNotEmptyCriteria(isSubFilter) === 0;
  }

  countNotEmptyCriteria(isSubFilter?: boolean): number {
    let count = 0;
    Object.keys(this.asObject()).forEach((key) => {
      if (this[key] instanceof BaseEntityFilterCriteria) {
        count += (this[key] as BaseEntityFilterCriteria<any, any>).countNotEmptyCriteria(true);
      } else if (this.isCriteriaNotEmpty(key, this[key], isSubFilter)) {
        count++;
      }
    });
    return count;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    if (['searchAttributes', 'systemId'].includes(key)) return false;
    // Consider includedIds and excludedIds as empty if not a sub filter
    if (!isSubFilter && ['includedIds', 'excludedIds', 'forceIncludedIds'].includes(key)) return false;
    return super.isCriteriaNotEmpty(key, value);
  }

  protected buildFilter(): FilterFn<E>[] {
    const filterFns = [];

    // Filter included/excluded ids
    if (isNotEmptyArray(this.includedIds)) {
      filterFns.push((entity: E) => isNotNil(entity.id) && this.includedIds.map((id) => id.toString()).includes(entity.id.toString()));
    }
    if (isNotEmptyArray(this.excludedIds)) {
      filterFns.push((entity: E) => isNil(entity.id) || !this.excludedIds.map((id) => id.toString()).includes(entity.id.toString()));
    }

    const searchTextFilter = EntityUtils.searchTextFilter(this.searchAttributes, this.searchText);
    if (searchTextFilter) filterFns.push(searchTextFilter);

    return filterFns;
  }
}

export interface IBaseEntityFilter<ID, C extends IBaseEntityFilterCriteria<ID>> {
  // Name of the filter
  name?: string;
  // Transcribing system used for all criterias
  systemId?: TranscribingSystemType;
  // List of filter criteria
  criterias: C[];
}

export interface BaseFilterAsObjectOptions extends EntityAsObjectOptions {
  keepMinifiedSystemId?: boolean;
}

export abstract class BaseEntityFilter<
    F extends EntityFilter<F, E, ID, AO, FO>,
    C extends BaseEntityFilterCriteria<E, ID>,
    E extends IEntity<E, ID>,
    ID = number,
    AO extends BaseFilterAsObjectOptions = BaseFilterAsObjectOptions,
    FO = any,
  >
  extends EntityFilter<F, E, ID, AO, FO>
  implements IBaseEntityFilter<ID, C>
{
  name: string = null;
  systemId: TranscribingSystemType = null;
  criterias: C[] = [];

  fromObject(source: any, opts?: FO) {
    super.fromObject(source, opts);
    this.name = source.name;
    this.systemId = source.systemId ?? 'QUADRIGE';
    // Parse criterias: return at least one empty criteria
    this.criterias = (source.criterias || [undefined]).map((criteria: any) => this.criteriaFromObject(criteria || {}, opts));
  }

  abstract criteriaFromObject(source: any, opts?: FO): C;

  asObject(opts?: AO): any {
    const target = super.asObject(opts);
    target.criterias = this.criterias?.map((value) => value.asObject(opts));
    if (opts?.minify) {
      delete target.name; // filter name not sent to server
      delete target.updateDate; // in case of an entity is used directly as filter (ex: OrderItem with parent as OrderItemType)
    }
    return target;
  }

  isEmpty(): boolean {
    return isNilOrBlank(this.name) && super.isEmpty();
  }

  countNotEmptyCriteria(): number {
    return this.criterias.reduce((sum, criteria) => {
      return sum + criteria.countNotEmptyCriteria();
    }, 0);
  }

  asFilterFn(): FilterFn<E> {
    const criteriaFilterFns = this.criterias.map((criteria) => criteria.asFilterFn()).filter(isNotNil);
    if (isEmptyArray(criteriaFilterFns)) return undefined;
    // Use 'some' method to simulate an 'or' predicate
    return (entity: E) => criteriaFilterFns.some((filterFn) => filterFn(entity));
  }

  patch(newCriteria: Partial<C> | Partial<IBaseEntityFilterCriteria<ID>>) {
    this.criterias = this.criterias.map((criteria) => this.criteriaFromObject({ ...criteria, ...newCriteria }));
  }
}

export class BaseFilterUtils {
  static isFilterEmpty(filter: IBaseEntityFilter<any, any>): boolean {
    return !filter || isEmptyArray(filter.criterias) || filter.criterias.every((criteria) => BaseFilterUtils.isCriteriaEmpty(criteria));
  }

  static isCriteriaEmpty(criteria: IBaseEntityFilterCriteria<any>, isSubFilter?: boolean): boolean {
    if (!criteria) return true;
    if (typeof criteria['isEmpty'] === 'function') {
      return criteria.isEmpty(isSubFilter);
    }
    return isNilOrBlank(criteria.searchText) && (isSubFilter ? isEmptyArray(criteria.includedIds) && isEmptyArray(criteria.excludedIds) : true);
  }

  static clean(filter: any): any {
    if (!filter || (isNilOrBlank(filter.name) && isEmptyArray(filter.criterias))) return undefined;
    return { ...filter, criterias: filter.criterias?.map(BaseFilterUtils.cleanCriteria) };
  }

  static cleanCriteria(criteria: any, subFilter?: boolean): any {
    if (!criteria) return undefined;
    const cleaned = { ...criteria };

    // Delete null or undefined properties
    Object.keys(cleaned).forEach((key) => {
      const attribute = cleaned[key];
      if (isNilOrBlank(attribute) || (Array.isArray(attribute) && isEmptyArray(attribute))) {
        delete cleaned[key];
      } else if (typeof attribute === 'object' && !Array.isArray(attribute)) {
        if (attribute instanceof Entity && !(attribute instanceof EntityFilter)) {
          // For entity used as filter, store minimal object
          cleaned[key] = { id: attribute.id, label: attribute['label'], name: attribute['name'] };
        } else {
          const cleanedAttribute = BaseFilterUtils.cleanCriteria(attribute, true);
          if (!Object.keys(cleanedAttribute).length) {
            delete cleaned[key];
          } else {
            cleaned[key] = cleanedAttribute;
          }
        }
      }
    });

    // Keep these attributes on sub filters
    if (!subFilter) {
      delete cleaned.includedIds;
      delete cleaned.excludedIds;
      delete cleaned.forceIncludedIds;
      // Delete some specific attributes (ex: ProgramFilter)
      delete cleaned.managedOnly;
      delete cleaned.writableOnly;
    }

    // Remove possible presence of default systemId on an empty criteria
    if (TranscribingItemTypeUtils.isDefaultSystem(cleaned.systemId) || BaseFilterUtils.isCriteriaEmpty(cleaned, subFilter)) {
      delete cleaned.systemId;
    }

    delete cleaned.__typename;
    return cleaned;
  }
}

export class FilterHint {
  prefix?: string;
  fields?: string;
  contains?: string;
  searchText?: string;
  separator?: string;
  includeOrExclude?: string;
  list?: string;
  remaining?: string;

  toString(): string {
    return `${this.fields || ''} ${this.contains || ''} ${this.searchText || ''}${this.separator || ''}${this.includeOrExclude || ''} ${
      this.list || ''
    }`;
  }
}
