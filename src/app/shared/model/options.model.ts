import { Type } from '@angular/core';
import { BaseEntityFilterCriteria } from '@app/shared/model/filter.model';
import { TranscribingFunctionType, TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

export type SelectMode = 'include' | 'exclude';

export interface ISelectCriteria {
  parentId?: string;
  parentFilter?: any;
  includedIds?: any[];
  excludedIds?: any[];
  forceIncludedIds?: boolean;
  statusId?: number;
  searchText?: string;
  // Related to transcribing items
  referentialEntityName?: string;
  systemId?: TranscribingSystemType;
  functionIds?: TranscribingFunctionType[];
}

export interface IModalOptions {
  titleI18n?: string;
  canEdit?: boolean;
  defaultReferentialCriteria?: any;
}

export interface ISelectModalOptions<C extends ISelectCriteria = ISelectCriteria> extends IModalOptions {
  entityName: string;
  component?: Type<any>;
  title?: string;
  addTitle?: string;
  titlePrefixI18n?: string;
  titleAddPrefixI18n?: string;

  selectCriteria?: C;
  addCriteria?: C;
  addFirstCriteria?: C;
  fromSelectTable?: boolean;

  showMode?: boolean;
  mode?: SelectMode;

  cssClass?: string;
}

export interface ISelectResult {
  selectedIds?: any[];
  mode?: SelectMode;
}

export type ModalRole = 'cancel' | 'validate';

export interface IModalResult<R> {
  data?: R;
  role?: ModalRole | string;
}

export interface IAddResult {
  added?: any[];
  addedIds?: any[];
  mode?: SelectMode;
}

export interface ISetFilterOptions<C extends BaseEntityFilterCriteria<any, any> = BaseEntityFilterCriteria<any, any>> {
  emitEvent?: boolean;
  updateQueryParams?: boolean;
  permanentSelectionChangedPrevented?: boolean;
  firstTime?: boolean;
  defaultCriteria?: Partial<C & ISelectCriteria>;
  staticCriteria?: Partial<C & ISelectCriteria>;
  resetNamedFilter?: boolean;
  findByName?: boolean;
}
