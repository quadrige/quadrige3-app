import { Feature, FeatureCollection, Geometry, GeometryCollection } from 'geojson';
import { isNotEmptyArray, ObjectMap } from '@sumaris-net/ngx-components';

export type FeatureKey = number | string;
export type FeatureRecord = Record<FeatureKey, Feature>;

export class Geometries {
  static isFeatureCollectionNotEmpty(featureCollection: FeatureCollection): boolean {
    return isNotEmptyArray(featureCollection?.features);
  }

  static isAreaGeometry(geometry: Geometry): boolean {
    if (geometry.type === 'GeometryCollection') return geometry.geometries.every(Geometries.isAreaGeometry);
    return ['Polygon', 'MultiPolygon'].includes(geometry?.type);
  }

  static toFeature(id: FeatureKey, geometry: Geometry): Feature {
    if (!geometry) return undefined;
    if (!geometry.type) {
      console.error('[geometries] the geometry is not GeoJsonObject', geometry);
      throw 'Geometry error';
    }
    return {
      type: 'Feature',
      id,
      geometry,
      properties: {},
    };
  }

  static toCollection(features: Feature[]): FeatureCollection {
    return {
      type: 'FeatureCollection',
      features,
    };
  }

  static toFeatureCollection(geometry: Geometry | Geometry[]): FeatureCollection {
    if (!geometry) return undefined;
    if (Array.isArray(geometry)) {
      let id = 0;
      return {
        type: 'FeatureCollection',
        features: geometry.map((g) => Geometries.toFeature(++id, g)),
      };
    } else {
      if (geometry.type === 'GeometryCollection') {
        const geometryCollection = geometry as GeometryCollection;
        let id = 0;
        return {
          type: 'FeatureCollection',
          features: geometryCollection.geometries.map((g) => Geometries.toFeature(++id, g)),
        };
      } else {
        return {
          type: 'FeatureCollection',
          features: [Geometries.toFeature(1, geometry)],
        };
      }
    }
  }

  static toGeometryCollection(features: FeatureCollection): GeometryCollection {
    if (!features) return undefined;
    return {
      type: 'GeometryCollection',
      geometries: features.features.map((feature) => feature.geometry),
    };
  }

  static toFeatureRecord(value: Geometry | ObjectMap): FeatureRecord {
    if (!value) return undefined;
    if (!!value.type) {
      // value is a GeoJsonObject
      const collection = Geometries.toFeatureCollection(value as Geometry);
      return collection.features.reduce((record, feature) => {
        record[feature.id] = feature;
        return record;
      }, {} as FeatureRecord);
    }
    if (typeof value === 'object') {
      // a simple map object
      return Object.keys(value).reduce((record, key) => {
        record[key] = Geometries.toFeature(key, value[key]);
        return record;
      }, {} as FeatureRecord);
    }
  }
}
