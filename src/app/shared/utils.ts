import { AbstractControl } from '@angular/forms';
import {
  changeCaseToUnderscore,
  fromDateISOString,
  isEmptyArray,
  isNil,
  isNotNilOrBlank,
  ObjectMap,
  toBoolean,
  TranslateContextService,
} from '@sumaris-net/ngx-components';
import { ISimpleType } from '@app/shared/model/interface';
import { isMoment } from 'moment';

// Filter useful to get unique values only
export const uniqueValue = (value, index, array) => array.indexOf(value) === index;

export const hasDuplicates = (set, compareFn: (v1, v2) => boolean) => {
  const items = [];
  return set.reduce((duplicates, value) => {
    return duplicates || items.find((item) => compareFn(item, value)) || (items.push(value) && false);
  }, false);
};

export const asyncSome = async (arr, predicate) => {
  for (const e of arr) {
    if (await predicate(e)) return true;
  }
  return false;
};
export const asyncEvery = async (arr, predicate) => {
  for (const e of arr) {
    if (!(await predicate(e))) return false;
  }
  return true;
};

export type RecordableKeys<T> = {
  // for each key in T
  [K in keyof T]: T[K] extends string | number | symbol // is the value a valid object key?
    ? K // Yes, return the key itself
    : never; // No. Return `never`
}[keyof T]; // Get a union of the values that are not `never`.

export class Utils {
  // todo move to ngx-components
  static clearControlValue(event: UIEvent, formControl: AbstractControl, path?: string, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (event) event.stopPropagation(); // Avoid to enter input the field
    const targetControl = isNotNilOrBlank(path) ? formControl.get(path) : formControl;
    targetControl.reset(null, opts);
    if (opts?.emitEvent !== false) {
      targetControl.markAsDirty();
    }
  }

  static toFixed(x): string {
    if (!x) return x;
    if (Math.abs(x) < 1.0) {
      const e = parseInt(x.toString().split('e-')[1]);
      if (e) {
        x *= Math.pow(10, e - 1);
        x = '0.' + new Array(e).join('0') + x.toString().substring(2);
      }
    } else {
      let e = parseInt(x.toString().split('+')[1]);
      if (e > 20) {
        e -= 20;
        x /= Math.pow(10, e);
        x += new Array(e + 1).join('0');
      }
    }
    return x;
  }

  static arrayEquals(a1, a2): boolean {
    return Array.isArray(a1) && Array.isArray(a2) && a1.length === a2.length && a1.every((val, index) => val === a2[index]);
  }

  static arrayContainsAll(someArray: (string | number)[], inArray: (string | number)[]): boolean {
    return (someArray || []).every((someValue) => (inArray || []).map((inValue) => inValue.toString()).includes(someValue.toString()));
  }

  /* TODO: try to use equals from ngx-components */
  static equals(object1: any, object2: any): boolean {
    if ((isNil(object1) && isNil(object1)) || object1 === object2) return true;
    const json1 = JSON.stringify(object1);
    const json2 = JSON.stringify(object2);
    const equals = json1 === json2;
    const deepEquals = Utils.deepEquals(object1, object2);
    if (equals !== deepEquals) {
      console.error('Utils.equals result differs', object1, object2);
    }
    // can be simplified but need to debug
    return equals;
  }

  static deepEquals(object1: any, object2: any): boolean {
    // Handle strict equality and null/undefined cases
    if (object1 === object2) return true;
    if (!object1 || !object2) return false;

    // Handle different types
    if (typeof object1 !== typeof object2) {
      // Make sure comparing moments if one of objects is a Moment
      if (isMoment(object1) || isMoment(object2)) {
        object1 = fromDateISOString(object1);
        object2 = fromDateISOString(object2);
      } else {
        return false;
      }
    }

    // Handle Moment objects
    if (isMoment(object1) && isMoment(object2)) {
      return object1.isSame(object2);
    }

    // Handle Arrays
    if (Array.isArray(object1) && Array.isArray(object2)) {
      if (object1.length !== object2.length) return false;
      return object1.every((item, index) => Utils.deepEquals(item, object2[index]));
    }

    // Handle Objects
    if (typeof object1 === 'object') {
      const keys1 = Object.keys(object1);
      const keys2 = Object.keys(object2);
      if (keys1.length !== keys2.length) return false;
      return keys1.every((key) => Object.prototype.hasOwnProperty.call(object2, key) && Utils.deepEquals(object1[key], object2[key]));
    }

    // Handle primitives
    return object1 === object2;
  }

  static toObjectMap<O>(keys: string[], valueMapper: (key: string) => O): ObjectMap<O> {
    return keys.reduce((map, key) => ({ ...map, [key]: valueMapper(key) }), {});
  }

  static toRecord<T extends { [P in RecordableKeys<T>]: string | number | symbol }, K extends RecordableKeys<T>>(
    array: T[],
    selector: K
  ): Record<T[K], T> {
    return array.reduce((acc, item) => ((acc[item[selector]] = item), acc), {} as Record<T[K], T>);
  }

  static renameKeys(object: any, renameFn: (key: string) => string): any {
    const keyValues = Object.keys(object).map((key) => {
      const newKey = renameFn(key);
      return { [newKey]: object[key] };
    });
    return Object.assign({}, ...keyValues);
  }

  static contextualI18nKey(translateContext: TranslateContextService, key: string, context: string): string {
    const contextualKey = translateContext.contextualKey(key, context);
    return contextualKey !== translateContext.instant(contextualKey) ? contextualKey : key;
  }

  static naturalSort(array: string[]): string[] {
    const collator = new Intl.Collator(undefined, { numeric: true });
    return (array || []).sort((a, b) => collator.compare(a, b));
  }

  static textWidth(text: string, font: string): number {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    context.font = font;
    return context.measureText(text).width;
  }

  static fromBooleanString(value: string, defaultValue?: boolean): boolean {
    return value ? value.trim() !== '0' : defaultValue || false;
  }

  static fromOptionalBooleanString(value: string): boolean | undefined {
    return value ? value.trim() !== '0' : undefined;
  }

  static toBooleanString(value: boolean): string {
    return value === true ? '1' : '0';
  }

  static trimArray(array: any[], length: number) {
    if ((array?.length || 0) > (length || 0)) array.splice(length);
  }

  static splitArray<T>(array: T[], predicate: (item: T) => boolean): [T[], T[]] {
    return (array || []).reduce(
      ([accepted, rejected], elem) => {
        return predicate(elem) ? [[...accepted, elem], rejected] : [accepted, [...rejected, elem]];
      },
      [[], []]
    );
  }

  static buildSimpleTypeArray<K, T extends ISimpleType<K>>(keys: K[], i18nPrefix: string): T[] {
    return keys.map((key) => <T>{ id: key, i18nLabel: `${i18nPrefix}.${key}` });
  }

  /**
   * Return the shared start of all input strings
   *
   * @param array of strings
   */
  static sharedStart(array: string[]): string {
    if (isEmptyArray(array)) return undefined;
    if (array.length === 1) return array[0];
    const A = array.concat().sort();
    const a1 = A[0];
    const a2 = A[A.length - 1];
    const L = a1.length;
    let i = 0;
    while (i < L && a1.charAt(i) === a2.charAt(i)) i++;
    return a1.substring(0, i);
  }

  static toI18nKey(entityName: string, plural?: boolean): string {
    plural = toBoolean(plural, true);
    let key = changeCaseToUnderscore(entityName).toUpperCase();
    if (plural) {
      if (key.slice(-1) === 'H') {
        // ex: BATCH to BATCHES
        key = `${key}ES`;
      } else if (key.slice(-1) === 'Y') {
        // ex: STRATEGY to STRATEGIES
        key = `${key.slice(0, -1)}IES`;
      } else if (key.slice(-1) === 'X') {
        // ex: MATRIX to MATRICES
        key = `${key.slice(0, -1)}CES`;
      } else {
        // ex: PROGRAM to PROGRAMS
        key = `${key}S`;
      }
    }
    return `REFERENTIAL.ENTITY.${key}`; // todo: maybe make REFERENTIAL a parameter
  }

  static async promiseEveryTrue(promises: Promise<boolean>[]): Promise<boolean> {
    return Promise.all(promises).then((results) => results.every((result) => result === true));
  }
}
