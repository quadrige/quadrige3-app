import { Inject, Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ComponentRef } from '@ionic/core';
import { ENVIRONMENT, Environment, isNotNilOrBlank, sleep } from '@sumaris-net/ngx-components';
import { IModalOptions, IModalResult } from '@app/shared/model/options.model';

const stackedCssClass = 'stack-modal';

@Injectable({ providedIn: 'root' })
export class ModalService {
  private readonly debug: boolean;

  constructor(
    protected modalController: ModalController,
    @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    this.debug = !environment.production;
  }

  async openModal<O extends IModalOptions, R>(component: ComponentRef, options: O, cssClass?: string | string[]): Promise<IModalResult<R>> {
    if (await this.opened()) {
      if (Array.isArray(cssClass) && !cssClass.includes(stackedCssClass)) {
        cssClass.push(stackedCssClass);
      } else if (isNotNilOrBlank(cssClass) && stackedCssClass !== cssClass) {
        cssClass = [cssClass as string, stackedCssClass];
      } else {
        cssClass = stackedCssClass;
      }
    }
    const modal = await this.modalController.create({
      component,
      componentProps: options,
      backdropDismiss: false,
      cssClass,
    });
    await modal.present();
    const result: IModalResult<R> = await modal.onDidDismiss();
    if (this.debug) {
      console.debug('[modal-service] returned from modal', result);
    }
    return result;
  }

  async opened(): Promise<boolean> {
    return !!(await this.modalController.getTop());
  }

  async getModal(): Promise<HTMLIonModalElement> {
    await sleep(100); // Need a small sleep to let modal appear
    return this.modalController.getTop();
  }
}
