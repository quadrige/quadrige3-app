import { AfterViewInit, Directive, inject, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BaseForm } from '@app/shared/form/base.form';
import { UntypedFormGroup } from '@angular/forms';
import { IModalOptions } from '@app/shared/model/options.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Directive()
export abstract class ModalComponent<R> extends BaseForm<R> implements OnInit, AfterViewInit, OnDestroy, IModalOptions {
  @Input() titleI18n: string;

  protected validIfPristine = false;
  private readonly modalController = inject(ModalController);

  protected constructor(
    protected injector: Injector,
    protected formGroup?: UntypedFormGroup
  ) {
    super(injector, formGroup);
  }

  get disabled(): boolean {
    // if (this.debug) {
    //   console.debug(`super.disabled:${super.disabled} this.loading:${this.loading} this.form:`, this.form);
    // }
    return super.disabled || this.loading || (!!this.form ? this.invalid || !this.dirty : false);
  }

  get dirty(): boolean {
    return this.validIfPristine || super.dirty;
  }

  async validate(event: UIEvent) {
    event?.stopPropagation();
    // Avoid multiple call
    if (this.disabled) return;
    try {
      this.markAsLoading();
      // Get data to validate
      const data = await this.dataToValidate();
      await this.modalController.dismiss(data, 'validate');
    } catch (e) {
      this.onError(e);
      this.markAsLoaded();
    }
  }

  async cancel(role?: string) {
    await this.modalController.dismiss(null, role || 'cancel');
  }

  ngAfterViewInit(): void {
    setTimeout(async () => {
      await this.afterInit();
      this.markAsLoaded();
      this.enable();
    });
  }

  protected onError(e: any) {
    if (this.debug && !!e) console.error(`[modal-handler] Error on validating data`, e);
    this.markAsError(e?.message || e);
  }

  protected abstract afterInit(): Promise<void> | void;

  protected abstract dataToValidate(): R | Promise<R>;
}
