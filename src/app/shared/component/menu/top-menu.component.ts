import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Input, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AccountService, ConfigService, IMenuItem, MenuService } from '@sumaris-net/ngx-components';
import { MatMenu, MatMenuTrigger } from '@angular/material/menu';
import { Subscription } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

export type ShowLogoOption = 'NEVER' | 'ALWAYS' | 'ON_MAIN_MENU_HIDDEN';

@Component({
  selector: 'app-top-menu',
  templateUrl: 'top-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopMenuComponent implements OnInit, OnDestroy {
  @ViewChildren(MatMenuTrigger) triggers: QueryList<MatMenuTrigger>;

  @Input() items: IMenuItem[];
  @Input() showLogo: ShowLogoOption = 'ON_MAIN_MENU_HIDDEN';
  @Input() logo: string;

  filteredItems: IMenuItem[];
  logoVisible: boolean;
  opened = false;
  private subscription = new Subscription();
  private currentUrl: string;
  protected readonly menuService = inject(MenuService);
  private readonly configService = inject(ConfigService);
  private readonly accountService = inject(AccountService);
  private readonly router = inject(Router);
  private readonly cd = inject(ChangeDetectorRef);

  constructor() {}

  ngOnInit() {
    // Filter menu items
    this.initMenuItems();

    if (!this.logo) {
      this.subscription.add(this.configService.config.subscribe((config) => (this.logo = config.smallLogo)));
    }

    if (this.showLogo === 'ON_MAIN_MENU_HIDDEN') {
      this.subscription.add(
        this.menuService.splitPaneWhen.subscribe((value) => {
          this.logoVisible = !value;
          this.cd.markForCheck();
        })
      );
    } else {
      this.logoVisible = this.showLogo === 'ALWAYS';
    }

    // Listen navigation start to keep previous url/page
    this.subscription.add(
      this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
        this.currentUrl = event.url;
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  toggle() {
    this.opened = !this.opened;
  }

  openOnHover(menu: MatMenu) {
    if (!this.opened) return;
    this.closeAll(true);
    this.triggers.find((item) => item.menu === menu)?.openMenu();
  }

  closeAll(anotherMenuWillOpen?: boolean) {
    this.triggers.forEach((item) => item.closeMenu());
    this.opened = anotherMenuWillOpen || false;
  }

  isChildSelected(children: IMenuItem[]): boolean {
    return children.some((item) => this.currentUrl?.startsWith(item.path));
  }

  private initMenuItems() {
    // Filter out items by profile (2 levels only)
    this.filteredItems = this.items
      ?.filter((item) => this.hasMinProfile(item))
      .map((item) => ({
        ...item,
        children: item.children?.filter((item) => this.hasMinProfile(item)),
      }));
  }

  private hasMinProfile(item: IMenuItem): boolean {
    return !item.profile || this.accountService.hasMinProfile(item.profile);
  }
}
