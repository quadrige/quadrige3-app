import { AfterViewInit, ChangeDetectorRef, Directive, inject } from '@angular/core';
import { IMenuItem } from '@sumaris-net/ngx-components';

@Directive()
export class MenuPage implements AfterViewInit {
  private readonly cd = inject(ChangeDetectorRef);

  constructor(public menuItems: IMenuItem[]) {}

  ngAfterViewInit() {
    setTimeout(() => this.cd.markForCheck());
  }
}
