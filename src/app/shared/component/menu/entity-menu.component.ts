import { Component, Inject, input, Input, OnInit, Output } from '@angular/core';
import {
  createPromiseEventEmitter,
  emitPromiseEvent,
  Environment,
  ENVIRONMENT,
  IMenuItem,
  isEmptyArray,
  isNotEmptyArray,
} from '@sumaris-net/ngx-components';
import { BehaviorSubject } from 'rxjs';

export interface IEntityMenuItem<E, V = string> extends IMenuItem {
  attribute?: keyof E;
  entityName?: string;
  view?: V;
  viewArgs?: any;
  viewTitle?: string;
  viewTitleArgs?: any;
  children?: IEntityMenuItem<E, V>[];
  badge?: number | string;
  params?: any;
}

@Component({
  selector: 'app-entity-menu',
  templateUrl: 'entity-menu.component.html',
  styleUrls: ['entity-menu.component.scss'],
})
export class EntityMenuComponent<E, V = string> implements OnInit {
  menuTitleI18n = input<string>();
  selectFirstItemWithView = input<boolean>(false);
  panelClass = input<string>('default-menu-panel-class');

  @Output() selectedItemChange = createPromiseEventEmitter<boolean, IEntityMenuItem<E, V>>();

  protected items$ = new BehaviorSubject<IEntityMenuItem<E, V>[]>([]);
  protected selectedItem$ = new BehaviorSubject<IEntityMenuItem<E, V>>(undefined);

  private readonly debug: boolean;

  constructor(@Inject(ENVIRONMENT) protected environment: Environment) {
    // FOR DEV ONLY
    this.debug = !environment.production;
  }

  @Input() set items(items: IEntityMenuItem<E, V>[]) {
    this.items$.next(items);
  }

  get items(): IEntityMenuItem<E, V>[] {
    return this.items$.value;
  }

  @Input() set selectedItem(item: IEntityMenuItem<E, V>) {
    this.selectedItem$.next(item);
  }

  get selectedItem(): IEntityMenuItem<E, V> {
    return this.selectedItem$.value;
  }

  ngOnInit() {
    if (isNotEmptyArray(this.items) && !this.selectedItem && this.selectFirstItemWithView) {
      this.selectedItem = this.firstViewItem(this.items);
    }
    if (this.debug) {
      console.debug(`[entity-menu] items:`, this.items);
      console.debug(`[entity-menu] selected item:`, this.selectedItem);
    }
  }

  async itemClick(event: MouseEvent, item: IEntityMenuItem<E, V>) {
    event?.preventDefault();
    if (this.debug) {
      console.debug(`[entity-menu] item clicked:`, item);
    }

    // Send event
    emitPromiseEvent(this.selectedItemChange, 'selectedItemChange', { detail: item }).then((valid) => {
      if (valid) {
        this.selectedItem = item;
      }
    });
  }

  protected firstViewItem(items: IEntityMenuItem<E, V>[]): IEntityMenuItem<E, V> {
    if (isEmptyArray(items)) return undefined;
    const firstItem = items.find((item) => !!item.view || !!item.viewTitle);
    if (firstItem) return firstItem;
    for (const item of items) {
      if (isNotEmptyArray(item.children)) {
        const firstSubItem = this.firstViewItem(item.children);
        if (firstSubItem) return firstSubItem;
      }
    }
    return undefined;
  }
}
