import { ChangeDetectionStrategy, Component, inject, Injector, Input } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { IEntity, isNotEmptyArray, ObjectMap, toBoolean } from '@sumaris-net/ngx-components';
import { Dates } from '@app/shared/dates';
import moment from 'moment';
import { ExportType, exportTypeI18nLabel, exportTypeList, IExportModel } from '@app/shared/component/export/export.model';
import { Alerts } from '@app/shared/alerts';
import { EntityUtils } from '@app/shared/entity.utils';
import { BaseColumnItem, ExportOptions, IBaseTable } from '@app/shared/table/table.model';
import { IExportContext } from '@app/shared/service/base-entity.service';
import { IBaseEntityFilter, IBaseEntityFilterCriteria } from '@app/shared/model/filter.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { Validators } from '@angular/forms';
import { IModalOptions } from '@app/shared/model/options.model';

export interface IExportModalOptions extends IModalOptions, ExportOptions {
  table: IBaseTable<any, any>;
  columns?: BaseColumnItem[];
  additionalColumns?: ObjectMap<BaseColumnItem[]>;
  locale: string;
  sortBy: string;
  sortDirection: string;
}

@Component({
  selector: 'app-export-modal',
  templateUrl: './export.modal.html',
  styleUrls: ['./export.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExportModal<EM extends IExportModel = IExportModel, EC extends IExportContext = IExportContext>
  extends ModalComponent<EM>
  implements IExportModalOptions
{
  exportTypeList = exportTypeList;

  @Input() table: IBaseTable<any, any>;
  @Input() defaultFileName: string;
  @Input() defaultFilter: IBaseEntityFilter<any, IBaseEntityFilterCriteria<any>>;
  @Input() rowCountWarningThreshold: number;
  @Input() columns: BaseColumnItem[];
  @Input() additionalColumns: ObjectMap<BaseColumnItem[]>;
  @Input() forceAllColumns: boolean;
  @Input() transcribingItemEnabled = false;
  @Input() rightsEnabled = false;
  @Input() showSelectType = false;
  @Input() exportType: ExportType = 'CSV'; // default
  @Input() locale: string;
  @Input() sortBy: string;
  @Input() sortDirection: string;

  title: string;
  fileName: string;
  filter: IBaseEntityFilter<any, IBaseEntityFilterCriteria<any>>;
  entityName: string;
  entityDisplayName: string;
  selectedIds: any[];

  exporting = false;

  protected readonly alertCtrl = inject(AlertController);

  readonly logPrefix = '[export-modal]';

  constructor(protected injector: Injector) {
    super(injector);
    this.validIfPristine = true;
    // Build form
    this.setForm(this.formBuilder.group(this.getFormConfig()));
  }

  get disabled(): boolean {
    return super.disabled || this.exporting;
  }

  protected afterInit(): Promise<void> | void {
    // Build export filter
    this.filter = (this.defaultFilter || this.table.filter).asObject({ keepTypename: true });
    // Get entityName
    if (!this.entityName) {
      this.entityName = this.table.entityName;
    }
    // Build default file name
    this.entityDisplayName = this.translate.instant(this.table.entityI18nName(this.entityName));
    this.fileName =
      this.defaultFileName ||
      this.translate.instant('EXPORT.FILE_NAME_PATTERN', { entity: this.entityDisplayName, date: Dates.toLocalDateString(moment()) });
    // Build title
    this.title = this.showSelectType
      ? this.translate.instant('EXPORT.TITLE')
      : this.translate.instant('EXPORT.TITLE_WITH_FORMAT', { type: this.translate.instant(exportTypeI18nLabel(this.exportType || 'CSV')) });
    this.selectedIds = EntityUtils.ids(this.table.selection.selected);

    this.forceAllColumns = toBoolean(this.forceAllColumns, false);

    // Set form value
    this.setValue(this.getDefaultValue());
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);

    if (!this.selectedIds?.length) {
      this.form.controls.selectionOnly.disable();
    }
  }

  async export(event: MouseEvent) {
    event?.preventDefault();

    if (this.exporting) return;
    if (this.invalid) {
      console.error(`${this.logPrefix} Invalid form`);
      return;
    }
    if (!this.table) {
      console.error(`${this.logPrefix} Missing referential table`);
      return;
    }
    if (!this.filter) {
      console.error(`${this.logPrefix} Missing filter`);
      return;
    }

    const userOptions: EM = this.value;

    // Build header map
    const allFields: boolean = this.forceAllColumns || userOptions.allFields;
    const headers: string[] =
      this.columns
        ?.filter((column) => allFields || column.visible)
        // Sort columns only when visible fields are selected (Mantis #66544)
        .sort((a, b) => (!allFields ? a.rankOrder - b.rankOrder : 0))
        .map((column) => column.label) || [];

    // Build additional header maps
    const additionalHeaders: ObjectMap<string[]> = {};
    if (this.additionalColumns) {
      Object.keys(this.additionalColumns).forEach((key) => {
        additionalHeaders[key] = this.additionalColumns[key].filter((column) => allFields || column.visible).map((column) => column.label);
      });
    }
    // Estimate long export
    let longExport = !!this.rowCountWarningThreshold && this.table.totalRowCount > this.rowCountWarningThreshold;
    // Modify filter if selection only
    if (isNotEmptyArray(this.selectedIds) && userOptions.selectionOnly) {
      // Recreate a new filter with only selected ids (Mantis #59673)
      this.filter = { criterias: [{ includedIds: this.selectedIds }] };
      longExport = this.selectedIds.length > this.rowCountWarningThreshold;
    }
    // Ask confirmation if potentially long export
    if (
      longExport &&
      !(await Alerts.askConfirmation('EXPORT.CONFIRM_LONG_EXPORT', this.alertCtrl, this.translate, event, {
        entity: this.entityDisplayName,
      }))
    ) {
      return;
    }

    this.exporting = true;
    this.markAsError(undefined);
    this.markForCheck();
    try {
      // Execute export on an async job
      const job = await this.table.entityService.exportAllAsync(this.buildExportContext(userOptions, headers, additionalHeaders), this.filter);
      if (!job || (job as IEntity<any>).__typename !== 'JobVO') {
        console.error(`${this.logPrefix} a Job should be returned !`, job);
        throw new Error(this.translate.instant('EXPORT.ERROR.JOB_NOT_STARTED'));
      }
      this.exporting = false;
      await this.validate(event);
    } catch (e) {
      this.exporting = false;
      this.markAsError(e?.message || e);
    }
  }

  protected dataToValidate(): Promise<EM> | EM {
    return this.value;
  }

  protected getFormConfig(): Record<keyof EM, any> {
    return <Record<keyof EM, any>>{
      exportType: [null, Validators.required],
      fileName: [null, Validators.required],
      allFields: [false],
      selectionOnly: [false],
      withTranscribingItems: [false],
      withRights: [false],
    };
  }

  protected getDefaultValue(): EM {
    return <EM>{
      exportType: this.exportType,
      fileName: this.fileName,
      allFields: this.forceAllColumns,
      selectionOnly: false,
      withTranscribingItems: false,
      withRights: false,
    };
  }

  protected buildExportContext(userOptions: EM, headers: string[], additionalHeaders: ObjectMap<string[]>): EC {
    return <EC>{
      exportType: userOptions.exportType || this.exportType,
      fileName: userOptions.fileName,
      entityName: this.entityName,
      headers,
      additionalHeaders,
      locale: this.locale,
      sortBy: this.sortBy,
      sortDirection: this.sortDirection,
      withTranscribingItems: userOptions.withTranscribingItems || false,
      withRights: userOptions.withRights || false,
    };
  }
}
