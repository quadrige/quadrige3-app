import { ISimpleType } from '@app/shared/model/interface';

export type ExportType = 'CSV' | 'SHAPEFILE' | 'SHAPEFILE_WITH_CSV';

export const exportTypeList: Readonly<ISimpleType<ExportType>[]> = Object.freeze([
  {
    id: 'CSV',
    i18nLabel: 'EXPORT.TYPE.CSV',
  },
  {
    id: 'SHAPEFILE',
    i18nLabel: 'EXPORT.TYPE.SHAPEFILE',
  },
  {
    id: 'SHAPEFILE_WITH_CSV',
    i18nLabel: 'EXPORT.TYPE.SHAPEFILE_WITH_CSV',
  },
]);

export const exportTypeI18nLabel = (type: ExportType): string => exportTypeList.find((value) => value.id === type)?.i18nLabel;

export interface IExportModel {
  exportType?: ExportType;
  fileName: string;
  allFields: boolean;
  selectionOnly: boolean;
  withTranscribingItems: boolean;
  withRights: boolean;
}
