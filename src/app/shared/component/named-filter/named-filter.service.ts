import {
  AbstractNamedFilterService,
  AccountService,
  BaseEntityGraphqlQueries,
  EntityFilterUtils,
  GraphqlService,
  isEmptyArray,
  isNilOrBlank,
  isNotEmptyArray,
  isNotNilOrBlank,
  LoadResult,
  NamedFilter,
  Page,
  PlatformService,
} from '@sumaris-net/ngx-components';
import { Injectable } from '@angular/core';
import { NamedFilterFilter, NamedFilterFilterCriteria } from '@app/shared/component/named-filter/named-filter.model';
import gql from 'graphql-tag';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { IReferentialService, ReferentialServiceLoadOptions } from '@app/referential/service/base-referential.service';
import { IExportContext } from '@app/shared/service/base-entity.service';
import { Job } from '@app/social/job/job.model';
import { Observable } from 'rxjs';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

const namedFilterFragments = {
  namedFilter: gql`
    fragment NamedFilterFragment on NamedFilterVO {
      id
      name
      entityName
      recorderPersonId
      recorderDepartmentId
      updateDate
      __typename
    }
  `,
  namedFilterWithContent: gql`
    fragment NamedFilterWithContentFragment on NamedFilterVO {
      id
      name
      entityName
      content
      recorderPersonId
      recorderDepartmentId
      updateDate
      __typename
    }
  `,
};

const queries: BaseEntityGraphqlQueries = {
  loadAll: gql`
    query NamedFilters($filter: NamedFilterFilterVOInput, $offset: Int, $size: Int, $sortBy: String, $sortDirection: String) {
      data: namedFilters(filter: $filter, offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection) {
        ...NamedFilterWithContentFragment
      }
    }
    ${namedFilterFragments.namedFilterWithContent}
  `,
  loadAllWithTotal: gql`
    query NamedFiltersWithTotal($filter: NamedFilterFilterVOInput, $offset: Int, $size: Int, $sortBy: String, $sortDirection: String) {
      data: namedFilters(filter: $filter, offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection) {
        ...NamedFilterFragment
      }
      total: namedFiltersCount(filter: $filter)
    }
    ${namedFilterFragments.namedFilter}
  `,
  load: gql`
    query NamedFilter($id: Int) {
      data: namedFilter(id: $id) {
        ...NamedFilterWithContentFragment
      }
    }
    ${namedFilterFragments.namedFilterWithContent}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  save: gql`
    mutation SaveNamedFilter($data: NamedFilterVOInput!) {
      data: saveNamedFilter(namedFilter: $data) {
        ...NamedFilterWithContentFragment
      }
    }
    ${namedFilterFragments.namedFilterWithContent}
  `,
  delete: gql`
    mutation DeleteNamedFilter($id: Int!) {
      data: deleteNamedFilter(id: $id)
    }
  `,
};

@Injectable()
export class NamedFilterService
  extends AbstractNamedFilterService<NamedFilter, NamedFilterFilter>
  implements IReferentialService<NamedFilter, NamedFilterFilter, NamedFilterFilterCriteria, number, any, any>
{
  systemId: TranscribingSystemType;

  constructor(
    protected graphql: GraphqlService,
    protected platform: PlatformService,
    protected accountService: AccountService
  ) {
    super(graphql, platform, accountService, NamedFilter, NamedFilterFilter, { queries, mutations });
  }

  asSuggestFilter(searchText: string, filter?: NamedFilterFilter): NamedFilterFilter {
    filter = this.asFilter(filter);
    filter.criterias[0].searchText = searchText;
    return filter;
  }

  asFilter(source: any): NamedFilterFilter {
    const entityName = source?.entityName;
    const filter = EntityFilterUtils.fromObject(source || {}, this.filterType);
    if (isNilOrBlank(filter.criterias?.[0]?.entityName) && isNilOrBlank(entityName)) {
      console.error('The source filter should have an entityName property', source);
      throw new Error('The source filter should have an entityName property');
    }

    if (isEmptyArray(filter.criterias) || filter.criterias.length > 1) filter.criterias = [NamedFilterFilterCriteria.fromObject({})];
    if (isNilOrBlank(filter.criterias[0].entityName)) filter.criterias[0].entityName = entityName;
    filter.criterias[0].recorderPersonId = this.accountService.person.id;
    filter.criterias[0].systemId = this.systemId;
    return filter;
  }

  async exists(criteria: NamedFilterFilterCriteria, entityName?: string): Promise<boolean> {
    if (!!criteria && isNilOrBlank(criteria.entityName) && isNotNilOrBlank(entityName)) {
      criteria.entityName = entityName;
    }
    return (await this.countAll({ criterias: [criteria] })) > 0;
  }

  async find(name: string, entityName: string, opts?: ReferentialServiceLoadOptions): Promise<NamedFilter> {
    const debug = this._debug || opts?.debug;
    const filter = this.asFilter({ entityName });
    // Find with exact text
    filter.criterias[0].exactText = name;
    const variables = { filter: filter.asPodObject() };

    const now = Date.now();
    if (debug) console.debug(`${this._logPrefix} Loading NamedFilter items...`, variables);
    const { data } = await this.graphql.query<any>({
      query: this.queries.loadAll,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: opts?.fetchPolicy || 'cache-first',
    });
    if (debug) console.debug(`${this._logPrefix} NamedFilter items loaded in ${Date.now() - now}ms`, data);

    if (isNotEmptyArray(data)) {
      // Assume unique result
      return NamedFilter.fromObject(data[0]);
    }
    return undefined;
  }

  exportAllAsync(context: IExportContext, filter: NamedFilterFilter): Promise<Job> {
    throw new Error('Not implemented');
  }

  loadPage(page: Partial<Page>, filter: Partial<NamedFilterFilter>, opts?: any): Promise<LoadResult<NamedFilter>> {
    throw new Error('Not implemented');
  }

  watchPage(page: Partial<Page>, filter: NamedFilterFilter, opts?: any): Observable<LoadResult<NamedFilter>> {
    throw new Error('Not implemented');
  }
}
