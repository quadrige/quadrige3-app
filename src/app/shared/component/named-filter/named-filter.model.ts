import { EntityAsObjectOptions, EntityClass, FilterFn, INamedFilterFilter, NamedFilter } from '@sumaris-net/ngx-components';
import { BaseEntityFilter, BaseEntityFilterCriteria } from '@app/shared/model/filter.model';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';

@EntityClass({ typename: 'NamedFilterFilterCriteriaVO' })
export class NamedFilterFilterCriteria extends BaseEntityFilterCriteria<NamedFilter, number> {
  static fromObject: (source: any, opts?: any) => NamedFilterFilterCriteria;

  entityName: string = null;
  recorderPersonId: number = null;
  recorderDepartmentId: number = null;
  systemId: TranscribingSystemType = null;

  constructor() {
    super(NamedFilterFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.entityName = source.entityName;
    this.recorderPersonId = source.recorderPersonId;
    this.recorderDepartmentId = source.recorderDepartmentId;
    this.systemId = source.systemId;
  }

  protected buildFilter(): FilterFn<NamedFilter>[] {
    const filterFns = super.buildFilter() || [];

    if (this.exactText) {
      filterFns.push((entity) => entity.name === this.exactText);
    }
    if (this.searchText) {
      filterFns.push((entity) => entity.name.includes(this.searchText));
    }
    if (this.entityName) {
      filterFns.push((entity) => entity.entityName === this.entityName);
    }
    if (this.recorderPersonId) {
      filterFns.push((entity) => entity.recorderPersonId === this.recorderPersonId);
    }
    if (this.recorderDepartmentId) {
      filterFns.push((entity) => entity.recorderDepartmentId === this.recorderDepartmentId);
    }

    return filterFns;
  }
}

@EntityClass({ typename: 'NamedFilterFilterVO' })
export class NamedFilterFilter
  extends BaseEntityFilter<NamedFilterFilter, NamedFilterFilterCriteria, NamedFilter>
  implements INamedFilterFilter<NamedFilterFilter, NamedFilter>
{
  static fromObject: (source: any, opts?: any) => NamedFilterFilter;

  constructor() {
    super(NamedFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  get entityName(): string {
    return this.criterias?.[0]?.entityName;
  }

  get recorderDepartmentId(): number {
    return this.criterias?.[0]?.recorderDepartmentId;
  }

  get recorderPersonId(): number {
    return this.criterias?.[0]?.recorderPersonId;
  }

  get searchText(): string {
    return this.criterias?.[0]?.searchText;
  }

  criteriaFromObject(source: any, opts: any): NamedFilterFilterCriteria {
    return NamedFilterFilterCriteria.fromObject(source, opts);
  }
}
