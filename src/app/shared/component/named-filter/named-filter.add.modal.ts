import { Component, inject, Injector, Input, ViewChild } from '@angular/core';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';
import { APP_NAMED_FILTER_SERVICE, AppFormUtils, isNilOrBlank, isNotNilOrBlank, TextForm } from '@sumaris-net/ngx-components';
import { ReferentialAsyncValidators } from '@app/referential/service/referential-validator.service';
import { NamedFilterService } from '@app/shared/component/named-filter/named-filter.service';

export interface INamedFilterAddOptions extends IModalOptions {
  entityName: string;
  name?: string;
}

@Component({
  selector: 'app-named-filter-add-modal',
  templateUrl: './named-filter.add.modal.html',
})
export class NamedFilterAddModal extends ModalComponent<string> implements INamedFilterAddOptions {
  @ViewChild('nameForm') nameForm: TextForm;

  @Input() entityName: string;
  @Input() name: string;

  private readonly service = inject(APP_NAMED_FILTER_SERVICE) as NamedFilterService;

  constructor(protected injector: Injector) {
    super(injector);
  }

  get disabled(): boolean {
    return super.disabled || isNilOrBlank(this.name) || this.nameForm.invalid;
  }

  protected async dataToValidate(): Promise<string> {
    this.nameForm.updateValueAndValidity();
    await AppFormUtils.waitWhilePending(this.nameForm.textControl);
    if (this.nameForm.textControl.invalid) {
      const error = this.errorTranslator.translateFormErrors(this.nameForm.textControl, { pathTranslator: this });
      throw new Error(error);
    }
    return this.name;
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    this.nameForm.enable(opts);
  }

  protected afterInit() {
    // Add validator
    this.nameForm.textControl.addAsyncValidators(
      ReferentialAsyncValidators.checkAttributeAlreadyExists(
        this.service,
        undefined,
        { entityName: 'NamedFilter', newData: true, criteria: <any>{ entityName: this.entityName } },
        undefined
      )
    );

    if (isNotNilOrBlank(this.name)) {
      this.nameForm.value = { text: this.name };
    }
    this.registerSubscription(this.nameForm.textControl.valueChanges.subscribe((value) => (this.name = value)));
  }
}
