import { NgModule } from '@angular/core';
import { NamedFilterAddModal } from '@app/shared/component/named-filter/named-filter.add.modal';
import { QuadrigeSharedModule } from '@app/shared/quadrige.shared.module';
import { AppTextPopoverModule } from '@sumaris-net/ngx-components';

@NgModule({
  imports: [QuadrigeSharedModule, AppTextPopoverModule],
  declarations: [NamedFilterAddModal],
  exports: [NamedFilterAddModal],
})
export class NamedFilterModule {}
