import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@sumaris-net/ngx-components';
import { MaterialSharedModule } from '@app/shared/material/material.shared.module';
import { MapComponent } from '@app/shared/component/map/map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgxLeafletFullscreenModule } from '@runette/ngx-leaflet-fullscreen';

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule.forChild(), SharedModule, MaterialSharedModule, LeafletModule, NgxLeafletFullscreenModule],
  declarations: [MapComponent],
  exports: [MapComponent],
})
export class MapModule {}
