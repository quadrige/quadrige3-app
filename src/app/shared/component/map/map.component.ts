import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnDestroy } from '@angular/core';
import * as L from 'leaflet';
import { Layer, LeafletEvent } from 'leaflet';
import { isEmptyArray, isNotEmptyArray, isNotNil } from '@sumaris-net/ngx-components';
import { Feature, FeatureCollection, Geometry } from 'geojson';
import { PredefinedColors } from '@ionic/core';
import { LeafletControlLayersConfig } from '@asymmetrik/ngx-leaflet';
import { MapGraticule } from './map.graticule';
import 'leaflet-easybutton';
import '@geoman-io/leaflet-geoman-free';
import { Geometries } from '@app/shared/geometries';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';
import { BaseForm } from '@app/shared/form/base.form';

// Workaround for icon not shown in production mode (Mantis #57327)
const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41],
});
L.Marker.prototype.options.icon = iconDefault;

const maxZoom = 18;

@Component({
  selector: 'app-map-component',
  templateUrl: './map.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapComponent extends BaseForm<any> implements AfterViewInit, OnDestroy {
  @Input() showToolbar = true;
  @Input() titleI18n: string;
  @Input() toolbarColor: PredefinedColors = 'primary';
  @Input() canGoBack = false;
  @Input() showAllTooltips = false;
  @Input() showGraticule = false;

  // -- Map Layers --
  osmBaseLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom,
    attribution: "<a target='_blank' href='https://www.openstreetmap.org'>Open Street Map</a>",
  });
  sextantBaseLayer = L.tileLayer(
    'https://sextant.ifremer.fr/geowebcache/service/wmts' +
      '?Service=WMTS&Layer=sextant&Style=&TileMatrixSet=EPSG:3857&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix=EPSG:3857:{z}&TileCol={x}&TileRow={y}',
    { maxZoom, attribution: "<a target='_blank' href='https://sextant.ifremer.fr'>Sextant</a>" }
  );
  options = <L.MapOptions>{
    preferCanvas: true,
    attributionControl: true,
    layers: [this.osmBaseLayer],
    zoomControl: false,
    maxZoom,
    minZoom: 2,
    fullscreenControl: {
      position: 'topleft',
      title: {
        false: this.translate.instant('MAP.ENTER_FULLSCREEN'),
        true: this.translate.instant('MAP.EXIT_FULLSCREEN'),
      },
    },
  };
  layersControl = <LeafletControlLayersConfig>{
    baseLayers: {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'Open Street Map': this.osmBaseLayer,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'Sextant (Ifremer)': this.sextantBaseLayer,
    },
    overlays: {},
  };

  map: L.Map;
  drawEnabled = false;
  private layers: L.Layer[];
  private graticule = new MapGraticule({
    showLabel: true,
    opacity: 1,
    weight: 0.8,
    color: '#aaa',
    font: '12px Roboto, Helvetica, Arial, sans-serif',
    fontColor: 'black',
    zoomInterval: [
      { start: 1, end: 2, interval: 40 },
      { start: 3, end: 3, interval: 20 },
      { start: 4, end: 4, interval: 10 },
      { start: 5, end: 7, interval: 5 },
      { start: 8, end: 9, interval: 1 },
      { start: 10, end: 10, interval: 0.5 },
      { start: 11, end: 11, interval: 0.25 },
      { start: 12, end: 12, interval: 0.1 },
      { start: 13, end: 13, interval: 0.05 },
      { start: 14, end: 14, interval: 0.025 },
      { start: 15, end: 15, interval: 0.01 },
      { start: 16, end: 16, interval: 0.005 },
      { start: 17, end: 17, interval: 0.0025 },
      { start: 18, end: 18, interval: 0.001 },
    ],
  });

  constructor(protected injector: Injector) {
    super(injector);
    this.logPrefix = '[map]';
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    // Listen language change
    this.registerSubscription(
      this.translate.onLangChange.subscribe((event) => {
        this.map?.pm?.setLang(this.getSupportedLanguage(event.lang));
      })
    );

    this.fitToBounds();
  }

  onMapReady(leafletMap: L.Map) {
    this.map = leafletMap;

    // Add scale control
    L.control.scale().addTo(this.map);

    // Add customized zoom control
    L.control
      .zoom({
        zoomInTitle: this.translate.instant('MAP.ZOOM_IN'),
        zoomOutTitle: this.translate.instant('MAP.ZOOM_OUT'),
      })
      .addTo(this.map);

    // Add custom button to show all tooltips
    const tooltipControl: L.Control.EasyButton = L.easyButton({
      states: [
        {
          stateName: 'open',
          icon: '<i class="material-icons tooltip-button">label</i>',
          title: this.translate.instant('MAP.SHOW_ALL_TOOLTIPS'),
          onClick: (btn, map) => {
            this.showAllTooltips = true;
            map.eachLayer((layer) => this.bindTooltip(layer));
            btn.state('close');
          },
        },
        {
          stateName: 'close',
          icon: '<i class="material-icons tooltip-button">label_off</i>',
          title: this.translate.instant('MAP.HIDE_ALL_TOOLTIPS'),
          onClick: (btn, map) => {
            this.showAllTooltips = false;
            map.eachLayer((layer) => this.bindTooltip(layer));
            btn.state('open');
          },
        },
      ],
    });
    tooltipControl.addTo(this.map);

    // Add custom button to show/hide graticule
    const graticuleControl: L.Control.EasyButton = L.easyButton({
      states: [
        {
          stateName: 'show',
          icon: '<i class="material-icons tooltip-button">grid_on</i>',
          title: this.translate.instant('MAP.SHOW_GRATICULE'),
          onClick: (btn, map) => {
            this.showGraticule = true;
            this.graticule.addTo(map);
            btn.state('hide');
          },
        },
        {
          stateName: 'hide',
          icon: '<i class="material-icons tooltip-button">grid_off</i>',
          title: this.translate.instant('MAP.HIDE_GRATICULE'),
          onClick: (btn, map) => {
            this.showGraticule = false;
            this.graticule.removeFrom(map);
            btn.state('show');
          },
        },
      ],
    });
    graticuleControl.addTo(this.map);

    // Init Geoman plugin
    // L.PM.setOptIn(true);

    // Try to change attribution links (Mantis #60611)
    this.map.attributionControl.setPrefix(
      '<a target="_blank" href="https://leafletjs.com" title="A JavaScript library for interactive maps">Leaflet</a>'
    );

    // Log
    this.map.on('zoom', () => {
      if (this.debug) console.debug(`${this.logPrefix} zoom=${this.map.getZoom()}`);
    });

    // Call ready in a timeout to let leaflet map to initialize
    setTimeout(() => {
      this.map.fitWorld();
      this.markAsLoaded({ emitEvent: false });
      this.markAsReady({ emitEvent: false });
      this.markForCheck();
    });
  }

  async loadGeoData(data: Feature[] | FeatureCollection, drawEnabled?: boolean) {
    await this.ready();

    if (this.loading) {
      if (this.debug) {
        console.warn(`${this.logPrefix} Cannot load twice !`);
      }
      return;
    }

    try {
      this.markAsLoading();
      this.cleanMapLayers();

      const features: Feature[] = ((Array.isArray(data) ? data : data?.features) || []).filter(isNotNil);
      if (isEmptyArray(features)) {
        return;
      }

      const now = Date.now();
      if (this.debug) console.debug(`${this.logPrefix} Loading layer ...`);

      if (drawEnabled) {
        // Enable draw before adding layers
        this.enableDraw();
      }

      // Create layers
      features
        .map((feature) => this.fixFeature(feature))
        .forEach((feature) => {
          const layer = L.geoJSON(feature, {
            style: {
              className: 'geojson-shape',
            },
            pmIgnore: !drawEnabled,
            pointToLayer: (geoJsonPoint, latlng) => {
              // Convenient method to convert a circle feature to real circle shape
              // But Oracle can't hold a circle geometry with point+radius, it uses 3-points circle
              if (feature.properties.radius) {
                return new L.Circle(latlng, feature.properties.radius);
              } else {
                return new L.Marker(latlng);
              }
            },
          })
            .on('click', (event) => this.onLayerClick(event))
            .addTo(this.map);
          if (feature.properties?.name) {
            // Bind tooltip for feature name
            this.bindTooltip(layer);
          }
          if (drawEnabled) {
            this.addLayerEvent(layer);
          }
          this.layers.push(layer);
        });

      console.debug(`${this.logPrefix} ${features.length} geometries loaded in ${Date.now() - now}ms`);
    } catch (err) {
      console.error(err);
      this.errorEvent.emit(err?.message || err);
    } finally {
      this.markAsLoaded();
      await this.fitToBounds();
    }
  }

  bindTooltip(layer: L.Layer) {
    const feature = this.getFeature(layer);
    if (feature) {
      layer.unbindTooltip();
      layer.bindTooltip(() => feature.properties.name, {
        permanent: this.showAllTooltips,
        interactive: this.showAllTooltips,
        sticky: !this.showAllTooltips,
      });
    }
  }

  async fitToBounds() {
    await this.ready();

    if (isEmptyArray(this.layers)) {
      this.map.fitWorld();
      return;
    }

    // Compute bounds from layers
    let bounds: L.LatLngBounds;
    this.layers
      .filter((layer) => layer instanceof L.GeoJSON)
      .forEach((layer) => {
        if (!bounds) {
          bounds = (layer as L.GeoJSON).getBounds();
        } else {
          bounds.extend((layer as L.GeoJSON).getBounds());
        }
      });

    this.goTo(bounds);
  }

  enableDraw() {
    // Add map events to enable Geoman handlers and emit dirty event
    this.map.on('pm:create', (event: LeafletEvent) => {
      // noinspection JSDeprecatedSymbols
      event.layer.options.pmIgnore = false; // enable Geoman handlers (because of OptIn mode)
      // noinspection JSDeprecatedSymbols
      L.PM.reInitLayer(event.layer);
      if (this.debug) console.debug(`${this.logPrefix} Layer added`);
      this.dirtyEvent.emit();

      // Add layer events
      // noinspection JSDeprecatedSymbols
      this.addLayerEvent(event.layer);
    });
    this.map.on('pm:remove', () => {
      if (this.debug) console.debug(`${this.logPrefix} Layer removed`);
      this.dirtyEvent.emit();
    });

    this.map.pm.addControls({
      position: 'bottomleft',
      drawMarker: false,
      drawCircleMarker: false,
      drawText: false,
      drawPolyline: false,
    });
    this.map.pm.setLang(this.getSupportedLanguage());

    this.drawEnabled = true;
  }

  addLayerEvent(layer: Layer) {
    layer.on('pm:edit', () => {
      if (this.debug) console.debug(`${this.logPrefix} Layer modified`);
      this.dirtyEvent.emit();
    });
  }

  disableDraw() {
    this.map.pm.removeControls();
    this.drawEnabled = false;
  }

  hasDrawnFeatures(): boolean {
    return this.drawEnabled ? isNotEmptyArray(this.map.pm.getGeomanLayers(false)) : false;
  }

  getDrawnLayers(): FeatureCollection {
    this.map.pm.disableGlobalEditMode();
    const layers: L.Layer[] = this.map.pm.getGeomanLayers(false);
    const geometries: Geometry[] = [];
    for (const layer of layers) {
      /* If circle feature can be (de)serialized correctly
      // @ts-ignore
      const json = layer.toGeoJSON();

      // Specific handling of circle (see https://medium.com/geoman-blog/how-to-handle-circles-in-geojson-d04dcd6cb2e6)
      if (layer instanceof L.Circle) {
        json.properties.radius = layer.getRadius();
      }
       */

      let json: Feature;
      // If circles must be converted as polygon
      if (layer instanceof L.Circle) {
        json = L.PM.Utils.circleToPolygon(layer).toGeoJSON();
      } else {
        json = (<L.Circle>layer).toGeoJSON();
      }
      geometries.push(json.geometry);
    }
    return Geometries.toFeatureCollection(geometries);
  }

  containerResize() {
    setTimeout(() => this.map.invalidateSize(), 0);
  }

  /* -- protected methods -- */

  protected getFeature(layer: any): any {
    if (!layer) return undefined;
    if (layer.feature) {
      return layer.feature;
    } else if (layer._layers) {
      return this.getFeature(layer._layers[layer._leaflet_id - 1]);
    }
  }

  protected goTo(bounds: L.LatLngBounds) {
    if (bounds?.isValid()) {
      this.map.flyToBounds(bounds, { maxZoom, duration: 1 });
      // this.map.fitBounds(bounds, { maxZoom });
    } else {
      console.warn(`${this.logPrefix} Cannot fit to bound. GeoJSON layer not found.`);
    }
  }

  protected onLayerClick(event: L.LeafletEvent) {
    if (event.sourceTarget.feature) {
      this.onFeatureClick(event);
    } else {
      this.onTooltipClick(event);
    }
  }

  protected onTooltipClick(event: L.LeafletEvent) {
    if (this.debug) {
      console.debug(`${this.logPrefix} Tooltip clicked`, event);
    }
    const layer = event.sourceTarget._layers[event.sourceTarget._leaflet_id - 1];
    if (layer) {
      this.goTo(this.getBounds(layer));
    }
  }

  protected onFeatureClick(event: L.LeafletEvent) {
    if (this.debug) {
      console.debug(`${this.logPrefix} Feature clicked`, event);
    }
    const layer = event.target._layers[event.sourceTarget._leaflet_id];
    if (layer) {
      this.goTo(this.getBounds(layer));
    }
  }

  protected getBounds(layer: L.Layer | any): L.LatLngBounds {
    if (layer.getBounds) {
      // Line or Polygon
      return layer.getBounds();
    } else if (layer.getLatLng) {
      // Point
      return new L.LatLngBounds(layer.getLatLng(), layer.getLatLng());
    }
    return undefined;
  }

  protected cleanMapLayers() {
    if (this.drawEnabled) {
      (this.map.pm?.getGeomanLayers(false) || []).forEach((layer) => this.map.removeLayer(layer));
      this.disableDraw();
    }
    (this.layers || []).forEach((layer) => this.map.removeLayer(layer));
    this.layers = [];
  }

  private fixFeature(data: Feature): Feature {
    if (!data.type) {
      data.type = 'Feature';
    }
    return data;
  }

  private getSupportedLanguage(lang?: string): L.PM.SupportLocales {
    switch (lang || this.translate.currentLang) {
      case 'fr':
      case 'fr_FR':
        return 'fr';
      default:
        return 'en';
    }
  }
}
