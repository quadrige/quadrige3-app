import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { StatusById, StatusList } from '@sumaris-net/ngx-components';
import { Validators } from '@angular/forms';
import { CompositeUtils, IComposite } from '@app/shared/model/composite';
import { Referential } from '@app/referential/model/referential.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';

class StatusModel {
  statusId: number;

  static fromReferential(source: Referential<any>) {
    const target = new StatusModel();
    target.statusId = source.statusId;
    return target;
  }
}

export class StatusComposite implements IComposite {
  statusId: number = null;
  statusIdMultiple = false;

  constructor(source: StatusModel) {
    this.statusId = source.statusId;
  }
}

export interface IStatusMultiEditModalOptions extends IModalOptions {
  selection: Referential<any>[];
}

@Component({
  selector: 'app-status-multi-edit-modal',
  templateUrl: './status.multi-edit.modal.html',
  styleUrls: ['./status.multi-edit.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusMultiEditModal extends ModalComponent<StatusComposite> implements IStatusMultiEditModalOptions {
  @Input() placeholderI18n = 'REFERENTIAL.STATUS_ID';
  @Input() selection: Referential<any>[];

  statusList = StatusList;
  statusById = StatusById;

  constructor(protected injector: Injector) {
    super(injector);
    this.setForm(this.formBuilder.group({ statusId: [null, Validators.required], statusIdMultiple: [null] }));
  }

  protected afterInit() {
    const models: StatusModel[] = (this.selection || []).map((value) => StatusModel.fromReferential(value));
    this.value = CompositeUtils.build(models, (candidate) => new StatusComposite(candidate));
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    CompositeUtils.listenCompositeFormChanges(this.form.controls, (subscription) => this.registerSubscription(subscription));
  }

  protected dataToValidate(): Promise<StatusComposite> | StatusComposite {
    return CompositeUtils.value(this.value);
  }
}
