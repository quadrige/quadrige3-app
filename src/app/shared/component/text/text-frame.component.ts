import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { AppFormProvider, TextForm } from '@sumaris-net/ngx-components';
import { PredefinedColors } from '@ionic/core';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

@Component({
  selector: 'app-text-frame',
  templateUrl: './text-frame.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextFrameComponent extends AppFormProvider<TextForm> {
  @ViewChild('textForm', { static: true }) textForm: TextForm;

  @Input() toolbarColor: PredefinedColors = 'secondary';
  @Input() titleI18n: string;
  @Input() maxLength = 2000;
  @Input() autofocus = false;

  @Output() valueChanged = new EventEmitter<string>();

  protected settingValue = false;

  constructor(private cd: ChangeDetectorRef) {
    super(() => this.textForm);
  }

  get value(): string {
    // inner form can not be initialized
    if (!this.textForm.form) return undefined;
    return this.textForm.value?.text;
  }

  set value(text: string) {
    this.settingValue = true;
    this.textForm.setValue({ text });
    this.settingValue = false;
  }

  textAreaChanged(value: string) {
    if (this.settingValue) return;
    this.valueChanged.emit(value);
  }

  @Input()
  set canEdit(value: boolean) {
    if (value) {
      this.settingValue = true;
      this.enable();
      this.settingValue = false;
    } else {
      this.disable();
    }
  }

  containerResize() {
    setTimeout(() => {
      this.textForm.containerResize();
      this.cd.markForCheck();
    });
  }
}
