import { NG_VALUE_ACCESSOR, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { ChangeDetectionStrategy, Component, forwardRef, inject, Input, OnInit } from '@angular/core';
import { FloatLabelType } from '@angular/material/form-field';
import { DateFilterUtils } from '@app/shared/model/date-filter';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-date-range-filter-form-field',
  templateUrl: './date-range-filter-form-field.component.html',
  styleUrls: ['./date-range-filter-form-field.component.scss'],
  standalone: true,
  imports: [QuadrigeCoreModule],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => DateRangeFilterFormField),
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateRangeFilterFormField implements OnInit {
  @Input() form: UntypedFormGroup;
  @Input() logPrefix = '[date-range-filter-form-field] ';

  @Input() floatLabel: FloatLabelType = 'auto';
  @Input() startDateI18n: string;
  @Input() endDateI18n: string;

  @Input() mobile: boolean;
  @Input() tabindex: number;
  @Input() readonly = false;
  @Input() required = false;
  @Input() clearable = true;
  @Input() doubleRange = true;
  @Input() debug = false;

  private readonly formBuilder = inject(UntypedFormBuilder);

  constructor() {}

  get hasStart(): boolean {
    return !!this.value?.startLowerBound || !!this.value?.startUpperBound;
  }

  get hasEnd(): boolean {
    return !!this.value?.endLowerBound || !!this.value?.endUpperBound;
  }

  get value(): any {
    return this.form.value;
  }

  ngOnInit(): void {
    if (!this.form) this.form = DateFilterUtils.createFormGroup(this.formBuilder);
  }
}
