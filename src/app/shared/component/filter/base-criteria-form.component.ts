import { AfterViewInit, Directive, inject, input, model, OnDestroy, viewChild } from '@angular/core';
import { IBaseTable } from '@app/shared/table/table.model';
import { BaseEntityFilter, BaseEntityFilterCriteria, BaseFilterUtils } from '@app/shared/model/filter.model';
import { BaseFilterFormComponent, ICheckSystemIdOptions } from '@app/shared/component/filter/base-filter-form.component';
import { Utils } from '@app/shared/utils';
import { AbstractControl } from '@angular/forms';
import { Params } from '@angular/router';
import {
  Beans,
  changeCaseToUnderscore,
  isNotEmptyArray,
  isNotNilOrBlank,
  PromiseEvent,
  TranslateContextService,
  waitForTrue,
} from '@sumaris-net/ngx-components';
import { PartialRecord } from '@app/shared/model/interface';
import { DateFilter } from '@app/shared/model/date-filter';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { attributes } from '@app/referential/model/referential.constants';
import { BehaviorSubject, Subject } from 'rxjs';
import { WaitForOptions } from '@sumaris-net/ngx-components/src/app/shared/observables';
import { Alerts } from '@app/shared/alerts';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { EntityUtils } from '@app/shared/entity.utils';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';

/**
 * Base component for table filter templates
 */
@Directive()
export abstract class BaseCriteriaFormComponent<F extends BaseEntityFilter<F, C, any, any>, C extends BaseEntityFilterCriteria<any, any>>
  implements AfterViewInit, OnDestroy
{
  table = input.required<IBaseTable<any, any>>();
  allowTabs = input<boolean>(true);
  allowNamedFilter = input<boolean>(true);

  base = viewChild.required<BaseFilterFormComponent<F, C>>('base');

  protected readonly alertController = inject(AlertController);
  protected readonly translateService = inject(TranslateService);
  protected readonly translateContextService = inject(TranslateContextService);
  protected searchAttributes = model<string[]>(undefined);
  protected searchFields = model<string>(undefined);
  private readonly ready$ = new BehaviorSubject<boolean>(false);
  private readonly destroy$ = new Subject<void>();

  protected constructor() {}

  get value(): F {
    return this.base().form.getRawValue();
  }

  get systemId(): TranscribingSystemType {
    return this.base().systemId;
  }

  get i18nColumnPrefix(): string {
    return this.table().i18nColumnPrefix;
  }

  get filterCriteriaCount(): number {
    return this.base().filterCriteriaCount;
  }

  ngAfterViewInit() {
    this.base().panelOpened.subscribe(() => this.panelOpened());
    this.base().selectedTab.subscribe(() => this.updateView());

    this.searchAttributes.subscribe((attributes) => {
      if (isNotEmptyArray(attributes)) {
        const context = changeCaseToUnderscore(this.table().entityName).toUpperCase();
        this.searchFields.set(
          attributes
            .map((attribute) => this.table().getI18nColumnName(attribute))
            .map((attribute) => this.translateContextService.instant(attribute, context))
            .join(', ')
        );
      }
    });

    // Subscription related to transcribing system
    this.base().systemIdChange.subscribe((systemId) => this.updateSystemId(systemId, true));
    this.base().valueSet.subscribe((value) => this.updateSystemId(value?.systemId));
    this.base().checkSystemIdEventEmitter.subscribe((event) => this.confirmSystemIdChange(event));

    this.markAsReady();
  }

  clearControlValue(event: UIEvent, formControl: AbstractControl, path?: string, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    Utils.clearControlValue(event, formControl, path, opts);
  }

  // Delegate methods

  async setValue(value: F, opts?: ISetFilterOptions<C>) {
    await this.ready();
    await this.base().setValue(value, opts);

    this.updateSystemId(this.systemId);
  }

  async patchCriterias(value: Partial<C>, opts?: { emitEvent?: boolean }) {
    this.base().criteriaControls.forEach((formGroup) => {
      formGroup.patchValue(this.base().asCriteria(value).asObject());
    });
    if (opts?.emitEvent !== false) {
      await this.submit(undefined);
    }
  }

  updateFilterCriteriaCount() {
    this.base().updateFilterCriteriaCount();
  }

  async submit(event: UIEvent, opts?: ISetFilterOptions<C>) {
    await this.base().submit(event, opts);
  }

  toggle() {
    this.base().toggle();
  }

  open() {
    this.base().open();
  }

  close() {
    this.base().close();
  }

  disableControlPermanent(control: keyof C) {
    this.base().addDisabledControl(control, true);
  }

  disableControl(control: keyof C) {
    this.base().addDisabledControl(control, false);
  }

  enableControl(control: keyof C) {
    this.base().removeDisabledControl(control);
  }

  clearControl(control: keyof C, propertiesToPreserve?: string[]) {
    this.base().clearControl(control, propertiesToPreserve);
  }

  filterToQueryParam(): Params {
    if (isNotNilOrBlank(this.base().name)) {
      return { filterName: this.base().name };
    } else {
      const params: Params = {};
      if (!TranscribingItemTypeUtils.isDefaultSystem(this.systemId)) {
        params['system'] = this.systemId;
      }
      params['criterias'] =
        this.base()
          .criterias?.map((criteria) => this.criteriaToQueryParams(criteria))
          .filter((criteria) => Object.keys(criteria).length)
          .map((criteria) => JSON.stringify(criteria)) || [];
      return params;
    }
  }

  async queryParamToFilter(params: Params) {
    if (params?.filterName) {
      await this.setValue(
        {
          ...this.base().value,
          name: params.filterName,
        },
        { findByName: true }
      );
    } else {
      let criterias: string | string[] = params?.criterias;
      if (!Array.isArray(criterias)) criterias = [criterias];
      if (isNotEmptyArray(criterias)) {
        await this.setValue({
          ...this.base().value,
          systemId: params?.system,
          criterias: criterias.filter(isNotNilOrBlank).map((criteria) => this.queryParamsToCriteria(criteria)),
        });
      }
    }
  }

  async updateEntityName(entityName: string) {
    await this.base().updateEntityName(entityName);
    this.updateSearchAttributes();
  }

  setSearchFieldsText(value: string) {
    this.searchFields.set(value);
  }

  protected updateSearchAttributes(systemId?: TranscribingSystemType) {
    systemId = systemId ?? this.systemId;
    let searchAttributes = this.getSearchAttributes();
    if (!TranscribingItemTypeUtils.isDefaultSystem(systemId)) {
      searchAttributes = searchAttributes?.filter((attribute) => this.filterAttributeBySystem(attribute, systemId));
    }
    this.searchAttributes.set(searchAttributes);
  }

  protected abstract getSearchAttributes(): string[];

  protected async confirmSystemIdChange(event?: PromiseEvent<boolean, ICheckSystemIdOptions>) {
    let confirmed = true;
    if (!BaseFilterUtils.isFilterEmpty(this.table().asFilter(this.value))) {
      confirmed = await Alerts.askConfirmation('REFERENTIAL.FILTER.CONFIRM.CHANGE_SYSTEM', this.alertController, this.translateService);
    }
    event.detail.success(confirmed);
  }

  // This version of confirmSystemIdChange don't test all properties, but I keep it for the moment in case of specification change
  // protected async confirmSystemIdChange(event?: PromiseEvent<boolean, ICheckSystemIdOptions>) {
  //   let hasNonEmptyCriteria = false;
  //   const criteriaKeys = Object.keys(this.base().asCriteria(undefined));
  //   while (criteriaKeys.length > 0 && !hasNonEmptyCriteria) {
  //     const criteria = criteriaKeys.pop() as keyof C;
  //     if (criteria === 'systemId') continue;
  //
  //     // Check not nil value or not empty subCriteria on not-preserved criterias
  //     if (!this.getPreservedCriteriaBySystem(event.detail.nextSystemId).includes(criteria)) {
  //       hasNonEmptyCriteria = this.base().criteriaControls.some((formGroup) => {
  //         const control = formGroup.controls[criteria as string];
  //         if (control.enabled) {
  //           const value = control.value;
  //           if (typeof value === 'object') {
  //             if (!BaseFilterUtils.isCriteriaEmpty(value, true) || !!getPropertyByPath(value, 'id')) {
  //               return true;
  //             }
  //           } else if (isNotNilOrBlank(value)) {
  //             return true;
  //           }
  //         }
  //         return false;
  //       });
  //     }
  //
  //     // Check included/excluded ids on subCriteria on all criterias
  //     if (!hasNonEmptyCriteria) {
  //       hasNonEmptyCriteria = this.base().criteriaControls.some((formGroup) => {
  //         const control = formGroup.controls[criteria as string];
  //         if (control.enabled) {
  //           const value = control.value;
  //           if (isNotNil(value) && typeof value === 'object') {
  //             if (isNotEmptyArray(value.includedIds) || isNotEmptyArray(value.excludedIds)) {
  //               return true;
  //             }
  //           }
  //         }
  //       });
  //     }
  //   }
  //   const confirmed = hasNonEmptyCriteria
  //     ? await Alerts.askConfirmation('REFERENTIAL.FILTER.CONFIRM.CHANGE_SYSTEM', this.alertController, this.translateService)
  //     : true;
  //
  //   // Set event result
  //   event.detail.success(confirmed);
  // }

  protected updateSystemId(systemId: TranscribingSystemType, reset?: boolean) {
    this.updateFormGroup(systemId, reset);
    this.updateSearchAttributes(systemId);
  }

  protected updateFormGroup(systemId: TranscribingSystemType, reset?: boolean) {
    Object.keys(this.base().asCriteria(undefined))
      .map((criteria) => criteria as keyof C)
      .forEach((criteria) => {
        if (TranscribingItemTypeUtils.isDefaultSystem(systemId) || this.getPreservedCriteriaBySystem(systemId).includes(criteria)) {
          if (reset) {
            // Reset control but preserving searchText (because search text is independent of system)
            this.clearControl(criteria /*, ['searchText']*/); // Note: Don't preserve searchText at the moment
          }
          this.enableControl(criteria);
        } else {
          if (reset) {
            // Reset entirely
            this.clearControl(criteria);
          }
          this.disableControl(criteria);
        }
      });
    this.updateView();
  }

  updateView() {}

  protected filterAttributeBySystem(attribute: string, systemId: TranscribingSystemType): boolean {
    // Default: only id and name
    return attributes.idName.includes(attribute);
  }

  protected getPreservedCriteriaBySystem(systemId: TranscribingSystemType): (keyof C)[] {
    // Preserve all attribute of base criteria
    return ['id', 'updateDate', '__typename', 'searchAttributes', 'searchText', 'exactText', 'includedIds', 'excludedIds', 'forceIncludedIds'];
  }

  protected criteriaToQueryParams(criteria: C): PartialRecord<keyof C, any> {
    const params: PartialRecord<keyof C, any> = {};
    if (isNotNilOrBlank(criteria?.searchText)) params.searchText = criteria.searchText;
    if (isNotEmptyArray(criteria?.includedIds)) {
      params.includedIds = criteria.includedIds;
    }
    if (isNotEmptyArray(criteria?.excludedIds)) {
      params.excludedIds = criteria.excludedIds;
    }
    return params;
  }

  protected subCriteriaToQueryParams(params: PartialRecord<keyof C, string>, criteria: C, subCriteriaName: keyof C) {
    let subCriteria: any = criteria?.[subCriteriaName];
    if (!!subCriteria && typeof subCriteria.asObject === 'function') {
      subCriteria = subCriteria.asObject();
    }
    // Note: don't use BaseFilterUtils.isEmpty because of ignored properties
    if (!Beans.isEmpty(subCriteria, undefined, { blankStringLikeEmpty: true })) {
      const criteriaParam = BaseFilterUtils.cleanCriteria(subCriteria, true);
      if (!EntityUtils.isEmptyObject(criteriaParam)) {
        params[subCriteriaName] = criteriaParam;
      }
    }
    // todo use another serializer: HttpParams but need to modify array serialization
    // const p = new HttpParams({fromObject: filter});
    // console.info(p.toString());
  }

  protected dateCriteriaToQueryParams(params: PartialRecord<keyof C, string>, criteria: C, subCriteriaName: keyof C) {
    const dateFilter = DateFilter.fromObject(criteria?.[subCriteriaName]);
    if (dateFilter && !dateFilter.isEmpty()) {
      params[subCriteriaName] = dateFilter.asObject();
    }
  }

  protected queryParamsToCriteria(params: string): C {
    return JSON.parse(params);
  }

  protected panelOpened() {
    this.updateView();
  }

  ngOnDestroy() {
    this.ready$.unsubscribe();
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  private markAsReady() {
    if (!this.ready$.closed && this.ready$.value !== true) {
      this.ready$.next(true);
    }
  }

  private async ready(opts?: WaitForOptions) {
    if (this.ready$.value) return Promise.resolve();
    return waitForTrue(this.ready$, { stop: this.destroy$, ...opts });
  }
}
