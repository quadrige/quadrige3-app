import { BaseForm } from '@app/shared/form/base.form';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  inject,
  Injector,
  input,
  model,
  OnInit,
  Output,
  output,
  TemplateRef,
  viewChild,
} from '@angular/core';
import { MatExpansionPanel } from '@angular/material/expansion';
import {
  AppFormArray,
  arraySize,
  createPromiseEventEmitter,
  emitPromiseEvent,
  getPropertyByPath,
  isNilOrBlank,
  isNotEmptyArray,
  isNotNil,
  isNotNilOrBlank,
  MINIFY_ENTITY_FOR_POD,
  NamedFilter,
  NamedFilterSelector,
  SharedNamedFilterModule,
} from '@sumaris-net/ngx-components';
import { IBaseTable } from '@app/shared/table/table.model';
import { BaseEntityFilter, BaseEntityFilterCriteria, BaseFilterUtils } from '@app/shared/model/filter.model';
import { MatTabNav } from '@angular/material/tabs';
import { Alerts } from '@app/shared/alerts';
import { AlertController } from '@ionic/angular';
import { UntypedFormGroup } from '@angular/forms';
import { PartialRecord } from '@app/shared/model/interface';
import { DateFilter, DateFilterUtils } from '@app/shared/model/date-filter';
import { MatAutocompleteFieldConfig } from '@sumaris-net/ngx-components/src/app/shared/material/autocomplete/material.autocomplete.config';
import { INamedFilterAddOptions, NamedFilterAddModal } from '@app/shared/component/named-filter/named-filter.add.modal';
import { NamedFilterService } from '@app/shared/component/named-filter/named-filter.service';
import { StrReferential } from '@app/referential/model/referential.model';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { distinctUntilChanged, filter, map, mergeMap, pairwise, startWith } from 'rxjs/operators';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { SelectionModule } from '@app/selection/selection.module';
import { NamedFilterModule } from '@app/shared/component/named-filter/named-filter.module';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';
import { transcribingSystemsForFilter, TranscribingSystemType } from '@app/referential/transcribing-item-type/transcribing-item-type.model';
import { TranscribingItemTypeService } from '@app/referential/transcribing-item-type/transcribing-item-type.service';
import { uniqueValue, Utils } from '@app/shared/utils';
import { ReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { ReferentialFilterFormFieldValidator } from '@app/referential/component/referential-filter-form-field.validator';
import { MatSelect } from '@angular/material/select';
import { TranscribingItemTypeUtils } from '@app/referential/transcribing-item-type/transcribing-item-type.utils';
import { EntityUtils } from '@app/shared/entity.utils';
import { ReferentialUtils } from '@app/referential/model/referential.utils';

export interface ICheckSystemIdOptions {
  previousSystemId: TranscribingSystemType;
  nextSystemId: TranscribingSystemType;
}

/**
 * Base component for table filter
 */
@Component({
  selector: 'app-base-filter-form',
  templateUrl: './base-filter-form.component.html',
  styleUrls: ['./base-filter-form.component.scss'],
  standalone: true,
  imports: [QuadrigeCoreModule, SelectionModule, SharedNamedFilterModule, NamedFilterModule],
  providers: [NamedFilterService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BaseFilterFormComponent<F extends BaseEntityFilter<F, C, any, any>, C extends BaseEntityFilterCriteria<any, any>>
  extends BaseForm<F>
  implements OnInit, AfterViewInit
{
  filterPanel = viewChild.required<MatExpansionPanel>('filterExpansionPanel');
  filterPanelRef = viewChild.required('filterExpansionPanel', { read: ElementRef });
  namedFilterSelector = viewChild<NamedFilterSelector>('namedFilterSelector');
  filterTabs = viewChild(MatTabNav);
  transcribingSystemSelect = viewChild<MatSelect>('transcribingSystemSelect');

  table = input.required<IBaseTable<any, any>>();
  filterCriteriaTemplate = input.required<TemplateRef<any>>();
  allowTabs = input.required<boolean>();
  allowNamedFilter = input.required<boolean>();

  panelOpened = new EventEmitter<boolean>();
  systemIdChange = output<TranscribingSystemType>();
  @Output() checkSystemIdEventEmitter = createPromiseEventEmitter<boolean, ICheckSystemIdOptions>();
  valueSet = output<F>();

  autocompleteConfig: MatAutocompleteFieldConfig = {
    showAllOnFocus: true,
  };
  tabs: number[] = [];
  selectedTab = model(0);

  filterCriteriaCount = 0;
  transcribingSystemIds: TranscribingSystemType[];
  transcribingSystemsByIds: Record<TranscribingSystemType, StrReferential>;
  transcribingSystemAllowed = true;
  showTranscribingSystemSelect = false;

  protected readonly alertController = inject(AlertController);
  protected readonly namedFilterService = inject(NamedFilterService);
  protected readonly transcribingItemTypeService = inject(TranscribingItemTypeService);
  protected readonly referentialFilterFormFieldValidator = inject(ReferentialFilterFormFieldValidator);
  protected inconsistentCount = false;
  private disabledControls: (keyof C)[] = [];
  private permanentDisabledControls: (keyof C)[] = [];
  private settingValue = false;
  private defaultCriteria: Partial<C>;
  private staticCriteria: Partial<C>;

  constructor(protected injector: Injector) {
    super(injector);
    this.logPrefix = '[base-filter-form]';
  }

  get loading(): boolean {
    return super.loading || this.table().loading;
  }

  get value(): F {
    return super.value;
  }

  get name(): string {
    return this.value?.name;
  }

  get systemId(): TranscribingSystemType {
    return this.value?.systemId;
  }

  get criterias(): C[] {
    return this.value?.criterias;
  }

  get criteriaControls(): UntypedFormGroup[] {
    return this.criteriasFormArray.controls.map((control) => control as UntypedFormGroup);
  }

  get criteriasFormArray(): AppFormArray<C, UntypedFormGroup> {
    return this.form.controls.criterias as AppFormArray<C, UntypedFormGroup>;
  }

  get entityName(): string {
    return this.table().entityName;
  }

  filterContentProvider = (): object => {
    const filter = this.asFilter(this.value);
    filter.patch(this.staticCriteria);
    const content = BaseFilterUtils.clean(filter.asObject({ ...MINIFY_ENTITY_FOR_POD, keepMinifiedSystemId: true }));
    // Remove filter name from content to save
    delete content.name;
    return content;
  };

  ngOnInit(): void {
    super.ngOnInit();

    // Default empty form
    this.setForm(this.formBuilder.group({}));

    if (this.debug) {
      console.debug(`${this.logPrefix} allowTabs:${this.allowTabs()} allowNamedFilter:${this.allowNamedFilter()}`);
    }
  }

  ngAfterViewInit() {
    // Initialize form
    this.setForm(
      this.formBuilder.group({
        name: [null],
        systemId: [null],
        // Add criterias array
        criterias: new AppFormArray<C, UntypedFormGroup>(
          (criteria) => this.createCriteriaFormGroup(criteria),
          () => false,
          () => false
        ),
      })
    );

    // Update tabs on criterias count change
    this.registerSubscription(
      this.form.controls.criterias.valueChanges
        .pipe(
          map((value) => arraySize(value)),
          filter((value) => value > 0),
          distinctUntilChanged()
        )
        .subscribe((nbCriteria) => {
          this.tabs = [...Array(nbCriteria).keys()];
          this.selectedTab.update((selectedTab) => Math.min(selectedTab, nbCriteria - 1));
        })
    );
    // Add first empty criteria
    this.criteriasFormArray.add(this.asCriteria({}));

    this.enable();
    this.markAsLoaded();
    this.markAsReady();
    this.markForCheck();
    this.updateView();

    this.registerSubscription(this.form.valueChanges.pipe(filter(() => !this.settingValue)).subscribe(() => (this.inconsistentCount = true)));

    // React on system change
    this.registerSubscription(
      this.form.controls.systemId.valueChanges
        .pipe(
          startWith(this.systemId),
          pairwise(),
          filter(() => !this.settingValue),
          mergeMap(async ([previousSystemId, nextSystemId]) => {
            if (this.checkSystemIdEventEmitter.observed) {
              // Ask confirmation by external check to allow system change
              const nextSystemIdValid = await emitPromiseEvent(this.checkSystemIdEventEmitter, 'checkSystemId', {
                detail: <ICheckSystemIdOptions>{ previousSystemId, nextSystemId },
              });
              if (nextSystemIdValid) {
                return [previousSystemId, nextSystemId];
              } else {
                return [previousSystemId, null];
              }
            }
            return [previousSystemId, nextSystemId];
          })
        )
        .subscribe(([previousSystemId, nextSystemId]) => {
          if (isNotNil(nextSystemId)) {
            // Propagate system change
            this.systemIdChange.emit(nextSystemId);
          } else if (isNotNil(previousSystemId)) {
            // Restore previous value silently
            this.settingValue = true;
            this.form.patchValue({ systemId: previousSystemId });
            this.settingValue = false;
          }
        })
    );
  }

  async updateEntityName(entityName: string) {
    if (!this.allowNamedFilter()) return;
    await this.ready();
    this.detectChanges();
    this.namedFilterSelector().entityName = entityName;
    await this.loadTranscribingSystems(entityName);
    this.updateView();
  }

  async setValue(filter: F, opts?: ISetFilterOptions<C>) {
    await this.ready();

    // Try to load existing filter
    if (opts?.findByName && isNotNilOrBlank(filter?.name)) {
      const name = filter.name;
      const namedFilter = await this.namedFilterService.find(name, this.entityName, { debug: this.debug });
      if (!!namedFilter) {
        // Update selector component
        this.namedFilterSelector().setValue({ namedFilter });
        // Build filter to set
        filter = { ...namedFilter.content, name: namedFilter.name };
      } else {
        // Not found: Remove the name from input filter
        delete filter.name;
      }
    }

    // Check conformity of this filter with systemId, if any (Mantis #66818)
    const parentSystemId = opts?.staticCriteria?.systemId;
    const currentSystemId = filter?.systemId;
    if (isNotNil(parentSystemId)) {
      // Update service
      this.namedFilterService.systemId = parentSystemId;

      // Don't allow system selection change
      this.transcribingSystemAllowed = false;

      // If current system doesn't correspond to parent,
      if (isNotNil(currentSystemId) && parentSystemId !== currentSystemId) {
        // Simply reset the current filter
        filter = undefined;
        this.clearSelectedFilterName();
        if (this.debug) {
          console.debug(
            `${this.logPrefix} Filter systemId (${currentSystemId}) does not match parent systemId (${parentSystemId}), resetting filter...`
          );
        }
      }
    }

    // Convert to filter object
    filter = this.asFilter(filter);

    // Patch with default criteria, only if empty filter
    this.defaultCriteria = opts?.defaultCriteria;
    if (!!this.defaultCriteria && filter.isEmpty()) {
      filter.patch(BaseFilterUtils.cleanCriteria(this.defaultCriteria, true));
    }
    // Patch with static criteria
    this.staticCriteria = opts?.staticCriteria;
    if (!!this.staticCriteria) {
      filter.patch(BaseFilterUtils.cleanCriteria(this.staticCriteria, true));
      this.disableControlFromCriteria(this.staticCriteria);
    }

    if (opts?.resetNamedFilter) {
      this.clearSelectedFilterName();
    } else {
      if (isNilOrBlank(filter.name) && isNotNilOrBlank(this.getSelectedFilterName())) {
        // Update filter name from selection
        filter.name = this.getSelectedFilterName();
      } else if (!this.namedFilterSelector()?.value?.namedFilter?.id) {
        // Set filter name if not already set by id
        this.setSelectedFilterName(filter.name);
      }
    }

    // Set value
    this.settingValue = true;
    await super.setValue(filter, { emitEvent: true, ...opts });
    this.markAsPristine();
    this.settingValue = false;
    this.inconsistentCount = true;
    this.updateView();
    this.valueSet.emit(filter);
  }

  resetFilterName() {
    this.form.patchValue({ name: undefined });
  }

  updateFilterCriteriaCount() {
    this.filterCriteriaCount = this.asFilter(this.form.getRawValue())?.countNotEmptyCriteria() || 0;
    this.inconsistentCount = false;
  }

  criteriaCount(tab: number): number | undefined {
    const count = this.asCriteria(this.criteriasFormArray.at(tab)?.getRawValue()).countNotEmptyCriteria();
    return count === 0 ? undefined : count;
  }

  addCriteria() {
    this.criteriasFormArray.add(this.asCriteria(BaseFilterUtils.cleanCriteria({ ...this.defaultCriteria, ...this.staticCriteria }, true)));
    this.detectChanges();
    this.selectedTab.set(this.tabs[this.tabs.length - 1]);
  }

  duplicateCriteria(index: number) {
    this.criteriasFormArray.add(this.criteriasFormArray.at(index).getRawValue(), { insertAt: index, emitEvent: true });
    this.detectChanges();
    this.selectedTab.update((selectedTab) => selectedTab++);
  }

  async removeCriteria(tab: number) {
    if (this.criteriaCount(tab)) {
      if (!(await Alerts.askDetailedConfirmation('REFERENTIAL.FILTER.BLOCK.CONFIRM.DELETE', this.alertController, this.translate))) {
        return;
      }
    }
    this.criteriasFormArray.removeAt(tab);
    this.detectChanges();
  }

  async submit(event: UIEvent, opts?: ISetFilterOptions<C>) {
    if (this.dirty && !!this.namedFilterSelector() && !this.namedFilterSelector().saveDisabled && this.getSelectedFilterName()) {
      const res = await Alerts.askSaveBeforeAction(
        this.alertController,
        this.translate,
        {
          valid: true,
          validMessageI18n: 'REFERENTIAL.FILTER.CONFIRM.SAVE_BEFORE_CONTINUE',
        },
        { name: this.getSelectedFilterName() }
      );
      if (!res.confirmed) {
        return;
      }
      if (res.save) {
        await this.namedFilterSelector()?.save(event);
      } else {
        this.clearSelectedFilterName();
      }
    }
    this.close();
    this.table().submitFilter(opts);
  }

  toggle() {
    if (this.filterPanel().expanded) this.close();
    else this.open();
  }

  open() {
    this.filterPanel().open();
  }

  close() {
    this.namedFilterSelector()?.autocompleteField?.closePanel();
    this.filterPanel().close();
  }

  async addNamedFilter(event: UIEvent, opts?: { duplicate?: boolean }): Promise<boolean> {
    let name: string;
    if (opts?.duplicate) {
      const actualName = this.getSelectedFilterName();
      if (isNotNilOrBlank(actualName)) name = await this.getUniqueNamedFilterName(actualName);
    }

    const { role, data } = await this.modalService.openModal<INamedFilterAddOptions, string>(
      NamedFilterAddModal,
      {
        titleI18n: 'COMMON.NAMED_FILTER.NAME',
        entityName: this.entityName,
        name,
      },
      'modal-200'
    );

    if (role === 'validate' && !!data) {
      if (!opts?.duplicate) {
        await this.clearValue();
      }
      this.setSelectedFilterName(data);
      await this.saveNamedFilter(event);
      return true;
    }
    return false;
  }

  async saveNamedFilter(event: UIEvent) {
    if (isNilOrBlank(this.getSelectedFilterName())) {
      // Propose a new named filter with current criterias (like a duplication) (Mantis #63763)
      if (!(await this.addNamedFilter(event, { duplicate: true }))) {
        return;
      }
    }

    await this.namedFilterSelector().save(event);
    this.markAsPristine();
  }

  async deleteNamedFilter() {
    if (await Alerts.askDetailedConfirmation('COMMON.NAMED_FILTER.CONFIRM.DELETE', this.alertController, this.translate)) {
      await this.namedFilterSelector()?.delete();
      await this.clearValue();
    }
  }

  async clearCriterias() {
    if (await Alerts.askDetailedConfirmation('COMMON.NAMED_FILTER.CONFIRM.CLEAR', this.alertController, this.translate)) {
      await this.clearValue();
      this.markAsDirty();
    }
  }

  filterImportCallback = async (namedFilter: NamedFilter): Promise<NamedFilter> => {
    // Force object creation
    namedFilter = NamedFilter.fromObject(namedFilter);

    // Check entityName
    if (namedFilter.entityName !== this.entityName) {
      await Alerts.showError(
        'COMMON.NAMED_FILTER.ERROR.INVALID_ENTITY_NAME',
        this.alertController,
        this.translate,
        { titleKey: 'COMMON.NAMED_FILTER.IMPORT_FAILED' },
        {
          expectedEntityName: this.translate.instant(Utils.toI18nKey(this.entityName)),
          actualEntityName: this.translate.instant(Utils.toI18nKey(namedFilter.entityName)),
        }
      );
      return null;
    }

    // Update name and save it
    return this.updateNamedFilterName(namedFilter);
  };

  protected async updateNamedFilterName(namedFilter: NamedFilter): Promise<NamedFilter> {
    // Get unique name
    if (isNotNilOrBlank(namedFilter.name)) {
      namedFilter.name = await this.getUniqueNamedFilterName(namedFilter.name);
    }
    // Reset id
    namedFilter.id = undefined;
    // Save it
    const saved = await this.namedFilterService.save(NamedFilter.fromObject(namedFilter));
    // Update namedFilter with saved object by calling fromObject to parse filter content from JSON
    namedFilter.fromObject(saved);
    return namedFilter;
  }

  protected getUniqueNamedFilterName(actualName: string): Promise<string> {
    return ReferentialUtils.proposeUniqueName(this.namedFilterService, { entityName: this.entityName }, actualName);
  }

  protected getSelectedFilterName(): string {
    return this.namedFilterSelector()?.value?.namedFilter?.name;
  }

  protected setSelectedFilterName(name: string): void {
    this.namedFilterSelector()?.setValue({ namedFilter: { name, entityName: this.entityName } });
    this.form.patchValue({ name });
  }

  protected async clearValue() {
    // Clear all criterias but preserve selected system
    await this.setValue(this.asFilter({ systemId: this.systemId }), { staticCriteria: this.staticCriteria });
  }

  protected clearSelectedFilterName(): void {
    // Clear filter name
    this.namedFilterSelector()?.autocompleteField.clearValue();
    this.form.patchValue({ name: undefined });
  }

  protected asFilter(source: any): F {
    return this.table().asFilter(source);
  }

  asCriteria(source: any): C {
    return this.asFilter({}).criteriaFromObject(source || {});
  }

  protected createCriteriaFormGroup(criteria: any) {
    const object = this.asCriteria(criteria);
    const config: PartialRecord<keyof C, any> = {};
    Object.keys(object).forEach((key) => {
      const value = object[key];
      if (typeof value !== 'function') {
        if (value instanceof DateFilter) {
          config[key] = this.createFormGroupForCriteria(key as keyof C, () => DateFilterUtils.createFormGroup(this.formBuilder));
        } else if (value instanceof ReferentialFilterCriteria) {
          config[key] = this.createFormGroupForCriteria(key as keyof C, () => this.referentialFilterFormFieldValidator.getFormGroup(value));
        } else {
          config[key] = [
            {
              value: value || null,
              disabled: this.isCriteriaDisabled(key as keyof C),
            },
          ];
        }
      }
    });
    return this.formBuilder.group(config);
  }

  protected createFormGroupForCriteria(criteriaKey: keyof C, supplier: () => UntypedFormGroup): UntypedFormGroup {
    const formGroup = supplier();
    if (this.isCriteriaDisabled(criteriaKey)) formGroup.disable();
    return formGroup;
  }

  protected isCriteriaDisabled(criteriaKey: keyof C): boolean {
    return this.disabledControls.includes(criteriaKey) || this.permanentDisabledControls.includes(criteriaKey);
  }

  /**
   * Disable form controls if the provided criteria has non-empty object
   */
  protected disableControlFromCriteria(criteria: Partial<C>) {
    if (!criteria) return;
    const keys = Object.keys(criteria) as (keyof C)[];
    keys.forEach((key) => {
      const attribute = criteria[key];
      if (typeof attribute === 'object') {
        if (!BaseFilterUtils.isCriteriaEmpty(attribute, true) || !!getPropertyByPath(attribute, 'id')) {
          this.addDisabledControl(key, true);
        }
      } else if (isNotNilOrBlank(attribute)) {
        this.addDisabledControl(key, true);
      }
    });
  }

  addDisabledControl(criteria: keyof C, permanent: boolean) {
    if (permanent) this.permanentDisabledControls.push(criteria);
    else this.disabledControls.push(criteria);
    this.setCriteriaControlState(criteria as string, false);
  }

  removeDisabledControl(criteria: keyof C) {
    if (this.disabledControls.includes(criteria)) {
      this.disabledControls = this.disabledControls.filter((c) => c !== criteria);
      this.setCriteriaControlState(criteria as string, true);
    }
  }

  setCriteriaControlState(name: string, enabled: boolean) {
    this.criteriaControls.forEach((criteriaForm) => {
      const control = criteriaForm.controls[name];
      if (enabled) control?.enable();
      else control?.disable();
    });
  }

  clearControl(criteria: keyof C, propertiesToPreserve?: string[]) {
    this.criteriaControls.forEach((criteriaForm) => {
      const control = criteriaForm.controls[criteria as string];
      if (control) {
        // Keep properties to preserve
        const resetValue = propertiesToPreserve?.reduce((acc, property) => {
          const value = control.value?.[property];
          if (isNotNil(value)) {
            acc[property] = value;
          }
          return acc;
        }, {});
        // Reset control
        control.reset(EntityUtils.isEmptyObject(resetValue) ? null : resetValue, { emitEvent: false });
      }
    });
  }

  protected showAndOpenTranscribingSystemSelect() {
    this.showTranscribingSystemSelect = true;
    this.detectChanges();
    this.transcribingSystemSelect().open();
  }

  protected updateView() {
    if (isNotEmptyArray(this.transcribingSystemIds)) {
      this.showTranscribingSystemSelect = this.showTranscribingSystemSelect || !TranscribingItemTypeUtils.isDefaultSystem(this.value.systemId);
    }
    // Must use a small timeout to allow tabs refreshing correctly
    setTimeout(() => {
      this.selectedTab.set(0);
      this.filterTabs()?.updatePagination();
      this.filterTabs()?._alignInkBarToSelectedTab();
      this.markForCheck();
    }, 10);
  }

  private async loadTranscribingSystems(entityName: string) {
    const transcribingSystemIds = [<TranscribingSystemType>'QUADRIGE']
      .concat(
        (await this.transcribingItemTypeService.loadTranscribingItemTypesByEntityName(entityName))
          .filter((type) => transcribingSystemsForFilter.includes(type.systemId))
          .map((type) => type.systemId)
      )
      .filter(uniqueValue);
    const systems = await this.transcribingItemTypeService.getSystems();
    this.transcribingSystemsByIds = Utils.toRecord(
      systems.filter((system) => transcribingSystemIds.includes(<TranscribingSystemType>system.id)),
      'id'
    );
    // Affect this array at last, because of template update
    this.transcribingSystemIds = transcribingSystemIds;
  }
}
