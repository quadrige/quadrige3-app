import { AsyncTableElement } from '@e-is/ngx-material-table';

export declare type ReadOnlyIfFn = (row: AsyncTableElement<any>) => boolean;
