import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { DisplayFn } from '@sumaris-net/ngx-components';
import { BaseColumn } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-date-column',
  templateUrl: './date-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => DateColumnComponent) }],
})
export class DateColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name!: string;
  @Input() headerI18n: string;
  @Input() headerTitleI18n: string;
  @Input() placeholderI18n: string;
  @Input() readOnly = false;
  @Input() readOnlyIf: ReadOnlyIfFn;
  @Input() displayFn: DisplayFn;
  @Input() format: {
    pattern?: string;
    time?: boolean;
    seconds?: boolean;
  } = { time: false };
  @Input() sortable = true;
  @Input() required = false;
  @Input() autoFocus = false;
  @Input() sticky = false;
  @Input() draggable = true;

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    super();
    if (!matTable) {
      throw new Error(`[date-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    if (!this.readOnlyIf) {
      this.readOnlyIf = (row) => !row.validator; // default row is read only if no validator
    }
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
