import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { BaseColumn } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-select-column',
  templateUrl: './select-column.component.html',
})
export class SelectColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    super();
    if (!matTable) {
      throw new Error(`[select-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);

    if (!this.table) {
      throw new Error(`[select-column] this column component must be provide a appTable`);
    }
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
