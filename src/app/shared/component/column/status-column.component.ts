import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { StatusById, StatusList } from '@sumaris-net/ngx-components';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { BaseColumn, ColumnWidth } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-status-column',
  templateUrl: './status-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => StatusColumnComponent) }],
})
export class StatusColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name = 'statusId';
  @Input() placeholderI18n = 'REFERENTIAL.STATUS_ID';
  @Input() readOnly = false;
  @Input() width: ColumnWidth; // no default value

  statusList = StatusList;
  statusById = StatusById;

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    super();
    if (!matTable) {
      throw new Error(`[status-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
