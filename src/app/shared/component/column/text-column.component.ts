import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DisplayFn, isNilOrBlank, isNotNil, isNotNilOrBlank, TextPopover, TextPopoverOptions, toBoolean } from '@sumaris-net/ngx-components';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { TranslateService } from '@ngx-translate/core';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { lengthComment, lengthDescription } from '@app/referential/service/referential-validator.service';
import { PopoverController } from '@ionic/angular';
import { BaseColumn, ColumnWidth } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-text-column',
  templateUrl: './text-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => TextColumnComponent) }],
})
export class TextColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name!: string;
  @Input() headerI18n: string;
  @Input() headerTitleI18n: string;
  @Input() placeholderI18n: string;
  @Input() notUniqueErrorI18n: string;
  @Input() modalPlaceholderI18n: string;
  @Input() readOnly = false;
  @Input() readOnlyIf: ReadOnlyIfFn;
  @Input() displayFn: DisplayFn;
  @Input() sortable = true;
  @Input() resizable = true;
  @Input() required = false;
  @Input() autoFocus = false;
  @Input() sticky = false;
  @Input() draggable = true;
  @Input() maxLength: number;
  @Input() showModalButton: boolean;
  @Input() modalButtonOnly: boolean;
  @Input() width: ColumnWidth = 150;

  protected logPrefix = '[text-column]';

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef,
    private translate: TranslateService,
    private popoverController: PopoverController
  ) {
    super();
    if (!matTable) {
      throw new Error(`${this.logPrefix} this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    if (!this.readOnlyIf) {
      this.readOnlyIf = (row) => !row.validator; // default row is read only if no validator
    }
    // Compute the 'not unique error message' depending on column name
    if (!this.notUniqueErrorI18n) {
      const key = `ERROR.FIELD_NOT_UNIQUE_${this.name.toUpperCase()}`;
      const result = this.translate.instant(key);
      this.notUniqueErrorI18n = result !== key ? key : 'ERROR.FIELD_NOT_UNIQUE';
    }
    // Default behaviors
    this.showModalButton = toBoolean(this.showModalButton, this.name === 'description');
    this.modalButtonOnly = toBoolean(this.modalButtonOnly, this.name === 'comments');
    // Default max length (fixme: may be useless because lengthComment === lengthDescription since Mantis #65242)
    if (!this.maxLength) {
      if (this.name === 'comments') this.maxLength = lengthComment;
      else this.maxLength = lengthDescription;
    }

    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }

  async openModal(event: UIEvent, row: AsyncTableElement<any>) {
    if (this.modalButtonOnly) {
      if (this.table) {
        // Let the row enter in edition (Mantis #54582)
        if (!this.table.checkBoxSelection) await this.table.clickRow(undefined, row);
      } else {
        console.warn(`${this.logPrefix} BaseTable not found, can't select the row !`);
      }
    }
    const readOnly = this.readOnly || !row.editing || !row.validator || false;
    // Get the 'name' or 'id' attribute to build popover placeholder
    let name = row.validator?.controls.name?.value || row.currentData['name'];
    if (isNilOrBlank(name)) name = row.validator?.controls.id?.value || row.currentData['id'];

    const placeholder: string = this.translate.instant(
      this.modalPlaceholderI18n ?? `COMMON.MODAL.${this.name.toUpperCase()}.${isNotNilOrBlank(name) ? 'PLACEHOLDER_WITH_NAME' : 'PLACEHOLDER'}`,
      { name }
    );
    const previousValue = row.currentData[this.name];
    const modal = await this.popoverController.create({
      component: TextPopover,
      componentProps: <TextPopoverOptions>{
        placeholder,
        text: previousValue,
        editing: !readOnly,
        autofocus: true,
        multiline: true,
        maxLength: this.maxLength,
      },
      backdropDismiss: false,
      keyboardClose: false,
      event,
      translucent: true,
      cssClass: 'popover-comment',
    });

    await modal.present();
    const { data } = await modal.onDidDismiss();

    if (!readOnly && isNotNil(data) && data !== previousValue) {
      row.validator.patchValue({ [this.name]: data }, { onlySelf: false, emitEvent: true });
      if (this.table) {
        // Mark current row as dirty
        this.table.markRowAsDirty(row);
      } else {
        console.warn(`${this.logPrefix} BaseTable not found, can't mark row as dirty !`);
      }
    }
  }
}
