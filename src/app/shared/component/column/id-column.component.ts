import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { FormBuilder } from '@angular/forms';
import { BaseColumn, ColumnWidth } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-id-column',
  templateUrl: './id-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => IdColumnComponent) }],
})
export class IdColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name = 'id';
  @Input() property: string;
  @Input() headerI18n: string;
  @Input() headerTitleI18n: string;
  @Input() placeholderI18n: string;
  @Input() idIsString = false;
  @Input() readOnly = false;
  @Input() readOnlyIf: ReadOnlyIfFn;
  @Input() required = false;
  @Input() sortable = true;
  @Input() resizable = true;
  @Input() hidden = false;
  @Input() width: ColumnWidth; // no default value

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef,
    protected formBuilder: FormBuilder
  ) {
    super();
    if (!matTable) {
      throw new Error(`[id-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    if (!this.readOnlyIf) {
      this.readOnlyIf = (row) => !row.validator; // default row is read only if no validator
    }
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
