import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatAutocompleteField, MatAutocompleteFieldConfig } from '@sumaris-net/ngx-components';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { BaseColumn, ColumnWidth } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-combo-column',
  templateUrl: './combo-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => ComboColumnComponent) }],
})
export class ComboColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;
  @ViewChild(MatAutocompleteField) autocompleteField: MatAutocompleteField;

  @Input() name: string;
  @Input() property: string;
  @Input() headerI18n: string;
  @Input() placeholderI18n: string;
  @Input() config: MatAutocompleteFieldConfig;
  @Input() attributes: string[];
  @Input() readOnly = false;
  @Input() readOnlyIf: ReadOnlyIfFn;
  @Input() sortable = true;
  @Input() resizable = true;
  @Input() required = false;
  @Input() loading = false;
  @Input() autoFocus = false;
  @Input() panelWidth = autocompleteWidthExtraLarge;
  @Input() width: ColumnWidth = 150;

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    super();
    if (!matTable) {
      throw new Error(`[combo-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    if (!this.readOnlyIf) {
      this.readOnlyIf = (row) => !row.validator; // default row is read only if no validator
    }
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
