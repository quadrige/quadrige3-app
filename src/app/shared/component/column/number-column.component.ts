import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DisplayFn } from '@sumaris-net/ngx-components';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { ReadOnlyIfFn } from '@app/shared/component/column/column.model';
import { BaseColumn, ColumnWidth } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-number-column',
  templateUrl: './number-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => NumberColumnComponent) }],
})
export class NumberColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name!: string;
  @Input() headerI18n: string;
  @Input() headerTitleI18n: string;
  @Input() placeholderI18n: string;
  @Input() readOnly = false;
  @Input() readOnlyIf: ReadOnlyIfFn;
  @Input() displayFn: DisplayFn;
  @Input() format: Intl.NumberFormatOptions;
  @Input() sortable = true;
  @Input() resizable = true;
  @Input() required = false;
  @Input() autoFocus = false;
  @Input() sticky = false;
  @Input() draggable = true;
  @Input() width: ColumnWidth = 100;

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    super();
    if (!matTable) {
      throw new Error(`[number-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    if (!this.readOnlyIf) {
      this.readOnlyIf = (row) => !row.validator; // default row is read only if no validator
    }
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
