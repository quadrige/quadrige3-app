import { Directive, Input, ViewChild } from '@angular/core';
import { ResizableComponent } from '@sumaris-net/ngx-components';
import { IBaseTable } from '@app/shared/table/table.model';

export type ColumnWidth = 50 | 100 | 110 | 150 | 200 | 300; // 150 is the actual default for almost all base columns

@Directive()
export abstract class BaseColumn {
  @Input() table: IBaseTable<any, any>;
  @ViewChild(ResizableComponent) resizableColumn: ResizableComponent;

  protected constructor() {}
}
