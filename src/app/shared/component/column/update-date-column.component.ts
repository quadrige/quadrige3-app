import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { BaseColumn } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-update-date-column',
  templateUrl: './update-date-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => UpdateDateColumnComponent) }],
})
export class UpdateDateColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name = 'updateDate';
  @Input() i18nColumnPrefix = 'REFERENTIAL.';
  @Input() sortable = true;
  @Input() draggable = true;

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    super();
    if (!matTable) {
      throw new Error(`[update-date-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
