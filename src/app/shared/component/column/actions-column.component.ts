import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { AsyncTableElement } from '@e-is/ngx-material-table';

@Component({
  selector: 'app-row-actions-column',
  templateUrl: './actions-column.component.html',
})
export class ActionsColumnComponent implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() hidden = false;
  @Input() undoOnly = false;
  @Output() headerClick = new EventEmitter<UIEvent>();
  @Output() undoOrDeleteRow = new EventEmitter<{ event: UIEvent; row: AsyncTableElement<any> }>();

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    if (!matTable) {
      throw new Error(`[update-date-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
