import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { months } from 'moment/moment';
import { capitalizeFirstLetter } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { BaseColumn } from '@app/shared/component/column/base-column.class';

@Component({
  selector: 'app-month-column',
  templateUrl: './month-column.component.html',
  providers: [{ provide: BaseColumn, useExisting: forwardRef(() => MonthColumnComponent) }],
})
export class MonthColumnComponent extends BaseColumn implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name: string;
  @Input() headerI18n: string;
  @Input() placeholderI18n: string;
  @Input() readOnly = false;
  @Input() clearable = true;

  months = months().map((value) => capitalizeFirstLetter(value));

  constructor(
    public matTable: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    super();
    if (!matTable) {
      throw new Error(`[month-column] this column component must be inside a MatTable`);
    }
  }

  ngOnInit(): void {
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  clearValue(event: UIEvent, row: AsyncTableElement<any>) {
    row.validator.patchValue({ [this.name]: undefined });
    row.validator.markAsDirty();
    event.stopPropagation();
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
