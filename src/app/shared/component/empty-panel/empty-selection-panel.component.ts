import { Component, input } from '@angular/core';
import { SharedModule } from '@sumaris-net/ngx-components';
import { ToI18nKeyPipe } from '@app/shared/pipes/toI18n.pipe';

@Component({
  selector: 'app-empty-selection-panel',
  standalone: true,
  imports: [SharedModule, ToI18nKeyPipe],
  templateUrl: './empty-selection-panel.component.html',
})
export class EmptySelectionPanelComponent {
  titleI18n = input<string>();
  entityNameI18n = input<string>();
  entityName = input<string>();
  multiple = input<boolean>(false);
}
