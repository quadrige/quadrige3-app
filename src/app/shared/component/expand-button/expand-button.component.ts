import { Component, EventEmitter, Input, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { EXPANSION_PANEL_ANIMATION_TIMING } from '@angular/material/expansion';

export type ExpandButtonDirection = 'leftRight' | 'upDown';

@Component({
  selector: 'app-expand-button',
  templateUrl: './expand-button.component.html',
  styleUrls: ['./expand-button.component.scss'],
  animations: [
    trigger('expanded', [
      state('true', style({ rotate: '180deg' })),
      state('false', style({ rotate: '0deg' })),
      transition('false <=> true', animate(EXPANSION_PANEL_ANIMATION_TIMING)),
    ]),
  ],
})
export class ExpandButtonComponent {
  @Input() visible = true;
  @Input() direction: ExpandButtonDirection = 'leftRight';
  @Input() expanded = false;
  @Input() accent = false;
  @Input() titleI18n = '';
  @Input() noRipple = false;

  @Output() toggle = new EventEmitter<UIEvent>();

  constructor() {}
}
