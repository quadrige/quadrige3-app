import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-table-footer',
  templateUrl: './table-footer.component.html',
  styleUrls: ['./table-footer.component.scss'],
})
export class TableFooterComponent {
  @Input() totalTitleI18n = 'COMMON.ROM_COUNT';
  @Input() totalRowCount: number;
}
