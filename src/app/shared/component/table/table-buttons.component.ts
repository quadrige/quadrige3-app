import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-table-buttons',
  templateUrl: './table-buttons.component.html',
  styleUrls: ['./table-buttons.component.scss'],
})
export class TableButtonsComponent {
  @Input() disabled = false;
  @Input() addDisabled = false;
  @Input() multiEditDisabled = false;
  @Input() duplicateDisabled = false;
  @Input() deleteDisabled = false;
  @Input() selection: SelectionModel<any>;
  @Input() canFilter = true;
  @Input() canEdit = false;
  @Input() canAdd = true;
  @Input() canMultiEdit = false;
  @Input() canDuplicate = false;
  @Input() canDelete = true;
  @Input() canExport = false;
  @Input() filterCriteriaCount = 0;
  @Input() inlineEdition = true;
  @Input() dirty = false;
  @Input() addTitleI18n: string;
  @Input() multiEditTitleI18n: string;
  @Input() duplicateTitleI18n: string;
  @Input() deleteTitleI18n: string;
  @Input() filterTitleI18n: string;
  @Input() unfilterTitleI18n: string;
  @Input() exportTitleI18n: string;

  @Output() add = new EventEmitter<any>();
  @Output() multiEdit = new EventEmitter<any>();
  @Output() duplicate = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  @Output() showFilter = new EventEmitter<any>();
  @Output() clearFilter = new EventEmitter<any>();
  @Output() export = new EventEmitter<any>();

  constructor() {}
}
