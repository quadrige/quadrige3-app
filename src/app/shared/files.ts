import { FileEvent, FileResponse, FilesUtils } from '@sumaris-net/ngx-components';
import { HttpEventType } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

export class FileUtils extends FilesUtils {
  static readAsArrayBuffer(file: File): Observable<FileEvent<ArrayBuffer>> {
    const $progress = new Subject<FileEvent<ArrayBuffer>>();
    const reader = new FileReader();

    // Listen progress
    reader.onprogress = (e: ProgressEvent) => {
      const total = e.total || 1;
      const loaded = e.total || 0;
      if (loaded < total) $progress.next({ type: HttpEventType.UploadProgress, loaded, total });
    };

    // Listen end
    reader.onloadend = (e: ProgressEvent) => {
      const body = reader.result;
      if (typeof body === 'string') {
        console.error(`File '${file.name}' is a text file, use readAsText instead !`);
        $progress.error(`Cannot read file as ArrayBuffer`);
      } else {
        $progress.next(new FileResponse({ body }));
        $progress.complete();
      }
    };

    // Listen error
    reader.onerror = (event: ProgressEvent) => {
      console.error('[files] Error while reading file:', event);
      $progress.error(event);
    };

    // Start reading the file
    reader.readAsArrayBuffer(file);

    return $progress.asObservable();
  }
}
