import { BaseEntityFilter, BaseEntityFilterCriteria, IBaseEntityFilter, IBaseEntityFilterCriteria } from '@app/shared/model/filter.model';
import { EntityAsObjectOptions, FilterFn, isNil, isNotEmptyArray, isNotNil } from '@sumaris-net/ngx-components';
import { EntityUtils } from '@app/shared/entity.utils';
import { DataEntity } from '@app/data/model/data.model';

export interface IDataFilterCriteria extends IBaseEntityFilterCriteria<number> {
  parentId?: number;
  recorderDepartmentId?: number;
}

export abstract class DataFilterCriteria<E extends DataEntity<any>> extends BaseEntityFilterCriteria<E, number> implements IDataFilterCriteria {
  parentId?: number = null;
  recorderDepartmentId?: number = null;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.parentId = source.parentId;
    this.recorderDepartmentId = source.recorderDepartmentId;
  }

  protected buildFilter(): FilterFn<E>[] {
    const filterFns = super.buildFilter() || [];

    // todo handle parentId ?

    if (this.recorderDepartmentId) {
      filterFns.push((entity) => this.recorderDepartmentId === entity.recorderDepartmentId);
    }

    // Filter included/excluded ids
    if (isNotEmptyArray(this.includedIds)) {
      filterFns.push((entity) => isNotNil(entity.id) && this.includedIds.includes(entity.id));
    }
    if (isNotEmptyArray(this.excludedIds)) {
      filterFns.push((entity) => isNil(entity.id) || !this.excludedIds.includes(entity.id));
    }

    const searchTextFilter = EntityUtils.searchTextFilter(this.searchAttributes, this.searchText);
    if (searchTextFilter) filterFns.push(searchTextFilter);

    return filterFns;
  }
}

export type IDataDataFilter<C extends IDataFilterCriteria> = IBaseEntityFilter<number, C>;

export abstract class DataFilter<
    F extends BaseEntityFilter<F, C, E, number, AO, FO>,
    C extends DataFilterCriteria<E>,
    E extends DataEntity<E, AO>,
    AO extends EntityAsObjectOptions = EntityAsObjectOptions,
    FO = any,
  >
  extends BaseEntityFilter<F, C, E, number, AO, FO>
  implements IDataDataFilter<C>
{
  asObject(opts?: AO): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }
}
