import { Entity, EntityAsObjectOptions, fromDateISOString, toDateISOString } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';

export interface DataAsObjectOptions extends EntityAsObjectOptions {
  _placeOtherOptionsHere?: any;
}

export abstract class DataEntity<E extends DataEntity<any, AO>, AO extends DataAsObjectOptions = DataAsObjectOptions, FO = any> extends Entity<
  E,
  number,
  AO,
  FO
> {
  recorderDepartmentId: number = null;

  controlDate: Moment = null;
  validationDate: Moment = null;
  qualificationDate: Moment = null;
  qualificationComment: string = null;
  qualityFlagId: string = null;

  fromObject(source: any, opts?: FO) {
    super.fromObject(source, opts);
    this.recorderDepartmentId = source.recorderDepartmentId;
    this.controlDate = fromDateISOString(source.controlDate);
    this.validationDate = fromDateISOString(source.validationDate);
    this.qualificationDate = fromDateISOString(source.qualificationDate);
    this.qualificationComment = source.qualificationComment;
    this.qualityFlagId = source.qualityFlagId;
  }

  asObject(opts?: AO): any {
    const target = super.asObject(opts);
    target.controlDate = toDateISOString(this.controlDate);
    target.validationDate = toDateISOString(this.validationDate);
    target.qualificationDate = toDateISOString(this.qualificationDate);
    return target;
  }
}
