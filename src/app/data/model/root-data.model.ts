import { fromDateISOString, toDateISOString } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { DataAsObjectOptions, DataEntity } from '@app/data/model/data.model';

export abstract class RootData<E extends RootData<any, AO>, AO extends DataAsObjectOptions = DataAsObjectOptions, FO = any> extends DataEntity<
  E,
  AO,
  FO
> {
  creationDate: Moment = null;
  comments: string = null;
  programIds: string[] = null;

  fromObject(source: any, opts?: FO) {
    super.fromObject(source, opts);
    this.creationDate = fromDateISOString(source.creationDate);
    this.comments = source.comments;
    this.programIds = source.programIds || [];
  }

  asObject(opts?: AO): any {
    const target = super.asObject(opts);
    target.creationDate = toDateISOString(this.creationDate);
    target.programIds = this.programIds || undefined;
    return target;
  }
}
