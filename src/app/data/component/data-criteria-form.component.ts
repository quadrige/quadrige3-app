import { Directive, QueryList, ViewChildren } from '@angular/core';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import { autocompleteWidthExtraLarge } from '@app/shared/constants';
import { DataFilter, DataFilterCriteria } from '@app/data/model/data.filter.model';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

/**
 * Base component for data table filter templates
 * Should be in shared directory but still use a reference to a referential component
 */
@Directive()
export abstract class DataCriteriaFormComponent<F extends DataFilter<F, C, any>, C extends DataFilterCriteria<any>> extends BaseCriteriaFormComponent<
  F,
  C
> {
  @ViewChildren(ReferentialFilterFormField) referentialFilterFormFields: QueryList<ReferentialFilterFormField>;

  autocompleteWidthExtraLarge = autocompleteWidthExtraLarge;

  protected constructor() {
    super();
  }

  async setValue(value: F, opts?: ISetFilterOptions<C>) {
    await super.setValue(value, opts);
    // Render referential filter for fields
    setTimeout(() => this.referentialFilterFormFields?.forEach((item) => item.onBlur()), 100);
  }
}
