import { Injectable, Injector } from '@angular/core';
import { BaseDataReadOnlyService } from '@app/data/service/base-data-read.service';
import { Survey } from '@app/data/survey/survey.model';
import { BaseEntityGraphqlQueries } from '@sumaris-net/ngx-components';
import gql from 'graphql-tag';
import { referentialFragments } from '@app/referential/service/base-referential.service';
import { SurveyFilter, SurveyFilterCriteria } from '@app/data/survey/filter/survey.filter.model';

const surveyFragments = {
  survey: gql`
    fragment SurveyFragment on SurveyVO {
      id
      date
      time
      utFormat
      label
      comments
      programIds
      monitoringLocation {
        ...ReferentialFragment
      }
      userIds
      creationDate
      updateDate
      controlDate
      validationDate
      qualificationDate
      qualificationComment
      qualityFlagId
      recorderDepartmentId
    }
    ${referentialFragments.light}
  `,
};

const queries: BaseEntityGraphqlQueries = {
  load: gql`
    query Survey($id: Int) {
      data: survey(id: $id) {
        ...SurveyFragment
      }
    }
    ${surveyFragments.survey}
  `,
  loadAll: gql`
    query Surveys($page: PageInput, $filter: SurveyFilterVOInput) {
      data: surveys(page: $page, filter: $filter) {
        ...SurveyFragment
      }
    }
    ${surveyFragments.survey}
  `,
  loadAllWithTotal: gql`
    query SurveysWithTotal($page: PageInput, $filter: SurveyFilterVOInput) {
      data: surveys(page: $page, filter: $filter) {
        ...SurveyFragment
      }
      total: surveysCount(filter: $filter)
    }
    ${surveyFragments.survey}
  `,
  countAll: gql`
    query SurveysCount($filter: SurveyFilterVOInput) {
      total: surveysCount(filter: $filter)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class SurveyService extends BaseDataReadOnlyService<Survey, SurveyFilter, SurveyFilterCriteria> {
  constructor(protected injector: Injector) {
    super(injector, Survey, SurveyFilter, { queries });
    this._logPrefix = '[survey-service]';
  }
}
