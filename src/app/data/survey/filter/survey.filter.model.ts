import { EntityAsObjectOptions, EntityClass } from '@sumaris-net/ngx-components';
import { DateFilter } from '@app/shared/model/date-filter';
import { IntReferentialFilterCriteria, StrReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Survey } from '@app/data/survey/survey.model';
import { DataFilter, DataFilterCriteria } from '@app/data/model/data.filter.model';
import { PmfmuFilterCriteria } from '@app/referential/pmfmu/filter/pmfmu.filter.model';

@EntityClass({ typename: 'SurveyFilterCriteriaVO' })
export class SurveyFilterCriteria extends DataFilterCriteria<Survey> {
  static fromObject: (source: any, opts?: any) => SurveyFilterCriteria;

  programFilter: StrReferentialFilterCriteria = null;
  monitoringLocationFilter: IntReferentialFilterCriteria = null;
  campaignFilter: IntReferentialFilterCriteria = null;
  occasionFilter: IntReferentialFilterCriteria = null;
  dateFilter: DateFilter = null; // fixme: may be deprecated, use dateFilters
  dateFilters: DateFilter[] = null;
  pmfmuFilters: PmfmuFilterCriteria[] = null;
  multiProgramOnly: boolean = null;
  inheritedGeometry: boolean = null;

  constructor() {
    super(SurveyFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.programFilter = StrReferentialFilterCriteria.fromObject(source.programFilter || {});
    this.monitoringLocationFilter = IntReferentialFilterCriteria.fromObject(source.monitoringLocationFilter || {});
    this.campaignFilter = IntReferentialFilterCriteria.fromObject(source.campaignFilter || {});
    this.occasionFilter = IntReferentialFilterCriteria.fromObject(source.occasionFilter || {});
    this.dateFilter = DateFilter.fromObject(source.dateFilter || {});
    this.dateFilters = source.dateFilters?.map((dateFilter: any) => DateFilter.fromObject(dateFilter)) || [];
    this.pmfmuFilters = source.pmfmuFilters?.map((pmfmuFilter: any) => PmfmuFilterCriteria.fromObject(pmfmuFilter)) || [];
    this.multiProgramOnly = source.multiProgramOnly;
    this.inheritedGeometry = source.inheritedGeometry;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.programFilter = this.programFilter?.asObject(opts);
    target.monitoringLocationFilter = this.monitoringLocationFilter?.asObject(opts);
    target.campaignFilter = this.campaignFilter?.asObject(opts);
    target.occasionFilter = this.occasionFilter?.asObject(opts);
    target.dateFilter = this.dateFilter?.asObject(opts);
    target.dateFilters = this.dateFilters?.map((dateFilter) => dateFilter.asObject(opts));
    target.pmfmuFilters = this.pmfmuFilters?.map((pmfmuFilter) => pmfmuFilter.asObject(opts));
    target.multiProgramOnly = this.multiProgramOnly || false;
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any, isSubFilter?: boolean): boolean {
    //     if (key === 'parentFilter') return this.parentFilter && !this.parentFilter.isEmpty(true);
    return super.isCriteriaNotEmpty(key, value, isSubFilter);
  }
}

@EntityClass({ typename: 'SurveyFilterVO' })
export class SurveyFilter extends DataFilter<SurveyFilter, SurveyFilterCriteria, Survey> {
  static fromObject: (source: any, opts?: any) => SurveyFilter;

  criteriaFromObject(source: any, opts: any): SurveyFilterCriteria {
    return SurveyFilterCriteria.fromObject(source, opts);
  }
}
