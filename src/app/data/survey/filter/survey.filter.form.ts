import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SurveyFilter, SurveyFilterCriteria } from '@app/data/survey/filter/survey.filter.model';
import { DataCriteriaFormComponent } from '@app/data/component/data-criteria-form.component';
import { attributes } from '@app/referential/model/referential.constants';

@Component({
  selector: 'app-survey-filter-form',
  templateUrl: './survey.filter.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SurveyFilterForm extends DataCriteriaFormComponent<SurveyFilter, SurveyFilterCriteria> {
  constructor() {
    super();
  }

  protected getSearchAttributes(): string[] {
    return attributes.idLabel;
  }
}
