import { Injectable } from '@angular/core';
import { DataValidatorService } from '@app/data/service/data-validator.service';
import { Survey } from '@app/data/survey/survey.model';
import { UntypedFormBuilder } from '@angular/forms';
import { toNumber } from '@sumaris-net/ngx-components';

@Injectable({ providedIn: 'root' })
export class SurveyValidatorService extends DataValidatorService<Survey> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: Survey, opts?: any): { [p: string]: any } {
    return {
      id: [data?.id || null],
      label: [data?.label || null],
      date: [data?.date || null],
      time: [data?.time || null],
      utFormat: [toNumber(data?.utFormat)],
      monitoringLocation: [data?.monitoringLocation || null],
      userIds: [data?.userIds || null],
      programIds: [data?.programIds || null],
      comments: [data?.comments || null],
      recorderDepartmentId: [data?.recorderDepartmentId || null],
      controlDate: [data?.controlDate || null],
      validationDate: [data?.validationDate || null],
      qualificationDate: [data?.qualificationDate || null],
      qualificationComment: [data?.qualificationComment || null],
      qualityFlagId: [data?.qualityFlagId || null],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
    };
  }
}
