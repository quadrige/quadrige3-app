import { EntityClass, fromDateISOString } from '@sumaris-net/ngx-components';
import { RootData } from '@app/data/model/root-data.model';
import { Moment } from 'moment';
import { DataAsObjectOptions } from '@app/data/model/data.model';
import { Dates } from '@app/shared/dates';
import { IntReferential } from '@app/referential/model/referential.model';
import { EntityUtils } from '@app/shared/entity.utils';
import { Times } from '@app/shared/times';

@EntityClass({ typename: 'SurveyVO' })
export class Survey extends RootData<Survey> {
  static fromObject: (source: any, opts?: any) => Survey;
  date: Moment = null;
  time: string = null;
  utFormat: number = null;
  label: string = null;
  monitoringLocation: IntReferential = null;
  userIds: number[] = null;

  constructor() {
    super(Survey.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.date = fromDateISOString(source.date);
    this.time = Times.secondsToString(source.time);
    this.utFormat = source.utFormat;
    this.label = source.label;
    this.monitoringLocation = IntReferential.fromObject(source.monitoringLocation);
    this.userIds = source.userIds;
  }

  asObject(opts?: DataAsObjectOptions): any {
    const target = super.asObject(opts);
    target.date = Dates.toLocalDateString(this.date);
    target.time = Times.stringToSeconds(this.time);
    target.monitoringLocation = EntityUtils.asMinifiedObject(this.monitoringLocation, opts);
    target.userIds = this.userIds || undefined;
    return target;
  }
}
