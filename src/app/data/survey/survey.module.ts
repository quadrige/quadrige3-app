import { NgModule } from '@angular/core';
import { ReferentialModule } from '@app/referential/referential.module';
import { SurveyTable } from '@app/data/survey/survey.table';
import { SurveyFilterForm } from '@app/data/survey/filter/survey.filter.form';

@NgModule({
  imports: [ReferentialModule], // wrong import, should be DataModule, but it's ok for now
  declarations: [SurveyTable, SurveyFilterForm],
  exports: [SurveyTable, SurveyFilterForm],
})
export class SurveyModule {}
