import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit } from '@angular/core';
import { PromiseEvent, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { DataTable } from '@app/data/table/data.table';
import { Survey } from '@app/data/survey/survey.model';
import { SurveyValidatorService } from '@app/data/survey/survey.validator';
import { SurveyService } from '@app/data/survey/survey.service';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { SurveyFilter, SurveyFilterCriteria } from '@app/data/survey/filter/survey.filter.model';

@Component({
  selector: 'app-survey-table',
  templateUrl: './survey.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SurveyTable extends DataTable<Survey, SurveyFilter, SurveyFilterCriteria, SurveyValidatorService> implements OnInit, AfterViewInit {
  constructor(
    protected injector: Injector,
    protected _entityService: SurveyService,
    protected validatorService: SurveyValidatorService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat([
        'label',
        'date',
        'time',
        'utFormat',
        'monitoringLocation',
        /*'comments', make openCommentModal generic */
        /* add other dates */
        'creationDate',
        'updateDate',
      ]).concat(RESERVED_END_COLUMNS),
      Survey,
      _entityService,
      validatorService
    );

    this.titleI18n = 'DATA.ENTITY.SURVEY';
    this.i18nColumnPrefix = 'DATA.SURVEY.';
    this.logPrefix = '[survey-table]';
  }

  ngOnInit() {
    super.ngOnInit();

    // department combo
    this.registerAutocompleteField('monitoringLocation', {
      ...this.referentialOptions.monitoringLocation,
      service: this.referentialGenericService,
      filter: GenericReferentialFilter.fromObject({
        entityName: 'MonitoringLocation',
        criterias: [this.defaultReferentialCriteria],
      }),
    });

    // this.onBeforeDeleteRows.subscribe((event) => this.checkCanDeleteSurveys(event));
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  async onAfterSelectionChange(row?: AsyncTableElement<Survey>): Promise<void> {
    this.refreshButtons();
    await super.onAfterSelectionChange(row);
  }

  // updatePermission() {
  //   // Manage user rights :
  //   // By default, all user have edit write to allow further finer checks
  //   this.canEdit = true;
  //   // Only admins can add survey
  //   this.tableButtons.canAdd = this.accountService.isAdmin();
  //   // Refresh other controls
  //   this.refreshButtons();
  //   this.markForCheck();
  // }

  async editRow(event: MouseEvent | undefined, row: AsyncTableElement<Survey>): Promise<boolean> {
    // following lines comes from super.editRow(event, row), don't remove
    if (!this._enabled) return false;
    if (this.singleEditingRow === row) return true; // Already the edited row
    if (event?.defaultPrevented) return false;
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // User without right to edit, don't turn on row edition
    if (!this.hasRightOnSurvey(row.currentData)) {
      return true;
    }

    return super.editRow(event, row);
  }

  async checkCanDeleteSurveys(event: PromiseEvent<boolean, { rows: AsyncTableElement<Survey>[] }>) {
    // todo
    // const ids: string[] = EntityUtils.ids(event.detail.rows);
    // let canDelete = true;
    // if (ids.length) {
    //   canDelete = await this.entityService.canDeleteSurveys(ids);
    // }
    // event.detail.success(canDelete);
    // if (!canDelete) {
    //   await Alerts.showError('REFERENTIAL.SURVEY.ERROR.CANNOT_DELETE', this.alertCtrl, this.translate, {
    //     titleKey: 'ERROR.CANNOT_DELETE',
    //   });
    // }
  }

  // protected methods

  protected hasRightOnSurvey(survey: Survey): boolean {
    if (!survey) {
      return false;
    }

    // Admins and users with manager right can edit
    return this.accountService.isAdmin();
    // todo for now only admin have permission
    // || (this.accountService.isUser() && (survey.managerUserIds.includes(this.accountService.person.id) || survey.managerDepartmentIds.includes(this.accountService.department.id)))
  }

  /**
   * Refresh buttons state according to user profile or survey rights
   *
   * @protected
   */
  protected refreshButtons() {
    const canEdit = (this.selection.hasValue() && this.selection.selected.every((row) => this.hasRightOnSurvey(row.currentData))) || false;

    this.tableButtons.canDelete = canEdit;
    // this.tableButtons.canDuplicate = canEdit; todo duplication not implemented yet
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('label', 'date', 'time', 'monitoringLocation');
  }
}
