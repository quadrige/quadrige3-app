import { BaseValidatorService } from '@app/shared/service/base-validator.service';
import { DataEntity } from '@app/data/model/data.model';

export abstract class DataValidatorService<E extends DataEntity<E>, O = any> extends BaseValidatorService<E, O> {}
