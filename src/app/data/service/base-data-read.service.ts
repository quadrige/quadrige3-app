import { Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { errorCodes } from '@app/shared/errors';
import { FetchPolicy } from '@apollo/client/core';
import { SortDirection } from '@angular/material/sort';
import {
  BaseEntityGraphqlQueries,
  BaseEntityGraphqlSubscriptions,
  BaseEntityService,
  BaseEntityServiceOptions,
  EntitiesServiceWatchOptions,
  EntityServiceLoadOptions,
  ENVIRONMENT,
  IEntityService,
  isNotEmptyArray,
  LoadResult,
  Page,
  PlatformService,
  toBoolean,
} from '@sumaris-net/ngx-components';
import { Injector } from '@angular/core';
import { BaseEntityGraphqlMutations, EntitySaveOptions } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { EntityUtils } from '@app/shared/entity.utils';
import { DataEntity } from '@app/data/model/data.model';
import { Job } from '@app/social/job/job.model';
import { StableGraphqlService } from '@app/shared/service/stable-graphql.service';
import { IBaseEntitiesService, IExportContext } from '@app/shared/service/base-entity.service';
import { DataFilter, DataFilterCriteria } from '@app/data/model/data.filter.model';

export interface IDataService<E, F, C, ID, WO, LO> extends IBaseEntitiesService<E, F, WO>, IEntityService<E, ID, LO> {
  exists(criteria: C): Promise<boolean>;

  watchPage(page: Partial<Page>, filter: F, opts?: WO): Observable<LoadResult<E>>;

  loadPage(page: Partial<Page>, filter: Partial<F>, opts?: LO): Promise<LoadResult<E>>;
}

export abstract class BaseDataReadOnlyService<
    E extends DataEntity<E>,
    F extends DataFilter<F, C, E>,
    C extends DataFilterCriteria<E>,
    WO extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions,
    LO extends EntityServiceLoadOptions = EntityServiceLoadOptions,
    Q extends BaseEntityGraphqlQueries = BaseEntityGraphqlQueries,
    M extends BaseEntityGraphqlMutations = BaseEntityGraphqlMutations,
    S extends BaseEntityGraphqlSubscriptions = BaseEntityGraphqlSubscriptions,
  >
  extends BaseEntityService<E, F, number, WO, LO, Q, M, S>
  implements IDataService<E, F, C, number, WO, LO>
{
  protected constructor(
    protected injector: Injector,
    protected dataType: new () => E,
    protected filterType: new () => F,
    options: BaseEntityServiceOptions<E, number, Q, M, S>
  ) {
    super(injector.get(StableGraphqlService), injector.get(PlatformService), dataType, filterType, {
      equals: (e1, e2) => this.equals(e1, e2),
      ...options,
    });

    this._logPrefix = '[base-data-read-only-service]'; // Should be override by implementation class
    this._debug = toBoolean(!injector.get(ENVIRONMENT)?.production, true);
  }

  asFilter(source: any): F {
    return super.asFilter(source || {});
  }

  /** @deprecated use watchPage */
  watchAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<F>,
    opts?: WO & { query?: any }
  ): Observable<LoadResult<E>> {
    return this.watchPage({ offset, size, sortBy, sortDirection }, filter, opts);
  }

  watchPage(page: Partial<Page>, filter?: Partial<F>, opts?: WO & { query?: any }): Observable<LoadResult<E>> {
    filter = this.asFilter(filter);
    page = this.asPage(page, filter);

    const variables = {
      page: {
        ...page,
        sortDirection: page?.sortDirection?.toUpperCase(),
      },
      filter: filter?.asPodObject(),
    };

    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Loading ${this._logTypeName}...`, variables);

    const withTotal = opts?.withTotal !== false && !!this.queries.loadAllWithTotal;
    const query =
      opts?.query || // use given query
      // Or get loadAll or loadAllWithTotal query
      withTotal
        ? this.queries.loadAllWithTotal
        : this.queries.loadAll;
    return this.mutableWatchQuery<LoadResult<any>>({
      queryName: withTotal ? 'LoadAllWithTotal' : 'LoadAll',
      query,
      arrayFieldName: 'data',
      totalFieldName: withTotal ? 'total' : undefined,
      insertFilterFn: filter?.asFilterFn(),
      sortFn: EntityUtils.entitySortComparator(page.sortBy, page.sortDirection),
      variables,
      error: { code: errorCodes.load, message: 'DATA.ERROR.LOAD_DATA_ERROR' },
      fetchPolicy: opts?.fetchPolicy || 'cache-and-network',
    }).pipe(
      distinctUntilChanged((x, y) => this.distinctResultComparator(x, y)),
      map((res) => {
        if (!res) {
          console.warn('no result for:', query, variables);
          return { data: [], total: 0 };
        }
        const { data, total } = res;
        // Convert to entity (if needed)
        const entities = opts?.toEntity !== false ? (data || []).map((json) => this.fromObject(json)) : ((data || []) as E[]);
        if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} loaded in ${Date.now() - now}ms`, entities);
        return { data: entities, total };
      })
    );
  }

  distinctResultComparator(result1: LoadResult<E>, result2: LoadResult<E>): boolean {
    if ((!!result1.total && !!result2.total && result1.total !== result2.total) || result1.data.length !== result2.data.length) return false;

    return result1.data.every(
      (value, index) =>
        value.__typename === result2.data[index].__typename &&
        value.id === result2.data[index].id &&
        value.updateDate === result2.data[index].updateDate
    );
  }

  async load(id: number, opts?: LO & { query?: any }): Promise<E> {
    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Loading ${this._logTypeName} {${id}}...`);
    const query = (opts && opts.query) || this.queries.load;

    const { data } = await this.graphql.query<{ data: any }>({
      query,
      variables: {
        id,
      },
      fetchPolicy: (opts && (opts.fetchPolicy as FetchPolicy)) || undefined,
      error: { code: errorCodes.load, message: 'DATA.ERROR.LOAD_DATA_ERROR' },
    });
    if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} item loaded in ${Date.now() - now}ms`, data);

    // Convert to entity
    return opts?.toEntity !== false ? data && this.fromObject(data) : (data as E);
  }

  async loadAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<F>,
    opts?: LO & { query?: any; debug?: boolean; withTotal?: boolean }
  ): Promise<LoadResult<E>> {
    return this.loadPage(this.asPage({ offset, size, sortBy, sortDirection }, filter), filter, opts);
  }

  async loadPage(
    page: Partial<Page>, // use page as-is (don't call asPage())
    filter?: Partial<F>,
    opts?: LO & { query?: any; debug?: boolean; withTotal?: boolean }
  ): Promise<LoadResult<E>> {
    const debug = this._debug && opts?.debug !== false;
    filter = this.asFilter(filter);

    const variables: any = {
      page: {
        ...page,
        sortDirection: page?.sortDirection?.toUpperCase(),
      },
      filter: filter?.asPodObject(),
    };

    const now = Date.now();
    if (debug) console.debug(`${this._logPrefix} Loading ${this._logTypeName} items...`, variables);

    const withTotal = opts?.withTotal !== false && !!this.queries.loadAllWithTotal;
    const query =
      opts?.query || // use given query
      // Or get loadAll or loadAllWithTotal query
      withTotal
        ? this.queries.loadAllWithTotal
        : this.queries.loadAll;
    const { data, total } = await this.graphql.query<LoadResult<any>>({
      query,
      variables,
      error: { code: errorCodes.load, message: 'DATA.ERROR.LOAD_DATA_ERROR' },
      fetchPolicy: opts?.fetchPolicy || 'cache-first',
    });
    const entities = opts?.toEntity !== false ? (data || []).map((json) => this.fromObject(json)) : ((data || []) as E[]);
    if (debug) console.debug(`${this._logPrefix} ${this._logTypeName} items loaded in ${Date.now() - now}ms`, data);
    const res: LoadResult<E> = { data: entities, total };
    if (withTotal) {
      // Add fetch more function
      const nextOffset = page.offset + entities.length;
      if (nextOffset < total) {
        res.fetchMore = () => this.loadPage({ ...page, offset: nextOffset }, filter, opts);
      }
    }
    return res;
  }

  async exists(criteria: C): Promise<boolean> {
    const now = Date.now();
    const filter = this.asFilter({ criterias: [criteria] });
    const variables: any = {
      filter: filter?.asPodObject(),
    };

    if (this._debug) console.debug(`${this._logPrefix} Counting ${this._logTypeName} items...`, variables);
    const res = await this.graphql.query<{ total?: number }>({
      query: this.queries.countAll,
      variables,
      error: { code: errorCodes.load, message: 'DATA.ERROR.COUNT_DATA_ERROR' },
      fetchPolicy: 'no-cache',
    });
    if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} items counted in ${Date.now() - now}ms`, res.total);
    return res.total > 0;
  }

  async saveAll(entities: E[], opts?: EntitySaveOptions): Promise<E[]> {
    throw new Error(`${this._logPrefix} Not implemented`);
  }

  async save(entity: E, opts?: EntitySaveOptions): Promise<E> {
    throw new Error(`${this._logPrefix} Not implemented`);
  }

  async deleteAll(entities: E[], opts?: any): Promise<any> {
    throw new Error(`${this._logPrefix} Not implemented`);
  }

  async delete(entity: E, opts?: any): Promise<any> {
    throw new Error(`${this._logPrefix} Not implemented`);
  }

  listenChanges(id: number, opts?: { query?: any; variables?: any; interval?: number; toEntity?: boolean }): Observable<E> {
    throw new Error(`${this._logPrefix} Not implemented`);
  }

  exportAllAsync(context: IExportContext, filter: F): Promise<Job> {
    throw new Error(`${this._logPrefix} Not implemented`);
  }

  // Protected methods
  equals(e1: E, e2: E): boolean {
    return e1 && e2 && e1.id === e2.id;
  }

  protected asPage(page: Partial<Page>, filter?: Partial<F>): Page {
    return {
      offset: page.offset || 0,
      size: page.size || 100,
      sortBy:
        page.sortBy ||
        filter?.criterias?.find((criteria) => isNotEmptyArray(criteria.searchAttributes))?.searchAttributes[0] ||
        this.defaultSortBy.toString(),
      sortDirection: page.sortDirection || this.defaultSortDirection,
    };
  }
}
