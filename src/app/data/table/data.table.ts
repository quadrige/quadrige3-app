import { AfterViewInit, Directive, OnDestroy, OnInit } from '@angular/core';
import { CanLeave } from '@app/core/service/component-guard.service';
import { UpdatableView } from '@app/shared/model/updatable-view';
import { DataEntity } from '@app/data/model/data.model';
import { DataValidatorService } from '@app/data/service/data-validator.service';
import { DataFilter, DataFilterCriteria } from '@app/data/model/data.filter.model';
import { BaseTable } from '@app/shared/table/base.table';

@Directive()
export abstract class DataTable<
    E extends DataEntity<E>,
    F extends DataFilter<F, C, E>,
    C extends DataFilterCriteria<E>,
    V extends DataValidatorService<E> = any,
  >
  extends BaseTable<E, number, F, C, V>
  implements OnInit, AfterViewInit, CanLeave, UpdatableView, OnDestroy
{
  entityI18nName(entityName: string, i18nRoot?: string): string {
    return super.entityI18nName(entityName, 'DATA.');
  }
}
