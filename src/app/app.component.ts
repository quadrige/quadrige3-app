import { ChangeDetectorRef, Component, inject, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import {
  ConfigService,
  Configuration,
  getColorContrast,
  getColorShade,
  getColorTint,
  hexToRgbArray,
  isNotNil,
  mixHex,
  PlatformService,
} from '@sumaris-net/ngx-components';
import { PermissionService } from '@app/core/service/permission.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnDestroy {
  private readonly document = inject(DOCUMENT);
  private readonly platform = inject(PlatformService);
  private readonly permissionService = inject(PermissionService);
  private readonly configService = inject(ConfigService);
  private readonly matIconRegistry = inject(MatIconRegistry);
  private readonly domSanitizer = inject(DomSanitizer);
  private readonly cd = inject(ChangeDetectorRef);
  logo: string;
  appName: string;

  private readonly subscription = new Subscription();

  constructor() {
    this.start();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  async start() {
    console.info('[app] Starting...');

    await this.platform.start();
    await this.permissionService.start();

    // Listen for config changed
    this.subscription.add(this.configService.config.subscribe((config) => this.onConfigChanged(config)));

    this.addCustomSVGIcons();

    console.info('[app] Starting [OK]');
  }

  protected onConfigChanged(config: Configuration) {
    this.logo = config.smallLogo || config.largeLogo;
    this.appName = config.label;

    // Set document title
    this.document.getElementById('appTitle').textContent = isNotNil(config.name) ? `${config.label} - ${config.name}` : config.label;

    // Set document favicon
    const favicon = config.properties && config.properties['sumaris.favicon'];
    if (isNotNil(favicon)) {
      this.document.getElementById('appFavicon').setAttribute('href', favicon);
    }

    if (config.properties) {
      this.updateTheme({
        colors: {
          primary: config.properties['sumaris.color.primary'],
          secondary: config.properties['sumaris.color.secondary'],
          tertiary: config.properties['sumaris.color.tertiary'],
          success: config.properties['sumaris.color.success'],
          warning: config.properties['sumaris.color.warning'],
          accent: config.properties['sumaris.color.accent'],
          danger: config.properties['sumaris.color.danger'],
        },
      });
    }

    this.cd.markForCheck();
  }

  protected updateTheme(options: { colors?: { [color: string]: string } }) {
    if (!options) return;

    // Settings colors
    if (options.colors) {
      console.info('[app] Changing theme colors ', options);

      const style = document.documentElement.style;

      // Add 100 & 900 color for primary and secondary color
      ['primary', 'secondary'].forEach((colorName) => {
        const color = options.colors[colorName];
        options.colors[colorName + '100'] = (color && mixHex('#ffffff', color, 10)) || undefined;
        options.colors[colorName + '900'] = (color && mixHex('#000000', color, 12)) || undefined;
      });

      Object.getOwnPropertyNames(options.colors).forEach((colorName) => {
        // Remove existing value
        style.removeProperty(`--ion-color-${colorName}`);
        style.removeProperty(`--ion-color-${colorName}-rgb`);
        style.removeProperty(`--ion-color-${colorName}-contrast`);
        style.removeProperty(`--ion-color-${colorName}-contrast-rgb`);
        style.removeProperty(`--ion-color-${colorName}-shade`);
        style.removeProperty(`--ion-color-${colorName}-tint`);

        // Set new value, if any
        const color = options.colors[colorName];
        if (isNotNil(color)) {
          // Base color
          style.setProperty(`--ion-color-${colorName}`, color);
          style.setProperty(`--ion-color-${colorName}-rgb`, hexToRgbArray(color).join(', '));

          // Contrast color
          const contrastColor = getColorContrast(color, true);
          style.setProperty(`--ion-color-${colorName}-contrast`, contrastColor);
          style.setProperty(`--ion-color-${colorName}-contrast-rgb`, hexToRgbArray(contrastColor).join(', '));

          // Shade color
          style.setProperty(`--ion-color-${colorName}-shade`, getColorShade(color));

          // Tint color
          style.setProperty(`--ion-color-${colorName}-tint`, getColorTint(color));
        }
      });
    }
  }

  protected addCustomSVGIcons() {
    this.matIconRegistry.addSvgIcon('fish', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/fish.svg'));
  }
}
