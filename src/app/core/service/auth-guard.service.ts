import { inject, Injectable, InjectionToken } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { ENVIRONMENT } from '@sumaris-net/ngx-components';
import { PermissionService } from '@app/core/service/permission.service';

export const AUTH_GUARD = new InjectionToken<AuthGuardService>('authGuard');

@Injectable()
export class AuthGuardService {
  private readonly permissionService = inject(PermissionService);
  private readonly router = inject(Router);
  private readonly environment = inject(ENVIRONMENT);
  private readonly _debug: boolean;

  constructor() {
    this._debug = !this.environment.production;
  }

  canActivate: CanActivateFn = async (route, state) => {
    await this.permissionService.ready();

    // Force login
    if (!this.permissionService.isLogin()) {
      if (this._debug) console.debug('[auth-gard] Need authentication for page /' + route.url.join('/'));
      const res = await this.permissionService.login();
      if (!res) {
        if (this._debug) console.debug('[auth-gard] Authentication cancelled. Could not access to /' + route.url.join('/'));
        return this.router.parseUrl('/home');
      }
      return true;
    }

    // Tell Permission to update account
    await this.permissionService.updateAccount();

    if (route.data && route.data.profile && !this.permissionService.hasMinProfile(route.data.profile)) {
      if (this._debug)
        console.debug('[auth-gard] Not authorized access to /' + route.url.join('/') + '. Missing required profile: ' + route.data.profile);
      return false;
    }
    if (this._debug) console.debug('[auth-gard] Authorized access to /' + route.url.join('/'));
    return true;
  };
}
