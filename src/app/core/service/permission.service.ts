import { inject, Injectable } from '@angular/core';
import {
  Account,
  AccountService,
  APP_LOCAL_SETTINGS,
  AppAuthModal,
  arraySize,
  ConfigService,
  ENVIRONMENT,
  ErrorCodes,
  fromDateISOString,
  isNilOrBlank,
  LocalSettingsService,
  NetworkService,
  StartableService,
} from '@sumaris-net/ngx-components';
import { UpdatableView } from '@app/shared/model/updatable-view';
import { ProgramService } from '@app/referential/program-strategy/program/program.service';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { Moment } from 'moment';
import { merge, Observable, Subscription, timer } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { Alerts } from '@app/shared/alerts';
import { TranslateService } from '@ngx-translate/core';
import { UserProfileLabel } from '@sumaris-net/ngx-components/src/app/core/services/model/person.model';
import { accountConfigOptions } from '@app/core/config/account.config';
import { CanLeave } from '@app/core/service/component-guard.service';
import { StableGraphqlService } from '@app/shared/service/stable-graphql.service';
import gql from 'graphql-tag';
import { Storage } from '@ionic/storage-angular';
import { GeneralConditionService } from '@app/core/general-condition/general-condition.service';
import { ModalService } from '@app/shared/component/modal/modal.service';
import { GeneralConditionModal, IGeneralConditionOptions } from '@app/core/general-condition/general-condition.modal';
import { coreConfigOptions } from '@app/core/config/core.config';

/**
 * Wrapper service for AccountService and LocalSettingsService
 */
@Injectable({ providedIn: 'root' })
export class PermissionService extends StartableService {
  private readonly logPrefix = '[permission-service]';
  private currentPubkey: string;
  private lastUpdateDate: Moment;
  private mainView: CanLeave;
  private updatableViews: WeakRef<UpdatableView>[] = [];

  private readonly subscriptions = new Subscription();
  private accountAliveSubscription: Subscription;
  private checkPeerALivePeriodInSecond: number;
  private _writableProgramIds: string[];
  private _managedProgramIds: string[];

  private readonly networkService = inject(NetworkService);
  private readonly accountService = inject(AccountService);
  private readonly settingsService = inject(LocalSettingsService);
  private readonly configService = inject(ConfigService);
  private readonly generalConditionService = inject(GeneralConditionService);
  private readonly programService = inject(ProgramService);
  private readonly alertCtrl = inject(AlertController);
  private readonly translate = inject(TranslateService);
  private readonly graphql = inject(StableGraphqlService);
  private readonly storage = inject(Storage);
  private readonly modalCtrl = inject(ModalController);
  private readonly modalService = inject(ModalService);
  private readonly environment = inject(ENVIRONMENT, { optional: true });
  private readonly defaultSettings = inject(APP_LOCAL_SETTINGS, { optional: true });

  constructor(platform: Platform) {
    super(platform);
    this._debug = !this.environment?.production;
  }

  get writableProgramIds(): string[] {
    return this._writableProgramIds;
  }

  get managedProgramIds(): string[] {
    return this._managedProgramIds;
  }

  get hasSomeManagementPermission(): boolean {
    return this.accountService.isAdmin() || arraySize(this.managedProgramIds) > 0;
  }

  async registerView(view: UpdatableView) {
    await this.ready();

    if (!this.getUpdatableViews().includes(view)) this.updatableViews.push(new WeakRef<UpdatableView>(view));

    await this.onLogin(this.accountService.account);
  }

  async registerMainView(view: CanLeave) {
    await this.ready();

    this.mainView = view;
  }

  canLeave(): boolean {
    return !this.mainView?.dirty || !this.isLogin();
  }

  isLogin() {
    return this.accountService.isLogin();
  }

  hasMinProfile(profile: UserProfileLabel) {
    return this.accountService.hasMinProfile(profile);
  }

  async updateAccount() {
    await this.onLogin(this.accountService.account);
  }

  async accountExpired() {
    if (!this.accountService.isLogin()) return;
    this.stopAliveWatchdog();
    await this.accountService.logout();
    await Alerts.showExpiredSessionMessage(this.alertCtrl, this.translate);
    window.location.reload();
  }

  async login(): Promise<boolean> {
    const logged = await new Promise<boolean>(async (resolve) => {
      const modal = await this.modalCtrl.create({ component: AppAuthModal, componentProps: {} });
      modal.onDidDismiss().then(() => {
        if (this.isLogin()) {
          resolve(true);
          return;
        }
        resolve(false);
      });
      return modal.present();
    });

    if (!logged) {
      window.location.reload();
    }

    return logged;
  }

  /* protected methods */

  protected async ngOnStart(): Promise<any> {
    await this.settingsService.ready();
    await this.accountService.ready();

    window.addEventListener('beforeunload', (event) => {
      if (!this.canLeave()) {
        event.returnValue = 'Cannot leave';
      }
    });

    // Listen login/logout
    this.subscriptions.add(this.accountService.onLogin.subscribe((account) => this.onLogin(account)));
    this.subscriptions.add(this.accountService.onWillLogout.subscribe((account) => this.onWillLogout(account)));
    this.subscriptions.add(this.accountService.onLogout.subscribe(() => this.onLogout()));

    // Listen configuration
    this.subscriptions.add(
      this.configService.config.subscribe((config) => {
        this.checkPeerALivePeriodInSecond = config.getProperty(coreConfigOptions.serverPingIntervalInSecond);
        if (this._debug && !!this.checkPeerALivePeriodInSecond) {
          console.debug(`${this.logPrefix} Check peer alive refresh period = ${this.checkPeerALivePeriodInSecond}s`);
        }
      })
    );

    // Login
    await this.onLogin(this.accountService.account);
  }

  protected ngOnStop(): Promise<void> | void {
    console.warn(`${this.logPrefix} Stopped`);
    this.stopAliveWatchdog();
    this.subscriptions.unsubscribe();
  }

  protected async onLogin(account: Account) {
    if (!account) return;

    // Force account inheritance
    this.settingsService.settings.accountInheritance = true;

    const pubkeyChanged = account.pubkey !== this.currentPubkey;
    const accountUpdated = !fromDateISOString(account.updateDate)?.isSame(this.lastUpdateDate);

    if (pubkeyChanged || accountUpdated) {
      this.currentPubkey = account.pubkey;
      this.lastUpdateDate = account.updateDate;

      // Load account remotely to get user settings
      const remoteAccount = await this.accountService.load({ fetchPolicy: 'network-only' });
      const defaultProperties = Object.keys(accountConfigOptions).reduce((acc, key) => {
        acc[accountConfigOptions[key].key] = accountConfigOptions[key].defaultValue;
        return acc;
      }, {});
      const defaultSettings = { ...this.defaultSettings, properties: defaultProperties };
      const userSettings = remoteAccount.settings.asLocalSettings();
      await this.settingsService.apply({ ...defaultSettings, ...userSettings });

      // Load general condition and check if accepted
      if (!(await this.checkGeneralCondition())) {
        // Reject login
        await this.accountService.logout();
        return;
      }

      // Load available program permissions
      if (this._debug) console.debug(`${this.logPrefix} Load available programs for user`);
      Promise.all([
        this.programService.getWritableProgramIds(account.id).then((value) => (this._writableProgramIds = value)),
        this.programService.getManagedProgramIds(account.id).then((value) => (this._managedProgramIds = value)),
      ]).then(() => this.updatePermission());
    } else {
      this.updatePermission();
    }
    await this.startAliveWatchdog();
  }

  protected updatePermission() {
    this.getUpdatableViews()?.forEach((view) => view.updatePermission());
  }

  protected async onWillLogout(account: Account) {
    this.stopAliveWatchdog();
    await this.invalidatePubkey(account.pubkey);
  }

  protected onLogout() {
    this.currentPubkey = undefined;
    this.lastUpdateDate = undefined;
    this.stopAliveWatchdog();

    // Deactivate all views (will close datasource)
    this.getUpdatableViews()?.forEach((view) => view.deactivate());
    this.updatableViews = [];
    this.mainView?.deactivate();
    this.mainView = null;

    // Stop GraphQL service to close all queries and websocket channels
    this.graphql.stop();
  }

  protected getUpdatableViews(): UpdatableView[] {
    return this.updatableViews.map((weakRef) => weakRef.deref()).filter((ref) => !!ref);
  }

  protected async startAliveWatchdog() {
    if (this.accountAliveSubscription) return; // Already running: skip

    const token: string = await this.storage.get('token');
    if (isNilOrBlank(token)) {
      throw new Error('Stored token is empty ! Should not happened');
    }
    this.accountAliveSubscription = merge(this.listenAccountAlive(token), this.checkPeerAlive()).subscribe(async (alive) => {
      if (!alive) {
        await this.accountExpired();
      }
    });

    this.accountAliveSubscription.add(() => {
      if (this._debug) console.debug(`${this.logPrefix} Account alive subscription stopped`);
    });
  }

  protected stopAliveWatchdog() {
    this.accountAliveSubscription?.unsubscribe();
    this.accountAliveSubscription = undefined;
  }

  private checkPeerAlive(): Observable<boolean> {
    const timerMs = this.checkPeerALivePeriodInSecond * 1000;
    return timer(timerMs, timerMs).pipe(
      // Checkin if peer alive
      tap(() => {
        if (this._debug) console.debug(`${this.logPrefix} Checking peer alive...`);
      }),
      mergeMap(async () => {
        try {
          return !!(await this.networkService.checkPeerAlive());
        } catch (e) {
          if (this._debug) console.debug(`${this.logPrefix} Failed to check peer is alive`, e);
          return false;
        }
      })
    );
  }

  private listenAccountAlive(token: string): Observable<boolean> {
    if (this._debug) console.debug(`${this.logPrefix} [WS] Listening account alive`);

    return this.graphql
      .subscribe<{ data: boolean }, { token: string }>({
        query: gql`
          subscription UpdateAccountAlive($token: String) {
            data: updateAccountAlive(token: $token)
          }
        `,
        fetchPolicy: 'no-cache',
        variables: { token },
        error: {
          code: ErrorCodes.SUBSCRIBE_ACCOUNT_ERROR,
          message: 'ACCOUNT.ERROR.SUBSCRIBE_ACCOUNT_ERROR',
        },
      })
      .pipe(
        map(({ data }) => {
          if (this._debug) console.debug(`${this.logPrefix} Account alive ?`, data);
          return data;
        })
      );
  }

  private async invalidatePubkey(pubkey: string) {
    if (isNilOrBlank(pubkey)) return;

    if (this._debug) console.debug(`${this.logPrefix} Invalidate pubkey...`);
    await this.graphql.query({
      query: gql`
        query InvalidatePubkey($pubkey: String) {
          invalidatePubkey(pubkey: $pubkey)
        }
      `,
      variables: { pubkey },
      fetchPolicy: 'network-only',
      error: { code: ErrorCodes.LOAD_ACCOUNT_ERROR, message: 'ERROR.LOAD_ACCOUNT_ERROR' },
    });
    if (this._debug) console.debug(`${this.logPrefix} Pubkey invalidated...`);
  }

  private async checkGeneralCondition(): Promise<boolean> {
    const lastCondition = await this.generalConditionService.getLast();
    if (!lastCondition) return true;
    // Show condition
    const { role } = await this.modalService.openModal<IGeneralConditionOptions, void>(
      GeneralConditionModal,
      {
        titleI18n: 'GENERAL_CONDITION.TITLE',
        content: lastCondition.content,
      },
      'modal-full-height'
    );
    if (role === 'validate') {
      // Accept it
      await this.generalConditionService.accept(lastCondition.id);
      return true;
    } else {
      return false;
    }
  }
}
