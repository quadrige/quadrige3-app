import { Injectable, InjectionToken } from '@angular/core';
import { CanDeactivateFn } from '@angular/router';
import { Observable, of } from 'rxjs';

export interface CanLeave {
  dirty: boolean;
  canDeactivate: () => Observable<boolean>;

  deactivate(): void;
}

export const COMPONENT_GUARD = new InjectionToken<ComponentGuardService>('componentGuard');

@Injectable()
export class ComponentGuardService {
  canDeactivate: CanDeactivateFn<CanLeave> = (component) => {
    if (!component) {
      return of(true);
    }
    return component.canDeactivate();
  };
}
