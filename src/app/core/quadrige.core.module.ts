import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage-angular';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { CacheModule } from 'ionic-cache';
import { TranslateModule } from '@ngx-translate/core';
import { AppAccountModule, AppGraphQLModule, CoreModule, PlatformService } from '@sumaris-net/ngx-components';
import { QuadrigeSharedModule } from '../shared/quadrige.shared.module';
import { FooterComponent } from './component/footer/footer.component';
import { PermissionService } from '@app/core/service/permission.service';
import { GeneralConditionModal } from '@app/core/general-condition/general-condition.modal';

@NgModule({
  declarations: [FooterComponent, GeneralConditionModal],
  exports: [CoreModule, QuadrigeSharedModule, RouterModule, TranslateModule, FooterComponent],
  imports: [CoreModule, QuadrigeSharedModule, RouterModule, AppGraphQLModule, CacheModule, IonicStorageModule, AppAccountModule],
  providers: [provideHttpClient(withInterceptorsFromDi())],
})
export class QuadrigeCoreModule {
  static forRoot(): ModuleWithProviders<QuadrigeCoreModule> {
    console.info('[core] Creating Quadrige module (root)');
    return {
      ngModule: QuadrigeCoreModule,
      providers: [PlatformService, PermissionService],
    };
  }
}
