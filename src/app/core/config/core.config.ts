import { FormFieldDefinition } from '@sumaris-net/ngx-components';
import { referentialOptions } from '@app/referential/model/referential.constants';

export const coreConfigOptions = Object.freeze({
  defaultDepartment: <FormFieldDefinition>{
    key: 'quadrige3.default.ldap.department',
    label: 'CONFIGURATION.OPTIONS.DEFAULT_LDAP_DEPARTMENT',
    type: 'entity',
    autocomplete: {
      ...referentialOptions.department,
      filter: {
        entityName: 'Department',
      },
    },
    required: false,
  },
  extractionEnabled: <FormFieldDefinition>{
    key: 'quadrige3.extraction.enabled',
    label: 'CONFIGURATION.OPTIONS.EXTRACTION_ENABLED',
    type: 'boolean',
    defaultValue: false,
    required: false,
  },
  serverPingIntervalInSecond: <FormFieldDefinition>{
    key: 'quadrige3.server.ping.interval',
    label: 'CONFIGURATION.OPTIONS.SERVER_PING_INTERVAL',
    type: 'integer',
    defaultValue: 60,
    required: false,
  },
});
