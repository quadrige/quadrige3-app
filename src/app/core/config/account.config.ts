import { FormFieldDefinition } from '@sumaris-net/ngx-components';

export const accountConfigOptions = Object.freeze({
  tableCheckBoxSelection: <FormFieldDefinition>{
    key: 'quadrige3.table.checkBoxSelection',
    label: 'ACCOUNT.SETTINGS.TABLE_CHECKBOX_SELECTION',
    type: 'boolean',
    defaultValue: true,
  },
  showDisabledReferential: <FormFieldDefinition>{
    key: 'quadrige3.show.disabled.referential',
    label: 'ACCOUNT.SETTINGS.SHOW_DISABLED_REFERENTIAL',
    type: 'boolean',
    defaultValue: false,
  },
  showSocialAllUsers: <FormFieldDefinition>{
    key: 'quadrige3.show.social.allUsers',
    label: 'ACCOUNT.SETTINGS.SHOW_SOCIAL_ALL_USERS',
    type: 'boolean',
    defaultValue: false,
    minProfile: 'ADMIN',
  },
});
