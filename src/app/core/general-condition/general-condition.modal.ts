import { Component, Injector, Input } from '@angular/core';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';

export interface IGeneralConditionOptions extends IModalOptions {
  content: string;
}

@Component({
  selector: 'app-general-condition-modal',
  templateUrl: './general-condition.modal.html',
  styleUrls: ['./general-condition.modal.scss'],
})
export class GeneralConditionModal extends ModalComponent<void> implements IGeneralConditionOptions {
  @Input() content: string;

  constructor(protected injector: Injector) {
    super(injector);
  }

  protected dataToValidate(): void {
    return;
  }

  protected afterInit() {}
}
