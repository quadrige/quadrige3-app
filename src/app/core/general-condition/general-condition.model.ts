import { EntityClass } from '@sumaris-net/ngx-components';
import { Referential } from '@app/referential/model/referential.model';

@EntityClass({ typename: 'GeneralConditionVO' })
export class GeneralCondition extends Referential<GeneralCondition> {
  static entityName = 'GeneralCondition';
  static fromObject: (source: any, opts?: any) => GeneralCondition;

  content: string;

  constructor() {
    super(GeneralCondition.TYPENAME);
    this.entityName = GeneralCondition.entityName;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.entityName = GeneralCondition.entityName;
    this.content = source.content;
  }
}
