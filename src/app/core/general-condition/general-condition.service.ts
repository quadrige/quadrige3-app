import gql from 'graphql-tag';
import { inject, Injectable } from '@angular/core';
import { StableGraphqlService } from '@app/shared/service/stable-graphql.service';
import { ErrorCodes } from '@sumaris-net/ngx-components';
import { GeneralCondition } from '@app/core/general-condition/general-condition.model';

const generalConditionFragment = gql`
  fragment GeneralCondition on GeneralConditionVO {
    id
    content
    creationDate
    updateDate
    statusId
    __typename
  }
`;

const loadLastGeneralConditionQuery = gql`
  query LoadLastNonAcceptedGeneralCondition {
    data: lastNonAcceptedGeneralCondition {
      ...GeneralCondition
    }
  }
  ${generalConditionFragment}
`;

const acceptGeneralConditionMutation = gql`
  mutation AcceptGeneralCondition($id: Int!) {
    acceptGeneralCondition(id: $id)
  }
`;

@Injectable({ providedIn: 'root' })
export class GeneralConditionService {
  private readonly graphql = inject(StableGraphqlService);

  constructor() {}

  public async getLast(): Promise<GeneralCondition> {
    const res = await this.graphql.query<{ data: GeneralCondition }>({
      query: loadLastGeneralConditionQuery,
      fetchPolicy: 'network-only',
      error: {
        code: ErrorCodes.LOAD_CONFIG_ERROR,
        message: 'ERROR.LOAD_CONFIG_ERROR',
      },
    });
    return GeneralCondition.fromObject(res?.data);
  }

  public async accept(id: number) {
    await this.graphql.mutate({
      mutation: acceptGeneralConditionMutation,
      variables: { id },
      error: {
        code: ErrorCodes.SAVE_CONFIG_ERROR,
        message: 'ERROR.SAVE_CONFIG_ERROR',
      },
    });
  }
}
