import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { MenuService } from '@sumaris-net/ngx-components';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  @Input() canEdit: boolean;
  @Input() disabled: boolean;
  @Input() saveDisabled: boolean;
  @Input() saving: boolean;
  @Input() error: string;
  @Input() classList = 'inline-content';

  @Output() cancel = new EventEmitter<Event>();
  @Output() save = new EventEmitter<Event>();

  protected readonly menu = inject(MenuService);

  constructor() {}
}
