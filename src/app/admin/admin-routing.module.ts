import { inject, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { AdminModule } from '@app/admin/admin.module';
import { ConfigurationPage } from '@app/admin/config/configuration.page';
import { AUTH_GUARD } from '@app/core/service/auth-guard.service';

const routes: Routes = [
  {
    path: 'config',
    pathMatch: 'full',
    component: ConfigurationPage,
    canActivate: [(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => inject(AUTH_GUARD).canActivate(route, state)],
    data: {
      profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [AdminModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
