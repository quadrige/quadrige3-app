import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { ConfigurationPage } from '@app/admin/config/configuration.page';
import { ReferentialModule } from '@app/referential/referential.module';

@NgModule({
  imports: [CommonModule, QuadrigeCoreModule, ReferentialModule],
  declarations: [ConfigurationPage],
})
export class AdminModule {}
