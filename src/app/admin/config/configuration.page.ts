import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject, Injector, OnInit, Optional, ViewChild } from '@angular/core';
import {
  AccountService,
  Alerts,
  APP_CONFIG_OPTIONS,
  AppEntityEditor,
  AppPropertiesForm,
  ConfigService,
  Configuration,
  CORE_CONFIG_OPTIONS,
  Department,
  EntityServiceLoadOptions,
  EntityUtils,
  firstNotNilPromise,
  FormFieldDefinition,
  FormFieldDefinitionMap,
  HistoryPageReference,
  isNil,
  isNotNil,
  NetworkService,
  PlatformService,
} from '@sumaris-net/ngx-components';
import { BehaviorSubject } from 'rxjs';
import { SoftwareValidatorService } from '@app/admin/config/software.validator';
import { ReferentialForm } from '@app/referential/form/referential.form';
import { UntypedFormGroup } from '@angular/forms';
import { GenericService } from '@app/referential/generic/generic.service';
import { FileTransferService } from '@app/shared/service/file-transfer.service';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';

declare interface CacheStatistic {
  name: string;
  size: number;
  heapSize: number;
  offHeapSize: number;
  diskSize: number;
}

@Component({
  selector: 'app-configuration-page',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigurationPage extends AppEntityEditor<Configuration, ConfigService> implements OnInit {
  @ViewChild('referentialForm', { static: true }) referentialForm: ReferentialForm;
  @ViewChild('propertiesForm', { static: true }) propertiesForm: AppPropertiesForm;
  @ViewChild('logContainer') logContainer: ElementRef;

  propertyDefinitions: FormFieldDefinition[];
  form: UntypedFormGroup;

  partners = new BehaviorSubject<Department[]>(null);
  cacheStatistics = new BehaviorSubject<CacheStatistic[]>(null);
  cacheStatisticTotal = new BehaviorSubject<CacheStatistic>(null);

  log: string;

  constructor(
    protected injector: Injector,
    protected validatorService: SoftwareValidatorService,
    protected dataService: ConfigService,
    public network: NetworkService,
    protected accountService: AccountService,
    protected platform: PlatformService,
    protected cd: ChangeDetectorRef,
    protected referentialRefService: GenericService,
    protected transferService: FileTransferService,
    @Optional() @Inject(APP_CONFIG_OPTIONS) configOptions: FormFieldDefinitionMap
  ) {
    super(injector, Configuration, dataService, {
      tabCount: 4,
    });

    // default values
    this.defaultBackHref = null;

    // Convert map to list of options
    this.propertyDefinitions = Object.values({ ...CORE_CONFIG_OPTIONS, ...configOptions }).map((def) => {
      if (def.type === 'entity') {
        def = Object.assign({}, def); // Copy
        def.autocomplete = def.autocomplete || {};
        def.autocomplete.suggestFn = (value, filter) => this.referentialRefService.suggest(value, filter);
      }
      return def;
    });

    this.form = validatorService.getFormGroup();

    //this.debug = !environment.production;
  }

  get config(): Configuration {
    return (this.data && (this.data as Configuration)) || undefined;
  }

  ngOnInit() {
    super.ngOnInit();

    // Set entity name (required for referential form validator)
    this.referentialForm.entityName = 'Software';
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);

    if (!this.isNewData) {
      this.form.get('label').disable();
    }
  }

  async load(id?: number, opts?: EntityServiceLoadOptions): Promise<void> {
    const config = await firstNotNilPromise(this.dataService.config);

    // Force the load of the config
    await super.load(config.id, { ...opts, fetchPolicy: 'network-only' });

    this.registerSubscription(this.cacheStatistics.subscribe((value) => this.computeStatisticTotal(value)));

    // Get server cache statistics
    await this.loadCacheStat();
  }

  async clearCache(event?: UIEvent, cacheName?: string) {
    const confirm = await Alerts.askActionConfirmation(this.alertCtrl, this.translate, true, event);
    if (confirm) {
      await this.network.clearCache();
      await this.settings.removeOfflineFeatures();
      await this.dataService.clearCache({ cacheName });
      await this.loadCacheStat();
    }
  }

  async loadCacheStat() {
    const value = await this.dataService.getCacheStatistics();
    const stats: CacheStatistic[] = Object.keys(value).map((cacheName) => {
      const stat = value[cacheName];
      return {
        name: cacheName,
        size: stat.size,
        heapSize: stat.heapSize,
        offHeapSize: stat.offHeapSize,
        diskSize: stat.diskSize,
      };
    });
    this.cacheStatistics.next(stats);
  }

  computeStatisticTotal(stats: CacheStatistic[]) {
    const total: CacheStatistic = { name: undefined, size: 0, heapSize: 0, offHeapSize: 0, diskSize: 0 };
    (stats || []).forEach((stat) => {
      total.size += stat.size;
      total.heapSize += stat.heapSize;
      total.offHeapSize += stat.offHeapSize;
      total.diskSize += stat.diskSize;
    });
    this.cacheStatisticTotal.next(total);
  }

  canUserWrite(data: Configuration): boolean {
    return this.accountService.isAdmin();
  }

  loadLog() {
    this.markAsLoading();
    this.referentialRefService.loadServerLog().then((value) => {
      this.log = value;
      this.markAsLoaded();
      setTimeout(() =>
        this.logContainer.nativeElement.scroll({
          top: this.logContainer.nativeElement.scrollHeight,
        })
      );
    });
  }

  downloadLog() {
    this.platform.download({ uri: this.transferService.downloadFile('log') });
  }

  setValue(data: Configuration) {
    if (!data) return; // Skip

    const json = data.asObject();
    this.partners.next(json.partners);

    this.form.patchValue(
      {
        ...data.asObject(),
        properties: [],
      },
      { emitEvent: false }
    );

    // Program properties
    this.propertiesForm.value = EntityUtils.getMapAsArray(data.properties || {});

    this.markAsPristine();
  }

  /* -- protected methods -- */

  protected async getJsonValueToSave(): Promise<any> {
    const json = await super.getJsonValueToSave();

    // Re add label, because missing when field disable
    json.label = this.form.get('label').value;

    // Convert entities to id
    json.properties = this.propertiesForm.value; // getPropertyArrayAsObject ?
    json.properties
      .filter((property) => this.propertyDefinitions.find((def) => def.key === property.key && def.type === 'entity'))
      .forEach((property) => (property.value = property.value.id));

    // Re add partners
    json.partners = this.partners.getValue();

    return json;
  }

  protected registerForms() {
    this.addChildForms([this.referentialForm, this.propertiesForm]);
  }

  protected async loadFromRoute(): Promise<void> {
    // Make sure the platform is ready
    await this.platform.ready();

    return super.loadFromRoute();
  }

  protected computeTitle(data: Configuration): Promise<string> {
    // new data
    if (!data || isNil(data.id)) {
      return this.translate.get('CONFIGURATION.NEW.TITLE').toPromise();
    }

    return this.translate.get('CONFIGURATION.EDIT.TITLE', data).toPromise();
  }

  protected getFirstInvalidTabIndex(): number {
    if (this.referentialForm.invalid) return 0;
    if (this.propertiesForm.invalid) return 1;
    return 0;
  }

  protected async onEntityLoaded(data: Configuration, options?: EntityServiceLoadOptions): Promise<void> {
    await this.loadEntityProperties(data);
    await super.onEntityLoaded(data, options);
    this.markAsReady();
  }

  protected async onEntitySaved(data: Configuration): Promise<void> {
    await this.loadEntityProperties(data);
    await super.onEntitySaved(data);
    this.markAsReady();
  }

  protected async loadEntityProperties(data: Configuration | null) {
    return Promise.all(
      Object.keys(data.properties)
        .map((key) => this.propertyDefinitions.find((def) => def.key === key && def.type === 'entity'))
        .filter(isNotNil)
        .map((def) => {
          let value = data.properties[def.key];
          const filter = GenericReferentialFilter.fromObject(def.autocomplete.filter);
          const joinAttribute = def.autocomplete.filter.joinAttribute || 'id';
          if (joinAttribute === 'id') {
            filter.patch({ id: value });
            value = '*';
          } else {
            filter.patch({ searchAttributes: [joinAttribute] });
          }
          // Fetch entity, as a referential
          return (
            this.referentialRefService
              .suggest(value, filter)
              .then((matches) => {
                data.properties[def.key] = ((matches && matches.data && matches.data[0]) || { id: value, label: '??' }) as any;
              })
              // Cannot ch: display an error
              .catch((err) => {
                console.error('Cannot fetch entity, from option: ' + def.key + '=' + value, err);
                data.properties[def.key] = { id: value, label: '??' } as any;
              })
          );
        })
    );
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected async computePageHistory(title: string): Promise<HistoryPageReference> {
    return null; // No page history
  }
}
