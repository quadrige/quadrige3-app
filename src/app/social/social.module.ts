import { NgModule } from '@angular/core';
import {
  APP_JOB_PROGRESSION_SERVICE,
  APP_USER_EVENT_LIST_INFINITE_SCROLL_THRESHOLD,
  APP_USER_EVENT_SERVICE,
  JobProgressionService,
} from '@sumaris-net/ngx-components';
import { UserEventService } from '@app/social/user-event/user-event.service';
import { SocialComponent } from '@app/social/social.component';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { SocialHomePage } from '@app/social/menu/social-home.page';

@NgModule({
  providers: [
    {
      provide: APP_USER_EVENT_SERVICE,
      useClass: UserEventService,
    },
    {
      provide: APP_USER_EVENT_LIST_INFINITE_SCROLL_THRESHOLD,
      useValue: '10%',
    },
    {
      provide: APP_JOB_PROGRESSION_SERVICE,
      useClass: JobProgressionService,
    },
  ],
  imports: [QuadrigeCoreModule, SocialComponent],
  declarations: [SocialHomePage],
})
export class QuadrigeSocialModule {}
