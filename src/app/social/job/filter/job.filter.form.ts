import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PartialRecord } from '@app/shared/model/interface';
import { JobFilter, JobFilterCriteria } from '@app/social/job/filter/job.filter.model';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import { isNotNil } from '@sumaris-net/ngx-components';
import { jobStatus, jobStatusI18nLabels, jobTypes, jobTypesI18nLabels } from '@app/social/job/job.model';
import { StrReferential } from '@app/referential/model/referential.model';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { BaseFilterFormComponent } from '@app/shared/component/filter/base-filter-form.component';
import { ReferentialFilterComponentModule } from '@app/referential/component/referential-filter-component.module';
import { attributes } from '@app/referential/model/referential.constants';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';

@Component({
  selector: 'app-job-filter-form',
  templateUrl: './job.filter.form.html',
  standalone: true,
  imports: [QuadrigeCoreModule, BaseFilterFormComponent, ReferentialFilterComponentModule, ReferentialFilterFormField],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobFilterForm extends BaseCriteriaFormComponent<JobFilter, JobFilterCriteria> {
  @Input() showAllUsers: boolean;
  @Input() origins: StrReferential[];

  readonly jobStatus = jobStatus;
  readonly jobStatusI18nLabels = jobStatusI18nLabels;
  readonly jobTypes = jobTypes;
  readonly jobTypesI18nLabels = jobTypesI18nLabels;

  constructor() {
    super();
  }

  async setValue(value: JobFilter, opts?: ISetFilterOptions) {
    if (!this.showAllUsers && value?.criterias?.[0]?.allUsers) {
      value.criterias[0].allUsers = false;
    }
    await super.setValue(value, opts);
  }

  protected getSearchAttributes(): string[] {
    return attributes.idName;
  }

  protected criteriaToQueryParams(criteria: JobFilterCriteria): PartialRecord<keyof JobFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'userFilter');
    if (isNotNil(criteria.type)) params.type = criteria.type;
    if (isNotNil(criteria.status)) params.status = criteria.status;
    if (isNotNil(criteria.originId)) params.originId = criteria.originId;
    if (this.showAllUsers && isNotNil(criteria.allUsers)) params.allUsers = criteria.allUsers;
    return params;
  }

  protected queryParamsToCriteria(params: string): JobFilterCriteria {
    const criteria = super.queryParamsToCriteria(params);
    if (!this.showAllUsers) criteria.allUsers = false;
    return criteria;
  }
}
