import { EntityAsObjectOptions, EntityClass, FilterFn, isNotEmptyArray, isNotNil, toBoolean } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { EntityUtils } from '@app/shared/entity.utils';
import { IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { Job, JobStatus, JobType } from '@app/social/job/job.model';
import { BaseEntityFilter, BaseEntityFilterCriteria, BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'JobFilterCriteriaVO' })
export class JobFilterCriteria extends BaseEntityFilterCriteria<Job, number> {
  static fromObject: (source: any, opts?: any) => JobFilterCriteria;

  userId: number = null;
  allUsers: boolean = null;
  searchText: string = null;
  userFilter: IntReferentialFilterCriteria = null;
  originId: string = null;
  type: JobType = null;
  types: JobType[] = null;
  status: JobStatus = null;
  statuses: JobStatus[] = null;
  lastUpdateDate: Moment = null;

  constructor() {
    super(JobFilterCriteria.TYPENAME);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.userId = source.userId;
    this.allUsers = toBoolean(source.allUsers, false);
    this.searchText = source.searchText;
    this.userFilter = IntReferentialFilterCriteria.fromObject(source.userFilter || {});
    this.originId = source.originId;
    this.type = source.type;
    this.types = source.types || [];
    this.status = source.status;
    this.statuses = source.statuses || [];
    this.lastUpdateDate = source.lastUpdateDate;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.userFilter = this.userFilter?.asObject(opts);
    target.types = isNotEmptyArray(this.types) ? this.types : isNotNil(this.type) ? [this.type] : [];
    target.statuses = isNotEmptyArray(this.statuses) ? this.statuses : isNotNil(this.status) ? [this.status] : [];
    if (opts?.minify) {
      delete target.type;
      delete target.status;
    }
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any): boolean {
    if (key === 'userId') return false;
    if (key === 'allUsers' && value === false) return false;
    if (key === 'userFilter') return !BaseFilterUtils.isCriteriaEmpty(this.userFilter, true);
    return super.isCriteriaNotEmpty(key, value);
  }

  protected buildFilter(): FilterFn<Job>[] {
    const filterFns = super.buildFilter();

    if (this.userId && !this.allUsers) {
      filterFns.push((data) => data.userId === this.userId);
    }
    const searchTextFilter = EntityUtils.searchTextFilter(['name'], this.searchText);
    if (searchTextFilter) filterFns.push(searchTextFilter);
    if (this.userFilter) {
      const filter = this.userFilter.asFilterFn();
      if (filter) filterFns.push((data) => filter(data.user));
    }
    if (isNotNil(this.originId)) {
      filterFns.push((data) => this.originId === data.originId);
    }
    if (isNotEmptyArray(this.types)) {
      filterFns.push((data) => this.types.includes(data.type));
    }
    if (isNotEmptyArray(this.statuses)) {
      filterFns.push((data) => this.statuses.includes(data.status));
    }
    if (this.lastUpdateDate) {
      filterFns.push((data) => data.updateDate >= this.lastUpdateDate);
    }

    return filterFns;
  }
}

@EntityClass({ typename: 'JobFilterVO' })
export class JobFilter extends BaseEntityFilter<JobFilter, JobFilterCriteria, Job> {
  static fromObject: (source: any, opts?: any) => JobFilter;

  constructor() {
    super(JobFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  criteriaFromObject(source: any, opts: any): JobFilterCriteria {
    return JobFilterCriteria.fromObject(source, opts);
  }
}
