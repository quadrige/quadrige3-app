import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { JobResult } from '@app/social/job/job-result.model';
import { ModalComponent } from '@app/shared/component/modal/modal.component';
import { IModalOptions } from '@app/shared/model/options.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { JobResultForm } from '@app/social/job/result/job-result.form';

export interface IJobResultModalOptions extends IModalOptions {
  jobResult: JobResult;
}

@Component({
  selector: 'app-job-result-modal',
  templateUrl: './job-result.modal.html',
  styleUrls: ['./job-result.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule, JobResultForm],
})
export class JobResultModal extends ModalComponent<any> implements IJobResultModalOptions {
  @Input() titleI18n = 'SOCIAL.JOB.RESULT.TITLE';
  @Input() jobResult: JobResult;
  title: string;

  constructor(protected injector: Injector) {
    super(injector);
    this.logPrefix = '[job-result-modal]';
  }

  protected afterInit() {
    if (!this.jobResult) {
      console.error(`${this.logPrefix} No job result to show`);
      return;
    }
    if (this.debug) {
      console.debug(`${this.logPrefix} Viewing result for type=${this.jobResult.job.type}`, this.jobResult.job.report);
    }
    this.title = this.title || this.translate.instant(this.titleI18n, { name: this.jobResult.job.name });
    this.markForCheck();
  }

  protected dataToValidate() {
    throw new Error('Method not implemented.');
  }
}
