import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Input } from '@angular/core';
import { ENVIRONMENT, ObjectMap, PlatformService, toBoolean } from '@sumaris-net/ngx-components';
import { ImportShapeJobResult, JobResult } from '@app/social/job/job-result.model';
import { MonitoringLocationReport } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { FileTransferService } from '@app/shared/service/file-transfer.service';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-job-result-form',
  templateUrl: './job-result.form.html',
  styleUrls: ['./job-result.form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule],
})
export class JobResultForm {
  @Input() showDownloadLink = true;
  @Input() contentClass = 'content';

  protected logPrefix = '[job-result-form]';
  protected debug = false;
  private _jobResult: JobResult;

  protected readonly cd = inject(ChangeDetectorRef);
  protected readonly fileService = inject(FileTransferService);
  protected readonly platform = inject(PlatformService);
  protected readonly environment = inject(ENVIRONMENT);

  constructor() {
    this.debug = toBoolean(!this.environment?.production, true);
  }

  @Input() set jobResult(jobResult: JobResult) {
    this._jobResult = jobResult;
    this.cd.markForCheck();
  }

  get jobResult(): JobResult {
    return this._jobResult;
  }

  download(event: UIEvent, fileName: string) {
    const fileUrl = this.fileService.downloadFile(fileName);
    this.platform.download({ uri: fileUrl });
  }

  keys(map: ObjectMap): string[] {
    return Object.keys(map);
  }

  messagesForFile(file: string): any {
    return this.jobResult.report.messages[file] || {};
  }

  errorsForFile(file: string): any {
    return this.jobResult.report.errors[file] || {};
  }

  reportsForFile(file: string): ObjectMap<MonitoringLocationReport> {
    return (this.jobResult.report as ImportShapeJobResult).reports[file] || {};
  }
}
