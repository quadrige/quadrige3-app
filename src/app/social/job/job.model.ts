import { Entity, EntityAsObjectOptions, EntityClass, fromDateISOString, toDateISOString } from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { EntityUtils } from '@app/shared/entity.utils';
import { IntReferential, StrReferential } from '@app/referential/model/referential.model';
import { JobResult } from '@app/social/job/job-result.model';

export type JobType =
  | 'IMPORT_ORDER_ITEM_SHAPE'
  | 'IMPORT_MONITORING_LOCATION_SHAPE'
  | 'EXPORT_REFERENTIAL'
  | 'EXTRACTION_RESULT'
  | 'EXTRACTION_IN_SITU';
export const jobTypes: JobType[] = [
  'IMPORT_ORDER_ITEM_SHAPE',
  'IMPORT_MONITORING_LOCATION_SHAPE',
  'EXPORT_REFERENTIAL',
  'EXTRACTION_RESULT',
  'EXTRACTION_IN_SITU',
];
export const jobTypesI18nLabels: Record<JobType, string> = {
  /* eslint-disable @typescript-eslint/naming-convention */
  IMPORT_ORDER_ITEM_SHAPE: 'SOCIAL.JOB.TYPE.IMPORT_ORDER_ITEM_SHAPE',
  IMPORT_MONITORING_LOCATION_SHAPE: 'SOCIAL.JOB.TYPE.IMPORT_MONITORING_LOCATION_SHAPE',
  EXPORT_REFERENTIAL: 'SOCIAL.JOB.TYPE.EXPORT_REFERENTIAL',
  EXTRACTION_RESULT: 'SOCIAL.JOB.TYPE.EXTRACTION_RESULT',
  EXTRACTION_IN_SITU: 'SOCIAL.JOB.TYPE.EXTRACTION_IN_SITU',
  /* eslint-enable @typescript-eslint/naming-convention */
};

export type JobStatus = 'PENDING' | 'RUNNING' | 'SUCCESS' | 'WARNING' | 'ERROR' | 'FAILED' | 'CANCELLED';
export const jobStatus: JobStatus[] = ['PENDING', 'RUNNING', 'SUCCESS', 'WARNING', 'ERROR', 'FAILED', 'CANCELLED'];
export const jobStatusI18nLabels: Record<JobStatus, string> = {
  /* eslint-disable @typescript-eslint/naming-convention */
  PENDING: 'SOCIAL.JOB.STATUS.PENDING',
  RUNNING: 'SOCIAL.JOB.STATUS.RUNNING',
  SUCCESS: 'SOCIAL.JOB.STATUS.SUCCESS',
  WARNING: 'SOCIAL.JOB.STATUS.WARNING',
  ERROR: 'SOCIAL.JOB.STATUS.ERROR',
  FAILED: 'SOCIAL.JOB.STATUS.FAILED',
  CANCELLED: 'SOCIAL.JOB.STATUS.CANCELLED',
  /* eslint-enable @typescript-eslint/naming-convention */
};

@EntityClass({ typename: 'JobVO' })
export class Job extends Entity<Job> {
  static fromObject: (source: any, opts?: any) => Job;

  name: string;
  type: JobType;
  status: JobStatus;
  userId: number;
  user: IntReferential;
  originId: string;
  origin: StrReferential;
  startDate: Moment;
  endDate: Moment;

  log: string;
  context: any;
  report: any;

  // internal properties
  jobResultLoaded = false;
  jobResult: JobResult;

  constructor() {
    super(Job.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    if (!target.userId && EntityUtils.isNotEmpty(this.user, 'id')) {
      target.userId = this.user.id;
    }
    if (!target.originId && EntityUtils.isNotEmpty(this.origin, 'id')) {
      target.originId = this.origin.id;
    }
    target.startDate = toDateISOString(this.startDate);
    target.endDate = toDateISOString(this.endDate);
    // Serialize context and report
    if (typeof this.context === 'object') {
      target.context = JSON.stringify(this.context);
    }
    if (typeof this.report === 'object') {
      target.report = JSON.stringify(this.report);
    }
    return target;
  }

  fromObject(source: any) {
    Object.assign(this, source); // Copy all properties
    super.fromObject(source);
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
    this.user = IntReferential.fromObject(source.user);
    this.origin = StrReferential.fromObject(source.origin);

    try {
      // Deserialize context and report
      if (typeof source.context === 'string' && source.context.startsWith('{')) {
        this.context = JSON.parse(source.context);
      }
      if (typeof source.report === 'string' && source.report.startsWith('{')) {
        this.report = JSON.parse(source.report);
      }
    } catch (err) {
      console.error('Error during UserEvent deserialization', err);
    }
  }

  isActive(): boolean {
    return JobUtils.isActive(this);
  }

  isSuccess(): boolean {
    return this.status === 'SUCCESS';
  }

  get downloadLink(): string {
    return this.jobResult?.report?.fileName;
  }
}

export class JobUtils {
  static isActive(job: Job): boolean {
    return ['PENDING', 'RUNNING'].includes(job?.status);
  }

  static isImport(job: Job): boolean {
    return (['IMPORT_ORDER_ITEM_SHAPE', 'IMPORT_MONITORING_LOCATION_SHAPE'] as JobType[]).includes(job.type);
  }

  static isExport(job: Job): boolean {
    return (['EXPORT_REFERENTIAL'] as JobType[]).includes(job.type);
  }

  static isExtraction(job: Job): boolean {
    return (['EXTRACTION_RESULT', 'EXTRACTION_IN_SITU'] as JobType[]).includes(job.type);
  }
}
