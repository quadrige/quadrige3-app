import { Injectable } from '@angular/core';
import { Job } from '@app/social/job/job.model';
import { ExportJobResult, ImportShapeJobResult, JobResult } from '@app/social/job/job-result.model';
import { uniqueValue } from '@app/shared/utils';

@Injectable({ providedIn: 'root' })
export class JobResultService {
  private logPrefix = '[job-result-service]';

  constructor() {}

  parseJobResult(job: Job): JobResult {
    if (!job) {
      console.error(`${this.logPrefix} No log to parse`);
      return undefined;
    }

    const result: JobResult = { job };

    // Determine job type
    switch (job.type) {
      case 'IMPORT_ORDER_ITEM_SHAPE':
        result.report = job.report as ImportShapeJobResult;
        result.files = this.enumerateFiles(result.report);
        break;
      case 'IMPORT_MONITORING_LOCATION_SHAPE':
        result.i18nPrefix = 'REFERENTIAL.MON_LOC_ORDER_ITEM.REPORT.';
        result.report = job.report as ImportShapeJobResult;
        result.files = this.enumerateFiles(result.report);
        break;
      case 'EXPORT_REFERENTIAL':
        result.report = job.report as ExportJobResult;
        break;
      case 'EXTRACTION_RESULT':
      case 'EXTRACTION_IN_SITU':
        result.report = this.parseExtractionResult(job.report as ExportJobResult);
        break;
    }

    // Choose icon
    switch (job.status) {
      case 'PENDING':
      case 'RUNNING':
        console.warn(`${this.logPrefix} Should not happened`);
        break;
      case 'SUCCESS':
        result.icon = { icon: 'checkmark-circle-outline', color: 'success' };
        break;
      case 'ERROR':
        result.icon = { icon: 'close-circle-outline', color: 'danger' };
        break;
      case 'WARNING':
        result.icon = { icon: 'warning-outline', color: 'warning' };
        break;
      case 'FAILED':
        result.icon = { icon: 'alert-circle-outline', color: 'danger' };
        break;
      case 'CANCELLED':
        result.icon = { icon: 'stop-circle-outline', color: 'danger' };
        break;
    }

    return result;
  }

  protected enumerateFiles(report: ImportShapeJobResult): string[] {
    // Enumerate files in import shape report
    return Object.keys(report.messages).concat(Object.keys(report.errors)).concat(Object.keys(report.reports)).filter(uniqueValue);
  }

  protected parseExtractionResult(report: ExportJobResult): ExportJobResult {
    // todo ?
    return report;
  }
}
