import { IconRef, ObjectMap } from '@sumaris-net/ngx-components';
import { MonitoringLocationReport } from '@app/referential/mon-loc-order-item/mon-loc-order-item.model';
import { Job } from '@app/social/job/job.model';

export interface JobResult {
  job: Job;
  report?: ImportShapeJobResult | ExportJobResult; // todo: add other interface depending on job type
  files?: string[];
  icon?: IconRef;
  i18nPrefix?: string;
}

/**
 * Model of import shape job
 * All properties map are indexed by (1)fileName and (2)featureId
 */
export interface ImportShapeJobResult {
  fileName: string;
  messages: ObjectMap<ObjectMap<string[]>>;
  errors: ObjectMap<ObjectMap<string[]>>;
  reports: ObjectMap<ObjectMap<MonitoringLocationReport>>;
}

export interface ExportJobResult {
  fileName: string;
  messages: string[];
  errors: string[];
  details: string[];
}
