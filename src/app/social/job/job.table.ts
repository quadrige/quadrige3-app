import { AfterViewInit, ChangeDetectionStrategy, Component, inject, Injector, model, OnDestroy, OnInit, signal } from '@angular/core';
import {
  APP_JOB_PROGRESSION_SERVICE,
  isEmptyArray,
  isNotNil,
  isNotNilOrBlank,
  JobProgression,
  LoadResult,
  PlatformService,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  sleep,
} from '@sumaris-net/ngx-components';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { BaseTable } from '@app/shared/table/base.table';
import { Job, jobStatusI18nLabels, jobTypesI18nLabels } from '@app/social/job/job.model';
import { JobService } from '@app/social/job/job.service';
import { accountConfigOptions } from '@app/core/config/account.config';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { Dates } from '@app/shared/dates';
import { JobResultForm } from '@app/social/job/result/job-result.form';
import { JobResultService } from '@app/social/job/job-result.service';
import { Alerts } from '@app/shared/alerts';
import { Observable, Subscription } from 'rxjs';
import { StrReferential } from '@app/referential/model/referential.model';
import { JobFilter, JobFilterCriteria } from '@app/social/job/filter/job.filter.model';
import { GenericReferentialFilter } from '@app/referential/generic/filter/generic.filter.model';
import { FileTransferService } from '@app/shared/service/file-transfer.service';
import { JobResult } from '@app/social/job/job-result.model';
import { setTimeout } from '@rx-angular/cdk/zone-less/browser';
import { EntityUtils } from '@app/shared/entity.utils';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { HasSomeActiveJobPipe, HasSomeInactiveJobPipe } from '@app/social/job/job.pipe';
import { JobProgressionForm } from '@app/social/job/progression/job-progression.form';
import { JobFilterForm } from '@app/social/job/filter/job.filter.form';

@Component({
  selector: 'app-job-table',
  templateUrl: './job.table.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule, JobResultForm, JobProgressionForm, JobFilterForm, HasSomeActiveJobPipe, HasSomeInactiveJobPipe],
})
export class JobTable extends BaseTable<Job, number, JobFilter, JobFilterCriteria> implements OnInit, AfterViewInit, OnDestroy {
  showAllUsers: boolean;
  origins: StrReferential[] = [];
  downloadLinkEnabled = signal(false);
  showJobResult = signal(false);
  jobResult = model<JobResult>();
  showJobProgression = signal(false);
  jobProgression$: Observable<JobProgression>;
  jobProgressionSubscription: Subscription;

  protected readonly jobResultService = inject(JobResultService);
  protected readonly jobProgressionService = inject(APP_JOB_PROGRESSION_SERVICE);
  protected readonly platform = inject(PlatformService);
  protected readonly fileService = inject(FileTransferService);

  constructor(
    protected injector: Injector,
    protected _entityService: JobService
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['name', 'type', 'status', 'user', 'origin', 'startDate', 'endDate', 'duration']).concat(RESERVED_END_COLUMNS),
      Job,
      _entityService,
      undefined
    );

    this.titleI18n = 'SOCIAL.JOB.TITLE';
    this.i18nColumnPrefix = 'SOCIAL.JOB.TABLE.';
    this.defaultSortBy = 'startDate';
    this.defaultSortDirection = 'desc';
    this.logPrefix = '[job-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent && !!this.singleSelectedRow;
  }

  typeDisplayFn = (data: Job) => this.translate.instant(jobTypesI18nLabels[data.type]);

  statusDisplayFn = (data: Job) => this.translate.instant(jobStatusI18nLabels[data.status]);

  durationDisplayFn = (data: Job) => Dates.duration(data.startDate, data.endDate);

  ngOnInit() {
    super.ngOnInit();
    this.inlineEdition = false;
    this.forceNoInlineEdition = true;

    this.showAllUsers = this.accountService.isAdmin() && this.settings.getPropertyAsBoolean(accountConfigOptions.showSocialAllUsers);

    this.referentialGenericService.loadPage({}, GenericReferentialFilter.fromObject({ entityName: 'JobOrigin' })).then((result) => {
      this.origins = result.data?.map((value) => StrReferential.fromObject(value));
    });
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.restoreRightPanel(true, 75);
  }

  async resetFilter(filter?: JobFilter, opts?: ISetFilterOptions) {
    filter = this.asFilter(filter);
    filter.patch({ allUsers: false });
    await super.resetFilter(filter, opts);
  }

  async updateView(res: LoadResult<Job> | undefined, opts?: { emitEvent?: boolean }): Promise<void> {
    await super.updateView(res, opts);

    if (res?.total === 0) {
      this.selection.clear();
    } else if (res?.total === 1) {
      // Select unique row
      this.selectRowById(0).then(() => {
        if (!this.rightPanelVisible) {
          this.toggleRightPanel(undefined);
        }
      });
    } else {
      // Try to re-select previous
      if (isNotNil(this.singleSelectedRow?.currentData)) setTimeout(() => this.selectRowByData(this.singleSelectedRow.currentData));
    }
  }

  async cancel(event?: Event, opts?: { interactive?: boolean; selectPreviousSelectedRow?: boolean }): Promise<void> {
    return super.cancel(event, { ...opts, selectPreviousSelectedRow: false });
  }

  async onAfterSelectionChange(row?: AsyncTableElement<Job>) {
    this.jobProgressionSubscription?.unsubscribe();
    await this.loadSelectedJobs();
    this.downloadLinkEnabled.set(this.selectedEntities.some((job) => isNotNilOrBlank(job.downloadLink)));
    await this.showJob(row);
    await super.onAfterSelectionChange(row);
  }

  async stopSelectedJob(event: UIEvent) {
    if (this.selection.isEmpty()) return;
    const confirmed = await Alerts.askYesNoConfirmation('SOCIAL.JOB.CONFIRM.STOP', this.alertCtrl, this.translate, event);
    if (confirmed) {
      const jobIds = this.selection.selected.map((row) => row.currentData.id);
      for (const jobId of jobIds) {
        await this._entityService.stopJob(jobId);
      }
    }
  }

  async downloadResult(event: UIEvent) {
    event?.preventDefault();
    const links = this.getDownloadLinks();
    if (isEmptyArray(links)) return;
    if (links.length >= 20) {
      if (!(await Alerts.askYesNoConfirmation('SOCIAL.JOB.CONFIRM.DOWNLOAD_ALL', this.alertCtrl, this.translate, event))) return;
    }
    for (const fileUrl of links.map((link) => this.fileService.downloadFile(link))) {
      if (this.debug) console.debug(`${this.logPrefix} downloading result:`, fileUrl);
      this.platform.download({ uri: fileUrl });
      await sleep(1000); // Need a sleep to allow browser to manage multiple downloads
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.jobProgressionSubscription?.unsubscribe();
  }

  // protected methods

  protected getDownloadLinks(): string[] {
    return this.selectedEntities.map((job) => job.downloadLink).filter(isNotNilOrBlank);
  }

  protected async loadSelectedJobs() {
    this.markAsLoading();
    for (const row of this.selection.selected) {
      if (EntityUtils.isNotEmpty(row.currentData, 'id') && !row.currentData.jobResultLoaded) {
        const job = await this._entityService.load(row.currentData.id);
        if (!job) {
          console.error(`${this.logPrefix} Can't load job with id=${row.currentData.id}`);
          continue;
        }
        if (!job.isActive()) {
          job.jobResult = this.jobResultService.parseJobResult(job);
          job.jobResultLoaded = true;
        }
        // Affect to row
        row.currentData = job;
      }
    }
    this.markAsLoaded();
  }

  protected async showJob(row: AsyncTableElement<Job>) {
    if (!row) {
      this.showJobProgression.set(false);
      this.showJobResult.set(false);
      this.markForCheck();
      return;
    }
    const job = row.currentData;
    if (job.isActive()) {
      // Listen to progression
      this.jobProgressionSubscription?.unsubscribe();
      this.jobProgression$ = this.jobProgressionService.listenChanges(job.id);
      this.jobProgressionSubscription = this.jobProgression$.subscribe(() => this.markForCheck());
      this.showJobProgression.set(true);
      this.showJobResult.set(false);
    } else {
      // Set the job result
      this.jobResult.set(this.jobResultService.parseJobResult(job));
      this.showJobResult.set(true);
      this.showJobProgression.set(false);
    }
    this.markForCheck();
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().concat('comments');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('name');
  }
}
