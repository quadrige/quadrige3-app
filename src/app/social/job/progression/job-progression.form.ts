import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DateUtils, JobModule, JobProgression } from '@sumaris-net/ngx-components';
import { Job } from '@app/social/job/job.model';
import { interval, Observable, Subscription } from 'rxjs';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-job-progression-form',
  templateUrl: './job-progression.form.html',
  styleUrls: ['./job-progression.form.scss'],
  standalone: true,
  imports: [QuadrigeCoreModule, JobModule],
})
export class JobProgressionForm implements OnInit, OnDestroy {
  @Input() job: Job;
  @Input() jobProgression$: Observable<JobProgression>;

  now = DateUtils.moment;
  timerSubscription: Subscription;

  constructor(protected cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.timerSubscription = interval(1000).subscribe(() => {
      this.cd.markForCheck();
    });
  }

  ngOnDestroy(): void {
    this.timerSubscription?.unsubscribe();
  }
}
