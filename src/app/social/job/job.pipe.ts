import { SelectionModel } from '@angular/cdk/collections';
import { AbstractSelectionModelPipe } from '@sumaris-net/ngx-components';
import { ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { Job, JobUtils } from '@app/social/job/job.model';
import { AsyncTableElement } from '@e-is/ngx-material-table';

@Pipe({
  name: 'hasSomeActiveJob',
  pure: false,
  standalone: true,
})
export class HasSomeActiveJobPipe extends AbstractSelectionModelPipe<AsyncTableElement<Job>, boolean> implements PipeTransform {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }

  protected _transform(selection: SelectionModel<AsyncTableElement<Job>>): boolean {
    return selection.selected.map((row) => row.currentData).some((job) => JobUtils.isActive(job));
  }
}

@Pipe({
  name: 'hasSomeInactiveJob',
  pure: false,
  standalone: true,
})
export class HasSomeInactiveJobPipe extends AbstractSelectionModelPipe<AsyncTableElement<Job>, boolean> implements PipeTransform {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }

  protected _transform(selection: SelectionModel<AsyncTableElement<Job>>): boolean {
    return selection.selected.map((row) => row.currentData).some((job) => !JobUtils.isActive(job));
  }
}
