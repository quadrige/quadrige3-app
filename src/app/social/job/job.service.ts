import {
  AccountService,
  BaseEntityGraphqlSubscriptions,
  BaseEntityService,
  EntitiesServiceLoadOptions,
  EntitiesServiceWatchOptions,
  ENVIRONMENT,
  Environment,
  isNilOrNaN,
  LoadResult,
  Page,
  PlatformService,
  SocialErrorCodes,
} from '@sumaris-net/ngx-components';
import { Inject, Injectable, Injector, Optional } from '@angular/core';
import gql from 'graphql-tag';
import { BehaviorSubject, finalize, Observable, Subject, takeUntil } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Job } from '@app/social/job/job.model';
import { UpdatableView } from '@app/shared/model/updatable-view';
import { ToastController, ToastOptions } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ReferentialEntityGraphqlQueries, referentialFragments } from '@app/referential/service/base-referential.service';
import { SortDirection } from '@angular/material/sort';
import { EntityUtils } from '@app/shared/entity.utils';
import { referentialErrorCodes } from '@app/referential/service/errors';
import { BaseEntityGraphqlMutations } from '@sumaris-net/ngx-components/src/app/core/services/base-entity-service.class';
import { StableGraphqlService } from '@app/shared/service/stable-graphql.service';
import { IBaseEntitiesService, IExportContext } from '@app/shared/service/base-entity.service';
import { JobFilter } from '@app/social/job/filter/job.filter.model';
import { tapOnce } from '@app/shared/observable.utils';
import { Dates } from '@app/shared/dates';

export const jobFragments = {
  job: gql`
    fragment JobFragment on JobVO {
      id
      name
      type
      status
      userId
      user {
        ...ReferentialFragment
      }
      originId
      origin {
        ...ReferentialFragment
      }
      startDate
      endDate
      updateDate
      context
      report
      log
    }
    ${referentialFragments.light}
  `,
  lightJob: gql`
    fragment LightJobFragment on JobVO {
      id
      name
      type
      status
      userId
      user {
        ...ReferentialFragment
      }
      originId
      origin {
        ...ReferentialFragment
      }
      startDate
      endDate
      updateDate
    }
    ${referentialFragments.light}
  `,
};

const queries: ReferentialEntityGraphqlQueries = {
  loadAll: gql`
    query Jobs($page: PageInput, $filter: JobFilterVOInput) {
      data: jobs(page: $page, filter: $filter) {
        ...LightJobFragment
      }
    }
    ${jobFragments.lightJob}
  `,
  loadAllWithTotal: gql`
    query JobsWithCount($page: PageInput, $filter: JobFilterVOInput) {
      data: jobs(page: $page, filter: $filter) {
        ...LightJobFragment
      }
      total: jobsCount(filter: $filter)
    }
    ${jobFragments.lightJob}
  `,
  countAll: gql`
    query JobsCount($filter: JobFilterVOInput) {
      total: jobsCount(filter: $filter)
    }
  `,
  load: gql`
    query Job($id: Int!) {
      data: job(id: $id) {
        ...JobFragment
      }
    }
    ${jobFragments.job}
  `,
};

const mutations: BaseEntityGraphqlMutations = {
  deleteAll: gql`
    mutation DeleteJob($ids: [Int]) {
      deleteJobs(ids: $ids)
    }
  `,
};

const subscriptions: BaseEntityGraphqlSubscriptions = {
  listenChanges: gql`
    subscription UpdateJobs($filter: JobFilterVOInput!, $interval: Int) {
      data: updateJobs(filter: $filter, interval: $interval) {
        ...LightJobFragment
      }
    }
    ${jobFragments.lightJob}
  `,
};

const otherQueries = {
  stop: gql`
    query StopJob($id: Int) {
      data: stopJob(id: $id)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class JobService extends BaseEntityService<Job, JobFilter> implements IBaseEntitiesService<Job, JobFilter, any> {
  private readonly watchQueryName = 'LoadAllWithTotal';

  constructor(
    protected injector: Injector,
    protected graphql: StableGraphqlService,
    @Optional() @Inject(ENVIRONMENT) protected environment: Environment,
    protected toastController: ToastController,
    protected translate: TranslateService,
    protected accountService: AccountService
  ) {
    super(graphql, injector.get(PlatformService), Job, JobFilter, { queries, mutations, subscriptions });
    this._logPrefix = '[job-service]';
    this._debug = !environment.production;

    // Reset last watch variable on logout
    // this.registerSubscription(this.accountService.onLogout.subscribe(() => (this.lastWatchVariable = null)));
  }

  exportAllAsync(context: IExportContext, jobFilter: JobFilter): Promise<Job> {
    throw new Error('NOT IMPLEMENTED');
  }

  load(id: number, opts?: EntitiesServiceLoadOptions): Promise<Job> {
    return super.load(id, { ...opts, fetchPolicy: 'no-cache' });
  }

  watchAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    jobFilter?: JobFilter,
    opts?: EntitiesServiceWatchOptions
  ): Observable<LoadResult<Job>> {
    return this.watchPage({ offset, size, sortBy, sortDirection }, jobFilter, opts);
  }

  watchPage(page: Partial<Page>, jobFilter?: JobFilter, opts?: EntitiesServiceWatchOptions): Observable<LoadResult<Job>> {
    jobFilter = this.asFilter(jobFilter);
    page = this.asPage(page);

    const variables = {
      page: {
        ...page,
        sortDirection: page?.sortDirection?.toUpperCase(),
      },
      filter: jobFilter.asPodObject(),
    };

    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix} Loading ${this._logTypeName}...`, variables);

    // Prepare listen observable
    const listenStop$ = new Subject<boolean>();

    return this.mutableWatchQuery<LoadResult<any>>({
      queryName: this.watchQueryName,
      query: this.queries.loadAllWithTotal,
      arrayFieldName: 'data',
      totalFieldName: 'total',
      insertFilterFn: jobFilter.asFilterFn(),
      sortFn: EntityUtils.entitySortComparator(page.sortBy, page.sortDirection),
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: opts?.fetchPolicy || 'network-only',
    }).pipe(
      map((res) /*({ data, total })*/ => {
        if (!res) {
          console.warn(`${this._logPrefix} no result for:`, this.watchQueryName, variables);
          return { data: [], total: 0 };
        }
        const data = res.data;
        const total = res.total;
        // Convert to entity (if needed)
        const entities = !opts || opts.toEntity !== false ? (data || []).map((json) => this.fromObject(json)) : ((data || []) as Job[]);
        if (this._debug) console.debug(`${this._logPrefix} ${this._logTypeName} loaded in ${Date.now() - now}ms`, entities);
        return { data: entities, total };
      }),
      tapOnce((res) => {
        // Compute first filter with actual last update date
        const listenFilter = jobFilter.asPodObject();
        listenFilter.criterias[0].lastUpdateDate = Dates.max(res.data?.map((job: any) => job.updateDate));
        this.listenAllChanges(listenFilter)
          .pipe(takeUntil(listenStop$) /*, debounceTime(1000)*/)
          .subscribe(() => {
            if (this._debug) console.debug(`${this._logPrefix} [WS] Refetching jobs`);
            this.refetchMutableWatchQueries({ queryName: this.watchQueryName, variables });
          });
      }),
      finalize(() => listenStop$.next(true))
    );
  }

  listenAllChanges(jobFilter: Partial<JobFilter>): Observable<Job[]> {
    jobFilter = this.asFilter(jobFilter);

    if (this._debug) console.debug(`${this._logPrefix} [WS] Listening changes for jobs with filter:`, jobFilter);

    return this.graphql
      .subscribe<{ data: any[] }, { filter: any; interval: number }>({
        query: this.subscriptions.listenChanges,
        fetchPolicy: 'no-cache',
        variables: { filter: jobFilter.asPodObject(), interval: 10 },
        error: {
          code: SocialErrorCodes.SUBSCRIBE_USER_EVENTS_ERROR,
          message: 'SOCIAL.ERROR.SUBSCRIBE_USER_EVENTS_ERROR',
        },
      })
      .pipe(
        finalize(() => {
          if (this._debug) console.debug(`${this._logPrefix} [WS] Stop Listening changes for jobs with filter:`, jobFilter);
        }),
        map(({ data }) => {
          if (data && this._debug) console.debug(`${this._logPrefix} [WS] Received new jobs:`, data);
          return data?.map(Job.fromObject);
        })
      );
  }

  listenChanges(id: number): Observable<Job> {
    return this.listenAllChanges(JobFilter.fromObject({ criterias: [{ id }] })).pipe(map((jobs) => jobs?.[0]));
  }

  listenImportShapeJob(id: number, dirtySubject: BehaviorSubject<boolean>, view: UpdatableView) {
    this.listenChanges(id)
      .pipe(filter((job) => !job.isActive()))
      .subscribe((job) => {
        const toastOption: ToastOptions = {
          position: 'top',
          buttons: [{ role: 'cancel', side: 'end', icon: 'close-outline' }],
          translucent: true,
        };
        if (job.isSuccess()) {
          if (dirtySubject.getValue()) {
            toastOption.color = 'warning';
            toastOption.message = this.translate.instant('IMPORT_SHAPE.STATUS.IMPORTED_TABLE_DIRTY');
          } else {
            toastOption.color = 'secondary';
            toastOption.message = this.translate.instant('IMPORT_SHAPE.STATUS.IMPORTED');
            view.doRefresh();
          }
        } else {
          toastOption.color = 'danger';
          toastOption.message = this.translate.instant('IMPORT_SHAPE.STATUS.ERROR');
          console.error(`${this._logPrefix} uploadShapeFile failed`, job.report.errors);
        }
        this.toastController.create(toastOption).then((toast) => toast.present());
      });
  }

  async stopJob(jobId: number) {
    if (isNilOrNaN(jobId)) {
      console.warn(`${this._logPrefix} Can't stop job: undefined jobId`);
      return;
    }

    const variables: any = { id: jobId };
    if (this._debug) console.debug(`${this._logPrefix} Stopping job...`, variables);
    const now = Date.now();
    const query = otherQueries.stop;
    const res = await this.graphql.query<{ data: boolean }>({
      query,
      variables,
      error: { code: referentialErrorCodes.load, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR' },
      fetchPolicy: 'network-only',
    });
    const { data } = res;
    if (this._debug) console.debug(`${this._logPrefix} Job (id=${jobId}) has ${!data ? 'NOT ' : ''}been stopped in ${Date.now() - now}ms`);
  }

  protected asPage(page: Partial<Page>): Page {
    return {
      offset: page.offset || 0,
      size: page.size || 100,
      sortBy: page.sortBy || this.defaultSortBy.toString(),
      sortDirection: page.sortDirection || this.defaultSortDirection,
    };
  }

  asFilter(source: any): JobFilter {
    const jobFilter = super.asFilter(source || {});
    jobFilter.criterias?.forEach((criteria) => (criteria.userId = this.accountService.account.id));
    return jobFilter;
  }
}
