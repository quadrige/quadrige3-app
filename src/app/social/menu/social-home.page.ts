import { Component } from '@angular/core';
import { SocialMenuModel } from '@app/social/menu/social-menu.model';
import { MenuPage } from '@app/shared/component/menu/menu.page';

@Component({
  selector: 'app-social-home-page',
  templateUrl: 'social-home.page.html',
})
export class SocialHomePage extends MenuPage {
  constructor() {
    super(SocialMenuModel.socialMenuItems());
  }
}
