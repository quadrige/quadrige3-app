import { IMenuItem } from '@sumaris-net/ngx-components';

const jobMenu: IMenuItem = {
  title: 'MENU.SOCIAL.JOB',
  path: '/social/Job',
};
const userEventMenu: IMenuItem = {
  title: 'MENU.SOCIAL.USER_EVENT',
  path: '/social/UserEvent',
};

export class SocialMenuModel {
  static socialMenuItems(): IMenuItem[] {
    return [jobMenu, userEventMenu];
  }
}
