import { AfterViewInit, Component, inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  AccountService,
  APP_JOB_PROGRESSION_SERVICE,
  APP_USER_EVENT_SERVICE,
  ENVIRONMENT,
  IUserEventAction,
  JobProgressionIcon,
  JobProgressionOptions,
  PlatformService,
  SocialModule,
  toBoolean,
  UserEventNotificationIcon,
} from '@sumaris-net/ngx-components';
import { JobService } from '@app/social/job/job.service';
import { Job, JobUtils } from '@app/social/job/job.model';
import { UserEvent } from '@app/social/user-event/user-event.model';
import { IJobResultModalOptions, JobResultModal } from '@app/social/job/result/job-result.modal';
import { TranslateService } from '@ngx-translate/core';
import { ExportJobResult } from '@app/social/job/job-result.model';
import { FileTransferService } from '@app/shared/service/file-transfer.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JobResultService } from '@app/social/job/job-result.service';
import { JobFilter } from '@app/social/job/filter/job.filter.model';
import moment from 'moment';
import { ModalService } from '@app/shared/component/modal/modal.service';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';

@Component({
  selector: 'app-social-component',
  templateUrl: './social.component.html',
  standalone: true,
  imports: [QuadrigeCoreModule, SocialModule],
})
export class SocialComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(JobProgressionIcon) jobProgression: JobProgressionIcon;
  @ViewChild(UserEventNotificationIcon) userEvenNotification: UserEventNotificationIcon;

  jobProgressionOptions = <JobProgressionOptions>{
    autoHide: true,
  };
  debug: boolean;
  private logPrefix = '[social-component]';
  private listenJobsSubscription: Subscription;

  protected readonly userEventService = inject(APP_USER_EVENT_SERVICE);
  protected readonly jobProgressionService = inject(APP_JOB_PROGRESSION_SERVICE);
  protected readonly jobService = inject(JobService);
  protected readonly jobResultService = inject(JobResultService);
  protected readonly accountService = inject(AccountService);
  protected readonly translate = inject(TranslateService);
  protected readonly modalService = inject(ModalService);
  protected readonly fileService = inject(FileTransferService);
  protected readonly platform = inject(PlatformService);
  protected readonly router = inject(Router);
  protected readonly environment = inject(ENVIRONMENT);

  constructor() {
    this.debug = toBoolean(!this.environment?.production, true);
  }

  async ngOnInit() {
    // await this.socialService.ready();
    await this.jobService.ready();
    await this.accountService.ready();

    // Get current running jobs
    await this.listenJobs();
  }

  ngAfterViewInit() {
    // Add behavior on all job events
    this.userEventService.registerListener({
      accept: (userEvent: UserEvent) => this.isJobEvent(userEvent),
      onReceived: (userEvent: UserEvent) => {
        // override message
        const job = Job.fromObject(userEvent.content);
        userEvent.message = this.translate.instant('SOCIAL.JOB.STATUS_WITH_NAME.' + job.status, { name: job.name });
        // override avatarIcon
        if (userEvent.level === 'INFO') {
          if (JobUtils.isImport(job)) {
            userEvent.avatarIcon = { matIcon: 'file_upload' };
          } else if (JobUtils.isExport(job) || JobUtils.isExtraction(job)) {
            userEvent.avatarIcon = { matIcon: 'file_download' };
          }
        }
        // override icon
        userEvent.icon = { icon: 'time-outline' };
        return userEvent;
      },
    });
    // Add action on finished jobs
    this.userEventService.registerListener({
      accept: (userEvent: UserEvent) => this.isFinishedJobEvent(userEvent),
      onReceived: (userEvent: UserEvent) => {
        const job = Job.fromObject(userEvent.content);
        if (JobUtils.isImport(job)) {
          userEvent.actions = [this.createShowResultAction(job)];
        } else if (JobUtils.isExport(job) || JobUtils.isExtraction(job)) {
          if (job.status === 'SUCCESS') {
            if (JobUtils.isExtraction(job)) {
              userEvent.actions = [this.createShowResultAction(job), this.createDownloadAction(job)];
            } else {
              userEvent.actions = [this.createDownloadAction(job)];
            }
          } else if (['FAILED', 'ERROR', 'WARNING'].includes(job.status)) {
            userEvent.actions = [this.createShowResultAction(job)];
          }
        }
        return userEvent;
      },
    });
  }

  ngOnDestroy(): void {
    this.listenJobsSubscription?.unsubscribe();
    this.listenJobsSubscription = null;
  }

  showNotificationTable = () => {
    if (this.debug) console.debug(`${this.logPrefix} Will open notification table`);
    this.router.navigateByUrl('social/UserEvent', { replaceUrl: true });
  };

  showJobTable = () => {
    if (this.debug) console.debug(`${this.logPrefix} Will open job table`);
    this.router.navigateByUrl('social/Job', { replaceUrl: true });
  };

  protected createShowResultAction(job: Job): IUserEventAction {
    return {
      name: this.translate.instant('SOCIAL.JOB.ACTIONS.SHOW_RESULT'),
      iconRef: {
        icon: 'arrow-forward-outline',
        color: 'success',
      },
      executeAction: () => this.openJobResult(job),
    };
  }

  protected createDownloadAction(job: Job): IUserEventAction {
    return {
      name: this.translate.instant('SOCIAL.JOB.ACTIONS.DOWNLOAD_RESULT'),
      iconRef: {
        icon: 'arrow-forward-outline',
        color: 'success',
      },
      executeAction: () => {
        const report = job.report as ExportJobResult;
        const fileUrl = this.fileService.downloadFile(report.fileName);
        this.platform.download({ uri: fileUrl });
      },
    };
  }

  protected async listenJobs() {
    if (this.listenJobsSubscription != null) {
      return;
    }
    const lastUpdateDate = moment().add(-1, 'day');
    const filter = JobFilter.fromObject({ criterias: [{ lastUpdateDate }] });
    this.listenJobsSubscription = this.jobService.listenAllChanges(filter).subscribe((jobs) => {
      jobs?.forEach((job) => {
        if (job.isActive()) {
          console.debug(`${this.logPrefix} job added`, job.id);
          this.jobProgressionService.addJob(job.id);
        } else {
          console.debug(`${this.logPrefix} job removed`, job.id);
          this.jobProgressionService.removeJob(job.id);
        }
      });
    });
  }

  protected isJobEvent(event: UserEvent): boolean {
    if (event.type === 'JOB') {
      const jobId = event.content?.id;
      if (jobId) {
        return true;
      } else {
        console.error(`${this.logPrefix} No job id found in this job event`, event);
        return false;
      }
    }
  }

  protected isFinishedJobEvent(event: UserEvent): boolean {
    if (!this.isJobEvent(event)) return false;
    const job = Job.fromObject(event.content);
    return !job.isActive();
  }

  protected async openJobResult(job: Job) {
    await this.modalService.openModal<IJobResultModalOptions, void>(
      JobResultModal,
      {
        jobResult: this.jobResultService.parseJobResult(job),
      },
      'modal-medium'
    );
  }
}
