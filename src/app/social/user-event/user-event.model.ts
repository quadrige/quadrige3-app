import {
  Entity,
  EntityAsObjectOptions,
  EntityClass,
  EventLevel,
  fromDateISOString,
  IconRef,
  IUserEvent,
  IUserEventAction,
  toDateISOString,
} from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { IntReferential } from '@app/referential/model/referential.model';

export const userEventLevels: EventLevel[] = ['ERROR', 'WARNING', 'INFO', 'DEBUG'];
export const userEventLevelI18nLabels: Record<EventLevel, string> = {
  /* eslint-disable @typescript-eslint/naming-convention */
  ERROR: 'SOCIAL.USER_EVENT.LEVEL.ERROR',
  WARNING: 'SOCIAL.USER_EVENT.LEVEL.WARNING',
  INFO: 'SOCIAL.USER_EVENT.LEVEL.INFO',
  DEBUG: 'SOCIAL.USER_EVENT.LEVEL.DEBUG',
  /* eslint-enable @typescript-eslint/naming-convention */
};

@EntityClass({ typename: 'UserEventVO' })
export class UserEvent extends Entity<UserEvent> implements IUserEvent<UserEvent, number, number> {
  static fromObject: (source: any, opts?: any) => UserEvent;

  type: string;
  level: EventLevel;
  issuer: number;
  recipient: number;
  recipientUser: IntReferential;
  creationDate: Moment;
  readDate: Moment;

  message: string;
  content: any;

  avatar: string;
  avatarIcon: IconRef;
  icon: IconRef;

  actions: IUserEventAction[];

  constructor() {
    super(UserEvent.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.creationDate = toDateISOString(this.creationDate);
    target.readDate = toDateISOString(this.readDate);

    // Serialize content
    if (typeof this.content === 'object') {
      target.content = JSON.stringify(this.content);
    }

    delete target.avatar;
    delete target.avatarIcon;
    delete target.icon;
    delete target.actions;

    return target;
  }

  fromObject(source: any) {
    Object.assign(this, source); // Copy all properties
    super.fromObject(source);
    this.recipientUser = IntReferential.fromObject(source.recipientUser);
    this.creationDate = fromDateISOString(source.creationDate);
    this.readDate = fromDateISOString(source.readDate);

    try {
      // Deserialize content
      if (typeof source.content === 'string' && source.content.startsWith('{')) {
        this.content = JSON.parse(source.content);
      }
    } catch (err) {
      console.error('Error during UserEvent deserialization', err);
    }
  }
}
