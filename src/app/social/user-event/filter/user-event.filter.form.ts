import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PartialRecord } from '@app/shared/model/interface';
import { BaseCriteriaFormComponent } from '@app/shared/component/filter/base-criteria-form.component';
import { isNotNil } from '@sumaris-net/ngx-components';
import { UserEventFilter, UserEventFilterCriteria } from '@app/social/user-event/filter/user-event.filter.model';
import { userEventLevelI18nLabels, userEventLevels } from '@app/social/user-event/user-event.model';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { BaseFilterFormComponent } from '@app/shared/component/filter/base-filter-form.component';
import { ReferentialFilterComponentModule } from '@app/referential/component/referential-filter-component.module';
import { ReferentialFilterFormField } from '@app/referential/component/referential-filter-form-field.component';

@Component({
  selector: 'app-user-event-filter-form',
  templateUrl: './user-event.filter.form.html',
  standalone: true,
  imports: [QuadrigeCoreModule, BaseFilterFormComponent, ReferentialFilterComponentModule, ReferentialFilterFormField],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserEventFilterForm extends BaseCriteriaFormComponent<UserEventFilter, UserEventFilterCriteria> {
  @Input() showAllUsers: boolean;

  userEventLevels = userEventLevels;
  userEventLevelI18nLabels = userEventLevelI18nLabels;

  constructor() {
    super();
  }

  async setValue(value: UserEventFilter, opts?: ISetFilterOptions) {
    if (!this.showAllUsers && value?.criterias?.[0]?.allRecipients) {
      value.criterias[0].allRecipients = false;
    }
    await super.setValue(value, opts);
  }

  protected getSearchAttributes(): string[] {
    return undefined;
  }

  protected criteriaToQueryParams(criteria: UserEventFilterCriteria): PartialRecord<keyof UserEventFilterCriteria, any> {
    const params = super.criteriaToQueryParams(criteria);
    this.subCriteriaToQueryParams(params, criteria, 'recipientUserFilter');
    if (isNotNil(criteria.level)) params.level = criteria.level;
    if (this.showAllUsers && isNotNil(criteria.allRecipients)) params.allRecipients = criteria.allRecipients;
    return params;
  }

  protected queryParamsToCriteria(params: string): UserEventFilterCriteria {
    const criteria = super.queryParamsToCriteria(params);
    if (!this.showAllUsers) criteria.allRecipients = false;
    return criteria;
  }
}
