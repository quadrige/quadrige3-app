import {
  EntityAsObjectOptions,
  EntityClass,
  EventLevel,
  FilterFn,
  fromDateISOString,
  isNil,
  isNotEmptyArray,
  isNotNil,
  IUserEventFilter,
  toBoolean,
} from '@sumaris-net/ngx-components';
import { Moment } from 'moment';
import { IntReferentialFilterCriteria } from '@app/referential/model/referential.filter.model';
import { UserEvent } from '@app/social/user-event/user-event.model';
import { BaseEntityFilter, BaseEntityFilterCriteria, BaseFilterUtils } from '@app/shared/model/filter.model';

@EntityClass({ typename: 'UserEventFilterCriteriaVO' })
export class UserEventFilterCriteria extends BaseEntityFilterCriteria<UserEvent, number> {
  static fromObject: (source: any, opts?: any) => UserEventFilterCriteria;

  includedIds: number[] = null;
  types: string[] = null;
  level: EventLevel = null;
  levels: EventLevel[] = null;
  issuers: number[] = null;
  recipients: number[] = null;
  recipient: number = null;
  allRecipients: boolean = null;
  recipientUserFilter: IntReferentialFilterCriteria = null;
  startDate: Moment = null;
  excludeRead: boolean = null;

  constructor() {
    super(UserEventFilterCriteria.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.types = source.types || [];
    this.levels = source.levels || [];
    this.level = source.level;
    this.issuers = source.issuers || [];
    this.recipients = !!source.recipients ? source.recipients : !!source.recipient ? [source.recipient] : [];
    this.allRecipients = toBoolean(source.allRecipients, false);
    this.recipientUserFilter = IntReferentialFilterCriteria.fromObject(source.recipientUserFilter || {});
    this.startDate = fromDateISOString(source.startDate);
    this.excludeRead = source.excludeRead || false;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.levels = isNotEmptyArray(this.levels) ? this.levels : isNotNil(this.level) ? [this.level] : undefined;
    target.recipients = isNotEmptyArray(this.recipients) ? this.recipients : isNotNil(this.recipient) ? [this.recipient] : undefined;
    target.recipientUserFilter = this.recipientUserFilter?.asObject(opts);
    if (opts?.minify) {
      delete target.level;
      delete target.recipient;
    }
    return target;
  }

  protected isCriteriaNotEmpty(key: string, value: any): boolean {
    if (key === 'allRecipients' && value === false) return false;
    if (key === 'recipientUserFilter') return !BaseFilterUtils.isCriteriaEmpty(this.recipientUserFilter, true);
    if (key === 'excludeRead' && value === false) return false;
    return super.isCriteriaNotEmpty(key, value);
  }

  protected buildFilter(): FilterFn<UserEvent>[] {
    const filterFns = super.buildFilter();

    if (isNotEmptyArray(this.types)) {
      filterFns.push((t) => this.types.includes(t.type));
    }
    if (isNotEmptyArray(this.levels)) {
      filterFns.push((t) => this.levels.includes(t.level));
    } else if (isNotNil(this.level)) {
      filterFns.push((t) => this.level === t.level);
    }
    if (isNotEmptyArray(this.issuers)) {
      filterFns.push((t) => this.issuers.includes(t.issuer));
    }
    if (!this.allRecipients) {
      if (isNotEmptyArray(this.recipients)) {
        filterFns.push((t) => this.recipients.includes(t.recipient));
      } else if (this.recipient) {
        filterFns.push((t) => this.recipient === t.recipient);
      }
    } else {
      if (this.recipientUserFilter) {
        const filter = this.recipientUserFilter.asFilterFn();
        if (filter) filterFns.push((data) => filter(data.recipientUser));
      }
    }

    if (isNotNil(this.startDate)) {
      filterFns.push((t) => this.startDate.isSameOrBefore(t.creationDate));
    }
    if (this.excludeRead === true) {
      filterFns.push((t) => isNil(t.readDate));
    }

    return filterFns;
  }
}

@EntityClass({ typename: 'UserEventFilterVO' })
export class UserEventFilter
  extends BaseEntityFilter<UserEventFilter, UserEventFilterCriteria, UserEvent>
  implements IUserEventFilter<UserEvent, number, number>
{
  static fromObject: (source: any, opts?: any) => UserEventFilter;

  constructor() {
    super(UserEventFilter.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    delete target.systemId;
    return target;
  }

  get excludeRead(): boolean {
    return this.criterias?.[0]?.excludeRead;
  }

  get includedIds(): number[] {
    return this.criterias?.[0]?.includedIds;
  }

  get issuers(): number[] {
    return this.criterias?.[0]?.issuers;
  }

  get levels(): EventLevel[] {
    return this.criterias?.[0]?.levels;
  }

  get recipients(): number[] {
    return this.criterias?.[0]?.recipients;
  }

  set recipients(recipients: number[]) {
    this.criterias[0].recipients = recipients;
  }

  get startDate(): Moment {
    return this.criterias?.[0]?.startDate;
  }

  set startDate(startDate: Moment) {
    this.criterias[0].startDate = startDate;
  }

  get types(): string[] {
    return this.criterias?.[0]?.types;
  }

  criteriaFromObject(source: any, opts: any): UserEventFilterCriteria {
    return UserEventFilterCriteria.fromObject(source, opts);
  }
}
