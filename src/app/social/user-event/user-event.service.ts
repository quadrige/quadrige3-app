import { Inject, Injectable, Optional } from '@angular/core';
import {
  AbstractUserEventService,
  AccountService,
  Environment,
  ENVIRONMENT,
  isEmptyArray,
  IUserEventMutations,
  IUserEventQueries,
  IUserEventSubscriptions,
  LoadResult,
  NetworkService,
  Page,
  UserEventWatchOptions,
} from '@sumaris-net/ngx-components';
import { TranslateService } from '@ngx-translate/core';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { FetchPolicy } from '@apollo/client/core';
import { UserEvent } from '@app/social/user-event/user-event.model';
import { referentialFragments } from '@app/referential/service/base-referential.service';
import { Job } from '@app/social/job/job.model';
import moment from 'moment';
import { StableGraphqlService } from '@app/shared/service/stable-graphql.service';
import { IBaseEntitiesService, IExportContext } from '@app/shared/service/base-entity.service';
import { UserEventFilter } from '@app/social/user-event/filter/user-event.filter.model';

// fixme: fragment renamed because UserEventFragment exists in ngx-components
const userEventFragment = gql`
  fragment QuadrigeUserEventFragment on UserEventVO {
    id
    issuer
    recipient
    recipientUser {
      ...ReferentialFragment
    }
    level
    type
    message
    content
    creationDate
    readDate
    updateDate
    __typename
  }
  ${referentialFragments.light}
`;

const queries: IUserEventQueries = {
  loadAll: gql`
    query UserEvents($filter: UserEventFilterVOInput, $page: PageInput) {
      data: userEvents(filter: $filter, page: $page) {
        ...QuadrigeUserEventFragment
      }
      total: userEventsCount(filter: $filter)
    }
    ${userEventFragment}
  `,
  loadAllWithContent: gql`
    query UserEventsWithContent($filter: UserEventFilterVOInput, $page: PageInput) {
      data: userEvents(filter: $filter, page: $page) {
        ...QuadrigeUserEventFragment
      }
      total: userEventsCount(filter: $filter)
    }
    ${userEventFragment}
  `,
  count: gql`
    query UserEventsCount($filter: UserEventFilterVOInput) {
      total: userEventsCount(filter: $filter)
    }
  `,
};

const mutations: IUserEventMutations = {
  save: gql`
    mutation SaveUserEvent($data: UserEventVOInput) {
      data: saveUserEvent(userEvent: $data) {
        ...QuadrigeUserEventFragment
      }
    }
    ${userEventFragment}
  `,
  markAsRead: gql`
    mutation AcknowledgeUserEvents($ids: [Int]) {
      acknowledgeUserEvents(ids: $ids)
    }
  `,
  deleteByIds: gql`
    mutation DeleteUserEvents($ids: [Int]) {
      deleteUserEvents(ids: $ids)
    }
  `,
};

const subscriptions: IUserEventSubscriptions = {
  listenAllChanges: gql`
    subscription UpdateUserEvents($filter: UserEventFilterVOInput, $interval: Int) {
      data: updateUserEvents(filter: $filter, interval: $interval) {
        ...QuadrigeUserEventFragment
      }
    }
    ${userEventFragment}
  `,
  listenCountChanges: gql`
    subscription UpdateUserEventsCount($filter: UserEventFilterVOInput, $interval: Int) {
      total: updateUserEventsCount(filter: $filter, interval: $interval)
    }
  `,
};

@Injectable()
export class UserEventService
  extends AbstractUserEventService<UserEvent, UserEventFilter>
  implements IBaseEntitiesService<UserEvent, UserEventFilter, any>
{
  constructor(
    protected graphql: StableGraphqlService,
    protected accountService: AccountService,
    protected network: NetworkService,
    protected translate: TranslateService,
    @Optional() @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    super(graphql, accountService, network, translate, { queries, mutations, subscriptions, production: environment?.production });

    // Customize icons
    this.registerListener({
      accept: () => true,
      onReceived: (userEvent: UserEvent) => {
        this.customize(userEvent);
        return userEvent;
      },
    });
  }

  asFilter(filter: Partial<UserEventFilter>): UserEventFilter {
    const result = UserEventFilter.fromObject(filter || {});
    result.patch(filter as any);
    return result;
  }

  fromObject(source: any): UserEvent {
    return UserEvent.fromObject(source);
  }

  watchPage(page: Page, filter?: Partial<UserEventFilter>, options?: UserEventWatchOptions<UserEvent>): Observable<LoadResult<UserEvent>> {
    filter = this.asFilter(filter);
    if (isEmptyArray(filter.recipients)) {
      filter.recipients = [this.defaultRecipient()];
    }
    if (!filter.startDate) {
      filter.startDate = moment().add(-1, 'year');
    }
    // Default page
    if (!page?.sortBy || !page?.sortDirection) {
      page = { ...page, sortBy: 'creationDate', sortDirection: 'desc' };
    }
    return super.watchPage(page, filter, {
      ...options,
      fetchPolicy: 'network-only',
      withContent: true,
    });
  }

  listenAllChanges(
    filter: Partial<UserEventFilter>,
    options?: UserEventWatchOptions<UserEvent> & {
      interval?: number;
      fetchPolicy?: FetchPolicy;
    }
  ): Observable<UserEvent[]> {
    return super.listenAllChanges(filter, {
      ...options,
      fetchPolicy: 'no-cache',
      withContent: true,
    });
  }

  listenCountChanges(
    filter: Partial<UserEventFilter>,
    options?: UserEventWatchOptions<UserEvent> & {
      interval?: number;
      fetchPolicy?: FetchPolicy;
    }
  ): Observable<number> {
    return super.listenCountChanges(filter, { ...options, fetchPolicy: 'no-cache' });
  }

  exportAllAsync(context: IExportContext, filter: UserEventFilter): Promise<Job> {
    throw new Error('NOT IMPLEMENTED');
  }

  protected defaultRecipient(): any {
    return this.accountService.person.id;
  }

  protected customize(userEvent: UserEvent) {
    // Choose default avatarIcon
    switch (userEvent.level) {
      case 'ERROR':
        userEvent.avatarIcon = { icon: 'close-circle-outline', color: 'danger' };
        break;
      case 'WARNING':
        userEvent.avatarIcon = { icon: 'warning-outline', color: 'warning' };
        break;
      case 'INFO':
        userEvent.avatarIcon = { icon: 'information-circle-outline', color: 'primary' };
        break;
      case 'DEBUG':
        userEvent.avatarIcon = { icon: 'cog', color: 'medium' };
        break;
    }
  }
}
