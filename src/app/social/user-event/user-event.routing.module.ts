import { RouterModule, Routes } from '@angular/router';
import { inject, NgModule } from '@angular/core';
import { CanLeave, COMPONENT_GUARD } from '@app/core/service/component-guard.service';
import { SocialModule } from '@sumaris-net/ngx-components';
import { UserEventTable } from '@app/social/user-event/user-event.table';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: UserEventTable,
    canDeactivate: [(component: CanLeave) => inject(COMPONENT_GUARD).canDeactivate(component, null, null, null)],
    data: {
      // profile: 'ADMIN',
    },
  },
];

@NgModule({
  imports: [SocialModule, RouterModule.forChild(routes)],
})
export class UserEventRoutingModule {}
