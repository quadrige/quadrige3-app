import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { PlatformService, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '@sumaris-net/ngx-components';
import { BaseTable } from '@app/shared/table/base.table';
import { accountConfigOptions } from '@app/core/config/account.config';
import { ISetFilterOptions } from '@app/shared/model/options.model';
import { UserEventService } from '@app/social/user-event/user-event.service';
import { UserEvent, userEventLevelI18nLabels } from '@app/social/user-event/user-event.model';
import { TextFrameComponent } from '@app/shared/component/text/text-frame.component';
import { Router } from '@angular/router';
import { UserEventFilter, UserEventFilterCriteria } from '@app/social/user-event/filter/user-event.filter.model';
import { QuadrigeCoreModule } from '@app/core/quadrige.core.module';
import { UserEventFilterForm } from '@app/social/user-event/filter/user-event.filter.form';

@Component({
  selector: 'app-user-event-table',
  templateUrl: './user-event.table.html',
  styleUrls: ['./user-event.table.scss'],
  providers: [{ provide: UserEventService, useClass: UserEventService }],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [QuadrigeCoreModule, UserEventFilterForm],
})
export class UserEventTable extends BaseTable<UserEvent, number, UserEventFilter, UserEventFilterCriteria> implements OnInit, AfterViewInit {
  showAllUsers: boolean;

  @ViewChild('message') message: TextFrameComponent;

  constructor(
    protected injector: Injector,
    protected _entityService: UserEventService,
    protected platform: PlatformService,
    protected router: Router
  ) {
    super(
      injector,
      // columns
      RESERVED_START_COLUMNS.concat(['type', 'level', 'recipientUser', 'creationDate']).concat(RESERVED_END_COLUMNS),
      UserEvent,
      _entityService,
      undefined
    );

    this.titleI18n = 'SOCIAL.USER_EVENT.TITLE';
    this.i18nColumnPrefix = 'SOCIAL.USER_EVENT.TABLE.';
    this.defaultSortBy = 'creationDate';
    this.defaultSortDirection = 'desc';
    this.logPrefix = '[user-event-table]';
  }

  get rightPanelButtonAccent(): boolean {
    return super.rightPanelButtonAccent && !!this.singleSelectedRow?.currentData.message;
  }

  // typeDisplayFn = (data: Job) => this.translate.instant(jobTypesI18nLabels[data.type]);

  levelDisplayFn = (data: UserEvent) => this.translate.instant(userEventLevelI18nLabels[data.level]);

  ngOnInit() {
    super.ngOnInit();
    this.inlineEdition = false;
    this.forceNoInlineEdition = true;

    this.showAllUsers = this.accountService.isAdmin() && this.settings.getPropertyAsBoolean(accountConfigOptions.showSocialAllUsers);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.restoreRightPanel(true, 50);
  }

  async resetFilter(filter?: UserEventFilter, opts?: ISetFilterOptions) {
    filter = this.asFilter(filter);
    filter.patch({ allRecipients: false });
    await super.resetFilter(filter, opts);
  }

  gotoJob(id: any) {
    this.router.navigate(['/social/Job'], {
      queryParams: {
        criterias: JSON.stringify({
          searchText: id.toString(),
        }),
      },
    });
  }

  // protected methods

  updatePermission() {
    this.canEdit = true;
  }

  protected async loadRightArea() {
    this.detectChanges();
    if (!this.singleSelectedRow) {
      this.message.value = undefined;
      this.markForCheck();
      return;
    }
    // Set the event message result
    this.message.value = this.singleSelectedRow.currentData.message;
    this.markForCheck();
  }

  protected getDefaultHiddenColumns(): string[] {
    return super.getDefaultHiddenColumns().filter((value) => value !== 'creationDate');
  }

  protected getRequiredColumns(): string[] {
    return super.getRequiredColumns().concat('type', 'recipientUser', 'creationDate');
  }
}
