import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SocialModule } from '@sumaris-net/ngx-components';
import { SocialHomePage } from '@app/social/menu/social-home.page';

const routes: Routes = [
  {
    path: '',
    component: SocialHomePage,
    data: {
      profile: 'ADMIN',
    },
    children: [
      {
        path: 'Job',
        loadChildren: () => import('./job/job.routing.module').then((m) => m.JobRoutingModule),
      },
      {
        path: 'UserEvent',
        loadChildren: () => import('./user-event/user-event.routing.module').then((m) => m.UserEventRoutingModule),
      },
    ],
  },
];

@NgModule({
  imports: [SocialModule, RouterModule.forChild(routes)],
})
export class SocialRoutingModule {}
