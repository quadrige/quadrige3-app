#!/bin/bash
# Get to the root project
if [[ "_" == "_${PROJECT_DIR}" ]]; then
  SCRIPT_DIR=$(dirname "$0")
  PROJECT_DIR=$(cd "${SCRIPT_DIR}/.." && pwd)
  export PROJECT_DIR
fi

cd "${PROJECT_DIR}" || exit 1

# Read parameters
version=$1
release_description=$2

if [[ ! $version =~ ^[0-9]+.[0-9]+.[0-9]+(-(alpha|beta|rc)[0-9]*)?$ ]]; then
  echo "Wrong version format"
  echo "Usage:"
  echo " > ./release.sh <version> <release_description>"
  echo "with:"
  echo " - version: x.y.z"
  echo " - release_description: a comment on release"
  exit 1
fi

### Control that the script is run on `dev` branch
resumeRelease=0
branch=$(git rev-parse --abbrev-ref HEAD)
if [[ ! "$branch" == "develop" ]]; then
  if [[ "$branch" == "release/$version" ]]; then
    echo "Resuming release ..."
    resumeRelease=1
  else
    echo ">> This script must be run under \`develop\` or \`release/$version\` branch"
    exit 1
  fi
fi

PROJECT_DIR=$(pwd)

### Get current version (package.json)
current=$(grep -oP "version\": \"\d+.\d+.\d+(-(alpha|beta|rc)[0-9]*)?" package.json | grep -m 1 -oP "\d+.\d+.\d+(-(alpha|beta|rc)[0-9]*)?")
if [[ "_$current" == "_" ]]; then
  echo ">> Unable to read the current version in 'package.json'. Please check version format is: x.y.z (x and y should be an integer)."
  exit 1
fi
echo "Current version: $current"

echo "**********************************"
if [[ $resumeRelease == 0 ]]; then
  echo "* Starting release..."
else
  echo "* Resuming release..."
fi
echo "**********************************"
echo "* new build version: $version"
echo "**********************************"

if [[ $resumeRelease == 0 ]]; then
  read -r -p "Is these new versions correct ? [y/N] " response
  response=${response,,} # tolower
  [[ ! "$response" =~ ^(yes|y)$ ]] && exit 1
  if ! git flow release start "$version"; then
    exit 1
  fi
fi

# Change the version in files: 'package.json'
sed -i "s/version\": \"$current\"/version\": \"$version\"/g" package.json

# Change version in file: 'src/assets/manifest.json'
currentManifestJsonVersion=$(grep -oP "version\": \"\d+.\d+.\d+(-(alpha|beta|rc)[0-9]*)?\"" src/assets/manifest.json | grep -oP "\d+.\d+.\d+(-(alpha|beta|rc)[0-9]*)?")
sed -i "s/version\": \"$currentManifestJsonVersion\"/version\": \"$version\"/g" src/assets/manifest.json

echo "----------------------------------"
echo "- Refresh dependencies..."
echo "----------------------------------"
if ! npm install; then
  exit 1
fi

echo "----------------------------------"
echo "- Compiling sources..."
echo "----------------------------------"
if ! npm run build.prod; then
  exit 1
fi

description="$release_description"
if [[ "_$description" == "_" ]]; then
  description="Release $version"
fi

echo "**********************************"
echo "* Uploading artifacts to Gitlab..."
echo "**********************************"
cd "$PROJECT_DIR"/scripts || exit 1
if ! ./deploy-gitlab.sh; then
  exit 1
fi

echo "**********************************"
echo "* Finishing release"
echo "**********************************"
cd "${PROJECT_DIR}"/scripts || exit 1
if ! ./release-finish.sh "$version" ''"$release_description"''; then
  exit 1
fi

echo "**********************************"
echo "* Build release succeed !"
echo "**********************************"
