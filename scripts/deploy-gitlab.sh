#!/bin/bash

# Get to the root project
if [[ "_" == "_${projectDir}" ]]; then
  scriptDir=$(dirname "$0")
  projectDir=$(cd "${scriptDir}/.." && pwd)
  export projectDir
fi;

cd "${projectDir}" || exit 1

projectName=quadrige

###  get token
GITLAB_TOKEN=$(cat ~/.config/${projectName}/.gitlab)
if [[ "_$GITLAB_TOKEN" = "_" ]]; then
    echo "ERROR: Unable to find gitlab authentication token file: "
    echo " - You can create such a token at https://gitlab.ifremer.fr/quadrige/quadrige3-app/-/settings/access_tokens"
    echo " - Then copy the token and paste it in the file '~/.config/${projectName}/.gitlab' using a valid token"
    exit 1
fi

# Get current version (package.json)
version=$(grep -ioP "version\": \"\d+.\d+.\d+(-(alpha|beta|rc)([0-9]*)?)?" package.json | grep -m 1 -ioP "\d+.\d+.\d+(-(alpha|beta|rc)([0-9]*)?)?")
if [[ "_$version" == "_" ]]; then
  echo ">> Unable to read the current version in 'package.json'. Please check version format is: x.y.z (x and y should be an integer)."
  exit 1;
fi
echo "Current version: ${version}"

fileName=${projectName}-${version}.zip
targetUrl="https://gitlab.ifremer.fr/api/v4/projects/1415/packages/generic/${projectName}/${version}/${fileName}"

echo "Checking version already deployed..."
# Check if the version is already deployed
if curl --output /dev/null --silent --head --fail "${targetUrl}"; then
  echo "This version is already deployed"
  exit 1;
fi

# Zip the www folder
mkdir -p "${projectDir}/dist"
zipFile=${projectDir}/dist/${fileName}
if [[ -f "${zipFile}" ]]; then
  rm "${zipFile}"
fi
cd "${projectDir}"/www || exit 1
if ! zip -q -r "${zipFile}" . ; then
  echo "Cannot create the archive for the web artifact"
  exit 1
fi

echo "Deploy to gitlab generic package :"
echo " File: ${zipFile}"
echo " Url: ${targetUrl}"
curl --header "DEPLOY-TOKEN: ${GITLAB_TOKEN}" --upload-file "${zipFile}" "${targetUrl}"
# url to download it :  https://gitlab.ifremer.fr/api/v4/projects/1415/packages/generic/${projectName}/$version/$zipBasename

checksum=$(sha256sum "${zipFile}" | sed 's/ /\n/gi' | head -n 1)

# Check if the version is correctly deployed
if curl --output /dev/null --silent --head --fail "${targetUrl}"; then
  echo "${projectName} ${version} correctly deployed"
  echo "checksum: ${checksum}"
else
  echo "Deployment failed"
  exit 1;
fi
