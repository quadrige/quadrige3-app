#!/bin/bash

# Get to the root project
if [[ "_" == "_${projectDir}" ]]; then
  scriptDir=$(dirname "$0")
  projectDir=$(cd "${scriptDir}/.." && pwd)
  export projectDir
fi;

cd "${projectDir}" || exit 1

projectName=quadrige
repoUrl=https://gitlab.ifremer.fr/api/v4/projects/quadrige%2Fquadrige3-app/packages/maven
#repoUrl=https://nexus.e-is.pro/nexus/content/repositories/quadrige3-core-releases
groupId=fr.ifremer.quadrige3
artifactId=quadrige3-app

# Get current version (package.json)
version=$(grep -ioP "version\": \"\d+.\d+.\d+(-(alpha|beta|rc)([0-9]+)?)?" package.json | grep -m 1 -ioP "\d+.\d+.\d+(-(alpha|beta|rc)([0-9]+)?)?")
if [[ "_$version" == "_" ]]; then
  echo ">> Unable to read the current version in 'package.json'. Please check version format is: x.y.z (x and y should be an integer)."
  exit 1;
fi
echo "Current version: $version"

finalFileUrl=$repoUrl/${groupId//.//}/$artifactId/$version/$artifactId-$version.zip

# Check if the version is already deployed
if curl --output /dev/null --silent --head --fail "$finalFileUrl"; then
  echo "This version is already deployed"
  exit 1;
fi

# Zip the www folder
mkdir -p "${projectDir}/dist"
zipFile=${projectDir}/dist/$projectName-$version.zip
if [[ -f "$zipFile" ]]; then
  rm "$zipFile"
fi
cd "$projectDir"/www || exit 1
if ! zip -q -r "$zipFile" . ; then
  echo "Cannot create the archive for the web artifact"
  exit 1
fi

# Deploy to Nexus as a Maven artifact
#-DrepositoryId=eis-nexus-deploy if deployment on eis
mvn deploy:deploy-file -DrepositoryId=quadrige3-app-gitlab-deploy \
  -Dfile="$zipFile" \
  -Durl=$repoUrl \
  -DgroupId=$groupId \
  -DartifactId=$artifactId \
  -Dversion="$version" \
  -Dpackaging=zip

checksum=$(sha256sum "$zipFile" | sed 's/ /\n/gi' | head -n 1)

# Check if the version is correctly deployed
if curl --output /dev/null --silent --head --fail "$finalFileUrl"; then
  echo "$projectName $version correctly deployed"
  echo "checksum: $checksum"
else
  echo "Deployment failed"
  exit 1;
fi
