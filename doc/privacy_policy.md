## Privacy policy

Quadrige App use the user's device for:

  - Upload pictures, such as user profile's avatar or picture of fish; 
  - Listening network status, to warn the user when he lost the network. 

### Network status

Privacy policy are : 

 - When the App starts, it listen the network status. A warning is displayed when the network is lost;
 - But no data is stored or uploaded, locally or anywhere;
