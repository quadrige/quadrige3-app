# Quadrige

## Cycle de vie du projet

### Branches

Suivre au maximum la [convention de nommages](https://dev-ops.gitlab-pages.ifremer.fr/documentation/gitlab_quickstart/git/git/#convention-de-nommage-de-branche) et les [bonnes pratiques](https://dev-ops.gitlab-pages.ifremer.fr/documentation/dev_best_practices/general/branch/) préconisée à Ifremer concernant les branches Git.

### Processus d'intégration continue

Le processus d'intégration continue mis en place est normalisé, car il s'appuie sur le [socle CI/CD Ifremer](https://dev-ops.gitlab-pages.ifremer.fr/templates/automatisation/ci-cd/), \
qui propose un [catalogue de pipelines d'automatisation Java](https://dev-ops.gitlab-pages.ifremer.fr/templates/automatisation/ci-cd/pipelines/java/) \
utilisé dans le pipeline du projet `.gitlab-ci.yml`.

### Version

Pour générer une version de l'application [créer un tag Git](https://dev-ops.gitlab-pages.ifremer.fr/documentation/gitlab_quickstart/gitlab/repository/#creer-une-version). Ceci exécutera automatiquement les tâches automatisées permettant de générer les livrables.

### Release

Une releases Gitlab est créée automatiquement à la création d'une version, elle regroupe les sources de l'application et ses artefacts pour la version en question. La liste des release est consultable depuis la page principale du projet.

### Déploiement

Avoir connaissance des [exigences Ifremer pour l'hébergement](https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/overview/resume-des-exigences-hebergement/#resume-des-exigences-pour-lhebergement) ainsi que de la [procédure à suivre pour héberger une application dans les plateformes Dockerisées](https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/integration/golden-path/).

#### Validation

Suivre la [procédure dédiée](https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/declaration/deployer-et-tester-une-application-sur-isival/) pour une première intégration de l'application à la plateforme Dockerisée de validation Ifremer.

#### Production

Procédure de mise en production : <https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/overview/mex-et-mep/>

## Développement

### Docker

#### Exécution avec Docker Compose

L'application a été dockerisées afin de simplifier son intégration dans les différents environnement, y compris sur son poste en local.

Afin de tester vos développement sous Docker, il faut builder l'application l'exécuter grâce aux commandes ci-dessous (Cf. fichier `compose.yml` pour plus de détails sur les conteneurs exécutés) :

```bash
# build l'application
npm run build.dev
# exécution de l'application
docker compose up
```

URL webapp : <http://localhost:8080>

#### Logs

Les logs s'affichent dans la sortie standard du conteneur.

```bash
docker logs quaridge-app -f
```

#### Arrêt et relance

```bash
docker compose down # arret
docker compose up   # relance
```
